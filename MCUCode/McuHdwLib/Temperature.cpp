/**
 * @file	Temperature.cpp
 * @par		Package: UGMcu
 * @par		Project: Union Generator
 * @par		Copyright (c) 2017, Uptake Medical Technology, Inc.
 * @author	Glen Sep 15, 2010 Created file
 * @brief	This file contains the code for getting thermocouple temperatures in EGU, CTemperature
 */

// Include Files
#include "Temperature.h"
#include "LogMessage.h"

// External Public Data
CTemperature * g_pTemperature = 0;

// File Scope Macros and Constants

// File Scope Type Definitions (Enums, Structs & Classes)
#define THERMO_UNUSED_LOW_BITS (3)
#define THERMO_LOW_BITS_MASK   (0x7FF8)
#define THERMO_BITS_ALWAYS_LOW (0x8004)

// File Scope Data

// File Scope Functions

// Public Functions

/**
 * @brief	This is the constructor for CTemperature. It sets the temperature values to zero
 *
 */
CTemperature::CTemperature()
{
	g_pTemperature = this;

	for ( int i = 0; i < THERMO_CNT; i++ )
		{
		m_LastErrs[ i ] = ERR_OK;
		m_LastTemps[ i ] = 0;
		m_FilteredTemps[ i ] = 0;
		for ( int j = 0; j < TEMPERATURE_SPI_BYTES; j++ )
			m_thermoRaw[ i ][ j ] = 0;
		for ( int j = 0; j < TEMPERATURE_FIR_COUNT; j++ )
			m_firBuffer[ i ][ j ] = 0;
		}

	m_bHwFault = false;
}

/**
 * @brief	This method initializes the thermocouple hardware and pass mask value.
 *
 * The initialization is to set up the SPI bus. The pass mask value of PASS_MASK_TEMPER is used.
 *
 * @return   Status as upErr
 */
upErr CTemperature::InitializeHardware()
{
	STwoLinearPiecesLongIntFactors sFactors;
	SetPassMask( PASS_MASK_TEMPER );
	LOG_ERROR_RETURN( IdentifyRevision() )

	if ( MCU_BOARD_C == GetBoardRevision() )
		{
		// Apply the outlet cal to all TC using MAX6675 part
		sFactors.ilBoffsetUpper = CNVT_REAL_TO_LINEAR_B( -5.307 );
		sFactors.ilMslopeUpper = CNVT_REAL_TO_LINEAR_M( 1.0075 );
		sFactors.ilBreakPoint = 0;
		sFactors.ilBoffsetLower = CNVT_REAL_TO_LINEAR_B( -5.307 );
		sFactors.ilMslopeLower =CNVT_REAL_TO_LINEAR_M( 1.0075 );
		SetFactors( THERMO_OUTLET, sFactors );
		SetFactors( THERMO_INTERIOR, sFactors );
		SetFactors( THERMO_SPARE, sFactors );

		// Apply coil cal
		sFactors.ilBoffsetUpper = CNVT_REAL_TO_LINEAR_B( -2.8913 );
		sFactors.ilMslopeUpper = CNVT_REAL_TO_LINEAR_M( 1.01193 );
		sFactors.ilBreakPoint = 0;
		sFactors.ilBoffsetLower = CNVT_REAL_TO_LINEAR_B( -2.8913 );
		sFactors.ilMslopeLower = CNVT_REAL_TO_LINEAR_M( 1.01193 );
		SetFactors( THERMO_COIL, sFactors );
		}

	else if ( MCU_BOARD_D == GetBoardRevision() )
		{
		// Apply the coil cal to TC
		sFactors.ilBoffsetUpper = CNVT_REAL_TO_LINEAR_B( 7.7386 );
		sFactors.ilMslopeUpper = CNVT_REAL_TO_LINEAR_M( 0.969 );
		sFactors.ilBreakPoint = 305;
		sFactors.ilBoffsetLower = CNVT_REAL_TO_LINEAR_B( -3.2998 );
		sFactors.ilMslopeLower =CNVT_REAL_TO_LINEAR_M( 1.005 );
		SetFactors( THERMO_OUTLET, sFactors );
		SetFactors( THERMO_INTERIOR, sFactors );
		SetFactors( THERMO_COIL, sFactors );
		}
	else
		{
		LOG_ERROR_RETURN( ERR_INTERNAL_FAULT );
		m_bHwFault = true;
		}

	return InitializeSpi();
}

/**
 * @brief	This method reads the four thermocouples on the SPI bus.
 *
 * It reads the high-speed thermocouple, THERMO_COIL, on each loop and reads the other thermocouples
 * on passes as controlled by the PassMask value PASS_MASK_TEMPER. This must give enough time for the MAX6675
 * chips to complete a conversion (~150ms) or those chips will never create new data.
 *
 * @return   Status as upErr
 */
upErr CTemperature::ReadFromHardware()
{
	upErr eErr = ERR_OK;
	upMcuBoardRevision eBoard = GetBoardRevision();

	LOG_ERROR_RETURN( eErr = ReadSpi( SPI_THERMO_COIL, TEMPERATURE_SPI_BYTES, m_thermoRaw[ THERMO_COIL ] ) );
	m_LastErrs[ THERMO_COIL ] = eErr;
	if ( ERR_OK == eErr )
		ConvertThermoTemp( THERMO_COIL );

	// Board C has three MAX6675 parts
	if ( MCU_BOARD_C == eBoard )
		{
		if ( ERR_OK == eErr && GetPassEnabled() )
			{
			if ( ERR_OK == eErr )
				{
				LOG_ERROR_RETURN( eErr = ReadSpi( SPI_THERMO_SPARE, TEMPERATURE_SPI_BYTES, m_thermoRaw[ THERMO_SPARE ] ) );
				m_LastErrs[ THERMO_SPARE ] = eErr;
				if ( ERR_OK == eErr )
					ConvertThermoTemp( THERMO_SPARE );
				}
			if ( ERR_OK == eErr )
				{
				LOG_ERROR_RETURN( eErr = ReadSpi( SPI_THERMO_INTERIOR, TEMPERATURE_SPI_BYTES, m_thermoRaw[ THERMO_INTERIOR ] ) );
				m_LastErrs[ THERMO_INTERIOR ] = eErr;
				if ( ERR_OK == eErr )
					ConvertThermoTemp( THERMO_INTERIOR );
				}
			if ( ERR_OK == eErr )
				{
				LOG_ERROR_RETURN( eErr = ReadSpi( SPI_THERMO_OUTLET, TEMPERATURE_SPI_BYTES, m_thermoRaw[ THERMO_OUTLET ] ) );
				m_LastErrs[ THERMO_OUTLET ] = eErr;
				if ( ERR_OK == eErr )
					ConvertThermoTemp( THERMO_OUTLET );
				}
			}
		}

	// Board D has two more high-speed channels
	else if ( MCU_BOARD_D == eBoard )
		{
		if ( ERR_OK == eErr )
			{
			LOG_ERROR_RETURN( eErr = ReadSpi( SPI_THERMO_INTERIOR, TEMPERATURE_SPI_BYTES, m_thermoRaw[ THERMO_INTERIOR ] ) );
			m_LastErrs[ THERMO_INTERIOR ] = eErr;
			if ( ERR_OK == eErr )
				ConvertThermoTemp( THERMO_INTERIOR );
			}
		if ( ERR_OK == eErr )
			{
			LOG_ERROR_RETURN( eErr = ReadSpi( SPI_THERMO_OUTLET, TEMPERATURE_SPI_BYTES, m_thermoRaw[ THERMO_OUTLET ] ) );
			m_LastErrs[ THERMO_OUTLET ] = eErr;
			if ( ERR_OK == eErr )
				ConvertThermoTemp( THERMO_OUTLET );
			}
		}
	// Bad board code
	else
		{
		LOG_ERROR_RETURN( eErr = ERR_BOARD_ASSEMBLY_UNKNOWN )
		m_bHwFault = true;
		}

	// Check for internal faults received by the SPI Bus
	if ( ERR_INTERNAL_FAULT == m_LastErrs[ THERMO_SPARE ]
	   || ERR_INTERNAL_FAULT == m_LastErrs[ THERMO_INTERIOR ]
	   || ERR_INTERNAL_FAULT == m_LastErrs[ THERMO_OUTLET ]
	   || ERR_INTERNAL_FAULT == m_LastErrs[ THERMO_COIL ] )
		{
		// Set the hardware fault flag permanently for later retrieval by other monitoring classes
		LOG_ERROR_RETURN( eErr = ERR_INTERNAL_FAULT )
		m_bHwFault = true;
		}

	return eErr;
}

/**
 * @brief	This method gets one temperature measurement as fixed point integer
 * 			degrees Celsius.
 *
 * 			The fixed point temperature is calculated using
 *          a conversion factor of 0.25 and a fixed point scaling
 * 			factor of 1. The conversion factor is found by computing 1023.75
 * 			Celsius / 4095 where 1023.75 is the max temperature read by the MAX6675
 * 			and 4095 represents 12-bits of data (0xFFF). A returned value of 0xFFFF
 *          indicates an error.
 *
 * @param	eTemp An enumeration that specifies which thermocouple to get
 *
 * @return   Status as upErr
 */
upErr CTemperature::ConvertThermoTemp( upTempMeas eTemp )
{
	// Max measurable coil temp is 590
#define COIL_ERROR_LEVEL (0xFC8)

	upErr eErr = ERR_OK;
	int rawTemp = 0;
	int iTemp;
	bool_c bHighSpeed;

	if ( eTemp >= THERMO_CNT )
		return ERR_ENUM_OUT_OF_RANGE;

	// Determine what type of TC is being converted
	bHighSpeed = ( THERMO_COIL == eTemp || MCU_BOARD_D == GetBoardRevision() ? true : false );

	// For high speed raw data is stored MSB first (index 0)
	if ( bHighSpeed )
		{
		twobytes tbRawTemp;
		tbRawTemp = m_thermoRaw[ eTemp ][ 1 ] + ( m_thermoRaw[ eTemp ][ 0 ] << 8 );
		// Coil ADC drifts to maximum when TC is open
		if ( COIL_ERROR_LEVEL > tbRawTemp )
			{
			// Multiply to provide higher resolution
			tbRawTemp <<= (twobytes) 4;
			// 0.14648n = ( n/8 + n/64 + n/256 + n/512)
			tbRawTemp = ( tbRawTemp >> (twobytes) 3 ) + ( tbRawTemp >> (twobytes) 6 ) + ( tbRawTemp >> (twobytes) 8 )
			        + ( tbRawTemp >> (twobytes) 9 );
			iTemp = (int) ( tbRawTemp >> (twobytes) 4 );
			}
		else
			{
			iTemp = 0;
			eErr = ERR_THERMO_ERROR;
			}

		}
	else
		{
		rawTemp = ( m_thermoRaw[ eTemp ][ 0 ] << 8 ) + m_thermoRaw[ eTemp ][ 1 ];

		if ( eErr == ERR_OK && ( rawTemp & THERMO_BITS_ALWAYS_LOW ) == 0 )
			{
			// Only bits D14:D3 are useful
			rawTemp = ( rawTemp & THERMO_LOW_BITS_MASK ) >> THERMO_UNUSED_LOW_BITS;
			// Resolution found by computing ( 1023.75 Celsius / 4095 ),
			// where 4095 is 2^12 - 1.  Will use fixed-point scaling factor of 100
			// RS_FACTOR = resolution of 0.25 multiplied by scaling factor of 100
			iTemp = rawTemp >> 2;
			}
		else
			{
			iTemp = 0;
			eErr = ERR_THERMO_ERROR;
			}
		}

	// Apply the cal if ERR_OK or set to zero
	if ( ERR_OK == eErr )
		{
		eErr = ApplyCal( eTemp, iTemp, m_LastTemps + eTemp );
		}
	else
		m_LastTemps[ eTemp ] = 0;

	// Apply FIR
	int iTempOther = m_LastTemps[ eTemp ];
	for ( int i = 0; i < TEMPERATURE_FIR_COUNT - 1; i++ )
		{
		iTempOther += m_firBuffer[ eTemp ][ i + 1 ];
		m_firBuffer[ eTemp ][ i ] = m_firBuffer[ eTemp ][ i + 1 ];
		}

	m_firBuffer[ eTemp ][ TEMPERATURE_FIR_COUNT - 1 ] = m_LastTemps[ eTemp ];
	m_FilteredTemps[ eTemp ] = iTempOther / TEMPERATURE_FIR_COUNT;
	m_LastErrs[ eTemp ] = eErr;

	return eErr;
}

/**
 * @brief	This method get the filtered temperature value
 *
 * @param	eTemp The enumeration as upTempMeas for the temperature to get
 * @param	pfTemp A pointer of int* to receive the temeprature
 *
 * @return   Status as upErr
 */
upErr CTemperature::GetFilteredThermoTemp( upTempMeas eTemp, int * pfTemp )
{
	if ( THERMO_CNT <= eTemp || 0 == pfTemp )
		return ERR_ENUM_OUT_OF_RANGE;

	*pfTemp = m_FilteredTemps[ eTemp ];
	return m_LastErrs[ eTemp ];
}

/**
 * @brief	This method get the last read temperature value
 *
 * @param	eTemp The enumeration as upTempMeas for the temperature to get
 * @param	pfTemp A pointer of int* to receive the temeprature
 *
 * @return   Status as upErr
 */
upErr CTemperature::GetThermoTemp( upTempMeas eTemp, int * pfTemp )
{
	if ( THERMO_CNT <= eTemp || 0 == pfTemp )
		return ERR_ENUM_OUT_OF_RANGE;

	*pfTemp = m_LastTemps[ eTemp ];
	return m_LastErrs[ eTemp ];
}

/**
 * @brief	This method returns the status of the SpiBus Hardware.
 *
 * @return   Status as a bool_c.  TRUE if a Hardware Fault occurred.
 */
bool_c CTemperature::GetHwFault()
{
	return m_bHwFault;
}

// Private Class Functions

/**
 * @file	OneWireDriver.cpp
 * @par		Package: UGMcu
 * @par		Project: Union Generator
 * @par		Copyright (c) 2017, Uptake Medical Technology, Inc.
 * @author	Glen Sep 16, 2010 Created file
 * @author  Chris Feb 3, 2011 Added driver code
 * @brief	This code performs 1-wire read and write operations using the TWI bus to a Maxim DS2482-100 in COneWireDriver
 */

// Include Files
#include "UGMcuDefinitions.h"
#include "OneWireDriver.h"
#include "UGMcuTypes.h"
#include "UGError.h"
#include "stddef.h"
#include "string.h"
#include "LogMessage.h"

// External Public Data

// File Scope Macros and Constants

// File Scope Type Definitions (Enums, Structs & Classes)

// File Scope Data

// File Scope Data

// File Scope Functions

// Public Functions

/**
 * @brief	This is the constructor for COneWireDriver
 *
 */
COneWireDriver::COneWireDriver()
{
	m_owPresence = false;
	m_twiMsgSize = 0;
	m_twiRxCnt = 0;
	m_twiBufPtr = 0;
	m_twiRepStart = false;
	m_twiState = 1;
	m_bOffsetByte = 0;
	m_bHwResponse = false;
	ClearRomInfo();
}


/**
 * @brief	This method initializes the TWI port and 1-wire hardware.
 *
 *
 * @return   Status as upErr
 */
upErr COneWireDriver::InitializeHardware()
{
	upErr eErr = ERR_OK;
	twobytes tbFraction;
	byte bDivide = 0;
	byte bPrescale = 0;

	// Compute diver and prescaler so CLKio is just below 400KHz
	// The ratio is given by 16+2*TWBR*4**TWPS
	// TWBR must be 10 or more

	tbFraction = ( SYSTEM_CLOCK_RATE / TWI_CLOCK_RATE );
	// Per at90can128 datasheet, clock divider (TWBR) cannot be less than 10
	tbFraction = ( tbFraction - 16 ) / ( 2 * TWI_MIN_TWBR);
	bPrescale = 0;

	//  Solve ( 4^TWPS == tbFraction ) to find prescaler
	while ( tbFraction > 1 )
		{
		tbFraction = tbFraction / 4;
		bPrescale++;
		}

	bDivide = (byte) ( tbFraction & 0xFF );

// Set clock divider

	// Set TWI prescaler bits

	// Default content = SDA released.

	// Enable TWI operation

	// Initialize the Dallas IC
	eErr = I2CConfigMaster();
	if ( ERR_I2C_DEVICE_ERROR == eErr )
		m_bHwResponse = false;
	else
		m_bHwResponse = true;

	return eErr;
}

/**
 * @brief	This method performs needed read operations. This includes checking for
 * 			the presence of a 1-wire memory on each cycle.  If the hardware did not
 * 			pass initialization, return immediately.
 *
 * @return   Status as upErr
 */
upErr COneWireDriver::ReadFromHardware()
{
	// Update the presence status of the one-wire device
	upErr eErr = ERR_OK;
	// If the I2C Master IC was not found, return immediately
	// The error was already logged in the initalization, so don't continue
	if ( false == m_bHwResponse )
		return ERR_OK;

	bool_c owPresenceCheck = false;
	LOG_ERROR_RETURN( eErr = OwPresenceCheck( &owPresenceCheck ) )
	// don't need to return error since presence is not necessary
	m_owPresence = owPresenceCheck;

	return eErr;
}

/**
 * @brief	This method returns whether the I2C Master IC returned expected information
 * 			during the hardware initialization step.
 *
 * @return   A boolean status: true == I2C Master IC found and functioning, false = initialization failure
 */
bool_c COneWireDriver::ReturnHwHealth()
{
	return m_bHwResponse;
}

/**
 * @brief	This method returns the state of the 1wire presence
 *
 * @param   bOwPresence A pointer to receive the current one-wire presence state
 *
 * @return   Status as upErr
 */
upErr COneWireDriver::GetOwPresence(bool_c * bOwPresence )
{
	upErr eErr = ERR_OK;

	*bOwPresence = m_owPresence;

	return eErr;
}


// Private Class Functions


/**
 * @brief	This method clears the stored one-wire rom information
 *
 */
void COneWireDriver::ClearRomInfo()
{
	for ( int i = 0; i < OW_ROM_SIZE; i++ )
		m_romInfo[ i ] = 0;
}


/**
 * @brief	This function returns the state of the TWI interface
 *
 * @return   The TWI interface state
 */
byte COneWireDriver::I2CGetStateInfo()
{
	twobytes tbPollCount = 0;

	while ( I2CTxcBusy() && tbPollCount++ < OW_MAX_POLLS )
		; // Wait until TWI has completed the transmission.
	return ( m_twiState ); // Return error state.
}

/**
 * @brief	This function returns whether the I2C interface is
 * 			in a busy state.
 *
 * @return   The busy status of I2C
 */
byte COneWireDriver::I2CTxcBusy()
{
	byte returnStatus = ERR_OK;
	// If the TWI interrupt is enabled, it is busy

	return returnStatus;
}

/**
 * @brief	This function verifies that the DS2482 is on the I2C bus
 *
 * @param	pbStatusReg A pointer to the status register value returned
 *
 * @return  Status of I2C setup
 */
upErr COneWireDriver::I2CCheckForDevice( byte * pbStatusReg )
{
	upErr eErr = ERR_OK;
	byte statusRtn = 0;
//	byte dataOut[ 2 ] =	{ DS2482_SLAVE_ADDR, DS2482_PTR_STATUS };

	*pbStatusReg = statusRtn;

	// send start condition
	// wait for TWINT flag set == START has been transmitted
	if ( I2CTransmit( I2C_TX_START ) != TWI_START )
		return ERR_I2C_DEVICE_ERROR;

	// load SLA_W into TWDR register

	// Check value of TWI status register.  Mask pre-scaler bits.  If status
	// different than MTA_SLA_ACK, return error


	// load data in TWDR register (reset command) and transmit

	// check value of twi status register.  If status != MT_DATA_ACK, return error


	// send a repeated start

	// set to master receive mode

	// TWINT bit to start transmission of address and place in MR mode


	// now in Master Receive mode, will expect to receive data

	// expecting to receive a data NACK indicating we are finished receiving data

	// transmit STOP
	I2CTransmit( I2C_TX_STOP );

	*pbStatusReg = statusRtn;

	return eErr;
}

/**
 * @brief	This function sets the TWI configuration register to the
 *    		transmit state type specified
 *
 * @param	eTxState The state of the transmission
 *
 * @return  The I2C peripheral status register
 */
byte COneWireDriver::I2CTransmit( upI2cTxStates eTxState )
{
//	twobytes tbPollCount = 0;

	switch ( eTxState )
		{
		case I2C_TX_START:
			// send start condition
			break;
		case I2C_TX_DATA:
			break;
		case I2C_TX_DATA_ACK:
			break;
		case I2C_TX_STOP:
			break;
		default:
			break;
		}

	//Remove if want interrupt driven I2C driver
	if ( eTxState != I2C_TX_STOP )
		{
		// Use interrupt polling
		}
	return 1;
}

/**
 * @brief	This function sends a command to the DS2482 and optionally
 *    		reads data back into a buffer by sending a repeated start
 *    		after the commands are sent.  If there are no commands
 *    		sent, the function will simply send a start command and
 *    		read bytes into the buffer.
 *
 * @param	pbCommands An array of commands to send
 * @param   bCmdSize The number of command bytes to send
 * @param   pbRtnRead A pointer to return bytes read from the DS2482
 * @param   bRtnSize The expected number of bytes to return
 * @param   bStopFlag  A flag to determine whether to send an I2C stop command
 *
 * @return  The status/result of sending the command
 */
upErr COneWireDriver::I2CSendCommand( byte * pbCommands, byte bCmdSize, byte * pbRtnRead, byte bRtnSize, byte bStopFlag )
{
	upErr eErr = ERR_OK;
	byte bCounter = 1;

	// load SLA_W into TWDR register
	if ( bCmdSize > 0 )
		{
//		byte testByte = I2CTransmit( I2C_TX_START );

		// Check value of TWI status register.  Mask pre-scaler bits.  If status
		// different than MTA_SLA_ACK, return error

		}
	// load data in TWDR register (reset command) and transmit
	while ( bCounter < bCmdSize )
		{

//		bCounter =				pbCommands[ bCounter++ ];
		// check value of twi status register.  If status != MT_DATA_ACK, return error

		}

	// reset counter
	bCounter = 0;
	if ( bRtnSize > 0 )
		{
		// send repeated start
		I2CTransmit( I2C_TX_START );


		// set in master receive mode

		// Check value of TWI status register.  Mask pre-scaler bits.  If status
		// different than MTA_SLA_ACK, return error


		while ( bCounter < bRtnSize )
			{
			pbRtnRead[ bCounter++ ] = 1;
			}
		// expecting to receive a data NACK indicating we are finished receiving data

		}
	if ( bStopFlag )
		I2CTransmit( I2C_TX_STOP );
	return eErr;
}

/**
 * @brief	This function resets and configures the DS2482 and
 *    		resets the one-wire device if present on the bus.
 *    		The ROM information is stored locally and returned
 *    		via a pointer passed into the function.
 *    		The DS2482 configuration register is set to allow
 *    		standard one-wire communication speeds (100kbps)
 *    		with no active pullups and no strong pullups.
 *
 *
 * @return  The status/result of configuring the device
 */
upErr COneWireDriver::I2CConfigMaster()
{
	upErr eErr = ERR_OK;
	byte statusRtn = 0;
	byte dataIn[ 1 ];
	byte crcCheck = 0;
	byte romInfo[ OW_ROM_SIZE ];

	eErr = I2CCheckForDevice( &statusRtn );
	if ( eErr != ERR_OK )
		return eErr;

	// device should now be in reset mode
	if ( ( statusRtn & DS2482_RESET_MODE ) == 0 )
		return ERR_I2C_DEVICE_ERROR;

	// Must delay to allow DS2482 to fully reset
	MsDelay( 2 );
	eErr = DsConfig( dataIn );
	// send a 1-Wire Reset
	if ( eErr != ERR_OK )
		return eErr;

	eErr = OwReset( dataIn );
	if ( 0 == ( dataIn[ 0 ] & DS2482_PPD ) )
		// Pulse presence not detected
		m_owPresence = false;
	else
		{
			// 1-wire is present.  Retrieve ROM information
			m_owPresence = true;
			if ( ( eErr = OwSendReadRom() ) != ERR_OK )
				return eErr;
			if ( ( eErr = OwReadBytes( (byte*) romInfo, OW_ROM_SIZE ) ) != ERR_OK )
				return eErr;
			// check CRC
			crcCheck = OwPerformCrc( (byte*) romInfo, OW_ROM_SIZE );
			if ( 0 == crcCheck )
				memcpy( (void*) m_romInfo, (void*) romInfo, OW_ROM_SIZE );
			else
			{
				ClearRomInfo();
				return ERR_OW_DATA_READ_ERROR;
			}
		}
	MsDelay( 1 );

	return eErr;
}

/**
 * @brief	This function writes data to the one-wire device
 *   		from the StartAddress to the EndAddress.  The function
 *    		only copies up to OW_SCPAD_MAX_BYTES which is the
 *    		size of a memory page (32-bytes).
 *
 * @param	iStartAddress The starting one-wire target address
 * @param   pbData The data to write to the one-wire device
 * @param   bNumBytes The number of bytes to write to the scratchpad
 * @param   bPass The substate of the write memory state machine
 *
 * @return  The status of writing to the one-wire memory indicated
 */
upErr COneWireDriver::OwWriteMem( twobytes iStartAddress, byte * pbData, byte bNumBytes, byte bPass )
{
	upErr eErr = ERR_OK;
	byte esOffsetByte = 0;
	byte bIndex = 0;
	byte readScPad[ OW_SCPAD_MAX_BYTES ];
	// Can only write 32 bytes at a time
	if ( bNumBytes > OW_SCPAD_MAX_BYTES )
		return ERR_OW_DATA_WRITE_ERROR;

	switch ( bPass )
	{
		case WRITE_PASS_1:
			if ( ( eErr = OwWriteScPad( (byte*)&iStartAddress, pbData, bNumBytes ) ) != ERR_OK )
				return eErr;
			// Send a reset pulse to ensure the scratchpad can be read
			break;
		case WRITE_PASS_2:
			// Read back the value written to ensure its validity
			if ( ( eErr = OwReadScPad( (byte*)&iStartAddress, &esOffsetByte, readScPad, bNumBytes ) ) != ERR_OK )
				return eErr;
			m_bOffsetByte = esOffsetByte;
			// ensure that the data read from the scratchpad matches the data written
			for ( bIndex = 0; bIndex < bNumBytes; bIndex++ )
				if ( readScPad[ bIndex ] != pbData[ bIndex ] )
					return ERR_OW_DATA_WRITE_ERROR;
			break;
		case WRITE_PASS_3:
			// Send the command to copy over the scratch pad
			if ( ( eErr = OwCopyScPad( (byte*)&iStartAddress, m_bOffsetByte ) ) != ERR_OK )
				return ERR_OW_DATA_WRITE_ERROR;
			break;
		case WRITE_PASS_4:
			// reset the offset byte as is no longer used
			m_bOffsetByte = 0;
			eErr = OwReadMem( iStartAddress, readScPad, bNumBytes );
			// check again if memory has been written correctly
			if ( ERR_OK == eErr )
				{
				byte resetData;
				for ( bIndex = 0; bIndex < bNumBytes; bIndex++ )
					{
					if ( readScPad[ bIndex ] != pbData[ bIndex ] )
						return ERR_OW_DATA_WRITE_ERROR;
					}
				// reset the device
				OwReset( &resetData );
				}
			else
				return eErr;
			break;
		default:
			// send a reset
			m_bOffsetByte = 0;
			byte dataIn;
			OwReset( &dataIn );
			eErr = ERR_BAD_PARAM;
			break;
	}

	return eErr;
}

/**
 * @brief	This function reads data from a block of memory on
 *    		the one-wire device from the StartAddress to the EndAddress.
 *    		This function can only read OW_SCPAD_MAX_BYTES, or one page
 *    		of one-wire memory at a time
 *
 * @param	iStartAddress The starting one-wire target address
 * @param   pData The data to write to the one-wire device
 * @param   bNumBytes The number of bytes to read from the one-wire
 *
 * @return  The status of the one-wire data read
 */
upErr COneWireDriver::OwReadMem( twobytes iStartAddress, byte * pData, byte bNumBytes )
{
	upErr eErr = ERR_OK;
	byte owStatus = 0;
	byte addHighByte = (byte)( iStartAddress >> 8 );
	byte addLowByte = (byte)( iStartAddress & 0x00FF );

	if ( ( eErr = OwReset( &owStatus ) ) != ERR_OK )
		return eErr;
	// Check the presence bit
	if ( ( owStatus & DS2482_PPD ) == 0 )
		return ERR_OW_DATA_READ_ERROR;

	OwSendSkipRom();

	if ( bNumBytes > OW_SCPAD_MAX_BYTES )
		return ERR_OW_DATA_READ_ERROR;
	if ( ( eErr = OwWriteByte( OW_READ_MEMORY ) ) != ERR_OK )
		return eErr;
	if ( ( eErr = OwWriteByte( addLowByte ) ) != ERR_OK )
		return eErr;
	if ( ( eErr = OwWriteByte( addHighByte ) ) != ERR_OK )
		return eErr;
	if ( ( eErr = OwReadBytes( pData, bNumBytes ) ) != ERR_OK )
		return eErr;

	MsDelay( 1 );

	return eErr;
}

/**
 * @brief	This function polls the status register of the one-wire
 *    		master IC until the one-wire busy bit is 0.  It also
 *    		returns a count of the number of times polled to help
 *    		with timing analysis.
 *
 * @param	pPollCount The number of polls
 * @param   bStopFlag A boolean that indicates if the transmission will be halted
 *
 * @return  The status of polling the one-wire
 */
upErr COneWireDriver::OwBusyPoll( twobytes * pPollCount, byte bStopFlag )
{
	upErr eErr = ERR_OK;
	byte statusReg = 0x01;
	twobytes pollCount = 0;

	// send repeated start
	I2CTransmit( I2C_TX_START );


	// set in master receive mode

	// Check value of TWI status register.  Mask pre-scaler bits.  If status
	// different than MTA_SLA_ACK, return error

	// keep reading until the 1WB byte is 0
	while ( ( statusReg & OW_BUSY ) != 0 && pollCount < OW_MAX_POLLS )
		{

		pollCount++;
		}

	// expecting to receive a data NACK indicating we are finished receiving data


	if ( bStopFlag )
		I2CTransmit( I2C_TX_STOP );

	*pPollCount = pollCount;
	return eErr;
}

/**
 * @brief	This function writes the configuration register of the
 *    		Dallas One-Wire Master IC.  The configuration is set as follows:
 * 				Active Pull-Up, Standard OW Speed
 *
 * @param   pbConfigReg  A pointer to the configuration register to store the returned configuration register
 *
 * @return  An error code relating to the state of I2C communication.
 */
upErr COneWireDriver::DsConfig( byte * pbConfigReg )
{
	upErr eErr = ERR_OK;
	byte dataOut[ 2 ] =
	{ DS2482_WRITE_CONFIG, DS2482_CONFIGURATION };
	// set read pointer to config register
	I2CSendCommand( dataOut, 2, pbConfigReg, 1, FLAG_STOP );
	// Config register only reads last 4 bits
	if ( pbConfigReg[ 0 ] != ( DS2482_CONFIGURATION & 0x0F ) )
		eErr = ERR_I2C_DEVICE_ERROR;
	return eErr;
}

/**
 * @brief	This function acts as a getter for the stored one-wire
 *    		ROM information
 *
 * @param pbRomInfo A pointer to a byte to receive the ROM information
 *
 * @return  An error code relating to the state of I2C communication.
 */
upErr COneWireDriver::OwGetRomInfo( byte * pbRomInfo )
{
	upErr eErr = ERR_OK;
	byte owStatus = 0;
	byte crcCheck = 0;

	eErr = OwReset( &owStatus );
	if ( ( owStatus & DS2482_PPD ) == 0 )
		{
		eErr = ERR_OW_DATA_READ_ERROR;
		return eErr;
		}

	eErr = OwSendReadRom();

	OwReadBytes( (byte*) m_romInfo, OW_ROM_SIZE );
	// check CRC
	crcCheck = OwPerformCrc( (byte*) m_romInfo, OW_ROM_SIZE );
	if ( 0 == crcCheck )
		memcpy( (void*) pbRomInfo, (void*) m_romInfo, OW_ROM_SIZE );
	else
		eErr = ERR_OW_DATA_READ_ERROR;

	return eErr;
}

/**
 * @brief	This function sends a READ ROM command to the one-wire device.
 *
 * @return  An error code relating to the state of I2C communication.
 */
upErr COneWireDriver::OwSendReadRom()
{
	upErr eErr = ERR_OK;
	eErr = OwWriteByte( OW_READ_ROM );
	return eErr;
}

/**
 * @brief	This function sends a SKIP ROM command to the one-wire device.
 *
 * @return  An error code relating to the state of I2C communication.
 */
upErr COneWireDriver::OwSendSkipRom()
{
	upErr eErr = ERR_OK;
	eErr = OwWriteByte( OW_SKIP_ROM );
	return eErr;
}

/**
 * @brief	This function writes a single byte to the one-wire device.
 *
 * @param  bByte The byte to write to the one-wire device
 *
 * @return  An error code relating to the state of I2C communication.
 */
upErr COneWireDriver::OwWriteByte( byte bByte )
{
	upErr eErr = ERR_OK;
	byte dataOut[ 2 ];
	twobytes pollCount = 0;
	dataOut[ 0 ] = DS2482_CMD_1WWB;
	dataOut[ 1 ] = bByte;
	if ( ( eErr = I2CSendCommand( dataOut, 2, NULL, 0, FLAG_NO_STOP ) ) != ERR_OK )
		return eErr;
	if ( ( eErr = OwBusyPoll( &pollCount, FLAG_STOP ) ) != ERR_OK )
		return eErr;
	return eErr;
}

/**
 * @brief	This function writes to the one-wire scratch pad.
 *
 * @param  pTarget A pointer to the target address to write to
 * @param  pData The bytes to write to the scratchpad
 * @param  bNumBytes The number of bytes to write to the scratchpad
 *
 * @return  An error code relating to the state of I2C communication.
 */
upErr COneWireDriver::OwWriteScPad( byte * pTarget, byte * pData, byte bNumBytes )
{
	upErr eErr = ERR_OK;
	byte owStatus = 0;
	int i = 0;

	// must send reset pulse after writing scratchpad
	OwReset( &owStatus );
	if ( ( owStatus & DS2482_PPD ) == 0 )
		// One-wire has been removed
		return ERR_OW_DATA_READ_ERROR;

	OwSendSkipRom();

	OwWriteByte( OW_WRITE_SCRATCHPAD );
	OwWriteByte( pTarget[ 0 ] );
	OwWriteByte( pTarget[ 1 ] );

	for ( i = 0; i < bNumBytes; i++ )
		OwWriteByte( pData[ i ] );

	return eErr;
}

/**
 * @brief	This function reads the data in the one-wire scratch pad.
 *
 * @param  pTarget A pointer to the target address to read from
 * @param  pOffset A pointer to receive the offset value
 * @param  pData The bytes to write to the scratchpad
 * @param  bNumBytes The number of bytes to write to the scratchpad
 *
 * @return  An error code relating to the state of I2C communication.
 */
upErr COneWireDriver::OwReadScPad( byte * pTarget, byte * pOffset, byte * pData, byte bNumBytes )
{
	upErr eErr = ERR_OK;
	byte owStatus = 0;

	OwReset( &owStatus );
	OwSendSkipRom();

	OwWriteByte( OW_READ_SCRATCHPAD );
	OwReadBytes( &pTarget[ 0 ], 1 );
	OwReadBytes( &pTarget[ 1 ], 1 );
	OwReadBytes( pOffset, 1 );
	OwReadBytes( pData, bNumBytes );

	return eErr;
}

/**
 * @brief	This function copies the data from the one-wire
 *    		scratchpad to the target address.  Three verification
 *    		bytes need to be transmitted for a copy to succeed:
 *    		1) The two target address bytes and 2) The E/S byte
 *    		returned from the one-wire upon reading back the data
 *    		previously written to the scratchpad.
 *
 * @param  pTarget A pointer to the target address to copy to
 * @param  bOffset The offset byte (E/S byte) read during read scratchpad stage
 *
 * @return  An error code relating to the state of I2C communication.
 */
upErr COneWireDriver::OwCopyScPad( byte * pTarget, byte bOffset )
{
	upErr eErr = ERR_OK;
	byte owStatus = 0;

	if( ( eErr = OwReset( &owStatus ) ) != ERR_OK )
		return eErr;

	// Check the presence bit
	if ( ( owStatus & DS2482_PPD ) == 0 )
		return ERR_OW_DATA_READ_ERROR;

	eErr = OwSendSkipRom();

	if ( ( eErr = OwWriteByte( OW_COPY_SCRATCHPAD ) ) != ERR_OK )
		return eErr;
	if ( ( eErr = OwWriteByte( pTarget[ 0 ] ) ) != ERR_OK )
		return eErr;
	if ( ( eErr = OwWriteByte( pTarget[ 1 ] ) ) != ERR_OK )
		return eErr;
	if( ( eErr = OwWriteByte( bOffset ) ) != ERR_OK )
		return eErr;

	return eErr;
}


/**
 * @brief	This function reads a pre-determined amount of bytes
 *    		from the one-wire device.  The read pointer is set
 *    		to the READ DATA register on the DS2482 and one
 *    		byte is read at a time.
 *
 * @param   pbReadBytes A pointer to return the read bytes to
 * @param   iNumBytes The number of bytes to read
 *
 * @return  An error code relating to the state of I2C communication.
 */
upErr COneWireDriver::OwReadBytes( byte * pbReadBytes, int iNumBytes )
{
	byte dataOut[ 2 ];
	upErr eErr = ERR_OK;
	twobytes pollCount = 0;
	int i = 0;

	for ( i = 0; i < iNumBytes; i++ )
		{
		dataOut[ 0 ] = DS2482_CMD_1WRB;
		LOG_ERROR_RETURN( eErr = I2CSendCommand( dataOut, 1, NULL, 0, FLAG_NO_STOP ) )

		if ( ERR_OK != eErr )
			return eErr;

		LOG_ERROR_RETURN( eErr = OwBusyPoll( &pollCount, FLAG_NO_STOP ) )

		if ( ERR_OK != eErr )
			return eErr;

		dataOut[ 0 ] = DS2482_SET_RDPTR;
		dataOut[ 1 ] = DS2482_PTR_RD_DATA;
		LOG_ERROR_RETURN( eErr = I2CSendCommand( dataOut, 2, &pbReadBytes[ i ], 1, FLAG_STOP ) )
		}
	return eErr;
}

/**
 * @brief	This function sends a one-wire reset command to the DS2482.
 *
 * @param 	pbOwStatus A pointer to a byte to receive the status of the reset operation
 *
 * @return An error code relating to the state of I2C communication.
 */
upErr COneWireDriver::OwReset( byte * pbOwStatus )
{
	upErr eErr = ERR_OK;
	twobytes pollCount = 0;
	byte dataOut[ 1 ];
	dataOut[ 0 ] = DS2482_CMD_1WRST;

	LOG_ERROR_RETURN( eErr = I2CSendCommand( dataOut, 1, NULL, 0, FLAG_NO_STOP ) )

	if ( ERR_OK != eErr )
		return eErr;

	LOG_ERROR_RETURN( eErr = OwBusyPoll( &pollCount, FLAG_STOP ) )


	if ( ERR_OK != eErr )
		return eErr;

	eErr = I2CSendCommand( NULL, 0, pbOwStatus, 1, FLAG_STOP );

	MsDelay( 1 );
	return eErr;
}

/**
 * @brief	This function checks for the presence of the one-wire
 *
 * @param  bPresence Returns a true or false depending on presence of one-wire
 *
 * @return  An error code relating to the state of I2C communication.
 */
upErr COneWireDriver::OwPresenceCheck( bool_c * bPresence )
{
	upErr eErr = ERR_OK;
	byte owStatus = 0;
	eErr = OwReset( &owStatus );
	if ( ( owStatus & DS2482_PPD ) == 0)
		*bPresence = false;
	else
		*bPresence = true;
	return eErr;
}

/**
 * @brief	This function performs a CRC check by XORng the bits
 *   		together from a byte array LSb first.
 *
 * @param   pBytes The array of bytes to perform the CRC on
 * @param   numBytes The number of bytes in the pBytes array
 *
 * @return  The result of the CRC calculation
 */
byte COneWireDriver::OwPerformCrc( byte * pBytes, byte numBytes )
{
	int i = 0;
	int j = 0;
	byte tempByte = 0;
	for ( i = 0; i < numBytes; i++ )
		for ( j = 0; j < 8; j++ )
			{
			tempByte ^= ( ( pBytes[ i ] & CrcMask( j ) ) >> j );
			}
	return tempByte;
}


/*
 * @brief	This function readies a new one-wire by bypassing the ROM read
 *  		state normally necessary to make successful reads/writes to the
 *  		device
 *
 * @return SkipRom command status as upErr
 */
upErr COneWireDriver::ReadyNewOneWire()
{
	upErr eErr = ERR_OK;
	byte owStatus = 0;

	OwReset( &owStatus );
	eErr = OwSendSkipRom();

	return eErr;
}

/**
 * @file	SpiDriver.cpp
 * @par		Package: UGMcu
 * @par		Project: Union Generator
 * @par		Copyright (c) 2017, Uptake Medical Technology, Inc.
 * @author	Glen Sep 15, 2010 Created file
 * @brief	This file contains the code as CSpiDriver for reading the SPI bus. There is no need to
 * 			write over the SPI bus
 */

// Include Files
#include "UGMcuDefinitions.h"
#include "SpiDriver.h"


// External Public Data

// File Scope Macros and Constants

// File Scope Type Definitions (Enums, Structs & Classes)

// File Scope Data
bool_c CSpiDriver::m_bInitialized = false;

/**
 * m_bSpiChipSelect maps upSpiChips to value used on pins so CPLD will assert chip select to single SPI slave
 */
byte CSpiDriver::m_bSpiChipSelect[ SPI_CHIP_CNT ] =
{ 0, 1, 2, 7, 3, 6, 5, 4 };

// File Scope Functions

// Public Functions

/**
 * @brief	This is the constructor for CSpiDriver.
 *
 * It has no function besides being the constructor.
 *
 */
CSpiDriver::CSpiDriver()
{
}

/**
 * @brief	This method initializes the SPI port on the MCU.
 *
 * Initialization is done once. The static variable m_bInitialized is set to false at program
 * load. If this is false then it is set to true and the ports are initialized. PORTB has
 * the
 *
 * @return   Status as upErr
 */
upErr CSpiDriver::InitializeSpi()
{
	if ( m_bInitialized == false )
		{

		m_bInitialized = true;

		/* Set SS, MOSI and SCK output, all others input */

		/* Enable SPI, Master, set clock rate fck/16 */

		// Four low lines in port F for slave select

		}

	return ERR_OK;
}

/**
 * @brief	This method reads the SPI bus. It asserts the chip select and reads the number of bytes requested
 *
 * @param   eChip Gives the enumeration for the target chip
 * @param   iBytes Count of bytes to read
 * @param	byteBuf Pointer to caller-allocated buffer to receive the bytes
 *
 * @return   Status as upErr
 */
upErr CSpiDriver::ReadSpi( upSpiChips eChip, int iBytes, byte *byteBuf )
{
#define LOOP_MAX	(0xFFF0)

	twobytes tbLoop = 0;
	int i;
	upErr err;

	err = AssertChipSelect( eChip );
	for ( i = 0; i < iBytes && ERR_OK == err; i++ )
		{
		/* Start transmission */
		/* Wait for transmission complete */

		//for ( tbLoop = 0; tbLoop < LOOP_MAX && !( SPSR & ( 1 << SPIF ) ); tbLoop++ )
			;
//		byteBuf[ i ] = SPDR;
		if ( tbLoop >= LOOP_MAX )
			{
			err = ERR_INTERNAL_FAULT;
			}
		}
	AssertChipSelect( SPI_NO_CHIP );
	return err;
}

// Private Class Functions

upErr CSpiDriver::AssertChipSelect( upSpiChips eChip )
{
	if ( eChip >= SPI_CHIP_CNT )
		return ERR_ENUM_OUT_OF_RANGE;


	return ERR_OK;
}


/**
  ******************************************************************************
  * File Name          : main.h
  * Description        : This file contains the common defines of the application
  ******************************************************************************
  *
  * Copyright (c) 2017 STMicroelectronics International N.V. 
  * All rights reserved.
  *
  * Redistribution and use in source and binary forms, with or without 
  * modification, are permitted, provided that the following conditions are met:
  *
  * 1. Redistribution of source code must retain the above copyright notice, 
  *    this list of conditions and the following disclaimer.
  * 2. Redistributions in binary form must reproduce the above copyright notice,
  *    this list of conditions and the following disclaimer in the documentation
  *    and/or other materials provided with the distribution.
  * 3. Neither the name of STMicroelectronics nor the names of other 
  *    contributors to this software may be used to endorse or promote products 
  *    derived from this software without specific written permission.
  * 4. This software, including modifications and/or derivative works of this 
  *    software, must execute solely and exclusively on microcontroller or
  *    microprocessor devices manufactured by or for STMicroelectronics.
  * 5. Redistribution and use of this software other than as permitted under 
  *    this license is void and will automatically terminate your rights under 
  *    this license. 
  *
  * THIS SOFTWARE IS PROVIDED BY STMICROELECTRONICS AND CONTRIBUTORS "AS IS" 
  * AND ANY EXPRESS, IMPLIED OR STATUTORY WARRANTIES, INCLUDING, BUT NOT 
  * LIMITED TO, THE IMPLIED WARRANTIES OF MERCHANTABILITY, FITNESS FOR A 
  * PARTICULAR PURPOSE AND NON-INFRINGEMENT OF THIRD PARTY INTELLECTUAL PROPERTY
  * RIGHTS ARE DISCLAIMED TO THE FULLEST EXTENT PERMITTED BY LAW. IN NO EVENT 
  * SHALL STMICROELECTRONICS OR CONTRIBUTORS BE LIABLE FOR ANY DIRECT, INDIRECT,
  * INCIDENTAL, SPECIAL, EXEMPLARY, OR CONSEQUENTIAL DAMAGES (INCLUDING, BUT NOT
  * LIMITED TO, PROCUREMENT OF SUBSTITUTE GOODS OR SERVICES; LOSS OF USE, DATA, 
  * OR PROFITS; OR BUSINESS INTERRUPTION) HOWEVER CAUSED AND ON ANY THEORY OF 
  * LIABILITY, WHETHER IN CONTRACT, STRICT LIABILITY, OR TORT (INCLUDING 
  * NEGLIGENCE OR OTHERWISE) ARISING IN ANY WAY OUT OF THE USE OF THIS SOFTWARE,
  * EVEN IF ADVISED OF THE POSSIBILITY OF SUCH DAMAGE.
  *
  ******************************************************************************
  */
/* Define to prevent recursive inclusion -------------------------------------*/
#ifndef __MAIN_H
#define __MAIN_H
  /* Includes ------------------------------------------------------------------*/

/* USER CODE BEGIN Includes */

/* USER CODE END Includes */

/* Private define ------------------------------------------------------------*/

#define One_wire_Pin GPIO_PIN_8
#define One_wire_GPIO_Port GPIOB
#define CAN_Slave_RX_Pin GPIO_PIN_5
#define CAN_Slave_RX_GPIO_Port GPIOB
#define CAN_Master_TX_Pin GPIO_PIN_9
#define CAN_Master_TX_GPIO_Port GPIOB
#define One_Wire_Pin GPIO_PIN_7
#define One_Wire_GPIO_Port GPIOB
#define Heater_Enable_2_Relays_Pin GPIO_PIN_13
#define Heater_Enable_2_Relays_GPIO_Port GPIOJ
#define Heater_Enable_1_PS_Enable_Pin GPIO_PIN_12
#define Heater_Enable_1_PS_Enable_GPIO_Port GPIOJ
#define Spare_GPIO6_Pin GPIO_PIN_6
#define Spare_GPIO6_GPIO_Port GPIOD
#define CAN_Master_RX_Pin GPIO_PIN_0
#define CAN_Master_RX_GPIO_Port GPIOD
#define Spare_UART_Pin GPIO_PIN_11
#define Spare_UART_GPIO_Port GPIOC
#define Spare_UARTC10_Pin GPIO_PIN_10
#define Spare_UARTC10_GPIO_Port GPIOC
#define USB_A_Host_Pin GPIO_PIN_12
#define USB_A_Host_GPIO_Port GPIOA
#define MCU_Rev_4_Pin GPIO_PIN_4
#define MCU_Rev_4_GPIO_Port GPIOI
#define Power_Good_Pin GPIO_PIN_14
#define Power_Good_GPIO_Port GPIOJ
#define Spare_GPIO5_Pin GPIO_PIN_5
#define Spare_GPIO5_GPIO_Port GPIOD
#define Spare_GPIO3_Pin GPIO_PIN_3
#define Spare_GPIO3_GPIO_Port GPIOD
#define MCU_Rev_3_Pin GPIO_PIN_3
#define MCU_Rev_3_GPIO_Port GPIOI
#define MCU_Rev_2_Pin GPIO_PIN_2
#define MCU_Rev_2_GPIO_Port GPIOI
#define USB_A_HostA11_Pin GPIO_PIN_11
#define USB_A_HostA11_GPIO_Port GPIOA
#define MCU_Rev_5_Pin GPIO_PIN_5
#define MCU_Rev_5_GPIO_Port GPIOI
#define MCU_Rev_7_Pin GPIO_PIN_7
#define MCU_Rev_7_GPIO_Port GPIOI
#define MCU_Rev_6_Pin GPIO_PIN_6
#define MCU_Rev_6_GPIO_Port GPIOI
#define TC_1_Alert_Pin GPIO_PIN_4
#define TC_1_Alert_GPIO_Port GPIOK
#define TC_2_Alert_Pin GPIO_PIN_3
#define TC_2_Alert_GPIO_Port GPIOK
#define AC_Fail_Pin GPIO_PIN_15
#define AC_Fail_GPIO_Port GPIOJ
#define Spare_GPIO4_Pin GPIO_PIN_4
#define Spare_GPIO4_GPIO_Port GPIOD
#define MCU_Rev_1_Pin GPIO_PIN_1
#define MCU_Rev_1_GPIO_Port GPIOI
#define Spare_GPIO2_Pin GPIO_PIN_10
#define Spare_GPIO2_GPIO_Port GPIOA
#define Motor_BUSY_Pin GPIO_PIN_13
#define Motor_BUSY_GPIO_Port GPIOH
#define Motor_SW_Pin GPIO_PIN_14
#define Motor_SW_GPIO_Port GPIOH
#define MCU_Rev_0_Pin GPIO_PIN_0
#define MCU_Rev_0_GPIO_Port GPIOI
#define Spare_GPIO1_Pin GPIO_PIN_9
#define Spare_GPIO1_GPIO_Port GPIOA
#define TC_1_Pin GPIO_PIN_9
#define TC_1_GPIO_Port GPIOC
#define TC_1A8_Pin GPIO_PIN_8
#define TC_1A8_GPIO_Port GPIOA
#define Watchdog_Pin GPIO_PIN_8
#define Watchdog_GPIO_Port GPIOC
#define Debug_Uart_RX_Pin GPIO_PIN_7
#define Debug_Uart_RX_GPIO_Port GPIOC
#define Debug_Uart_TX_Pin GPIO_PIN_6
#define Debug_Uart_TX_GPIO_Port GPIOC
#define One_wire_Enable_Pin GPIO_PIN_4
#define One_wire_Enable_GPIO_Port GPIOF
#define Motor_Cntl_Pin GPIO_PIN_5
#define Motor_Cntl_GPIO_Port GPIOH
#define Debug_LED8_Pin GPIO_PIN_7
#define Debug_LED8_GPIO_Port GPIOG
#define Debug_LED7_Pin GPIO_PIN_6
#define Debug_LED7_GPIO_Port GPIOG
#define LED8_Pin GPIO_PIN_15
#define LED8_GPIO_Port GPIOD
#define CAN_Slave_TX_Pin GPIO_PIN_13
#define CAN_Slave_TX_GPIO_Port GPIOB
#define LED3_Pin GPIO_PIN_10
#define LED3_GPIO_Port GPIOD
#define LED7_Pin GPIO_PIN_14
#define LED7_GPIO_Port GPIOD
#define LED2_Pin GPIO_PIN_9
#define LED2_GPIO_Port GPIOD
#define LED1_Pin GPIO_PIN_8
#define LED1_GPIO_Port GPIOD
#define Monitor_24V_Pin GPIO_PIN_0
#define Monitor_24V_GPIO_Port GPIOC
#define Trigger_Pin GPIO_PIN_1
#define Trigger_GPIO_Port GPIOC
#define Debug_LED2_Pin GPIO_PIN_1
#define Debug_LED2_GPIO_Port GPIOG
#define LED5_Pin GPIO_PIN_12
#define LED5_GPIO_Port GPIOD
#define LED6_Pin GPIO_PIN_13
#define LED6_GPIO_Port GPIOD
#define Debug_LED4_Pin GPIO_PIN_3
#define Debug_LED4_GPIO_Port GPIOG
#define Debug_LED3_Pin GPIO_PIN_2
#define Debug_LED3_GPIO_Port GPIOG
#define Motor_STBY_Pin GPIO_PIN_12
#define Motor_STBY_GPIO_Port GPIOH
#define Heater_Current_Pin GPIO_PIN_1
#define Heater_Current_GPIO_Port GPIOA
#define Heater_Setpoint_Pin GPIO_PIN_4
#define Heater_Setpoint_GPIO_Port GPIOA
#define Debug_LED1_Pin GPIO_PIN_0
#define Debug_LED1_GPIO_Port GPIOG
#define LED4_Pin GPIO_PIN_11
#define LED4_GPIO_Port GPIOD
#define Debug_LED6_Pin GPIO_PIN_5
#define Debug_LED6_GPIO_Port GPIOG
#define Debug_LED5_Pin GPIO_PIN_4
#define Debug_LED5_GPIO_Port GPIOG
#define Motor_CntlH7_Pin GPIO_PIN_7
#define Motor_CntlH7_GPIO_Port GPIOH
#define Motor_Flag_Pin GPIO_PIN_11
#define Motor_Flag_GPIO_Port GPIOH
#define Heater_Voltage_Pin GPIO_PIN_2
#define Heater_Voltage_GPIO_Port GPIOA
#define Spare_DAC_Pin GPIO_PIN_5
#define Spare_DAC_GPIO_Port GPIOA
#define Syringe_SW_Pin GPIO_PIN_2
#define Syringe_SW_GPIO_Port GPIOJ
#define Motor_CntlF11_Pin GPIO_PIN_11
#define Motor_CntlF11_GPIO_Port GPIOF
#define Spare_SPI_Pin GPIO_PIN_11
#define Spare_SPI_GPIO_Port GPIOE
#define Spare_SPIE14_Pin GPIO_PIN_14
#define Spare_SPIE14_GPIO_Port GPIOE
#define TC_2_Pin GPIO_PIN_10
#define TC_2_GPIO_Port GPIOB
#define Motor_CntlH6_Pin GPIO_PIN_6
#define Motor_CntlH6_GPIO_Port GPIOH
#define Motor_Clock_Pin GPIO_PIN_10
#define Motor_Clock_GPIO_Port GPIOH
#define Temp_Sensor_Pin GPIO_PIN_3
#define Temp_Sensor_GPIO_Port GPIOA
#define Spare_ADC_Pin GPIO_PIN_7
#define Spare_ADC_GPIO_Port GPIOA
#define OTG_USB_B_Device_OverCurrent_Pin GPIO_PIN_1
#define OTG_USB_B_Device_OverCurrent_GPIO_Port GPIOB
#define OTG_USB_B_Device_PowerSwitchOn_Pin GPIO_PIN_0
#define OTG_USB_B_Device_PowerSwitchOn_GPIO_Port GPIOB
#define Extend_SW_Pin GPIO_PIN_0
#define Extend_SW_GPIO_Port GPIOJ
#define Retract_SW_Pin GPIO_PIN_1
#define Retract_SW_GPIO_Port GPIOJ
#define Spare_SPIE12_Pin GPIO_PIN_12
#define Spare_SPIE12_GPIO_Port GPIOE
#define Spare_SPIE13_Pin GPIO_PIN_13
#define Spare_SPIE13_GPIO_Port GPIOE
#define TC_2B11_Pin GPIO_PIN_11
#define TC_2B11_GPIO_Port GPIOB
#define USB_B_Device_Pin GPIO_PIN_14
#define USB_B_Device_GPIO_Port GPIOB
#define USB_B_DeviceB15_Pin GPIO_PIN_15
#define USB_B_DeviceB15_GPIO_Port GPIOB

/* USER CODE BEGIN Private defines */

/* USER CODE END Private defines */

/**
  * @}
  */ 

/**
  * @}
*/ 

#endif /* __MAIN_H */
/************************ (C) COPYRIGHT STMicroelectronics *****END OF FILE****/

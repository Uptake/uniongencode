/**
 * @file	PIDControl.cpp
 * @par		Package: UGMcu
 * @par		Project: Union Generator
 * @par		Copyright (c) 2017, Uptake Medical Technology, Inc.
 * @author	Glen Nov 24, 2010 Created file
 * @brief	This file contains the definitions of the class CPIDControl
 */

// Include Files
#include "PIDControl.h"

// External Public Data

// File Scope Macros and Constants

// File Scope Type Definitions (Enums, Structs & Classes)

// File Scope Data

// File Scope Functions

// Public Functions

/**
 * @brief	This is the constructor for CPIDControl.
 *
 * 			All member variables are set to zero or false. This includes the coefficient structure.
 */
CPIDControl::CPIDControl()
{
	m_PIDCoeff.m_Di = 0;
	m_PIDCoeff.m_Kc = 0;
	m_PIDCoeff.m_Kd = 0;
	m_PIDCoeff.m_Ki = 0;
	m_PIDCoeff.m_Kp = 0;
	m_PIDCoeff.m_offset = 0;
	m_PIDCoeff.m_slope = 0;
	m_PIDCoeff.m_upMax = 0;
	m_PIDCoeff.m_dnMax = 0;
	m_setpoint = 0;
	m_errIntegral = 0;
	m_lastErr = 0;
	m_lastCntl = 0;
	m_newCoeff = false;
}

/**
 * @brief	This method clears the integer total and sets the m_newCoeff flag. It's purpose
 * 			is to allow the calculation to be executed without preceding information.
 *
 * @return   Status as upErr
 */
upErr CPIDControl::ResetCalc()
{
	m_newCoeff = true;
	m_errIntegral = 0;
	m_lastCntl = 0;
	return ERR_OK;
}

/**
 * @brief	This method computes the PID output including applying the linear mapping to
 * 			a control signal
 *
 * @param	newFeedback The most recent feedback value as an int
 * @param	tickDelta The time in ticks since the last computation as a twobyte
 * @param 	pNewControl A pointer to an int to receive the new control signal
 *
 * @return   Status as upErr
 */
upErr CPIDControl::ComputePID( int newFeedback, twobytes tickDelta, int *pNewControl )
{
	long int iErr;
	long int iErrDeriv;
	long int liControl;
	int iCntl;

	// Compute error and multiple into factored number
	iErr = ( m_setpoint - newFeedback );

	// Compute derivative if not first time after coefficient change or reset
	if ( m_newCoeff )
		{
		iErrDeriv = 0;
		m_errIntegral = 0;
		m_newCoeff = false;
		liControl = m_PIDCoeff.m_Kc * m_PIDCoeff.m_slope;
		liControl = liControl / ( PID_INT_FACTOR ) + m_PIDCoeff.m_offset;
		m_lastCntl = (int) liControl;
		}
	else
		{
		iErrDeriv = ( iErr - m_lastErr ) / (int) tickDelta;
		}

	m_lastErr = iErr;

	// Add to integral term. Pull out the PID_INT_FACTOR as Di includes it again
	m_errIntegral = ( m_errIntegral * m_PIDCoeff.m_Di ) / PID_INT_FACTOR + iErr * tickDelta;

	// Build up control value
	liControl = m_PIDCoeff.m_Kc; // Start with the constant value
	liControl += iErr * m_PIDCoeff.m_Kp; // Add the proportional term
	liControl += m_errIntegral * m_PIDCoeff.m_Ki; // Add the integral term
	liControl += iErrDeriv * m_PIDCoeff.m_Kd; // Add derivative term

	// Clamp iControl to range zero to one
	if ( liControl > PID_INT_FACTOR )
		liControl = PID_INT_FACTOR;
	else if ( liControl < 0 )
		liControl = 0;

	// Scale using m*x+b
	liControl *= m_PIDCoeff.m_slope;
	liControl = liControl / ( PID_INT_FACTOR ) + m_PIDCoeff.m_offset;
	iCntl = liControl;

	// Clamp to allowed change rate
	if ( iCntl > m_lastCntl + m_PIDCoeff.m_upMax )
		iCntl = m_lastCntl + m_PIDCoeff.m_upMax;
	else if ( iCntl < m_lastCntl - m_PIDCoeff.m_dnMax )
		iCntl = m_lastCntl - m_PIDCoeff.m_dnMax;

	// Assign to output parameter
	m_lastCntl = iCntl;
	*pNewControl = iCntl;

	return ERR_OK;
}

/**
 * @brief	This method sets the parameters used in calculating the PID control signal. Setting
 * 			new coefficients has the stored history cleared except for the last power output.
 *
 * @param	pPIDCoeff A pointer to the new coefficients in a SPIDCoefficients structure
 *
 * @return   Status as upErr
 */
upErr CPIDControl::SetPIDCoefficients( SPIDCoefficients *pPIDCoeff )
{
	m_PIDCoeff = *pPIDCoeff;
	m_newCoeff = true;
	return ERR_OK;
}

/**
 * @brief	This method gets the parameters used in calculating the PID control signal.
 *
 * @param	pPIDCoeff A pointer to return the coefficients in a SPIDCoefficients structure
 *
 * @return   Status as upErr
 */
upErr CPIDControl::GetPIDCoefficients( SPIDCoefficients *pPIDCoeff )
{
	*pPIDCoeff = m_PIDCoeff;
	return ERR_OK;
}

/**
 * @brief	This method sets the new setpoint. This has the integral and derivative re-started
 *
 * @param	setpoint The new target feedback value and an int
 *
 * @return   Status as upErr
 */upErr CPIDControl::SetSetpoint( int setpoint )
{
	if ( m_setpoint != setpoint )
		{
		m_setpoint = setpoint;
		m_newCoeff = true;
		}
	return ERR_OK;
}

/**
 * @brief	This method returns the latest setpoint value
 *
 * @param	pSetpoint A pointer to an int to receive the setpoint
 *
 * @return   Status as upErr
 */
 upErr CPIDControl::GetSetpoint( int *pSetpoint )
{
	*pSetpoint = m_setpoint;
	return ERR_OK;
}

// Private Class Functions


/**
 * @file	Watchdog.h
 * @par		Package: UGMcu
 * @par		Project: Union Generator
 * @par		Copyright (c) 2017, Uptake Medical Technology, Inc.
 * @author	Glen Sep 15, 2010 Created file
 * @brief	This file declares the class and its interface for watchdog timer control, CWatchdog
 */

#ifndef WATCHDOG_H_
#define WATCHDOG_H_

// Include Files
#include "UGMcuDefinitions.h"
#include "UGError.h"
#include "UGMcuTypes.h"
#include "RndRobinHelper.h"
#include "MemMapIntfc.h"

// Public Macros and Constants

// Public Type Definitions (Enums, Structs & Classes)

/**
 * @class	CWatchdog
 * @brief	Kicks the watchdog timers if the system is functioning properly
 *
 *			This configures the MCU watchdog and the CPLD watchdog. The Execute() function kicks
 *			both watchdogs. The ResetAfterWait() method uses the MCU watchdog to have the processor reset.
 *
 * Virtual Functions Overridden:
 * 	- Execute
 * 	- InitializeHardware
 *
 * External Data Members: None @n@n
 *
 */
class CWatchdog: private CMemMapIntfc, private CRndRobinHelper
{
public:
	CWatchdog();
	upErr Execute();
	upErr InitializeHardware();
	void ResetAfterWait( byte bWDTCode );
};

// Inline Functions (follows class definition)


#endif /* WATCHDOG_H_ */

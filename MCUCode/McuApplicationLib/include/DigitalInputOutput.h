/**
 * @file       DigitalInputOutput.h
 * @par        Package: UGMcu
 * @par        Project: Union Generator
 * @par        Copyright (c) 2017, Uptake Medical Technology, Inc.
 * @author     Glen Sep 15, 2010 Created file
 * @brief      This file contains the class definition for the digital I/O control CDigitalInputOutput and the
 * 			   enumerations for the inputs and outputs
 */

#ifndef CDIGITALINPUTOUTPUT_H_
#define CDIGITALINPUTOUTPUT_H_

// Include Files
#include "UGMcuDefinitions.h"
#include "UGMcuTypes.h"
#include "MemMapIntfc.h"


// Public Macros and Constants

#define DIG_IN_UNUSED_0_BIT 	(0x0)	//!< Unused
#define DIG_IN_UNUSED_1_BIT 	(0x1)	//!< Unused
#define DIG_IN_SYR_PRESENT_BIT 		(0x2)	//!< Bit index for Syringe presence switch
#define DIG_IN_MOTOR_RETRACT_BIT	(0x3)	//!< Bit index for Rod Retraction switch
#define DIG_IN_MOTOR_EXTEND_BIT 	(0x4)	//!< Bit index for Rod Extension switch
#define DIG_IN_STANDBY_BIT			(0x5)	//!< Unused
#define DIG_IN_TRIGGER_BIT 			(0x6)	//!< Bit index for Delivery Device trigger
#define DIG_IN_RELAY_HOT_BIT 		(0x7)	//!< Unused
class CDigitalInputOutput;
extern CDigitalInputOutput * g_pDigitalInputOutput; //!< Globally accessible pointer to single object of CDigitalInputOutput

// Public Type Definitions (Enums, Structs & Classes)

/**
 * This enumeration defines all of the logical digital inputs into the Union Generator.
 *
 * These values are used in the application code to separate bit values from logical uses
 *
 */
typedef enum upDigitalInputsEnum
{
	DIG_IN_TRIGGER = 0,
	DIG_IN_SYR_PRESENT,
	DIG_IN_MOTOR_RETRACT,
	DIG_IN_MOTOR_EXTEND,
	DIG_IN_UNUSED_0,
	DIG_IN_UNUSED_1,
	DIG_IN_STANDBY_UNUSED,
	DIG_IN_CNT
} upDigitalInputs;

/**
 * This enumeration defines the visual signal to show using the LEDs
 *
 */
typedef enum upLEDStateEnum
{
	LED_OFF,
	LED_POWER,
	LED_PRIME,
	LED_READY,
	LED_TREAT,
	LED_TERMINATE,
	LED_WARN,
	LED_ENUM_COUNT
} upLEDState;

/**
 * This enumeration defines all of the logical digital outputs in and from the Union Generator.
 *
 * This is both a bit-wise view of the two digital output registers and the values are used in
 * the application layers.
 *
 */
typedef enum upDigitalOutputsEnum
{
	DIG_OUT_UNUSED_1 = 0, //!< Unused
	DIG_OUT_RF_ENABLE = 1, //!< Switch to enable the RF generator interlock
	DIG_OUT_RF_AC_POWER, //!< Controls voltage applied to AC relays to turn RF generator ON/OFF
	DIG_OUT_UNUSED_2, //!< Unused and is a spare 12V output
	DIG_OUT_UNUSED_3, //!< Unused and is a spare 12V signal
	DIG_OUT_CONTROL_CNT, //!< Control signal are on a different register in the CPLD and are handled separately
	DIG_OUT_LED0 = DIG_OUT_CONTROL_CNT, //!< First LED (0) is same index as count of control bits
	DIG_OUT_LED1, //!< LED1
	DIG_OUT_LED2, //!< LED2
	DIG_OUT_LED3, //!< LED3
	DIG_OUT_LED4, //!< LED4
	DIG_OUT_LED5, //!< LED5
	DIG_OUT_LED6, //!< LED6
	DIG_OUT_LED7, //!< LED7
	DIG_OUT_CNT, // The following values are re-used so don't add to the total
	DIG_OUT_GREEN_LED1 = DIG_OUT_LED0, //!< Remap logical LED0 to green 1
	DIG_OUT_GREEN_LED2 = DIG_OUT_LED1, //!< Remap logical LED1 to green 2
	DIG_OUT_AMBER_LED1 = DIG_OUT_LED2, //!< Remap logical LED2 to amber 1
	DIG_OUT_AMBER_LED2 = DIG_OUT_LED3, //!< Remap logical LED3 to amber 2
	DIG_OUT_BLUE_LED1 = DIG_OUT_LED4, //!< Remap logical LED4 to blue 1
	DIG_OUT_BLUE_LED2 = DIG_OUT_LED6, //!< Remap logical LED5 to blue 2
	DIG_OUT_BLUE_LED3 = DIG_OUT_LED5, //!< Remap logical LED6 to blue 3
	DIG_OUT_BLUE_LED4 = DIG_OUT_LED7, //!< Remap logical LED7 to blue 4
	DIG_OUT_FINAL_MAX
} upDigitalOutputs;

/**
 * @class   CDigitalInputOutput
 * @brief	This class reads the digital inputs and write the digital outputs.@n
 *
 * 			It is tied to the round-robin scheduling
 * 			with inputs accessed during the ReadHardware call and digital outputs set during the WriteHardware call. Outputs will
 * 			only be set if the state has changed. This can be forced by toggling the value (E.g., ON-OFF-ON) before the
 * 			Write is executed.
 *
 * Virtual Functions Overridden:
 *	- ReadFromHardware
 *	- WriteToHardware
 *	- InitializeHardware
 *
 * External Data Members:
 *  - g_pDigitalInputOutput
 *
 */
class CDigitalInputOutput: private CMemMapIntfc
{
private:
	bool_c m_digIn[ DIG_IN_CNT ]; //!< The values of the digital inputs
	bool_c m_digInInvert[ DIG_IN_CNT ]; //!< Flags on which inputs are inverted from 1 being ON
	bool_c m_digInBit[ DIG_IN_CNT ]; //!< This maps the bits in the bytes from the CPLD to logical values
	bool_c m_digOut[ DIG_OUT_CNT ]; //!< States for outputs. 1 => ON
	byte m_digSysLEDOut; //!< Next set out LED settings
	byte m_digSysLEDLastOut; //!< Last set out LED settings
	byte m_lastDigIn; //!< Last read inputs
	byte m_digInInvertMask; //!< Input inversion in a byte mask
	upLEDState m_eLedState; //!< Enumeration value for used LEDs
	upErr ApplyLEDState();

public:
	CDigitalInputOutput();
	upErr ReadFromHardware();
	upErr WriteToHardware();
	upErr InitializeHardware();
	upErr SetOutput( upDigitalOutputs eOut, bool_c bOn );
	upErr GetOutput( upDigitalOutputs eOut, bool_c * pbOn );
	upErr GetInput( upDigitalInputs eIn, bool_c * pbOn );
	upErr GetAllOutput( twobytes * ptOn );
	upErr GetAllInput( byte * pbOn );
	upErr SetLEDState( upLEDState eState );
	upErr GetLEDState( upLEDState * peState );
};

#endif /*CDIGITALINPUTOUTPUT_H_*/

/**
 * @file	TimeBase.h
 * @par		Package: UGMcu
 * @par		Project: Union Generator
 * @par		Copyright (c) 2017, Uptake Medical Technology, Inc.
 * @author	Glen Sep 23, 2010 Created file
 * @brief	This file contains the class definition for CTimeBase
 */

#ifndef TIMEBASE_H_
#define TIMEBASE_H_

// Include Files
#include "UGMcuDefinitions.h"
#include "UGError.h"
#include "UGMcuTypes.h"
#include "RndRobinHelper.h"
#include "TimerAccess.h"

// Public Macros and Constants

// Public Type Definitions (Enums, Structs & Classes)

/**
 * @class	CTimeBase
 * @brief	This class provides a fine-grained timer for real-time operations and a second
 * 			counter for larger increments and for wall-clock time.
 *
 * 			The wall clock will be seeded with a time stamp from
 * 			the SBC and is updated only when ReadFromHardware() is called. Thus the seconds
 * 			resolution is limited to the round-robin execution cycle time.
 *
 * Virtual Functions Overridden:
 *	- InitializeHardware
 *	- ReadFromHardware
 *
 * External Data Members: None @n@n
 *
 */
class CTimeBase: public CTimerAccess, protected CRndRobinHelper
{
private:
	tick m_lastSecondTick;
	upErr ClearTick();

protected:
	upErr SetTick( tick tTick );

public:
	CTimeBase();
	upErr InitializeHardware();
	upErr ReadFromHardware();
};

// Inline Functions (follows class definition)

#endif /* TIMEBASE_H_ */

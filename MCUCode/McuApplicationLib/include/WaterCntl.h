/**
 * @file	WaterCntl.h
 * @par		Package: UGMcu
 * @par		Project: Union Generator
 * @par		Copyright (c) 2017, Uptake Medical Technology, Inc.
 * @author	Glen Sep 16, 2010 Created file
 * @brief	This file declares the class for water flow control, CWaterCntl
 */

#ifndef WATERCNTL_H_
#define WATERCNTL_H_

// Include Files
#include "UGMcuDefinitions.h"
#include "StepperVelocity.h"
#include "StepperPosition.h"
#include "RndRobinHelper.h"

// Public Macros and Constants
#define STEPS_PER_REV				(20000)

// Public Type Definitions (Enums, Structs & Classes)
class CWaterCntl;
extern CWaterCntl * g_pWaterCntl; //!< Globally accessible pointer to single object of CWaterCntl

/**
 * @class	CWaterCntl
 * @brief	This class combines CStepperVelocity and CStepperPosition to produce an easy-to-use
 * 			set of methods for controlling and monitoring the stepper motor.
 *
 * 			It will set the velocity using a water flow rate taking into account the syringe
 * 			cross-sectional area, lead screw pitch, stepper motor steps and controller sub-step
 * 			attributes. It also provides for retracting the lead screw so the retracted switch is
 * 			turned ON and to extend the lead screw.
 *
 * Virtual Functions Overridden:
 * 	ReadFromHardware
 *  - ReadFromHardware
 * 	- WriteToHardware
 * 	- InitializeHardware
 *
 * External Data Members:
 *  - g_pWaterCntl
 *
 */
class CWaterCntl: public CStepperVelocity, public CStepperPosition, private CRndRobinHelper
{
private:
	long int m_rate;
	bool_c m_bRetractRod;

public:
	CWaterCntl();
	upErr ReadFromHardware();
	upErr WriteToHardware();
	upErr InitializeHardware();
	upErr GetFlowRateUlPerMin( long int * pilRate );
	upErr SetFlowRateUlPerMin( int iRate );
	upErr SetFlowRateMlPerMin( int iRate );
	upErr StartRetractingPumpRod();
	upErr ExtendPumpRod( bool_c * pbExtended );
	upErr PerformRodRetraction( bool_c * pbRetracted );
	bool_c GetRetracted( bool_c * pbRetracted );
};

// Inline Functions (follows class definition)

/**
 * @brief	This method gets the last set flow rate.
 *
 * The rate is returned in microliters per minute
 *
 * @param	pilRate A pointer to a long int to receive the water flow rate in uL per minute
 *
 * @return   Status as upErr
 */
inline upErr CWaterCntl::GetFlowRateUlPerMin( long int *pilRate )
{
	*pilRate = m_rate;
	return ERR_OK;
}

#endif /* WATERCNTL_H_ */

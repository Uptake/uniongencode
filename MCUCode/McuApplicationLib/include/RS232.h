/**
 * @file	RS232.h
 * @par		Package: UGMcu
 * @par		Project: Union Generator
 * @par		Copyright (c) 2017, Uptake Medical Technology, Inc.
 * @author	Glen Sep 15, 2010 Created file
 * @brief	This file declares the RS232 interface, CRS232
 */

#ifndef RS232_H_
#define RS232_H_

// Include Files
#include "UGError.h"
#include "UGMcuTypes.h"
#include "TimerAccess.h"
#include "Messages.h"

// Public Macros and Constants
#define SBC_SEND_BYTE_MAX       (MAX_PACKET_SIZE + 1) //!< The largest SBC message plus the overhead plus one
#define SBC_RECV_BYTE_MAX       (MAX_PACKET_SIZE + 1) //!< The largest SBC message plus the overhead plus one

#define RF_SEND_BYTE_MAX 		(12)	//!< The largest message including overhead sent to RDO plus two
#define RF_RECV_BYTE_MAX 		(20)	//!< The largest message including overhead received from iTherm plus two

// Public Type Definitions (Enums, Structs & Classes)

/**
 * This selects which of the two USART ports is being used
 *
 */
typedef enum upRS232PortEnum
{
	RS232_USART0 = 0,
	RS232_USART1,
	RS232_CNT
} upRS232Port;

#define RS232_SBC		RS232_USART1	//!< USART1 is for the Single Board COmputer
#define RS232_RF_GEN	RS232_USART0	//!< USART2 is for the iTherm communication

/**
 * This defines the available BAUD rates for the ports.
 *
 * There are more rates that the USARTS can produce but these are the
 * set we are interested in using or trying
 *
 */
typedef enum upRS232BaudEnum
{
	RS232_BAUD_9600 = 0,
	RS232_BAUD_38400,
	RS232_BAUD_115200,
	RS232_BAUD_CNT
} upRS232Baud;

/**
 * This defines the available byte parities for the ports
 *
 */
typedef enum upRS232ParityEnum
{
	RS232_PARITY_NONE = 0,
	RS232_PARITY_EVEN,
	RS232_PARITY_ODD,
	RS232_PARITY_CNT
} upRS232Parity;

/**
 * @class	CRS232
 * @brief	This class configures and control the two serial port USARTs.
 *
 * 			The reading and writing is done using interrupts to allow
 * 			program execution to continue while reading and writing is done. The application configures
 * 			the ports and then simply reads and writes the byte buffers. There is no queue of buffers.
 * 			Multiple write operations may be done which will add to the outgoing buffer up to the maximum
 * 			size of the internal write buffer. The read interrupts will collect bytes up to the maximum
 * 			buffer size. It is the responsibility of the application to break out multiple packets if they
 * 			are present in the read data.
 *
 * Virtual Functions Overridden: None @n@n
 *
 * External Data Members: None @n@n
 *
 */
class CRS232: protected CTimerAccess
{
private:
	long int m_baudRate[ RS232_BAUD_CNT ];	//!< Array of BAUD rate bit setting configurations

public:
	CRS232();

protected:
	upErr InitializePort( upRS232Port ePort, upRS232Baud eBaud, upRS232Parity eParity, byte bBytes, byte bStop );
	upErr ReadPort( upRS232Port ePort, twobytes iGet, byte * pBytes, twobytes * ptbRead, byte bWait );
	upErr WritePort( upRS232Port ePort, twobytes tbWrite, byte * pBytes );
	upErr GetPendingSendCount( upRS232Port ePort, twobytes * ptbRead );
	upErr FlushReadBuffer( upRS232Port ePort );
};

// Inline Functions (follows class definition)

#endif /* RS232_H_ */

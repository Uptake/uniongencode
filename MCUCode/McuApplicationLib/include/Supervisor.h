/**
 * @file	Supervisor.h
 * @par		Package: UGMcu
 * @par		Project: Union Generator
 * @par		Copyright (c) 2017, Uptake Medical Technology, Inc.
 * @author	Glen Sep 15, 2010 Created file
 * @brief	This file declares the class and interface for the system supervisor, CSupervisor
 */

#ifndef CSUPERVISOR_H_
#define CSUPERVISOR_H_

// Include Files
#include "UGMcuDefinitions.h"
#include "UGError.h"
#include "UGMcuEnum.h"
#include "UGMcuTypes.h"
#include "RndRobinHelper.h"
#include "TimerAccess.h"
#include "OneWireMemory.h"
#include "HandpiecePrime.h"
#include "CatheterPrime.h"
#include "AblationControl.h"

// Public Macros and Constants

// Public Type Definitions (Enums, Structs & Classes)
/**
 * @class	CSupervisor
 * @brief	The decision making portion of the Union Generator real-time control software.
 *
 * This class manages the operation of the generator. It determines what operation should be performed and
 * controls the actions. The class has three entry points: constructor, Execute() and SetOverrunFlag(). The
 * last method is used to indicate a failure in the round-robin scheme of CRndRobinScheduler. The constructor
 * sets default value to internal variables. The Execute() method is called from the round-robin loop and is
 * provides control over most of the Union Generator operation.
 *
 * The Execute() function is the entry point to a multi-level state machine that determines what the Union Generator will do
 * when performing the  system operations. The principal operations are to prime the delivery device and to create
 * treatments. Additional operations are self-testing, system set-up and error handling.
 *
 * The top-level state machine in CSupervisor uses the upOperState enumeration. These values are mapped in Execute() to other
 * methods or operations on a one-to-one basis.
 *
 * 	- OPER_STATE_BOOT : Immediately changed to OPER_STATE_TEST
 *
 *  - OPER_STATE_TEST : TestState()
 *  - OPER_STATE_IDLE : IdleState()
 *  - OPER_STATE_CNCTD : ConnectedState()
 *  - OPER_STATE_HAND_PRIMING : HandpiecePrimingState()
 *  - OPER_STATE_HAND_PRIMED : HandpiecePrimedState()
 *  - OPER_STATE_CATH_PRIMING : CatheterPrimingState()
 *  - OPER_STATE_SETUP : SetupState()
 *  - OPER_STATE_PENDING : PendingState()
 *  - OPER_STATE_READY : ReadyState()
 *  - OPER_STATE_PRECOND : PreconditionState()
 *  - OPER_STATE_DELIVERING : DeliveringState()
 *  - OPER_STATE_RELEASE : ReleaseState()
 *  - OPER_STATE_ERROR : ErrorState()
 *
 * The system always starts in the BOOT state. The first actionable state is TEST. If the test passes the system enters the IDLE state.
 * If the self-test fails, or there are other errors that may be generator faults identified, the system enters the TERMINATE state.
 * There is no transition from TERMINATE and the user needs to power-cycle the generator to start again.
 *
 * The IDLE state loERR_OKs for the
 * user to connect a Delivery Device. Once it is connected the state transitions to CONNECTED which waits for the priming operation to
 * start. When the user keys the Delivery Device in CONNECTED the system enters PRIMING. When the system is primed the system enters PENDING_READY.
 *
 *
 * The PENDING_READY state is used when the system is primed and connected but other criteria would prevent a treatment
 * from starting. When all criteria to start a treatment are met the system is in READY. When the Delivery Device trigger is closed the
 * system transitions from READY to TREATMENT. In the TREATMENT state an individual ablation is performed. When the treatment completes
 * the system returns to PENDING_READY.
 *
 * If the user disconnects any identifiable components the system returns to the IDLE state. If the
 * same delivery device is fully re-connected the system will move to CONNECTED, PENDING_READY or READY as appropriate.
 *
 * Each state's operations are described in the discussion for the methods. The method SetOperState() is used to transition
 * between states and performs any actions that are required for the "edge" of the transition.
 *
 * Virtual Functions Overridden:
 *	- Execute
 *
 * External Data Members: None @n@n
 *
 */
class CSupervisor: protected CTimerAccess, protected CRndRobinHelper
{
private:
	/**
	 * This identifies the state of the Delivery Device (DD)
	 */
	typedef enum EnumDDeviceField
	{
		DD_STATE_DISCONNECTED = 0,
		DD_STATE_ATTACHED = 1,
		DD_STATE_DISABLED = 2,
		DD_STATE_CNT
	} upDDeviceState;

private:
	CHandpiecePrime m_handpiecePrime;	//!< CHandpiecePrime as composed member variable
	CCatheterPrime m_catheterPrime;	//!< CHandpiecePrime as composed member variable
	CAblationControl m_ablationCntl;	//!< CAblationControl as composed member variable
	upDDeviceState m_eDDeviceState;
	upOperState m_operState;
	upPendingReady m_ePendingSubState;
	bool_c m_bHandpiecePrimed;
	bool_c m_bCatheterPrimed;
	bool_c m_lastTrig;
	bool_c m_bVersionsVerified;
	bool_c m_bRoundRobinOverrun;
#ifdef ALLOW_EMC_TREATMENT_CODE
	twobytes m_emcTestCycle;
#endif
	byte m_lastOwRomId[ OW_ROM_SIZE ];
	byte m_bProgress;
	tick m_lastEnd;
	tick m_tStateStart;

	bool_c CoilIdleAllowed();

protected:
	void SetInitValues();
	upErr SetOperatingConditions();
	upErr WaitingForTrigger( bool_c * pbTrig );
	upErr CheckHandpiece();
	upErr SetOperState( upOperState eOperState );
	upOperState GetOperState();

	// Thirteen methods for thirteen states.
	// "Boot" does not get a method
	upErr TestState();
	upErr IdleState();
	upErr ConnectedState();
	upErr HandpiecePrimingState();
	upErr HandpiecePrimed();
	upErr CatheterPrimingState();
	upErr SetupState();
	upErr PendingState();
	upErr ReadyState();
	upErr PreconditionState();
	upErr DeliveringState();
	upErr ReleaseState();
	upErr ErrorState();

	upErr SetHandpiecePrimedFlag( bool_c bPrimed );
	upErr SetCatheterPrimedFlag( bool_c bPrimed );
	upErr DisableDeliveryDevice( upErr eErr, bool_c bPlayTone );
	upErr PerformCoilIdle();
	upErr CheckSyringe();
	upErr CheckCriticalMonitoring();

public:
	CSupervisor();
	upErr Execute();
	void SetOverrunFlag();

};

// Inline Functions (follows class definition)

#endif /* CSUPERVISOR_H_ */

/**
 * @file	TreatmentRecord.cpp
 * @par		Package: UGMcu
 * @par		Project: Union Generator
 * @par		Copyright (c) 2017, Uptake Medical Technology, Inc.
 * @author	Chris Keller Apr 2, 2011 Created file
 * @brief	This file contains the interface description for the CTreatmentRecord class.
 */

#ifndef TREATMENT_RECORD_H_
#define TREATMENT_RECORD_H_

// Include Files
#include "UGMcuDefinitions.h"
#include "UGError.h"
#include "UGMcuTypes.h"
#include "UGMcuEnum.h"

// Public Macros and Constants

// Public Type Definitions (Enums, Structs & Classes)
class CTreatmentRecord;
extern CTreatmentRecord * g_pTreatmentRecord; //!< Globally accessible pointer to single object of CTreatmentRecord

/**
 * @class	CTreatmentRecord
 * @brief	This class handles storage of current treatment information.
 *
 * 		The information stored in this class is used to keep the
 * 		one-wire memory and the SBC updated with stored treatment information.
 * 		After a treatment is performed, the therapy record will be updated and
 * 		a member index will be incremented to indicate that new record information
 * 		is available for other classes to view.
 *
 * Virtual Functions Overridden: None @n@n
 *
 * External Data Members:
 *  - g_pTreatmentRecord
 *
 */
class CTreatmentRecord
{
private:
	twobytes m_treatIdx;
	twobytes m_fullTreatmentCount;
	twobytes m_initiatedTreatmentCount;
	twobytes m_lastTreatmentTime;
	upDeliveryStatus m_lastTreatmentStatus;
	unsignedfourbytes m_therapyTime;
	twobytes m_entryID;

public:
	CTreatmentRecord();
	void StartNewRecord();
	byte GetEntry();
	void NewEntry();
	upErr UpdateTreatmentInfoMessage();
	twobytes GetLastTreatmentTime();
	void SetLastTreatmentTime( twobytes iTime );
	twobytes GetRecordTreatmentCount();
	void SetRecordTreatmentCount( twobytes tbCount );
	twobytes GetInitiatedTreatmentCount();
	void SetInitiatedTreatmentCount( twobytes tbCount );
	upDeliveryStatus GetLastTreatmentStatus();
	void SetLastTreatmentStatus( upDeliveryStatus eStatus );
	unsignedfourbytes GetTherapyTime();
	void SetTherapyTime( unsignedfourbytes ulTime );
	upErr UpdateTreatmentRecord( upDeliveryStatus eStatus, twobytes tbVaporOnTicks );
	void ClearStoredValues();

};

// Inline Functions (follows class definition)


#endif /* TREATMENT_RECORD_H_ */

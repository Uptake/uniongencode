/**
 * @file	McuInfo.h
 * @par		Package: UGMcu
 * @par		Project: Union Generator
 * @par		Copyright (c) 2017, Uptake Medical Technology, Inc.
 * @author	Glen Aug 18, 2012 Created file
 * @brief	This file contains the declarations for CMcuInfo
 */

#ifndef MCUINFO_H_
#define MCUINFO_H_

// Include Files

#include "MemMapIntfc.h"

// Public Macros and Constants

// Public Type Definitions (Enums, Structs & Classes)

/**
 * The versions of MCU board
 *
 */
typedef enum upMcuBoardRevisionEnum
{
	MCU_BOARD_C = 0x00, //<! Assemblies prior to G
	MCU_BOARD_D = 0x01, //<! Assemblies F and later
	MCU_BOARD_UNKNOWN = 0xFF
//<! Flag the board as not identified
} upMcuBoardRevision;

/**
 * The versions of MCU board
 * The lower nibble is the board and the upper nibble is the assembly
 */
typedef enum upMcuAssemblyRevisionEnum
{
	MCU_ASSEMBLY_F = 0x00, //<! F is used for all assemblies of the "C" board
	MCU_ASSEMBLY_G = 0x11, //<! G is the first assembly to use the "D" board
	MCU_ASSEMBLY_UNKNOWN = 0xFF
//<! Flag the assembly as not identified
} upMcuAssemblyRevision;

/**
 * @class	CMcuInfo
 * @brief	This class provides the revisions of the MCU board (PCB) and the assembly.
 *
 * The assembly may be changed without changing the board or PCB. This allows selections based on
 * the functionality of the board as a whole regardless of assembly versions which may include part
 * changes that need specific support. The revision data is read from the CPLD. It is encoded into the
 * board as zero-Ohm resistors mounted to the board. These are changed for each assembly per the BOM.
 *
 * Virtual Functions Overridden: None @n@n
 *
 * External Data Members: None @n@n
 *
 */
class CMcuInfo: public CMemMapIntfc
{
private:
	static upMcuAssemblyRevision m_assemblyRev;
	static upMcuBoardRevision m_boardRev;

public:
	CMcuInfo();
	upErr IdentifyRevision();
	upMcuAssemblyRevision GetAssemblyRevision();
	upMcuBoardRevision GetBoardRevision();
};

// Inline Functions (follows class definition)

/**
 * @brief	This method return the MCU board assembly enumeration for the board in use
 *
 * @return   Assembly revision as upMcuAssemblyRevision
 */
inline upMcuAssemblyRevision CMcuInfo::GetAssemblyRevision()
{
	return m_assemblyRev;
}

/**
 * @brief	This method return the MCU printed circuit board enumeration for the board in use
 *
 * @return   Assembly revision as upMcuBoardRevision
 */
inline upMcuBoardRevision CMcuInfo::GetBoardRevision()
{
	return m_boardRev;
}

#endif /* MCUINFO_H_ */

/**
 * @file	RndRobinScheduler.h
 * @par		Package: UGMcu
 * @par		Project: Union Generator
 * @par		Copyright (c) 2017, Uptake Medical Technology, Inc.
 * @author	Glen Sep 17, 2010 Created file
 * @brief	This file declares the Round-robin scheduler class, CRndRobinScheduler
 */

#ifndef RNDROBINSCHEDULER_H_
#define RNDROBINSCHEDULER_H_

// Include Files
#include "UGError.h"
#include "TimerAccess.h"
#include "RndRobinHelper.h"

// Public Macros and Constants
#define PASS_CYCLE_CNT	(8)
#define LOOP_TIMING_LOG_PERIOD	(1000 * PASS_CYCLE_CNT)

// Public Type Definitions (Enums, Structs & Classes)

/**
 * @class	CRndRobinScheduler
 * @brief	This class controls the scheduling of the tasks presented by the top-level objects in the software.
 *
 * 			All of the top-level objects for the MCU software are instantiated a file- scoped objects in the RndRobinScheduler.cpp file.
 * 			Those objects present one or more of the methods:
 * 			- InitializeHardware
 * 			- ReadFromHardware
 * 			- WriteToHardware
 * 			- Execute
 *
 * 			Those methods are defined in CRndRobinHelper. The top-level classes inherit that class and override the methods needed. This class
 * 			has two prinicapal methods: Initialize() and Run(). Inside Initialize() all of the needed InitializeHardware() calls are made on the
 * 			top-level objects. The Run() method will then be executed. This is an infinite loop. It calls the top-level objects in this general
 * 			pattern:
 * 			- *.ReadFromHardware
 * 			- Supervisor.Execute()
 * 			- *.WriteToHardware
 *
 * 			This has new data accessed prior to the CSupervisor class making decisions about system operation. The WriteToHardware calls are then made
 * 			to control the hardware with new values. There are exceptions for the CEeprom and COneWireMemory.
 *
 * 			The main loop is executed periodically at 50ms cadence. Each loop starts 50ms apart however much time is used in the preceding cycle.
 * 			The class tracks the execution time of the loops for max and min times. These are output as logs every 8000 cycles.
 *
 * The general operation is brERR_OKen into these steps:
 * 				- read hardware
 * 				- perform supervisory actions
 * 				- write to hardware
 * 				- perform communication
 * 				- Kick watchdogs
 *
 * Virtual Functions Overridden: None @n@n
 *
 * External Data Members: None @n@n
 *
 */
class CRndRobinScheduler:  private CTimerAccess, private CRndRobinHelper
{

private:
	twobytes m_maxLoopTime[ PASS_CYCLE_CNT ];
	twobytes m_minLoopTime[ PASS_CYCLE_CNT ];
	twobytes m_loopTimePassCnt;
	upErr PerformLoopTiming( twobytes tbTickDiff, byte bPassIndex );

public:
	CRndRobinScheduler();
	upErr Initialize();
	upErr Run();
};

// Inline Functions (follows class definition)


#endif /* RNDROBINSCHEDULER_H_ */

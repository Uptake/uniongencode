/**
 * @file	SoundCntl.h
 * @par		Package: UGMcu
 * @par		Project: Union Generator
 * @par		Copyright (c) 2017, Uptake Medical Technology, Inc.
 * @author	Chris Keller Sep 15, 2011 Created file
 * @brief	This file contains the declaration for CSoundCntl
 */

#ifndef SOUNDCNTL_H_
#define SOUNDCNTL_H_

// Include Files
#include "UGMcuDefinitions.h"
#include "UGMcuTypes.h"
#include "UGError.h"
#include "TimerAccess.h"

// Public Macros and Constants
class CSoundCntl;
extern CSoundCntl * g_pSoundCntl; //!< Globally accessible pointer to single object of CSoundCntl

/**
 * @class	CSoundCntl
 * @brief	This class provides methods to enable and control the frequency of the critical tone.
 *
 * A call to the public method OutputTone() will start the tone
 * generation the next time the Execute method is called.
 *
 * Virtual Functions Overridden:
 *	- InitializeHardware
 *	- Execute
 *
 * External Data Members:
 *  - g_pSoundCntl
 */
class CSoundCntl: private CTimerAccess
{

public:
	/**
	 * This enum defines the different sounds the MCU can make
	 */
	typedef enum
	{
		SOUND_OFF,
		SOUND_TEST,
		SOUND_CRITICAL,
		SOUND_INVALIDATE1,
		SOUND_INVALIDATE2,
		SOUND_CNT
	} upSoundState;

private:
	bool_c m_bEnabled;
	twobytes m_iFreq;
	twobytes m_tbToneDuration; //<! The duration of the tone in ticks
	bool_c m_bStartSound;
	tick m_tStart;
	bool_c m_toneDelivered;
	upSoundState m_eSoundState;

public:
	CSoundCntl();
	upErr Execute();
	upErr InitializeHardware();
	upErr OutputToneOnce();
	upErr OutputTestTone();
	upErr OutputInvalidateTone();
	upSoundState GetSoundState();

private:
	upErr EnableClock( bool_c bEnable );
	upErr SetFrequency( twobytes iFreq );
	upErr SoundEnable();
	upErr SoundDisable();
};

// Inline Functions (follows class definition)


#endif /* SOUNDCNTL_H_ */

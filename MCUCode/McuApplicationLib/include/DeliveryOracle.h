/**
 * @file	DeliveryOracle.h
 * @par		Package: 
 * @par		Project: Flow-Based Generator
 * @par		Copyright (c) 2017, Uptake Medical Technology, Inc.
 * @author	Glen Jan 12, 2016 Created file
 * @brief	This file contains TODO
 */

#ifndef INCLUDE_DELIVERYORACLE_H_
#define INCLUDE_DELIVERYORACLE_H_

// Include Files
#include "UGMcuDefinitions.h"
#include "UGMcuTypes.h"
#include "UGError.h"
#include "Messages.h"

// Public Type Definitions (Enums, Structs & Classes)
class CDeliveryOracle;
extern CDeliveryOracle * g_pDelOracle;

// Type definitions 

// Forward declarations

// Struct and Union declarations

/**
 * @class	CDeliveryOracle
 * @brief	This class manages the use of the time setting for emphysema ablations.
 *
 * 			This class checks that the supplied time is acceptable and converts the time to individual steps
 * 			of an ablation. Each step includes a duration, power and flow rate. The effective ablation is when
 * 			the steps are all run, in order, for the given periods.
 *
 */

class CDeliveryOracle
{
	STreatmentSelection m_lastSelection;

public:
	CDeliveryOracle();
	upErr NewSelection( STreatmentSelection &refSelection );
	upErr CheckSelection( STreatmentSelection &refSelection );
	upErr GetLastSelection( STreatmentSelection * pSelection );
	upErr GetTotalTime( twobytes * ptbTime );
};

// Inline Functions (follows class definition)

#endif /* INCLUDE_DELIVERYORACLE_H_ */

/**
 * @file	StepperPosition.h
 * @par		Package: UGMcu
 * @par		Project: Union Generator
 * @par		Copyright (c) 2017, Uptake Medical Technology, Inc.
 * @author	Glen Sep 16, 2010 Created file
 * @brief	This file defines the class for tracking the stepper motor position using an optical encoder, CStepperPosition
 */

#ifndef STEPPERPOSITION_H_
#define STEPPERPOSITION_H_

// Include Files
#include "UGMcuDefinitions.h"
#include "MemMapIntfc.h"
#include "UGError.h"
#include "UGMcuTypes.h"

// Public Macros and Constants
#define SAVED_POSITION_CNT	(2)

// Public Type Definitions (Enums, Structs & Classes)

/**
 * @class	CStepperPosition
 * @brief	Reads the count of relative motion for the optical encoder and determines the position and
 * 			the velocity of the motor
 *
 * This module is used for testing purposes only.
 *
 * Virtual Functions Overridden: None @n@n
 *
 * External Data Members: None @n@n
 *
 */
class CStepperPosition: private CMemMapIntfc
{
private:
	long int m_lastPosition;
	int m_deltaPosition[ SAVED_POSITION_CNT ];
	twobytes m_deltaTime[ SAVED_POSITION_CNT ];
	long int m_position;
	int m_velocity;

protected:
	CStepperPosition();
	upErr ReadStepperPosition( twobytes tbTickDiff );

public:
	upErr ResetStepperPosition();
	upErr GetStepperPosition( long int * pliPosition, int * piVelocity );
};

// Inline Functions (follows class definition)

#endif /* STEPPERPOSITION_H_ */

/**
 * @file	HandpiecePrime.h
 * @par		Package: UGMcu
 * @par		Project: Union Generator
 * @par		Copyright (c) 2017, Uptake Medical Technology, Inc.
 * @author	Glen Oct 26, 2010 Created file
 * @brief	This file contains the declaration for CHandpiecePrime
 */

#ifndef HANDPIECEPRIME_H_
#define HANDPIECEPRIME_H_

// Include Files
#include "UGMcuDefinitions.h"
#include "UGError.h"
#include "UGMcuEnum.h"
#include "TimerAccess.h"
#include "TreatmentRecord.h"

// Public Macros and Constants

// Public Type Definitions (Enums, Structs & Classes)

/**
 * @class	CHandpiecePrime
 * @brief	This class performs priming of the handpiece also know as the delivery device
 *
 *	Priming is done in a multi-step process that can be re-entered if the user chooses to halt it.
 *	The first stage is to pump water without applying power. The water pumping is done at three rates
 *	in series. When the thrid flow is completed a vapor prime is done using RF power. This stage is monitored
 *	for temperatures that indicate a good prime and a good delivery device.
 *	@n
 *	If the user halts
 *	the operation by opening the trigger the priming is resumed where it left off. During water pumping it
 *	re-starts just where it stopped. If the user halts in the vapor priming then the entire vapor
 *	prime is repeated. Repeating a vapor prime is preceded by a short pulse of water to
 *	address possible bubbles or incompletely filled syringe.
 *	@n
 * Virtual Functions Overridden: None @n@n
 *
 * External Data Members: None @n@n
 *
 */
class CHandpiecePrime: protected CTimerAccess
{
public:
	/**
	 * @enum PrimeStepEnum
	 * This enumeration defines the steps taken during the priming operation. It includes an error state
	 * to accommodate transitions resulting from an operational failure
	 */
	enum PrimeStepEnum
	{
		PRIME_ERROR,			/*!< Halt and report the error */
		PRIME_START,			/*!< Start the priming operation */
		PRIME_MONITOR_FLOW, 	/*!< Monitor pumping phase */
		PRIME_CONTROLLED_STOP, 	/*!< End priming in a controlled way*/
		PRIME_SUCCESS,			/*!< Set flags and report success */
		PRIME_FAILED,			/*!< Set flags and report failure */
		PRIME_STEP_COUNT		/*!< Counter to check good enum values in code */
	};

	CHandpiecePrime();

private:
	tick m_tStartTick;
	tick m_tRunTicks;
	tick m_tPriorPumpTicks;
	tick m_tNewPumpTicks;
	PrimeStepEnum m_state;
	tick m_tTotalPumpTicks;
	upErr SetFlowRate(tick tElapsed);
	upErr StartPriming();
	upErr MonitorFlow();

public:
	upErr PerformPrime();
	PrimeStepEnum GetState() const;
	upErr InitializePrime();
	upErr GetProgress( byte * pbProgress );

};

// Inline Functions (follows class definition)

#endif /* HANDPIECEPRIME_H_ */

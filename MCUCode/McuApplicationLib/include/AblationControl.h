/**
 * @file	AblationControl.h
 * @par		Package: UGMcu
 * @par		Project: Union Generator
 * @par		Copyright (c) 2017, Uptake Medical Technology, Inc.
 * @author	Glen Oct 26, 2010 Created file
 * @brief	This file contains the declarations for CAblationControl
 */

#ifndef ABLATIONCONTROL_H_
#define ABLATIONCONTROL_H_

// Include Files
#include "UGMcuDefinitions.h"
#include "UGError.h"
#include "UGMcuEnum.h"
#include "TimerAccess.h"

// Public Macros and Constants

// Public Type Definitions (Enums, Structs & Classes)

/**
 * @brief These are the ablation states used in CAblationControl
 */
enum upAblationStateEnum
{
	ABLATE_STATE_START,		//!< The initial state where RF power and/or water flow is initiated
	ABLATE_STATE_ABLATE,	//!< The "middle" state of a treatment where the ablation is performed
	ABLATE_STATE_STOP,		//!< The ending state of the ablation where cooling and/or drying is done
	ABLATE_STATE_COMPLETE,	//!< The "COMPLETE" state has no actions and is a flag that the ablation has finished
	ABLATE_STATE_ERROR,		//!< The ERROR state is entered if something has gone wrong.
	ABLATE_STATE_COUNT
};

/**
 * @class	CAblationControl
 * @brief	This class provides the overall control of a single treatment ablation.
 *
 * The ablation is done in several steps of start, middle, stop, update and complete. These are identified using the
 * enumeration upAblationStateEnum which also includes an additional state for errors. @n
 * In the Start step, the RF generator is turned on and water flow is started. The start time is marked to measure elapsed time
 * and to be used in treatment records@n
 * In the Middle step the water is pumped while RF energy is applied to the coil. The temperatures are
 * monitored to ensure the treatment is effective and safe@n
 * During the Stop step the RF power and water flow are turned off. @n
 * Once the ablation is stopped, the Update is performed to inform the UI and 1-wire interfaces with the
 * results of the ablation. This could be a full treatment, a partial treatment due to trigger release or a partial
 * treatment due to an error being detected@n
 * If there is an error found then the Error action is taken which turns off RF and water flow immediately. The Error
 * operation includes an update of ablation activities. @n
 * During the ablation the water flow and temperature are monitored for correct operation of the system.
 * This is done using facilities from CCommonOperations and CGeneratorMonitor classes @n
 * All times, powers and flow rates are based on the therapy code read from the Delivery Device that is connected.
 * The therapy information is obtained from the CTreatmentInformation class using the global handle g_pTherapyInfo. @n
 * This class inherits CTimerAccess to access the tick and second timers and to perform math on them. @n

 *  @n@n
 * Virtual Functions Overridden: None @n@n
 * External Data Members: None @n@n
 */

class CAblationControl: protected CTimerAccess
{

public:
	CAblationControl();
	upErr PerformTreatment();
	upErr StartAblation();
	upAblationStateEnum GetAblationState();
	upErr GetProgress( byte * pbProgress );
	upErr SetLastAblationEndTime();

private:
	upAblationStateEnum m_eAblState; 	//!< The current state of the ablation
	upDeliveryStatus m_eTreatStatus;	//!< Set TRUE when a full treatment has been performed and FALSE otherwise
	tick m_startTick;					//!< The tick clock count for when the ablation is started
	tick m_lastEndTick;					//!< The tick clock count for when the previous ablation ended
	tick m_tbAblationRunTicks;			//!< The running count of ticks during the ablation since it started

	upErr AblationMiddle();

};

// Inline Functions (follows class definition)

#endif /* ABLATIONCONTROL_H_ */

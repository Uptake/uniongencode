/**
 * @file	SbcIntfc.h
 * @par		Package: UGMcu
 * @par		Project: Union Generator
 * @par		Copyright (c) 2017, Uptake Medical Technology, Inc.
 * @author	Glen Sep 17, 2010 Created file
 * @brief	This file declares the application level interface for MCU-to-SBC communication
 */

#ifndef SBCINTFC_H_
#define SBCINTFC_H_

// Include Files
#include "UGMcuTypes.h"
#include "UGError.h"
#include "UGMcuEnum.h"
#include "SbcSerialIntfc.h"
#include "Messages.h"
#include "LogMessage.h"
#include "TreatmentInformation.h"
#include "MemMapIntfc.h"

// Public Macros and Constants
class CSbcIntfc;
extern CSbcIntfc * g_pSbcIntfc; //!< Globally accessible pointer to single object of CSbcIntfc

// Public Type Definitions (Enums, Structs & Classes)

/**
 * An enum that indicates which Q to pop from
 */
typedef enum upPopQueueEnum
{
	POP_Q_NONE = 0,
	POP_Q_LOG = 1,
	POP_Q_USER = 2,
	POP_Q_CNT
} upPopQueue;

/**
 * Commands that the user may initiate from the SBC
 */
typedef enum upSbcCmdEnum
{
	SBC_CMD_UNDEFINED = 0x00,
	SBC_CMD_RELEASE_SYRINGE = 0x01,
	SBC_CMD_PRIMED = 0x02, /// TODO Fix cmds
	SBC_CMD_NOT_PRIMED = 0x03, /// TODO Fix cmds
	SBC_CMD_SETUP = 0x04,
	SBC_CMD_ENTER = SBC_CMD_SETUP, // TODO fix cmds
	SBC_CMD_UNUSED = 0x05,
	SBC_CMD_BUTTON1_UP = 0x08,
	SBC_CMD_BUTTON1_DOWN = 0x09,
	SBC_CMD_BUTTON2_UP = 0x0A,
	SBC_CMD_BUTTON2_DOWN = 0x0B,
	SBC_CMD_BUTTON3_UP = 0x0C,
	SBC_CMD_BUTTON3_DOWN = 0x0D,
	SBC_CMD_DELIVERY_UNLOCK = 0x0E,
	SBC_CMD_BUTTON4_DOWN = 0x0F,
	SBC_CMD_RADIO1_ON = 0x10,
	SBC_CMD_RADIO1_OFF = 0x11,
	SBC_CMD_RADIO2_ON = 0x12,
	SBC_CMD_RADIO2_OFF = 0x13,
	SBC_CMD_RADIO3_ON = 0x14,
	SBC_CMD_RADIO3_OFF = 0x15,
	SBC_CMD_RADIO4_ON = 0x16,
	SBC_CMD_RADIO4_OFF = 0x17,
	SBC_CMD_CNT
} upSbcCmd;

/**
 * @class	CSbcIntfc
 * @brief	This class provides the application-level interface for interacting with the SBC.
 *
 * 			This class is independent of the transport layer and protocol being used. It provides access to
 * 			the data that will be passed between the processors.
 *
 * Virtual Functions Overridden:
 * 	- InitializeHardware
 * 	- ReadFromHardware
 * 	- WriteToHardware
 *
 * External Data Members:
 * - g_pSbcIntfc
 */

class CSbcIntfc: public CSbcSerialIntfc, private CMemMapIntfc
{
private:
	upSbcState m_lastSbcState;
	tick m_timerSbcState;
	bool_c m_timerSbcStateFlag;
	upMessageTypes m_lastMsgRecvd;
	byte m_lastSbcRequest;
	byte m_lastLogSeqSent;
	byte m_lastLogAckRx;
	byte m_mcuRequest;
	byte m_logRequests;
	byte m_userMsgRequests;
	bool_c m_sbcTimeSet;
	upPopQueue m_ePopMsgOnAck;
	SUserMsg m_msgUserMsg;
	SOpconMsg m_msgOpcon;
	SSBCCond m_msgSbcState;
	SSetupMsg m_msgSetup;
	SLogMsg m_msgLog;
	SSbcCmd m_msgSbcCmd;
	SHpInfo m_msgHpInfo;
	SMcuInfo m_msgMcuInfo;
	SDeliverySteps m_msgDeliverySteps;
	STreatmentInfoMsg m_msgTreatmentInfo;
	STreatmentSelection m_msgTreatmentSelection;
	upSbcCmd m_lastSbcCmd;
	upErr RequestController( upMessageTypes * eRequestType );
	upErr UpdateLogMessage();
	void GetCustomSetup();
	upErr UpdateCustomSetup();
	upErr SendMessage( upMessageTypes eMsgType );
	upErr GetSbcRequestType( upMessageTypes * pRequest );
	upErr ReadySbcRequest( upMessageTypes eMsgType );
	upErr GetWaitingMsgType( upMessageTypes * peMsgType );
	upErr RecvMessageUpdate( upMessageTypes eMsgType );
	upErr HandleSbcCommand();
	upErr UpdateDTInfo();
	upErr UpdateUserMessage();
	upErr PopFromQIfLastMsgAckd();

protected:
	upErr SetNextSbcRequest( byte bRequest );
	upErr SetNextMcuRequest( byte bRequest );

public:
	CSbcIntfc();
	void ResetCustomSetup();
	upErr ReadFromHardware();
	upErr WriteToHardware();
	upErr SendTextOperatingConditions();
	upErr SendTextMessage( int iCnt, byte * pbBytes );
	upErr ReadSbcState( upSbcState * pSbcState );
	upErr GetMessage( void ** pMsgStruct, upMessageTypes eMsg );
	upErr InitializeHardware();
	upErr CheckForTimeout();
	upMessageTypes GetLastMsgReceived();
	upErr ResetCriticalTimer();
	upErr GetLastCmnd( upSbcCmd * peSbcCmd );
	upErr SetLastCmnd( upSbcCmd eSbcCmd );

};

// Inline Functions (follows class definition)

#endif /* SBCINTFC_H_ */

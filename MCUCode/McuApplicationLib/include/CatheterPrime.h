/**
 * @file	CatheterPrime.h
 * @par		Package: UGMcu
 * @par		Project: Union Generator
 * @par		Copyright (c) 2017, Uptake Medical Technology, Inc.
 * @author	Glen Oct 26, 2010 Created file
 * @brief	This file contains the declaration for CCatheterPrime
 */

#ifndef CATHETERPRIME_H_
#define CATHETERPRIME_H_

// Include Files
#include "UGMcuDefinitions.h"
#include "UGError.h"
#include "UGMcuEnum.h"
#include "TimerAccess.h"
#include "TreatmentRecord.h"

// Public Macros and Constants

// Public Type Definitions (Enums, Structs & Classes)

/**
 * @class	CCatheterPrime
 * @brief	This class performs priming of the Catheter.
 *
 *	Priming is done is done using a vapor prime using the longest allowed vapor delivery. This is monitored
 *	for temperatures that indicate a good delivery and a good delivery device. The priming of the catheter itself
 *	can not be judged from the generator.
 *	@n
 *	If the user halts
 *	the operation by opening the trigger then the entire vapor
 *	delivery is repeated. Repeating a vapor prime is preceded by a short pulse of water to
 *	address possible bubbles or an incompletely filled syringe.
 *	@n
 * Virtual Functions Overridden: None @n@n
 *
 * External Data Members: None @n@n
 *
 */
class CCatheterPrime: private CTimerAccess
{
public:

	/**
	 * @enum PrimeStepEnum
	 * This enumeration defines the steps taken during the priming operation. It includes an error state
	 * to accommodate transitions resulting from an operational failure
	 */
	enum PrimeStepEnum
	{
		PRIME_ERROR,			/*!< Halt and report the error */
		PRIME_VAPOR_START,		/*!< Initiate vaporization */
		PRIME_VAPOR_TEST,		/*!< Run vapor for a period of time */
		PRIME_CONTROLLED_STOP, 	/*!< End priming in a controlled way*/
		PRIME_SUCCESS,			/*!< Set flags and report success */
		PRIME_FAILED,			/*!< Set flags and report failure */
		PRIME_STEP_COUNT		/*!< Counter to check good enum values in code */
	};

	CCatheterPrime();
	upErr PerformPrime();
	PrimeStepEnum GetState() const;
	upErr InitializePrime();
	upErr GetProgress( byte * pbProgress );

private:
	tick m_tStartTick;
	tick m_tRunTicks;
	tick m_tTotalPrimeTicks;
	PrimeStepEnum m_state;
	upErr VaporTest();

};

// Inline Functions (follows class definition)

#endif /* CATHETERPRIME_H_ */

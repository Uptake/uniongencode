/**
 * @file	TimerAccess.h
 * @par		Package: UGMcu
 * @par		Project: Union Generator
 * @par		Copyright (c) 2017, Uptake Medical Technology, Inc.
 * @author	Glen Sep 23, 2010 Created file
 * @brief	This file contains the definition for class CTimerAccess
 */

#ifndef TIMERACCESS_H_
#define TIMERACCESS_H_

// Include Files
#include "UGMcuDefinitions.h"
#include "UGMcuTypes.h"
#include "UGError.h"

// Public Macros and Constants

// Public Type Definitions (Enums, Structs & Classes)

/**
 * @class	CTimerAccess
 * @brief	This class provides access to the fine-grained timer.
 *
 * 			It is separate from
 * 			CTimeBase so it can be inherited into classes that need to use the tick timer or
 * 			the second timer. This provides access to the timers and functions to get
 * 			elapsed times in seconds and ticks.
 *
 * Virtual Functions Overridden: None @n@n
 *
 * External Data Members: None @n@n
 *
 */
class CTimerAccess
{
public:
	CTimerAccess();
	upErr SetTime( time tClock );
	upErr GetTime( time * ptClock );
	upErr GetTick( tick * ptTick );
	bool_c GetTickElapsed( tick tOld, twobytes * ptbDiff );
	bool_c GetTickElapsed( tick tOld, tick * ptDiff );
	bool_c GetTickDiff( tick tNew, tick tOld, twobytes * ptbDiff );
	bool_c GetTickDiff( tick tNew, tick tOld, tick * ptbDiff );
	upErr MsDelay( twobytes tbMsDelay );
};

// Inline Functions (follows class definition)

#endif /* TIMERACCESS_H_ */

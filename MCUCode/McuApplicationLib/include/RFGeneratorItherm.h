/**
 * @file	RFGeneratorItherm.h
 * @par		Package: UGMcu
 * @par		Project: Union Generator
 * @par		Copyright (c) 2017, Uptake Medical Technology, Inc.
 * @author	Glen Jul 24, 2012 Created file
 * @brief	This file contains the definition of the code for CRFGeneratorItherm
 */

#ifndef RFGENERATORITHERM_H_
#define RFGENERATORITHERM_H_

#include "UGMcuDefinitions.h"

// Include Files
#include "RFGenerator.h"
#include "RS232.h"
#include "CalibrateLinear.h"

// Public Macros and Constants

// Public Type Definitions (Enums, Structs & Classes)
typedef CCalibrateLinear<byte, 1, twobytes> CIthermCal;	//!< Declare calibration template as a type to make Doxygen process it correctly


/**
 * @class	CRFGeneratorItherm
 * @brief	This class provides the interface for the iTherm generator.
 *
 * Virtual Functions Overridden:
 * - SendInitialCommand
 * - ReadFromHardware
 * - WriteToHardware
 * - InitializeHardware
 * - StartFreqSweep
 * - SetOutputEnable
 * - GetRfGenModel
 *
 * External Data Members: None @n@n
 *
 */
class CRFGeneratorItherm: public CIthermCal, public CRFGenerator, private CRS232
{
private:
	bool_c m_statusGetInProgress;
	bool_c m_powerSetInProgress;
	byte m_bPowerSentData[ RF_SEND_BYTE_MAX ];

	upErr SetDefaultsToGenerator();
	upErr SendCommand( byte * pbBuf, byte bCnt, byte * bSendPacket );
	upErr ReceiveCommand( byte bCnt, byte * bSendPacket );
	upErr ReceiveGetter( byte *pbGetBuf, byte bCmd, byte bGetCnt, byte bMsTimeout );
	upErr PerformSetter( byte * pbBuf, byte bCnt );
	upErr PerformGetter( byte *pbOutBuf, byte bOutCnt, byte *pbGetBuf, byte bGetCnt );
	upErr WritePowerSetpoint( twobytes bPowerSetpoint );
	upErr StartWritePowerSetpoint( twobytes bPower );
	upErr WriteMode( upRFGeneratorMode eMode );
	upErr StartStatusGetter();
	upErr ReadStatusResponse();
	upErr RunningCommunication();

public:
	CRFGeneratorItherm();

	upErr SendInitialCommand();
	upErr ReadFromHardware();
	upErr WriteToHardware();
	upErr InitializeHardware();
	upErr SetOutputEnable( bool_c bOn );
	upErr GetRfGenModel( upRFGeneratorModel * peModel );

};

// Inline Functions (follows class definition)


#endif /* RFGENERATORITHERM_H_ */

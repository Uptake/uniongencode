/**
 * @file	SbcSerialIntfc.h
 * @par		Package: UGMcu
 * @par		Project: Union Generator
 * @par		Copyright (c) 2017, Uptake Medical Technology, Inc.
 * @author	Glen Sep 17, 2010 Created file
 * @brief	This file declares the class that provides a serial communication protocol to communicate with the SBC, CSbcSerialIntfc
 */

#ifndef SBCSERIALINTFC_H_
#define SBCSERIALINTFC_H_

// Include Files
#include "UGMcuDefinitions.h"
#include "UGError.h"
#include "UGMcuTypes.h"
#include "UGMcuEnum.h"
#include "RS232.h"
#include "RndRobinHelper.h"
#include "Messages.h"

// Public Macros and Constants
#define SBC_MINOR_TIMEOUT_TICKS		( MS_TO_TICKS( 200 ) )	//!< Ticks between packets for a minor timeout event
#define SBC_CRITICAL_TIMEOUT_TICKS 	( MS_TO_TICKS( 3000 ) )	//!< Ticks between packets for a critical timeout event
#define SBC_CONNECTION_TIMEOUT ( MS_TO_TICKS( 120000 )) //!< Ticks between packets for a connection timeout event

#define LOG_TIMEOUT_INTERVAL ( 5 )
// Public Type Definitions (Enums, Structs & Classes)


/**
 * @class	CSbcSerialIntfc
 * @brief	This provides RS232 interface to the SBC. It leverages the RS232 class and support RndRobinHelper as well.
 * 			It will read and write the serial communication in sequence of the task scheduler. The ReadBuffer and WriteBuffer
 *  		methods will access cached data.
 *
 * Virtual Functions Overridden:
 * 	- InitializeHardware
 * 	- ReadFromHardware
 * 	- WriteToHardware
 *
 * External Data Members: None @n@n
 *
 */
class CSbcSerialIntfc: protected CRS232, public CRndRobinHelper
{
private:
	// A large packet (265 * 8 = 2120 bits) could
	// take up to 18.5ms at 115.2kbps
	/**
	 * @brief This struct contains a complete definition of a incoming or outgoing packet
	 */
	struct SPacketBuffer
	{
		byte m_data[ MAX_MESSAGE_SIZE ];
		byte m_sequence;
		byte m_ack;
		byte m_request;
		byte m_messageType;
		twobytes m_packetLength;
		twobytes m_payloadLength;
		twobytes m_checksum;
		bool_c m_packetComplete;
		upPacketStates m_ePacketState;
	};

	/**
	 * @brief This structure contains data on the statistics of packet transfers
	 */
	struct SPacketStats
	{
		twobytes m_packetsTx;
		twobytes m_packetsRx;
		twobytes m_packetRxErrors;
		twobytes m_nacksRx;
		twobytes m_sequenceErr;
		twobytes m_timeouts;
		twobytes m_longestLatency;
	};
	SPacketStats m_packetStats;
	SPacketBuffer m_packetBuffer;
	byte m_sendBuffer[ SBC_SEND_BYTE_MAX ];
	byte m_readBuffer[ SBC_RECV_BYTE_MAX ];
	twobytes m_sendPktLength;
	tick m_minorTimeoutTick;
	tick m_criticalTimeoutTick;
	tick m_connectionTimer;
	bool_c m_connectionTimeout;
	byte m_intervalCnt;
	byte m_lastRxSequence;
	bool_c m_lastAckd;
	byte m_lastTxSequence;
	byte m_currentRequest;
	bool_c m_waitForReplyFlag;
	bool_c m_criticalTimeout;
	byte m_sequence;
	twobytes m_checksumCompare;
	bool_c m_connectedState;
	upErr UpdatePacketBytes( twobytes tbRdSize );
	upErr UpdatePacketState( upPacketStates ePacketState );
	upErr GetPacketState( upPacketStates * pPacketState );
	upErr RetrievePacketState( upPacketStates * pPacketState  );
	upErr RetrievePayload( byte * pbMessage, byte * pSequence,
	        byte * pMsgType, byte * pRequest, twobytes * pLength, twobytes tbMaxLength );
	upErr ConstructPacket( byte * pPacket, byte bMsgType,
			byte * pbMessage, byte bRequest, twobytes tbMsgLength, twobytes * ptbPacketLength );
	upErr CheckForTimeout();
	upErr FlushPacketBuffer();
	void ResetTimers();
	void ErrorFlushAndReSynch();
	void UpdateRxPacketStats();
	void InternalInitialization();

public:
	CSbcSerialIntfc();
	upErr ReadFromHardware();
	upErr WriteToHardware();
	upErr InitializeHardware();

protected:
	upErr MessageToPacketBuffer( byte bMsgType, byte * pbMessage, twobytes tbMsgLength, byte bRequest );
	upErr PacketBufferToMessage( byte * pbMessage, twobytes tbMaxLength );
	upErr CheckWaitForReplyFlag( bool_c * pWaitFlag );
	upErr GetLastSbcRequest( byte * pbRequest );
	upErr ReturnNextMsgType( upMessageTypes * peMessageType );
	bool_c PacketReceived();
	upErr WriteBuffer( twobytes tbCnt, byte * pbBytes );
	upErr GetTimeoutFlag();
	upErr ResetCriticalTimestamp();
	bool_c GetLastAckStatus();
};

// Inline Functions (follows class definition)
/*
 * @brief	This inline method resets the packet buffer,updates
 * 			the receive error count statistic, and resets the packet
 * 			parser state to Synch when a packet receive error has
 * 			been detected.
 *
 * @return   Status as upErr
 */
inline void CSbcSerialIntfc::ErrorFlushAndReSynch()
{
		FlushPacketBuffer();
		m_packetStats.m_packetRxErrors++;
		// log it and be done
		UpdatePacketState( SYNCH );
}

/**
 * @brief	This method returns the status of the wait for reply flag
 *
 * @return   Status as upErr
 */
inline void CSbcSerialIntfc::ResetTimers()
{
	GetTick( &m_minorTimeoutTick );
	m_criticalTimeoutTick = m_minorTimeoutTick;
}

/**
 * @brief	This method updates the packet receive state
 *
 * @param	ePacketState The packet receive state to update to
 *
 * @return   Status as upErr
 */
inline upErr CSbcSerialIntfc::UpdatePacketState( upPacketStates ePacketState )
{
	upErr err = ERR_OK;
	m_packetBuffer.m_ePacketState = ePacketState;
	return err;
}

/**
 * @brief	This method updates packet stats applicable when
 * 			a packet has just been received.  This method assumes
 * 			the packet complete state has already been checked.
 *
 * @return   Status as upErr
 */
inline void CSbcSerialIntfc::UpdateRxPacketStats()
{
	if ( m_packetBuffer.m_sequence != m_lastTxSequence )
		m_packetStats.m_sequenceErr++;
	if ( !m_packetBuffer.m_ack )
		m_packetStats.m_nacksRx++;
	m_packetStats.m_packetsRx++;
}

#endif /* SBCSERIALINTFC_H_ */

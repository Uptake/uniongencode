/**
 * @file	LogMessage.h
 * @par		Package: UGMcu
 * @par		Project: Union Generator
 * @par		Copyright (c) 2017, Uptake Medical Technology, Inc.
 * @author	Glen Oct 5, 2010 Created file
 * @brief	This file contains the declarations for message logging using class CLogMessage
 */

#ifndef LOGMESSAGE_H_
#define LOGMESSAGE_H_

// Include Files
#include "UGMcuDefinitions.h"
#include "UGError.h"
#include "UGMcuTypes.h"
#include "UGMcuEnum.h"
#include "TimerAccess.h"
#include "RndRobinHelper.h"
#include "Messages.h"

 #if (((__GNUC__ * 1000) + __GNUC_MINOR__) * 1000 + __GNUC_PATCHLEVEL__) < 4006002
# ifdef __cplusplus
#  undef PROGMEM
#  define PROGMEM __attribute__(( section(".progmem.data") ))
#  undef PSTR
# endif
#endif


// Public Macros and Constants
//
#define USER_MSG_MAX_CNT 	(10)
#define FILE_STR_NAME	f_FileName

// Declare the file name in this header and force it to be in the memory segment .progman.data that will stay in FLASH
static const char FILE_STR_NAME[]  = __BASE_FILE__;
extern void LogErrorRet( upErr x, const char * sFile, int iLine );
#define LOG_ERROR_RETURN(x) {LogErrorRet(x, FILE_STR_NAME, __LINE__ );}

// Public Type Definitions (Enums, Structs & Classes)
class CLogMessage;
extern CLogMessage * g_pLogMessage; //!< Globally accessible pointer to single object of CLogMessage

/**
 * @class	CLogMessage
 * @brief	This class provides a queue for logging general messages and specific mechanisms for reporting error codes.
 *
 * 			This class manages a queue of messages used to report status and errors in software execution and generator operation.
 * 			All logs messages are time-stamped.
 * 			This class handles informational messages with stings and values in addition to error messages. It logs upErr error codes
 * 			and constructs full messages from supplied parts. The messages are added to a local circular queue and
 * 			may be sent to the SBC.  The messages will be moved from the local queue to the SBC interface, if possible,
 * 			when Execute() is called. For development use the logs may be pumped out the serial port as plain ASCII strings.
 * @n
 * 			The error codes used in the logging are used to create user logs messages. This is a separate queue of messages that
 * 			are provided to the GUI. The GUI typically turns these into pop-up messages for the user. The class filters out redundant
 * 			error codes from multiple logged messages as an error code will be passed up call chains and logged multiple times. All
 * 			upErr error codes that will be handled as user messages start at enumeration value USER_ERROR_START.
 * @n
 * 			Both message queues operate as FIFO buffers. They are expected to be off-loaded by the SBC interfaces as the messages are
 * 			uploaded for reporting and storage. If the buffers become full a failure is deemed to have occurred.
 *
 * Virtual Functions Overridden: None @n@n
 *
 * External Data Members:
 *  - g_pLogMessage
 *
 */
class CLogMessage: protected CTimerAccess, private CRndRobinHelper
{
private:
	byte m_msgHead;
	byte m_msgCnt;
	byte m_userMsgCnt;
	byte m_userMsgHead;
	char m_errMsgs[ LOG_MESSAGE_CNT ][ LOG_MESSAGE_LENGTH ];
	SUserMsg m_userMsgQueue[ USER_MSG_MAX_CNT ];

public:
	CLogMessage();
	upErr LogMessage( const char * sMsg );
	upErr LogErrorMessage( upErr eErr, const char * sMsg );
	upErr LogErrorMessage( upErr eErr, const char * sMsg, int iLine );
	upErr LogErrorMessageInt( upErr eErr, const char * sMsg, int iVal );
	upErr LogErrorMessage( upErr eErr, const char * sMsg1, const char * sMsg2 );
	upErr LogErrorMessage( upErr eErr, const char * sMsg1, const char * sMsg2, int iLine );
	upErr GetLogCount( byte *pbLogCount );
	upErr GetUserMsgCount( byte * pbUserMsgCount );
	upErr PopLogMessage();
	upErr PeekLogMessage( char * sMsg, byte * pbLen );
	void * GetLogMsgPointer();
	upErr PopUserMessage();
	upErr PeekUserMessage( SUserMsg * psMsg );
	upErr ReturnUserMsgCode( upErr eErr, upOperState eOperState );

private:
	upErr ReturnUserMsgTreating( upErr eErr );
	upErr ReturnUserMsgPriming( upErr eErr );
	upErr GetNextFreeUserMsgEntry( byte * pbEntry );
	upErr GetNextFreeLogEntry( byte * pbEntry );
	upErr GetTimeTickStr( char * pStr, byte * pbLen );
	upErr GetTimeTickErrStr( char *pStr, upErr eErr, byte *pbLen );
	bool_c CheckForDuplicates( upErr eErr );
	upErr CreateAndAddUserMsg( upErr eErr, int iValue );
	upErr AddUserMessage( upErr eErr, int iValue );
};

// Inline Functions (follows class definition)

/**
 * @brief This returns the pointer to the root of the error message buffer
 *
 * @return The pointer to the array of strings as a void pointer
 */
inline void * CLogMessage::GetLogMsgPointer()
{
	return (void*) m_errMsgs;
}

#endif /* LOGMESSAGE_H_ */

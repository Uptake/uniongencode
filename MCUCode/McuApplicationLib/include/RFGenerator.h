/**
 * @file	RFGenerator.h
 * @par		Package: UGMcu
 * @par		Project: Union Generator
 * @par		Copyright (c) 2017, Uptake Medical Technology, Inc.
 * @author	Glen Sep 15, 2010 Created file
 * @brief	This file declares the interface for the RF generator, CRFGenerator
 */

#ifndef RFGENERATOR_H_
#define RFGENERATOR_H_

// Include Files
#include "UGMcuDefinitions.h"
#include "UGMcuEnum.h"
#include "LogMessage.h"
#include "RndRobinHelper.h"

// Public Macros and Constants
#define RF_GEN_POWER_UP_CYCLE_COUNT	(MS_TO_RND_ROBIN_CYC(3000)) //!< Wait 3 seconds for generator to power up
#define RF_GEN_POWER_UP_CYCLE_MAX	(MS_TO_RND_ROBIN_CYC(4000)) //!< Wait 4 more seconds for RF generator to respond
#define RF_GEN_AC_OFF_SECS 			(6)
#define COMM_ERR_TOLERANCE			(3)

// Public Type Definitions (Enums, Structs & Classes)
class CRFGenerator;
extern CRFGenerator * g_pRFGen; //!< Globally accessible pointer to single object of CRFGenerator

/**
 * @brief	This enumeration defines the operating modes for the RF power supply
 *
 */
typedef enum upRFModeEnum
{
	RF_MODE_POWER = 0,
	RF_MODE_TEMP,
	RF_MODE_TIME,
	RF_MODE_CNT
} upRFGeneratorMode;


typedef enum upRfCoilStatusEnum
{
	RF_COIL_ERR_OK,
	RF_COIL_NOT_READY,
	RF_COIL_ERROR
} upRfCoilStatus;

/**
 * @brief	This enumeration defines the model of RF generator in use
 *
 */
typedef enum upRFModelEnum
{
	RF_MODEL_UNSET = 0,
	RF_MODEL_ITHERM,
	RF_MODEL_RDO,
	RF_MODEL_CNT
} upRFGeneratorModel;

/**
 * @class	CRFGenerator
 * @brief	This class provides the interface for application software to use the RF generator and determines which
 * 			RF generator is in the Union Generator.
 *
 *			This class provides base functionality and a standard interface for the RF generator control classes. It is inherited into
 *			each of those classes and certain methods are overridden to provide custom functionality. This class carries the member variables
 *			that are used in the common set/get interface methods.
 *
 *			This class determines which RF generator is attached to the system: RDO or iTherm. This is done immediately after
 *			the first enable AC action. Once the RF generator is identified the global pointer g_pRFGen is reassigned
 *			to point to the object that controls the attached generator.
 *
 * 			All application-level sets and gets act on cached data. The status of the RF generator is
 *			The status of the RF generator contains the operational
 * 			conditions which will confirm the success of the configuration write operations.
 *
 * Virtual Functions Overridden:
 * 	- InitializeHardware
 * 	- ReadFromHardware
 * 	- WriteToHardware
 *
 * External Data Members:
 *  - g_pRFGen
 *
 */
class CRFGenerator : public CRndRobinHelper
{
protected:
	static CRFGenerator * m_baseRfGenObj;
	static upGenericState m_eAcPowerUp;
	static int m_iAcPowerUpCycle;
	static upErr m_generatorStatus;
	static bool_c m_acInput;
	static upErr m_criticalError;
	static upRfCoilStatus m_coilStatus;
	static bool_c m_bResetRequired;
	bool_c m_applyingPower;
	bool_c m_outputEnabled;
	twobytes m_powerOutput;
#ifdef ENABLE_RDO_CURRENT_READ
	twobytes m_currentOutput;
#endif
	twobytes m_powerRequest;
	twobytes m_appliedPowerRequest;
	bool_c m_bPowerNew;
	int m_temperature;
	twobytes m_statusBytes;
	twobytes m_errorBytes;
	static byte m_bCommErr;
	static byte m_bFatalErrorCount;
	void SetInitValues();
	void SetCriticalError( upErr eErr );
	void SetCoilStatus( upRfCoilStatus eStatus );
	void ResetStates();

public:
	CRFGenerator();
	virtual upErr SendInitialCommand();
	virtual upErr ReadFromHardware();
	virtual upErr WriteToHardware();
	virtual upErr InitializeHardware();
	virtual upErr StartFreqSweep(); // For RDO only
	virtual upErr SetOutputEnable( bool_c bOn );
	virtual upErr GetRfGenModel( upRFGeneratorModel * peModel );

	// Shared core functionality functions
	virtual upErr GetError( twobytes * piError );
	twobytes GetPowerRequest();
	twobytes GetAppliedPowerRequest();
	upErr GetStatus( twobytes * piStatus );
	upErr GetTemperature( int * piTemperature );
	upErr GetPowerOutput( twobytes * ptbPowerOutput );
#ifdef ENABLE_RDO_CURRENT_READ
	upErr GetCurrentOutput( twobytes * ptbCurrentOutput );
#endif
	upErr GetApplyingPower( bool_c * piPowerOn );
	upErr GetOutputEnable( bool_c * pbOn );
	upErr SetACInput( bool_c bOn );
	upErr GetACInput( bool_c * pbOn );
	upErr SetPowerRequest( twobytes tbPower );
	twobytes GetACOffTicks();
	upErr GetCriticalError();
	upRfCoilStatus GetCoilStatus();
	bool_c ResetRequired();
};

// Inline Functions (follows class definition)

#endif /* RFGENERATOR_H_ */

/**
 * @file	TreatmentInformation.h
 * @par		Package: UGMcu
 * @par		Project: Union Generator
 * @par		Copyright (c) 2017, Uptake Medical Technology, Inc.
 * @author	Glen Oct 26, 2010 Created file
 * @brief	This file contains the declaration for CTreatmentInformation
 */

#ifndef TREATMENTINFORMATION_H_
#define TREATMENTINFORMATION_H_

// Include Files
#include "UGMcuDefinitions.h"
#include "UGError.h"
#include "UGMcuTypes.h"
#include "UGMcuEnum.h"
#include "Messages.h"

// Public Macros and Constants

// Public Type Definitions (Enums, Structs & Classes)
class CTreatmentInformation;
extern CTreatmentInformation * g_pTreatmentInfo; //!< Globally accessible pointer to single object of CTreatmentInformation

/**
 * @class	CTreatmentInformation
 * @brief	This class provides the information needed to define a therapy.
 *
 * 			It will accommodate the differences between handpieces by referencing therapy type codes. The therapy codes are
 * 			verified for correctness and a "custom" code is supported for algorithm development. The custom values can be set using
 * 			setters and it is expected that this is done over the SBC interface.
 *
 * 			An "EMC" code is supported for Electromagnetic Compliance testing. This option is only enabled is a special build
 * 			of the software and is disallowed in the release builds.
 *
 * 			This class is instantiated as a singleton and is accessed using the global pointer g_pTreatmentInfo.
 *
 * Virtual Functions Overridden: None @n@n
 *
 * External Data Members:
 *  - g_pTreatmentInfo
 *
 */

class CTreatmentInformation
{
private:
	upTreatmentCode m_TreatmentCode;

	twobytes m_Asyringe;
	int m_CoilTempLower;
	int m_iOverTempBand;
	int m_CoilTempUpper;
	int m_OutletTempMax;
	int m_OutletTempMin;
	int m_iMaxInternalTemp;
	twobytes m_RfPowerTol;
	byte m_bCustom22;
	byte m_bTdeb;
	twobytes m_tbDeliveryCount;
	twobytes m_Trest;
	byte m_bCustom6;
	twobytes m_Tvapor;
	twobytes m_Tdelivery;
	twobytes m_DeliveryMin;
	twobytes m_DeliveryMax;
	int m_WrateDelivery;
	int m_Custom19;
	int m_iWratePrime1;
	int m_iWratePrime2;
	int m_iWratePrime3;
	twobytes m_uiVprime1;
	twobytes m_uiVprime2;
	twobytes m_tbTprecond;
	twobytes m_tbPowerDelivery;
	int m_CoilTempShutdownTol;
	int m_OutletTempShutdownTol;
	twobytes m_uiTminValueTest;
	twobytes m_uiTminPrimeValueTest;
	twobytes m_uiEmcCycles;
	twobytes m_uiTtempWindow;
	twobytes m_uiCooldowntime;
	int m_WrateIdle;
	twobytes m_tbPowerIdle;
	int m_iTempUpperLimitIdle;
	int m_iTempLowerLimitIdle;
	int m_stbCustom8;
	int m_Custom15;
	int m_Custom16;
	int m_Custom17;
	int m_Custom18;
	SDeliverySteps m_customDeliverySteps;

public:
	CTreatmentInformation();
	void ResetCustomValues( upTreatmentCode eCode );

	// Getters
	int getMaxInternalTemp() const;
	int getCoilTempLower() const;
	int getOverTempBand() const;
	int getCoilTempUpper() const;
	twobytes getRfPowerTol() const;
	int getCustom15() const;
	int getCoilIdleTempSetpoint() const;
	int getCustom17() const;
	int getCustom18() const;
	int getCustom19() const;
	int getWrateDelivery() const;
	byte getCustom22() const;
	twobytes getTrest() const;
	twobytes getAsyringe() const;
	byte getTdeb() const;
	int getOutletTempMax() const;
	int getOutletTempMin() const;
	twobytes getDeliveryCount() const;
	byte getCustom6() const;
	twobytes getTdelivery() const;
	twobytes getTvapor() const;
	twobytes getDeliveryMin() const;
	twobytes getDeliveryMax() const;
	int getWratePrime3() const;
	twobytes getPowerDelivery() const;
	int getCoilTempShutdownTol() const;
	int getOutletTempShutdownTol() const;
	twobytes getTminValueTest() const;
	twobytes getTminPrimeValueTest() const;
	twobytes getEmcCycles() const;
	twobytes getTprecond() const;
	twobytes getTtempWindow() const;
	twobytes getCooldownTime() const;
	int getWrateIdle() const;
	int getTempUpperLimitIdle() const;
	int getTempLowerLimitIdle() const;
	int getCustom8() const;
	twobytes getPowerIdle() const;
	twobytes getVprime1() const;
	twobytes getVprime2() const;
	int getWratePrime1() const;
	int getWratePrime2() const;

	// Setters
	upErr setMaxInternalTemp( int iMaxInternalTemp );
	upErr setCoilTempLower( int iCoilTempLower );
	upErr setOverTempBand( int iOverTempBand );
	upErr setCoilTempUpper( int iCoilTempUpper );
	upErr setRfPowerTol( twobytes tbRfPowerTol );
	upErr setCustom15( int iCustom15 );
	upErr setCoilIdleTempSetpoint( int iCustom16 );
	upErr setCustom17( int iCustom17 );
	upErr setWrateDelivery( int iWrateDelivery );
	upErr setCustom19( int iWpresLower );
	upErr setCustom22( byte bCustom22 );
	upErr setTrest( twobytes tbTrest );
	upErr setAsyringe( twobytes tbAsyringe );
	upErr setTdeb( byte bTdeb );
	upErr setOutletTempMax( int iOutletTempMax );
	upErr setOutletTempMin( int iOutletTempMin );
	upErr setDeliveryCount( twobytes tbDeliveryCount );
	upErr setCustom6( byte bCustom6 );
	upErr setTdelivery( twobytes tbTvapor );
	upErr setTvapor( twobytes tbCustom0 );
	upErr setDeliveryMin( twobytes tbDeliveryMin );
	upErr setDeliveryMax( twobytes tbDeliveryMax );
	upErr setWratePrime3( int iWratePrime3 );
	upErr setPowerDelivery( twobytes tbCustom7 );
	upErr setCoilTempShutdownTol( int iCoilTempShutdownTol );
	upErr setOutletTempShutdownTol( int iOutletTempShutdownTol );
	upErr setTminValueTest( twobytes uiTminValueTest );
	upErr setTminPrimeValueTest( twobytes uiTminPrimeValueTest );
	upErr setEmcCycles( twobytes uiEmcCycles );
	upErr setTprecond( twobytes tbTprecond );
	upErr setTtempWindow( twobytes uiTwindow );
	upErr setCooldownTime( twobytes uiTwindow );
	upErr setWrateIdle( int iWrateIdle );
	upErr setTempUpperLimitIdle( int iUpperLimit );
	upErr setTempLowerLimitIdle( int iLowerLimit );
	upErr setCustom8( int iCustom8 );
	upErr setPowerIdle( twobytes tbPowerIdle );
	upErr setVprime1( twobytes tbVprime1 );
	upErr setVprime2( twobytes tbVprime2 );
	upErr setWratePrime1( int iWratePrime1 );
	upErr setWratePrime2( int iWratePrime2 );
	upErr setCustom18( int iCustom17 );

	// Step values set/get
	void ResetStepValues( upTreatmentCode eCode );
	byte GetStepCount();
	upErr SetStepCount(byte bCount );
	int GetStepWrate( byte bStep );
	upErr SetStepWrate( byte bStep, int iRate );
	twobytes GetStepPower( byte bStep );
	upErr SetStepPower( byte bStep, twobytes tbPower );
	twobytes GetStepTime( byte bStep );
	upErr SetStepTime( byte bStep, twobytes tbTime );
	upErr GetStepValues( SDeliverySteps * pCustomDeliverySteps );
	upErr SetStepValues( SDeliverySteps * pCustomDeliverySteps );

	// Treatment Code
	upTreatmentCode getTreatmentCode() const;
	upErr setTreatmentCode( upTreatmentCode eTreatmentCode );
	upErr checkTreatmentCode( upTreatmentCode eTreatmentCode );
};

// Inline Functions (follows class definition)

#endif /* TREATMENTINFORMATION_H_ */

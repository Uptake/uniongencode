/**
 * @file	StepperVelocity.h
 * @par		Package: UGMcu
 * @par		Project: Union Generator
 * @par		Copyright (c) 2017, Uptake Medical Technology, Inc.
 * @author	Glen Sep 16, 2010 Created file
 * @brief	This file declares the class for setting stepper motor velocity, CStepperVelocity
 */

#ifndef STEPPERVELOCITY_H_
#define STEPPERVELOCITY_H_

// Include Files
#include "UGMcuDefinitions.h"
#include "UGError.h"
#include "UGMcuTypes.h"
#include "TimeBase.h"

// Public Macros and Constants

// Public Type Definitions (Enums, Structs & Classes)

/**
 * @class	CStepperVelocity
 * @brief	Sets the stepper motor to a specific velocity.
 *
 * 			This action includes ramping the velocity to prevent
 * 			too large of accelerations and decellerations. The motor may be immediately stopped.
 * 			The velocities are in pulses per second to the stepper motor controller.
 *
 * Virtual Functions Overridden: None @n@n
 *
 * External Data Members: None @n@n
 *
 */
class CStepperVelocity : protected CTimerAccess
{
private:
	long int m_targetVelocity;
	long int m_appliedVelocity;
	bool_c m_bHalt;
	bool_c m_bEnabled;
	bool_c m_bMotorEnabled;
	upErr EnableClock( bool_c bEnable );
	tick m_tHalted;

protected:
	CStepperVelocity();
	upErr InitializeVelocityHardware();
	upErr WriteVelocityToHardware();

public:
	upErr SetTargetVelocity( long int ilVelocity );
	upErr GetTargetVelocity( long int * pilVelocity );
	upErr GetAppliedVelocity( long int * pilVelocity );
	upErr HaltStepper();
};

// Inline Functions (follows class definition)

#endif /* STEPPERVELOCITY_H_ */

/**
 * @file	MemMapIntfc.cpp
 * @par		Package: UGMcu
 * @par		Project: Union Generator
 * @par		Copyright (c) 2017, Uptake Medical Technology, Inc.
 * @author	Chris Feb 21, 2011 Created file
 * @brief   This file contains the definitions for the MemMapIntfc class, CMemMapIntfc
 */

#ifndef MEMMAPINTFC_H_
#define MEMMAPINTFC_H_

// Include Files
#include "UGMcuDefinitions.h"
#include "UGError.h"
#include "UGMcuTypes.h"
#include "UGMcuEnum.h"

// Public Macros and Constants

#define MMAP_FUNC_WDOG_SIZE 			( 2 )
#define MMAP_FUNC_WDOG_STROBE_SIZE 		( 1 )
#define MMAP_FUNC_ENCODER_CLEAR_SIZE 	( 1 )
#define MMAP_FUNC_ENCODER_SIZE 			( 3 )
#define MMAP_FUNC_HTEMP_SIZE 			( 2 )
#define MMAP_FUNC_LTEMP_SIZE 			( 2 )
#define MMAP_FUNC_UNUSED_0_SIZE 		( 2 )
#define MMAP_FUNC_SYSTEM_SIGNALS_SIZE 	( 1 )
#define MMAP_FUNC_SYSTEM_LED_SIZE 		( 1 )
#define MMAP_FUNC_SYSTEM_CONTROLS_SIZE 	( 1 )
#define MMAP_FUNC_MCU_BOARD_REVISION_SIZE 	( 1 )
#define MMAP_FUNC_CPLD_VERSION_SIZE 	( 4 )  //!< The length in bytes of the version identifier

// Public Type Definitions (Enums, Structs & Classes)
#define MMAP_LOW_ADDRESS 		(0x1100)
#define MMAP_HIGH_ADDRESS 		(0x1113)
#define MMAP_ADDRESS_COUNT       (MMAP_HIGH_ADDRESS - MMAP_LOW_ADDRESS + 1)
#define MMAP_ADDR_WDOG_LSB 			 (0x1100)
#define MMAP_ADDR_WDOG_MSB 			 (0x1101)
#define MMAP_ADDR_WDOG_STROBE 		 (0x1102)
#define MMAP_ADDR_ENCODER_CLEAR      (0x1103)
#define MMAP_ADDR_ENCODER_LSB 		 (0x1103)
#define MMAP_ADDR_ENCODER_MDB 		 (0x1104)
#define MMAP_ADDR_ENCODER_MSB        (0x1105)
#define MMAP_ADDR_HIGH_TEMP_LSB 	 (0x1106)
#define MMAP_ADDR_HIGH_TEMP_MSB 	 (0x1107)
#define MMAP_ADDR_LOW_TEMP_LSB 		 (0x1108)
#define MMAP_ADDR_LOW_TEMP_MSB 		 (0x1109)
#define MMAP_ADDR_MCU_BOARD_REVISION (0x1109) // re-used on REV G and following
#define MMAP_ADDR_UNUSED_0_LOW 		 (0x110A)
#define MMAP_ADDR_UNUSED_0_HIGH 	 (0x110B)
#define MMAP_ADDR_SYSTEM_SIGNALS 	 (0x110C)
#define MMAP_ADDR_SYSTEM_LEDS 		 (0x110D)
#define MMAP_ADDR_SYSTEM_CONTROLS 	 (0x110E)
#define MMAP_ADDR_CPLD_VERSION_DASH  (0x1110)
#define MMAP_ADDR_CPLD_VERSION_YEAR	 (0x1111)
#define MMAP_ADDR_CPLD_VERSION_DAY	 (0x1112)
#define MMAP_ADDR_CPLD_VERSION_MONTH (0x1113)

// Helper macros for the application
#define MMAP_READ( ii, jj )  MMapRead( ii, jj, sizeof( jj ) / sizeof( byte ) );
#define MMAP_WRITE( ii, jj ) MMapWrite( ii, jj, sizeof( jj ) / sizeof( byte ) );

/**
 * Definition of memory mapped function indices used by MemMapIntfc
 */
typedef enum
{
	MMAP_FUNC_WDOG = 0,
	MMAP_FUNC_WDOG_STROBE,
	MMAP_FUNC_ENCODER_CLEAR,
	MMAP_FUNC_ENCODER,
	MMAP_FUNC_HTEMP,
	MMAP_FUNC_LTEMP,
	MMAP_FUNC_UNUSED_0,
	MMAP_FUNC_SYSTEM_SIGNALS,
	MMAP_FUNC_SYSTEM_LED,
	MMAP_FUNC_SYSTEM_CONTROLS,
	MMAP_FUNC_CPLD_VERSION,
	MMAP_FUNC_MCU_BOARD_REVISION,
	MMAP_FUNC_COUNT
} upMMapFunction;


/**
 * @class	CMemMapIntfc
 * @brief   The MemMapIntfc class makes use of the external memory interface of the MCU to communicate
 * 			with devices accessed via registers on the CPLD.
 *
 *  Byte or arrays of bytes can be read/written from/to the interface by addressing a specific device mapped to a specific CPLD register.
 *
 * Virtual Functions Overridden: None @n@n
 *
 * External Data Members: None @n@n
 *
 */
class CMemMapIntfc
{
private:

	/**
	 * @brief This is the structure that carries per-funciton information
	 */
	struct SMMapInfo
	{
		byte * m_firstAddr;
		byte m_length;
	};
	static SMMapInfo m_sMMapInfo[ MMAP_FUNC_COUNT ];

public:
	CMemMapIntfc();
	upErr MMapRead( upMMapFunction eFunction, byte * pbBytes, byte bRdSize );
	upErr MMapWrite( upMMapFunction eFunction, byte * pbBytes, byte bWrSize );
	upErr MMapWriteByte( upMMapFunction eFunction, byte bByte );
};

#endif /* MMAPINTFC_H_ */

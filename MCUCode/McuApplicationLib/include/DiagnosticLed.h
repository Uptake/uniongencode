/**
 * @file       DiagnosticLed.h
 * @par        Package: UGMcu
 * @par        Project: Union Generator
 * @par        Copyright (c) 2017, Uptake Medical Technology, Inc.
 * @author     Glen Sep 15, 2010 Created file
 * @brief      This defines the CDiagnosticLed class and the enumeration for the on-board diagnostic LEDs
 */

#ifndef CDIAGNOSTICLED_H_
#define CDIAGNOSTICLED_H_

// Include Files
#include "RndRobinHelper.h"
#include "UGError.h"
#include "UGMcuTypes.h"

// Public Macros and Constants

// Public Type Definitions (Enums, Structs)
class CDiagnosticLed;
extern CDiagnosticLed * g_pDiagnosticLed; //!< Globally accessible pointer to single object of CDiagnosticLed

/**
 * @enum upDiagLed
 * These are used to select the four LEDs that are mounted directly to the daughtercard
 */
enum upDiagLed
{
	DIAG_LED0 = 0,
	DIAG_LED1,
	DIAG_LED2,
	DIAG_LED3,
	DIAG_LED_CNT
};

/**
 * @class	CDiagnosticLed
 * @brief	Provides a common interface to the 4 diagnostic LEDs mounted to the board
 *
 * Virtual Functions Overridden:
 *	- InitializeHardware
 *
 * External Data Members:
 *  - g_pDiagnosticLed
 *
 */
class CDiagnosticLed: private CRndRobinHelper
{
private:
	byte m_ledBit[ DIAG_LED_CNT ];

public:
	CDiagnosticLed();
	upErr InitializeHardware();
	upErr SetLED( upDiagLed eLed, bool_c bOnOff );
};

#endif /*CDIAGNOSTICLED_H_*/

/**
 * @file	OneWireMemory.cpp
 * @par		Package: UGMcu
 * @par		Project: Union Generator
 * @par		Copyright (c) 2017, Uptake Medical Technology, Inc.
 * @author	Glen Sep 16, 2010 Created file
 * @brief	This file declares the interface for the OneWireMemory class.
 */
#ifndef ONEWIREMEMORY_H_
#define ONEWIREMEMORY_H_

// Include Files
#include "UGMcuDefinitions.h"
#include "OneWireDriver.h"
#include "OneWireMemoryHelper.h"
#include "UGMcuEnum.h"
#include "UGMcuTypes.h"
#include "TimerAccess.h"

// Public Macros and Constants
class COneWireMemory;
extern COneWireMemory * g_pOneWireMemory; //!< Globally accessible pointer to single object of COneWireMemory

// Public Type Definitions (Enums, Structs & Classes)
#define OW_ID_RESERVED ( 8 )

/**
 * The state of the one-wire connection and ready status
 */
typedef enum upOwStateEnum
{
	OW_STATE_DISCONNECTED,
	OW_STATE_CONNECTED,
	OW_STATE_READY
} upOwState;

// Individual memory blocks

/**
 * @brief This is the portion of one-wire memory (block) that contains
 * data representing treatment information.
 */
struct SOwTreatData
{
	twobytes tbTreatCount;
	unsignedfourbytes ulTherapyTime;
};

/**
 * @brief This is the portion of one-wire memory (block) that contains
 * identifying information such as the one-wire serial number and
 * one-wire therapy code.
 */
struct SOwMemId
{
	char cReserved[ OW_ID_RESERVED ];
	byte bTherapyCode; 				  // 0x08
	byte bReserved;
	char cLotNumber[ OW_LENGTH_LOT ]; // 0x0A - 0x15
	unsignedfourbytes ulSerial; 	  // 0x16 - 0x19
};

/**
 * @brief This is the portion of one-wire memory (block) that contains
 * the time (in Unix Epoch format) that the one-wire was first
 * connected to a generator.
 */
struct SOwConnectTime
{
	unsignedfourbytes ulConnectTime;
};

/**
 * @brief This is the portion of one-wire memory (block) that contains
 * the Model Number.
 */
struct SOwModel
{
	char strModel[ OW_LENGTH_MODEL ];
};

#define OW_ADDR_PROTECT (0x0A00)
#define OW_ADDR_FACTORY (0x0A20)
#define OW_ADDR_MFGRID (0x0A23)
#define OW_SIZE_MFGRID ( 2 )

#define OW_DATA_BLOCK_CNT ( 9 )

// block definitions
#define OW_BLOCK_ID ( 0 )
#define OW_BLOCK_FIRSTUSE ( 1 )
#define OW_BLOCK_TREATINFO ( 2 )
#define OW_BLOCK_MODEL ( 3 )

#define OW_BLOCK_ADDR_ID ( 0x0000 )
#define OW_BLOCK_ADDR_CONNECT ( 0x0100 )
#define OW_BLOCK_ADDR_TREAT ( 0x0200 )
#define OW_BLOCK_ADDR_MODEL ( 0x0300 )

#define OW_SIZE_ID  ( sizeof( SOwMemId ) / sizeof ( byte ) )
#define OW_SIZE_CONNECT ( sizeof( SOwConnectTime ) / sizeof ( byte ) )
#define OW_SIZE_TREATINFO ( sizeof( SOwTreatData ) / sizeof ( byte ) )
#define OW_SIZE_MODEL ( sizeof( SOwModel ) / sizeof ( byte ) )

#define OW_SIZE_PROTECT ( 10 )

#define OW_READPASS_LIMIT ( 3 )
#define OW_WRITEPASS_LIMIT ( 4 )
#define OW_END_WRITE_ACCESS ( 0xFF )
#define OW_REG_CODE_SIZE (8)  // 8*8 bits

// This is the family code for the DS28EC20+
#define OW_FAMILY_CODE_ADDR (0)
#define OW_FAMILY_CODE (0x43)

/**
 * @class	COneWireMemory
 *
 * @brief	This class manages the 1-Wire memory.  When a one-wire device
 *   		is present on the two-wire interface bus, the memory map is read
 *    		and individual fields are read into member variables for temporary storage.
 *    		Treatment data is written to the one-wire memory using this class as an interface.
 *    		This class also write protects blocks of memory when applicable.
 *
 * Virtual Functions Overridden:
 *  - InitializeHardware
 *  - ReadFromHardware
 *  - Execute
 *
 * External Data Members: None @n@n
 *
 */
class COneWireMemory: protected COneWireDriver, protected COneWireMemoryHelper
{
private:
	/**
	 * @brief Create memory space containing memory blocks
	 *
	 */
	struct SOwMemSpace
	{
		SOwTreatData sTreat;
		SOwMemId sId;
		SOwConnectTime sConnectTime;
		SOwModel sModel;
	};

	upOwState m_eOwState;
	tick m_lastDetachedTick;
	SOwMemSpace m_sOwMemSpace;
	upTreatmentCode m_therapyCode;
	upErr m_eErrorState;
	uint32_t m_serial;
	char m_cLotNumber[ OW_LENGTH_LOT ];
	time m_firstUseTime;
	unsignedfourbytes m_therapyTime;
	byte m_fullTreatmentCount;
	bool_c m_treatFlag;
	bool_c m_writeAccessActive;
	// The last entry ID will be used to synchronize with the CTreatmentRecord class
	twobytes m_lastEntryID;
	bool_c m_deviceReady;
	bool_c m_locked;
	bool_c m_bEnableOwMonitor;
	twobytes m_mfgrId;
	byte m_owErrPass;
	byte m_romInfo[ OW_ROM_SIZE ];
	SOwState EvaluateWriteError();
	SOwState EvaluateReadError();
	upErr WriteProtectBlock( byte bBlock, byte bPass );
	upErr SetFullTreatmentCount( twobytes bFullTreatmentCount );
	upErr SetTherapyTime( unsignedfourbytes ulTime );
	upErr SetModel( void * pModel );
	upErr ReadAndStoreMfgrId(); // Read from 1wire
	upErr CheckForLock( bool_c * pbLock );
	upErr StampFirstUse();
	upErr WriteTreatment();
	upErr WriteLockdown();
	upErr ReadAll();
	upErr LockOneWire();
public:
	COneWireMemory();
	upErr InitializeHardware();
	upErr Execute();
	upErr ReadFromHardware();
	upErr CheckHardware();
	upErr CheckForHandpiece( bool_c * pbReady );
	upErr GetTherapyCode( upTreatmentCode * peCode );
	upErr GetSerialNumber( uint32_t * piSerial );
	upErr GetLotNumber( char * pcLotNumber );
	upErr GetFullTreatmentCount( twobytes * ptbCount );
	upErr GetConnectedTime( time * ptTime );
	upErr GetTherapyTime( unsignedfourbytes * pulTime );
	upErr SetFirstUseTime( time tFirstUse );
	upErr GetFirstUseTime( time * tFirstUse );
	upErr GetLockState( bool_c * pbLock );
	upErr GetModel( char * pModel );
	upErr GetRomId( byte * pbRomInfo );
	upOwState GetOwState();
	upErr DisableTool();
	upErr WriteAccess( int iAddress, byte * bStream, byte bSize, byte bPass );
	upErr ReadAccess( int iAddress, byte * pbStream, byte bSize );
	upErr ReadDevice();
	void EnableOwMonitor( bool_c bEnable );
	void ClearOwValues();
};

// Inline Functions (follows class definition)


#endif /* ONEWIREMEMORY_H_ */

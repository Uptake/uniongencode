/**
 * @file	PIDControl.h
 * @par		Package: UGMcu
 * @par		Project: Union Generator
 * @par		Copyright (c) 2017, Uptake Medical Technology, Inc.
 * @author	Glen Nov 24, 2010 Created file
 * @brief	This file contains the declaration for the PID control algorithm class, CPIDControl
 */

#ifndef PIDCONTROL_H_
#define PIDCONTROL_H_

// Include Files
#include "UGMcuDefinitions.h"
#include "UGError.h"
#include "UGMcuTypes.h"

// Public Macros and Constants
#define PID_INT_FACTOR		(4096)
#define CNVT_REAL_TO_FACTOR(x)		( (long int) ( x * (float) PID_INT_FACTOR ) )

// Public Type Definitions (Enums, Structs & Classes)

/**
 * @class	CPIDControl
 * @brief	This class provides a PID control algorithm. This includes a ideal form of
 * 			PID computation along with a linear output mapping to produce a control value.
 *
 * 			The PID computation uses the following coefficients and values:
 *
 * <ul>
 * <li>  			DeltaT 	The differential execution time
 * <li>  			e(n)	The error at sample "n" (setpoint minus feedback)
 * <li>  			Kc	An offset constant.
 * <li>  			Kp	The coefficient for the proportional term
 * <li>				Ki	The coefficient for the integrated error term
 * <li>				Di	The integral decay factor.
 * <li>				Kd	The coefficient for the error derivative term
 * </ul>
 *
 *			The linear mapping is done using a simple mx + b equation with the factors:
 *			slope 	The slope of the mapping or "m" in m*x+b
 *			offset	The offset of the mapping or "b" in m*x+b
 *
 *			The change in output signal is controlled by two limits. These are in control
 *			signal units:
 * <ul>
 * <li>			upMax	The highest rate of change for the control signal to increase
 * <li>			dnMax	The highest rate of change for control signal decrease
 * </ul>
 *
 *			To facilitate execution on an 8-bit microcontroller, all calculations will be done in integer
 *			math. The values that should actually be real in a calculation will be represented as an integer.
 *			This integer will be the real number times PID_INT_FACTOR with the remaining fractional part truncated off.
 *			The values given as fractional integers are: Kc, Kp, Ki, Di, Kd and slope. The offset, upMax and
 *			dnMax are in the same units as the control signal. The setpoint is in the units of the feedback
 *			signal.
 *
 * Virtual Functions Overridden: None @n@n
 *
 * External Data Members: None @n@n
 *
 */

class CPIDControl
{
public:
	CPIDControl();

	/**
	 * These are the coefficients for the PID calculation
	 */
	typedef struct
	{
		long int m_Kc; 		//!< The constant value
		long int m_Kp; 		//!< Proportional gain
		long int m_Ki; 		//!< Integral term gain
		long int m_Di; 		//!< Integral decay
		long int m_Kd; 		//!< Derivative term gain
		long int m_slope; 	//!< "m" in converting PID result to control signal
		long int m_offset; 	//!< "b" in converting PID result to control signal
		int m_upMax; 		//!< Maximum upwards change allowed in control signal per cycle
		int m_dnMax; 		//!< Maximum downwards change allowed in control signal per cycle
	} SPIDCoefficients;

private:
	SPIDCoefficients m_PIDCoeff; 	//!< The set of coefficients
	int m_setpoint;					//!< The target setpoint
	long int m_errIntegral;			//!< The integral of error carried between calculations
	long int m_lastErr;				//!< The last computed error (setpoint-feedback)
	int m_lastCntl;					//!< The last control signal value
	bool_c m_newCoeff;				//!< Set when persistent values will be ignored on the next calculation loop

public:
	upErr ResetCalc();
	upErr ComputePID( int newFeedback, twobytes tickDelta, int * pNewControl );
	upErr SetPIDCoefficients( SPIDCoefficients * pPIDCoeff );
	upErr GetPIDCoefficients( SPIDCoefficients * pPIDCoeff );
	upErr SetSetpoint( int setpoint );
	upErr GetSetpoint( int * pSetpoint );
};

// Inline Functions (follows class definition)


#endif /* PIDCONTROL_H_ */

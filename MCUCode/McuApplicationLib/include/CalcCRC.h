/**
 * @file	CalcCRC.h
 * @par		Package: UGMcu
 * @par		Project: Union Generator
 * @par		Copyright (c) 2017, Uptake Medical Technology, Inc.
 * @author	Glen Nov 11 10:04:52 2011 Created file
 * @brief	This file contains the declarations for CCalcCRC
 */
#ifndef __CALCCRC_H__
#define __CALCCRC_H__

#include <stdlib.h>
#include <stdint.h>

// Public Macros and Constants

/**
 * The definition of the used algorithm.
 *****************************************************************************/
#define CRC_ALGO_TABLE_DRIVEN 1

/**
 * The type of the CRC values.
 *
 * This type must be big enough to contain at least 16 bits.
 *****************************************************************************/
typedef uint16_t crc_t;

/**
 * @class	CCalcCRC
 * @brief	This class provides methods to calculate a 16-bit CRC from 8-bit data
 *
 * Generated on Fri Nov 11 10:04:52 2011, by pycrc v0.7.8, http://www.tty1.net/pycrc/ using the configuration:
 * -   Width        = 16
 * -   Poly         = 0x8005
 * -   XorIn        = 0x0000
 * -   ReflectIn    = True
 * -   XorOut       = 0x0000
 * -   ReflectOut   = True
 * -   Algorithm    = table-driven
 *
 * Virtual Functions Overridden: None @n@n
 *
 * External Data Members: None @n@n
 *
 */
class CCalcCRC
{
public:
	CCalcCRC();
	crc_t crc_reflect( crc_t data, size_t data_len );
	crc_t crc_init( void );
	crc_t crc_update( crc_t crc, const unsigned char *data, size_t data_len );
	crc_t crc_finalize( crc_t crc );
};

/**
 * @brief Provide the initial CRC value.
 *
 * @return     The initial CRC value.
 *****************************************************************************/
inline crc_t CCalcCRC::crc_init()
{
	return 0x0000;
}

/**
 * @brief Calculate the final CRC value.
 *
 * @param crc  The current CRC value.
 * @return     The final CRC value.
 *****************************************************************************/
inline crc_t CCalcCRC::crc_finalize( crc_t crc )
{
	return crc ^ 0x0000;
}

#endif      /* __CALCCRC_H__ */

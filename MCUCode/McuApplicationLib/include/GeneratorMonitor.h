/**
 * @file	GeneratorMonitor.h
 * @par		Package: UGMcu
 * @par		Project: Union Generator
 * @par		Copyright (c) 2017, Uptake Medical Technology, Inc.
 * @author	Glen Oct 21, 2010 Created file
 * @brief	This file contains the declaration for the CMonitorGenerator class that checks
 * 			the generator for obvious failures
 */

#ifndef GENERATORMONITOR_H_
#define GENERATORMONITOR_H_

// Include Files
#include "UGError.h"
#include "UGMcuTypes.h"
#include "UGMcuEnum.h"
#include "TimerAccess.h"
#include "McuInfo.h"

// Public Macros and Constants
#define THERMO_CONNECT_TOL		(MS_TO_RND_ROBIN_CYC(500)) //!< Wait a half second before flagging TC error when connecting
#define OVER_MAX_TEMP_CNT		(MS_TO_RND_ROBIN_CYC(100)) //!< Wait 100 ms before signaling an over-temp to allow disconnects to fully slew the ADC readings
#define RF_STATUS_LAG_TOLERANCE (MS_TO_RND_ROBIN_CYC(500))//!< The count of errors to allow from the RF generator before flagging an error

class CGeneratorMonitor;
extern CGeneratorMonitor * g_pGeneratorMonitor; //!< Globally accessible pointer to single object of CGeneratorMonitor

// Public Type Definitions (Enums, Structs & Classes)

/**
 * Mask for use with handpiece interlock tests
 */
typedef enum upInterlockFlagEnum
{
	INTLCK_NONE = 0, //!< No interlocks made
	INTLCK_HANDPIECE = 1, //!< 1-Wire present
	INTLCK_SYRINGE = 2, //!< Syringe switch ON
	INTLCK_UNUSED_4 = 4, //!< Unused switch ON
	INTLCK_COIL = 8, //!< RF generator status good
	INTLCK_SBC = 16, //!< SBC communicating periodically
	INTLCK_ALL_NO_SYR = 29, //!< Mask of all used interlock bits except for syringe
	INTLCK_ALL = 31, //!< Mask of all used interlock bits
	INTLCK_OVERVALUE //!< One more than the highest value
} upInterlockFlag;

/**
 * @class	CGeneratorMonitor
 * @brief	This class provides methods to check for the status of the generator.
 *
 * 			It is used in all operations to determine if there has been faults in
 * 			internal operation or feedback conditions. This class checks for interlocks with the
 * 			delivery device and checks on temperatures during ablations and between
 * 			ablations. It also checks the status of the RF generator.
 *
 * Virtual Functions Overridden: None @n@n
 *
 * External Data Members:
 *  - g_pGeneratorMonitor
 *
 */
class CGeneratorMonitor: private CMcuInfo, private CTimerAccess
{
private:
	tick m_tDebounceOffTick;
	bool_c m_bLastDebounce;
	byte m_bPowerOnErrorCnt;
	byte m_bPowerRunningErrorCnt;
	byte m_bPowerRangeErrorCnt;
	byte m_bOverTempCnt;
	upGenericState m_eHandpieceConnection;
	byte m_bThermoUnconnectCount;
	byte m_bOverMaxCnt;
	byte m_bRfGenFailureCnt;

protected:
	upErr CheckCriticalTemperatures();

public:
	CGeneratorMonitor();
	virtual upErr CheckGenerator( upInterlockFlag eInterlockNeeded );
	virtual upErr CheckInterlocks( bool_c bThermoUnconnError, upInterlockFlag * pInter, bool_c bEnableRfAC );
	virtual upErr GetDebouncedTrigger( bool_c * pbOn );
	virtual upErr CheckRF();
	virtual upErr PendingReadyCheck( bool_c bLastTrig, tick tLastEnd, upPendingReady * pPendingSubstate, byte * pbProgress );
	virtual upErr CheckOverTemp();
	virtual upErr PerformCriticalMonitoring();
	upErr CheckInternalTemperature();
	upErr GetNeededInterlocks( upOperState eOperState, upInterlockFlag *peInterlockNeeded );
	upErr SetOverTempCnt(byte bOverTempCnt);
};

// Inline Functions (follows class definition)


#endif /* GENERATORMONITOR_H_ */

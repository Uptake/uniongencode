/**
 * @file	SelfTest.h
 * @par		Package: UGMcu
 * @par		Project: Union Generator
 * @par		Copyright (c) 2017, Uptake Medical Technology, Inc.
 * @author	Glen May 10, 2011 Created file
 * @brief	This file contains the declaration of CSelfTest
 */

#ifndef SELFTEST_H_
#define SELFTEST_H_

// Include Files
#include "UGMcuDefinitions.h"
#include "UGError.h"
#include "UGMcuEnum.h"
#include "TimerAccess.h"
#include "MemMapIntfc.h"
#include "CalcCRC.h"
#include "McuInfo.h"

// Public Macros and Constants
#define SELF_TEST_CYCLE_CNT	( 20 * RND_ROBIN_LOOP_HZ )

// Public Type Definitions (Enums, Structs & Classes)
class CSelfTest;
extern CSelfTest * g_pSelfTest; //!< Globally accessible pointer to single object of CSelfTest
extern twobytes g_tbImageCrc;

/**
 * @class	CSelfTest
 * @brief	This class provides methods to perform self-test operations on the MCU hardware.
 *
 * 			These tests cover the water pump motor drive, CPLD, internal and external
 * 			SRAM memories and CRC of the program as stored in FLASH.
 *
 * Virtual Functions Overridden: None @n@n
 *
 * External Data Members:
 *  - g_pSelfTest
 */
class CSelfTest: private CCalcCRC, private CMemMapIntfc, protected CTimerAccess
{
private:
	upErr m_eWaterPumpErr;
	upErr m_eCpldErr;
	byte m_bProgress;
	twobytes m_testCycle;
	bool_c m_initiallyRetracted;
	upGenericState m_eState;
	CMcuInfo m_mcuInfo;

	byte * MemTestDevice( volatile byte * baseAddress, twobytes nBytes );
	byte * MemTestAddressBus( volatile byte * baseAddress, twobytes nBytes );
	byte MemTestDataBus( volatile byte * address );
	upErr MemTestBlockWithSave( byte * pBaseAddress, byte * pSaveAddress, byte * pEndAddress, twobytes tbBlockSize );

public:
	CSelfTest();
	upErr MemTest( void );
	upErr SystemTest( upGenericState * pNextState );
	upErr CrcTest();
	upErr GetProgress( byte * pbProgress );
	upErr SetTestCycle( twobytes tbTestCycle );
	twobytes GetTestCycle();
};

// Inline Functions (follows class definition)

/**
 * @brief	This method gets the tests cycle counter
 *
 * @return   Test cycle count
 */
inline twobytes CSelfTest::GetTestCycle()
{
	return m_testCycle;
}

#endif /* SELFTEST_H_ */

/**
 * @file       RndRobinHelper.h
 * @par        Package: UGMcu
 * @par        Project: Union Generator
 * @par        Copyright (c) 2017, Uptake Medical Technology, Inc.
 * @author     Glen Sep 15, 2010 Created file
 * @brief      This file declares the helper class, CRndRobinHelper, for round-robin scheduling
 */
#ifndef RNDROBINHELPER_H_
#define RNDROBINHELPER_H_

// Include Files
#include "UGMcuDefinitions.h"
#include "UGError.h"
#include "UGMcuTypes.h"

// Public Macros and Constants

// Pass Masks
#define PASS_MASK_ALL		(0xFF)			//!< Mask TRUE on each cycle
#define PASS_MASK_TEMPER	(0b00010001)	//!< Mask TRUE on every fourth cycle

// Public Type Definitions (Enums, Structs & Classes)

/**
 * @class   CRndRobinHelper
 * @brief	Virtual stubs and helper methods for classes that are in in round-robin task system.
 *
 * 			The virtual methods are read, write, execute
 * 			and initialize of the hardware. The class could be pure virtual but is defined as regular
 * 			virtual to allow sub-classes to override only those classes they need. This class also
 * 			provides a mechanism to execute operations on certain passes through the round-robin
 * 			scheduler using the PassFlag and PassMask. The bits in the mask are the passes through
 * 			the loop on which the action is executed. Use of the mask is done at the class level
 * 			allowing certain operations (read/write/execute) to be performed periodically.
 *
 * Virtual Functions Overridden: None @n@n
 *
 * External Data Members: None @n@n
 *
 */
class CRndRobinHelper
{
private:
	static byte m_passFlag; //!< This is the mask representation of the cycle 1, 10, 100, etc.
	static byte m_passIndex; //!< This is the index (0->7) of the cycle
	static twobytes m_passCount; //!< This is the count of passes through the loop. ERR_OK to wrap.
	byte m_passMask; //!< Each child class may have its own mask for per-cycle execution

public:
	CRndRobinHelper();
	virtual upErr ReadFromHardware();
	virtual upErr WriteToHardware();
	virtual upErr InitializeHardware();
	virtual upErr Execute();
	upErr SetPassMask( byte bPassMask );

protected:
	twobytes GetPassCount();
	upErr ResetPassFlag();
	upErr IncrementPassFlag( byte * pbPassIndex );
	upErr IncrementOnlyPassFlag( byte * pbPassIndex );
	bool_c GetPassEnabled();
	upErr ClearPassCount();
};

/**
 * @brief	This method returns the pass count which is a count of round-robin cycles
 *
 * @return	cycle count as twobytes
 */
inline twobytes CRndRobinHelper::GetPassCount()
{
	return m_passCount;
}

/**
 * @brief	This method resets the pass count to zero
 *
 * @return	ERR_OK
 */
inline upErr CRndRobinHelper::ClearPassCount()
{
	m_passCount = 0;
	return ERR_OK;
}

#endif /*RNDROBINHELPER_H_*/

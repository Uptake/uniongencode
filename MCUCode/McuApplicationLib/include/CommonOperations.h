/**
 * @file	CommonOperations.h
 * @par		Package: UGMcu
 * @par		Project: Union Generator
 * @par		Copyright (c) 2017, Uptake Medical Technology, Inc.
 * @author	Glen Nov 3, 2010 Created file
 * @brief	This file contains the declarations for CCommonOperations
 */

#ifndef COMMONOPERATIONS_H_
#define COMMONOPERATIONS_H_

// Include Files
#include "UGMcuDefinitions.h"
#include "UGError.h"
#include "UGMcuEnum.h"
#include "TimerAccess.h"
#include "MemMapIntfc.h"
#include "PIDControl.h"

// Public Macros and Constants

// Public Type Definitions (Enums, Structs & Classes)
class CCommonOperations;
extern CCommonOperations * g_pComOp; //!< Globally accessible pointer to single object of CCommonOperations

/**
 * @class	CCommonOperations
 * @brief	This class provides lower-level operations that are shared between tasks.
 *
 * These operations include
 * initiating steam flow and halting steam flow in both abrupt and controlled ways for ablations. It handle
 * the Coil Idling logic and uses three composed instances of CPIDControl to control the power when Coil Idling.
 * This class also contains the version identification for the software and the Union Generator system. @n@n
 *
 * Virtual Functions Overridden: None @n@n
 *
 * External Data Members:
 *  - g_pComOp
 */
class CCommonOperations: private CMemMapIntfc, private CTimerAccess
{

private:
	static const char * m_sVersionId; //!< Character string that uniquely identifies a build of the code
	upGenericState m_subState; //!< State of ablation inside CCommonOperations
	const char * m_sSystemVersionId; //!< Character string that uniquely identifies a system version
	tick m_tCoilIdleTick; //!< Tick time of change in coil idling action ON, OFF or power level
	upErr GetCpldVersion( char * pcCpldVersion );
	byte m_bCurrentStep;
	tick m_tStepEnd[DELIVERY_STEPS_MAX];
	CPIDControl m_CoilIdlePID;

protected:
	// This protected methods is used for unit testing only
	void SetSubState( upGenericState eSubState );

public:
	CCommonOperations();
	upErr InitiateVaporImmediately( int iWrate, twobytes tbPower );
	upErr HaltPumpAndRF();
	const char * GetMcuVersion();
	const char * GetSystemVersion();
	upErr SetStartState();
	upGenericState GetSubState();
	upErr GetRtcVersions();
	upErr VerifyCodeVersions();
	upErr ConstantVaporFlow( tick tbElapsedTicks, tick tbMinValueCheckTicks );
	upErr StartCoilIdle();
	upErr ApplyCoilIdle( tick tStartTick );
	upErr StartDeliverySteps();
	upErr AdvancedDeliverySteps( tick tbRunTicks );
};

// Inline Functions (follows class definition)

/**
 * @brief	This method returns the MCU software version
 *
 * @return   Pointer to null-terminated string containing MCU version label
 */
inline const char * CCommonOperations::GetMcuVersion()
{
	return m_sVersionId;
}

/**
 * @brief	This method returns the system version
 *
 * @return   Pointer to null-terminated string containing System version label
 */
inline const char * CCommonOperations::GetSystemVersion()
{
	return m_sSystemVersionId;
}

#endif /* COMMONOPERATIONS_H_ */

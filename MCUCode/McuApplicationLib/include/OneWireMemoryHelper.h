/**
 * @file	OneWireMemoryHelper.h
 * @par		Package: UGMcu
 * @par		Project: Union Generator
 * @par		Copyright (c) 2017, Uptake Medical Technology, Inc.
 * @author	Chris Jul 22, 2011 Created file
 * @brief	This file contains the definitions for the COneWireMemoryHelper class
 */

#ifndef ONEWIREMEMORYHELPER_H_
#define ONEWIREMEMORYHELPER_H_

// Include Files
#include "UGMcuDefinitions.h"
#include "UGMcuEnum.h"
#include "UGError.h"
#include "UGMcuTypes.h"

// Public Macros and Constants

// Public Type Definitions (Enums, Structs & Classes)
#define MAX_OW_EVENTS ( 5 )
#define OW_STATE_ENTER ( true )
#define OW_STATE_EXIT ( false )
#define OW_PASSES_WRITE ( 4 )
#define OW_PASSES_READALLBANKS ( 6 )
#define OW_RDALL_ROM ( 0 )
#define OW_RDALL_LOCKS ( 1 )
#define OW_RDALL_ID ( 2 )
#define OW_RDALL_MODEL ( 3 )
#define OW_RDALL_CONNECT ( 4 )
#define OW_RDALL_TREATMENTS ( 5 )
#define OW_RDALL_ERROR ( 6 )

/**
 * @brief These identify the individual events that may be queued for later action
 */
typedef enum upOneWireEventEnum
{
	OW_EVENT_NONE,
	OW_EVENT_NEW_MEMORY,
	OW_EVENT_STAMP_FIRST_USE,
	OW_EVENT_DATA_CHANGE,
	OW_EVENT_LOCK_DEVICE,
	OW_EVENT_CNT
} upOneWireEvent;


/**
 * @brief This identifies the top-level state of the One-Wire driver.
 *
 * 		There are sub-states for FIRSTUSE and READ_ALL.
 */
typedef enum upOneWireStateEnum
{
	OW_STATE_IDLE,
	OW_STATE_WRITE_FIRSTUSE, // writes have 3 sub-states each
	OW_STATE_WRITE_TREATMENT,
	OW_STATE_WRITE_LOCKDOWN,
	OW_STATE_READ_ALL,  // 4 substates
	OW_STATE_ERROR,
	OW_STATE_CNT
} upOneWireState;

/**
 * @brief This struct contains the state of the 1-wire memory
 */
struct SOwState
{
	upOneWireState eTopState;
	// when time to exit, set to GEN_STATE_END
	upGenericState eSubState;
	byte bSubStatePass; // multiple passes needed for writes
	bool_c isRunning;
};

/**
 * @class	COneWireMemoryHelper
 * @brief	This class provides event queue handling for the COneWireMemory class
 * @brief   and encapsulates all state transitions.
 *
 * Events are queued and dequeued in a first-in-first-out (FIFO) ordering scheme.
 * Top states are updated by dequeueing events and loERR_OKing up the top state attached
 * to that event.  Sub-states are changed by calling public methods that set the
 * sub-state directly.
 *
 * Virtual Functions Overridden: None @n@n
 *
 * External Data Members: None @n@n
 *
 */
class COneWireMemoryHelper
{
private:
	upOneWireEvent m_eventQ[ MAX_OW_EVENTS ];
	static byte m_bStateCounts[ OW_STATE_CNT ];
	byte m_eventHead;
	byte m_eventCnt;
	SOwState m_currentState;
	upOneWireEvent EventQPop();
	upErr EventQPush( upOneWireEvent eEvent );
	upErr GetNextFreeEventIndex( byte * pbIndex );
	upOneWireState EventToTopState( upOneWireEvent eEvent );
	upOneWireEvent GetNextEvent();
	void TransitionToState( upOneWireState eState );
protected:
	COneWireMemoryHelper();
	SOwState GetCurrentState();
	SOwState GetNextTopState();
	SOwState IncrementSubStatePass();
	SOwState SetSubState( upGenericState eState );
	upErr AddEvent( upOneWireEvent eEvent );
	void ResetEventQ();
};

// Inline Functions (follows class definition)


#endif /* ONEWIREMEMORYHELPER_H_ */

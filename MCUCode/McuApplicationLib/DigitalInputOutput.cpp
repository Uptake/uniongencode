/**
 * @file       DigitalInputOutput.cpp
 * @par        Package: UGMcu
 * @par        Project: Union Generator
 * @par        Copyright (c) 2017, Uptake Medical Technology, Inc.
 * @author     Glen Sep 15, 2010 Created file
 * @brief      This file contains the code to control the digital inputs and outputs through CDigitalInputOutput
 */

// Include Files

#include "UGMcuDefinitions.h"
#include "DigitalInputOutput.h"
#include "MemMapIntfc.h"
#include "LogMessage.h"
#include "stm32f4xx_hal.h"

// External Public Data
CDigitalInputOutput * g_pDigitalInputOutput = 0;

// File Scope Macros and Constants

// File Scope Type Definitions (Enums, Structs & Classes)

// File Scope Data

// File Scope Functions

// Public Functions


/**
 * @brief	This is the constructor for CDigitalInputOutput.
 *
 * It clears the inputs and outputs to all false states and sets the output change flags to false. It assigns the
 * inversion map for incoming inputs so at the application level true means ON.
 *	The @a this pointer is assigned to g_pDigitalInputOutput for access as a singleton.
 */
CDigitalInputOutput::CDigitalInputOutput()
{
	int i;

	g_pDigitalInputOutput = this;
	m_lastDigIn = 0;
	m_digSysLEDOut = 0;
	m_digSysLEDLastOut = 0;
	m_eLedState = LED_OFF;

	// Set that everything is off and not inverted
	for ( i = 0; i < DIG_IN_CNT; i++ )
		{
		m_digIn[ i ] = false;
		m_digInInvert[ i ] = false;
		}

	// Set bit relationship
	m_digInBit[ DIG_IN_TRIGGER ] = DIG_IN_TRIGGER_BIT;
	m_digInBit[ DIG_IN_SYR_PRESENT ] = DIG_IN_SYR_PRESENT_BIT;
	m_digInBit[ DIG_IN_MOTOR_RETRACT ] = DIG_IN_MOTOR_RETRACT_BIT;
	m_digInBit[ DIG_IN_MOTOR_EXTEND ] = DIG_IN_MOTOR_EXTEND_BIT;
	m_digInBit[ DIG_IN_UNUSED_0 ] = DIG_IN_UNUSED_0_BIT;
	m_digInBit[ DIG_IN_UNUSED_1 ] = DIG_IN_UNUSED_1_BIT;
	m_digInBit[ DIG_IN_STANDBY_UNUSED ] = DIG_IN_STANDBY_BIT;

	// Mark inverted inputs
	m_digInInvert[ DIG_IN_UNUSED_0 ] = true;
	m_digInInvert[ DIG_IN_SYR_PRESENT ] = true;
	m_digInInvert[ DIG_IN_MOTOR_EXTEND ] = true;
	m_digInInvert[ DIG_IN_MOTOR_RETRACT ] = true;

	// Construct inversion mask
	m_digInInvertMask = 0;
	for ( i = 0; i < DIG_IN_CNT; i++ )
		{
		if ( m_digInInvert[ i ] )
			m_digInInvertMask |= ( 1 << m_digInBit[ i ] );
		}

	// Set all digital output fields
	for ( i = 0; i < DIG_OUT_CNT; i++ )
		{
		m_digOut[ i ] = false;
		}

}

/**
 * @brief	This method clears all UI LEDs and all digital outputs.
 *
 * It then sets the UI LEDs to the LED_POWER
 * setting. This is forced to the hardware by calling WriteToHardware().
 *
 * @return   Status of WriteToHardware all as upErr
 */
upErr CDigitalInputOutput::InitializeHardware()
{
	MMapWriteByte( MMAP_FUNC_SYSTEM_LED, 0 );
	MMapWriteByte( MMAP_FUNC_SYSTEM_CONTROLS, 0 );
	SetLEDState( LED_POWER );
	return WriteToHardware();
}

/**
 * @brief	This method reads all physical DI in the system.
 *
 * It applies inversion so true means ON
 * and then sorts them into the logical identities in m_digIn[].
 *
 * @return   Status as upErr
 */
upErr CDigitalInputOutput::ReadFromHardware()
{
	byte bVal;

	MMAP_READ( MMAP_FUNC_SYSTEM_SIGNALS, &bVal );

	// If NOT simulating then invert the bits. This way they appear in output logs with 1 => ON
	// Don't invert in simulation because those same "pre-inverted" data was used as the input for the simulation run
	if ( 0 == g_bIsSimulated )
		bVal = ( bVal & ( ~m_digInInvertMask ) ) | ( ( ~bVal ) & m_digInInvertMask );

	// If changed then assign to individual values
	if ( m_lastDigIn != bVal )
		{
		for ( int i = 0; i < DIG_IN_CNT; i++ )
			m_digIn[ i ] = ( bVal & ( 1 << m_digInBit[ i ] ) ) ? true : false;
		m_lastDigIn = bVal;
		}

	return ERR_OK;
}

/**
 * @brief	This method sets the values of the digital outputs (DO).
 *
 * It only sets the DO and System LEDs if they have changed since the last call.
 *
 * @return   Status as upErr
 */
upErr CDigitalInputOutput::WriteToHardware()
{
	int iBit;
	int i;
	byte bVal = 0;
	upErr eErr = ERR_OK;

	// Set the system LEDs
	ApplyLEDState();
	if ( m_digSysLEDLastOut != m_digSysLEDOut )
		{
		m_digSysLEDLastOut = m_digSysLEDOut;
		LOG_ERROR_RETURN( eErr = MMapWriteByte( MMAP_FUNC_SYSTEM_LED, m_digSysLEDOut ) )
		}

	// Loop over all other outputs
	for ( iBit = 1, i = 0; i < DIG_OUT_CONTROL_CNT; i++, iBit <<= 1 )
		{
		if ( m_digOut[ i ] )
			bVal |= iBit;
		}

	LOG_ERROR_RETURN( eErr = MMapWriteByte( MMAP_FUNC_SYSTEM_CONTROLS, bVal ) )

	return eErr;
}

/**
 * @brief	This method lets the application specify the state of an output.
 *
 * 			The method work on both outputs and system LEDs.
 * 			The actual setting is done in the next call to WriteHardware().
 * 			Only changes of state will actually be applied. The same state can be re-asserted by toggling (E.G. on-off) in
 * 			consecutive calls to this function.
 *
 * @param    eOut	The enumeration value of the output
 * @param    bOn	bool_c of true for ON or false for OFF
 *
 * @return   Status as upErr
 */
upErr CDigitalInputOutput::SetOutput( upDigitalOutputs eOut, bool_c bOn )
{
	if ( eOut >= DIG_OUT_CNT )
		return ERR_ENUM_OUT_OF_RANGE;

	if ( m_digOut[ eOut ] != bOn )
		{
		m_digOut[ eOut ] = bOn;

		if ( DIG_OUT_LED0 <= eOut && eOut <= DIG_OUT_LED7 )
			{
			if ( bOn )
				m_digSysLEDOut |= ( 1 << ( eOut - DIG_OUT_LED0 ) );
			else
				m_digSysLEDOut &= ~( 1 << ( eOut - DIG_OUT_LED0 ) );
			}
		}

	return ERR_OK;
}

/**
 * @brief	This method returns the last set state of an output.
 *
 * @param    eOut	The output enumeration as upDigitalOutputs
 * @param    pbOn	A pointer to a bool_c to get the output setting
 *
 * @return   Status as upErr
 */
upErr CDigitalInputOutput::GetOutput( upDigitalOutputs eOut, bool_c *pbOn )
{
	if ( eOut >= DIG_OUT_CNT )
		return ERR_ENUM_OUT_OF_RANGE;
	*pbOn = m_digOut[ eOut ];
	return ERR_OK;
}

/**
 * @brief	This method returns the latest setting of an input
 *
 * @param    eIn	The input enumeration as upDigitalInputs
 * @param    pbOn	A pointer to a bool_c to get the input setting
 *
 * @return   Status as upErr
 */
upErr CDigitalInputOutput::GetInput( upDigitalInputs eIn, bool_c *pbOn )
{
	if ( eIn >= DIG_IN_CNT )
		return ERR_ENUM_OUT_OF_RANGE;
	*pbOn = m_digIn[ eIn ];
	return ERR_OK;
}

/**
 * @brief	This method gets all of the output states as a two-byte value.
 *
 * The bits in the value correspond to the order of the upDigitalOutputs enumeration.
 *
 * @param	ptOn	A pointer to a twobyte to receive the masked value
 *
 * @return   Status as upErr
 */
upErr CDigitalInputOutput::GetAllOutput( twobytes *ptOn )
{
	byte i;
	*ptOn = 0;
	for ( i = 0; i < DIG_OUT_CNT; i++ )
		if ( m_digOut[ i ] )
			*ptOn |= ( 1 << i );
	return ERR_OK;
}

/**
 * @brief	This method gets all of the input states as a single byte value
 *
 * @param	pbOn	A pointer to a byte to receive the masked value
 *
 * @return   Status as upErr
 */
upErr CDigitalInputOutput::GetAllInput( byte *pbOn )
{
	*pbOn = m_lastDigIn;
	return ERR_OK;
}

/**
 * @brief	This method sets the LED state as a member variable
 *
 * @param	eState The state to display as upLEDState
 *
 * @return   Status as upErr
 */
upErr CDigitalInputOutput::SetLEDState( upLEDState eState )
{
	if ( LED_ENUM_COUNT <= eState )
		return ERR_ENUM_OUT_OF_RANGE;
	if ( LED_TERMINATE != m_eLedState || LED_OFF == eState )
		m_eLedState = eState;
	return ERR_OK;
}

/**
 * @brief	This method sets the system LEDs to reflect the operating condition of the generator.
 *
 * 			The LEDs are lit in these patterns:
 * <TT><PRE>
 *      Enum     Left Blue  Right Blue  Green   Amber @n
 *    LED_OFF       OFF        OFF       OFF     OFF @n
 *    LED_POWER     ON         ON        OFF     OFF @n
 *    LED_PRIME     OFF        ON       Blink    OFF @n
 *    LED_READY     OFF        ON        ON      OFF @n
 *    LED_TREAT     OFF        ON       Blink    OFF @n
 *    LED_WARN      ON         OFF       OFF     ON @n
 *    LED_TERMINATE OFF        OFF       OFF     ON @n
 * </PRE></TT>
 *
 *
 * @return   Status as upErr
 */
upErr CDigitalInputOutput::ApplyLEDState()
{
	bool_c bBlueLeft = false;
	bool_c bBlueRight = false;
	bool_c bGreen = false;
	bool_c bAmber = false;

	// Determine which colors are ON and OFF
	switch ( m_eLedState )
		{
		case LED_OFF:
			break;

		case LED_POWER:
			bBlueLeft = true;
			bBlueRight = true;
			break;

		case LED_PRIME:
			bBlueRight = true;
				bGreen = true;
			break;

		case LED_READY:
			bBlueRight = true;
			bGreen = true;
			break;

		case LED_TREAT:
			bBlueRight = true;
				bGreen = true;
			break;

		case LED_TERMINATE:
			bAmber = true;
			break;

		case LED_WARN:
			bBlueLeft = true;
			bAmber = true;
			break;

		default:
			return ERR_ENUM_OUT_OF_RANGE;
			break;
		}

	// Set the LEDs
	SetOutput( DIG_OUT_BLUE_LED1, bBlueLeft );
	SetOutput( DIG_OUT_BLUE_LED2, bBlueLeft );
	SetOutput( DIG_OUT_BLUE_LED3, bBlueRight );
	SetOutput( DIG_OUT_BLUE_LED4, bBlueRight );

	SetOutput( DIG_OUT_GREEN_LED1, bGreen );
	SetOutput( DIG_OUT_GREEN_LED2, bGreen );

	SetOutput( DIG_OUT_AMBER_LED1, bAmber );
	SetOutput( DIG_OUT_AMBER_LED2, bAmber );

	return ERR_OK;
}


/**
 * @brief	This method gets the LED state
 *
 * @param	peState a pointer to an upLEDState to receive the state
 *
 * @return   Status as upErr
 */
upErr CDigitalInputOutput::GetLEDState( upLEDState * peState )
{
	*peState = m_eLedState;
	return ERR_OK;
}

// Private Class Functions

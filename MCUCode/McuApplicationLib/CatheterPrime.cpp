/**
 * @file	CatheterPrime.cpp
 * @par		Package: UGMcu
 * @par		Project: Union Generator
 * @par		Copyright (c) 2017, Uptake Medical Technology, Inc.
 * @author	Glen Oct 26, 2010 Created file
 * @brief	This file contains the code for CCatheterPrime
 */

// Include Files
#include "CatheterPrime.h"
#include "TreatmentInformation.h"
#include "GeneratorMonitor.h"
#include "DigitalInputOutput.h"
#include "WaterCntl.h"
#include "LogMessage.h"
#include "Temperature.h"
#include "RFGenerator.h"
#include "CommonOperations.h"
#include "DeliveryOracle.h"

// External Public Data

// File Scope Macros and Constants

// This conversion uses ul volume target and ml/min flow rate
// It is more formally SECS_TO_TICKS(  MIN_TO_SECS( ( vol / 1000 ) / rate ) )
// At this time the tick clock is 1000/sec so those factors fall out
#if TICK_CLOCK_RATE != 1000
Tick clock not 1000
#endif

// This leaves a simplified form using wide math for accuracy
#define VOLUME_AND_RATE_TO_TICKS( vol, rate )  ( MIN_TO_SECS( (unsignedfourbytes) vol ) / (unsignedfourbytes)rate )

// File Scope Type Definitions (Enums, Structs & Classes)

// File Scope Data

// File Scope Functions

// Public Functions

/**
 * @brief	This is the constructor for CatheterPrime.
 *
 * 			It assigns default values to all member variables.
 *
 */
CCatheterPrime::CCatheterPrime()
{
	m_tStartTick = 0;
	m_tRunTicks = 0;
	m_tTotalPrimeTicks = 0;
	m_state = PRIME_VAPOR_START;
}

/**
 * @brief	This method starts the priming operation.
 *
 * It checks the pump extended bit and exits in error if it is set ON.
 * If there is water available, it starts the water flowing at WRateFast.
 * It also grabs the tick timer to mark the start of priming for time-out evaluations.@n
 * If any errors are found the next state is PRIME_ERROR.@n
 *
 * @return   Status as upErr
 */
upErr CCatheterPrime::InitializePrime()
{
	upErr eErr = ERR_ERROR;

	bool_c bExt;

	m_tRunTicks = 0;
	m_state = PRIME_VAPOR_START;

	// Configure treatment
	STreatmentSelection msgTreatmentSelection;
	msgTreatmentSelection.Ttreat = g_pTreatmentInfo->getDeliveryMax();
	msgTreatmentSelection.TtreatStatus = ERR_OK;
	LOG_ERROR_RETURN( eErr = g_pDelOracle->NewSelection( msgTreatmentSelection ) )
	if ( ERR_OK != eErr )
		{
		return eErr;
		}

	//* TODO Fix total time
	twobytes tbMilliSecs = 0;
	LOG_ERROR_RETURN( eErr = g_pDelOracle->GetTotalTime( &tbMilliSecs ) )
	m_tTotalPrimeTicks = MS_TO_TICKS( tbMilliSecs );
	GetTick( &m_tStartTick );

	// Check rod extension
	LOG_ERROR_RETURN( eErr = g_pDigitalInputOutput->GetInput( DIG_IN_MOTOR_EXTEND, &bExt ) )
	if ( bExt )
		{
		LOG_ERROR_RETURN( eErr = ERR_PUMP_AT_LIMIT )
		}
	else
		{
		// Start controlled flow of steam
		g_pComOp->SetStartState();
		LOG_ERROR_RETURN( eErr = g_pComOp->StartDeliverySteps() )
		GetTick( &m_tStartTick );
		}

	if ( ERR_OK == eErr )
		m_state = PRIME_VAPOR_TEST;
	else
		m_state = PRIME_ERROR;

	return eErr;
}

/**
 * @brief	This method performs the vapor test portion of the priming operation.
 *
 * 			This has water pumped while RF is ON so the performance of the coil and delivery system can be judged
 * 			and the system prepared for the first treatment. It monitors the generator operation and delivery device trigger and
 * 			checks that temperatures are within normal ranges. It also
 * 			monitors the elapsed time and will end the ablation once the full time has elapsed.
 *
 * @return   Status as upErr
 */
upErr CCatheterPrime::VaporTest()
{
	upErr eErr = ERR_OK;

	// Stop priming if the priming vapor ON time is over or equal to bCustom22
	if ( m_tRunTicks >= m_tTotalPrimeTicks )
		{
		m_state = PRIME_CONTROLLED_STOP;
		g_pComOp->SetStartState();
		}
	else
		{
		LOG_ERROR_RETURN(
		        eErr = g_pComOp->ConstantVaporFlow( m_tRunTicks, (tick) MS_TO_TICKS( (twobytes ) g_pTreatmentInfo->getTminPrimeValueTest() ) ) )

		if ( ERR_OK != eErr )
			{
			m_state = PRIME_ERROR;
			}
		}

	// Advance steps if needed
	if ( ERR_OK == eErr && PRIME_VAPOR_TEST == m_state )
		{
		LOG_ERROR_RETURN( eErr = g_pComOp->AdvancedDeliverySteps( m_tRunTicks ) )
		}

	return eErr;
}

/**
 * @brief	This method is the top level for priming the handpiece.
 *
 * @return   Status as upErr
 */
upErr CCatheterPrime::PerformPrime()
{
	upErr eErr = ERR_ERROR;
	bool_c bOn;

	// Update run ticks
	GetTickElapsed( m_tStartTick, &m_tRunTicks );


	upInterlockFlag eInterlockNeeded = INTLCK_ALL;
	g_pGeneratorMonitor->GetNeededInterlocks( OPER_STATE_CATH_PRIMING, &eInterlockNeeded );
	LOG_ERROR_RETURN( eErr = g_pGeneratorMonitor->CheckGenerator( eInterlockNeeded ) )
	if ( ERR_OK != eErr )
		{
		m_state = PRIME_ERROR;
		}

	else
		{
		// Get the debounced trigger
		LOG_ERROR_RETURN( eErr = g_pGeneratorMonitor->GetDebouncedTrigger( &bOn ) )
		if ( ERR_OK != eErr || false == bOn )
			{
			LOG_ERROR_RETURN( g_pComOp->HaltPumpAndRF() )
			m_state = PRIME_VAPOR_START;
			return ERR_TRIGGER_OFF;
			}
		}

	switch ( m_state )
		{

		case PRIME_VAPOR_START:
			break;

		case PRIME_VAPOR_TEST:
			// Continue flow of steam
			LOG_ERROR_RETURN( eErr = VaporTest() )
			if ( ERR_OK != eErr )
				m_state = PRIME_ERROR;
			break;

			// Should not get here. Included for completeness
		case PRIME_CONTROLLED_STOP:
		case PRIME_SUCCESS:
		case PRIME_FAILED:
		case PRIME_ERROR:
			LOG_ERROR_RETURN( eErr = g_pComOp->HaltPumpAndRF() )
			break;

		default:
			LOG_ERROR_RETURN( eErr = g_pComOp->HaltPumpAndRF() )
			LOG_ERROR_RETURN( eErr = ERR_ERROR )
			m_state = PRIME_FAILED;
			break;
		}

	// Catch Error now and act to halt operations
	if ( PRIME_ERROR == m_state || PRIME_CONTROLLED_STOP == m_state )
		{
		// Update the vapor ON time on the one-wire
		LOG_ERROR_RETURN( eErr = g_pComOp->HaltPumpAndRF() )
		g_pTreatmentRecord->UpdateTreatmentRecord( DELIVERY_PRIME, m_tRunTicks );
		if ( PRIME_CONTROLLED_STOP == m_state )
			m_state = PRIME_SUCCESS;
		else
			m_state = PRIME_FAILED;
		}

	return eErr;
}

/**
 * @brief	This method returns the latest internal state of the priming operation
 *
 * @return   Status as upErr
 */
CCatheterPrime::PrimeStepEnum CCatheterPrime::GetState() const
{
	return m_state;
}

/**
 * @brief	This method returns the percent completion of the priming operation
 *
 * @param	pbProgress A pointer to receive the percentage as byte*
 *
 * @return   Status as upErr
 */
upErr CCatheterPrime::GetProgress( byte * pbProgress )
{
	tick tDiff;

	GetTickElapsed( m_tStartTick, &tDiff );
	*pbProgress = (byte) ( 100 * tDiff / m_tTotalPrimeTicks );

	if ( *pbProgress > 100 )
		*pbProgress = 100;
	return ERR_OK;
}

// Private Class Functions

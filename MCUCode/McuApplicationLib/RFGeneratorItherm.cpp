/**
 * @file	RFGeneratorItherm.cpp
 * @par		Package: UGMcu
 * @par		Project: Union Generator
 * @par		Copyright (c) 2017, Uptake Medical Technology, Inc.
 * @author	Glen Jul 24, 2012 Created file
 * @brief	This file contains the definitions for CRFGeneratorItherm
 */

// Include Files
#include "UGMcuDefinitions.h"

#include "RFGeneratorItherm.h"
#include "DigitalInputOutput.h"
#include "LogMessage.h"
#include "stdio.h"

// External Public Data
// CRFGenerator * g_pRFGen = 0;

// File Scope Macros and Constants
#define SETTER_RESPONSE_WAIT 		(20)
#define READ_STATUS_RESPONSE_WAIT 	(10)
#define INITIAL_COMM_RESPONSE_WAIT 	(10)

// File Scope Type Definitions (Enums, Structs & Classes)

/**
 * @brief	This enumeration are the bit values of the error word returned from the ITherm
 *
 0 Corrupted configuration structure in EEProm @n
 1 Line voltage too low @n
 2 Line voltage too high @n
 3 Main Power Off @n
 4 No Load Connected @n
 5 Circuit Breaker Tripped (note no CB in Uptake Medical Unit) @n
 6 Control Board Overheat @n
 7 Thermal Fuse 1 Trip @n
 8 Thermal Fuse 2 Trip @n
 9 Thermocouple 1 disconnected @n
 10 Not used, will always be 1 @n
 11 Set power greater than maximum power @n
 12 Invalid Load @n
 13 Not used @n
 14 Not used @n
 15 Generic Error @n
 */
typedef enum upRFGeneratorErrorEnum
{
	ITHERM_ERROR_EEPROM = 1 << 0,
	ITHERM_ERROR_LINE_HIGH = 1 << 1,
	ITHERM_ERROR_LINE_LOW = 1 << 2,
	ITHERM_ERROR_POWER_OFF = 1 << 3,
	ITHERM_ERROR_NO_LOAD = 1 << 4,
	ITHERM_ERROR_BREAKER = 1 << 5,
	ITHERM_ERROR_OVERHEAT = 1 << 6,
	ITHERM_ERROR_FUSE1 = 1 << 7,
	ITHERM_ERROR_FUSE2 = 1 << 8,
	ITHERM_ERROR_NO_THERMO = 1 << 9,
	ITHERM_ERROR_10_NOT_USED = 1 << 10,
	ITHERM_ERROR_SET_OVER_MAX_POWER = 1 << 11,
	ITHERM_ERROR_INVALID_LOAD = 1 << 12,
	ITHERM_ERROR_13_NOT_USED = 1 << 13,
	ITHERM_ERROR_14_NOT_USED = 1 << 14,
	ITHERM_ERROR_GENERIC = 1 << 15,
	ITHERM_ERROR_CNT
} upRFGeneratorError;

/**
 * @brief	This enumeration are the bit values of the status word returned from the ITherm
 *
 Bit Status @n
 0 Running? 0 is not running 1 is running @n
 1,2,3 Mode: 1 = Temperature 2 = not used 3 = Power 4 = Time @n
 4 Greed LED 0 = off 1 = on � Can be ignored @n
 5 Yellow LED 0 = off 1 = on in Power Off � Can be ignored @n
 6 Red LED 0 = off 1 = on � Can be ignored @n
 7 Temperature Units 1 = C 0 =F @n
 8-15 Not Used @n
 */
typedef enum upRFGeneratorStatusEnum
{
	ITHERM_STATUS_RUNNING = 1 << 0,
	ITHERM_STATUS_MODE_0 = 1 << 1,
	ITHERM_STATUS_MODE_1 = 1 << 2,
	ITHERM_STATUS_MODE_2 = 1 << 3,
	ITHERM_STATUS_GLED_UNUSED = 1 << 4,
	ITHERM_STATUS_YLED_UNUSED = 1 << 5,
	ITHERM_STATUS_RLED_UNUSED = 1 << 6,
	ITHERM_STATUS_TEMP_EGU_C = 1 << 7
} upRFGeneratorStatus;

// File Scope Data

// File Scope Functions

// Public Functions

/**
 * @brief	This is the constructor for CRFGenerator.
 *
 * It clears the status values and captures the @a this pointer to g_pRFGen
 * for access as a singleton.
 *
 */
CRFGeneratorItherm::CRFGeneratorItherm()
{
	SLinearLongIntFactors calData;
	CRFGenerator();

	m_eAcPowerUp = GEN_STATE_START;
	m_iAcPowerUpCycle = 0;
	m_statusGetInProgress = false;
	m_powerSetInProgress = false;
	m_bPowerSentData[ 0 ] = 0;
	m_applyingPower = false;
	m_outputEnabled = false;
	m_powerOutput = 0;
	m_acInput = false;
	m_powerOutput = 0;
	m_powerRequest = 0;
	m_bPowerNew = false;
	m_temperature = 0;
	m_statusBytes = 0;
	m_errorBytes = 0;
	m_generatorStatus = ERR_OK;

	// These factors map the given RDO VA power to iTherm power supply wattage. The mapping is 10 VA becomes 10W and 340VA becomes 140W
	calData.ilBoffset = CNVT_REAL_TO_LINEAR_B( 6.0606 );
	calData.ilMslope = CNVT_REAL_TO_LINEAR_M( 0.3939 );
	SetFactors( calData );
	SetPassMask(0xAA);
}

/**
 * @brief	This method sets the default configuration onto the iTherm.
 *
 * This is power mode with a power of zero.
 *
 * @return   Status as upErr
 */
upErr CRFGeneratorItherm::SetDefaultsToGenerator()
{
	upErr eErr = ERR_OK;

	// Set initial configuration
	LOG_ERROR_RETURN( eErr = WriteMode( RF_MODE_POWER ) )

	if ( ERR_OK == eErr )
		{
		LOG_ERROR_RETURN( eErr = WritePowerSetpoint( 0 ) )
		}

	m_generatorStatus = eErr;

	return m_generatorStatus;
}

/**
 * @brief	This method collects the response from the most recent communication with the ITherm.
 *
 * If the iTherm is not powered-on then nothing is done and the response status is ERR_OK. There may be no activity
 * or either a status response or a power set response coming. Each type of actual response is handled by a separate
 * receive function. @n
 * If there is a receive failure on a power response the changed flag is set true to force another power send. Getting
 * status is the default operation and a "re-try" will happen automatically if the packet is bad. The iTherm will be
 * flagged as having failed if COMM_ERR_TOLERANCE receive errors are found. There is no "down counting" for good reponses.
 *
 * @return   Status as upErr
 */
upErr CRFGeneratorItherm::ReadFromHardware()
{
	upErr eErr = ERR_OK;

	// Just exit if AC not ON
	if ( false == m_acInput )
		{
		return eErr;
		}

	// Receive in progress
	if ( m_statusGetInProgress || m_powerSetInProgress )
		{

		// Receiving response to status request
		if ( m_statusGetInProgress )
			{
			LOG_ERROR_RETURN( eErr = ReadStatusResponse() )
			// Set status
			twobytes tbError;
			eErr = GetError( &tbError );
			if ( ERR_OK == eErr && 0 == tbError )
				{
				SetCoilStatus( RF_COIL_ERR_OK );
				}
			else
				{
				SetCoilStatus( RF_COIL_ERROR );
				}
			}

		// Receiving response portion of power set
		else
			{
			LOG_ERROR_RETURN( eErr = ReceiveCommand( 3, m_bPowerSentData ) )
			}

		// If less than COMM_ERR_TOLERANCE errors then clear error
		if ( ERR_OK != eErr )
			{
			m_bCommErr++;
			if ( m_bCommErr < COMM_ERR_TOLERANCE )
				{
				eErr = ERR_OK;

				// If setting power then force another set power. A status get replicates itself
				if ( m_powerSetInProgress )
					m_bPowerNew = true;
				}
			}

		// Reset counter upon success
		else
			{
			m_bCommErr = 0;
			}

		// Clear both flags as only one could have been in progress
		m_statusGetInProgress = false;
		m_powerSetInProgress = false;

		// Set status for RF generator and turn OFF AC power if bad
		if ( ERR_OK != eErr )
			SetACInput( false );
		m_generatorStatus = eErr;
		}

	return m_generatorStatus;
}

/**
 * @brief	This method sends the initial command to the iTherm.
 *
 * It then waits INITIAL_COMM_RESPONSE_WAIT milliseconds for the response. If the response
 * is not correct or there's a timeout then ERR_RF_GEN_FAIL is returned.
 *
 * @return   Status as upErr
 */
upErr CRFGeneratorItherm::SendInitialCommand()
{
	upErr eErr = ERR_OK;
	byte bCommand[ 10 ];
	twobytes tbGot;

	bCommand[ 0 ] = 'o';
	LOG_ERROR_RETURN( eErr = FlushReadBuffer( RS232_RF_GEN ) )
	eErr = WritePort( RS232_RF_GEN, 1, bCommand );
	if ( ERR_OK == eErr )
		{
		eErr = ReadPort( RS232_RF_GEN, 1, bCommand, &tbGot, INITIAL_COMM_RESPONSE_WAIT );
		if ( ERR_OK == eErr )
			{
			if ( tbGot != 1 || bCommand[ 0 ] != '!' )
				eErr = ERR_RF_GEN_COMM_FATAL;
			}
		}
	return eErr;
}

/**
 * @brief	This method performs port initialization and local parameter set-up.
 *
 * The port is configured to 115200 BAUD, 1 stop bit and no parity. The power levels for the sections of the
 * priming and treatment are assigned here to use data from CTHerapyInformation which may not have
 * been instantiated previously.
 *
 * @return   Status as upErr
 */
upErr CRFGeneratorItherm::InitializeHardware()
{
	upErr eErr = ERR_OK;

	// Set up port
	LOG_ERROR_RETURN( eErr = InitializePort( RS232_RF_GEN, RS232_BAUD_115200, RS232_PARITY_NONE, 8, 1 ) )

	m_generatorStatus = eErr;

	return m_generatorStatus;
}

/**
 * @brief	This method controls iTherm power-up communication and normal running communication
 * 			configuration to the ITherm.
 *
 *
 * @return   Status as upErr
 */
upErr CRFGeneratorItherm::WriteToHardware()
{
	upErr eErr = ERR_OK;

	// Exit if not ON
	if ( false == m_acInput )
		{
		return ERR_OK;
		}

	// Power-up state machine
	// Wait a RF_GEN_POWER_UP_CYCLE_COUNT cycles for iTherm CPU to potentially be alive
	if ( GEN_STATE_START == m_eAcPowerUp )
		{
		m_iAcPowerUpCycle++;
		if ( RF_GEN_POWER_UP_CYCLE_COUNT <= m_iAcPowerUpCycle )
			{
			m_eAcPowerUp = GEN_STATE_FIRST_ACTION;
			m_iAcPowerUpCycle = 0;
			}
		}

	// Send the "init" command a few times and proceed once it's received
	else if ( GEN_STATE_FIRST_ACTION == m_eAcPowerUp )
		{
		m_iAcPowerUpCycle++;
		if ( ERR_OK == SendInitialCommand() )
			{
			m_eAcPowerUp = GEN_STATE_SECOND_ACTION;
			m_iAcPowerUpCycle = 0;
			}
		// iTherm didn't respond in time. Fail and turn it off
		else if ( m_iAcPowerUpCycle > RF_GEN_POWER_UP_CYCLE_MAX )
			{
			SetACInput( false );
			m_eAcPowerUp = GEN_STATE_ERROR;
			LOG_ERROR_RETURN( eErr = ERR_RF_GEN_NOT_FOUND )
			m_generatorStatus = eErr;
			}
		}

	// iTherm responded so send default configuration
	else if ( GEN_STATE_SECOND_ACTION == m_eAcPowerUp )
		{
		m_iAcPowerUpCycle++;
		LOG_ERROR_RETURN( eErr = SetDefaultsToGenerator() )
		if ( ERR_OK == eErr )
			{
			m_eAcPowerUp = GEN_STATE_COMPLETE;
			}
		else if ( m_iAcPowerUpCycle >= COMM_ERR_TOLERANCE )
			{
			m_eAcPowerUp = GEN_STATE_ERROR;
			SetACInput( false );
			LOG_ERROR_RETURN( eErr = ERR_RF_GEN_COMM_FATAL )
			m_generatorStatus = eErr;
			}
		}

	// Normal operations
	else if ( GEN_STATE_COMPLETE == m_eAcPowerUp )
		{
		m_generatorStatus = RunningCommunication();
		} // GEN_STATE_COMPLETE

	return m_generatorStatus;
}

/**
 * @brief	This method either initiates a set of a new power level to the iTherm or a get of the iTherm's status.
 *
 * 			If there has been a new level requested, it is sent. Without new information, it will use the Pass Mask system
 * 			to initiate status reads. The command or status response will be picked-up in the next read operation.
 * 			Since reads happen first in the round robin scheme this will happen on the next round robin loop execution.
 * 			This allows the iTherm almost 50ms to reply before the code loERR_OKs for a response.
 *
 * @return   Status as upErr
 */
upErr CRFGeneratorItherm::RunningCommunication()
{
	upErr eErr = ERR_OK;

	// Alternate between setting power and getting status
	if ( false == m_outputEnabled )
		{
		LOG_ERROR_RETURN( eErr = SetPowerRequest( 0 ) )
		}

	// Start a send of power if changed
	if ( m_bPowerNew && GetPassEnabled() )
		{
		m_generatorStatus = StartWritePowerSetpoint( m_powerRequest );
		m_bPowerNew = false;
		}

	// Otherwise start a read of status
	else
		{
		LOG_ERROR_RETURN( eErr = StartStatusGetter() )
		if ( ERR_OK == eErr )
			m_statusGetInProgress = true;
		else
			m_generatorStatus = eErr;
		}

	if ( ERR_OK != m_generatorStatus )
		{
		SetOutputEnable( false );
		}

	// Turn ON if status is good

	else if ( m_outputEnabled )
		{
		LOG_ERROR_RETURN( eErr = g_pDigitalInputOutput->SetOutput( DIG_OUT_RF_ENABLE, true ) )
		}

	return eErr;
}

// Private Class Functions

/**
 * @brief	This method turn the RF generator ON or OFF.
 *
 * @param	bOn	Flag of whether the generator should go ON (true) or OFF (false)
 *
 * @return   Status as upErr
 */
upErr CRFGeneratorItherm::SetOutputEnable( bool_c bOn )
{
	upErr eErr = ERR_OK;

	if ( false == bOn )
		{
		LOG_ERROR_RETURN ( eErr = g_pDigitalInputOutput->SetOutput( DIG_OUT_RF_ENABLE, false ) )
		}

	m_outputEnabled = bOn;
	return eErr;
}

/**
 * @brief	This method sends a command to the ITherm. It computes the checksum for the outgoing
 * 			packet and send the completed packet. The outgoing packet is copied into the bSentPacket
 * 			data area so a comparison of sent and received packets can be made on setters
 *
 * @param	pbBuf	A pointer to a byte buffer that contains the set command
 * @param	bCnt	The length of the packet
 * @param	bSentPacket A pointer to a buffer to use for the send data.
 *
 * @return   Status as upErr
 */
upErr CRFGeneratorItherm::SendCommand( byte *pbBuf, byte bCnt, byte * bSentPacket )
{
	upErr eErr = ERR_OK;
	byte bCheckSum = 0;
	byte bIndx;

	if ( 0 == pbBuf || 0 == bCnt || bCnt + 1 > RF_SEND_BYTE_MAX )
		return ERR_BAD_PARAM;

	// Copy the packet and add the computed checksum
	for ( bIndx = 0; bIndx < bCnt; bIndx++ )
		{
		bSentPacket[ bIndx ] = pbBuf[ bIndx ];
		bCheckSum += pbBuf[ bIndx ];
		}
	bSentPacket[ bIndx++ ] = bCheckSum;

	// Write the packet
	LOG_ERROR_RETURN( eErr = FlushReadBuffer( RS232_RF_GEN ) )
	LOG_ERROR_RETURN( eErr = WritePort( RS232_RF_GEN, (int) bIndx, bSentPacket ) )
	return eErr;
}

/**
 * @brief	This method reads a response from the serial port and checks it for correctness
 *
 * @param	bCnt The count of bytes i the original out-going packet as a byte
 * @param	bSentPacket A pointer to the sent packet as a byte*
 *
 * @return   Status as upErr
 */
upErr CRFGeneratorItherm::ReceiveCommand( byte bCnt, byte *bSentPacket )
{
	byte bRecvPacket[ RF_RECV_BYTE_MAX ];
	byte bIndx;
	twobytes tbRecv;
	upErr eErr;

	bCnt++; // Account for checksum that should come back

	// Get the response
	LOG_ERROR_RETURN( eErr = ReadPort( RS232_RF_GEN, bCnt, bRecvPacket, &tbRecv, SETTER_RESPONSE_WAIT) )

	// Check for correct response size
	if ( ERR_OK == eErr )
		{
		if ( (byte) ( tbRecv & 0xFF ) != bCnt )
			LOG_ERROR_RETURN( eErr = ERR_RF_GEN_BAD_RESP )
		else
			{

			// The packets should be identical if the set was correct
			for ( bIndx = 0; bIndx < bCnt - 1; bIndx++ )
				{
				if ( bSentPacket[ bIndx ] != bRecvPacket[ bIndx ] )
					LOG_ERROR_RETURN( eErr = ERR_RF_GEN_BAD_RESP )
				}
			// Last byte is checksum and identify that error
			if ( ERR_OK == eErr && bSentPacket[ bIndx ] != bRecvPacket[ bIndx ] )
				LOG_ERROR_RETURN( eErr = ERR_RF_GEN_BAD_CHCK )
			}
		}

	// Get partial data
	else
		{
		char sTemp[ 40 ];
		ReadPort( RS232_RF_GEN, RF_RECV_BYTE_MAX, bRecvPacket, &tbRecv, 0 );
		snprintf( sTemp, 40, "Bad iTherm read Set had %u char\n", tbRecv );
		g_pLogMessage->LogMessage( sTemp );
		}

	return eErr;
}

/**
 * @brief	This method retrieves the response following a getter command to the ITherm.
 * 			Only the information payload is returned to the caller. It will wait a specific
 * 			time before giving up
 *
 * @param	pbGetBuf	A pointer to the byte buffer that will receive the payload
 * @param 	bCmd		The command byte of the command
 * @param	bGetCnt		The count of bytes that should be in the payload
 * @param	bMsTimeout	The time to wait for a packet in milliseconds
 *
 * @return   Status as upErr
 */
upErr CRFGeneratorItherm::ReceiveGetter( byte *pbGetBuf, byte bCmd, byte bGetCnt, byte bMsTimeout )
{
	upErr eErr = ERR_OK;
	byte bRecvPacket[ RF_RECV_BYTE_MAX ];
	byte bCheckSum;
	byte bIndx;
	twobytes tbRecv;
	byte bRecv;

	// Read the packet
	LOG_ERROR_RETURN( eErr = ReadPort( RS232_RF_GEN, bGetCnt + 3, bRecvPacket, &tbRecv, bMsTimeout ) );
	bRecv = (byte) ( tbRecv & 0xFF ); // Response will be less than 256 bytes

	if ( ERR_OK == eErr )
		{

		// Return if command byte does not match expected
		if ( bRecvPacket[ 0 ] != bCmd )
			{
			LOG_ERROR_RETURN( eErr = ERR_RF_GEN_BAD_RESP )
			}

		// Return if count does not match expected
		else if ( bGetCnt + 3 != bRecv )
			{
			LOG_ERROR_RETURN( eErr = ERR_RF_GEN_BAD_RESP )
			}

		// Count in packet does not match expected
		else if ( bRecvPacket[ 1 ] != bGetCnt + 1 )
			{
			LOG_ERROR_RETURN( eErr = ERR_RF_GEN_BAD_RESP )
			}

		// Test Checksum
		else
			{
			bCheckSum = bRecvPacket[ 0 ] + bRecvPacket[ 1 ]; // add in command byte and count byte
			for ( bIndx = 2; bIndx < bRecv - 1; bIndx++ )
				{
				pbGetBuf[ bIndx - 2 ] = bRecvPacket[ bIndx ];
				bCheckSum += bRecvPacket[ bIndx ];
				}

			if ( bCheckSum != bRecvPacket[ bRecv - 1 ] )
				{
				LOG_ERROR_RETURN( eErr = ERR_RF_GEN_BAD_CHCK )
				}
			}
		}

	// Get partial data
	else
		{
		char sTemp[ 40 ];
		ReadPort( RS232_RF_GEN, RF_RECV_BYTE_MAX, bRecvPacket, &tbRecv, 0 );
		snprintf( sTemp, 40, "Bad iTherm read Get had %u char\n", tbRecv );
		g_pLogMessage->LogMessage( sTemp );
		}

	return eErr;
}

/**
 * @brief	This method executes a get command with the ITherm. It adds the checksum to the request and
 * 			then processes returned packet. The packet is checked for the correct byte count and checksum.
 * 			Only the information payload is returned to the caller.
 *
 * @param	pbOutBuf	A pointer to the outgoing request without the checksum
 * @param	bOutCnt		The count of bytes in the request
 * @param	pbGetBuf	A pointer to the byte buffer that will receive the payload
 * @param	bGetCnt		The count of bytes that should be in the payload
 *
 * @return   Status as upErr
 */
upErr CRFGeneratorItherm::PerformGetter( byte *pbOutBuf, byte bOutCnt, byte *pbGetBuf, byte bGetCnt )
{
	byte bSentPacket[ RF_SEND_BYTE_MAX ];
	upErr eErr;

	// Send the command
	LOG_ERROR_RETURN( eErr = SendCommand( pbOutBuf, bOutCnt, bSentPacket ) )

	// Get response
	if ( ERR_OK == eErr )
		LOG_ERROR_RETURN( eErr = ReceiveGetter( pbGetBuf, pbOutBuf[0], bGetCnt, 20 ) )

	return eErr;
}

/**
 * @brief	This method write a set command to the ITherm. It computes the checksum for the outgoing
 * 			packet and determines if the response was correct.
 *
 * @param	pbBuf	A pointer to a byte buffer that contains the set command
 * @param	bCnt	The length of the packet
 *
 * @return   Status as upErr
 */
upErr CRFGeneratorItherm::PerformSetter( byte *pbBuf, byte bCnt )
{
	byte bSentPacket[ RF_SEND_BYTE_MAX ];
	upErr eErr;

	// Send the command
	LOG_ERROR_RETURN( eErr = SendCommand( pbBuf, bCnt, bSentPacket ) )
	if ( ERR_OK == eErr )
		{
		eErr = ReceiveCommand( bCnt, bSentPacket );
		}

	return eErr;
}

/**
 * @brief	This method sends a operating mode to the ITherm using PerformSetter()
 *
 * @param	eMode The mode as upRFGeneratorMode
 *
 * @return   Status as upErr
 */
upErr CRFGeneratorItherm::WriteMode( upRFGeneratorMode eMode )
{
	upErr eErr;
	byte bSetData[ RF_SEND_BYTE_MAX ];

	switch ( eMode )
		{
		case RF_MODE_POWER:
			bSetData[ 0 ] = 'D';
			break;
		case RF_MODE_TEMP:
			bSetData[ 0 ] = 'j';
			break;
		case RF_MODE_TIME:
			bSetData[ 0 ] = 'k';
			break;
		default:
			return ERR_ENUM_OUT_OF_RANGE;
		}

	LOG_ERROR_RETURN( eErr = PerformSetter( bSetData, 1 ) )

	return eErr;
}

/**
 * @brief	This method sends a power setpoint to the ITherm using PerformSetter()
 *
 * @param	tbPower The setpoint in integer Watts
 *
 * @return   Status as upErr
 */
upErr CRFGeneratorItherm::WritePowerSetpoint( twobytes tbPower )
{
	upErr eErr;
	byte bSetData[ RF_SEND_BYTE_MAX ];

	m_powerRequest = tbPower;
	if ( tbPower > 0 )
		eErr = ApplyCal( tbPower, &m_appliedPowerRequest );
	else
		m_appliedPowerRequest = 0;

	bSetData[ 0 ] = 'A';
	bSetData[ 1 ] = ( m_appliedPowerRequest & 0xFF );
	bSetData[ 2 ] = ( ( m_appliedPowerRequest >> 8 ) & 0xFF );
	LOG_ERROR_RETURN( eErr = PerformSetter( bSetData, 3 ) )

	return eErr;
}

/**
 * @brief	This method initiates the send of a two-part write of a power setpoint to the ITherm.
 *
 * 			It is sent using SendCommand() and the response must be processed separately using ReceiveCommand().
 * 			Prior to snding the value it is calibrated using ApplyCal(). The calibrated value is saved as a member
 * 			variable for later access.
 *
 * @param	tbPower The setpoint in integer Watts
 *
 * @return   Status as upErr
 */
upErr CRFGeneratorItherm::StartWritePowerSetpoint( twobytes tbPower )
{
	upErr eErr;
	byte bSetData[ RF_SEND_BYTE_MAX ];

	m_powerRequest = tbPower;
	if ( tbPower > 0 )
		eErr = ApplyCal( tbPower, &m_appliedPowerRequest );
	else
		m_appliedPowerRequest = 0;

	bSetData[ 0 ] = 'A';
	bSetData[ 1 ] = ( m_appliedPowerRequest & 0xFF );
	bSetData[ 2 ] = ( ( m_appliedPowerRequest >> 8 ) & 0xFF );

	// Send the command
	LOG_ERROR_RETURN( eErr = SendCommand( bSetData, 3, m_bPowerSentData ) )
	m_powerSetInProgress = ( ERR_OK == eErr ? true : false );

	return eErr;
}

/**
 * @brief	This method start a status get from the iTherm. It must be followed by a call to ReadStatusResponse() to
 * 			have the returned packet removed from the port buffer and decomposed into data fields.
 *
 * @return   Status as upErr
 */
upErr CRFGeneratorItherm::StartStatusGetter()
{
	upErr eErr;
	byte bSetData[ RF_SEND_BYTE_MAX ];

	// Execute get of status
	bSetData[ 0 ] = 'p';
	LOG_ERROR_RETURN( eErr = SendCommand( bSetData, 1, bSetData ) )
	m_statusGetInProgress = ( ERR_OK == eErr ? true : false );

	return eErr;
}

/**
 * @brief	This method gets the status structure response from the ITherm and breaks out the individual fields.
 * 			The components are available as individual get* calls. Errors are set into m_generatorStatus.
 *
 * @return   Status as upErr
 */
upErr CRFGeneratorItherm::ReadStatusResponse()
{
	upErr eErr = ERR_OK;
	byte bGetData[ RF_RECV_BYTE_MAX ];

	// Execute get of status
	LOG_ERROR_RETURN( eErr = ReceiveGetter( bGetData, 'p', 12, READ_STATUS_RESPONSE_WAIT ) )

	// Decompose packet into data elements
	if ( ERR_OK == eErr )
		{
		m_temperature = ( 256 * (int) bGetData[ 1 ] + (int) bGetData[ 0 ] ) / 4;
		m_powerOutput = ( 256 * (twobytes) bGetData[ 3 ] + (twobytes) bGetData[ 2 ] );
		m_statusBytes = bGetData[ 9 ];
		m_statusBytes <<= 8;
		m_statusBytes |= bGetData[ 8 ];
		m_applyingPower = ( ( bGetData[ 8 ] & ITHERM_STATUS_RUNNING ) ? true : false );
		m_errorBytes = bGetData[ 11 ];
		m_errorBytes <<= 8;
		m_errorBytes |= bGetData[ 10 ];
		m_errorBytes &= ~( ITHERM_ERROR_13_NOT_USED | ITHERM_ERROR_14_NOT_USED | ITHERM_ERROR_10_NOT_USED );
		m_errorBytes = m_errorBytes & ( ITHERM_ERROR_NO_LOAD | ITHERM_ERROR_INVALID_LOAD );
		}

	return eErr;
}

/**
 * @brief	This method returns the flag giving the model of RF generator in use
 *
 * @param	peModel	A pointer of upRFGeneratorModel to receive the model flag in use
 *
 * @return   Status as upErr
 */
upErr CRFGeneratorItherm::GetRfGenModel( upRFGeneratorModel * peModel )
{
	*peModel = RF_MODEL_ITHERM;
	return ERR_OK;
}

/**
 * @file	LogMessage.cpp
 * @par		Package: UGMcu
 * @par		Project: Union Generator
 * @par		Copyright (c) 2017, Uptake Medical Technology, Inc.
 * @author	Glen Oct 5, 2010 Created file
 * @brief	This file contains the definitions for CLogMessage
 */

// Include Files
#include "LogMessage.h"
#include "string.h"
#include "stdio.h"
#include "SbcIntfc.h"
#include "DigitalInputOutput.h"

// External Public Data

// File Scope Macros and Constants

// File Scope Type Definitions (Enums, Structs & Classes)

// File Scope Data
CLogMessage * g_pLogMessage = 0;

// File Scope Functions

// Public Functions

/**
 * @brief	This method prints an error log string if the error value is not ERR_OK.
 *
 * 			The sFile argument is a pointer into FLASH that is expected to contain the file name.
 * 			This method is called by the LOG_ERROR_RETURN() macro throughout the code.
 *
 * @param	x An error code as upErr
 * @param	sFile A string that is a pointer into FLASH as const char*
 * @param	iLine The line number of the error as int
 *
 * @return   Status as upErr
 */
void LogErrorRet( upErr x, const char * sFile, int iLine )
{
#define STEMP_LEN	(50)
	char sTemp[ STEMP_LEN ];

	if ( ERR_OK != x && g_pLogMessage )
		{
		strncpy( sTemp, sFile, STEMP_LEN );
		sTemp[ STEMP_LEN - 1 ] = 0;
		g_pLogMessage->LogErrorMessage( x, sTemp, iLine );
		}
}

/**
 * @brief	This is the constructor for LogMessage. It captures the @a this handle to use in CLogMessageAccess
 *
 */
CLogMessage::CLogMessage()
{
	g_pLogMessage = this;
	m_msgCnt = 0;
	m_msgHead = 0;
	m_userMsgCnt = 0;
	m_userMsgHead = 0;
}

/**
 * @brief	This method adds a message to the queue. The message is time and tick stamped.
 *
 *
 * @param	sMsg	A const char pointer to the string to log
 *
 * @return   Status as upErr
 */
upErr CLogMessage::LogMessage( const char *sMsg )
{
	byte bEntry;
	byte bLen;
	upErr eRet;

	eRet = GetNextFreeLogEntry( &bEntry );
	if ( ERR_OK == eRet )
		{
		eRet = GetTimeTickStr( m_errMsgs[ bEntry ], &bLen );
		if ( ERR_OK == eRet )
			{
			byte bMsgLen = strlen( sMsg );
			strncpy( m_errMsgs[ bEntry ] + bLen, sMsg, LOG_MESSAGE_LENGTH - 1 - bLen );
			if ( bLen + bMsgLen < LOG_MESSAGE_LENGTH - 2 )
				{
				m_errMsgs[ bEntry ][ bLen + bMsgLen ] = '\n';
				m_errMsgs[ bEntry ][ bLen + bMsgLen + 1 ] = 0;
				}
			}
		m_errMsgs[ bEntry ][ LOG_MESSAGE_LENGTH - 1 ] = 0;
		}
	return eRet;
}

/**
 * @brief	This method adds a message with error code to the queue. The message is time and tick stamped.
 * 			It is sent to the SBC if that is enabled
 *
 * @param	eErr	An error code
 * @param	sMsg	A const char pointer to the string to log
 *
 * @return   Status as upErr
 */
upErr CLogMessage::LogErrorMessage( upErr eErr, const char *sMsg )
{
	byte bEntry;
	byte bLen = 0;
	upErr eRet;

	CreateAndAddUserMsg( eErr, 0 );
	eRet = GetNextFreeLogEntry( &bEntry );
	if ( ERR_OK == eRet )
		{
		eRet = GetTimeTickErrStr( m_errMsgs[ bEntry ], eErr, &bLen );
		if ( ERR_OK == eRet )
			{
			byte bMsgLen = strlen( sMsg );
			strncpy( m_errMsgs[ bEntry ] + bLen, sMsg, LOG_MESSAGE_LENGTH - 1 - bLen );
			if ( bLen + bMsgLen < LOG_MESSAGE_LENGTH - 2 )
				{
				m_errMsgs[ bEntry ][ bLen + bMsgLen ] = '\n';
				m_errMsgs[ bEntry ][ bLen + bMsgLen + 1 ] = 0;
				}
			}
		m_errMsgs[ bEntry ][ LOG_MESSAGE_LENGTH - 1 ] = 0;
		}
	return eRet;
}

/**
 * @brief	This method adds a message and integer labeled "Line" with error code to the queue. The message is time and tick stamped.
 * 			It is sent to the SBC if that is enabled.
 *
 * @param	eErr	An error code
 * @param	sMsg	A const char pointer to the string to log
 * @param	iLine   An integer to add to the log
 *
 * @return   Status as upErr
 */
upErr CLogMessage::LogErrorMessage( upErr eErr, const char *sMsg, int iLine )
{
	byte bEntry;
	byte bLen = 0;
	upErr eRet;

	CreateAndAddUserMsg( eErr, 0 );
	eRet = GetNextFreeLogEntry( &bEntry );
	if ( ERR_OK == eRet )
		{
		eRet = GetTimeTickErrStr( m_errMsgs[ bEntry ], eErr, &bLen );
		if ( ERR_OK == eRet )
			{
			strncpy( m_errMsgs[ bEntry ] + bLen, sMsg, LOG_MESSAGE_LENGTH - bLen );
			bLen = strlen( m_errMsgs[ bEntry ] );
			if ( bLen < LOG_MESSAGE_LENGTH )
				{
				snprintf( m_errMsgs[ bEntry ] + bLen, LOG_MESSAGE_LENGTH - bLen, " Line: %d\n", iLine );
				}
			}
		m_errMsgs[ bEntry ][ LOG_MESSAGE_LENGTH - 1 ] = 0;
		}
	return eRet;
}

/**
 * @brief	This method adds a message and integer with error code to the queue. The message is time and tick stamped.
 * 			It is sent to the SBC if that is enabled.
 *
 * @param	eErr	An error code
 * @param	sMsg	A const char pointer to the string to log
 * @param	iVal   An integer to add to the log
 *
 * @return   Status as upErr
 */
upErr CLogMessage::LogErrorMessageInt( upErr eErr, const char *sMsg, int iVal )
{
	byte bEntry;
	byte bLen = 0;
	upErr eRet;

	CreateAndAddUserMsg( eErr, 0 );
	eRet = GetNextFreeLogEntry( &bEntry );
	if ( ERR_OK == eRet )
		{
		eRet = GetTimeTickErrStr( m_errMsgs[ bEntry ], eErr, &bLen );
		if ( ERR_OK == eRet )
			{
			strncpy( m_errMsgs[ bEntry ] + bLen, sMsg, LOG_MESSAGE_LENGTH - bLen );
			bLen = strlen( m_errMsgs[ bEntry ] );
			if ( bLen < LOG_MESSAGE_LENGTH )
				{
				snprintf( m_errMsgs[ bEntry ] + bLen, LOG_MESSAGE_LENGTH - bLen, " Value: %d\n", iVal );
				}
			}
		m_errMsgs[ bEntry ][ LOG_MESSAGE_LENGTH - 1 ] = 0;
		}
	return eRet;
}

/**
 * @brief	This method adds an error code to the queue along with the file name, function name and line number.
 * 			The message is time and tick stamped. It is sent to the SBC if that is enabled.
 *
 * @param	eErr	An error code
 * @param	sMsg1	A const char pointer to the first string to log
 * @param	sMsg2	A const char pointer to the second string to log
 * @param	iLine   An integer to add to the log
 *
 * @return   Status as upErr
 */
upErr CLogMessage::LogErrorMessage( upErr eErr, const char *sMsg1, const char *sMsg2, int iLine )
{
	char * psLogErrFmt = (char *) "%s : %s Line: %04d\n";
	char sLogErrorTemp[ LOG_MESSAGE_LENGTH ];

	snprintf( sLogErrorTemp, LOG_MESSAGE_LENGTH, psLogErrFmt, sMsg1, sMsg2, iLine );
	return LogErrorMessage( eErr, sLogErrorTemp );
}

/**
 * @brief	This method adds a two-string message with error code to the queue. The message is time and tick stamped.
 * 			It is sent to the SBC if that is enabled.
 *
 * @param	eErr	An error code
 * @param	sMsg1	A const char pointer to the first string to log
 * @param	sMsg2	A const char pointer to the second string to log
 *
 * @return   Status as upErr
 */
upErr CLogMessage::LogErrorMessage( upErr eErr, const char *sMsg1, const char *sMsg2 )
{
	byte bEntry;
	byte bLen = 0;
	upErr eRet;

	CreateAndAddUserMsg( eErr, 0 );
	eRet = GetNextFreeLogEntry( &bEntry );
	if ( ERR_OK == eRet )
		{
		eRet = GetTimeTickErrStr( m_errMsgs[ bEntry ], eErr, &bLen );
		if ( ERR_OK == eRet )
			{
			strncpy( m_errMsgs[ bEntry ] + bLen, sMsg1, LOG_MESSAGE_LENGTH - bLen );
			bLen = strlen( m_errMsgs[ bEntry ] );
			if ( bLen < LOG_MESSAGE_LENGTH )
				{
				strncpy( m_errMsgs[ bEntry ] + bLen, sMsg2, LOG_MESSAGE_LENGTH - bLen );
				}
			}
		m_errMsgs[ bEntry ][ LOG_MESSAGE_LENGTH - 1 ] = 0;
		}
	return eRet;
}

/**
 * @brief	This method dequeues the oldest error message from the
 * 		    log message FIFO queue.
 *
 * @return   Status as upErr
 */
upErr CLogMessage::PopLogMessage()
{
	upErr eRet = ERR_OK;

	if ( 0 < m_msgCnt )
		{
		m_msgHead++;
		m_msgCnt--;
		if ( m_msgHead >= LOG_MESSAGE_CNT )
			m_msgHead = 0;
		}

	return eRet;
}

/**
 * @brief	This method peeks at the oldest error message from the
 *    		log message FIFO queue.
 *
 * @param	pStr 	A pointer to a char array to receive the data. Is expected to be LOG_MESSAGE_LENGTH long
 * @param	pbLen 	A pointer to a byte to receive the length of the resultant string
 *
 * @return   Status as upErr
 */
upErr CLogMessage::PeekLogMessage( char *pStr, byte *pbLen )
{
	upErr eRet = ERR_OK;
	byte bLen;

	*pbLen = 0;
	if ( 0 < m_msgCnt )
		{
		bLen = strlen( m_errMsgs[ m_msgHead ] );
		if ( bLen <= LOG_MESSAGE_LENGTH )
			{
			strncpy( pStr, m_errMsgs[ m_msgHead ], LOG_MESSAGE_LENGTH );
			*pbLen = bLen;
			}
		}

	return eRet;
}

// Private Class Functions

/**
 * @brief	This method gets the index of the next free entry in the circular buffer of messages
 *
 * @param	pbEntry A pointer to a byte that will receive the entry index. Zero if not free
 *
 * @return   Status as upErr. Error if no free entry
 */
upErr CLogMessage::GetNextFreeLogEntry( byte *pbEntry )
{
	byte bNext;
	upErr eErr;

	bNext = m_msgCnt + 1;
	if ( bNext >= LOG_MESSAGE_CNT )
		{
		eErr = ERR_LOG_QUEUE_FULL;
		*pbEntry = 0;
		}
	else
		{
		eErr = ERR_OK;
		m_msgCnt = bNext;
		bNext = bNext + m_msgHead - 1;
		if ( bNext >= LOG_MESSAGE_CNT )
			bNext -= LOG_MESSAGE_CNT;
		*pbEntry = bNext;
		}

	return eErr;
}

/**
 * @brief	This method gets the index of the next free entry in the circular buffer of messages
 *
 * @param	pbEntry A pointer to a byte that will receive the entry index. Zero if not free
 *
 * @return   Status as upErr. Error if no free entry
 */
upErr CLogMessage::GetNextFreeUserMsgEntry( byte *pbEntry )
{
	byte bNext;
	upErr eErr;

	bNext = m_userMsgCnt + 1;
	if ( bNext >= USER_MSG_MAX_CNT )
		{
		eErr = ERR_USER_MSG_QUEUE_FULL;
		*pbEntry = 0;
		}
	else
		{
		eErr = ERR_OK;
		m_userMsgCnt = bNext;
		bNext = bNext + m_userMsgHead - 1;
		if ( bNext >= USER_MSG_MAX_CNT )
			bNext -= USER_MSG_MAX_CNT;
		*pbEntry = bNext;
		}

	return eErr;
}

/**
 * @brief	This method gets the number of log messages waiting in the log queue
 *
 * @param	pbLogCount A pointer to a byte that will receive the number of log messages
 * 					in the queue.
 *
 * @return   Status as upErr.
 */
upErr CLogMessage::GetLogCount( byte *pbLogCount )
{
	upErr eErr = ERR_OK;
	*pbLogCount = m_msgCnt;
	return eErr;
}

/**
 * @brief	This method gets the number of log messages waiting in the log queue
 *
 * @param	pbUserMsgCount A pointer to a byte that will receive the number of log messages
 * 					in the queue.
 *
 * @return   Status as upErr.
 */
upErr CLogMessage::GetUserMsgCount( byte *pbUserMsgCount )
{
	upErr eErr = ERR_OK;
	*pbUserMsgCount = m_userMsgCnt;
	return eErr;
}

/**
 * @brief	This method formats the current time and tick as a string
 *
 * @param	pStr 	A pointer to a char array to receive the data. Is expected to be LOG_MESSAGE_LENGTH long
 * @param	pbLen 	A pointer to a byte to receive the length of the resultant string
 *
 * @return   Status as upErr
 */
upErr CLogMessage::GetTimeTickStr( char *pStr, byte *pbLen )
{
	time tTime;
	tick tTick;

	GetTime( &tTime );
	GetTick( &tTick );

	snprintf( pStr, LOG_MESSAGE_LENGTH, "%4lu : %05u ", tTime, (unsigned int) ( tTick & 0xFFFF ) );
	*pbLen = strlen( pStr );
	if ( *pbLen > LOG_MESSAGE_LENGTH )
		return ERR_ERROR;

	return ERR_OK;
}

/**
 * @brief	This method builds a string with the time, tick and error code
 *
 * @param	pStr 	A pointer to a char array to receive the data. Is expected to be LOG_MESSAGE_LENGTH long
 * @param	eErr 	An error code from upErr
 * @param	pbLen 	A pointer to a byte to receive the length of the resultant string
 *
 * @return   Status as upErr
 */
upErr CLogMessage::GetTimeTickErrStr( char *pStr, upErr eErr, byte *pbLen )
{
	byte bLen;
	upErr eRet;

	eRet = GetTimeTickStr( pStr, &bLen );
	*pbLen = 0;
	if ( ERR_OK == eRet )
		{
		snprintf( pStr + bLen, LOG_MESSAGE_LENGTH - bLen, "Err: %04u ", (twobytes) eErr );
		bLen = strlen( pStr );
		if ( LOG_MESSAGE_LENGTH < bLen )
			{
			eRet = ERR_ERROR;
			bLen = LOG_MESSAGE_LENGTH - 1;
			}
		*( pStr + bLen ) = 0;
		*pbLen = bLen;
		}
	return eRet;
}

/**
 * @brief	This method dequeues the next user message from the FIFO queue
 *
 * @return  Status as upErr
 */
upErr CLogMessage::PopUserMessage()
{
	upErr eErr = ERR_OK;

	if ( 0 == m_userMsgCnt )
		{
		return ERR_USER_MSG_QUEUE_EMPTY;
		}
	if ( m_userMsgCnt > 0 )
		{
		m_userMsgHead++;
		m_userMsgCnt--;
		}
	if ( m_userMsgHead >= USER_MSG_MAX_CNT )
		m_userMsgHead = 0;

	return eErr;
}

/**
 * @brief	This method peeks at the next user message in the FIFO queue
 *
 * @param	psMsg A pointer to store the user message
 *
 * @return  Status as upErr
 */
upErr CLogMessage::PeekUserMessage( SUserMsg * psMsg )
{
	upErr eErr = ERR_OK;

	if ( 0 == m_userMsgCnt )
		{
		psMsg->code = ERR_OK;
		return ERR_USER_MSG_QUEUE_EMPTY;
		}
	if ( m_userMsgCnt > 0 )
		{
		*psMsg = m_userMsgQueue[ m_userMsgHead ];
		}

	return eErr;
}

/**
 * @brief	This method returns true if there is a duplicate
 *    		user message type waiting in the queue
 *
 * @param   eErr The user message type to compare
 *
 * @return  bool_c true: a match was found, else false
 */
bool_c CLogMessage::CheckForDuplicates( upErr eErr )
{
	bool_c bMatch = false;
	int i = 0;
	int endCountTmp = m_userMsgCnt;

	// In typical operation there will be at most 2 user messages
	// in the queue so a linear search should not be resource intensive
	for ( i = m_userMsgHead; endCountTmp > 0 && i < USER_MSG_MAX_CNT; i++ )
		{
		endCountTmp--;
		if ( eErr == (upErr) m_userMsgQueue[ i ].code )
			{
			bMatch = true;
			endCountTmp = 0;
			}
		}

	// Roll-over if necessary
	if ( false == bMatch && endCountTmp > 0 )
		{
		for ( i = 0; endCountTmp > 0; i++ )
			{
			endCountTmp--;
			if ( eErr == (upErr) m_userMsgQueue[ i ].code )
				{
				bMatch = true;
				endCountTmp = 0;
				}
			}
		}

	return bMatch;
}

/**
 * @brief	This method adds a user message to the local
 *    		LogMessage user message queue.
 *
 * If the queue is full, the message is thrown away.  If there
 * is a duplicate user message type in the queue
 * already, the message isn't added.
 *
 * @param   eErr The error code to create the user message from
 * @param	iValue A single value to add to the message
 *
 * @return  Status of result as upErr
 */
upErr CLogMessage::AddUserMessage( upErr eErr, int iValue )
{
	upErr eRtnErr = ERR_OK;
	byte bEntry = 0;
	bool_c dupCheck = false;

	dupCheck = CheckForDuplicates( eErr );
	if ( dupCheck )
		return ERR_OK;

	// If the queue is full, the message is thrown away
	eRtnErr = GetNextFreeUserMsgEntry( &bEntry );
	if ( ERR_OK == eRtnErr )
		{
		m_userMsgQueue[ bEntry ].code = eErr;
		m_userMsgQueue[ bEntry ].value1 = iValue;
		}

	return eRtnErr;
}

/**
 * @brief	This method creates a user message from a MCU error
 *    		code and attempts to add it to the use message queue.
 *
 * 			Duplicates to errors in the queue are ignored. If the error
 * 			given is a user error then the LEDs are set to LED_WARN state
 * 			but only if the LEDs are not already showing a LED_TERMINATE.
 *
 * @param   eErr The error code to create the user message from
 * @param	iValue A single value to add to the message
 *
 * @return  Status as a upErr
 */
upErr CLogMessage::CreateAndAddUserMsg( upErr eErr, int iValue )
{
	upErr eRtnErr = ERR_OK;

	// Only add error codes above the start of user errors
	if ( eErr < USER_ERROR_START )
		return ERR_OK;

	eRtnErr = AddUserMessage( eErr, iValue );

	// Turn on warning light for all user error messages unless already showing TERMINATE
	upLEDState eLed;
	g_pDigitalInputOutput->GetLEDState( &eLed );
	if ( eLed != LED_TERMINATE )
		g_pDigitalInputOutput->SetLEDState( LED_WARN );

	return eRtnErr;
}

/**
 * @brief	This method takes a general error code and an MCU operating state
 *			and returns a user message code. If no equivalent user message code
 *			is found, the input error code is returned.
 *
 * @param   eErr The error code that has a user message code equivalent
 * @param	eOperState The operating state of the MCU
 *
 * @return  Status as a upErr
 */
upErr CLogMessage::ReturnUserMsgCode( upErr eErr, upOperState eOperState )
{
	switch ( eOperState )
		{
		case OPER_STATE_CNCTD:
		case OPER_STATE_HAND_PRIMING:
		case OPER_STATE_CATH_PRIMING:
			return ReturnUserMsgPriming( eErr );

		case OPER_STATE_PENDING:
		case OPER_STATE_READY:
		case OPER_STATE_DELIVERING:
			return ReturnUserMsgTreating( eErr );

		default:
			return eErr;
		}
}

/**
 * @brief	This method takes a general error code and returns a priming state
 * 			specific user message code. If no equivalent user message code
 *			is found, the input error code is returned.
 *
 * @param   eErr The error code that has a priming specific user message code equivalent
 *
 * @return  Status as a upErr
 */
upErr CLogMessage::ReturnUserMsgPriming( upErr eErr )
{
	switch ( eErr )
		{
		case ERR_THERMO_UNDER_TEMP:
			// missed the priming temp target
			return ERR_PRIME_TEMP_LOW;
		case ERR_THERMO_OVER_TEMP:
			return ERR_PRIME_OVER_TEMP;
		case ERR_TRIGGER_OFF:
			return ERR_PRIME_TRIGGER_OFF;
		default:
			return eErr;
		}
}

/**
 * @brief	This method takes a general error code and returns a treating state
 * 			specific user message code. If no equivalent user message code
 *			is found, the input error code is returned.
 *
 * @param   eErr The error code that has a treatment specific user message code equivalent
 *
 * @return  Status as a upErr
 */
upErr CLogMessage::ReturnUserMsgTreating( upErr eErr )
{
	// Make treatment errors ablation specific where needed
	switch ( eErr )
		{
		case ERR_THERMO_OVER_TEMP:
			return ERR_ABLATE_OVER_TEMP;
		case ERR_THERMO_UNDER_TEMP:
			return ERR_ABLATE_UNDER_TEMP;
		default:
			return eErr;
		}
}

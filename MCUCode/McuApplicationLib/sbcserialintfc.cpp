/**
 * @file	sbcserialintfc.cpp
 * @par		Package: UGMcu
 * @par		Project: Union Generator
 * @par		Copyright (c) 2017, Uptake Medical Technology, Inc.
 * @author	Glen Sep 17, 2010 Created file
 * @brief	This file performs the interface, CSbcSerialIntfc, between the MCU and SBC, or other computer, using an RS232 interface and protocol
 */

// Include Files
#include "SbcSerialIntfc.h"
#include "string.h"
#include "LogMessage.h"
#include "stdio.h"

// External Public Data

// File Scope Macros and Constants

// File Scope Type Definitions (Enums, Structs & Classes)

// File Scope Data

// File Scope Functions

// Public Functions


/**
 * @brief	This is the constructor for CSbcSerialIntfc. Sets the "new" flags to false
 *
 */
CSbcSerialIntfc::CSbcSerialIntfc()
{
	InternalInitialization();
}

/**
 * @brief	This method calls InternalInitialization and sets up the the RS232 port.
 *
 * @return   Status as upErr
 */
upErr CSbcSerialIntfc::InitializeHardware()
{
	InternalInitialization();

	// Flushing the buffer needed for unit testing
	FlushReadBuffer( RS232_SBC );

	return InitializePort( RS232_SBC, RS232_BAUD_115200, RS232_PARITY_NONE, 8, 2 );

}

/**
 * @brief	This method moves the data received, if any, on the USART to local storage. It is
 * 			then accessible using ReadBuffer()
 *
 * @return   Status as upErr
 */
upErr CSbcSerialIntfc::ReadFromHardware()
{
	upErr eErr = ERR_OK;
	twobytes tbRead = 0;

	if ( !m_waitForReplyFlag )
	{
		// Check if the SBC UART buffer is receiving characters.
		// If so, the UART buffer is cleared by reading it and the information is ignored
		LOG_ERROR_RETURN( eErr = ReadPort( RS232_SBC, SBC_RECV_BYTE_MAX, m_readBuffer, &tbRead, 0 ) )
		if ( tbRead > 0 )
			{
			eErr = ERR_IPC_RECV_UNEXPECTED;
			}
		return eErr;
	}

	// Get bytes without waiting
	LOG_ERROR_RETURN( eErr = ReadPort( RS232_SBC, SBC_RECV_BYTE_MAX, m_readBuffer, &tbRead, 0 ) )
	if ( ERR_OK == eErr && tbRead > 0 )
		{
		// The packet buffer can never be complete if a new packet has just been read
		m_packetBuffer.m_packetComplete = false;
		UpdatePacketBytes( tbRead );
		if ( false == m_packetBuffer.m_packetComplete )
			return eErr;
		// A complete packet has been received
		UpdateRxPacketStats();
		ResetTimers();
		m_waitForReplyFlag = false;
		m_lastRxSequence = m_packetBuffer.m_sequence;
		m_currentRequest = m_packetBuffer.m_request;

		if ( !m_connectedState )
			m_connectedState = true;

		return eErr;
		}
	// There wasn't anything waiting in the buffer, so check for a timeout
	eErr = CheckForTimeout();
	if ( ERR_OK != eErr )
		{
			// in all cases, reset the flag to allow packets to be sent
			m_waitForReplyFlag = false;
			// Since there was no reply, the last message was NACKd
			m_lastAckd = false;
			// Evaluate the latency for logging
			if ( m_connectedState )
				{
				twobytes tbDiff;
				// the critical timeout timer contains the longest latency
				GetTickElapsed( m_criticalTimeoutTick, &tbDiff );
				unsignedfourbytes currentTimeoutMs = TICKS_TO_MS( tbDiff );

				m_packetStats.m_timeouts++;
				if ( currentTimeoutMs > m_packetStats.m_longestLatency )
					m_packetStats.m_longestLatency = currentTimeoutMs;
				m_intervalCnt++;
				}
			// If the timeout count exceeds the logging interval, create a log message and reset the timer
			if ( m_intervalCnt >= LOG_TIMEOUT_INTERVAL )
				{
				// The send buffer is currently guaranteed to be unused
				snprintf( (char*)m_sendBuffer, SBC_SEND_BYTE_MAX, "RxCnt: %d TxCnt: %d RxErrs: %d NacksRx: %d SeqErrs: %d Timeouts: %d Longest Latency: %d\n", m_packetStats.m_packetsRx,
						m_packetStats.m_packetsTx, m_packetStats.m_packetRxErrors, m_packetStats.m_nacksRx, m_packetStats.m_sequenceErr, m_packetStats.m_timeouts, m_packetStats.m_longestLatency );
				// If it is a minor timeout, the error will be ERR_OK
				g_pLogMessage->LogErrorMessage( eErr, (const char*)m_sendBuffer );
				m_intervalCnt = 0;
				}
			switch ( eErr )
				{
				case ERR_IPC_MINOR_TIMEOUT:
					// don't log minor timeouts, just send another packet
					eErr = ERR_OK;
					// reset the minor timeout timer
					GetTick( &m_minorTimeoutTick );
					break;
				case ERR_IPC_CRITICAL_TIMEOUT:
	#ifndef SIM_OUTPUT_CAPTURE
					m_criticalTimeout = true;
	#endif
					ResetTimers();
					break;
				case ERR_IPC_CONNECTION_TIMEOUT:
	#ifndef SIM_OUTPUT_CAPTURE
					m_connectionTimeout = true;
	#endif
					ResetTimers();
					break;
				default:
					eErr = ERR_BAD_PARAM;
					break;
				}
		}
	return eErr;
}


/**
 * @brief	This method returns the status of the wait for reply flag
 *
 * @param   pWaitFlag A pointer to the wait status flag
 *
 * @return   Status as upErr
 */
upErr CSbcSerialIntfc::CheckWaitForReplyFlag( bool_c * pWaitFlag )
{
	upErr eErr = ERR_OK;
	*pWaitFlag = m_waitForReplyFlag;
	return eErr;
}

/**
 * @brief	This method sends any newly-set buffer to the SBC via the USART by calling WritePort()
 *
 * @return   Status as upErr
 */
upErr CSbcSerialIntfc::WriteToHardware()
{
	upErr eErr = ERR_OK;
#ifndef SIM_OUTPUT_CAPTURE
	if ( m_waitForReplyFlag )
		// This can only be cleared in ReadFromHardware
		return eErr;

	// Initialize the connection timer
	if ( !m_connectedState && 0 == m_connectionTimer )
	{
		// initialize the timers
		GetTick( &m_connectionTimer );
		m_minorTimeoutTick = m_connectionTimer;
	}

	if ( ERR_OK == eErr && m_sendPktLength > 0 )
		{
		LOG_ERROR_RETURN( eErr = WritePort( RS232_SBC, m_sendPktLength, m_sendBuffer ) );
		// regardless of the error state, reset the packet length
		m_sendPktLength = 0;
		if ( ERR_NOT_READY == eErr )
			{
			// packet is ignored if the RS-232 queue is full
			return eErr;
			}
		m_packetStats.m_packetsTx++;
		// set the timeout timer and wait flag
		m_waitForReplyFlag = true;
		// this is the last successful transmission
		m_lastTxSequence = m_sendBuffer[ FRAME_SEQUENCE ] >> 1;
		}
#endif
	return eErr;

}

/**
 * @brief	This method resets the critical timeout timer time stamp.
 *
 * @return   Status as upErr
 */
upErr CSbcSerialIntfc::ResetCriticalTimestamp()
{
	tick tNow;
	GetTick( &tNow );
	m_criticalTimeoutTick = tNow;
	return ERR_OK;
}

/**
 * @brief	This method returns whether the last
 *
 * @return   A boolean: true == The last packet sent was Ackd by the peer.
 * 			 		    false == The last packet sent was Nackd by the peer.
 */
bool_c CSbcSerialIntfc::GetLastAckStatus()
{
	return m_lastAckd;
}

/**
 * @brief	This method sets the internal variables and clears the buffers
 *
 */
void CSbcSerialIntfc::InternalInitialization()
{
	FlushPacketBuffer();
	m_packetBuffer.m_request = MSG_TYPE_OPCOND;
	m_packetBuffer.m_ePacketState = SYNCH;
	m_lastRxSequence = 0;
	// set to 1 so that Tx and Rx are not the same starting
	m_sequence = 1;
	m_lastTxSequence = 1;
	m_lastAckd = false;
	m_waitForReplyFlag = false;
	m_minorTimeoutTick = 0;
	m_criticalTimeoutTick = 0;
	m_connectionTimer = 0;
	m_connectionTimeout = false;
	m_criticalTimeout = false;
	m_connectedState = false;
	m_intervalCnt = 0;
	m_sendPktLength = 0;
	m_currentRequest = MSG_TYPE_OPCOND;
	m_checksumCompare = 0;
	m_packetStats.m_packetRxErrors = 0;
	m_packetStats.m_sequenceErr = 0;
	m_packetStats.m_packetsTx = 0;
	m_packetStats.m_packetsRx = 0;
	m_packetStats.m_timeouts = 0;
	m_packetStats.m_nacksRx = 0;
	m_packetStats.m_longestLatency = 0;
}

/*
 * @brief	This method performs data-link layer processing on a message and
 * 			adds the resultant packet to the outgoing packet queue
 *
 * @param	bMsgType The message type related to the message data
 * @param 	pbMessage The pointer to a serial message to enqueue
 * @param   iMsgLength The length of the message
 * @param 	bRequest The next requested message type
 *
 * @return   Status as upErr
 */
upErr CSbcSerialIntfc::MessageToPacketBuffer( byte bMsgType, byte * pbMessage, twobytes tbMsgLength,
        byte bRequest )
{
	upErr eErr = ERR_OK;
	twobytes tbPacketLength;

	// check wait status first.  don't construct the packet if waiting for a reply.
	if ( m_waitForReplyFlag )
		{
		m_sendPktLength = 0;
		return ERR_NOT_READY;
		}
	eErr = ConstructPacket( m_sendBuffer, bMsgType, pbMessage, bRequest, tbMsgLength, &tbPacketLength );
	if ( ERR_OK == eErr )
		{
		m_sendPktLength = tbPacketLength;
		}
	else
		m_sendPktLength = 0;

	return eErr;
}

/*
 * @brief	This method retrieves a packet from the packet Q, performs
 * 			data-link layer checking on the packet, removes the protocol overhead
 * 			and returns the message retrieved from the packet
 *
 * @param	pbMessage The pointer to a serial message to enqueue
 *
 * @return   Status as upErr
 */
upErr CSbcSerialIntfc::PacketBufferToMessage( byte * pbMessage, twobytes tbMaxLength )
{
	upErr eErr = ERR_OK;
	twobytes pktLength = 0;
	byte pktSequence = 0;
	byte pktRequest = 0;
	byte pbMsgType = 0;

	if ( true == m_packetBuffer.m_packetComplete )
		{
		LOG_ERROR_RETURN( eErr = RetrievePayload( pbMessage, &pktSequence,
				&pbMsgType, &pktRequest, &pktLength, tbMaxLength ) )
		if ( ERR_OK == eErr )
			{
			m_currentRequest = pktRequest;
			m_lastAckd = ( 0x01 == m_packetBuffer.m_ack ) ? true : false;
			}
		// Message retrieved, reset the buffer
		FlushPacketBuffer();
		}
	else
		eErr = ERR_NOT_READY;
	return eErr;
}

/**
 * @brief	This method returns the next message type sitting in either
 * 			the out or the in queue
 *
 * @param	peMessageType The message type to return
 *
 * @return   Status as upErr
 */
upErr CSbcSerialIntfc::ReturnNextMsgType( upMessageTypes * peMessageType )
{
	upErr eErr = ERR_OK;

	// decide which queue to use based on the inOutFlag (OUT == 0, IN == 1)
	if ( true == m_packetBuffer.m_packetComplete )
		{
		// find the message type in the payload
		*peMessageType = (upMessageTypes)m_packetBuffer.m_messageType;
		}
	else
		{
		*peMessageType = MSG_TYPE_NOTSPECIFIED;
		}

	return eErr;
}

/**
 * @brief	This method checks for a minor or critical timeout due to the lack of
 *    		a received packet from the SBC.  The timeout counter
 *    		is set when a write to the RS232 buffer occurs.
 *    		If the wait for reply flag isn't set, just return ERR_OK and
 *    		a flag status of false.
 *
 * @return   Status as upErr
 */
upErr CSbcSerialIntfc::CheckForTimeout()
{
	upErr eErr = ERR_OK;
	twobytes tbDiff = 0;

	// default to false
	if ( !m_waitForReplyFlag )
		return ERR_OK;

	GetTickElapsed( m_minorTimeoutTick, &tbDiff );
	if ( tbDiff > SBC_MINOR_TIMEOUT_TICKS )
		eErr = ERR_IPC_MINOR_TIMEOUT;


	if ( m_connectedState )
		{
		GetTickElapsed( m_criticalTimeoutTick, &tbDiff );
#ifndef SIM_OUTPUT_CAPTURE
		if ( tbDiff > SBC_CRITICAL_TIMEOUT_TICKS )
			eErr = ERR_IPC_CRITICAL_TIMEOUT;
#endif
		}
	else
		{
		// check for connection timeout
		tick tDiff = 0;
		GetTickElapsed( m_connectionTimer, &tDiff );
		if ( tDiff > SBC_CONNECTION_TIMEOUT )
			eErr = ERR_IPC_CONNECTION_TIMEOUT;
		}

	return eErr;
}

/**
 * @brief	This method returns the status of the critical timeout
 *    		state.
 *
 * @return   ERR_OK: No critical timeout state, normal operation
 * @return   ERR_IPC_CRITICAL_TIMEOUT: A critical timeout state is active
 */
upErr CSbcSerialIntfc::GetTimeoutFlag()
{
	upErr eErr = ERR_OK;
#ifndef SIM_OUTPUT_CAPTURE
	if ( m_criticalTimeout )
		eErr = ERR_IPC_CRITICAL_TIMEOUT;
	if ( m_connectionTimeout )
		eErr = ERR_IPC_CONNECTION_TIMEOUT;
#endif
	return eErr;
}


/**
 * @brief	This method returns the size of the outgoing queue
 *
 *
 * @return   Status as upErr
 */
bool_c CSbcSerialIntfc::PacketReceived()
{
	return m_packetBuffer.m_packetComplete;
}

/**
 * @brief	This method returns the last stored request
 *
 * @param	pbRequest A pointer to receive the last request received
 *
 * @return   Status as upErr
 */
upErr CSbcSerialIntfc::GetLastSbcRequest( byte * pbRequest )
{
	upErr eErr = ERR_OK;
	*pbRequest = m_currentRequest;
	return eErr;
}

// Private Class Functions

/**
 * @brief	This method constructs a frame from a given payload
 *
 * @param	pPacket The constructed packet to return
 * @param   bMsgType The message type relating to message data
 * @param	pbMessage The serial message payload
 * @param   bRequest The next requested message type
 * @param   tbMsgLength The length of the message to send
 * @param   ptbPacketLength The resultant packet length
 *
 * @return   Status as upErr
 */
upErr CSbcSerialIntfc::ConstructPacket( byte * pPacket, byte bMsgType, byte * pbMessage,
        byte bRequest, twobytes tbMsgLength, twobytes * ptbPacketLength )
{
	upErr err = ERR_OK;
	int i = 0;
	twobytes checkSum = 0;
	int endPayFrame = 0;
	int messageIndex = 0;
	// load the known indices if payload size <= max
	*ptbPacketLength = 0;
	byte ackBit = 0;
	if ( tbMsgLength > MAX_MESSAGE_SIZE )
		{
		err = ERR_IPC_PAYLOAD_OVERFLOW;
		return err;
		}
	pPacket[ FRAME_START_FLAG ] = PACKET_START_FLAG;
	pPacket[ FRAME_SEQUENCE ] = (byte) ( m_sequence << 1 );
	// OR the ACK bit
	ackBit = ( m_lastRxSequence == m_lastTxSequence ) ? 1 : 0;
	pPacket[ FRAME_SEQUENCE ] |= ackBit;
	pPacket[ FRAME_MSGTYPE ] = bMsgType;
	pPacket[ FRAME_REQUEST ] = bRequest;
	pPacket[ FRAME_LENGTH ] = (byte)tbMsgLength;

	endPayFrame = FRAME_PAYLOAD + tbMsgLength;
	for ( i = FRAME_PAYLOAD; i < endPayFrame; i++ )
		{
		// copy array while also adding checksum count
		pPacket[ i ] = pbMessage[ messageIndex++ ];
		checkSum += pPacket[ i ];
		}
	// finish adding bytes to checksum
	checkSum += pPacket[ FRAME_SEQUENCE ] + pPacket[ FRAME_LENGTH ];
	checkSum += pPacket[ FRAME_MSGTYPE ] + pPacket[ FRAME_REQUEST ];
	// finish putting together rest of protocol overhead
	i = endPayFrame;
	// Checksum is little endian to match the endianess of the MCU
	pPacket[ i++ ] = (byte) ( checkSum & 0x00FF );
	pPacket[ i++ ] = (byte) ( checkSum >> 8 );
	pPacket[ i ] = PACKET_END_FLAG;
	*ptbPacketLength = tbMsgLength + PROTOCOL_OVERHEAD;
	// increase sequence.  will rollover at 128 due to bit-shift left.
	m_sequence = ( m_sequence + 1 ) & 0x7F;

	return err;
}

/**
 * @brief	This method returns the payload from a frame
 *
 * @param	pbMessage A pointer to the message bytes
 * @param   pMsgType A pointer to the message type
 * @param   pSequence A pointer to the sequence number
 * @param   pRequest A pointer to the requested message type
 * @param   ptbLength  A pointer to the message length to return
 * @param   tbMaxLength The maximum length to retrieve
 *
 * @return   Status as upErr
 */
upErr CSbcSerialIntfc::RetrievePayload( byte * pbMessage, byte * pSequence,
        byte * pMsgType, byte * pRequest, twobytes * ptbLength, twobytes tbMaxLength )
{
	upErr err = ERR_OK;

	// check start flags
	if ( false == m_packetBuffer.m_packetComplete )
		{
		return ERR_NOT_READY;
		}
	*pMsgType = m_packetBuffer.m_messageType;
	*pSequence = m_packetBuffer.m_sequence;
	*pRequest = m_packetBuffer.m_request;
	// load the known indices if payload size <= max
	if ( m_packetBuffer.m_payloadLength > MAX_MESSAGE_SIZE )
		{
		err = ERR_IPC_PAYLOAD_OVERFLOW;
		return err;
		}
	else
		{
		*ptbLength = m_packetBuffer.m_payloadLength;
		}

	// If the message is larger than expected, still retrieve it but return an error
	if ( m_packetBuffer.m_payloadLength > tbMaxLength )
		{
		return ERR_IPC_PAYLOAD_OVERFLOW;
		}

	memcpy( (void*) pbMessage, (void*) m_packetBuffer.m_data, (size_t)m_packetBuffer.m_payloadLength );

	return err;
}


/**
 * @brief	This method updates the packet buffer using the bytes received
 * 			from the RS-232 buffer.  Once a packet is complete and is
 * 			considered valid, the packet complete flag in the packet
 * 			buffer structure is set to "complete."  The packet must then
 * 			be enqueued by another method if room is available.
 *
 * @param	tbRdSize The size of the incoming data
 *
 * @return   Status as upErr
 */
upErr CSbcSerialIntfc::UpdatePacketBytes( twobytes tbRdSize )
{
	upErr err = ERR_OK;
	twobytes i = 0;
	twobytes dataIndex = 0;

	while ( i < tbRdSize && m_packetBuffer.m_packetLength < MAX_PACKET_SIZE )
		{
		switch ( m_packetBuffer.m_ePacketState )
			{
			case ( SYNCH ):
				if ( PACKET_START_FLAG == m_readBuffer[ i ] )
					{
					// reset packet buffer values
					FlushPacketBuffer();
					m_packetBuffer.m_packetLength++;
					m_checksumCompare = 0;
					UpdatePacketState( SEQUENCEACK );
					}
				break;
			case ( SEQUENCEACK ):
				m_packetBuffer.m_sequence = m_readBuffer[ i ] >> 1;
				m_packetBuffer.m_ack = m_readBuffer[ i ] & 0x01;
				m_packetBuffer.m_packetLength++;
				m_packetBuffer.m_checksum = m_readBuffer[ i ];
				UpdatePacketState( MSGTYPE );
				break;
			case ( MSGTYPE ):
				m_packetBuffer.m_messageType = m_readBuffer[ i ];
				m_packetBuffer.m_packetLength++;
				m_packetBuffer.m_checksum += m_readBuffer[ i ];
				UpdatePacketState( REQUEST );
				break;
			case ( REQUEST ):
				m_packetBuffer.m_request = m_readBuffer[ i ];
				m_packetBuffer.m_packetLength++;
				m_packetBuffer.m_checksum += m_readBuffer[ i ];
				UpdatePacketState( LENGTH );
				break;
			case ( LENGTH ):
				if ( m_readBuffer[ i ] > MAX_MESSAGE_SIZE )
					{
					ErrorFlushAndReSynch();
					}
				else
					{
					m_packetBuffer.m_payloadLength = m_readBuffer[ i ];
					m_packetBuffer.m_packetLength++;
					m_packetBuffer.m_checksum += m_readBuffer[ i ];
					UpdatePacketState( DATA );
					}
				// handle the case where no data has been sent (a NACK message)
				if ( 0 == m_packetBuffer.m_payloadLength )
					{
					// skip right to the checksum since payload length is 0
					UpdatePacketState( CHECKSUM1 );
					}
				break;
			case ( DATA ):
				dataIndex = m_packetBuffer.m_packetLength - (twobytes)FRAME_PAYLOAD;
				// must check for end of data segment
				// Cannot get here unless there is at least 1 byte of data
				m_packetBuffer.m_data[ dataIndex ] = m_readBuffer[ i ];
				m_packetBuffer.m_packetLength++;
				m_packetBuffer.m_checksum += m_readBuffer[ i ];
				if ( dataIndex >= m_packetBuffer.m_payloadLength - 1 )
					UpdatePacketState( CHECKSUM1 );
				break;
			case ( CHECKSUM1 ):
				// Note: Checksum is two bytes
				// This is the LSB (little endian)
				m_packetBuffer.m_packetLength++;
				m_checksumCompare = m_readBuffer[ i ];
				UpdatePacketState( CHECKSUM2 );
				break;
			case ( CHECKSUM2 ):
				m_packetBuffer.m_packetLength++;
				m_checksumCompare |= (twobytes) m_readBuffer[ i ] << 8;
				if ( m_checksumCompare != m_packetBuffer.m_checksum )
					{
					ErrorFlushAndReSynch();
					}
				else
					// checksum passed
					UpdatePacketState( END );
				break;
			case ( END ):
				m_packetBuffer.m_packetLength++;
				if ( PACKET_END_FLAG == m_readBuffer[ i ] && m_packetBuffer.m_packetLength
				        <= MAX_PACKET_SIZE )
					{
					// update the packet flag to indicate complete status
					m_packetBuffer.m_packetComplete = true;
					UpdatePacketState( SYNCH );
					}
				else
					{
					// may not fall to PACKET_ERROR state, so ensure the
					// packet buffer is flushed
					ErrorFlushAndReSynch();
					}
				break;
			default:
				break;
			} // end switch
		i++;
		} // end while

	return err;
}

/*
 * @brief	This method flushes the packet buffer
 *
 * @return   Status as upErr
 */
upErr CSbcSerialIntfc::FlushPacketBuffer()
{
	upErr eErr = ERR_OK;
	m_packetBuffer.m_packetComplete = false;
	m_packetBuffer.m_packetLength = 0;
	m_packetBuffer.m_checksum = 0;
	m_packetBuffer.m_payloadLength = 0;
	m_packetBuffer.m_ack = 0;
	m_packetBuffer.m_request = 0;
	m_packetBuffer.m_sequence = 0;
	m_packetBuffer.m_messageType = 0;
	return eErr;
}


/**
 * @brief	This method retrieves the current packet receive state
 *
 * @param	pPacketState A pointer that returns the packet state
 *
 * @return   Status as upErr
 */
upErr CSbcSerialIntfc::GetPacketState( upPacketStates * pPacketState )
{
	upErr err = ERR_OK;
	*pPacketState = m_packetBuffer.m_ePacketState;
	return err;
}

/**
 * @brief	This method has a data packet sent using the serial communication protocol.
 *
 * @param	tbCnt	The number of bytes to write
 * @param   pbBytes	A pointer to a buffer of bytes to send
 *
 * @return   Status as upErr
 */
upErr CSbcSerialIntfc::WriteBuffer( twobytes tbCnt, byte *pbBytes )
{
	upErr eErr = ERR_OK;

	if ( tbCnt > SBC_SEND_BYTE_MAX )
		{
		eErr = ERR_BAD_PARAM;
		}
	else
		{
		memcpy( (void*) m_sendBuffer, (void*) pbBytes, (size_t)tbCnt );
		}
	return eErr;
}


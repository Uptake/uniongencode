/**
 * @file	McuInfo.cpp
 * @par		Package: UGMcu
 * @par		Project: Union Generator
 * @par		Copyright (c) 2017, Uptake Medical Technology, Inc.
 * @author	Glen Aug 18, 2012 Created file
 * @brief	This file contains the definitions for CMcuInfo
 */

// Include Files
#include "McuInfo.h"

// External Public Data

// File Scope Macros and Constants

// File Scope Type Definitions (Enums, Structs & Classes)
#define	REV_F_MASK	0x00
#define	REV_G_MASK	0x11

// File Scope Data

upMcuAssemblyRevision CMcuInfo::m_assemblyRev = MCU_ASSEMBLY_UNKNOWN;
upMcuBoardRevision CMcuInfo::m_boardRev = MCU_BOARD_UNKNOWN;

// File Scope Functions

// Public Functions

/**
 * @brief	This is the constructor for CMcuInfo
 *
 */

CMcuInfo::CMcuInfo()
{
}

/**
 * @brief	This method reads the identification of the board and assembly revision via the CPLD and
 * 			creates the enumeration representation of that information.
 *
 * 			This method must be called prior to accessing the getters for the data.
 *
 * @return   Status as upErr
 */
upErr CMcuInfo::IdentifyRevision()
{
	byte bRev;
	upErr eErr;

	eErr = MMAP_READ( MMAP_FUNC_MCU_BOARD_REVISION, &bRev )
	if ( ERR_OK == eErr )
		{
		switch ( bRev )
			{
			case REV_F_MASK:
				m_assemblyRev = MCU_ASSEMBLY_F;
				m_boardRev = MCU_BOARD_C;
				break;

			case REV_G_MASK:
				m_assemblyRev = MCU_ASSEMBLY_G;
				m_boardRev = MCU_BOARD_D;
				break;

			default:
				m_assemblyRev = MCU_ASSEMBLY_F;
				m_boardRev = MCU_BOARD_C;
				eErr = ERR_BOARD_ASSEMBLY_UNKNOWN;
				break;
			}
		}

	return eErr;
}

// Private Class Functions


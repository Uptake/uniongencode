/**
 * @file	SelfTest.cpp
 * @par		Package: UGMcu
 * @par		Project: Union Generator
 * @par		Copyright (c) 2017, Uptake Medical Technology, Inc.
 * @author	Glen May 10, 2011 Created file
 * @brief	This file contains the definitions for the self test class CSelfTest
 */

// Include Files
#include "SelfTest.h"
#include "DiagnosticLed.h"
#include "DigitalInputOutput.h"
#include "CommonOperations.h"
#include "LogMessage.h"
#include "WaterCntl.h"
#include "string.h"
#include "MemMapIntfc.h"
#include "SoundCntl.h"
#include "OneWireMemory.h"

// External Public Data
CSelfTest * g_pSelfTest = 0;
//extern twobytes __bss_end;
extern twobytes __data_start;
extern twobytes __data_load_start;

// File Scope Macros and Constants

// File Scope Type Definitions (Enums, Structs & Classes)
#define MOTOR_TEST_VEL			( (long int) STEPS_PER_REV / 2 ) // 1/2 RPS for 1 second makes about 1/16" of travel

// File Scope Data
twobytes g_tbImageCrc = 0xFFFE;

// File Scope Functions

// Public Functions

/**
 * @brief	This is the constructor for SelfTest
 *
 */
CSelfTest::CSelfTest()
{
	g_pSelfTest = this;
	SetTestCycle( 0 );
}

/**
 * @brief	This method test the operations of the Union Generator
 *
 * @param pNextState A pointer to an upGenericState to receive the next state of self test
 *
 * @return   Status as upErr
 */
upErr CSelfTest::SystemTest( upGenericState * pNextState )
{

	upErr eErr = ERR_OK;
	bool_c bRetracted;

	*pNextState = GEN_STATE_FIRST_ACTION;

	/*
	 * Get retracted switch and set initial values
	 * Sound tone on first entry
	 */
	g_pDigitalInputOutput->GetInput( DIG_IN_MOTOR_RETRACT, &bRetracted );
	if ( 0 == m_testCycle )
		{
		g_pSoundCntl->OutputTestTone();
		}

	/*
	 * test CPLD
	 */
	else if ( 1 == m_testCycle )
		{
		m_initiallyRetracted = bRetracted;
		byte iDog[ 2 ];
		byte iSent[ 2 ];
		byte iRcvd[ 2 ];

		// Save existing value
		MMAP_READ( MMAP_FUNC_WDOG, iDog );

		// Write and check all two byte values
		for ( unsigned long int i = 1; ERR_OK == m_eCpldErr && i < 0x10000; i <<= 1 )
			{
			iSent[ 0 ] = (byte) i;
			iSent[ 1 ] = (byte) ( i >> 8 );
			MMAP_WRITE( MMAP_FUNC_WDOG, iSent );
			MMAP_READ( MMAP_FUNC_WDOG, iRcvd );
			if ( iSent[ 0 ] != iRcvd[ 0 ] || iSent[ 1 ] != iRcvd[ 1 ] )
				LOG_ERROR_RETURN( m_eCpldErr = ERR_SELFTEST_CPLD )
			}

		// Write back original value
		MMAP_WRITE( MMAP_FUNC_WDOG, iDog );

		// Check version which is on CPLD
		if ( m_mcuInfo.GetAssemblyRevision() == MCU_ASSEMBLY_UNKNOWN )
			LOG_ERROR_RETURN( m_eCpldErr = ERR_SELFTEST_CPLD )

		}

	// After cycles 0 and 1
	else
		{

		// Rod retracted in first second so extend it a bit
		if ( m_initiallyRetracted && m_testCycle < RND_ROBIN_LOOP_HZ * 2)
			{
			g_pWaterCntl->SetTargetVelocity( MOTOR_TEST_VEL );
			}

		// If still retracted then it's an error
		else if ( m_initiallyRetracted && RND_ROBIN_LOOP_HZ * 2 == m_testCycle )
			{
			if ( bRetracted )
				{
				LOG_ERROR_RETURN( m_eWaterPumpErr = ERR_SELFTEST_WATER_PUMP )
				g_pWaterCntl->SetTargetVelocity( 0 );
				}
			}

		// Not initially retracted and in the first second and not an error so retract
		else if ( ERR_OK == m_eWaterPumpErr )
			{
			g_pWaterCntl->StartRetractingPumpRod();
			}

		/*
		 * Test LEDs
		 */
		if ( m_testCycle < 3 * RND_ROBIN_LOOP_HZ )
			{
			g_pDigitalInputOutput->SetLEDState( LED_WARN );
			}
		else if ( m_testCycle < 6 * RND_ROBIN_LOOP_HZ )
			{
			g_pDigitalInputOutput->SetLEDState( LED_READY );
			}
		else
			{
			g_pDigitalInputOutput->SetLEDState( LED_POWER );
			}

		}

	// If the CPLD bus check has failed, exit immediately
	if ( ERR_SELFTEST_CPLD == m_eCpldErr )
		{
		*pNextState = GEN_STATE_END;
		}

	// Exit after test time used
	m_testCycle++;
	if ( m_testCycle > SELF_TEST_CYCLE_CNT )
		{
		g_pWaterCntl->SetFlowRateUlPerMin( 0 );
		*pNextState = GEN_STATE_END;

		// If not retracted in test time it's an error
		if ( false == bRetracted )
			{
			LOG_ERROR_RETURN( m_eWaterPumpErr = ERR_SELFTEST_WATER_PUMP )
			}
		}

	m_bProgress = (byte) ( 100 * m_testCycle / SELF_TEST_CYCLE_CNT );

	// Ending self test
	if ( GEN_STATE_END == *pNextState )
		{
		// Report CPLD error
		if ( ERR_OK != m_eCpldErr )
			{
			LOG_ERROR_RETURN( eErr = m_eCpldErr )
			}

		// Report pump error
		if ( ERR_OK != m_eWaterPumpErr )
			{
			LOG_ERROR_RETURN( eErr = m_eWaterPumpErr )
			}

		// Check the one-wire hardware
		if ( ERR_OK != g_pOneWireMemory->CheckHardware() )
			{
			LOG_ERROR_RETURN ( eErr = ERR_I2C_DEVICE_ERROR )
			}

		// Check the versions of the real-time code
		if ( ERR_OK != g_pComOp->GetRtcVersions() )
			{
			LOG_ERROR_RETURN( eErr = ERR_VERSION_CHECK )
			}
		}

	return eErr;
}

/**
 * @brief	This method
 *
 * @param	pBaseAddress A byte* that is the address of the first byte to test
 * @param	pSaveAddress A byte* to the first byte of the scratch area
 * @param	pEndAddress A byte* to the last address to test
 * @param	tbBlockSize The size of bytes to test at one time as a twobytes
 *
 * @return   Status as upErr
 */
upErr CSelfTest::MemTestBlockWithSave( byte * pBaseAddress, byte * pSaveAddress, byte * pEndAddress,
        twobytes tbBlockSize )
{
	volatile byte * pTestBlock;

	// Loop over memory in block steps. Be careful not to increment past 0xFFFF or pointer will wrap
	for ( pTestBlock = pBaseAddress; pBaseAddress <= pTestBlock && pTestBlock <= pEndAddress; pTestBlock += tbBlockSize )
		{

		// Copy target region to temporary storage block
		if ( pTestBlock != (volatile byte *) pSaveAddress )
			memcpy( (void*) pSaveAddress, (void*) pTestBlock, tbBlockSize );

		// Perform tests on block
		if ( MemTestDataBus( pTestBlock ) != 0 )
			return ERR_ERROR;

		if ( MemTestAddressBus( pTestBlock, tbBlockSize ) != NULL )
			return ERR_ERROR;

		if ( MemTestDevice( pTestBlock, tbBlockSize ) != NULL )
			return ERR_ERROR;

		// Copy temporary block back or zero it out if the target area was the temporary block
		if ( pTestBlock != (volatile byte *) pSaveAddress )
			memcpy( (void*) pTestBlock, (void*) pSaveAddress, tbBlockSize );
		else
			memset( (void*) pSaveAddress, 0, tbBlockSize );

		} // for

	return ERR_OK;

}

/**
 * @brief	This method tests the internal and external SRAM
 *
 * @return   Status as upErr
 */
upErr CSelfTest::MemTest( void )
{
#define EXT_BLOCK_SIZE	  	( (twobytes) 1024)
#define EXT_BASE_ADDRESS  	( (volatile byte *) 0x8000)
#define EXT_MEMORY_SIZE    	( (twobytes) 32 * 1024)
#define EXT_TEMPORARY_BLOCK ( (twobytes) EXT_BASE_ADDRESS )
#define EXT_LAST_BLOCK		( EXT_BASE_ADDRESS + ( EXT_MEMORY_SIZE - EXT_BLOCK_SIZE ) )

#define INT_BLOCK_SIZE	  	( (twobytes) 64)
#define INT_BASE_ADDRESS  	( (twobytes) &__data_start )
#define INT_END_ADDRESS  	( (twobytes) 0x1100 )
#define INT_START_ADDRESS  	( INT_BLOCK_SIZE * ( 1 + ( (twobytes) &__bss_end / INT_BLOCK_SIZE ) ) )
#define INT_TEMPORARY_BLOCK ( INT_END_ADDRESS - 5 * INT_BLOCK_SIZE )
#define INT_LAST_BLOCK		( INT_END_ADDRESS - 4 * INT_BLOCK_SIZE )

	upErr eErr = ERR_OK;

	// Internal RAM test
//	eErr = MemTestBlockWithSave( (byte *) INT_START_ADDRESS, (byte *) INT_TEMPORARY_BLOCK, (byte *) INT_LAST_BLOCK, (twobytes) INT_BLOCK_SIZE );

	// External RAM test
	// Don't test external memory in simulation as testing will overwrite the pre-loaded sim records
	if ( g_bIsSimulated || ERR_OK != eErr )
		return eErr;

//	eErr = MemTestBlockWithSave( (byte *) EXT_BASE_ADDRESS, (byte *) EXT_TEMPORARY_BLOCK, (byte *) EXT_LAST_BLOCK, (twobytes) EXT_BLOCK_SIZE );

	return eErr;

} /* MemTest() */

/**********************************************************************
 *
 * @param       Test the data bus wiring in a memory region by
 *              performing a walking 1's test at a fixed address
 *              within that region.  The address (and hence the
 *              memory region) is selected by the caller.
 *
 * @return     0 if the test succeeds.
 *              A non-zero result is the first pattern that failed.
 *
 **********************************************************************/
byte CSelfTest::MemTestDataBus( volatile byte * address )
{
	byte pattern;

	/*
	 * Perform a walking 1's test at the given address.
	 */
	for ( pattern = 1; pattern != 0; pattern <<= 1 )
		{
		/*
		 * Write the test pattern.
		 */
		*address = pattern;

		/*
		 * Read it back (immediately is ERR_OKay for this test).
		 */
		if ( *address != pattern )
			{
			return ( pattern );
			}
		}

	return ( 0 );

} /* MemTestDataBus() */

/**********************************************************************
 *
 * Function:    MemTestAddressBus()
 *
 * @param       Test the address bus wiring in a memory region by
 *              performing a walking 1's test on the relevant bits
 *              of the address and checking for aliasing. This test
 *              will find single-bit address failures such as stuck
 *              -high, stuck-low, and shorted pins.  The base address
 *              and size of the region are selected by the caller.
 *
 * Notes:       For best results, the selected base address should
 *              have enough LSB 0's to guarantee single address bit
 *              changes.  For example, to test a 64-Kbyte region,
 *              select a base address on a 64-Kbyte boundary.  Also,
 *              select the region size as a power-of-two--if at all
 *              possible.
 *
 * @return     NULL if the test succeeds.
 *              A non-zero result is the first address at which an
 *              aliasing problem was uncovered.  By examining the
 *              contents of memory, it may be possible to gather
 *              additional information about the problem.
 *
 **********************************************************************/
byte * CSelfTest::MemTestAddressBus( volatile byte * baseAddress, twobytes nBytes )
{
	unsigned long addressMask = ( nBytes / ( 8 * sizeof(byte) ) - 1 );
	unsigned long offset;
	unsigned long testOffset;

	byte pattern = (byte) 0xAAAAAAAA;
	byte antipattern = (byte) 0x55555555;

	/*
	 * Write the default pattern at each of the power-of-two offsets.
	 */
	for ( offset = 1; ( offset & addressMask ) != 0; offset <<= 1 )
		{
		baseAddress[ offset ] = pattern;
		}

	/*
	 * Check for address bits stuck high.
	 */
	testOffset = 0;
	baseAddress[ testOffset ] = antipattern;

	for ( offset = 1; ( offset & addressMask ) != 0; offset <<= 1 )
		{
		if ( baseAddress[ offset ] != pattern )
			{
			return ( (byte *) &baseAddress[ offset ] );
			}
		}

	baseAddress[ testOffset ] = pattern;

	/*
	 * Check for address bits stuck low or shorted.
	 */
	for ( testOffset = 1; ( testOffset & addressMask ) != 0; testOffset <<= 1 )
		{
		baseAddress[ testOffset ] = antipattern;

		if ( baseAddress[ 0 ] != pattern )
			{
			return ( (byte *) &baseAddress[ testOffset ] );
			}

		for ( offset = 1; ( offset & addressMask ) != 0; offset <<= 1 )
			{
			if ( ( baseAddress[ offset ] != pattern ) && ( offset != testOffset ) )
				{
				return ( (byte *) &baseAddress[ testOffset ] );
				}
			}

		baseAddress[ testOffset ] = pattern;
		}

	return ( NULL );

} /* MemTestAddressBus() */

/**********************************************************************
 *
 * Function:    MemTestDevice()
 *
 * @param       Test the integrity of a physical memory device by
 *              performing an increment/decrement test over the
 *              entire region.  In the process every storage bit
 *              in the device is tested as a zero and a one.  The
 *              base address and the size of the region are
 *              selected by the caller.
 *
 * @return     NULL if the test succeeds.
 *
 *              A non-zero result is the first address at which an
 *              incorrect value was read back.  By examining the
 *              contents of memory, it may be possible to gather
 *              additional information about the problem.
 *
 **********************************************************************/
byte * CSelfTest::MemTestDevice( volatile byte * baseAddress, twobytes nBytes )
{
	unsigned long offset;
	unsigned long nWords = nBytes / sizeof(byte);

	byte pattern;
	byte antipattern;

	/*
	 * Fill memory with a known pattern.
	 */
	for ( pattern = 1, offset = 0; offset < nWords; pattern++, offset++ )
		{
		baseAddress[ offset ] = pattern;
		}

	/*
	 * Check each location and invert it for the second pass.
	 */
	for ( pattern = 1, offset = 0; offset < nWords; pattern++, offset++ )
		{
		if ( baseAddress[ offset ] != pattern )
			{
			return ( (byte *) &baseAddress[ offset ] );
			}

		antipattern = ~pattern;
		baseAddress[ offset ] = antipattern;
		}

	/*
	 * Check each location for the inverted pattern and zero it.
	 */
	for ( pattern = 1, offset = 0; offset < nWords; pattern++, offset++ )
		{
		antipattern = ~pattern;
		if ( baseAddress[ offset ] != antipattern )
			{
			return ( (byte *) &baseAddress[ offset ] );
			}
		}

	return ( NULL );

}

/**
 * @brief	This method performs the CRC test over the FLASH memory.
 *
 *		If pulls bytes from the FLASH memory and calculates a CRC on them. The AVR Harvard architecture
 *		requires an extra step for code to access that program memory as data. The calculation skips over
 *		the bytes in the FLASH where the expected CRC value was inserted. The final setp is to compare the found CRC
 *		to the expected value.
 *
 * @return   ERROR if CRC check fails otherwise ERR_OK
 */
upErr CSelfTest::CrcTest()
{
	crc_t crc = 0;
	uint32_t i;
	unsigned char c;
	uint32_t crcAddr;

	crcAddr = ( uint32_t ) & g_tbImageCrc; // Address of variable
	crcAddr -= ( uint32_t ) & __data_start; // Base address for all variables

	// HACK warning!
	// The compiler can't handle "variable" addresses above 64K. Use the low two bytes and add on the upper byte manually
	// The data segment must by above 64K for this to work or be needed
	crcAddr += (uint32_t) & __data_load_start; // Location for .data segment in Flash
	crcAddr += (uint32_t) 0x10000; // In second 64K

	// Get bytes from FLASH up to the address of the stored CRC value
	for ( i = 0; i < crcAddr; i++ )
		{
		c = i;
		crc = crc_update( crc, &c, 1 );
		}

	// Skip over the CRC value
	i += sizeof( g_tbImageCrc );

	// Continue CRC calculation following pre-calculated CRC location
	for ( ; i <= (uint32_t) 111111111; i++ )
		{
		c = i;
		crc = crc_update( crc, &c, 1 );
		}

	// Compared found CRC to pre-calculated CRC
	if ( crc != g_tbImageCrc )
		{
		return ERR_ERROR;
		}

	return ERR_OK;
}

/**
 * @brief	This method returns the percent completion of the self test operation
 *
 * @param	pbProgress A pointer to receive the percentage as byte*
 *
 * @return   Status as upErr
 */
upErr CSelfTest::GetProgress( byte * pbProgress )
{
	*pbProgress = m_bProgress;
	if ( *pbProgress > 100 )
		*pbProgress = 100;
	return ERR_OK;
}

/**
 * @brief	This method sets the test cycle counter
 *
 * @param	tbTestCycle The new cycle count as twobytes
 *
 * @return  ERR_OK
 */
upErr CSelfTest::SetTestCycle( twobytes tbTestCycle )
{
	m_testCycle = tbTestCycle;
	if ( 0 == tbTestCycle )
		{
		m_initiallyRetracted = false;
		m_testCycle = 0;
		m_eState = GEN_STATE_START;
		m_eWaterPumpErr = ERR_OK;
		m_eCpldErr = ERR_OK;
		m_bProgress = 0;
		}

	return ERR_OK;
}

// Private Class Functions



/**
 * @file	GeneratorMonitor.cpp
 * @par		Package: UGMcu
 * @par		Project: Union Generator
 * @par		Copyright (c) 2017, Uptake Medical Technology, Inc.
 * @author	Glen Oct 21, 2010 Created file
 * @brief	This file contains the definition for CGeneratorMonitor
 */

// Include Files
#include "GeneratorMonitor.h"
#include "DigitalInputOutput.h"
#include "OneWireMemory.h"
#include "TreatmentInformation.h"
#include "LogMessage.h"
#include "RFGenerator.h"
#include "AblationControl.h"
#include "SbcIntfc.h"
#include "WaterCntl.h"
#include "Temperature.h"
#include "HandpiecePrime.h"

// External Public Data
CGeneratorMonitor * g_pGeneratorMonitor = 0;

// File Scope Macros and Constants

// File Scope Type Definitions (Enums, Structs & Classes)
#define RDO_POWER_LEVEL_THRESHOLD	(100)

// File Scope Data

// File Scope Functions

// Public Functions

/**
 * @brief	This is the constructor for CGeneratorMonitor.
 *
 * It captures the @a this pointer
 * 			to g_pGeneratorMonitor for access of other classes by a singleton
 *
 */
CGeneratorMonitor::CGeneratorMonitor()
{
	g_pGeneratorMonitor = dynamic_cast<CGeneratorMonitor*>( this );
	m_tDebounceOffTick = 0;
	m_bLastDebounce = false;
	m_bPowerOnErrorCnt = 0;
	m_bPowerRunningErrorCnt = 0;
	m_bPowerRangeErrorCnt = 0;
	m_bOverTempCnt = 0;
	m_eHandpieceConnection = GEN_STATE_START;
	m_bThermoUnconnectCount = 0;
	m_bOverMaxCnt = 0;
	m_bRfGenFailureCnt = 0;
}

/**
 * @brief	This method check the generator for a gross failure in either accessing the I/O hardware
 * 			or in the values received.
 *
 * @param eInterlockNeeded The interlock bits needed to be a good generator state as upInterlockFlag
 *
 * @return   Status as upErr
 */
upErr CGeneratorMonitor::CheckGenerator( upInterlockFlag eInterlockNeeded )
{
	upErr eErr;
	upInterlockFlag eInter;

	// Monitor criticals first!
	LOG_ERROR_RETURN( eErr = PerformCriticalMonitoring() )
	if ( ERR_OK != eErr )
		// return immediately!
		return eErr;

	// Check whether the RF generator should be reset
	if ( g_pRFGen->ResetRequired() )
		{
		return ERR_RF_GEN_RESET;
		}

	// Check interlocks.
	LOG_ERROR_RETURN( eErr = CheckInterlocks( true, &eInter, false ) )
	if ( ERR_OK == eErr && eInterlockNeeded != ( eInterlockNeeded & eInter ) )
		{
		LOG_ERROR_RETURN( eErr = ERR_INTERLOCK_MISSING )
		}

	// Check RF Generator
	if ( ERR_OK == eErr )
		{
		LOG_ERROR_RETURN( eErr = CheckRF() )
		} // RF gen check

	// Return extended error code if no other errors
	if ( ERR_OK == eErr )
		{
		bool_c bExt;
		LOG_ERROR_RETURN( eErr = g_pDigitalInputOutput->GetInput( DIG_IN_MOTOR_EXTEND, &bExt ) );
		if ( ERR_OK == eErr && bExt )
			{
			LOG_ERROR_RETURN( eErr = ERR_PUMP_AT_LIMIT )
			}
		}

	// Check over temp always
	upErr eErr2;
	eErr2 = CheckOverTemp();
	if ( ERR_OK != eErr2 )
		eErr = eErr2;

	return eErr;
}

/**
 * @brief	This method checks the generator to construct the interlock mask.
 *
 * This is done by checking each sensible component. It reads the syringe
 * switch. The 1-wire is checked for presence. The SBC must be communicating.
 * The RF generator must be reporting no errors.@n
 * Another operation is that if all other interlocks have been made then the AC power will be
 * applied to the RF generator. Doing this is controlled by the bEnableRfAC parameter.
 *
 * @param bThermoUnconnError A bool_c the is TRUE if an unconnected TC should post a fault and FALSE if not
 * @param pInter A pointer to an upInterlockFlag enumeration to receive the current set of made interlocks
 * @param bEnableRfAC True if the AC power to the RF generator should be turned on when all interlocks are made
 *
 * @return   Status as upErr
 */
upErr CGeneratorMonitor::CheckInterlocks( bool_c bThermoUnconnError, upInterlockFlag * pInter, bool_c bEnableRfAC )
{
	upErr eErr;
	upErr eReturnErr = ERR_OK;
	upRfCoilStatus eStatus = RF_COIL_NOT_READY;
	bool_c bOn;

	*pInter = INTLCK_NONE;

	// Syringe is installed
	g_pDigitalInputOutput->GetInput( DIG_IN_SYR_PRESENT, &bOn );
	if ( bOn )
		{
		*pInter = (upInterlockFlag) ( INTLCK_SYRINGE | INTLCK_UNUSED_4);
		}

	// Sense the pressure puck just to show action in UI
	g_pDigitalInputOutput->GetInput( DIG_IN_UNUSED_0, &bOn );
	if ( bOn )
		{
		*pInter = (upInterlockFlag) ( (byte) INTLCK_UNUSED_4 | (byte) *pInter );
		}

	// 1-Wire
	LOG_ERROR_RETURN( eErr = g_pOneWireMemory->CheckForHandpiece( &bOn ) )

	if ( false == bOn )
		{
		m_eHandpieceConnection = GEN_STATE_START;
		m_bThermoUnconnectCount = 0;
		}

	// 1-wire reported as connected so check thermocouples
	else
		{
		int iTemp;
		eErr = g_pTemperature->GetThermoTemp( THERMO_COIL, &iTemp );
		if ( ERR_OK == eErr )
			eErr = g_pTemperature->GetThermoTemp( THERMO_OUTLET, &iTemp );

		// Both connected so mark as connected
		if ( ERR_OK == eErr )
			{
			m_eHandpieceConnection = GEN_STATE_COMPLETE;
			}

		// Not both connected
		else
			{

			// Was connected and now open so mark as clean disconnect
			if ( GEN_STATE_ERROR == m_eHandpieceConnection || GEN_STATE_COMPLETE == m_eHandpieceConnection )
				{
				m_eHandpieceConnection = GEN_STATE_ERROR;
				m_bThermoUnconnectCount = 0;

				// Report if unconnecting is NOT a reportable error
				if ( bThermoUnconnError )
					eReturnErr = ERR_THERMO_INTERLOCK_MISSING;
				}

			// Waiting for connection. Give it THERMO_UNCONNECT_MAX tries for both TC to become good
			else
				{
				m_bThermoUnconnectCount++;
				if ( GEN_STATE_START == m_eHandpieceConnection && THERMO_CONNECT_TOL == m_bThermoUnconnectCount )
					{
					m_eHandpieceConnection = GEN_STATE_ERROR;
					eReturnErr = eErr;
					}
				else
					{
					eErr = ERR_OK;
					}
				} // Waiting for TC
			} // TC check
		} // 1-wire connected

	// Only mark as interlocked once TCs are connected and stay connected
	if ( ERR_OK == eErr && GEN_STATE_COMPLETE == m_eHandpieceConnection )
		{
		*pInter = (upInterlockFlag) ( (byte) INTLCK_HANDPIECE | (byte) *pInter );
		}

	// SBC communication
	upSbcState eSbcState;
	LOG_ERROR_RETURN( eErr = g_pSbcIntfc->ReadSbcState( &eSbcState ) )
	if ( ERR_OK == eErr && SBC_STATE_OOS != eSbcState )
		{
		*pInter = (upInterlockFlag) ( (byte) INTLCK_SBC | (byte) *pInter );
		}
	else
		eReturnErr = eErr;

	// Turn on the RF generator once all other interlocks are made
	if ( bEnableRfAC )
		{
		g_pRFGen->SetACInput( ( INTLCK_ALL & ~INTLCK_COIL ) == *pInter ? true : false );
		}

	// Check the coil status
	eStatus = g_pRFGen->GetCoilStatus();
	if ( RF_COIL_ERROR == eStatus )
		{
		eReturnErr = ERR_DDEVICE_COIL;
		}
	else if ( RF_COIL_ERR_OK == eStatus )
		{
		*pInter = (upInterlockFlag) ( (byte) INTLCK_COIL | (byte) *pInter );
		}

	return eReturnErr;
}

/**
 * @brief	This method debounces the trigger off operation.
 *
 * It loERR_OKs for the trigger being OFF for a TDeb period.
 * The supplied parameter receives the debounced evaluation of the trigger.
 *
 * @param	pbOn A bool_c * to get the debounced trigger state
 *
 * @return   Status as upErr
 */
upErr CGeneratorMonitor::GetDebouncedTrigger( bool_c * pbOn )
{
	upErr eErr = ERR_OK;
	bool_c bTemp;

	eErr = g_pDigitalInputOutput->GetInput( DIG_IN_TRIGGER, pbOn );

#ifdef ALLOW_EMC_TREATMENT_CODE
	upTreatmentCode eTherapyCode;
	g_pOneWireMemory->GetTherapyCode( &eTherapyCode );
	if ( TREATMENT_EMC == eTherapyCode )
		{
		if ( g_pTreatmentInfo->getEmcCycles() > 0 )
		*pbOn = true;
		}
#endif

	bTemp = *pbOn;

	// If OFF then debounce
	if ( false == *pbOn )
		{
		// Just went OFF so grab ticks and set back ON
		if ( m_bLastDebounce )
			{
			eErr = GetTick( &m_tDebounceOffTick );
			*pbOn = true;
			}

		// Check for debounce time elapsing
		else
			{
			twobytes tbTickDiff;
			GetTickElapsed( m_tDebounceOffTick, &tbTickDiff );
			if ( MS_TO_TICKS( g_pTreatmentInfo->getTdeb() ) > tbTickDiff )
				*pbOn = true;
			}
		}

	m_bLastDebounce = bTemp;
	return eErr;
}

/**
 * @brief	This method checks the operation of the RF generator.
 *
 * This includes checking the status code and then, if the generator is on, loERR_OKing at the power output level and
 * power ON state. The power level is checked for being close to the requested power and for being zero when the
 * generator should be off.
 * Counters are used to allow some lag between requesting power ON/OFF and specific power levels. The
 * response must be bad for RF_STATUS_LAG_TOLERANCE attempts before an error is raised.@n
 * If the RF generator is found to have produced an error then the AC power to it is shut off.@n
 * This method also checks for the coil errors. This will produce a user error.
 *
 * @return   Status as upErr
 */
upErr CGeneratorMonitor::CheckRF()
{
	upErr eErr = ERR_OK;
	upRfCoilStatus eStatus;
	bool_c bOn;
	bool_c bRfOn;
	twobytes tbPowerReported;
	twobytes tbPowerRequested;

	// First check the coil status and exit if a bad coil is returned
	eStatus = g_pRFGen->GetCoilStatus();
	if ( RF_COIL_ERROR == eStatus )
		return ERR_DDEVICE_COIL;

	//
	// Check for power ON/OFF status being ON and OFF correctly
	//
	g_pRFGen->GetApplyingPower( &bRfOn );
	g_pRFGen->GetOutputEnable( &bOn );
	g_pRFGen->GetPowerOutput( &tbPowerReported );
	tbPowerRequested = g_pRFGen->GetAppliedPowerRequest();


	// Check for improperly ON
	if ( true == bRfOn && false == bOn )
		{
		if ( ++m_bPowerRunningErrorCnt > RF_STATUS_LAG_TOLERANCE )
			{
			LOG_ERROR_RETURN( eErr = ERR_RF_GEN_POWER_LEVEL );
			}
		}

	// Check for improperly OFF. Must also have non-zero power requested.
	else if ( false == bRfOn && true == bOn && 0 < tbPowerRequested )
		{
		if ( ++m_bPowerRunningErrorCnt > RF_STATUS_LAG_TOLERANCE )
			{
			LOG_ERROR_RETURN( eErr = ERR_RF_GEN_POWER_LEVEL );
			}
		}
	else if ( 0 < m_bPowerRunningErrorCnt )
		{
		m_bPowerRunningErrorCnt--;
		}

	//
	// Checks for power level from RF generator
	//

	// Is power both off and non-zero?
	if ( false == bOn && tbPowerReported > 0 )
		{
		if ( ++m_bPowerOnErrorCnt > RF_STATUS_LAG_TOLERANCE )
			{
			LOG_ERROR_RETURN( eErr = ERR_RF_GEN_POWER_LEVEL );
			}
		}
	else if ( m_bPowerOnErrorCnt > 0 )
		m_bPowerOnErrorCnt--;

	// Check for if power is applied and the reported power is out-of-range
	twobytes tbUpperLim;
	twobytes tbLowerLim;
	twobytes tbPowerTol = g_pTreatmentInfo->getRfPowerTol();

	tbUpperLim = tbPowerRequested + tbPowerTol;
	tbLowerLim = ( tbPowerRequested < tbPowerTol ? 0 : tbPowerRequested - tbPowerTol ); // Don't use power limit that is less than zero with unsigned values

	// Use wider values for RDO as it reports controlled power that will oscillate
	upRFGeneratorModel eModel;
	g_pRFGen->GetRfGenModel( &eModel );
	if ( RF_MODEL_RDO == eModel )
		{
		tbUpperLim = tbPowerRequested + RDO_POWER_LEVEL_THRESHOLD;
		// Divide power by three to bracket expected RDO output during initial part of control or use zero below 100 VA
		tbLowerLim = ( tbPowerRequested > RDO_POWER_LEVEL_THRESHOLD ? tbPowerRequested / 3 : 0 );
		}

	// Check for power being inside acceptable band
	if ( bOn && ( tbPowerReported > tbUpperLim || tbPowerReported < tbLowerLim ) )
		{
		// Give 10 tries for power to get into band
		if ( ++m_bPowerRangeErrorCnt > RF_STATUS_LAG_TOLERANCE )
			{
			LOG_ERROR_RETURN( eErr = ERR_RF_GEN_POWER_LEVEL );
			g_pLogMessage->LogErrorMessageInt( eErr, "Requested", (int) tbPowerRequested );
			g_pLogMessage->LogErrorMessageInt( eErr, "Reported", (int) tbPowerReported );
			}
		}
	else if ( m_bPowerRangeErrorCnt > 0 )
		m_bPowerRangeErrorCnt--;

	return eErr;
}

/**
 * @brief	This method checks the coil and outlet temperatures for being out-of-range.
 *
 * @return   Status as upErr
 */
upErr CGeneratorMonitor::CheckOverTemp()
{
	upErr eErr;
	int iCoilTemp;
	int iOutletTemp;

	// Check coil and outlet temperatures for excessive and high temperatures
	LOG_ERROR_RETURN( eErr = g_pTemperature->GetThermoTemp( THERMO_COIL, &iCoilTemp ) )
	if ( ERR_OK == eErr )
		LOG_ERROR_RETURN( eErr = g_pTemperature->GetThermoTemp( THERMO_OUTLET, &iOutletTemp ) )

	if ( ERR_OK == eErr )
		{

		// Check for over maximum temperature limits
		LOG_ERROR_RETURN( eErr = CheckCriticalTemperatures() )

		// Check normal upper temp limits against filtered values
		if ( ERR_OK == eErr )
			{
			int iDuration = MS_TO_RND_ROBIN_CYC( g_pTreatmentInfo->getTtempWindow() );
			int iBand = g_pTreatmentInfo->getOverTempBand();
			LOG_ERROR_RETURN( eErr = g_pTemperature->GetFilteredThermoTemp( THERMO_COIL, &iCoilTemp ) )
			LOG_ERROR_RETURN( eErr = g_pTemperature->GetFilteredThermoTemp( THERMO_OUTLET, &iOutletTemp ) )
			if ( iCoilTemp > g_pTreatmentInfo->getCoilTempUpper() || iOutletTemp > g_pTreatmentInfo->getOutletTempMax() )
				{
				if ( iDuration == ++m_bOverTempCnt )
					{
					LOG_ERROR_RETURN( eErr = ERR_THERMO_OVER_TEMP )
					}

				// Report continuing error but no user error reported
				else if ( iDuration < m_bOverTempCnt )
					{
					eErr = ERR_THERMO_OVER_TEMP_PERSIST;
					m_bOverTempCnt = iDuration + 1;
					}
				}

			// Check for falling below targets minus band
			else if ( iDuration <= m_bOverTempCnt && //
			        ( iCoilTemp > g_pTreatmentInfo->getCoilTempUpper() - iBand || //
			                iOutletTemp > g_pTreatmentInfo->getOutletTempMax() - iBand ) )
				{
				eErr = ERR_THERMO_OVER_TEMP_PERSIST;
				}
			else
				m_bOverTempCnt = 0;

			}
		}

	return eErr;
}

/**
 * @brief	This method performs check to determine if the system is fully ready beyond interlocks and
 * 			delivery device checks.
 *
 * @param	bLastTrig The last trigger state
 * @param	tLastEnd The last end of a treatment as a tick
 * @param	pPendingSubstate A pointer to a upPendingReady to receive the evaluation
 * @param   pbProgress A pointer to a caller-allocated byte to receive the progress percentage
 *
 * @return   Status as upErr
 */
upErr CGeneratorMonitor::PendingReadyCheck( bool_c bLastTrig, tick tLastEnd, upPendingReady * pPendingSubstate,
        byte * pbProgress )
{
	upErr eErr = ERR_OK;

	*pPendingSubstate = PENDING_READY_ERR_OK;

	//
	// Perform checks in reverse order of importance
	//
	if ( bLastTrig )
		{
#ifdef ALLOW_EMC_TREATMENT_CODE
		upTreatmentCode eTherapyCode;
		g_pOneWireMemory->GetTherapyCode( &eTherapyCode );
		if ( TREATMENT_EMC != eTherapyCode )
#endif
		*pPendingSubstate = PENDING_READY_TRIG_ON;
		}

	// SBC must be on right screen
	upSbcState eSbcState;
	g_pSbcIntfc->ReadSbcState( &eSbcState );
	if ( SBC_STATE_DELIVERY != eSbcState && SBC_STATE_TECH_DIAG != eSbcState )
		{
		*pPendingSubstate = PENDING_READY_WRONG_SBC;
		}

	// Wait for Trest period before READY
	tick tDiff;
	tick tTotal = SEC_TENTHS_TO_TICKS( g_pTreatmentInfo->getTrest() );
	GetTickElapsed( tLastEnd, &tDiff );
	if ( tDiff < tTotal )
		{
		*pPendingSubstate = PENDING_READY_WAIT;
		}

	// Compute percentage complete and clamp at 100
	*pbProgress = (byte) ( ( 100 * tDiff ) / tTotal );
	if ( *pbProgress > 100 )
		*pbProgress = 100;

	// Wait out coil over temp
	// New errors are found in CGeneratorMonitor::CheckOverTemp()
	int iDuration = MS_TO_RND_ROBIN_CYC( g_pTreatmentInfo->getTtempWindow() );
	int iBand = g_pTreatmentInfo->getOverTempBand();
	int iCoilTemp = 0;
	int iOutletTemp = 0;
	g_pTemperature->GetFilteredThermoTemp( THERMO_COIL, &iCoilTemp );
	g_pTemperature->GetFilteredThermoTemp( THERMO_OUTLET, &iOutletTemp );
	if ( iDuration <= m_bOverTempCnt
	        && ( iCoilTemp > g_pTreatmentInfo->getCoilTempUpper() - iBand
	                || iOutletTemp > g_pTreatmentInfo->getOutletTempMax() - iBand ) )
		{
		// Temperature is at or above the lower band limit - stay in Pending Ready state
		eErr = ERR_THERMO_OVER_TEMP_PERSIST;
		*pPendingSubstate = PENDING_READY_OVER_TEMP;
		}
	else if ( ( m_bOverTempCnt >= iDuration ) &&  //
	        ( ( iCoilTemp < g_pTreatmentInfo->getTempUpperLimitIdle() - iBand ) &&  //
	                ( iOutletTemp < g_pTreatmentInfo->getTempUpperLimitIdle() - iBand ) ) )
		{
		// Temperature is below lower band limit - clear over temp counter
		m_bOverTempCnt = 0;
		}

	return eErr;
}

// Private Class Functions

/**
 * @brief	This method checks the coil and outlet temperatures for being above the generator shutdown level.
 *
 * @return   Status as upErr
 */
upErr CGeneratorMonitor::CheckCriticalTemperatures()
{
	upErr eErr = ERR_OK;
	bool_c bPres;
	int iCoilTemp;
	int iOutletTemp;

	// Check coil and outlet temperatures if handpiece is present
	g_pOneWireMemory->CheckForHandpiece( &bPres );
	if ( bPres && ERR_OK == g_pTemperature->GetThermoTemp( THERMO_COIL, &iCoilTemp ) //
	&& ERR_OK == g_pTemperature->GetThermoTemp( THERMO_OUTLET, &iOutletTemp ) )
		{
		if ( iCoilTemp > g_pTreatmentInfo->getCoilTempUpper() + g_pTreatmentInfo->getCoilTempShutdownTol() //
		|| iOutletTemp > g_pTreatmentInfo->getOutletTempMax() + g_pTreatmentInfo->getOutletTempShutdownTol() )
			{
			m_bOverMaxCnt++;
			if ( m_bOverMaxCnt == OVER_MAX_TEMP_CNT )
				LOG_ERROR_RETURN( eErr = ERR_EXCESSIVE_TEMP )
			}
		else
			m_bOverMaxCnt = 0;

		} // Interlock, Coil and Outlet ERR_OK
	else
		m_bOverMaxCnt = 0;

	return eErr;
}

/**
 * @brief	This method checks critical components of the system that must be
 *  		within specified boundaries and operating conditions.  An error
 *  		is only returned if a critical error is found.
 *
 * @return   Status as upErr. ERR_OK == No critical errors found.
 */
upErr CGeneratorMonitor::PerformCriticalMonitoring()
{
	upErr eErr = ERR_OK;

	// First check SBC/MCU I/O health
	LOG_ERROR_RETURN( eErr = g_pSbcIntfc->CheckForTimeout() )
	if ( ERR_OK != eErr )
		return eErr;

	// Check Internal temperature
	LOG_ERROR_RETURN( eErr = g_pGeneratorMonitor->CheckInternalTemperature() )
	if ( ERR_OK != eErr )
		{
		return eErr;
		}

	// Only check for the excessive temps
	LOG_ERROR_RETURN( eErr = CheckCriticalTemperatures() )
	if ( ERR_OK != eErr )
		return eErr;

#ifdef OLD_RF_ERROR_SCHEME
	LOG_ERROR_RETURN( eErr = g_pRFGen->GetError( &rfErrThrowAway ) )
	if ( ERR_RF_GEN_NOT_READY == eErr )
	eErr = ERR_RF_GEN_COMM_FATAL;

	if ( ERR_DDEVICE_COIL == eErr || ERR_RF_GEN_NOT_FOUND == eErr //
			|| ERR_RF_GEN_RESET == eErr || ERR_RF_GEN_COMM_FATAL == eErr )
	return eErr;
#else
	// Check for fatal RF generator error
	LOG_ERROR_RETURN( eErr = g_pRFGen->GetCriticalError() );
	if ( ERR_OK != eErr )
		return eErr;
#endif

	return eErr;
}

/**
 * @brief	This method checks the internal temperature of the generator for being below the
 * 			accepted operating threshold.
 *
 * @return   Status as upErr
 */
upErr CGeneratorMonitor::CheckInternalTemperature()
{
	upErr eErr = ERR_OK;
	int iTemp;

	// Only check TC status on newer boards
	if ( MCU_BOARD_D == GetBoardRevision() )
		{
		LOG_ERROR_RETURN( eErr = g_pTemperature->GetFilteredThermoTemp( THERMO_INTERIOR, &iTemp ) )
		}
	else
		{
		g_pTemperature->GetFilteredThermoTemp( THERMO_INTERIOR, &iTemp );
		}

	if ( ERR_OK == eErr )
		{
		if ( iTemp > g_pTreatmentInfo->getMaxInternalTemp() )
			LOG_ERROR_RETURN( eErr = ERR_GENERATOR_OVER_TEMP )
		}

	return eErr;
}

/**
 * @brief	This method sets the Over Temperature Counter value.
 *
 * @param	bOverTempCnt The overtemp byte value to set
 *
 */
upErr CGeneratorMonitor::SetOverTempCnt( byte bOverTempCnt )
{
	m_bOverTempCnt = bOverTempCnt;
	return ERR_OK;
}

/**
 * @brief	This method provides the mask of interlock bits needed for the active state.
 * 			Once the plunger is extended an open syringe is ignored.
 *
 * @param	eOperState The state of interest as a upOperState
 * @param	peInterlockNeeded The interlocks needed as a mask and a upInterlockFlag*
 *
 * @return   Status as upErr
 */
upErr CGeneratorMonitor::GetNeededInterlocks( upOperState eOperState, upInterlockFlag* peInterlockNeeded )
{
	upErr eErr = ERR_OK;

	switch ( eOperState )
		{

		case OPER_STATE_BOOT:
		case OPER_STATE_TEST:
		case OPER_STATE_IDLE:
		case OPER_STATE_CNCTD:
		case OPER_STATE_HAND_PRIMING:
			*peInterlockNeeded = INTLCK_ALL;
			break;

		case OPER_STATE_HAND_PRIMED:
		case OPER_STATE_CATH_PRIMING:
		case OPER_STATE_SETUP:
		case OPER_STATE_PENDING:
		case OPER_STATE_READY:
		case OPER_STATE_PRECOND:
		case OPER_STATE_DELIVERING:
			*peInterlockNeeded = INTLCK_ALL_NO_SYR;
			break;

		case OPER_STATE_RELEASE:
		case OPER_STATE_ERROR:
			*peInterlockNeeded = INTLCK_ALL;
			break;

		default:
			*peInterlockNeeded = INTLCK_ALL;
			LOG_ERROR_RETURN( eErr = ERR_BAD_PARAM )
			break;
		}

	return eErr;
}

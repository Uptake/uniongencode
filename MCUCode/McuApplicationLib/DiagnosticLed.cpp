/**
 * @file       DiagnosticLed.cpp
 * @par        Package: UGMcu
 * @par        Project: Union Generator
 * @par        Copyright (c) 2017, Uptake Medical Technology, Inc.
 * @author     Glen Sep 15, 2010 Created file
 * @brief      This code controls the 4 diagnostic LEDs mounted to the board and implements CDiagnosticLed
 */

// Include Files
#include "UGMcuDefinitions.h"
#include "DiagnosticLed.h"

// External Public Data
CDiagnosticLed * g_pDiagnosticLed = 0;

// File Scope Macros and Constants

// File Scope Type Definitions (Enums, Structs & Classes)

// File Scope Data

// File Scope Functions

// Public Functions

/**
 * @brief	This is the constructor for CDiagnosticLed.
 *
 * It captures the "this" pointer to g_pDiagnosticLed and sets the maps for LED enumerations to port bits.
 *
 */
CDiagnosticLed::CDiagnosticLed()
{
	int i;
	g_pDiagnosticLed = this;
	for ( i = 0; i < DIAG_LED_CNT; i++ )
		{
		m_ledBit[ i ] = ( 1 << ( i + 4 ) );
		}
}

/**
 * @brief	This method initializes the 4 LED pins in PORTD to be outputs
 *
 * For all board revisions these are the upper four bits (0xF0)
 *
 * @return   Status as AtErrorCode
 */

upErr CDiagnosticLed::InitializeHardware()
{
	return ERR_OK;
}

/**
 * @brief	This method sets an LED value immediately. The change does not wait
 * 			for the next WriteToHardware() call.
 *
 * @param eLed		An enumeration giving which LED to set
 * @param bOnOff	The state to set (true->ON)
 *
 * @return   Status as upErr. ERR_ENUM_OUT_OF_RANGE if bad enum value is supplied or ERR_OK.
 */
upErr CDiagnosticLed::SetLED( upDiagLed eLed, bool_c bOnOff )
{
	byte bVal = 0;
	if ( eLed >= DIAG_LED_CNT )
		return ERR_ENUM_OUT_OF_RANGE;

	bVal = 1;
	if ( bOnOff )
		bVal |= m_ledBit[ eLed ];
	else
		bVal &= ~m_ledBit[ eLed ];
	bVal = bVal + 1; //fix

	return ERR_OK;
}

// Private Class Functions

/**
 * @file	OneWireMemory.cpp
 * @par		Package: UGMcu
 * @par		Project: Union Generator
 * @par		Copyright (c) 2017, Uptake Medical Technology, Inc.
 * @author	Glen Sep 16, 2010 Created file
 * @brief	This file contains the code that manages the 1-Wire memory.
 */

// Include Files
#include "UGMcuDefinitions.h"
#include "OneWireMemory.h"
#include "TreatmentRecord.h"
#include "RFGenerator.h"
#include "SbcIntfc.h"
#include "string.h"
// External Public Data
COneWireMemory * g_pOneWireMemory = 0;

// File Scope Macros and Constants

// File Scope Type Definitions (Enums, Structs & Classes)
#define DEBOUNCE_MS ( 350 )

// File Scope Data

// File Scope Functions

// Public Functions

/**
 * @brief	This is the constructor for COneWireMemory
 *
 */
COneWireMemory::COneWireMemory()
{
	g_pOneWireMemory = this;

	m_lastEntryID = 0;
	ClearOwValues();
	m_writeAccessActive = false;
	// The one-wire monitor is enabled via the supervisor
	m_bEnableOwMonitor = false;
	m_lastDetachedTick = 0;
	m_eErrorState = ERR_OK;
	m_eOwState = OW_STATE_DISCONNECTED;
	// Initialize the ROM ID byte array
	for (int i = 0; i < OW_ROM_SIZE; i++ )
		m_romInfo[i] = 0;
}

/**
 * @brief	This method initializes the I2C hardware.
 *
 * @return   Status as upErr
 */
upErr COneWireMemory::InitializeHardware()
{
	upErr eErr = ERR_OK;

	eErr = COneWireDriver::InitializeHardware();

	return eErr;
}

/**
 * @brief	This method is the main state machine handler for read and write events stored in the FIFO event queue.
 * 			In addition to event queue handling, the Execute method also determines whether a new therapy
 *          information update event should be queued; new therapy information is available if the record
 *    		index in the therapy record is out-of-synch with the current index stored in the OneWireMemory
 *    		class.
 *
 *
 * @return   Status as upErr
 */
upErr COneWireMemory::Execute()
{
	upErr eErr = ERR_OK;
	twobytes recordEntry = 0;
	unsignedfourbytes ulTreatmentTime = 0;
	SOwState sOwState;
	upDeliveryStatus eTreatStatus;

	if ( OW_STATE_DISCONNECTED == m_eOwState )
		return ERR_OK;

	// If the monitor has been turned off, return immediately
	if ( !m_bEnableOwMonitor )
		return ERR_OK;

	eTreatStatus = g_pTreatmentRecord->GetLastTreatmentStatus();
	recordEntry = g_pTreatmentRecord->GetEntry();

	// check whether we are in sync with the Treatment Record, if not,
	// update the delivery tool information
	// always update the records first before entering the state machine logic
	if ( m_lastEntryID != recordEntry && OW_STATE_READY == m_eOwState
			&& eTreatStatus != DELIVERY_NONE )
		{
		m_lastEntryID = recordEntry;

		if ( DELIVERY_FULL == g_pTreatmentRecord->GetLastTreatmentStatus() )
			SetFullTreatmentCount( m_fullTreatmentCount + 1 );

		ulTreatmentTime = (unsignedfourbytes)g_pTreatmentRecord->GetLastTreatmentTime();
		SetTherapyTime( m_therapyTime + ulTreatmentTime );

		eErr = AddEvent( OW_EVENT_DATA_CHANGE );
		if ( eErr != ERR_OK )
			return eErr;
		}

	// Getting the next state is triggered when a substate has reached the end state
	// or the top state is IDLE
	sOwState = GetCurrentState();
	if ( GEN_STATE_END == sOwState.eSubState || OW_STATE_IDLE == sOwState.eTopState )
		{
		sOwState = GetNextTopState();
		m_owErrPass = 0;
		}

	switch ( sOwState.eTopState )
		{
		case OW_STATE_WRITE_FIRSTUSE:
			if ( ERR_OK != ( eErr = StampFirstUse() ) )
				sOwState = EvaluateWriteError();
			// when READ_ALL occurs, it will lock the first use time
			// if the bank failed to lock
			break;
		case OW_STATE_WRITE_TREATMENT:
			// Set full treatment count and overall therapy time
			// need to write these all in one-block for faster write times
			if ( ERR_OK != ( eErr = WriteTreatment() ) )
				sOwState = EvaluateWriteError();
			break;
		case OW_STATE_WRITE_LOCKDOWN:
			if ( ERR_OK != ( eErr = WriteLockdown() ) )
				sOwState = EvaluateWriteError();
			break;
		case OW_STATE_READ_ALL:
			if ( ERR_OK != ( eErr = ReadAll() ) )
				sOwState = EvaluateReadError();
			break;
		case OW_STATE_IDLE:
		default:
			eErr = ERR_OK;
			break;
		}

	return eErr;
}

/**
 * @brief	This method reads data from the onw-wire memory.
 *
 *      The ReadFromHardware method performs the following functions:
 * 			1) Monitors for the presence of the one-wire memory
 * 			2) Debounces the one-wire during a connection
 * 			3) Queues the new one-wire memory event upon a successful connection
 * 			4) Evaluates whether a read error should be logged or ignored
 * 			If a read error is waiting to be evaluated, this method will check for the
 * 			presence of the one-wire before returning the error.
 * 			If the one-wire memory monitor is disabled, the method will return immediately.
 *
 * @return   Status as upErr
 */
upErr COneWireMemory::ReadFromHardware()
{
	upErr eErr = ERR_OK;
	SOwState sOwState;

	// If the monitor is disabled, return immediately
	if ( !m_bEnableOwMonitor )
		return ERR_OK;

	// don't reset the one-wire if a state is active!
	sOwState = GetCurrentState();
	if ( sOwState.eTopState != OW_STATE_IDLE && ERR_OK == m_eErrorState )
		return ERR_OK;

	// write access active is used by projects using the shared lib
	if ( 0 < g_bTestAccess && m_writeAccessActive )
		return ERR_OK;

	RETURN_IF_ERROR( COneWireDriver::ReadFromHardware() )

	bool_c bPresenceCheck = false;
	// Check for presence.  If present, is this different than the last result?
	// If so and it's false, reset the event queues and clear cached one-wire values.
	// If so and it's true, queue the new memory event to read memory into the cache.

	// debounce required before connection state
	GetOwPresence( &bPresenceCheck );

	// Only debounce when making a connection
	if ( true == bPresenceCheck && OW_STATE_DISCONNECTED == m_eOwState )
		{
		tick tDiff;
		GetTickElapsed( m_lastDetachedTick, &tDiff );
		if ( tDiff >= MS_TO_TICKS( DEBOUNCE_MS ) )
			{
			// Successfully debounced on connect
			AddEvent( OW_EVENT_NEW_MEMORY );
			m_eOwState = OW_STATE_CONNECTED;
			}
		}
	else if ( false == bPresenceCheck )
		{
		ResetEventQ();
		m_eOwState = OW_STATE_DISCONNECTED;
		ClearOwValues();
		// set the last known tick where "no presence" was sensed
		GetTick( &m_lastDetachedTick );
		}
	else if ( ERR_OK != m_eErrorState )
		{
		// the device is still present, so report the error and reset the state
		LOG_ERROR_RETURN( m_eErrorState )
		m_eErrorState = ERR_OK;
		m_eOwState = OW_STATE_CONNECTED;
		SetSubState( GEN_STATE_END );
		}

	return eErr;
}

/**
 * @brief	This method checks the initialization result of the one-wire I2C hardware.
 *
 * @return   Status as upErr:
 * 			 ERR_OK == HW initialization complete.
 * 			 ERR_I2C_DEVICE_ERROR == HW initialization failed.
 */
upErr COneWireMemory::CheckHardware()
{
	if ( ReturnHwHealth() )
		return ERR_OK;
	else
		return ERR_I2C_DEVICE_ERROR;
}

/**
 * @brief	This method handles invalidation of devices due to write errors.
 *    		The method also ensures a write error gets OW_WRITEPASS_LIMIT
 *    		chances to try the write before failing.
 *
 * @return  The top-state substate as SOwState
 */
SOwState COneWireMemory::EvaluateWriteError()
{
	SOwState sOwState;

	if ( OW_WRITEPASS_LIMIT <= m_owErrPass )
		{
		sOwState = SetSubState( GEN_STATE_END );
		m_owErrPass = 0;
		// report write errors immediately
		LOG_ERROR_RETURN( ERR_DDEVICE_WRITE_ERROR )
		m_eOwState = OW_STATE_CONNECTED;
		}
	else
		{
		m_owErrPass++;
		sOwState = SetSubState( GEN_STATE_START );
		}

	return sOwState;
}

/**
 * @brief	This method handles invalidation of devices due to read errors.
 *    		The method also ensures a write error gets OW_READPASS_LIMIT
 *    		chances to try the read before failing.
 *
 * @return  The top-state substate as SOwState
 */
SOwState COneWireMemory::EvaluateReadError()
{
	SOwState sOwState;

	if ( OW_READPASS_LIMIT <= m_owErrPass )
		{
		sOwState = SetSubState( GEN_STATE_END );
		m_owErrPass = 0;
		// This will be reported by ReadHardware if the device is still connected
		m_eErrorState = ERR_DDEVICE_READ_ERROR;
		m_eOwState = OW_STATE_CONNECTED;
		}
	else
		{
		m_owErrPass++;
		sOwState = SetSubState( GEN_STATE_START );
		}

	return sOwState;
}

/**
 * @brief	This method stamps the one-wire with a first use time stamp in Unix epoch time in seconds.
 *    		The first use memory bank is locked once the stamp is written.
 *
 * @return   Status as upErr
 */
upErr COneWireMemory::StampFirstUse()
{
	upErr eErr = ERR_OK;
	byte * connectBlock = (byte*)&m_sOwMemSpace.sConnectTime;
	SOwState sOwState = GetCurrentState();

	if ( OW_STATE_WRITE_FIRSTUSE != sOwState.eTopState )
		return ERR_BAD_PARAM;

	switch ( sOwState.eSubState )
		{
		case GEN_STATE_START:
		case GEN_STATE_FIRST_ACTION:
			if ( sOwState.bSubStatePass < OW_PASSES_WRITE )
				{
				// Update the cache block with the connect time
				m_sOwMemSpace.sConnectTime.ulConnectTime = m_firstUseTime;
				RETURN_IF_ERROR( OwWriteMem( OW_BLOCK_ADDR_CONNECT, connectBlock, OW_SIZE_CONNECT, sOwState.bSubStatePass ) )
				sOwState = IncrementSubStatePass();
				}
			if ( sOwState.bSubStatePass >= OW_PASSES_WRITE )
				sOwState = SetSubState( GEN_STATE_SECOND_ACTION );
			break;
		case GEN_STATE_SECOND_ACTION:
			RETURN_IF_ERROR( WriteProtectBlock( OW_BLOCK_FIRSTUSE, sOwState.bSubStatePass ) )
			// if we got this far, the first use stamp was successful
			sOwState = IncrementSubStatePass();
			if ( sOwState.bSubStatePass >= OW_PASSES_WRITE )
				{
				sOwState = SetSubState( GEN_STATE_END );
				}
			break;
		default:
			sOwState = SetSubState( GEN_STATE_END );
			eErr = ERR_BAD_PARAM;
			break;
		}

	return eErr;
}

/**
 * @brief	This method writes the current treatment data into the one-wire memory.
 *    		The method handles sub-state transitions to ensure the
 *    		round-robin loop pass time does not overrun.
 *
 * @return   Status as upErr
 */
upErr COneWireMemory::WriteTreatment()
{
	upErr eErr = ERR_OK;
	byte * treatBlock = (byte*)&m_sOwMemSpace.sTreat;
	SOwState sOwState = GetCurrentState();

	if ( OW_STATE_WRITE_TREATMENT != sOwState.eTopState )
		return ERR_BAD_PARAM;

	if ( m_locked )
		return ERR_OW_LOCKED;

	switch ( sOwState.eSubState )
		{
		case GEN_STATE_START:
		case GEN_STATE_FIRST_ACTION:
			if ( sOwState.bSubStatePass < OW_PASSES_WRITE )
				{
				// Update the cache with the latest treatment information
				m_sOwMemSpace.sTreat.tbTreatCount = m_fullTreatmentCount;
				m_sOwMemSpace.sTreat.ulTherapyTime = m_therapyTime;
				RETURN_IF_ERROR( OwWriteMem( OW_BLOCK_ADDR_TREAT, treatBlock, OW_SIZE_TREATINFO, sOwState.bSubStatePass ) )
				sOwState = IncrementSubStatePass();
				}
			if ( sOwState.bSubStatePass >= OW_PASSES_WRITE )
				sOwState = SetSubState( GEN_STATE_END );
			break;
		default:
			sOwState = SetSubState( GEN_STATE_END );
			eErr = ERR_BAD_PARAM;
			break;
		}
	return eErr;
}

/**
 * @brief	This method locks down the treatment info data block to
 *    		prevent data changes from occurring.
 *
 * @return   Status as upErr
 */
upErr COneWireMemory::WriteLockdown()
{
	upErr eErr = ERR_OK;
	twobytes iLockAdd = OW_ADDR_PROTECT + OW_BLOCK_TREATINFO;
	byte bWrite = OW_WRITE_PROTECT;
	SOwState sOwState = GetCurrentState();

	if ( sOwState.eTopState != OW_STATE_WRITE_LOCKDOWN )
		return ERR_BAD_PARAM;

	if ( sOwState.bSubStatePass < OW_PASSES_WRITE )
		{
		RETURN_IF_ERROR( OwWriteMem ( iLockAdd, &bWrite, 1, sOwState.bSubStatePass ) )
		sOwState = IncrementSubStatePass();
		}
	if ( sOwState.bSubStatePass >= OW_PASSES_WRITE )
		{
		m_locked = true;
		sOwState = SetSubState( GEN_STATE_END );
		}
	return eErr;
}

/**
 * @brief	This method reads the individual banks of memory
 * 			that are used by the Rezum generator and stores
 * 			the data in a local cache.  The ReadAll() method
 * 			also checks the ROM ID of the one-wire to ensure
 * 			that it's a part of the 0x43 family of devices
 * 			before reading the device.
 *
 * @return   Status as upErr
 */
upErr COneWireMemory::ReadAll()
{
	upErr eErr = ERR_OK;
	SOwState sOwState = GetCurrentState();
	bool_c bLock = false;

	if ( sOwState.eTopState != OW_STATE_READ_ALL )
		return ERR_BAD_PARAM;

	if ( GEN_STATE_START != sOwState.eSubState && GEN_STATE_FIRST_ACTION != sOwState.eSubState )
		{
		sOwState = SetSubState( GEN_STATE_END );
		return eErr;
		}

	switch ( sOwState.bSubStatePass )
		{
		case OW_RDALL_ROM:
			RETURN_IF_ERROR( OwGetRomInfo( m_romInfo ) )
			if ( OW_FAMILY_CODE != m_romInfo[ OW_FAMILY_CODE_ADDR ] )
				return ERR_OW_DATA_READ_ERROR;
			sOwState = IncrementSubStatePass();
			break;
		case OW_RDALL_LOCKS:
			ClearOwValues();
			// reset first
			RETURN_IF_ERROR( ReadyNewOneWire() )
			// check for a lock
			RETURN_IF_ERROR( CheckForLock( &bLock ) )
			m_locked = bLock;
			sOwState = IncrementSubStatePass();
			break;
		case OW_RDALL_ID:
			{
			byte * idBlock = (byte*)&m_sOwMemSpace.sId;
			RETURN_IF_ERROR( OwReadMem( OW_BLOCK_ADDR_ID, idBlock, OW_SIZE_ID ) )
			m_serial = m_sOwMemSpace.sId.ulSerial;
			memcpy( (void*)m_cLotNumber, (void*)m_sOwMemSpace.sId.cLotNumber, (size_t)OW_LENGTH_LOT );
			m_therapyCode = (upTreatmentCode) m_sOwMemSpace.sId.bTherapyCode;
			sOwState = IncrementSubStatePass();
			}
			break;
		case OW_RDALL_MODEL:
			{
			RETURN_IF_ERROR( OwReadMem( OW_BLOCK_ADDR_MODEL, (byte*)m_sOwMemSpace.sModel.strModel, OW_SIZE_MODEL ) )
			SetModel( (void*) m_sOwMemSpace.sModel.strModel );
			sOwState = IncrementSubStatePass();
			}
			break;
		case OW_RDALL_CONNECT:
			{
			byte * connectBlock = (byte*)&m_sOwMemSpace.sConnectTime;
			// Determine if the one-wire has already been initialized
			RETURN_IF_ERROR( OwReadMem( OW_BLOCK_ADDR_CONNECT, connectBlock, OW_SIZE_CONNECT ) )
			m_firstUseTime = m_sOwMemSpace.sConnectTime.ulConnectTime;
			sOwState = IncrementSubStatePass();
			}
			break;
		case OW_RDALL_TREATMENTS:
			{
			byte * treatBlock = (byte*)&m_sOwMemSpace.sTreat;
			RETURN_IF_ERROR( OwReadMem( OW_BLOCK_ADDR_TREAT, treatBlock , OW_SIZE_TREATINFO ) )
			SetFullTreatmentCount( m_sOwMemSpace.sTreat.tbTreatCount );
			SetTherapyTime( m_sOwMemSpace.sTreat.ulTherapyTime );
			sOwState = IncrementSubStatePass();
			}
			break;
		default:
			sOwState = SetSubState( GEN_STATE_END );
			break;
		}

	if ( sOwState.bSubStatePass >= OW_PASSES_READALLBANKS )
		{
		// The one-wire is determined to be viable
		m_eOwState = OW_STATE_READY;
		sOwState = SetSubState( GEN_STATE_END );
		}
	return eErr;
}


/**
 * @brief	This method sets the handpieceModel by copying the data
 *    		from an array
 *
 * @param  pModel A pointer to an array that contains the model number
 *
 * @return   Status as upErr
 */
upErr COneWireMemory::SetModel( void * pModel )
{
	byte newIndex = OW_SIZE_MODEL - 1;

	if ( 0 == pModel )
		return ERR_BAD_PARAM;

	// actual allowable length of string is 15 bytes, 1 for null
	strncpy( m_sOwMemSpace.sModel.strModel, (char*) pModel, OW_SIZE_MODEL );
	m_sOwMemSpace.sModel.strModel[ newIndex ] = 0;

	return ERR_OK;
}

/**
 * @brief	This method return the unique Maxim ROM ID read from the one-wire. The
 * 			8-byte ROM ID is copied using a pointer.
 *
 * @param  pbRomInfo A pointer to store the ROM ID
 *
 * @return   Status as upErr
 */
upErr COneWireMemory::GetRomId( byte * pbRomInfo )
{
	upErr eErr = ERR_OK;
	if ( 0 == pbRomInfo )
		return ERR_BAD_PARAM;

	memcpy( (void*)pbRomInfo, (void*)m_romInfo, OW_ROM_SIZE );

	return eErr;
}

/**
 * @brief	This method gets the handpieceModel by copying the data
 *    		from an array
 *
 * @param   pModel A pointer to an array to store the model number
 *
 * @return  Status as upErr
 */
upErr COneWireMemory::GetModel( char * pModel )
{
	if ( 0 == pModel )
		return ERR_BAD_PARAM;

	strncpy( pModel, m_sOwMemSpace.sModel.strModel, OW_SIZE_MODEL );
	return ERR_OK;
}

/**
 * @brief	This method enables or disables the one-wire memory check for presence as well as one-wire
 * 			memory reading and writing.
 *
 * @param	bEnable	A boolean value to enable the monitor (true) or disable the monitor (false)
 *
 * @return   void
 */
void COneWireMemory::EnableOwMonitor( bool_c bEnable )
{
	m_bEnableOwMonitor = bEnable;
}

/**
 * @brief	This method gives the presence of the memory and thus the handpiece
 *
 * @param	pbReady A pointer to a caller-allocated bool_c and is "true" if there is a handpiece
 *
 * @return   Status as upErr
 */
upErr COneWireMemory::CheckForHandpiece( bool_c *pbReady )
{
	upErr eErr = ERR_OK;

	// This variable is referenced from the driver
	*pbReady = ( OW_STATE_READY == m_eOwState );

	return eErr;
}

/**
 * @brief	This method returns the serial number of the handpiece
 *
 * @param   piSerial A pointer to a caller-allocated integer to receive the serial number
 *
 * @return   Status as upErr
 */
upErr COneWireMemory::GetSerialNumber( uint32_t * piSerial )
{
	*piSerial = m_serial;
	return ERR_OK;
}

/**
 * @brief	This method returns the lot number of the handpiece
 *
 * @param   pcLotNumber A pointer to the lot number of the device
 *
 * @return   Status as upErr
 */
upErr COneWireMemory::GetLotNumber( char * pcLotNumber )
{
	memcpy( pcLotNumber, m_cLotNumber, OW_LENGTH_LOT );
	return ERR_OK;
}


/**
 * @brief	This method returns the model number of the handpiece
 *
 * @param   peCode A pointer to a caller-allocated integer to receive the model number
 *
 * @return   Status as upErr
 */
upErr COneWireMemory::GetTherapyCode( upTreatmentCode *peCode)
{
	*peCode = m_therapyCode;
	return ERR_OK;
}

/**
 * @brief	This method returns the full treatment count read from the delivery tool
 *
 * @param   ptbCount A pointer to store the full treatment count
 *
 * @return   The full treatment count as a byte
 */
upErr COneWireMemory::GetFullTreatmentCount( twobytes * ptbCount )
{
	*ptbCount = m_fullTreatmentCount;
	return ERR_OK;
}

/**
 * @brief	This method returns the full treatment count read from the delivery tool
 *
 * @param   tbCount The full treatment value to set
 *
 * @return   Status as upErr
 */
upErr COneWireMemory::SetFullTreatmentCount( twobytes tbCount )
{
	m_fullTreatmentCount = tbCount;
	return ERR_OK;
}

/**
 * @brief	This method returns the length of time the delivery tool has been connected
 *
 * @param   ptTime A pointer to a caller-allocated time to receive the connect time
 *
 * @return   Status as upErr
 */
upErr COneWireMemory::GetConnectedTime( time *ptTime )
{
	*ptTime = m_firstUseTime;
	return ERR_OK;
}

/**
 * @brief	This method sets the first use time for the current delivery tool
 *
 * @param  	tFirstUse The first use time to set
 *
 * @return  Status as upErr
 */
upErr COneWireMemory::SetFirstUseTime( time tFirstUse )
{
	m_firstUseTime = tFirstUse;

	AddEvent( OW_EVENT_STAMP_FIRST_USE );
	return ERR_OK;
}

/**
 * @brief	This method gets the first use time for the current delivery tool
 *
 * @param  	ptFirstUse A pointer to store the first use time
 *
 * @return  Status as upErr
 */
upErr COneWireMemory::GetFirstUseTime( time * ptFirstUse )
{
	*ptFirstUse = m_firstUseTime;
	return ERR_OK;
}

/**
 * @brief	This method returns the accumulated treatment time stored on the delivery tool
 *
 * @param   pulTime A pointer to a caller-allocated time to receive the therapy time
 *
 * @return   Status as upErr
 */
upErr COneWireMemory::GetTherapyTime( unsignedfourbytes * pulTime )
{
	*pulTime = m_therapyTime;
	return ERR_OK;
}

/**
 * @brief	This method sets the accumulated treatment time stored on the delivery tool
 *
 * @param   ulTime The therapy time to set
 *
 * @return   Status as upErr
 */
upErr COneWireMemory::SetTherapyTime( unsignedfourbytes ulTime )
{
	m_therapyTime = ulTime;
	return ERR_OK;
}

// Private Class Functions

/**
 * @brief	This method sets a memory block (0-9) as write protected.  Once written, this
 *    		operation cannot be undone.
 *
 * @param   bBlock The block to write protect (0-9)
 * @param   bPass The write pass stage
 *
 * @return   Status as upErr
 */
upErr COneWireMemory::WriteProtectBlock( byte bBlock, byte bPass )
{
	upErr eErr = ERR_OK;
	twobytes wProtectAdd = OW_ADDR_PROTECT;
	byte wProtectByte = OW_WRITE_PROTECT;

	if ( bBlock > OW_DATA_BLOCK_CNT )
		return ERR_BAD_PARAM;

	// write to write protection address
	wProtectAdd += bBlock;
	RETURN_IF_ERROR( OwWriteMem( wProtectAdd, (byte*)&wProtectByte, 1, bPass ) )
	return eErr;
}

/**
 * @brief	This method clears the cache for the one-wire memory.
 *
 * @return   Status as upErr
 */
void COneWireMemory::ClearOwValues()
{
	m_therapyCode = TREATMENT_UNSET;
	m_serial = 0;
	SetTherapyTime( 0 );
	SetFullTreatmentCount( 0 );
	m_firstUseTime = 0;
	m_locked = false;
	m_owErrPass = 0;
	m_eErrorState = ERR_OK;

	for ( byte i = 0; i < OW_SIZE_MODEL; i++ )
		m_sOwMemSpace.sModel.strModel[ i ] = 0;

	for ( byte i = 0; i < OW_LENGTH_LOT; i++ )
		m_cLotNumber[ i ] = 0;
}


/**
 * @brief	This method sets the last known validity state of the current
 * 			delivery device to connected.  The device is not locked.  The
 * 			delivery device will remain disabled until it is removed.
 *
 * @return  void
 */
upErr COneWireMemory::DisableTool()
{
	upErr eErr = ERR_OK;

	// The device is still connected, but not ready
	m_eOwState = OW_STATE_CONNECTED;

	return eErr;
}

/**
 * @brief	This method reads the manufacturing ID stored at address OW_ADDR_MFGRID.
 *
 * @return  Status as upErr
 */
upErr COneWireMemory::ReadAndStoreMfgrId()
{
	upErr eErr = ERR_OK;
	byte bReadMem[ OW_SIZE_MFGRID ];
	if ( ( eErr = OwReadMem( OW_ADDR_MFGRID, bReadMem, OW_SIZE_MFGRID ) ) != ERR_OK )
		return eErr;
	m_mfgrId = (twobytes) bReadMem[ 0 ] | ( (twobytes) bReadMem[ 1 ] << 8 );
	return eErr;
}

/**
 * @brief	This method returns a boolean that indicates whether the treatment
 * 			block has been write protected.
 *
 * @param 	pbLock A pointer to a bool_c to receive the lock status (true == locked)
 *
 * @return  Status as upErr
 */
upErr COneWireMemory::CheckForLock( bool_c * pbLock )
{
	upErr eErr = ERR_OK;
	twobytes iAddress = OW_ADDR_PROTECT + OW_BLOCK_TREATINFO;
	byte readByte = 0;

	RETURN_IF_ERROR( OwReadMem( iAddress, &readByte, 1 ) )
	if ( OW_WRITE_PROTECT == readByte )
		*pbLock = true;
	else
		*pbLock = false;

	return eErr;
}

/**
 * @brief	This method adds an event to lock/write protect the treatment block
 * 			on the one-wire.
 *
 * @return  Status as upErr
 */
upErr COneWireMemory::LockOneWire()
{
	upErr eErr = ERR_OK;

	eErr = AddEvent( OW_EVENT_LOCK_DEVICE );

	return eErr;
}

/**
 * @brief	This method returns the locked state of the one-wire

 * @param pbLock A pointer to a bool_c to receive the lock status
 *
 * @return  Status as upErr
 */
upErr COneWireMemory::GetLockState( bool_c * pbLock )
{
	*pbLock = m_locked;
	return ERR_OK;
}

/**
 * @brief	This method returns the locked state of the one-wire
 *
 * @return  State as upOwState
 */
upOwState COneWireMemory::GetOwState()
{
	return m_eOwState;
}

/**
 * @brief	This method provides direct write access to the one-wire device
 *    		to projects that make use of the shared application lib.
 *
 * @param iAddress The address to write to as an int
 * @param pbStream The source of data to write as a pointer to bytes
 * @param bSize The count of bytes to write as a byte
 * @param bPass The "pass" code as a byte
 *
 * @return  void
 */
upErr COneWireMemory::WriteAccess( int iAddress, byte * pbStream, byte bSize, byte bPass )
{
	upErr eErr = ERR_OK;
	// if this isn't requesting access from another project, return immediately
	if ( 1 != g_bTestAccess )
		return ERR_OK;

	if ( OW_WRITEPASS_LIMIT < bPass || OW_END_WRITE_ACCESS == bPass )
		{
		m_writeAccessActive = false;
		return ERR_OK;
		}
	else
		m_writeAccessActive = true;

	LOG_ERROR_RETURN( eErr = OwWriteMem( iAddress, pbStream, bSize, bPass ) )

	return eErr;
}

/**
 * @brief	This method provides direct read access to the one-wire device
 *    		to projects that make use of the shared application lib.
 *
 * @param iAddress The address to read to as an int
 * @param pbStream A pointer to store the data
 * @param bSize The count of bytes to read
 *
 * @return  ERR_OK or error code
 */
upErr COneWireMemory::ReadAccess( int iAddress, byte * pbStream, byte bSize )
{
	upErr eErr = ERR_OK;
	// if this isn't requesting access from another project, return immediately
	if ( 1 != g_bTestAccess )
		return ERR_OK;

	LOG_ERROR_RETURN( eErr = OwReadMem( iAddress, pbStream, bSize ) )

	return eErr;
}

/**
 * @brief	This method provides a means to inject a READ_ALL event
 *    		into the OneWireMemory event handler helper class for
 *    		use with shared application libraries
 *
 * @return  ERR_OK or error code
 */
upErr COneWireMemory::ReadDevice()
{
	upErr eErr = ERR_OK;

	// if this isn't requesting access from another project, return immediately
	if ( 1 != g_bTestAccess )
		return ERR_OK;

	eErr = AddEvent( OW_EVENT_NEW_MEMORY );

	return eErr;
}


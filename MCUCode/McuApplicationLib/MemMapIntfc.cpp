/**
 * @file	MemMapIntfc.cpp
 * @par		Package: UGMcu
 * @par		Project: Union Generator
 * @par		Copyright (c) 2017, Uptake Medical Technology, Inc.
 * @author	Chris Feb 21, 2011 Created file
 * @brief   This file contains the code for the MemMapIntfc class, CMemMapIntfc
 */

// Include Files
#include "MemMapIntfc.h"
#include "stdlib.h"
#include "string.h"
#include "stdio.h"
#include "Messages.h"
#include "LogMessage.h"

// External Public Data

// File Scope Macros and Constants

// File Scope Type Definitions (Enums, Structs & Classes)
CMemMapIntfc::SMMapInfo CMemMapIntfc::m_sMMapInfo[ MMAP_FUNC_COUNT ];

// Public Functions

/**
 * @brief	This is the constructor for MemMapIntfc.
 *
 * 			It performs no functions as the member variables are initialized as static variables.
 *
 */
CMemMapIntfc::CMemMapIntfc()
{
	// CLear array in case of assignment holes
	for ( byte bI = 0; bI < MMAP_FUNC_COUNT; bI++ )
		{
		m_sMMapInfo[ bI ].m_length = 0;
		m_sMMapInfo[ bI ].m_firstAddr = 0;
		}

	m_sMMapInfo[ MMAP_FUNC_WDOG ].m_length = MMAP_FUNC_WDOG_SIZE;
	m_sMMapInfo[ MMAP_FUNC_WDOG ].m_firstAddr = (byte*) MMAP_ADDR_WDOG_LSB;

	m_sMMapInfo[ MMAP_FUNC_WDOG_STROBE ].m_length = MMAP_FUNC_WDOG_STROBE_SIZE;
	m_sMMapInfo[ MMAP_FUNC_WDOG_STROBE ].m_firstAddr = (byte*) MMAP_ADDR_WDOG_STROBE;

	m_sMMapInfo[ MMAP_FUNC_ENCODER_CLEAR ].m_length = MMAP_FUNC_ENCODER_CLEAR_SIZE;
	m_sMMapInfo[ MMAP_FUNC_ENCODER_CLEAR ].m_firstAddr = (byte*) MMAP_ADDR_ENCODER_CLEAR;

	m_sMMapInfo[ MMAP_FUNC_ENCODER ].m_length = MMAP_FUNC_ENCODER_SIZE;
	m_sMMapInfo[ MMAP_FUNC_ENCODER ].m_firstAddr = (byte*) MMAP_ADDR_ENCODER_LSB;

	m_sMMapInfo[ MMAP_FUNC_HTEMP ].m_length = MMAP_FUNC_HTEMP_SIZE;
	m_sMMapInfo[ MMAP_FUNC_HTEMP ].m_firstAddr = (byte*) MMAP_ADDR_HIGH_TEMP_LSB;

	m_sMMapInfo[ MMAP_FUNC_LTEMP ].m_length = MMAP_FUNC_LTEMP_SIZE;
	m_sMMapInfo[ MMAP_FUNC_LTEMP ].m_firstAddr = (byte*) MMAP_ADDR_LOW_TEMP_LSB;

	m_sMMapInfo[ MMAP_FUNC_UNUSED_0 ].m_length = MMAP_FUNC_UNUSED_0_SIZE;
	m_sMMapInfo[ MMAP_FUNC_UNUSED_0 ].m_firstAddr = (byte*) MMAP_ADDR_UNUSED_0_LOW;

	m_sMMapInfo[ MMAP_FUNC_SYSTEM_SIGNALS ].m_length = MMAP_FUNC_SYSTEM_SIGNALS_SIZE;
	m_sMMapInfo[ MMAP_FUNC_SYSTEM_SIGNALS ].m_firstAddr = (byte*) MMAP_ADDR_SYSTEM_SIGNALS;

	m_sMMapInfo[ MMAP_FUNC_SYSTEM_LED ].m_length = MMAP_FUNC_SYSTEM_LED_SIZE;
	m_sMMapInfo[ MMAP_FUNC_SYSTEM_LED ].m_firstAddr = (byte*) MMAP_ADDR_SYSTEM_LEDS;

	m_sMMapInfo[ MMAP_FUNC_SYSTEM_CONTROLS ].m_length = MMAP_FUNC_SYSTEM_CONTROLS_SIZE;
	m_sMMapInfo[ MMAP_FUNC_SYSTEM_CONTROLS ].m_firstAddr = (byte*) MMAP_ADDR_SYSTEM_CONTROLS;

	m_sMMapInfo[ MMAP_FUNC_CPLD_VERSION ].m_length = MMAP_FUNC_CPLD_VERSION_SIZE;
	m_sMMapInfo[ MMAP_FUNC_CPLD_VERSION ].m_firstAddr = (byte*) MMAP_ADDR_CPLD_VERSION_DASH;

	m_sMMapInfo[ MMAP_FUNC_MCU_BOARD_REVISION ].m_length = MMAP_FUNC_MCU_BOARD_REVISION_SIZE;
	m_sMMapInfo[ MMAP_FUNC_MCU_BOARD_REVISION ].m_firstAddr = (byte*) MMAP_ADDR_MCU_BOARD_REVISION;
}

/**
 * @brief	This method reads one or more bytes using a list of specified memory mapped addresses
 *    		for a specified memory mapped function
 *
 * @param	eFunction An enumerated value representing the function data set to retrieve
 * @param   pbBytes A pointer to store the returned data
 * @param   bRdSize The size of the pbBytes data set
 *
 * @return   Status as upErr
 */
upErr CMemMapIntfc::MMapRead( upMMapFunction eFunction, byte * pbBytes, byte bRdSize )
{
	upErr eErr = ERR_OK;

	if ( eFunction >= MMAP_FUNC_COUNT )
		{
		LOG_ERROR_RETURN( ERR_BAD_PARAM )
		return ERR_BAD_PARAM;
		}

	byte * pAddress = 0;
	byte bSize = 0;

	pAddress = m_sMMapInfo[ eFunction ].m_firstAddr;
	bSize = m_sMMapInfo[ eFunction ].m_length;

	if ( 0 == pAddress || 0 == bSize )
		{
		LOG_ERROR_RETURN( ERR_BAD_PARAM )
		return ERR_BAD_PARAM;
		}

	bSize = ( bRdSize <= bSize ) ? bRdSize : bSize;

	for ( int i = 0; i < bSize; i++, pAddress++ )
		{
		pbBytes[ i ] = *(volatile uint8_t *) pAddress;
		}

	return eErr;
}

/**
 * @brief	This method writes one or more bytes using a list of specified memory mapped addresses
 *    		for a specified memory mapped function
 *
 * @param	eFunction An enumerated value representing the mapped function to write to
 * @param   pbBytes A pointer to the bytes to write
 * @param   bWrSize The size of the pbBytes data set
 *
 * @return   Status as upErr
 */
upErr CMemMapIntfc::MMapWrite( upMMapFunction eFunction, byte * pbBytes, byte bWrSize )
{
	upErr eErr = ERR_OK;

	if ( eFunction >= MMAP_FUNC_COUNT )
		{
		LOG_ERROR_RETURN( ERR_BAD_PARAM )
		return ERR_BAD_PARAM;
		}

	byte * pAddress = 0;
	byte bSize = 0;

	pAddress = m_sMMapInfo[ eFunction ].m_firstAddr;
	bSize = m_sMMapInfo[ eFunction ].m_length;

	if ( 0 == pAddress || 0 == bSize )
		{
		LOG_ERROR_RETURN( ERR_BAD_PARAM )
		return ERR_BAD_PARAM;
		}

	// Use up to a maximum number of bytes stored in the indexer
	bSize = ( bWrSize <= bSize ) ? bWrSize : bSize;

	for ( int i = 0; i < bSize; i++, pAddress++ )
		{
		*(volatile uint8_t *) pAddress = pbBytes[ i ];
		}

	return eErr;
}

/**
 * @brief	This method writes one byte using a list of specified memory mapped address
 *    		for a specified memory mapped function
 *
 * @param	eFunction An enumerated value representing the mapped function to write to
 * @param   bByte 	  The byte to write to the CPLD
 *
 * @return   Status as upErr
 */
upErr CMemMapIntfc::MMapWriteByte( upMMapFunction eFunction, byte bByte )
{
	return MMapWrite( eFunction, &bByte, 1 );
}

// Private Class Functions



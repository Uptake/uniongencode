/**
 * @file	StepperVelocity.cpp
 * @par		Package: UGMcu
 * @par		Project: Union Generator
 * @par		Copyright (c) 2017, Uptake Medical Technology, Inc.
 * @author	Glen Sep 16, 2010 Created file
 * @brief	This file contains the code to control the stepper motor, CStepperVelocity
 */

// Include Files
#include "UGMcuDefinitions.h"
#include "StepperVelocity.h"
#include "DigitalInputOutput.h"
#include "LogMessage.h"

// External Public Data

// File Scope Macros and Constants
#define CLK_DIV						(8)									//!< prescaler of 1, 8, 64, 256, 1024
#define PULSE_CHANGE_PER_SEC		(25000)								//!< Maximum acceleration rate
#define CLK_RATE 					( SYSTEM_CLOCK_RATE / ( CLK_DIV ) )	//!< The clock as supplied to the timer
#define PULSE_CHANGE_PER_TICK(x)	( x * (PULSE_CHANGE_PER_SEC  / TICK_CLOCK_RATE ) )	//!< The allowed velocity change based on delta time
#define ENABLE_OCR1_INTERRUPT		//!< Enable interrupt on A compare match
#define DISABLE_OCR1_INTERRUPT		//!< Disable interrupt on A compare match
#define STEPPER_DISABLE_TIMEOUT 	(60*60*2)							//!< Two hours in seconds while stationary before disabling

// File Scope Type Definitions (Enums, Structs & Classes)

// File Scope Data
twobytes f_newOcr1a = 0xFFFF; //!< Static variable to carry new timer compare value

// File Scope Functions

/**
 * @brief	This is the interrupt function for a comparison match being made
 * 			on timer1.
 *
 * 			This timer is for the stepper motor. On each compare match the
 * 			new target compare value is set. This prevents a lower value from being set after
 * 			the counter has passed and making the counter wrap before a match is found.
 *
 */

// Public Functions

/**
 * @brief	This is the constructor for CStepperVelocity.
 *
 * It clears the member variables to zero and false.
 *
 */
CStepperVelocity::CStepperVelocity()
{
	m_targetVelocity = 0;
	m_appliedVelocity = 0;
	m_bEnabled = false;
	m_bHalt = false;
	m_bMotorEnabled = false;
	m_tHalted = 0;
}

/**
 * @brief	This method initializes the hardware for controlling the stepper motor velocity.
 *
 * 			The motion clock (SMCLOCK) is on port B pin 5 and is the OC1A output
 * 			The direction signal (SMDIRECTION) is on port B pin 4
 * 			The enable signal (SMENABLE) is on port B pin 6
 *
 * @return   Status as upErr
 */
upErr CStepperVelocity::InitializeVelocityHardware()
{
	// Set the direction bits for the status outputs. Pins 4, 5 and 6 on PORTB


	// Force a disabled state
	m_bEnabled = true;
	EnableClock( false );

	return ERR_OK;
}

/**
 * @brief	This method enables or disables the clock for the stepper motor.
 *
 * 			It sets the clock
 * 			control to CTC or "Clear Timer on Compare" operation and to toggle on each compare. This means
 * 			the output is clocking at half the frequency that matches are being made and with
 * 			a 50% duty cycle
 *
 * @param	bEnable	To activate the clock and motor or to stop the clock and motor
 *
 * @return   Status as upErr
 */
upErr CStepperVelocity::EnableClock( bool_c bEnable )
{
	byte bPrescale;

	if ( bEnable != m_bEnabled )
		{
		if ( bEnable )
			{
			bPrescale = 0;

			switch ( CLK_DIV )
				{
				case 1:

					break;
				case 8:

					break;
				case 64:

					break;
				case 256:

					break;
				case 1024:

					break;
				default:
					bPrescale = 0;
					break;
				}

			// Set new compare value and clear flag


			// Enable motor and flag as such

			m_bMotorEnabled = true;

			// Configure and enable the timer


			// Reset timer value. Done last as experiments showed odd behavior with clock running fast between previous statements.

			}

		// Disable - Stop clocking output pin
		else
			{

// Clear enable
			// Reset output toggling on comparisons
			// Disable the clock
			}

		m_bEnabled = bEnable;
		}

	return ERR_OK;
}

/**
 * @brief	This method communicates with the hardware to set motor velocity.
 *
 * 			The actual velocity may be ramped on each call to prevent too large acceleration. The requested
 * 			velocity will not be applied if that would serve to drive the rod further in the direction of an ON
 * 			limit switch. The velocity sense is used to set the direction control line on Port B pin 4 for the
 * 			motor controller.
 *
 * @return   ERR_PUMP_AT_LIMIT if extend switch is on and otherwise ERR_OK
 */
upErr CStepperVelocity::WriteVelocityToHardware()
{
	long int iNewVel = m_appliedVelocity;
	twobytes uCompare = 0;
	uint32_t ulNewVel = 0;
	bool_c bRetracted;
	bool_c bExtended;
	upErr eErr;
	twobytes tbDiff;

	// Compute allowed change based on running each loop cycle
	tbDiff = PULSE_CHANGE_PER_TICK( RND_ROBIN_TICK_CNT );

	// Accelerate or decelerate motor
	if ( m_targetVelocity > m_appliedVelocity )
		{
		iNewVel = m_appliedVelocity + (long int) tbDiff;
		if ( iNewVel > m_targetVelocity )
			iNewVel = m_targetVelocity;
		}
	else if ( m_targetVelocity < m_appliedVelocity )
		{
		iNewVel = m_appliedVelocity - (long int) tbDiff;
		if ( iNewVel < m_targetVelocity )
			iNewVel = m_targetVelocity;
		}

	// Ensure not driving past limits
	LOG_ERROR_RETURN( eErr = g_pDigitalInputOutput->GetInput( DIG_IN_MOTOR_RETRACT, &bRetracted ) )
	LOG_ERROR_RETURN( eErr = g_pDigitalInputOutput->GetInput( DIG_IN_MOTOR_EXTEND, &bExtended ) )
	if ( ( 0 < iNewVel && bExtended ) || ( 0 > iNewVel && bRetracted ) )
		{
		m_bHalt = true;

		// Only an error going too far out
		if ( bExtended )
			LOG_ERROR_RETURN( eErr = ERR_PUMP_AT_LIMIT )
		}

	// Halt motor immediately and set target to zero
	if ( m_bHalt )
		{
		m_bHalt = false;
		m_targetVelocity = 0;
		iNewVel = 0;
		}

	// Set new comparator value and direction if velocity has changed
	if ( iNewVel != m_appliedVelocity )
		{
		m_appliedVelocity = iNewVel;

		if ( 0 == iNewVel )
			{
			EnableClock( false );
			}
		else
			{
			if ( iNewVel > 0 )
				{
				ulNewVel = iNewVel;

				}
			else
				{

				ulNewVel = -iNewVel;
				}

			// The pulse rate will be half the output compare rate so the compare time
			// needs to be halved. The frequency is in Hertz. Subtract one for how the counter works
			// The following math is shorter for: uCompare = ( CLK_RATE / ( 2 * iNewVel ) ) - 1;
			uCompare = -1 + (twobytes) ( ( uint32_t ) CLK_RATE / ( 2 * ulNewVel ) );

			// Have the new value set on the next compare
			DISABLE_OCR1_INTERRUPT
			f_newOcr1a = uCompare;
			ENABLE_OCR1_INTERRUPT
			EnableClock( true );
			}

		}

	// Check to disable stepper if requested and actual velocities are zero
	if ( 0 == iNewVel && 0 == m_appliedVelocity )
		{
		tick tElapsed = 0;
		if ( 0 == m_tHalted )
			GetTick( &m_tHalted );
		else
			GetTickElapsed( m_tHalted, &tElapsed );

		// Diable if retracted switch in ON or if timer has expired
		if ( bRetracted || tElapsed > MS_TO_TICKS( SEC_TO_MS( STEPPER_DISABLE_TIMEOUT ) ) )
			EnableClock( false );
		}

	// Moving so clear halted timer
	else
		{
		m_tHalted = 0;
		}

	return eErr;
}

/**
 * @brief	This method returns the last set target velocity
 *
 * @param	pilVelocity A pointer to a long int to receive the velocity
 *
 * @return   Always ERR_OK
 */
upErr CStepperVelocity::GetTargetVelocity( long int * pilVelocity )
{
	*pilVelocity = m_targetVelocity;
	return ERR_OK;
}

/**
 * @brief	This method sets the velocity that the stepper motor should reach.
 *
 * Positive values mean to extend the pump rod. Negative values retract the rod.
 *
 * @param	ilVelocity The velocity that the stepper motor should achieve in pulses per second
 *
 * @return   Status as upErr
 */
upErr CStepperVelocity::SetTargetVelocity( long int ilVelocity )
{
	m_targetVelocity = ilVelocity;
	if ( m_targetVelocity != 0 )
		m_bHalt = false;
	return ERR_OK;
}

/**
 * @brief	This method returns the last applied velocity in pulses per tenths of a second
 *
 * @param   pilVelocity A pointer to a long integer to receive the last set velocity
 *
 * @return   Status as upErr
 */
upErr CStepperVelocity::GetAppliedVelocity( long int * pilVelocity )
{
	*pilVelocity = m_appliedVelocity;
	return ERR_OK;
}

/**
 * @brief	This method sets a flag to have the motor halted.
 *
 *  This will be done on the next WriteToHardware() cycle.
 *
 * @return   Status as upErr
 */
upErr CStepperVelocity::HaltStepper()
{
	m_targetVelocity = 0;
	m_bHalt = true;
	return ERR_OK;
}

// Private Class Functions

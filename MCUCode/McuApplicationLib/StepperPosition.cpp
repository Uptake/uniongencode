/**
 * @file	StepperPosition.cpp
 * @par		Package: UGMcu
 * @par		Project: Union Generator
 * @par		Copyright (c) 2017, Uptake Medical Technology, Inc.
 * @author	Glen Sep 16, 2010 Created file
 * @brief	This file contains the code to read and track the stepper motor position and velocity, CStepperPosition
 */

// Include Files
#include "UGMcuDefinitions.h"
#include "UGMcuEnum.h"
#include "StepperPosition.h"

// External Public Data

// File Scope Macros and Constants

// File Scope Type Definitions (Enums, Structs & Classes)

// File Scope Data

// File Scope Functions

// Public Functions


/**
 * @brief	This is the constructor for CStepperPosition.
 *
 * It clears all of the class member variables to zero including the buffer for the FIR filter data
 *
 */
CStepperPosition::CStepperPosition()
{
	m_lastPosition = 0;
	m_position = 0;
	m_velocity = 0;
	for ( int i = 0; i < SAVED_POSITION_CNT; i++ )
		{
		m_deltaPosition[ i ] = 0;
		m_deltaTime[ i ] = 0;
		}
}

/**
 * @brief	This method reads the hardware for the latest stepper motor position and computes the velocity.
 *
 * The data is three bytes wide and comes from the CPLD. The CPLD is doing the up/down counting and provides
 * synchronized values so the bytes are always coherent. The read data is placed into an buffer to be used in an FIR
 * (Finite Impulse Response) filter calculation. Both the position and delta time for each sample are saved. The velocity
 * is then the sum of delta positions divided by the sum of delta times.@par
 * This method must be executed periodically to have fresh position and velocity data available from GetStepperPosition.
 *
 * @param	tbTickDiff The difference in timer tick since last read operation
 *
 * @return   Status as upErr
 */
upErr CStepperPosition::ReadStepperPosition( twobytes tbTickDiff )
{
	upErr eErr = ERR_OK;
	byte bData[ 3 ];
	long int ilPosition;
	long int ilTemp;
	int iVelocity;
	int iDeltaPos;
	twobytes tbDeltaTick;

	eErr = MMAP_READ( MMAP_FUNC_ENCODER, bData );

	m_lastPosition = m_position;

	ilPosition = ( bData[ 2 ] );
	ilPosition <<= 8;
	ilPosition |= bData[ 1 ];
	ilPosition <<= 8;
	ilPosition |= bData[ 0 ];

	// Move recorded positions down and sum deltas
	iDeltaPos = 0;
	tbDeltaTick = 0;
	for ( int i = 0; i < SAVED_POSITION_CNT - 1; i++ )
		{
		m_deltaPosition[ i ] = m_deltaPosition[ i + 1 ];
		iDeltaPos += m_deltaPosition[ i ];
		m_deltaTime[ i ] = m_deltaTime[ i + 1 ];
		tbDeltaTick += m_deltaTime[ i ];
		}

	// Computer position difference
	// Change sign to correlate stepper pulse direction to velocity
	ilTemp = ilPosition - m_lastPosition;

	// Add new deltas to list and totals
	m_deltaPosition[ SAVED_POSITION_CNT - 1 ] = (int) ilTemp;
	iDeltaPos += m_deltaPosition[ SAVED_POSITION_CNT - 1 ];
	m_deltaTime[ SAVED_POSITION_CNT - 1 ] = tbTickDiff;
	tbDeltaTick += tbTickDiff;

	// Compute velocity over last SAVED_POSITION_CNT samples
	iVelocity = (int) ( (long int) SECS_TO_TICKS( (long int) iDeltaPos ) / (long int) tbDeltaTick );

	// Assign to member variables
	m_position = ilPosition;
	m_velocity = iVelocity;

	return eErr;
}

/**
 * @brief	This method resets the counter in the CPLD for the optical encoder to zero.
 *
 * It is intended to be used when the rod has been retracted and that limit switch has come ON
 *
 * @return   Status as upErr
 */
upErr CStepperPosition::ResetStepperPosition()
{
	return MMapWriteByte( MMAP_FUNC_ENCODER_CLEAR, 0 );
}

/**
 * @brief	This method provides the latest position and velocity of the stepper motor.
 *
 * These data are saved locally from the ReadStepperPosition() call. The pointers may be zero
 * if one piece of data is not of interest.
 *
 * @param	pliPosition A pointer to a long integer to receive the position.
 * @param	piVelocity A pointer to an integer to receive the velocity
 *
 * @return   Status as upErr
 */
upErr CStepperPosition::GetStepperPosition( long int *pliPosition, int *piVelocity )
{
	if ( 0 != pliPosition )
		*pliPosition = m_position;
	if ( 0 != piVelocity )
		*piVelocity = m_velocity;
	return ERR_OK;
}

// Private Class Functions


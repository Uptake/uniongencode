/**
 * @file	WaterCntl.cpp
 * @par		Package: UGMcu
 * @par		Project: Union Generator
 * @par		Copyright (c) 2017, Uptake Medical Technology, Inc.
 * @author	Glen Sep 16, 2010 Created file
 * @brief	This file contains the code for providing control and monitoring of the water pump, CWaterCntl
 */

// Include Files
#include "UGMcuDefinitions.h"
#include "WaterCntl.h"
#include "TreatmentInformation.h"
#include "LogMessage.h"
#include "stdio.h"
#include "DigitalInputOutput.h"

// External Public Data
CWaterCntl * g_pWaterCntl = 0;

// File Scope Macros and Constants
#define PUMP_ROD_RETRACT_VEL	( (-4) * (long int) STEPS_PER_REV ) // Negative turning retracts the rod
// File Scope Type Definitions (Enums, Structs & Classes)

// Flow in micro-liters per minute to steps per sec
#define SCREW_PITCH_REV_PER_INCH			( 8 )
#define MM_PER_INCH							( 25.4 )
#define SCREW_PITCH_REV_PER_MM				( SCREW_PITCH_REV_PER_INCH / MM_PER_INCH )
#define MM_TO_REVS(x)						( x * SCREW_PITCH_REV_PER_MM )
#define MM_PER_MIN_TO_STEP_PER_SEC(x)		( STEPS_PER_REV * MM_TO_REVS( (long int) x / SECS_PER_MIN  ) )
#define ML_TO_UL(x)							(1000 * x)

// Long form
// #define UL_PER_MIN_TO_STEP_PER_SEC(x,a)		( MM_PER_MIN_TO_STEP_PER_SEC( 1000 * (long int) x / (long int) a )  )
// #define UL_PER_MIN_TO_STEP_PER_SEC(x,a)		( 1000 * (20000/60) *( 8/25.4) / 1000) * x / a)
// Optimized form
#define UL_PER_MIN_TO_STEP_PER_SEC(x,a)		( ( 1050 * (long int) x)  / (10 * (long int) a ) )

// File Scope Data

// File Scope Functions

// Public Functions

/**
 * @brief	This is the constructor for CWaterCntl
 *
 */
CWaterCntl::CWaterCntl()
{
	g_pWaterCntl = this;
	m_rate = 0;
	m_bRetractRod = false;
}

/**
 * @brief	This method calls the Initialize methods on the velocity and position objects
 *
 * @return   Status as upErr
 */
upErr CWaterCntl::InitializeHardware()
{
	InitializeVelocityHardware();
	return ERR_OK;
}

/**
 * @brief	This method has the velocity set to the motor.
 *
 * 			This is called periodically to allow the motor to be accelerated correctly.
 *
 * @return   Status as upErr
 */
upErr CWaterCntl::WriteToHardware()
{
	WriteVelocityToHardware();
	return ERR_OK;
}

/**
 * @brief	This method has the rod retracted
 *
 * 			If a retraction operation has been requested then the rod is retracted if not already
 * 			into the retract limit switch. If retracted on command then the encoder value is reset.
 *
 * @param	pbRetracted A pointer to a bool_c value to receive TRUE if the leadscrew is retracted
 *
 * @return   Status as upErr
 */
upErr CWaterCntl::PerformRodRetraction( bool_c * pbRetracted )
{
	upErr eErr = ERR_OK;

	// Act on retraction request
	LOG_ERROR_RETURN( eErr = g_pDigitalInputOutput->GetInput( DIG_IN_MOTOR_RETRACT, pbRetracted ) )
	if ( m_bRetractRod )
		{
		if ( false == *pbRetracted )
			{
			LOG_ERROR_RETURN( eErr = SetTargetVelocity( PUMP_ROD_RETRACT_VEL ) )
			}

		// Retracted. Motion halted by CStepperVelocity
		else
			{
			m_bRetractRod = false;
			LOG_ERROR_RETURN( eErr = ResetStepperPosition() )
			}
		}

	return eErr;
}

/**
 * @brief	This method sets the stepper velocity to achieve a flow rate in uL per minute
 * 			which is 1000 times the mL per minute rate. "5000" will get 5ml/min
 *
 * @param	iUlPerMin Water flow rate in uL per minute
 *
 * @return   Status as upErr
 */
upErr CWaterCntl::SetFlowRateUlPerMin( int iUlPerMin )
{
	m_rate = (long int) iUlPerMin;
	return SetTargetVelocity( UL_PER_MIN_TO_STEP_PER_SEC( m_rate, g_pTreatmentInfo->getAsyringe() ) );
}

/**
 * @brief	This method sets the stepper velocity to achieve a flow rate in mL per minute
 *
 * @param	iMlPerMin Water flow rate in mL per minute
 *
 * @return   Status as upErr
 */
upErr CWaterCntl::SetFlowRateMlPerMin( int iMlPerMin )
{
	m_rate = ML_TO_UL( (long int) iMlPerMin );
	return SetTargetVelocity( UL_PER_MIN_TO_STEP_PER_SEC( m_rate, g_pTreatmentInfo->getAsyringe() ) );
}

/**
 * @brief	This method reads the position feedback for the stepper motor
 *
 * @return   Status as upErr
 */
upErr CWaterCntl::ReadFromHardware()
{
	return ReadStepperPosition( RND_ROBIN_TICK_CNT );
}

/**
 * @brief	This method rotates the stepper motor to retract the rod fully. It will retract the rod until the retracted
 * 			limit switch come ON. Only start the retraction if it's not retracted already.
 *
 * @return   Status as upErr
 */
upErr CWaterCntl::StartRetractingPumpRod()
{
	byte bRet;
	g_pDigitalInputOutput->GetInput( DIG_IN_MOTOR_RETRACT, &bRet );
	if ( false == bRet )
		m_bRetractRod = true;
	return ERR_OK;
}

/**
 * @brief	This method rotates the stepper motor to extend the rod fully.
 *
 * It will extend the rod until the extended limit switch come ON or the system commands it to halt.
 *
 * @param	pbExtended A pointer to a bool_c value to receive TRUE if the leadscrew is extended
 *
 * @return   Status as upErr
 */
upErr CWaterCntl::ExtendPumpRod( bool_c * pbExtended )
{
	upErr eErr = ERR_OK;

	LOG_ERROR_RETURN( eErr = g_pDigitalInputOutput->GetInput( DIG_IN_MOTOR_EXTEND, pbExtended ) )
	if ( false == *pbExtended )
		{
		LOG_ERROR_RETURN( eErr = SetFlowRateMlPerMin( g_pTreatmentInfo->getWratePrime3() ) )
		}

	return eErr;
}

/**
 * @brief	This method returns the falg that says if the rod has been retracted
 *
 * * @param	pbRetracted A pointer to a bool_c value to receive TRUE if the leadscrew is retracted
 *
 * @return   true if retract has been requested
 */
bool_c CWaterCntl::GetRetracted( bool_c * pbRetracted )
{
	LOG_ERROR_RETURN( g_pDigitalInputOutput->GetInput( DIG_IN_MOTOR_RETRACT, pbRetracted ) )
	return m_bRetractRod;
}

// Private Class Functions


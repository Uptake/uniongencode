/**
 * @file	OneWireMemoryHelper.cpp
 * @par		Package: UGMcu
 * @par		Project: Union Generator
 * @par		Copyright (c) 2017, Uptake Medical Technology, Inc.
 * @author	Chris Jul 22, 2011 Created file
 * @brief	This file contains the code for the COneWireMemoryHelper class
 */

// Include Files
#include "OneWireMemoryHelper.h"
// External Public Data

// File Scope Macros and Constants

// File Scope Type Definitions (Enums, Structs & Classes)

// File Scope Data

// File Scope Functions

// Public Functions

/**
 * @brief	This is the constructor for OneWireMemoryHelper.
 *
 */
COneWireMemoryHelper::COneWireMemoryHelper()
{
	m_eventHead = 0;
	m_eventCnt = 0;
	m_currentState.eTopState = OW_STATE_IDLE;
	m_currentState.eSubState = GEN_STATE_END;
	m_currentState.isRunning = false;
}


/**
 * @brief	This method allows adding to the queue via a public method
 *
 * @param	eEvent The event to add to the queue
 *
 * @return   Status as upErr
 */
upErr COneWireMemoryHelper::AddEvent( upOneWireEvent eEvent )
{
	upErr eErr = ERR_OK;
	eErr = EventQPush( eEvent );
	return eErr;
}

/**
 * @brief	This method gets the next top-state by popping an event from the
 * 			event queue and transitioning to the proper top-state that
 *    		corresponds to the event.
 *
 * @return  The updated state as a SOwState
 */
SOwState COneWireMemoryHelper::GetNextTopState()
{
	// have to take a loERR_OK at the next event in the
	// queue to determine the top state
	upOneWireEvent eEvent;
	upOneWireState eState;
	eEvent = GetNextEvent();
	eState = EventToTopState( eEvent );
	TransitionToState( eState );
	return m_currentState;
}

/**
 * @brief	This method allows the sub-state of the current top-state
 *    		to be updated via protected access
 *
 * @param	eState The sub-state to set
 *
 * @return  The updated top-state as a SOwState
 */
SOwState COneWireMemoryHelper::SetSubState( upGenericState eState )
{
	m_currentState.eSubState = eState;
	m_currentState.bSubStatePass = 0;
	return m_currentState;
}

/**
 * @brief	This method increments the current sub state pass count
 *
 * @return   The modified state as a SOwState
 */
SOwState COneWireMemoryHelper::IncrementSubStatePass()
{
	m_currentState.bSubStatePass++;
	return m_currentState;
}

/**
 * @brief	This method clears the event queue and sets the current
 * 			top state to IDLE.
 *
 * @return   void
 */
void COneWireMemoryHelper::ResetEventQ()
{
	m_eventHead = 0;
	m_eventCnt = 0;
	TransitionToState( OW_STATE_IDLE );
}

// Private Class Functions

/**
 * @brief	This method pops an event from the bottom of the queue
 *
 * @return   The popped event as a upOneWireEvent
 */
upOneWireEvent COneWireMemoryHelper::EventQPop()
{
	upOneWireEvent eEvent = OW_EVENT_NONE;
	if ( m_eventCnt > 0 )
		{
		eEvent = m_eventQ[ m_eventHead ];
		m_eventHead++;
		if ( m_eventHead >= MAX_OW_EVENTS )
			m_eventHead = 0;
		m_eventCnt--;
		}

	return eEvent;
}

/**
 * @brief	This method adds an event to the top of the queue.  If the
 *    		event is full, a full queue error is returned.
 *
 * @param 	eEvent The event to put on the queue as upOneWireEvent
 *
 * @return  Status as upErr
 */
upErr COneWireMemoryHelper::EventQPush( upOneWireEvent eEvent )
{

	upErr eErr = ERR_OK;
	byte bIndex = 0;
	if ( ( eErr = GetNextFreeEventIndex( &bIndex ) ) != ERR_OK )
		return eErr;

	m_eventQ[ bIndex ] = eEvent;
	return eErr;
}

/**
 * @brief	This method gets the index of the next free entry in the event queue
 *
 * @param	pbIndex A pointer to a byte that will receive the entry index. Zero if not free
 *
 * @return   Status as upErr. Error if queue is full
 */
upErr COneWireMemoryHelper::GetNextFreeEventIndex( byte * pbIndex )
{
	byte bNext;
	upErr eErr;

	bNext = m_eventCnt + 1;
	if ( bNext >= MAX_OW_EVENTS )
		{
		eErr = ERR_OW_EVENT_Q_FULL;
		*pbIndex = 0;
		}
	else
		{
		eErr = ERR_OK;
		m_eventCnt = bNext;
		bNext = bNext + m_eventHead - 1;
		if ( bNext >= MAX_OW_EVENTS )
			bNext -= MAX_OW_EVENTS;
		*pbIndex = bNext;
		}

	return eErr;
}

/**
 * @brief	This method requests the next event from the queue
 *
 * @return   The event as a upOneWireEvent
 */
upOneWireEvent COneWireMemoryHelper::GetNextEvent()
{
	return EventQPop();
}

/**
 * @brief	This method transitions to a new top state and
 *    		resets the sub-state and the number of sub state passes.
 *   		This method is of private scope to ensure that the
 *    		OneWireMemoryHelper class is solely responsible for
 *    		determining the top state.
 *
 * @param	eState The state to transition to
 *
 * @return   void
 */
void COneWireMemoryHelper::TransitionToState( upOneWireState eState )
{
	m_currentState.eTopState = eState;
	m_currentState.eSubState = GEN_STATE_START;
	m_currentState.isRunning = false;
	m_currentState.bSubStatePass = 0;
}

/**
 * @brief	This method returns the current top state
 *
 * @return   The state as a SOwState
 */
SOwState COneWireMemoryHelper::GetCurrentState()
{
	return m_currentState;
}

/**
 * @brief	This method gets the proper top-state that corresponds to
 *    		a particular event.
 *
 * @param   eEvent The event that corresponds to the top state
 *
 * @return  The top state as a upOneWireState
 */
upOneWireState COneWireMemoryHelper::EventToTopState( upOneWireEvent eEvent )
{
	upOneWireState eState = OW_STATE_IDLE;
	switch ( eEvent )
		{
		case ( OW_EVENT_NEW_MEMORY ):
			eState = OW_STATE_READ_ALL;
			break;
		case ( OW_EVENT_STAMP_FIRST_USE ):
			eState = OW_STATE_WRITE_FIRSTUSE;
			break;
		case ( OW_EVENT_DATA_CHANGE ):
			eState = OW_STATE_WRITE_TREATMENT;
			break;
		case ( OW_EVENT_LOCK_DEVICE ):
			eState = OW_STATE_WRITE_LOCKDOWN;
			break;
		case ( OW_EVENT_NONE ):
		default:
			eState = OW_STATE_IDLE;
			break;
		}
	return eState;
}


/**
 * @file	SbcIntfc.cpp
 * @par		Package: UGMcu
 * @par		Project: Union Generator
 * @par		Copyright (c) 2017, Uptake Medical Technology, Inc.
 * @author	Glen Sep 17, 2010 Created file
 * @brief	This file contains the code, CSbcIntfc, for processing the incoming and outgoing inter-processor communication. It deals
 * 			with assembling and decomposing the data packets. It does not apply the link-layer protocol content.
 */

// Include Files
#include "SbcIntfc.h"
#include "string.h"
#include "stdio.h"
#include "stdlib.h"
#include "Messages.h"
#include "CommonOperations.h"
#include "OneWireMemory.h"
#include "TreatmentRecord.h"
#include "WaterCntl.h"

// External Public Data
CSbcIntfc * g_pSbcIntfc = 0;

// File Scope Macros and Constants

// File Scope Type Definitions (Enums, Structs & Classes)
// File Scope Data
// File Scope Functions

// Public Methods

/**
 * @brief	This is the constructor for CSbcIntfc. It sets the initial values
 * 			for cached SBC values
 *
 */
CSbcIntfc::CSbcIntfc()
{
	g_pSbcIntfc = this;
#ifdef 	SIM_OUTPUT_CAPTURE
	m_lastSbcState = SBC_STATE_TECH_DIAG;
#else
	m_lastSbcState = SBC_STATE_OOS;
#endif
	m_lastSbcRequest = MSG_TYPE_OPCOND;
	m_msgOpcon.operState = (byte) OPER_STATE_BOOT;
	m_mcuRequest = MSG_TYPE_SBCCOND;
	m_lastMsgRecvd = MSG_TYPE_NOTSPECIFIED;
	m_timerSbcState = 0;
	// Must be non-zero for simulation
	m_sbcTimeSet = false;
	m_timerSbcStateFlag = false;
	m_lastLogSeqSent = 0;
	m_lastLogAckRx = 0;
	m_logRequests = 0;
	m_userMsgRequests = 0;
	m_msgLog.msgLength = 0;
	m_ePopMsgOnAck = POP_Q_NONE;
	m_lastSbcCmd = SBC_CMD_UNDEFINED;
}

/**
 * @brief	This method initializes the IPC communication hardware
 *
 * @return   Status as upErr
 */
upErr CSbcIntfc::InitializeHardware()
{
	upErr err = ERR_OK;
	LOG_ERROR_RETURN( err = CSbcSerialIntfc::InitializeHardware() )
	return err;
}

/**
 * @brief	This method is responsible for sending a
 *   		requested message type to the data-link-layer for
 *    		processing.  If the SbcIntfc is in a wait state, or if
 *    		there are pending outgoing messages, no new messages
 *    		will be added to the output queue.
 *
 * @return   Status as upErr
 */
upErr CSbcIntfc::WriteToHardware()
{
	upErr eErr = ERR_OK;
#ifndef SIM_OUTPUT_CAPTURE
	bool_c bWaitFlag = true;
	upMessageTypes eMsgType;
	byte bMsgCount = 0;

	CheckWaitForReplyFlag( &bWaitFlag );
	// if we are waiting for a reply, return immediately
	if ( bWaitFlag )
		return eErr;

	// Check the ack bit in the last received packet if waiting
	// to pop a message from either the log or user message Q's.
	if ( POP_Q_NONE != m_ePopMsgOnAck )
		{
		PopFromQIfLastMsgAckd();
		// Reset the flag
		m_ePopMsgOnAck = POP_Q_NONE;
		}

	GetSbcRequestType( &eMsgType );
	switch ( eMsgType )
		{
		case MSG_TYPE_LOG:
			m_logRequests++;
			g_pLogMessage->GetLogCount( &bMsgCount );
			// Allow at most one log to be sent at a time
			// and ensure at least 1 message is waiting in the queue
			if ( m_logRequests > 1 || 0 == bMsgCount )
				{
				// default to OPCON to ensure operating state info throughput
				eMsgType = MSG_TYPE_OPCOND;
				// reset the log request
				m_logRequests = 0;
				}
			break;
		case MSG_TYPE_USERMSG:
			m_userMsgRequests++;
			g_pLogMessage->GetUserMsgCount( &bMsgCount );
			// Allow at most one user message to be sent at a time
			// and ensure at least 1 message is waiting in the queue
			if ( m_userMsgRequests > 1 || 0 == bMsgCount )
				{
				// default to OPCON to ensure operating state info throughput
				eMsgType = MSG_TYPE_OPCOND;
				// reset the user msg requests
				m_userMsgRequests = 0;
				}
			break;
		default:
			break;
		}
	if ( ( eErr = ReadySbcRequest( eMsgType ) ) != ERR_OK )
		// if there was a problem readying the message ( an empty queue, etc.)
		// send an opcon as a failsafe
		eMsgType = MSG_TYPE_OPCOND;

	SendMessage( eMsgType );
#endif
	// timeouts are handled by the SbcSerialIntfc
	eErr = CSbcSerialIntfc::WriteToHardware();
	return eErr;
}

/**
 * @brief	This method reads messages from the message incoming queue
 *    		and updates the current request type and the SbcIntfc message
 *    		received.
 *
 * @return   Status as upErr
 */
upErr CSbcIntfc::ReadFromHardware()
{
	upErr eErr = ERR_OK;
	upMessageTypes eMsgType;

	// Grab any waiting message, then update SbcIntfc with the current request
	// and the specific message type and data received
	eErr = CSbcSerialIntfc::ReadFromHardware();

	if ( PacketReceived() )
		{
		// find the next message waiting in the queue then update
		// the appropriate message
		eErr = ReturnNextMsgType( &eMsgType );
		if ( eMsgType == MSG_TYPE_NOTSPECIFIED )
			{
			eErr = ERR_IPC_QUEUE_ERROR;
			}
		else
			RecvMessageUpdate( eMsgType );
		}
	return eErr;
}

/**
 * @brief	This method send the operating conditions and states to the SBC
 *
 *
 * @return   Status as upErr
 */
upErr CSbcIntfc::SendTextOperatingConditions()
{
	upErr eErr = ERR_OK;
#ifdef SIM_OUTPUT_CAPTURE
	char sTemp[SBC_SEND_BYTE_MAX];
	int stringLength = 0;
	snprintf( sTemp, SBC_SEND_BYTE_MAX, "%d,%d,%d,%d,%d,%d,%d,%04X%04X,%02X,%lX,%5u,%d\r\n",
			m_msgOpcon.stbUnused0,
			m_msgOpcon.stbUnused1,
			m_msgOpcon.coilTemp,
			m_msgOpcon.outletTemp,
			m_msgOpcon.intTemp,
			m_msgOpcon.tbRfPower,
			m_msgOpcon.iRfTemperature,
			m_msgOpcon.tbRfStatus,
			m_msgOpcon.tbRfError,
			(twobytes) m_msgOpcon.digInput,
			(long unsigned int) m_msgOpcon.encoderPos,
			m_msgOpcon.runTicks,
			m_msgOpcon.operState );
	stringLength = strlen( sTemp );
	if ( stringLength > SBC_SEND_BYTE_MAX )
		{
		stringLength = SBC_SEND_BYTE_MAX;
		sTemp[ SBC_SEND_BYTE_MAX - 1 ] = 0;
		}
	eErr = WritePort( RS232_SBC, (twobytes)stringLength, (byte*)sTemp );
#endif
	return eErr;
}

/**
 * @brief	This method resets the critical timeout timestamp to the current tick.
 *
 * @return   Status as upErr
 */
upErr CSbcIntfc::ResetCriticalTimer()
{
	return ResetCriticalTimestamp();
}

/**
 * @brief	This method send a text message to the SBC. This will be used for informational and error messages
 *
 * @param	iCnt 	Count of bytes in message
 * @param	pbBytes	Pointer to buffer of bytes containing the message
 *
 * @return   Status as upErr
 */
upErr CSbcIntfc::SendTextMessage( int iCnt, byte *pbBytes )
{
	return WriteBuffer( iCnt, pbBytes );
}

/**
 * @brief	This method returns a pointer to the requested message type
 *
 * @param   pMsgStruct A pointer to a message structure to return
 * @param   eMsg The message type to return
 *
 * @return   Status as upErr
 */
upErr CSbcIntfc::GetMessage( void ** pMsgStruct, upMessageTypes eMsg )
{
	upErr eErr = ERR_OK;
	switch ( eMsg )
		{
		case ( MSG_TYPE_SBCCOND ):
			*pMsgStruct = (void*) &m_msgSbcState;
			break;
		case ( MSG_TYPE_OPCOND ):
			*pMsgStruct = (void*) &m_msgOpcon;
			break;
		case ( MSG_TYPE_LOG ):
			*pMsgStruct = (void*) &m_msgLog;
			break;
		case ( MSG_TYPE_SETUP ):
			*pMsgStruct = (void*) &m_msgSetup;
			break;
		case ( MSG_TYPE_SBCCMD ):
			*pMsgStruct = (void*) &m_msgSbcCmd;
			break;
		case ( MSG_TYPE_TREATMENTINFO ):
			*pMsgStruct = (void*) &m_msgTreatmentInfo;
			break;
		case ( MSG_TYPE_USERMSG ):
			*pMsgStruct = (void*) &m_msgUserMsg;
			break;
		case ( MSG_TYPE_HPINFO ):
			*pMsgStruct = (void*) &m_msgHpInfo;
			break;
		case ( MSG_TYPE_MCUINFO ):
			*pMsgStruct = (void*) &m_msgMcuInfo;
			break;
		case ( MSG_TYPE_DELIVERYSTEPS ):
			*pMsgStruct = (void*) &m_msgDeliverySteps;
			break;
		case ( MSG_TYPE_TREATSELECT ):
			*pMsgStruct = (void*) &m_msgTreatmentSelection;
			break;
		default:
			*pMsgStruct = (void*) &m_msgOpcon;
			break;
		}
	return eErr;
}

/**
 * @brief	This method enqueues a message to be sent to the SBC.
 *    		This method also allows a message to be flagged so that
 *    		an Ack can be checked against it.
 *
 * @param	eMsgType The type of the outgoing message
 *
 * @return   Status as upErr
 */
upErr CSbcIntfc::SendMessage( upMessageTypes eMsgType )
{
	upErr eErr = ERR_OK;
	upMessageTypes eRequestType;
	void * pMsgStruct = 0;
	twobytes tbMsgSize = 0;

	switch ( eMsgType )
		{
		case ( MSG_TYPE_SBCCOND ):
			pMsgStruct = (void*) &m_msgSbcState;
			tbMsgSize = sizeof( m_msgSbcState ) / sizeof(byte);
			break;
		case ( MSG_TYPE_OPCOND ):
			pMsgStruct = (void*) &m_msgOpcon;
			tbMsgSize = sizeof( m_msgOpcon ) / sizeof(byte);
			break;
		case ( MSG_TYPE_LOG ):
			pMsgStruct = (void*) &m_msgLog;
			// The log length is variable
			tbMsgSize = m_msgLog.msgLength;
			break;
		case ( MSG_TYPE_SETUP ):
			pMsgStruct = (void*) &m_msgSetup;
			tbMsgSize = sizeof( m_msgSetup ) / sizeof(byte);
			break;
		case ( MSG_TYPE_SBCCMD ):
			pMsgStruct = (void*) &m_msgSbcCmd;
			tbMsgSize = sizeof( m_msgSbcCmd ) / sizeof(byte);
			break;
		case ( MSG_TYPE_TREATMENTINFO ):
			pMsgStruct = (void*) &m_msgTreatmentInfo;
			tbMsgSize = sizeof( m_msgTreatmentInfo ) / sizeof(byte);
			break;
		case ( MSG_TYPE_USERMSG ):
			pMsgStruct = (void*) &m_msgUserMsg;
			tbMsgSize = sizeof( m_msgUserMsg ) / sizeof(byte);
			break;
		case ( MSG_TYPE_HPINFO ):
			pMsgStruct = (void*) &m_msgHpInfo;
			tbMsgSize = sizeof( m_msgHpInfo ) / sizeof(byte);
			break;
		case ( MSG_TYPE_MCUINFO ):
			pMsgStruct = (void*) &m_msgMcuInfo;
			tbMsgSize = sizeof( m_msgMcuInfo ) / sizeof(byte);
			break;
		case ( MSG_TYPE_DELIVERYSTEPS ):
			g_pLogMessage->LogErrorMessage( ERR_OK, "Sending delivery steps" );
			pMsgStruct = (void*) &m_msgDeliverySteps;
			tbMsgSize = sizeof( m_msgDeliverySteps ) / sizeof(byte);
			break;
		case ( MSG_TYPE_TREATSELECT ):
			g_pLogMessage->LogErrorMessage( ERR_OK, "Sending treatment selection" );
			pMsgStruct = (void*) &m_msgTreatmentSelection;
			tbMsgSize = sizeof( m_msgTreatmentSelection ) / sizeof(byte);
			break;
		default:
			tbMsgSize = 0;
			break;
		}
	// Return if message unknown
	if ( 0 == tbMsgSize )
		return ERR_BAD_PARAM;

	LOG_ERROR_RETURN( RequestController( &eRequestType ) )
	eErr = MessageToPacketBuffer( (byte) eMsgType, (byte*) pMsgStruct, tbMsgSize, eRequestType );
	return eErr;
}

// Private Methods
/**
 * @brief	This method retrieves the oldest log message from the
 *   		log message queue.  The retrieved log message is dequeued.
 *
 * @return   Status as upErr
 */
upErr CSbcIntfc::UpdateLogMessage()
{
	upErr eErr = ERR_OK;
	byte pbLen;
	//	byte * locBuf = m_msgLog.message;
	eErr = g_pLogMessage->PeekLogMessage( (char *) m_msgLog.message, &pbLen );
	// Must update the log message length.  This will never be sent.
	if ( ERR_OK == eErr )
		{
		m_msgLog.msgLength = pbLen;
		m_ePopMsgOnAck = POP_Q_LOG;
		}
	else
		{
		m_msgLog.msgLength = 0;
		m_ePopMsgOnAck = POP_Q_NONE;
		}
	return eErr;
}

/**
 * @brief	This method retrieves the oldest user message from the
 *    		user message queue.  The retrieved log message is dequeued.
 *
 * @return   Status as upErr
 */
upErr CSbcIntfc::UpdateUserMessage()
{
	upErr eErr = ERR_OK;
	eErr = g_pLogMessage->PeekUserMessage( &m_msgUserMsg );
	if ( ERR_OK == eErr )
		{
		m_ePopMsgOnAck = POP_Q_USER;
		}
	else
		m_ePopMsgOnAck = POP_Q_NONE;
	return eErr;
}

/**
 * @brief	This method updates the delivery tool info message with
 *    		the latest information stored in the one-wire memory class
 *
 * @return   void
 */
upErr CSbcIntfc::UpdateDTInfo()
{
	upErr eErr = ERR_OK;
	unsignedfourbytes therapyTime;
	g_pOneWireMemory->GetFirstUseTime( &m_msgHpInfo.connectTime );
	m_msgHpInfo.therapyCode = (byte) g_pTreatmentInfo->getTreatmentCode();
	g_pOneWireMemory->GetSerialNumber( &m_msgHpInfo.hpSerial );
	g_pOneWireMemory->GetLotNumber( m_msgHpInfo.lotNumber );
	g_pOneWireMemory->GetTherapyTime( &therapyTime );
	m_msgHpInfo.therapyTime = therapyTime;
	g_pOneWireMemory->GetFullTreatmentCount( &m_msgHpInfo.treatmentCnt );
	g_pOneWireMemory->GetModel( m_msgHpInfo.handpieceModel );
	m_msgHpInfo.maxTreatCount = g_pTreatmentInfo->getDeliveryCount();
	m_msgHpInfo.maxUseTime = g_pTreatmentInfo->getTvapor();
	return eErr;
}

/**
 * @brief	This method determines what the next requested message
 *    		type to the SBC should be and returns the type.
 *
 * @param   peRequestType A pointer to receive the next request type
 *
 * @return   Status as upErr
 */
upErr CSbcIntfc::RequestController( upMessageTypes * peRequestType )
{
	upErr eErr = ERR_OK;
	switch ( m_lastSbcState )
		{
		case ( SBC_STATE_OOS ):
		case ( SBC_STATE_PRIME ):
		case ( SBC_STATE_DELIVERY ):
		case ( SBC_STATE_OTHER ):
		case ( SBC_STATE_TECH_DIAG ):
			*peRequestType = MSG_TYPE_SBCCOND;
			break;
		default:
			*peRequestType = MSG_TYPE_SBCCOND;
			eErr = ERR_BAD_PARAM;
			break;
		}

	return eErr;
}

/**
 * @brief	This method updates an SbcIntfc message with
 *    		message data waiting in the received packet Q.
 *
 * @param   eMsgType  The message type to update
 *
 * @return   Status as upErr
 */
upErr CSbcIntfc::RecvMessageUpdate( upMessageTypes eMsgType )
{
	upErr eErr = ERR_OK;
	switch ( eMsgType )
		{
		case MSG_TYPE_SBCCOND:
			eErr = PacketBufferToMessage( (byte*) &m_msgSbcState, sizeof( m_msgSbcState ) / sizeof(byte) );
			if ( ERR_OK == eErr )
				{
				m_lastSbcState = (upSbcState) m_msgSbcState.screenState;
				// if this is the first time the SBC time has been received, update the sbcTime variable
				if ( !m_sbcTimeSet )
					{
					SetTime( m_msgSbcState.sbcTime );
					m_sbcTimeSet = true;
					}
				}
			break;
		case MSG_TYPE_SETUP:
			eErr = PacketBufferToMessage( (byte*) &m_msgSetup, sizeof( m_msgSetup ) / sizeof(byte) );
			if ( ERR_OK == eErr )
				eErr = UpdateCustomSetup();
			break;
		case MSG_TYPE_SBCCMD:
			eErr = PacketBufferToMessage( (byte*) &m_msgSbcCmd, sizeof( m_msgSbcCmd ) / sizeof(byte) );
			if ( ERR_OK == eErr )
				eErr = HandleSbcCommand();
			break;
		case MSG_TYPE_HPINFO:
			if ( 1 == g_bTestAccess )
				eErr = PacketBufferToMessage( (byte*) &m_msgHpInfo, sizeof( m_msgHpInfo ) / sizeof(byte) );
			else
				eErr = ERR_BAD_PARAM;
			break;
		case MSG_TYPE_DELIVERYSTEPS:
			g_pLogMessage->LogErrorMessage( ERR_OK, "Received delivery steps" );
			eErr = PacketBufferToMessage( (byte*) &m_msgDeliverySteps, sizeof( m_msgDeliverySteps ) / sizeof(byte) );
			if ( ERR_OK == eErr )
				{
				g_pLogMessage->LogErrorMessage( ERR_OK, "Setting delivery steps" );
				eErr = g_pTreatmentInfo->SetStepValues( &m_msgDeliverySteps );
				g_pTreatmentInfo->setTreatmentCode( TREATMENT_CUSTOM );
				}
			else
				{
				g_pLogMessage->LogErrorMessage( eErr, "Failed to set delivery steps." );
				}
		case MSG_TYPE_TREATSELECT:
			g_pLogMessage->LogErrorMessage( ERR_OK, "Received treatment selection" );
			eErr = PacketBufferToMessage( (byte*) &m_msgTreatmentSelection,
			        sizeof( m_msgTreatmentSelection ) / sizeof(byte) );
			if ( ERR_OK == eErr )
				{
				g_pLogMessage->LogErrorMessage( ERR_OK, "Setting treatment selection" );
// TODO Set treatment selection				eErr = g_pTreatmentInfo->SetStepValues( &m_msgDeliverySteps );
//				g_pTreatmentInfo->setTreatmentCode( TREATMENT_CUSTOM );
				}
			else
				{
				g_pLogMessage->LogErrorMessage( eErr, "Failed to set treatment selection" );
				}
			break;
		default:
			break;
		}

	if ( ERR_OK == eErr )
		m_lastMsgRecvd = eMsgType;

	return eErr;
}

/**
 * @brief	This method helps perform an SBC command
 *
 * @return   Status as upErr
 */
upErr CSbcIntfc::HandleSbcCommand()
{
	upErr eErr = ERR_OK;
	upTreatmentCode eTreatment = TREATMENT_UNSET;

	switch ( m_msgSbcCmd.command )
		{
		case SBC_CMD_UNDEFINED:
			eErr = ERR_BAD_PARAM;
			break;
		case SBC_CMD_RELEASE_SYRINGE:
			eErr = g_pWaterCntl->StartRetractingPumpRod();
			break;
		case SBC_CMD_PRIMED:
			g_pLogMessage->LogMessage( "Primed CMD" );
			break;
		case SBC_CMD_NOT_PRIMED:
			g_pLogMessage->LogMessage( "Not Primed CMD" );
			break;
		case SBC_CMD_DELIVERY_UNLOCK:
			g_pLogMessage->LogMessage( "Unlock CMD" );
			break;
		case SBC_CMD_SETUP:
			g_pLogMessage->LogMessage( "Setup CMD" );
			break;
		case SBC_CMD_BUTTON1_DOWN:
			// syringe ON
			bool_c pbExtended;
			eErr = g_pWaterCntl->ExtendPumpRod( &pbExtended );
			break;
		case SBC_CMD_BUTTON2_DOWN:
			// syringe off
			eErr = g_pWaterCntl->SetFlowRateUlPerMin( 0 );
			break;
		case SBC_CMD_BUTTON3_DOWN:
			break;
		case SBC_CMD_RADIO1_ON:
			eTreatment = TREATMENT_CUSTOM;
			break;
		case SBC_CMD_RADIO2_ON:
			eTreatment = TREATMENT_V_1_0;
			break;
		case SBC_CMD_RADIO3_ON:
			eTreatment = TREATMENT_SPECIAL_1;
			break;
		case SBC_CMD_RADIO4_ON:
			eTreatment = TREATMENT_SPECIAL_2;
			break;
		default:
			g_pLogMessage->LogErrorMessageInt( eErr, "Unknown CMD", (int) m_msgSbcCmd.command );
			eErr = ERR_BAD_PARAM;
			break;
		}

	// Save last command for CSupervisor to fetch
	m_lastSbcCmd = (upSbcCmd) m_msgSbcCmd.command;

	// Have the new treatment code made the default.
	if ( eTreatment != TREATMENT_UNSET )
		{
		eErr = g_pTreatmentInfo->setTreatmentCode( eTreatment );
		if ( ERR_OK == eErr )
			ResetCustomSetup();
		}

	return eErr;
}

/**
 * @brief	This method gets the next message type waiting in the
 *    		in packet queue.
 *
 * @param   peMsgType A pointer to a message type to return
 *
 * @return   Status as upErr
 */
upErr CSbcIntfc::GetWaitingMsgType( upMessageTypes * peMsgType )
{
	upErr eErr = ERR_OK;
	eErr = ReturnNextMsgType( peMsgType );
	return eErr;
}

/**
 * @brief	This method updates the CTreatmentInformation class with the
 * 			current information stored in the setup message.  This method
 * 			sets the therapy code to TREATMENT_CUSTOM to ensure
 * 			the custom therapy parameters are used once set.
 *
 * @return   Status as upErr
 */
upErr CSbcIntfc::UpdateCustomSetup()
{
	upErr eErr = ERR_OK;
	g_pTreatmentInfo->setTreatmentCode( TREATMENT_CUSTOM );
	// Set custom values
	g_pTreatmentInfo->setDeliveryCount( m_msgSetup.DeliveryCount );
	g_pTreatmentInfo->setTdelivery( m_msgSetup.Tdelivery );
	g_pTreatmentInfo->setTvapor( m_msgSetup.Tvapor );
	g_pTreatmentInfo->setTdeb( m_msgSetup.Tdeb );
	g_pTreatmentInfo->setCustom6( m_msgSetup.bCustom6 );
	g_pTreatmentInfo->setDeliveryMin( m_msgSetup.TdeliveryMin );
	g_pTreatmentInfo->setDeliveryMax( m_msgSetup.TdeliveryMax );
	g_pTreatmentInfo->setWratePrime3( m_msgSetup.WratePrime3 );
	g_pTreatmentInfo->setOverTempBand( m_msgSetup.OverTempBand );
	g_pTreatmentInfo->setWrateDelivery( m_msgSetup.stbCustom9 );
	g_pTreatmentInfo->setCustom19( m_msgSetup.stbCustom19 );
	g_pTreatmentInfo->setCustom22( m_msgSetup.bCustom22 );
	g_pTreatmentInfo->setTrest( m_msgSetup.Trest );
	g_pTreatmentInfo->setAsyringe( m_msgSetup.Asyringe );
	g_pTreatmentInfo->setCustom15( m_msgSetup.stbCustom15 );
	g_pTreatmentInfo->setCoilIdleTempSetpoint( m_msgSetup.uiCustom5 );
	g_pTreatmentInfo->setCustom17( m_msgSetup.stbCustom17 );
	g_pTreatmentInfo->setCustom18( m_msgSetup.stbCustom18 );
	g_pTreatmentInfo->setTempUpperLimitIdle( m_msgSetup.TempUpperLimitIdle );
	g_pTreatmentInfo->setTempLowerLimitIdle( m_msgSetup.TempLowerLimitIdle );
	g_pTreatmentInfo->setCustom8( m_msgSetup.stbCustom8 );
	g_pTreatmentInfo->setCoilTempUpper( m_msgSetup.CoilUpperTemp );
	g_pTreatmentInfo->setCoilTempLower( m_msgSetup.CoilTempLower );
	g_pTreatmentInfo->setMaxInternalTemp( m_msgSetup.MaxInternalTemp );
	g_pTreatmentInfo->setOutletTempMax( m_msgSetup.OutletTempMax );
	g_pTreatmentInfo->setOutletTempMin( m_msgSetup.OutletTempMin );
	g_pTreatmentInfo->setRfPowerTol( m_msgSetup.RFpowerTol );
	g_pTreatmentInfo->setPowerDelivery( (twobytes) m_msgSetup.uiCustom12 );
	g_pTreatmentInfo->setCoilTempShutdownTol( m_msgSetup.CoilTempShutdownTol );
	g_pTreatmentInfo->setOutletTempShutdownTol( m_msgSetup.OutletTempShutdownTol );
	g_pTreatmentInfo->setTminValueTest( m_msgSetup.TminValueTest );
	g_pTreatmentInfo->setEmcCycles( m_msgSetup.EmcCycles );
	g_pTreatmentInfo->setTprecond( m_msgSetup.Tprecond );
	g_pTreatmentInfo->setTminPrimeValueTest( m_msgSetup.TminPrimeValueTest );
	g_pTreatmentInfo->setWrateIdle( m_msgSetup.WrateIdle );
	g_pTreatmentInfo->setPowerIdle( m_msgSetup.RFpowerIdle );
	g_pTreatmentInfo->setWratePrime1( m_msgSetup.WratePrime1 );
	g_pTreatmentInfo->setWratePrime2( m_msgSetup.WratePrime2 );
	g_pTreatmentInfo->setVprime1( m_msgSetup.Vprime1 );
	g_pTreatmentInfo->setVprime2( m_msgSetup.Vprime2 );
	g_pTreatmentInfo->setTtempWindow( m_msgSetup.TtempWindow );
	g_pTreatmentInfo->setCooldownTime( m_msgSetup.uiCustom13 );

	return eErr;
}

/**
 * @brief	This method sets the setup message therapy parameters
 *    		to their default values as assigned in the CTreatmentInformation class
 *
 */
void CSbcIntfc::GetCustomSetup()
{
	upTreatmentCode eCodeTemp = g_pTreatmentInfo->getTreatmentCode();
	// Setting the therapy code to custom is temporary
	// to gain access to custom parameters
	if ( TREATMENT_UNSET == eCodeTemp )
		eCodeTemp = TREATMENT_DEFAULT;
	g_pTreatmentInfo->setTreatmentCode( TREATMENT_CUSTOM );

	m_msgSetup.DeliveryCount = g_pTreatmentInfo->getDeliveryCount();
	m_msgSetup.Tdelivery = g_pTreatmentInfo->getTdelivery();
	m_msgSetup.Tdeb = g_pTreatmentInfo->getTdeb();
	m_msgSetup.bCustom6 = g_pTreatmentInfo->getCustom6();
	m_msgSetup.TdeliveryMax = g_pTreatmentInfo->getDeliveryMax();
	m_msgSetup.WratePrime3 = g_pTreatmentInfo->getWratePrime3();
	m_msgSetup.OverTempBand = g_pTreatmentInfo->getOverTempBand();
	m_msgSetup.stbCustom9 = g_pTreatmentInfo->getWrateDelivery();
	m_msgSetup.stbCustom19 = g_pTreatmentInfo->getCustom19();
	m_msgSetup.TdeliveryMin = g_pTreatmentInfo->getDeliveryMin();
	m_msgSetup.bCustom22 = g_pTreatmentInfo->getCustom22();
	m_msgSetup.Trest = g_pTreatmentInfo->getTrest();
	m_msgSetup.Asyringe = g_pTreatmentInfo->getAsyringe();
	m_msgSetup.Tvapor = g_pTreatmentInfo->getTvapor();
	m_msgSetup.stbCustom16 = 0;
	m_msgSetup.stbCustom17 = g_pTreatmentInfo->getCustom17();
	m_msgSetup.stbCustom15 = g_pTreatmentInfo->getCustom15();
	m_msgSetup.TempUpperLimitIdle = g_pTreatmentInfo->getTempUpperLimitIdle();
	m_msgSetup.TempLowerLimitIdle = g_pTreatmentInfo->getTempLowerLimitIdle();
	m_msgSetup.stbCustom8 = g_pTreatmentInfo->getCustom8();
	m_msgSetup.CoilUpperTemp = g_pTreatmentInfo->getCoilTempUpper();
	m_msgSetup.MaxInternalTemp = g_pTreatmentInfo->getMaxInternalTemp();
	m_msgSetup.CoilTempLower = g_pTreatmentInfo->getCoilTempLower();
	m_msgSetup.OutletTempMax = g_pTreatmentInfo->getOutletTempMax();
	m_msgSetup.OutletTempMin = g_pTreatmentInfo->getOutletTempMin();
	m_msgSetup.RFpowerTol = g_pTreatmentInfo->getRfPowerTol();
	m_msgSetup.uiCustom12 = g_pTreatmentInfo->getPowerDelivery();
	m_msgSetup.CoilTempShutdownTol = g_pTreatmentInfo->getCoilTempShutdownTol();
	m_msgSetup.OutletTempShutdownTol = g_pTreatmentInfo->getOutletTempShutdownTol();
	m_msgSetup.TminValueTest = g_pTreatmentInfo->getTminValueTest();
	m_msgSetup.EmcCycles = g_pTreatmentInfo->getEmcCycles();
	m_msgSetup.tbCustom23 = 0;
	m_msgSetup.TtempWindow = g_pTreatmentInfo->getTtempWindow();
	m_msgSetup.TminPrimeValueTest = g_pTreatmentInfo->getTminPrimeValueTest();
	m_msgSetup.WrateIdle = g_pTreatmentInfo->getWrateIdle();
	m_msgSetup.RFpowerIdle = g_pTreatmentInfo->getPowerIdle();
	m_msgSetup.WratePrime1 = g_pTreatmentInfo->getWratePrime1();
	m_msgSetup.WratePrime2 = g_pTreatmentInfo->getWratePrime2();
	m_msgSetup.Vprime1 = g_pTreatmentInfo->getVprime1();
	m_msgSetup.Vprime2 = g_pTreatmentInfo->getVprime2();
	m_msgSetup.stbCustom18 = g_pTreatmentInfo->getCustom18();
	m_msgSetup.uiCustom13 = g_pTreatmentInfo->getCooldownTime();
	m_msgSetup.Tprecond = g_pTreatmentInfo->getTprecond();
	m_msgSetup.tbCustom24 = 0;
	m_msgSetup.tbCustom25 = 0;
	m_msgSetup.uiCustom4 = 0;
	m_msgSetup.uiCustom5 = g_pTreatmentInfo->getCoilIdleTempSetpoint();
	m_msgSetup.stbCustom20 = 0;
	m_msgSetup.stbCustom10 = 0;
	m_msgSetup.uiCustom7 = 0;
	m_msgSetup.uiCustom21 = 0;
	m_msgSetup.stbCustom14 = 0;
	g_pTreatmentInfo->setTreatmentCode( eCodeTemp );
}

/**
 * @brief	This method resets the custom therapy setup message parameters
 *    		to default therapy code values.
 *
 */
void CSbcIntfc::ResetCustomSetup()
{
	upTreatmentCode eModel;
	eModel = g_pTreatmentInfo->getTreatmentCode();
	g_pTreatmentInfo->ResetCustomValues( eModel );
	GetCustomSetup();
}

/**
 * @brief	This method returns the last state of the SBC GUI from the cache
 *
 * @param	pSbcState A Pointer to and upSbcState to receive the cached state of the SBC
 *
 * @return   Status as upErr
 */
upErr CSbcIntfc::ReadSbcState( upSbcState * pSbcState )
{
	*pSbcState = m_lastSbcState;

	return ERR_OK;
}

/**
 * @brief	This method checks for a critical communication timeout
 *    		as monitored by the SbcSerialIntfc class
 *
 * @return  upErr: ERR_OK == no critical timeout
 * @return  upErr: ERR_IPC_CRITICAL_TIMEOUT == A critical timeout has occurred
 */
upErr CSbcIntfc::CheckForTimeout()
{
	// Do not return a timeout if ablating

	return GetTimeoutFlag();
}

/**
 * @brief	This method sets the next SBC request
 *
 * @param	bRequest The message type request
 *
 * @return   Status as upErr
 */
upErr CSbcIntfc::SetNextSbcRequest( byte bRequest )
{
	upErr eErr = ERR_OK;
	m_lastSbcRequest = bRequest;
	return eErr;
}

/**
 * @brief	This method sets the next SBC request
 *
 * @param	bRequest The message type request
 *
 * @return   Status as upErr
 */
upErr CSbcIntfc::SetNextMcuRequest( byte bRequest )
{
	upErr eErr = ERR_OK;
	m_mcuRequest = bRequest;
	return eErr;
}

/**
 * @brief	This method updates any messages as necessary
 *    		before being sent to the SBC.
 *
 * @param   eRequestType A pointer to receive the next request type
 *
 * @return   Status as upErr
 */
upErr CSbcIntfc::ReadySbcRequest( upMessageTypes eRequestType )
{
	upErr eErr = ERR_OK;
	switch ( eRequestType )
		{
		case ( MSG_TYPE_OPCOND ):
			// Log counts updated by the supervisor
			// but grab the most recent counts before sending
			{
			byte bMsgCount = 0;
			g_pLogMessage->GetUserMsgCount( &bMsgCount );
			m_msgOpcon.userMsgs = bMsgCount;
			g_pLogMessage->GetLogCount( &bMsgCount );
			m_msgOpcon.logMsgs = bMsgCount;
			}
			break;
		case ( MSG_TYPE_LOG ):
			UpdateLogMessage();
			break;
		case ( MSG_TYPE_HPINFO ):
			UpdateDTInfo();
			break;
		case ( MSG_TYPE_TREATMENTINFO ):
			g_pTreatmentRecord->UpdateTreatmentInfoMessage();
			break;
		case ( MSG_TYPE_USERMSG ):
			UpdateUserMessage();
			break;
		case ( MSG_TYPE_SETUP ):
			// Once a setup message is requested, the current therapy code is set to custom
			GetCustomSetup();
			break;
		case ( MSG_TYPE_DELIVERYSTEPS ):
			g_pLogMessage->LogErrorMessage( ERR_OK, "Readying delivery steps" );
			g_pTreatmentInfo->GetStepValues( &m_msgDeliverySteps );
			g_pTreatmentInfo->setTreatmentCode( TREATMENT_CUSTOM );
			break;
		case ( MSG_TYPE_TREATSELECT ):
			g_pLogMessage->LogErrorMessage( ERR_OK, "Readying treatment selection" );
// TODO add treatment selection			g_pTreatmentInfo->GetStepValues( &m_msgDeliverySteps );
// TODO add treatment selection				g_pTreatmentInfo->setTreatmentCode( TREATMENT_CUSTOM );
			break;
		default:
			break;
		}

	return eErr;
}

/**
 * @brief	This method determines what the next requested
 *    		message type should be and returns the requested
 *    		type through pRequest.
 *
 * @param   pRequest A pointer to the request to return
 *
 * @return   Status as upErr
 */
upErr CSbcIntfc::GetSbcRequestType( upMessageTypes * pRequest )
{
	upErr eErr = ERR_OK;
	eErr = GetLastSbcRequest( &m_lastSbcRequest );
	*pRequest = (upMessageTypes) m_lastSbcRequest;

	return eErr;
}

/**
 * @brief	This method returns the last successfully received message type.
 *    		This method is mainly for support of applications that make use
 *    		of the shared ApplicationLib.
 *
 * @return   Message type as upMessageTypes
 */
upMessageTypes CSbcIntfc::GetLastMsgReceived()
{
	return m_lastMsgRecvd;
}

/**
 * @brief	This method pops a message from the log message or
 * 			user message FIFO Q depending on the status of the
 * 			ack bit in the last received message.
 *
 * @return  Status as upErr: ERR_BAD_PARAM == Neither Q was flagged
 */
upErr CSbcIntfc::PopFromQIfLastMsgAckd()
{
	upErr eErr = ERR_OK;

	if ( !GetLastAckStatus() )
		return eErr;

	switch ( m_ePopMsgOnAck )
		{
		case POP_Q_LOG:
			eErr = g_pLogMessage->PopLogMessage();
			break;
		case POP_Q_USER:
			eErr = g_pLogMessage->PopUserMessage();
			break;
		default:
			eErr = ERR_BAD_PARAM;
			break;
		}

	return eErr;
}

/**
 * @brief	This method allows callers to get the last command received from the SBC
 *
 * @param	peSbcCmd A poiner to a caller-allocated upSbcCmd to receive the last command
 *
 * @return   Status as upErr
 */
upErr CSbcIntfc::GetLastCmnd( upSbcCmd* peSbcCmd )
{
	if ( NULL == peSbcCmd )
		{
		return ERR_BAD_PARAM;
		}

	*peSbcCmd = m_lastSbcCmd;
	return ERR_OK;
}

/**
 * @brief	This method allows a caller to set the last command. This may be used to clear the last command.
 *
 * @param	eSbcCmd The new value for the last command as an upSbcCmd
 *
 * @return   Status as upErr
 */
upErr CSbcIntfc::SetLastCmnd( upSbcCmd eSbcCmd )
{
	m_lastSbcCmd = SBC_CMD_UNDEFINED;
	return ERR_OK;
}

/**
 * @file	CommonOperations.cpp
 * @par		Package: UGMcu
 * @par		Project: Union Generator
 * @par		Copyright (c) 2017, Uptake Medical Technology, Inc.
 * @author	Glen Nov 3, 2010 Created file
 * @brief	This file contains the declaration for operations shared between multiple actions as CCommonOperations
 */

// Include Files
#include "CommonOperations.h"
#include "TreatmentInformation.h"
#include "GeneratorMonitor.h"
#include "DigitalInputOutput.h"
#include "WaterCntl.h"
#include "LogMessage.h"
#include "Temperature.h"
#include "RFGenerator.h"
#include "Messages.h"
#include "Version.h"
#include "SbcIntfc.h"
#include "string.h"
#include "stdlib.h"
#include "stdio.h"
#include "OneWireMemory.h"

// External Public Data
CCommonOperations * g_pComOp = 0;

// File Scope Objects, Macros and Constants
#define VERS_LENGTH_CPLD ( 15 )

// File Scope Type Definitions (Enums, Structs & Classes)

// Values used in coil idling control
#define COIL_IDLE_MIN_TICKS				MS_TO_TICKS(600)	//!< Don't change power more often than 600ms

/**
 * @brief This structure contains a coherent set of versions for the CPLD, MCU code and SBC code that create
 * 			a system version. All are character strings.
 */
struct SVersionSet
{
	const char * sCpld;
	const char * sSbc;
	const char * sMcuCode;
	const char * sSys;
};

/**
 * @brief These are the sets of version strings that are acceptable and define a specific release.
 *
 * It is placed in .xmem to conserve SRAM resources
 *
 */
static const SVersionSet f_versionSets[] =
{
// Simulation version
{ VERSION_SIMULATION_CPLD, VERSION_SIMULATION_SBC, MCU_VERSION_ID, VERSION_SIMULATION_SYSTEM },

  // Recent or upcoming SBC releases with trunk MCU lab build
  { VERSION_8_7_12_CPLD, VERSION_SBC_CURRENT, VERSION_TODAY_MCU, VERSION_SYSTEM_LAB_REVG },

  // System releases that match customized MCU version from Version.h
  { VERSION_CPLD_CURRENT, VERSION_SBC_CURRENT, VERSION_MCU_CURRENT, VERSION_SYSTEM_CURRENT },

  // System releases that match customized MCU version with later CPLD from Version.h
  { VERSION_8_7_12_CPLD, VERSION_SBC_CURRENT, VERSION_MCU_CURRENT, VERSION_SYSTEM_CURRENT } };

byte f_bVersionCount = sizeof( f_versionSets ) / sizeof(SVersionSet);

// File Scope Data
const char * CCommonOperations::m_sVersionId = MCU_VERSION_ID;

// File Scope Functions

// Public Functions

/**
 * @brief	This is the constructor for CCommonOperations.
 *
 * It assigns the @a this pointer to g_pComOp for use as a singleton.
 *
 * It clears the tick counters to zero and sets up the PID control for coil idling.
 *
 */
CCommonOperations::CCommonOperations()
{
	g_pComOp = this;
	m_subState = GEN_STATE_START;
	m_sSystemVersionId = (char*) "unset";
	m_tCoilIdleTick = 0;
	m_bCurrentStep = 0;
	for ( byte i = 0; i < DELIVERY_STEPS_MAX; i++ )
		{
		m_tStepEnd[ i ] = 0;
		}

}

/**
 * @brief	This method start the power and flow immediately.
 *
 * @param	iWrate	Water rate to use in uL/min
 * @param 	tbPower	Power to apply in Watts
 *
 * @return   Status as upErr
 */
upErr CCommonOperations::InitiateVaporImmediately( int iWrate, twobytes tbPower )
{

	upErr eErr = ERR_ERROR;
	// Perform initialization of flow
	LOG_ERROR_RETURN( eErr = g_pRFGen->SetOutputEnable( true ) )
	LOG_ERROR_RETURN( eErr = g_pRFGen->SetPowerRequest( tbPower ) )
	LOG_ERROR_RETURN( eErr = g_pWaterCntl->SetFlowRateUlPerMin( iWrate ) )
	SetSubState( GEN_STATE_COMPLETE );

	return eErr;
}

/**
 * @brief	This method halts the water pump and turns off RF output.
 *
 * @return   Status as upErr
 */
upErr CCommonOperations::HaltPumpAndRF()
{
	upErr eErr = ERR_ERROR;
	LOG_ERROR_RETURN( eErr = g_pWaterCntl->HaltStepper() )
	LOG_ERROR_RETURN( eErr = g_pRFGen->SetPowerRequest( 0 ) )
	LOG_ERROR_RETURN( eErr = g_pRFGen->SetOutputEnable( false ) )
	return eErr;
}

/**
 * @brief	This method gets the version numbers that identify
 * 		    the real-time software and the CPLD.
 *
 * @return  Status as upErr
 */
upErr CCommonOperations::GetRtcVersions()
{
	upErr eErr = ERR_OK;
	SMcuInfo * psMcuInfo;

	// Get structure from SBC interface
	LOG_ERROR_RETURN( eErr = g_pSbcIntfc->GetMessage( (void** )&psMcuInfo, MSG_TYPE_MCUINFO ) )
	if ( ERR_OK != eErr )
		return eErr;

	// Update the Mcu Version Info message with the known version info
	strncpy( psMcuInfo->mcuVersion, GetMcuVersion(), VERSION_MSG_LENGTH_MCU );
	strncpy( psMcuInfo->systemVersion, m_sSystemVersionId, VERSION_MSG_LENGTH_SYSTEM ); // Likely not set yet

	// Get CPLD
	LOG_ERROR_RETURN( eErr = GetCpldVersion( psMcuInfo->cpldVersion ) )
	if ( ERR_OK != eErr )
		return eErr;

	// Create dummy serial number
	strncpy( psMcuInfo->mcuSerial, "1000000000", VERSION_MSG_LENGTH_SERIAL );

	return eErr;
}

/**
 * @brief	This method verifies that the version number received by the SBC
 * 			and the CPLD are a viable set with the MCU version.
 *
 * 			There may be multiple sets of potential matches and each set may produce a different system version number.
 * 			This method assumes the SBC interface is in a CONNECTED state.
 *
 * @return  Status as upErr.
 */
upErr CCommonOperations::VerifyCodeVersions()
{
	upErr eErr = ERR_OK;
	SSBCCond * psSbcState = 0;
	char sCpld[ VERS_LENGTH_CPLD ];
	bool_c bMatch = false;
	char * pSbcVersion = 0;

	// Get SBC and CPLD versions
	eErr = g_pSbcIntfc->GetMessage( (void**) &psSbcState, MSG_TYPE_SBCCOND );
	if ( ERR_OK != eErr || 0 == psSbcState )
		return eErr;

	pSbcVersion = psSbcState->sbcVersion;

#ifdef SIM_OUTPUT_CAPTURE
	pSbcVersion = (char*) VERSION_SBC_V0p9p2;
#endif

	eErr = GetCpldVersion( sCpld );
	if ( ERR_OK != eErr )
		return eErr;

	// LoERR_OK for match of set
	byte i;
	for ( i = 0; i < f_bVersionCount && false == bMatch; )
		{
		if ( 0 == strncmp( (const char*) pSbcVersion, f_versionSets[ i ].sSbc, VERSION_MSG_LENGTH_SBC ) //
		&& 0 == strncmp( (const char*) sCpld, f_versionSets[ i ].sCpld, VERSION_MSG_LENGTH_CPLD ) //
		&& 0 == strncmp( (const char*) m_sVersionId, f_versionSets[ i ].sMcuCode, VERSION_MSG_LENGTH_MCU ) )
			{
			bMatch = true;
			}
		else
			i++;
		}

	if ( bMatch )
		{
		m_sSystemVersionId = f_versionSets[ i ].sSys;
		}
	else
		{
		eErr = ERR_VERSION_CHECK;
		m_sSystemVersionId = (char*) "Check FAILED";
		}

	// Save system version into SBC data
	SMcuInfo * psMcuInfo;
	upErr eErr2;

	LOG_ERROR_RETURN( eErr2 = g_pSbcIntfc->GetMessage( (void** )&psMcuInfo, MSG_TYPE_MCUINFO ) )
	if ( ERR_OK == eErr2 )
		{
		strncpy( psMcuInfo->systemVersion, m_sSystemVersionId, VERSION_MSG_LENGTH_SYSTEM );
		}

	return eErr;
}

/**
 * @brief	This method fetches and returns the CPLD version number.  The
 * @brief   version number is returned as a string. pcCpldVersion must be of
 * @brief   size CPLD_VERSION_SIZE or there is a risk of buffer overflow.
 *
 * @param	pcCpldVersion A pointer to store the CPLD version
 *
 * @return   Status as upErr
 */
upErr CCommonOperations::GetCpldVersion( char * pcCpldVersion )
{
	upErr eErr = ERR_OK;
	byte cpldVersion[ MMAP_FUNC_CPLD_VERSION_SIZE ];
	char toString[ VERS_LENGTH_CPLD ];
	pcCpldVersion[ 0 ] = 0;

	eErr = MMAP_READ( MMAP_FUNC_CPLD_VERSION, cpldVersion )
	;
	if ( ERR_OK != eErr )
		return eErr;

	// individual version bytes are in reverse order
	for ( int i = MMAP_FUNC_CPLD_VERSION_SIZE - 1; i > 0; i-- )
		{
		itoa( (int) cpldVersion[ i ], toString, BASE_10 );
		strncat( pcCpldVersion, toString, VERS_LENGTH_CPLD );
		strncat( pcCpldVersion, "-", VERS_LENGTH_CPLD );
		}
	// the last bit is in hex
	snprintf( toString, VERS_LENGTH_CPLD, "%X", cpldVersion[ 0 ] );
	strncat( pcCpldVersion, toString, VERS_LENGTH_CPLD );
	return eErr;
}

/**
 * @brief	This method performs the "middle" of the ablation and priming operations which is where
 * 			the steam is generated at a constant level.
 *
 * It monitors the generator operation and delivery device trigger and checks that temperatures are within normal ranges. It also
 * monitors the elapsed time and will end the ablation once the full time has elapsed.
 *
 * @param tbElapsedTicks 		The ticks since the start of the vapor flow stage as a tick
 * @param tbMinValueCheckTicks 	The ticks from the start where the minimum value check should start to be performed as a tick
 *
 * @return   Status as upErr
 */
upErr CCommonOperations::ConstantVaporFlow( tick tbElapsedTicks, tick tbMinValueCheckTicks )
{
	upErr eErr = ERR_OK;

	// Check for minimum temps after TminValueTest has elapsed
	if ( tbElapsedTicks >= tbMinValueCheckTicks )
		{

		// Check coil and outlet temperatures against minimums
		int iCoilTemp;
		int iOutletTemp;
		eErr = g_pTemperature->GetFilteredThermoTemp( THERMO_COIL, &iCoilTemp );
		if ( ERR_OK == eErr )
			{
			eErr = g_pTemperature->GetFilteredThermoTemp( THERMO_OUTLET, &iOutletTemp );
			}

		if ( ERR_OK != eErr )
			{
			// A thermo error is an internal fault
			LOG_ERROR_RETURN( eErr = ERR_THERMO_ERROR )
			}
		else if ( iCoilTemp < g_pTreatmentInfo->getCoilTempLower() )
			{
			LOG_ERROR_RETURN( eErr = ERR_THERMO_UNDER_TEMP )
			g_pLogMessage->LogErrorMessageInt( eErr, "Coil", iCoilTemp );
			}
		else if ( iOutletTemp < g_pTreatmentInfo->getOutletTempMin() )
			{
			LOG_ERROR_RETURN( eErr = ERR_THERMO_UNDER_TEMP )
			g_pLogMessage->LogErrorMessageInt( eErr, "Outlet", iOutletTemp );
			}
		}
	return eErr;
}

/**
 * @brief	This method is used by unit tests to set the sub state.
 *
 * @param	eState The generic state to set
 *
 */
void CCommonOperations::SetSubState( upGenericState eState )
{
	m_subState = eState;
}

/**
 * @brief	This method is used by unit tests to get the sub state.
 *
 * @return   The sub state as upGenericState
 */
upGenericState CCommonOperations::GetSubState()
{
	return m_subState;
}

/**
 * @brief	This method returns sets the next common operation to do a fresh start.
 *
 * @return   ERR_OK
 */
upErr CCommonOperations::SetStartState()
{
	m_subState = GEN_STATE_START;
	return ERR_OK;
}

/**
 * @brief	This method starts a coil idling by shutting off power and starting water flow.
 *
 * 			It also resets the start time so the RF will be OFF for the first time period. This will
 * 			allow the 1-wire operations to be done without the RF being ON.
 *
 * @return   Status as upErr
 */
upErr CCommonOperations::StartCoilIdle()
{
	CPIDControl::SPIDCoefficients pidCoeff;

	upErr eErr;

// Get time of start operation
	GetTick( &m_tCoilIdleTick );

// Set RF generator OFF and set flow if that works otherwise flow zero
	LOG_ERROR_RETURN( eErr = g_pRFGen->SetOutputEnable( false ) )
	if ( ERR_OK == eErr )
		{
		LOG_ERROR_RETURN( eErr = g_pWaterCntl->SetFlowRateUlPerMin( g_pTreatmentInfo->getWrateIdle() ) )
		}
	else
		{
		LOG_ERROR_RETURN( g_pWaterCntl->SetFlowRateUlPerMin( 0 ) )
		}

// Coil
	pidCoeff.m_slope = 100;
	pidCoeff.m_offset = 0;
	pidCoeff.m_upMax = pidCoeff.m_slope / 4;
	pidCoeff.m_dnMax = pidCoeff.m_slope / 4;
	pidCoeff.m_Di = CNVT_REAL_TO_FACTOR( 0.85 );
	pidCoeff.m_Kc = CNVT_REAL_TO_FACTOR( 0.5 ); // 20% power base
	pidCoeff.m_Kd = CNVT_REAL_TO_FACTOR( 0 ); // None
	pidCoeff.m_Ki = CNVT_REAL_TO_FACTOR( 0.001 ); // Full power with 20 degrees total integrated
	pidCoeff.m_Kp = CNVT_REAL_TO_FACTOR( 0.08 ); // Full power when 12 degrees low

	m_CoilIdlePID.SetPIDCoefficients( &pidCoeff );
	m_CoilIdlePID.SetSetpoint( g_pTreatmentInfo->getCoilIdleTempSetpoint() );
	m_CoilIdlePID.ResetCalc();

	return eErr;
}

/**
 * @brief	This method initiates, or continues, the coil idling.
 *
 * 			It sets the water rate and RF power according to the parameters from CTreatmentInformation. It applies the
 * 			temperature control and time limits.
 *
 * @param 	tStartTick Time that the idling started in tick
 *
 * @return   Status as upErr
 */
upErr CCommonOperations::ApplyCoilIdle( tick tStartTick )
{
	upErr eErr;
	int iCoilTemp;
	int iOutletTemp;
	tick tPowerTicks;
	twobytes tbPowerReq = 0;

// Get time since last power change
	GetTickElapsed( m_tCoilIdleTick, &tPowerTicks );

// Determine if it is time to change power and get base flow rate
	bool_c bSwitch = ( tPowerTicks > COIL_IDLE_MIN_TICKS ? true : false ); // rest time between changes of power
	int iRate = g_pTreatmentInfo->getWrateIdle();

// Use filtered temperatures for evaluations
	LOG_ERROR_RETURN( eErr = g_pTemperature->GetFilteredThermoTemp( THERMO_COIL, &iCoilTemp ) )
	if ( ERR_OK == eErr )
		{
		LOG_ERROR_RETURN( eErr = g_pTemperature->GetFilteredThermoTemp( THERMO_OUTLET, &iOutletTemp ) )
		}

// Perform operational status checks on temperatures
	if ( ERR_OK == eErr )
		{
		if ( iCoilTemp > g_pTreatmentInfo->getTempUpperLimitIdle()
		        || iOutletTemp > g_pTreatmentInfo->getTempUpperLimitIdle() )
			{
			LOG_ERROR_RETURN( eErr = ERR_IDLE_THERMO_OVER_TEMP )
			}
		}

// Turn power and water OFF if there was an error
	if ( ERR_OK != eErr )
		{
		LOG_ERROR_RETURN( g_pComOp->HaltPumpAndRF() )
		g_pOneWireMemory->EnableOwMonitor( true );
		}

// Set idling water rate and power
	else
		{

		// Set water flow
		LOG_ERROR_RETURN( eErr = g_pWaterCntl->SetFlowRateUlPerMin( iRate ) )

		// Constant Power idle
		tbPowerReq = g_pTreatmentInfo->getPowerIdle();

		// PID Coil Idle
		// Compute new output
		int iNewPower;
		LOG_ERROR_RETURN( m_CoilIdlePID.ComputePID( iCoilTemp, RND_ROBIN_LOOP_MS, &iNewPower ) )

		// Compute new power level
		iNewPower = ( iNewPower * g_pTreatmentInfo->getPowerIdle() ) / 50; // ( control * 2 * power / 100 )
		if ( 10 > iNewPower )
			iNewPower = 0;
		tbPowerReq = (twobytes) iNewPower;

		if ( bSwitch && tbPowerReq != g_pRFGen->GetPowerRequest() )
			{

			// Turn OFF if ON for COIL_IDLE_MIN_TICKS or more and no power is needed
			if ( 0 == tbPowerReq )
				{
				g_pOneWireMemory->EnableOwMonitor( true );
				LOG_ERROR_RETURN( g_pRFGen->SetOutputEnable( false ) )
				LOG_ERROR_RETURN( g_pRFGen->SetPowerRequest( 0 ) )
				}

			// Or turn ON if COIL_IDLE_MIN_TICKS time has elapsed and power change is needed
			else
				{
				g_pOneWireMemory->EnableOwMonitor( false );
				LOG_ERROR_RETURN( g_pRFGen->SetOutputEnable( true ) )
				LOG_ERROR_RETURN( g_pRFGen->SetPowerRequest( tbPowerReq ) )
				}
			GetTick( &m_tCoilIdleTick );
			} // Switch and needs change
		} // ERR_OK

	return eErr;
}

/**
 * @brief	This method start a vapor delivery using the steps for power and flow.
 *
 * @return   Status as upErr
 */
upErr CCommonOperations::StartDeliverySteps()
{
	upErr eErr = ERR_OK;

	m_subState = GEN_STATE_ERROR;
	if ( 0 == g_pTreatmentInfo->GetStepCount() )
		{
		LOG_ERROR_RETURN( eErr = ERR_NO_STEPS )
		}
	else
		{
		byte i = 0;
		m_tStepEnd[ i ] = MS_TO_TICKS( g_pTreatmentInfo->GetStepTime( 0 ) );
		i++;
		for ( ; i < g_pTreatmentInfo->GetStepCount(); i++ )
			{
			m_tStepEnd[ i ] = m_tStepEnd[ i - 1 ] + MS_TO_TICKS( g_pTreatmentInfo->GetStepTime( i ) );
			}
		m_tStepEnd[ i ] = TICK_MAX;

		LOG_ERROR_RETURN(
		        eErr = InitiateVaporImmediately( g_pTreatmentInfo->GetStepWrate( 0 ), g_pTreatmentInfo->GetStepPower( 0 ) ) )
		if ( ERR_OK == eErr )
			{
			m_subState = GEN_STATE_START;
			}
		}

	m_bCurrentStep = 0;
	return eErr;
}

/**
 * @brief	This method advances through the delivery steps based on the supplied run time. It does not stop the delivery.
 *
 * @param	tbRunTicks The elapsed run time since delivery start as tick
 *
 * @return   Status as upErr
 */
upErr CCommonOperations::AdvancedDeliverySteps( tick tbRunTicks )
{
	upErr eErr = ERR_OK;

	if ( m_bCurrentStep + 1 < g_pTreatmentInfo->GetStepCount() - 1 )
		{
		if ( tbRunTicks >= m_tStepEnd[ m_bCurrentStep ] )
			{
			m_bCurrentStep++;
			g_pLogMessage->LogErrorMessage( ERR_OK, "Changing to step: ", (int) m_bCurrentStep );
			LOG_ERROR_RETURN( eErr = g_pRFGen->SetPowerRequest( g_pTreatmentInfo->GetStepPower( m_bCurrentStep ) ) )
			LOG_ERROR_RETURN( eErr = g_pWaterCntl->SetFlowRateUlPerMin( g_pTreatmentInfo->GetStepPower( m_bCurrentStep ) ) )
			}
		}
	else if ( tbRunTicks >= m_tStepEnd[ m_bCurrentStep ] )
		{
		m_subState = GEN_STATE_END;
		}

	return eErr;
}

// Private Class Functions

/**
 * @file	RFGenerator.cpp
 * @par		Package: UGMcu
 * @par		Project: Union Generator
 * @par		Copyright (c) 2017, Uptake Medical Technology, Inc.
 * @author	Glen Sep 15, 2010 Created file
 * @brief	This code is a base class for the RF generator interface implementations, CRFGenerator.
 *
 */

// Include Files
#include "UGMcuDefinitions.h"
#include "RFGenerator.h"
#include "DigitalInputOutput.h"
#include "LogMessage.h"
#include "stdio.h"
#include "Temperature.h"
#include "TreatmentInformation.h"
#include "CommonOperations.h"
#include "RFGeneratorItherm.h"

// External Public Data
CRFGenerator * g_pRFGen = 0;

// File Scope Macros and Constants
#define SETTER_RESPONSE_WAIT 		(20)
#define READ_STATUS_RESPONSE_WAIT 	(10)
#define INITIAL_COMM_RESPONSE_WAIT 	(10)

// File Scope Type Definitions (Enums, Structs & Classes)

// File Scope Data
static CRFGeneratorItherm f_RfGenItherm;

// File Scope Functions
CRFGenerator * CRFGenerator::m_baseRfGenObj = 0;	//!< A pointer to the CRFGenerator base class
upGenericState CRFGenerator::m_eAcPowerUp;			//!< The boolean for AC power being applied
int CRFGenerator::m_iAcPowerUpCycle;				//!< A counter used for going through the power-up steps
upErr CRFGenerator::m_generatorStatus;			//!< The generator status flag
bool_c CRFGenerator::m_acInput;						//!< The boolean for AC power being applied
byte CRFGenerator::m_bCommErr;						//!< Count of communication errors in a row
byte CRFGenerator::m_bFatalErrorCount;				//!< Count of fatal errors resulting in Reset operations
upErr CRFGenerator::m_criticalError;
upRfCoilStatus CRFGenerator::m_coilStatus;
bool_c CRFGenerator::m_bResetRequired;

// Public Functions

/**
 * @brief	This is the constructor for CRFGenerator.
 *
 * It clears the status values and captures the @a this pointer to g_pRFGen
 * for access as a singleton.
 *
 */
CRFGenerator::CRFGenerator()
{
	if ( 0 == g_pRFGen )
		{
		g_pRFGen = this; // Seed with something at construction and each child will paste over this
		}
	m_baseRfGenObj = this;
	CRFGenerator::SetInitValues();
}

/**
 * @brief	This method sets the member variables to their initial values.
 *
 * 			This is called from the constructor in the application and as-needed
 * 			in unit tests to effectively "re-new" an CRFGenerator object
 *
 */
void CRFGenerator::SetInitValues()
{
	m_eAcPowerUp = GEN_STATE_START;
	m_iAcPowerUpCycle = 0;
	m_applyingPower = false;
	m_outputEnabled = false;
	m_powerOutput = 0;
	m_acInput = false;
	m_powerOutput = 0;
#ifdef ENABLE_RDO_CURRENT_READ
	m_currentOutput = 0;
#endif
	m_appliedPowerRequest = 0;
	m_powerRequest = 0;
	m_bPowerNew = false;
	m_temperature = 0;
	m_statusBytes = 0;
	m_errorBytes = 0;
	m_generatorStatus = ERR_OK;
	m_bCommErr = 0;
	m_bFatalErrorCount = 0;
	m_criticalError = ERR_OK;
	m_coilStatus = RF_COIL_NOT_READY;
	m_bResetRequired = false;
}

/**
 * @brief	This method is mostly a stub for the child classes to override.
 *
 * 			It is used in the round robin loop prior to an RF generator being identified in the system.
 *
 * @return   Status as upErr
 */
upErr CRFGenerator::ReadFromHardware()
{
	upErr eErr = ERR_OK;

	// Just exit if AC not ON
	if ( false == m_acInput )
		{
		return eErr;
		}

	return m_generatorStatus;
}

/**
 * @brief	This method is overridden by child classes to send an initial command to the specific RF generator.
 *
 * @return   Status as upErr
 */
upErr CRFGenerator::SendInitialCommand()
{
	return ERR_ERROR;
}

/**
 * @brief	This method performs initialization and local parameter set-up.
 *
 * @return   Status as upErr
 */
upErr CRFGenerator::InitializeHardware()
{
	upErr eErr = ERR_OK;

	// Set up port
	// The port configs are the same. This is redundant and makes either available if one is removed.
	m_generatorStatus = eErr;

	return m_generatorStatus;
}

/**
 * @brief	This method controls  power-up communication and determines which RF generator is attached, if any.
 *
 * 			It alternately calls the SendInitialCommand() method on the two RF generator objects. If one responds
 * 			successfully then the pointer to that object is assigned to the global g_pRFgen variable. Then all application
 * 			code will access the object of the attached RF generator.
 *
 * @return   Status as upErr
 */
upErr CRFGenerator::WriteToHardware()
{
	upErr eErr = ERR_OK;

	// Exit if not ON
	if ( false == m_acInput )
		{
		return ERR_OK;
		}

	// Identify the RF generator that is attached
	switch ( m_eAcPowerUp )
		{

		// Power-up state machine
		// Wait a RF_GEN_POWER_UP_CYCLE_COUNT cycles for RF Generator CPU to potentially be alive
		case GEN_STATE_START:
			m_iAcPowerUpCycle++;
			if ( RF_GEN_POWER_UP_CYCLE_COUNT <= m_iAcPowerUpCycle )
				{
				m_eAcPowerUp = GEN_STATE_FIRST_ACTION;
				m_iAcPowerUpCycle = 0;
				}
			break;

			// Send the "init" command alternately to the iTherm and RDO and proceed once one answers
		case GEN_STATE_FIRST_ACTION:
			m_iAcPowerUpCycle++;

			// Neither RF generator powered up
			if ( RF_GEN_POWER_UP_CYCLE_MAX < m_iAcPowerUpCycle )
				{
				LOG_ERROR_RETURN( eErr = ERR_RF_GEN_NOT_FOUND )
				SetCriticalError( eErr );
				m_eAcPowerUp = GEN_STATE_COMPLETE;
				m_iAcPowerUpCycle = 0;
				m_generatorStatus = eErr;
				}

			// On odd cycles try the iTherm
			else if ( 1 == m_iAcPowerUpCycle % 2 )
				{
				// If iTherm answers up, cast global pointer and set to second action where iTherm
				// start-up process will take over
				if ( ERR_OK == f_RfGenItherm.SendInitialCommand() )
					{
					m_eAcPowerUp = GEN_STATE_SECOND_ACTION;
					}
				}

			break;

			// Use a cycle to set for iTherm to even out round robin cycle counts
			// for unit test and simulation runs
		case GEN_STATE_SECOND_ACTION:
			m_iAcPowerUpCycle = 0;
			g_pRFGen = static_cast<CRFGenerator*>( &f_RfGenItherm );
			break;

			// Go through COMPLETE to allow one cycle of error reporting
		case GEN_STATE_COMPLETE:
			SetACInput( false );
			m_eAcPowerUp = GEN_STATE_START;
			m_iAcPowerUpCycle = 0;
			break;

			// Bad state. Go on to error shutdown
		case GEN_STATE_END:
		default:
			LOG_ERROR_RETURN( ERR_ENUM_OUT_OF_RANGE )

			// Error found in testing
		case GEN_STATE_ERROR:
			SetACInput( false );
			LOG_ERROR_RETURN( eErr = ERR_RF_GEN_COMM_FATAL )
			m_eAcPowerUp = GEN_STATE_START;
			m_iAcPowerUpCycle = 0;
			break;

		}

	return m_generatorStatus;
}

/**
 * @brief	This method sets the critical error bit for the RDO operation state
 * 			and stores the critical error for later retrieval.
 *
 * @param	eErr The error to set as a upErr
 *
 * @return  void
 */
void CRFGenerator::SetCriticalError( upErr eErr )
{
	m_criticalError = eErr;
}

/**
 * @brief	This method gets the critical error status
 *
 * @return   Status as upErr
 */
upErr CRFGenerator::GetCriticalError()
{
	return m_criticalError;
}

/**
 * @brief	This method gets the coil status
 *
 * @return   Status as upRfCoilStatus
 */
upRfCoilStatus CRFGenerator::GetCoilStatus()
{
	return m_coilStatus;
}

/**
 * @brief	This method sets the coil status
 *
 * @param	eStatus The status as upRfCoilStatus
 *
 * @return   void
 */
void CRFGenerator::SetCoilStatus( upRfCoilStatus eStatus )
{
	m_coilStatus = eStatus;
}

/**
 * @brief	This method resets states that can change due to a reset
 *
 * @return   void
 */
void CRFGenerator::ResetStates()
{
	SetCoilStatus( RF_COIL_NOT_READY );
	m_bResetRequired = false;
}

/**
 * @brief	This method returns whether the RF requires a reset
 *
 * @return   Reset flag as a boolean
 */
bool_c CRFGenerator::ResetRequired()
{
	return m_bResetRequired;
}

// Private Class Functions

/**
 * @brief	This method turn the RF generator ON or OFF.
 *
 * @param	bOn	Flag of whether the generator should go ON (true) or OFF (false)
 *
 * @return   Status as upErr
 */
upErr CRFGenerator::SetOutputEnable( bool_c bOn )
{
	m_outputEnabled = bOn;
	return ERR_OK;
}

/**
 * @brief	This method is a stub to cover functionality only present in the RDO unit
 *
 * @return   ERR_OK
 */
upErr CRFGenerator::StartFreqSweep()
{
	return ERR_OK;
}

/**
 * @brief	This method returns the last requested power without calibration applied.
 *
 * 			It is an  function that returns m_powerRequest.
 *
 * @return   Last requested power in Watts
 */
twobytes CRFGenerator::GetPowerRequest()
{
	return m_powerRequest;
}

/**
 * @brief	This method returns the last applied requested power which is the value after calibration.
 *
 * 			It is a function that returns m_appliedPowerRequest.
 *
 * @return   Last applied power in Watts
 */
twobytes CRFGenerator::GetAppliedPowerRequest()
{
	return m_appliedPowerRequest;
}

/**
 * @brief	This method return the error bytes of the RF generator.
 *
 * 			The bit values are defined in the RF generator manuals and class definitons.
 *
 * @param	piError A pointer to a twobytes to get the RF generator error value
 *
 * @return   Status as upErr
 */
upErr CRFGenerator::GetError( twobytes *piError )
{
	*piError = m_errorBytes;

	if ( GEN_STATE_COMPLETE == m_eAcPowerUp )
		{
		return m_generatorStatus;
		}
	return ERR_RF_GEN_NOT_READY;
}

/**
 * @brief	This method return the status of the RF generator.
 *
 * 			The bit values are defined in the RF generator manuals and classes
 *
 * @param	piStatus A pointer to a twobytes to get the status
 *
 * @return   Status as upErr
 */
upErr CRFGenerator::GetStatus( twobytes *piStatus )
{

	*piStatus = m_statusBytes;
	return m_generatorStatus;
}

/**
 * @brief	This method gets the last temperature measured by the RF generator.
 *
 * @param	piTemperature A pointer to an integer to receive a integer in degrees Celsius
 *
 * @return   Status as upErr
 */
upErr CRFGenerator::GetTemperature( int *piTemperature )
{
	*piTemperature = m_temperature;
	return ERR_OK;
}

/**
 * @brief	This method gets the power ON status of the RF generator
 *
 * @param	piPowerOn A bool_c pointer to get whether there is power being applied
 *
 * @return   Status as upErr
 */
upErr CRFGenerator::GetApplyingPower( bool_c *piPowerOn )
{
	*piPowerOn = m_applyingPower;
	return ERR_OK;
}

/**
 * @brief	This method return the last power output reported from the RF generator.
 *
 * @param	ptbPowerOutput A pointer to a twobytes to get the applied power
 *
 * @return   Status as upErr
 */
upErr CRFGenerator::GetPowerOutput( twobytes * ptbPowerOutput )
{
	*ptbPowerOutput = m_powerOutput;
	return ERR_OK;
}

#ifdef ENABLE_RDO_CURRENT_READ
/**
 * @brief	This method return the last current output reported from the RF generator.
 *
 * @param	ptbPowerOutput A pointer to an twobytes to get the applied power
 *
 * @return   Status as upErr
 */
upErr CRFGenerator::GetCurrentOutput( twobytes * ptbCurrentOutput )
{
	*ptbCurrentOutput = m_currentOutput;
	return ERR_OK;
}
#endif

/**
 * @brief	This method gets the latest requested ON or OFF state of the RF generator. Whether
 * 			the generator is actually applying power is given by GetApplyingPower();
 *
 * @param	pbOn Pointer to receive overall On/Off state of generator
 *
 * @return   Status as upErr
 */
upErr CRFGenerator::GetOutputEnable( bool_c *pbOn )
{
	*pbOn = m_outputEnabled;
	return ERR_OK;
}

/**
 * @brief	This method turns on the AC power to the RF generator immediately
 *
 * @param	bOn  A boolean to turn on (true) or off the AC power as bool_c
 *
 * @return   Status as upErr
 */
upErr CRFGenerator::SetACInput( bool_c bOn )
{
	// Wait for OFF setting to clear error
	if ( GEN_STATE_ERROR == m_eAcPowerUp && bOn )
		return ERR_OK;

	if ( GEN_STATE_ERROR == m_eAcPowerUp )
		m_eAcPowerUp = GEN_STATE_END;

	g_pDigitalInputOutput->SetOutput( DIG_OUT_RF_AC_POWER, bOn );
	if ( bOn && m_acInput != bOn )
		{
		g_pDigitalInputOutput->WriteToHardware(); // Force ON now
		m_eAcPowerUp = GEN_STATE_START;
		m_iAcPowerUpCycle = 0;
		}

	// Clear generator status when going OFF and open RF enable switch
	if ( false == bOn )
		{
		m_generatorStatus = ERR_OK;
		m_powerOutput = 0;
		m_temperature = 0;
		m_statusBytes = 0;
		m_applyingPower = false;
		m_errorBytes = 0;
		m_eAcPowerUp = GEN_STATE_END;
		ResetStates();
		LOG_ERROR_RETURN( g_pDigitalInputOutput->SetOutput( DIG_OUT_RF_ENABLE, false ) )
		}

	m_acInput = bOn;
	return ERR_OK;
}

/**
 * @brief	This method gets the powered-on status of the RF generator
 *
 * @param	pbOn A bool_c* pointer to received the on (ture) or off status
 *
 * @return   Status as upErr
 */
upErr CRFGenerator::GetACInput( bool_c *pbOn )
{
	*pbOn = m_acInput;
	return ERR_OK;
}

/**
 * @brief	This method sets the power level and flags if it is a change from the previous level
 *
 * @param	tbPower The new power level in "watts" as a twobytes
 *
 * @return   Status as upErr
 */
upErr CRFGenerator::SetPowerRequest( twobytes tbPower )
{
	if ( tbPower != m_powerRequest )
		{
		m_powerRequest = tbPower;
		m_bPowerNew = true;
		}
	return ERR_OK;
}

/**
 * @brief	This method returns the duration that the RF generator needs to be OFF to effect a power-off reset.
 *
 * @return   Time to hold RF generator OFF in ticks
 */
twobytes CRFGenerator::GetACOffTicks()
{
	return MS_TO_TICKS( SEC_TO_MS( RF_GEN_AC_OFF_SECS ) );
}

/**
 * @brief	This method returns the flag giving the model of RF generator in use
 *
 * @param	peModel	A pointer of upRFGeneratorModel to receive the model flag in use
 *
 * @return   Status as upErr
 */
upErr CRFGenerator::GetRfGenModel( upRFGeneratorModel * peModel )
{
	*peModel = RF_MODEL_UNSET;
	return ERR_OK;
}

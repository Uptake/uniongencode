/**
 * @file	DeliveryOracle.cpp
 * @par		Package: McuApplicationLib
 * @par		Project: Flow-Based Generator
 * @par		Copyright (c) 2017, Uptake Medical Technology, Inc.
 * @author	Glen Jan 12, 2016 Created file
 * @brief	This file contains the code to manage delivery parameters and to convert them into step-based ablations
 */

// Include Files
#include "DeliveryOracle.h"
#include "stddef.h"
#include "LogMessage.h"
#include "TreatmentInformation.h"

// File Scope Type Definitions (Enums, Structs & Classes)
// File Scope Macros and Constants
// Forward Declarations
// Global Variable Declarations
CDeliveryOracle * g_pDelOracle = 0;

// File Scope Function Prototypes
// File Scope Variables
// File Scope Functions
// Public Functions

/**
 * @brief	This is the constructor for CDeliveryOracle. It assigns the g_pDelOracle pointer and clears m_lastSelection.
 *
 */
CDeliveryOracle::CDeliveryOracle()
{
	g_pDelOracle = this;
	m_lastSelection.Ttreat = 0;
	m_lastSelection.TtreatStatus = ERR_BAD_PARAM;
	m_lastSelection.unused2 = 0;
	m_lastSelection.unused2Status = ERR_BAD_PARAM;
	m_lastSelection.unused3 = 0;
	m_lastSelection.unused3Status = ERR_BAD_PARAM;
	m_lastSelection.unused4 = 0;
	m_lastSelection.unused4Status = ERR_BAD_PARAM;
}

/**
 * @brief	This method provides a new selection to be used. This causes the parameters of the
 * 			g_pTreatmentInfor object to be set for a subsequent delivery. This is only done if the
 * 			selection is approved.
 *
 * @param	refSelection A reference the new delivery selection as a reference to an STreatmentSelection
 *
 * @return   Status as upErr
 */
upErr CDeliveryOracle::NewSelection( STreatmentSelection& refSelection )
{
	upErr eErr = ERR_OK;

	LOG_ERROR_RETURN( eErr = CheckSelection( refSelection ) )
	if ( ERR_OK == eErr )
		{
		g_pTreatmentInfo->SetStepCount( 2 );
		g_pTreatmentInfo->SetStepPower( 0, g_pTreatmentInfo->getPowerDelivery() );
		g_pTreatmentInfo->SetStepTime( 0, SEC_TENTHS_TO_MS( refSelection.Ttreat ) - g_pTreatmentInfo->getCooldownTime() );
		g_pTreatmentInfo->SetStepWrate( 0, g_pTreatmentInfo->getWrateDelivery() );

		g_pTreatmentInfo->SetStepPower( 1, 0 );
		g_pTreatmentInfo->SetStepTime( 1, g_pTreatmentInfo->getCooldownTime() );
		g_pTreatmentInfo->SetStepWrate( 1, g_pTreatmentInfo->getWrateDelivery() );

		m_lastSelection = refSelection;
		}

	return eErr;
}

/**
 * @brief	This method causes a new selection to be evaluated.
 * 			The evaluation is returned as the return upErr
 *
 * @param	refSelection A reference the delivery selection as a reference to an STreatmentSelection
 *
 * @return   Status as upErr
 */
upErr CDeliveryOracle::CheckSelection( STreatmentSelection& refSelection )
{
	upErr eErr = ERR_OK;
	m_lastSelection = refSelection;
	if ( g_pTreatmentInfo->getDeliveryMin() > m_lastSelection.Ttreat )
		{
		LOG_ERROR_RETURN( eErr = ERR_BAD_TIME )
		g_pLogMessage->LogErrorMessageInt( eErr, "Ttreat under min", (int) m_lastSelection.Ttreat );
		}
	else if ( g_pTreatmentInfo->getDeliveryMax() < m_lastSelection.Ttreat )
		{
		LOG_ERROR_RETURN( eErr = ERR_BAD_TIME )
		g_pLogMessage->LogErrorMessageInt( eErr, "Ttreat over max", (int) m_lastSelection.Ttreat );
		}

	m_lastSelection.TtreatStatus = eErr;
	return eErr;
}

/**
 * @brief	This method returns the last set selection structure including its evaluation codes.
 *
 * @param	pSelection A pointer to a caller-provided STreatmentSelection
 *
 * @return   Status as upErr
 */
upErr CDeliveryOracle::GetLastSelection( STreatmentSelection * pSelection )
{
	if ( NULL == pSelection )
		{
		return ERR_BAD_PARAM;
		}
	*pSelection = m_lastSelection;
	return ERR_OK;
}

/**
 * @brief	This method sums up the time, in milliseconds, of each step of the multi-step vapor delivery
 *
 * @param	ptbTime A pointer to a caller-allocated twobytes to receive the time
 *
 * @return   Status as upErr
 */
upErr CDeliveryOracle::GetTotalTime( twobytes * ptbTime )
{
	*ptbTime = 0;

	if ( 0 == g_pTreatmentInfo->GetStepCount() )
		{
		return ERR_NO_STEPS;
		}

	for ( byte bCnt = 0; bCnt < g_pTreatmentInfo->GetStepCount(); bCnt++ )
		{
		*ptbTime += g_pTreatmentInfo->GetStepTime( bCnt );
		}

	return ERR_OK;
}

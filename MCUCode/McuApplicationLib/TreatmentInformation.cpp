/**
 * @file	TreatmentInformation.cpp
 * @par		Package: UGMcu
 * @par		Project: Union Generator
 * @par		Copyright (c) 2017, Uptake Medical Technology, Inc.
 * @author	Glen Oct 26, 2010 Created file
 * @brief	This file contains the code for CTreatmentInformation.
 */

// Include Files
#include "UGMcuDefinitions.h"
#include "TreatmentInformation.h"
#include "LogMessage.h"

// External Public Data
CTreatmentInformation * g_pTreatmentInfo = 0;

// File Scope Macros and Constants

// File Scope Type Definitions (Enums, Structs & Classes)

// File Scope Data

// File Scope Functions

// Public Functions

/**
 * @brief	This is the constructor for CTreatmentInformation.
 *
 * It sets initial values for the
 * 			TREATMENT_UNSET type, sets TREATMENT_UNSET as the type and captures @a this to
 * 			g_pTreatmentInfo for use as a singleton
 *
 */
CTreatmentInformation::CTreatmentInformation()
{
	g_pTreatmentInfo = this;
	m_TreatmentCode = TREATMENT_UNSET;
	ResetCustomValues( TREATMENT_DEFAULT );
}

/**
 * @brief	This method resets the custom therapy parameters accessed by TREATMENT_CUSTOM to the initial values
 *
 * @param 	eCode The model to use for the custom values
 *
 * @return	ERR_OK if reset was successful
 */
void CTreatmentInformation::ResetCustomValues( upTreatmentCode eCode )
{
	upTreatmentCode eTemp = getTreatmentCode();
	if ( TREATMENT_UNSET == eCode )
		eCode = TREATMENT_DEFAULT;
	setTreatmentCode( eCode );
	setDeliveryCount( getDeliveryCount() );
	setTdelivery( getTdelivery() );
	setTvapor( getTvapor() );
	setTdeb( getTdeb() );
	setCustom6( getCustom6() );
	setDeliveryMin( getDeliveryMin() );
	setDeliveryMax( getDeliveryMax() );
	setWratePrime3( getWratePrime3() );
	setWrateDelivery( getWrateDelivery() );
	setCustom19( getCustom19() );
	setCustom22( getCustom22() );
	setTrest( getTrest() );
	setAsyringe( getAsyringe() );
	setCoilIdleTempSetpoint( getCoilIdleTempSetpoint() );
	setCustom17( getCustom17() );
	setCustom15( getCustom15() );
	setCustom18( getCustom18() );
	setOverTempBand( getOverTempBand() );
	setCoilTempUpper( getCoilTempUpper() );
	setMaxInternalTemp( getMaxInternalTemp() );
	setCoilTempLower( getCoilTempLower() );
	setOutletTempMax( getOutletTempMax() );
	setOutletTempMin( getOutletTempMin() );
	setRfPowerTol( getRfPowerTol() );
	setPowerDelivery( getPowerDelivery() );
	setCoilTempShutdownTol( getCoilTempShutdownTol() );
	setOutletTempShutdownTol( getOutletTempShutdownTol() );
	setTminValueTest( getTminValueTest() );
	setTminPrimeValueTest( getTminPrimeValueTest() );
	setEmcCycles( getEmcCycles() );
	setTprecond( getTprecond() );
	setTtempWindow( getTtempWindow() );
	setCooldownTime( getCooldownTime() );
	setTempUpperLimitIdle( getTempUpperLimitIdle() );
	setTempLowerLimitIdle( getTempLowerLimitIdle() );
	setCustom8( getCustom8() );
	setWrateIdle( getWrateIdle() );
	setPowerIdle( getPowerIdle() );
	setWratePrime1( getWratePrime1() );
	setWratePrime2( getWratePrime2() );
	setVprime1( getVprime1() );
	setVprime2( getVprime2() );

	int i = 0;

	// Steps need to be set by code
	m_customDeliverySteps.power[ i ] = 300;
	m_customDeliverySteps.time[ i ] = 9000;
	m_customDeliverySteps.wRate[ i ] = 2800;
	i++;

	m_customDeliverySteps.count = i;

	// Reset active model enumeration
	setTreatmentCode( eTemp );
}

/**
 * @brief	This method returns the maximum allowed internal temperature before an error is raised in Celsius for
 * 			the current treatment code
 *
 * @return	Value according to treatment code
 */
int CTreatmentInformation::getMaxInternalTemp() const
{
	switch ( m_TreatmentCode )
		{
		case TREATMENT_ENGINEERING:
		case TREATMENT_V_1_0:
		case TREATMENT_UPTAKE_DEV_1:
		case TREATMENT_V_1_1:
			return 50;
		case TREATMENT_EMC:
			return 50;
		case TREATMENT_SHAM_V_1_0:
		case TREATMENT_SPECIAL_1:
		case TREATMENT_SPECIAL_2:
		case TREATMENT_SPECIAL_3:
		case TREATMENT_SPECIAL_4:
			return 50;
		default:
			break;
		}

	return m_iMaxInternalTemp;
}

/**
 * @brief	This method returns the minimum coil temperature before an error is raised in Celsius for the current treatment code
 *
 * @return	Value according to treatment code
 */
int CTreatmentInformation::getCoilTempLower() const
{
	switch ( m_TreatmentCode )
		{
		case TREATMENT_ENGINEERING:
		case TREATMENT_V_1_0:
		case TREATMENT_UPTAKE_DEV_1:
		case TREATMENT_V_1_1:
			return 50;
		case TREATMENT_EMC:
			return 0;
		case TREATMENT_SHAM_V_1_0:
		case TREATMENT_SPECIAL_1:
		case TREATMENT_SPECIAL_2:
		case TREATMENT_SPECIAL_3:
		case TREATMENT_SPECIAL_4:
			return 50;
		default:
			break;
		}

	return m_CoilTempLower;
}

/**
 * @brief	This method returns the band for flagging an error condition after coil or outlet has gone over temp in priming or ablation.
 *
 * 			This value is subtracted from the coil and outlet max temperatures to create a lower point to identify when the temperatures
 * 			have sufficiently cooled to start another treatment.
 *
 * @return	Value in degrees C according to treatment code
 */
int CTreatmentInformation::getOverTempBand() const
{
	switch ( m_TreatmentCode )
		{
		case TREATMENT_ENGINEERING:
		case TREATMENT_V_1_0:
		case TREATMENT_UPTAKE_DEV_1:
		case TREATMENT_V_1_1:
			return 50;
		case TREATMENT_EMC:
			return 10;
		case TREATMENT_SHAM_V_1_0:
		case TREATMENT_SPECIAL_1:
		case TREATMENT_SPECIAL_2:
		case TREATMENT_SPECIAL_3:
		case TREATMENT_SPECIAL_4:
			return 50;
		default:
			break;
		}

	return m_iOverTempBand;
}

/**
 * @brief	This method returns the maximum coil temperature before an error is raised in Celsius for the current treatment code
 *
 * @return	Value according to treatment code
 */
int CTreatmentInformation::getCoilTempUpper() const
{
	switch ( m_TreatmentCode )
		{
		case TREATMENT_ENGINEERING:
		case TREATMENT_V_1_0:
		case TREATMENT_UPTAKE_DEV_1:
		case TREATMENT_V_1_1:
			return 250;
		case TREATMENT_EMC:
			return 250;
		case TREATMENT_SHAM_V_1_0:
		case TREATMENT_SPECIAL_1:
		case TREATMENT_SPECIAL_2:
		case TREATMENT_SPECIAL_3:
		case TREATMENT_SPECIAL_4:
			return 250;
		default:
			break;
		}

	return m_CoilTempUpper;
}

/**
 * @brief	This method returns the  power level tolerance in VA (Volt Amperes) for determining an operational error for
 * 			the current treatment code
 *
 * @return	Value according to treatment code
 */
twobytes CTreatmentInformation::getRfPowerTol() const
{
	switch ( m_TreatmentCode )
		{
		case TREATMENT_ENGINEERING:
		case TREATMENT_V_1_0:
		case TREATMENT_UPTAKE_DEV_1:
		case TREATMENT_V_1_1:
			return 10;
		case TREATMENT_EMC:
		case TREATMENT_SHAM_V_1_0:
		case TREATMENT_SPECIAL_1:
		case TREATMENT_SPECIAL_2:
		case TREATMENT_SPECIAL_3:
		case TREATMENT_SPECIAL_4:
			return 10;
		default:
			break;
		}

	return m_RfPowerTol;
}

/**
 * @brief	This method returns the Custom15 for the current treatment code
 *
 * @return	Value according to treatment code
 */
int CTreatmentInformation::getCustom15() const
{
#ifdef	ENABLE_EASY_ENGINEERING_MODE
#define UNLOCK_RESPONSE (2)
#else
#define UNLOCK_RESPONSE (0)
#endif

	switch ( m_TreatmentCode )
		{
		case TREATMENT_ENGINEERING:
		case TREATMENT_V_1_0:
		case TREATMENT_UPTAKE_DEV_1:
		case TREATMENT_V_1_1:
			return UNLOCK_RESPONSE;
		case TREATMENT_EMC:
			return UNLOCK_RESPONSE;
		case TREATMENT_SHAM_V_1_0:
		case TREATMENT_SPECIAL_1:
		case TREATMENT_SPECIAL_2:
		case TREATMENT_SPECIAL_3:
		case TREATMENT_SPECIAL_4:
			return UNLOCK_RESPONSE;
		default:
			break;
		}

	return m_Custom15;
}

/**
 * @brief	This method returns the Custom16 for the current treatment code
 *
 * @return	Value according to treatment code
 */
int CTreatmentInformation::getCoilIdleTempSetpoint() const
{
	switch ( m_TreatmentCode )
		{
		case TREATMENT_ENGINEERING:
		case TREATMENT_V_1_0:
		case TREATMENT_UPTAKE_DEV_1:
		case TREATMENT_V_1_1:
			return 100;
		case TREATMENT_EMC:
			return 100;
		case TREATMENT_SHAM_V_1_0:
		case TREATMENT_SPECIAL_1:
		case TREATMENT_SPECIAL_2:
		case TREATMENT_SPECIAL_3:
		case TREATMENT_SPECIAL_4:
			return 100;
		default:
			break;
		}

	return m_Custom16;
}

/**
 * @brief	This method returns the Custom17  for the current treatment code
 *
 * @return	Value according to treatment code
 */
int CTreatmentInformation::getCustom17() const
{
#ifdef	ENABLE_EASY_ENGINEERING_MODE
#define ENTER_RESPONSE (2)
#else
#define ENTER_RESPONSE (0)
#endif

	switch ( m_TreatmentCode )
		{
		case TREATMENT_ENGINEERING:
		case TREATMENT_V_1_0:
		case TREATMENT_UPTAKE_DEV_1:
		case TREATMENT_V_1_1:
			return ENTER_RESPONSE;
		case TREATMENT_EMC:
		case TREATMENT_SHAM_V_1_0:
		case TREATMENT_SPECIAL_1:
		case TREATMENT_SPECIAL_2:
		case TREATMENT_SPECIAL_3:
		case TREATMENT_SPECIAL_4:
			return ENTER_RESPONSE;
		default:
			break;
		}

	return m_Custom17;
}

/**
 * @brief	This method returns the Custom18 for the current treatment code
 *
 * @return	Value according to treatment code
 */
int CTreatmentInformation::getCustom18() const
{
	switch ( m_TreatmentCode )
		{
		case TREATMENT_ENGINEERING:
		case TREATMENT_V_1_0:
		case TREATMENT_UPTAKE_DEV_1:
		case TREATMENT_V_1_1:
			return 0;
		case TREATMENT_EMC:
		case TREATMENT_SHAM_V_1_0:
		case TREATMENT_SPECIAL_1:
		case TREATMENT_SPECIAL_2:
		case TREATMENT_SPECIAL_3:
		case TREATMENT_SPECIAL_4:
			return 0;
		default:
			break;
		}

	return m_Custom18;
}

/**
 * @brief	This method returns the time to let coil vaporize water after pumping stops in millisecond for the current treatment code
 *
 * @return	Value according to treatment code
 */
byte CTreatmentInformation::getCustom22() const
{
	switch ( m_TreatmentCode )
		{
		case TREATMENT_ENGINEERING:
		case TREATMENT_V_1_0:
		case TREATMENT_UPTAKE_DEV_1:
		case TREATMENT_V_1_1:
			return 0;
		case TREATMENT_EMC:
		case TREATMENT_SHAM_V_1_0:
		case TREATMENT_SPECIAL_1:
		case TREATMENT_SPECIAL_2:
		case TREATMENT_SPECIAL_3:
		case TREATMENT_SPECIAL_4:
			return 0;
		default:
			break;
		}

	return m_bCustom22;
}

/**
 * @brief	This method returns the time to wait between treatments in tenths of a second
 *
 * @return	Value according to treatment code
 */
twobytes CTreatmentInformation::getTrest() const
{
	switch ( m_TreatmentCode )
		{
		case TREATMENT_V_1_0:
		case TREATMENT_ENGINEERING:
		case TREATMENT_UPTAKE_DEV_1:
		case TREATMENT_V_1_1:
#ifdef	ENABLE_EASY_ENGINEERING_MODE
			return 50;
#else
			return 1200;
#endif
		case TREATMENT_EMC:
			return 100;
		case TREATMENT_SHAM_V_1_0:
		case TREATMENT_SPECIAL_1:
		case TREATMENT_SPECIAL_2:
		case TREATMENT_SPECIAL_3:
		case TREATMENT_SPECIAL_4:
			return 1200;
		default:
			break;
		}

	return m_Trest;
}

/**
 * @brief	This method returns the cross-section area of the syringe in square millimeters
 *
 * @return	Value according to treatment code
 */
twobytes CTreatmentInformation::getAsyringe() const
{

	switch ( m_TreatmentCode )
		{
		case TREATMENT_ENGINEERING:
		case TREATMENT_V_1_0:
		case TREATMENT_UPTAKE_DEV_1:
		case TREATMENT_V_1_1:
			return 197; // Measured area of 5/8"
		case TREATMENT_EMC:
		case TREATMENT_SHAM_V_1_0:
		case TREATMENT_SPECIAL_1:
		case TREATMENT_SPECIAL_2:
		case TREATMENT_SPECIAL_3:
		case TREATMENT_SPECIAL_4:
			// 3/4" size
			// return 285;

			// 5/8'" size
			return 197;
		default:
			break;
		}

	return m_Asyringe;
}

/**
 * @brief	This method returns the debouncing time that the trigger needs to be off in millisecond for the current treatment code
 *
 * @return	Value according to treatment code
 */
byte CTreatmentInformation::getTdeb() const
{
	switch ( m_TreatmentCode )
		{
		case TREATMENT_ENGINEERING:
		case TREATMENT_V_1_0:
		case TREATMENT_UPTAKE_DEV_1:
		case TREATMENT_V_1_1:
			return 250;
		case TREATMENT_EMC:
		case TREATMENT_SHAM_V_1_0:
		case TREATMENT_SPECIAL_1:
		case TREATMENT_SPECIAL_2:
		case TREATMENT_SPECIAL_3:
		case TREATMENT_SPECIAL_4:
			return 250;
		default:
			break;
		}

	return m_bTdeb;
}

/**
 * @brief	This method returns the maximum allowed temperature for the outlet thermocouple in Celsius for the current treatment code
 *
 * @return	Value according to treatment code
 */
int CTreatmentInformation::getOutletTempMax() const
{
	switch ( m_TreatmentCode )
		{
		case TREATMENT_ENGINEERING:
		case TREATMENT_V_1_0:
		case TREATMENT_UPTAKE_DEV_1:
		case TREATMENT_V_1_1:
			return 150;
		case TREATMENT_EMC:
			return 150;
		case TREATMENT_SHAM_V_1_0:
		case TREATMENT_SPECIAL_1:
		case TREATMENT_SPECIAL_2:
		case TREATMENT_SPECIAL_3:
		case TREATMENT_SPECIAL_4:
			return 150;
		default:
			break;
		}

	return m_OutletTempMax;
}

/**
 * @brief	This method returns the minimum allowed temperature for the outlet thermocouple in
 * 			Celsius for the current treatment code
 *
 * @return	Value according to treatment code
 */
int CTreatmentInformation::getOutletTempMin() const
{
	switch ( m_TreatmentCode )
		{
		case TREATMENT_ENGINEERING:
		case TREATMENT_V_1_0:
		case TREATMENT_UPTAKE_DEV_1:
		case TREATMENT_V_1_1:
			return 30;
		case TREATMENT_EMC:
			return 0;
		case TREATMENT_SHAM_V_1_0:
		case TREATMENT_SPECIAL_1:
		case TREATMENT_SPECIAL_2:
		case TREATMENT_SPECIAL_3:
		case TREATMENT_SPECIAL_4:
			return 30;
		default:
			break;
		}

	return m_OutletTempMin;
}

/**
 * @brief	This method returns the number of full deliveries that may be done in counts for the current treatment code
 *
 * @return	Value according to treatment code
 */
twobytes CTreatmentInformation::getDeliveryCount() const
{
	switch ( m_TreatmentCode )
		{
		case TREATMENT_V_1_0:
			return 15;
		case TREATMENT_EMC:
		case TREATMENT_ENGINEERING:
		case TREATMENT_UPTAKE_DEV_1:
		case TREATMENT_V_1_1:
			return 0xFFFF;
		case TREATMENT_SHAM_V_1_0:
		case TREATMENT_SPECIAL_1:
		case TREATMENT_SPECIAL_2:
		case TREATMENT_SPECIAL_3:
		case TREATMENT_SPECIAL_4:
			return 15;
		default:
			break;
		}

	return m_tbDeliveryCount;
}

/**
 * @brief	This method returns Custom6 for the current treatment code
 *
 * @return	Value according to treatment code
 */
byte CTreatmentInformation::getCustom6() const
{
	switch ( m_TreatmentCode )
		{
		case TREATMENT_ENGINEERING:
		case TREATMENT_V_1_0:
		case TREATMENT_UPTAKE_DEV_1:
		case TREATMENT_V_1_1:
			return 0;
		case TREATMENT_EMC:
			return 0;
		case TREATMENT_SHAM_V_1_0:
		case TREATMENT_SPECIAL_1:
		case TREATMENT_SPECIAL_2:
		case TREATMENT_SPECIAL_3:
		case TREATMENT_SPECIAL_4:
			return 0;
		default:
			break;
		}

	return m_bCustom6;
}

/**
 * @brief	This method returns maximum time in seconds allowed for vapor ON.
 * 			This is used as a limit for the viability of the handpiece.
 *
 * @return	Time limit is seconds
 */
twobytes CTreatmentInformation::getTvapor() const
{
	switch ( m_TreatmentCode )
		{
		case TREATMENT_V_1_0:
			return 300;
		case TREATMENT_EMC:
			return 1000;
		case TREATMENT_ENGINEERING:
		case TREATMENT_UPTAKE_DEV_1:
		case TREATMENT_V_1_1:
		case TREATMENT_SHAM_V_1_0:
		case TREATMENT_SPECIAL_1:
		case TREATMENT_SPECIAL_2:
		case TREATMENT_SPECIAL_3:
		case TREATMENT_SPECIAL_4:
			return 65535;
		default:
			break;
		}

	return m_Tvapor;
}

/**
 * @brief	This method returns the time for the next vapor delivery in tenths of a seocnd
 *
 * @return	Value according to treatment code
 */
twobytes CTreatmentInformation::getTdelivery() const
{
	switch ( m_TreatmentCode )
		{
		case TREATMENT_V_1_0:
		case TREATMENT_ENGINEERING:
		case TREATMENT_EMC:
		case TREATMENT_UPTAKE_DEV_1:
		case TREATMENT_V_1_1:
			return 100;
		case TREATMENT_SHAM_V_1_0:
		case TREATMENT_SPECIAL_1:
		case TREATMENT_SPECIAL_2:
		case TREATMENT_SPECIAL_3:
		case TREATMENT_SPECIAL_4:
			return 100;
		default:
			break;
		}

	return m_Tdelivery;
}

/**
 * @brief	This method returns the DeliveryMin value in tenths of a second
 *
 * @return	Time in tenths of a second according to treatment code
 */
twobytes CTreatmentInformation::getDeliveryMin() const
{
	switch ( m_TreatmentCode )
		{
		case TREATMENT_ENGINEERING:
		case TREATMENT_V_1_0:
		case TREATMENT_UPTAKE_DEV_1:
		case TREATMENT_V_1_1:
			return 30;
		case TREATMENT_EMC:
		case TREATMENT_SHAM_V_1_0:
		case TREATMENT_SPECIAL_1:
		case TREATMENT_SPECIAL_2:
		case TREATMENT_SPECIAL_3:
		case TREATMENT_SPECIAL_4:
			return 30;
		default:
			break;
		}

	return m_DeliveryMin;
}

/**
 * @brief	This method returns the maximum time selection allowed for the treatment code in tenths of a second
 *
 * @return	Time in tenths of a second according to treatment code
 */
twobytes CTreatmentInformation::getDeliveryMax() const
{
	switch ( m_TreatmentCode )
		{
		case TREATMENT_ENGINEERING:
		case TREATMENT_V_1_0:
		case TREATMENT_UPTAKE_DEV_1:
		case TREATMENT_V_1_1:
			return 100;
		case TREATMENT_EMC:
			return 100;
		case TREATMENT_SHAM_V_1_0:
		case TREATMENT_SPECIAL_1:
		case TREATMENT_SPECIAL_2:
		case TREATMENT_SPECIAL_3:
		case TREATMENT_SPECIAL_4:
			return 100;
		default:
			break;
		}

	return m_DeliveryMax;
}

/**
 * @brief	This method returns the Custom20 for the current treatment code
 *
 * @return	Value according to treatment code
 */
int CTreatmentInformation::getWrateDelivery() const
{
	switch ( m_TreatmentCode )
		{
		case TREATMENT_ENGINEERING:
		case TREATMENT_V_1_0:
		case TREATMENT_UPTAKE_DEV_1:
		case TREATMENT_V_1_1:
			return 7000;
		case TREATMENT_EMC:
			return 7000;
		case TREATMENT_SHAM_V_1_0:
		case TREATMENT_SPECIAL_1:
		case TREATMENT_SPECIAL_2:
		case TREATMENT_SPECIAL_3:
		case TREATMENT_SPECIAL_4:
			return 7000;
		default:
			break;
		}

	return m_WrateDelivery;
}

/**
 * @brief	This method returns the Custom19 for the current treatment code
 *
 * @return	Value according to treatment code
 */
int CTreatmentInformation::getCustom19() const
{
	switch ( m_TreatmentCode )
		{
		case TREATMENT_ENGINEERING:
		case TREATMENT_V_1_0:
		case TREATMENT_UPTAKE_DEV_1:
		case TREATMENT_V_1_1:
			return 0;
		case TREATMENT_EMC:
			return 0;
		case TREATMENT_SHAM_V_1_0:
		case TREATMENT_SPECIAL_1:
		case TREATMENT_SPECIAL_2:
		case TREATMENT_SPECIAL_3:
		case TREATMENT_SPECIAL_4:
			return 0;
		default:
			break;
		}

	return m_Custom19;
}

/**
 * @brief	This method returns the Custom7
 *
 * @return	Value according to treatment code
 */
twobytes CTreatmentInformation::getPowerDelivery() const
{
	switch ( m_TreatmentCode )
		{
		case TREATMENT_ENGINEERING:
		case TREATMENT_V_1_0:
		case TREATMENT_UPTAKE_DEV_1:
		case TREATMENT_V_1_1:
			return 350;
		case TREATMENT_EMC:
			return 350;
		case TREATMENT_SHAM_V_1_0:
		case TREATMENT_SPECIAL_1:
		case TREATMENT_SPECIAL_2:
		case TREATMENT_SPECIAL_3:
		case TREATMENT_SPECIAL_4:
			return 350;
		default:
			break;
		}

	return m_tbPowerDelivery;
}

/**
 * @brief	This method returns the temperature tolerance added to the upper limit to
 * 			give a critical fault temperature for the coil
 *
 * @return	Value according to treatment code
 */
int CTreatmentInformation::getCoilTempShutdownTol() const
{
	switch ( m_TreatmentCode )
		{
		case TREATMENT_ENGINEERING:
		case TREATMENT_V_1_0:
		case TREATMENT_UPTAKE_DEV_1:
		case TREATMENT_V_1_1:
			return 150;
		case TREATMENT_EMC:
			return 150;
		case TREATMENT_SHAM_V_1_0:
		case TREATMENT_SPECIAL_1:
		case TREATMENT_SPECIAL_2:
		case TREATMENT_SPECIAL_3:
		case TREATMENT_SPECIAL_4:
			return 150;
		default:
			break;
		}

	return m_CoilTempShutdownTol;
}

/**
 * @brief	This method returns the temperature tolerance added to the upper limit to
 * 			give a critical fault temperature for the outlet
 *
 * @return	Value according to treatment code
 */
int CTreatmentInformation::getOutletTempShutdownTol() const
{
	switch ( m_TreatmentCode )
		{
		case TREATMENT_ENGINEERING:
		case TREATMENT_V_1_0:
		case TREATMENT_UPTAKE_DEV_1:
		case TREATMENT_V_1_1:
			return 150;
		case TREATMENT_EMC:
			return 180;
		case TREATMENT_SHAM_V_1_0:
		case TREATMENT_SPECIAL_1:
		case TREATMENT_SPECIAL_2:
		case TREATMENT_SPECIAL_3:
		case TREATMENT_SPECIAL_4:
			return 150;
		default:
			break;
		}

	return m_OutletTempShutdownTol;
}

/**
 * @brief	This method returns the value for the elapsed time to wait before
 * 			comparing the minimum temperature limits in milliseconds
 *
 * @return	Value according to treatment code
 */
twobytes CTreatmentInformation::getTminValueTest() const
{
	switch ( m_TreatmentCode )
		{
		case TREATMENT_ENGINEERING:
		case TREATMENT_V_1_0:
		case TREATMENT_UPTAKE_DEV_1:
		case TREATMENT_V_1_1:
			return 5000;
		case TREATMENT_EMC:
			return 10000;
		case TREATMENT_SHAM_V_1_0:
		case TREATMENT_SPECIAL_1:
		case TREATMENT_SPECIAL_2:
		case TREATMENT_SPECIAL_3:
		case TREATMENT_SPECIAL_4:
			return 5000;
		default:
			break;
		}

	return m_uiTminValueTest;
}

/**
 * @brief	This method returns the value for the elapsed time to wait before
 * 			comparing the minimum temperature limits in milliseconds
 *
 * @return	Value according to treatment code
 */
twobytes CTreatmentInformation::getTminPrimeValueTest() const
{
	switch ( m_TreatmentCode )
		{
		case TREATMENT_ENGINEERING:
		case TREATMENT_V_1_0:
		case TREATMENT_UPTAKE_DEV_1:
		case TREATMENT_V_1_1:
			return 6000;
		case TREATMENT_EMC:
			return 10000;
		case TREATMENT_SHAM_V_1_0:
		case TREATMENT_SPECIAL_1:
		case TREATMENT_SPECIAL_2:
		case TREATMENT_SPECIAL_3:
		case TREATMENT_SPECIAL_4:
			return 6000;
		default:
			break;
		}

	return m_uiTminPrimeValueTest;
}

/**
 * @brief	This method returns the count of EMC testing cycles
 *
 * @return	Value according to treatment code
 */
twobytes CTreatmentInformation::getEmcCycles() const
{

	switch ( m_TreatmentCode )
		{
		case TREATMENT_ENGINEERING:
		case TREATMENT_V_1_0:
		case TREATMENT_UPTAKE_DEV_1:
		case TREATMENT_V_1_1:
			return 1;
		case TREATMENT_EMC:
			return 65535;
		case TREATMENT_SHAM_V_1_0:
		case TREATMENT_SPECIAL_1:
		case TREATMENT_SPECIAL_2:
		case TREATMENT_SPECIAL_3:
		case TREATMENT_SPECIAL_4:
			return 1;
		default:
			break;
		}

	return m_uiEmcCycles;
}

/**
 * @brief	This method returns the time in milliseconds for temps to be overlimit
 * 			before an error to be raised.
 *
 * @return	Value according to treatment code
 */
twobytes CTreatmentInformation::getTtempWindow() const
{
	switch ( m_TreatmentCode )
		{
		case TREATMENT_ENGINEERING:
		case TREATMENT_V_1_0:
		case TREATMENT_UPTAKE_DEV_1:
		case TREATMENT_V_1_1:
			return 500;
		case TREATMENT_EMC:
		case TREATMENT_SHAM_V_1_0:
		case TREATMENT_SPECIAL_1:
		case TREATMENT_SPECIAL_2:
		case TREATMENT_SPECIAL_3:
		case TREATMENT_SPECIAL_4:
			return 500;
		default:
			break;
		}

	return m_uiTtempWindow;
}

/**
 * @brief	This method returns the Custom21
 *
 * @return	Value according to treatment code
 */
twobytes CTreatmentInformation::getCooldownTime() const
{
	switch ( m_TreatmentCode )
		{
		case TREATMENT_ENGINEERING:
		case TREATMENT_V_1_0:
		case TREATMENT_UPTAKE_DEV_1:
		case TREATMENT_V_1_1:
			return 300;
		case TREATMENT_EMC:
		case TREATMENT_SHAM_V_1_0:
		case TREATMENT_SPECIAL_1:
		case TREATMENT_SPECIAL_2:
		case TREATMENT_SPECIAL_3:
		case TREATMENT_SPECIAL_4:
			return 300;
		default:
			break;
		}

	return m_uiCooldowntime;
}

/**
 * @brief	This method returns the upper temperature limit used for coil idling
 *
 * @return	Value according to treatment code
 */
int CTreatmentInformation::getTempUpperLimitIdle() const
{
	switch ( m_TreatmentCode )
		{
		case TREATMENT_ENGINEERING:
		case TREATMENT_V_1_0:
		case TREATMENT_UPTAKE_DEV_1:
		case TREATMENT_V_1_1:
			return 225;
		case TREATMENT_EMC:
			return 225;
		case TREATMENT_SHAM_V_1_0:
		case TREATMENT_SPECIAL_1:
		case TREATMENT_SPECIAL_2:
		case TREATMENT_SPECIAL_3:
		case TREATMENT_SPECIAL_4:
			return 225;
		default:
			break;
		}

	return m_iTempUpperLimitIdle;
}

/**
 * @brief	This method returns the lower temperature limit used for coil idling
 *
 * @return	Value according to treatment code
 */
int CTreatmentInformation::getTempLowerLimitIdle() const
{
	switch ( m_TreatmentCode )
		{
		case TREATMENT_ENGINEERING:
		case TREATMENT_V_1_0:
		case TREATMENT_UPTAKE_DEV_1:
		case TREATMENT_V_1_1:
			return 35;
		case TREATMENT_EMC:
			return 35;
		case TREATMENT_SHAM_V_1_0:
		case TREATMENT_SPECIAL_1:
		case TREATMENT_SPECIAL_2:
		case TREATMENT_SPECIAL_3:
		case TREATMENT_SPECIAL_4:
			return 35;
		default:
			break;
		}

	return m_iTempLowerLimitIdle;
}

/**
 * @brief	This method returns the Custom8. Now used for priming confirmation
 * 			TODO Stop use for confirmation of priming 2=> Primed. 1 => Fail
 *
 * @return	Value according to treatment code
 */
int CTreatmentInformation::getCustom8() const
{
#ifdef	ENABLE_EASY_ENGINEERING_MODE
#define PRIMED_RESPONSE (2)
#else
#define PRIMED_RESPONSE (0)
#endif

	switch ( m_TreatmentCode )
		{
		case TREATMENT_ENGINEERING:
		case TREATMENT_V_1_0:
		case TREATMENT_UPTAKE_DEV_1:
		case TREATMENT_V_1_1:
			return PRIMED_RESPONSE;
		case TREATMENT_EMC:
			return PRIMED_RESPONSE;
		case TREATMENT_SHAM_V_1_0:
		case TREATMENT_SPECIAL_1:
		case TREATMENT_SPECIAL_2:
		case TREATMENT_SPECIAL_3:
		case TREATMENT_SPECIAL_4:
			return PRIMED_RESPONSE;
		default:
			break;
		}

	return m_stbCustom8;
}

/**
 * @brief	This method returns the rate in microliters per minute for the inter-treatment "idle" water flow rate
 *
 * @return	Value according to treatment code
 */
int CTreatmentInformation::getWrateIdle() const
{
	switch ( m_TreatmentCode )
		{
		case TREATMENT_ENGINEERING:
		case TREATMENT_V_1_0:
		case TREATMENT_UPTAKE_DEV_1:
		case TREATMENT_V_1_1:
			return 80;
		case TREATMENT_EMC:
		case TREATMENT_SHAM_V_1_0:
		case TREATMENT_SPECIAL_1:
		case TREATMENT_SPECIAL_2:
		case TREATMENT_SPECIAL_3:
		case TREATMENT_SPECIAL_4:
			return 80;
		default:
			break;
		}

	return m_WrateIdle;
}

/**
 * @brief	This method returns the power in VA (Volt Amperes) for the inter-treatment "idle" power level
 *
 * @return	Value according to treatment code
 */
twobytes CTreatmentInformation::getPowerIdle() const
{
	switch ( m_TreatmentCode )
		{
		case TREATMENT_ENGINEERING:
		case TREATMENT_V_1_0:
		case TREATMENT_UPTAKE_DEV_1:
		case TREATMENT_V_1_1:
			return 10;
		case TREATMENT_EMC:
		case TREATMENT_SHAM_V_1_0:
		case TREATMENT_SPECIAL_1:
		case TREATMENT_SPECIAL_2:
		case TREATMENT_SPECIAL_3:
		case TREATMENT_SPECIAL_4:
			return 10;
		default:
			break;
		}

	return m_tbPowerIdle;
}

/**
 * @brief	Gives initial flow rate for priming in mL/min
 *
 * @return	Value according to treatment code
 */
int CTreatmentInformation::getWratePrime1() const
{
	switch ( m_TreatmentCode )
		{
		case TREATMENT_ENGINEERING:
		case TREATMENT_V_1_0:
		case TREATMENT_UPTAKE_DEV_1:
		case TREATMENT_V_1_1:
			return 70;
		case TREATMENT_EMC:
		case TREATMENT_SHAM_V_1_0:
		case TREATMENT_SPECIAL_1:
		case TREATMENT_SPECIAL_2:
		case TREATMENT_SPECIAL_3:
		case TREATMENT_SPECIAL_4:
			return 70;
		default:
			break;
		}

	return m_iWratePrime1;
}

/**
 * @brief	Gives second flow rate for priming in mL/min
 *
 * @return	Value according to treatment code
 */
int CTreatmentInformation::getWratePrime2() const
{
	switch ( m_TreatmentCode )
		{
		case TREATMENT_ENGINEERING:
		case TREATMENT_V_1_0:
		case TREATMENT_UPTAKE_DEV_1:
		case TREATMENT_V_1_1:
			return 25;
		case TREATMENT_EMC:
		case TREATMENT_SHAM_V_1_0:
		case TREATMENT_SPECIAL_1:
		case TREATMENT_SPECIAL_2:
		case TREATMENT_SPECIAL_3:
		case TREATMENT_SPECIAL_4:
			return 25;
		default:
			break;
		}

	return m_iWratePrime2;
}

/**
 * @brief	This method returns the flow rate for the final stage in water priming in milliliters/min  for the current treatment code
 *
 * @return	Value according to treatment code
 */
int CTreatmentInformation::getWratePrime3() const
{
	switch ( m_TreatmentCode )
		{
		case TREATMENT_ENGINEERING:
		case TREATMENT_V_1_0:
		case TREATMENT_UPTAKE_DEV_1:
		case TREATMENT_V_1_1:
			return 30;
		case TREATMENT_EMC:
		case TREATMENT_SHAM_V_1_0:
		case TREATMENT_SPECIAL_1:
		case TREATMENT_SPECIAL_2:
		case TREATMENT_SPECIAL_3:
		case TREATMENT_SPECIAL_4:
			return 30;
		default:
			break;
		}

	return m_iWratePrime3;
}

/**
 * @brief	Gives initial volume to pump for priming in uL
 *
 * @return	Value according to treatment code
 */
twobytes CTreatmentInformation::getVprime1() const
{
	switch ( m_TreatmentCode )
		{
		case TREATMENT_ENGINEERING:
		case TREATMENT_V_1_0:
		case TREATMENT_UPTAKE_DEV_1:
		case TREATMENT_V_1_1:
			return 6000;
		case TREATMENT_EMC:
		case TREATMENT_SHAM_V_1_0:
		case TREATMENT_SPECIAL_1:
		case TREATMENT_SPECIAL_2:
		case TREATMENT_SPECIAL_3:
		case TREATMENT_SPECIAL_4:
			return 6000;
		default:
			break;
		}

	return m_uiVprime1;
}

/**
 * @brief	Gives second volume point to pump to for priming in uL.
 *			TODO fix discussion
 * 			This value needs to be based on having a the Tvapor time, plus 2 seconds, at Wrate of flow available
 * 			from the syringe. The length between the limit switches in 4.85 +/- 0.125 inches for each so 4.35 inches
 * 			back-calculating give 11.3 ML for priming, so with 202 seconds at 3.5ml, 11 mL will give additional tolerance.
 *
 * @return	Value according to treatment code
 */
twobytes CTreatmentInformation::getVprime2() const
{
	switch ( m_TreatmentCode )
		{
		case TREATMENT_ENGINEERING:
		case TREATMENT_V_1_0:
		case TREATMENT_UPTAKE_DEV_1:
		case TREATMENT_V_1_1:
			return 9000;
		case TREATMENT_EMC:
		case TREATMENT_SHAM_V_1_0:
		case TREATMENT_SPECIAL_1:
		case TREATMENT_SPECIAL_2:
		case TREATMENT_SPECIAL_3:
		case TREATMENT_SPECIAL_4:
			return 9000;
		default:
			break;
		}

	return m_uiVprime2;
}

/**
 * @brief	This method returns the time for the generator to be ready to be triggered
 * 			after being unlocked and pending time has expired. The period is in tenths of a second.
 *
 * @return	Value according to treatment code in tenths of a second
 */
twobytes CTreatmentInformation::getTprecond() const
{
	switch ( m_TreatmentCode )
		{
		case TREATMENT_ENGINEERING:
		case TREATMENT_V_1_0:
		case TREATMENT_UPTAKE_DEV_1:
		case TREATMENT_V_1_1:
			return 200;
		case TREATMENT_EMC:
		case TREATMENT_SHAM_V_1_0:
		case TREATMENT_SPECIAL_1:
		case TREATMENT_SPECIAL_2:
		case TREATMENT_SPECIAL_3:
		case TREATMENT_SPECIAL_4:
			return 200;
		default:
			break;
		}

	return m_tbTprecond;
}

/**
 * @brief	This method sets the internal temperature limit where an error is raised for a high internal temp.
 *
 * 			The value is used with an unknown or TREATMENT_UNSET code
 *
 * @param	iMaxInternalTemp in Celsius units
 *
 * @return	ERR_OK if parameter was accepted
 */
upErr CTreatmentInformation::setMaxInternalTemp( int iMaxInternalTemp )
{
	m_iMaxInternalTemp = iMaxInternalTemp;
	return ERR_OK;
}

/**
 * @brief	This method sets the minimum coil temperature before an error is raised for use with an unknown or TREATMENT_UNSET code
 *
 * @param	iCoilTempLower in Celsius units
 *
 * @return	ERR_OK if parameter was accepted
 */
upErr CTreatmentInformation::setCoilTempLower( int iCoilTempLower )
{
	m_CoilTempLower = iCoilTempLower;
	return ERR_OK;
}

/**
 * @brief	This method sets the band for having coil and outlet temperatures cool before starting a treatment
 *
 * @param	iOverTempBand in Celsius units
 *
 * @return	ERR_OK if parameter was accepted
 */
upErr CTreatmentInformation::setOverTempBand( int iOverTempBand )
{
	m_iOverTempBand = iOverTempBand;
	return ERR_OK;
}

/**
 * @brief	This method sets the maximum coil temperature before an error is raised for use with an unknown or TREATMENT_UNSET code
 *
 * @param	iCoilTempUpper in Celsius units
 *
 * @return	ERR_OK if parameter was accepted
 */
upErr CTreatmentInformation::setCoilTempUpper( int iCoilTempUpper )
{
	m_CoilTempUpper = iCoilTempUpper;
	return ERR_OK;
}

/**
 * @brief	This method sets the power level tolerance in VA (Volt Amperes) for determining an operational error
 * 			for use with an unknown or TREATMENT_UNSET code
 *
 * @param	tbRfPowerTol in VA (Volt Amperes) units
 *
 * @return	ERR_OK if parameter was accepted
 */
upErr CTreatmentInformation::setRfPowerTol( twobytes tbRfPowerTol )
{
	m_RfPowerTol = tbRfPowerTol;
	return ERR_OK;
}

/**
 * @brief	This method sets Custom15 for use with an unknown or TREATMENT_UNSET code
 *
 * @param	iCustom15 in  units
 *
 * @return	ERR_OK if parameter was accepted
 */
upErr CTreatmentInformation::setCustom15( int iCustom15 )
{
	m_Custom15 = iCustom15;
	return ERR_OK;
}

/**
 * @brief	This method sets Custom16 for use with an unknown or TREATMENT_UNSET code
 *
 * @param	iCustom16 in units
 *
 * @return	ERR_OK if parameter was accepted
 */
upErr CTreatmentInformation::setCoilIdleTempSetpoint( int iCustom16 )
{
	m_Custom16 = iCustom16;
	return ERR_OK;
}

/**
 * @brief	This method sets the Custom17 for use with an unknown or TREATMENT_UNSET code
 *
 * @param	iCustom17 in units
 *
 * @return	ERR_OK if parameter was accepted
 */
upErr CTreatmentInformation::setCustom17( int iCustom17 )
{
	m_Custom17 = iCustom17;
	return ERR_OK;
}

/**
 * @brief	This method sets Custom18 for use with an unknown or TREATMENT_UNSET code
 *
 * @param	iCustom18 in units
 *
 * @return	ERR_OK if parameter was accepted
 */
upErr CTreatmentInformation::setCustom18( int iCustom18 )
{
	m_Custom18 = iCustom18;
	return ERR_OK;
}

/**
 * @brief	This method sets Custom22 value for use
 * 			with an unknown or TREATMENT_UNSET code
 *
 * @param	bCustom22 in unknown units
 *
 * @return	ERR_OK if parameter was accepted
 */
upErr CTreatmentInformation::setCustom22( byte bCustom22 )
{
	m_bCustom22 = bCustom22;
	return ERR_OK;
}

/**
 * @brief	This method sets the time to let wait between treatments
 *
 * @param	tbTrest in millisecond units
 *
 * @return	ERR_OK if parameter was accepted
 */
upErr CTreatmentInformation::setTrest( twobytes tbTrest )
{
	m_Trest = tbTrest;
	return ERR_OK;
}

/**
 * @brief	This method sets the area of the syringe in square millimeters
 *
 * @param	tbAsyringe Syringe area in mm^2 as twobytes
 *
 * @return   Status as upErr
 */
upErr CTreatmentInformation::setAsyringe( twobytes tbAsyringe )
{
	m_Asyringe = tbAsyringe;
	return ERR_OK;
}

/**
 * @brief	This method sets the debouncing time that the trigger needs to be off for use with an unknown or TREATMENT_UNSET code
 *
 * @param	bTdeb in millisecond units
 *
 * @return	ERR_OK if parameter was accepted
 */
upErr CTreatmentInformation::setTdeb( byte bTdeb )
{
	m_bTdeb = bTdeb;
	return ERR_OK;
}

/**
 * @brief	This method sets the maximum allowed temperature for the outlet thermocouple for use with an unknown or TREATMENT_UNSET code
 *
 * @param	iOutletTempMax in Celsius units
 *
 * @return	ERR_OK if parameter was accepted
 */
upErr CTreatmentInformation::setOutletTempMax( int iOutletTempMax )
{
	m_OutletTempMax = iOutletTempMax;
	return ERR_OK;
}

/**
 * @brief	This method sets the minimum allowed temperature for the outlet thermocouple for use with an unknown or TREATMENT_UNSET code
 *
 * @param	iOutletTempMin in Celsius units
 *
 * @return	ERR_OK if parameter was accepted
 */
upErr CTreatmentInformation::setOutletTempMin( int iOutletTempMin )
{
	m_OutletTempMin = iOutletTempMin;
	return ERR_OK;
}

/**
 * @brief	This method sets the number of full treatments that may be done for use with an unknown or TREATMENT_UNSET code
 *
 * @param	tbDeliveryCount in counts
 *
 * @return	ERR_OK if parameter was accepted
 */
upErr CTreatmentInformation::setDeliveryCount( twobytes tbDeliveryCount )
{
	m_tbDeliveryCount = tbDeliveryCount;
	return ERR_OK;
}

/**
 * @brief	This method sets the Custom6 with an unknown or TREATMENT_UNSET code
 *
 * @param	bCustom6 in  units
 *
 * @return	ERR_OK if parameter was accepted
 */
upErr CTreatmentInformation::setCustom6( byte bCustom6 )
{
	m_bCustom6 = bCustom6;
	return ERR_OK;
}

/**
 * @brief	This method sets the maximum allowed vapor ON time for a handpiece.
 *
 * @param	tbTvapor in seconds
 *
 * @return	ERR_OK if parameter was accepted
 */
upErr CTreatmentInformation::setTvapor( twobytes tbTvapor )
{
	m_Tvapor = tbTvapor;
	return ERR_OK;
}

/**
 * @brief	This method sets the delivery time for an unknown or TREATMENT_UNSET code
 *
 * @param	tbTdelivery Time selection in tenths of a second
 *
 * @return	ERR_OK if parameter was accepted
 */
upErr CTreatmentInformation::setTdelivery( twobytes tbTdelivery )
{
	m_Tdelivery = tbTdelivery;
	return ERR_OK;
}

/**
 * @brief	This method sets the minimum allowed time selection for a delivery.
 *
 * @param	tbDeliveryMin in tenths of a second
 *
 * @return	ERR_OK if parameter was accepted
 */
upErr CTreatmentInformation::setDeliveryMin( twobytes tbDeliveryMin )
{
	m_DeliveryMin = tbDeliveryMin;
	return ERR_OK;
}

/**
 * @brief	This method sets the Water rate for delivery for use with an unknown or TREATMENT_UNSET code
 *
 * @param	iWrateDelivery uL/min in units
 *
 * @return	ERR_OK if parameter was accepted
 */
upErr CTreatmentInformation::setWrateDelivery( int iWrateDelivery )
{
	m_WrateDelivery = iWrateDelivery;
	return ERR_OK;
}

/**
 * @brief	This method sets the Custom19 for use with an unknown or TREATMENT_UNSET code
 *
 * @param	iCustom19 in units
 *
 * @return	ERR_OK if parameter was accepted
 */
upErr CTreatmentInformation::setCustom19( int iCustom19 )
{
	m_Custom19 = iCustom19;
	return ERR_OK;
}

/**
 * @brief	This method sets the minimum delivery selection time.
 *
 * @param	tbDeliveryMax in tenths of a second
 *
 * @return	ERR_OK if parameter was accepted
 */
upErr CTreatmentInformation::setDeliveryMax( twobytes tbDeliveryMax )
{
	m_DeliveryMax = tbDeliveryMax;
	return ERR_OK;
}

/**
 * @brief	This method sets the flow rate for initial priming for use with an unknown or TREATMENT_UNSET code
 *
 * @param	iWratePrime3 in milliliters/min units
 *
 * @return	ERR_OK if parameter was accepted
 */
upErr CTreatmentInformation::setWratePrime3( int iWratePrime3 )
{
	m_iWratePrime3 = iWratePrime3;
	return ERR_OK;
}

/**
 * @brief	This method returns the current delivery device model code
 *
 * @return	The current treatment code
 */
upTreatmentCode CTreatmentInformation::getTreatmentCode() const
{
	return m_TreatmentCode;
}

/**
 * @brief	This method sets the treatment code that will select the other parameters
 *
 * @param	eTherapyCode Code as upTreatmentCode
 *
 * @return	ERR_OK if parameter was accepted
 */
upErr CTreatmentInformation::setTreatmentCode( upTreatmentCode eTherapyCode )
{
	upErr eErr = checkTreatmentCode( eTherapyCode );
	if ( ERR_OK == eErr )
		m_TreatmentCode = eTherapyCode;
	return eErr;
}

/**
 * @brief	This method sets Custom7
 *
 * @param	tbCustom7 New value as twobytes
 *
 * @return   Status as upErr
 */
upErr CTreatmentInformation::setPowerDelivery( twobytes tbCustom7 )
{
	m_tbPowerDelivery = tbCustom7;
	return ERR_OK;
}

/**
 * @brief	This method sets the coil critical shutdown tolerance value m_CoilTempShutdownTol
 *
 * @param	iCoilTempShutdownTol New value as int
 *
 * @return   Status as upErr
 */
upErr CTreatmentInformation::setCoilTempShutdownTol( int iCoilTempShutdownTol )
{
	m_CoilTempShutdownTol = iCoilTempShutdownTol;
	return ERR_OK;
}

/**
 * @brief	This method sets the spare value m_OutletTempShutdownTol
 *
 * @param	iOutletTempShutdownTol New value as int
 *
 * @return   Status as upErr
 */
upErr CTreatmentInformation::setOutletTempShutdownTol( int iOutletTempShutdownTol )
{
	m_OutletTempShutdownTol = iOutletTempShutdownTol;
	return ERR_OK;
}

/**
 * @brief	This method sets time to test for minimum values: m_uiTminValueTest
 *
 * @param	uiTminValueTest New value as twobytes
 *
 * @return   Status as upErr
 */
upErr CTreatmentInformation::setTminValueTest( twobytes uiTminValueTest )
{
	m_uiTminValueTest = uiTminValueTest;
	return ERR_OK;
}

/**
 * @brief	This method sets time in priming to test for minimum values: m_uiTminValueTest
 *
 * @param	uiTminPrimeValueTest New value as twobytes
 *
 * @return   Status as upErr
 */
upErr CTreatmentInformation::setTminPrimeValueTest( twobytes uiTminPrimeValueTest )
{
	m_uiTminPrimeValueTest = uiTminPrimeValueTest;
	return ERR_OK;
}

/**
 * @brief	This method sets number of cycles in a row to run for EMC testing
 *
 * @param	uiEmcCycles New value as twobytes
 *
 * @return   Status as upErr
 */
upErr CTreatmentInformation::setEmcCycles( twobytes uiEmcCycles )
{
	m_uiEmcCycles = uiEmcCycles;
	return ERR_OK;
}

/**
 * @brief	This method sets the tbCustom23 value for the time that the generator may be activated for a delivery
 *
 * @param	tbTprecond New value as twobytes in tenths of a second
 *
 * @return   Status as upErr
 */
upErr CTreatmentInformation::setTprecond( twobytes tbTprecond )
{
	m_tbTprecond = tbTprecond;
	return ERR_OK;
}

/**
 * @brief	This method sets the time for over-temperature to exist before an error is raised
 *
 * @param	uiTtempWindow New value as twobytes
 *
 * @return   Status as upErr
 */
upErr CTreatmentInformation::setTtempWindow( twobytes uiTtempWindow )
{
	m_uiTtempWindow = uiTtempWindow;
	return ERR_OK;
}

/**
 * @brief	This method sets the Custom21
 *
 * @param	uiCustom21 New value as twobytes
 *
 * @return   Status as upErr
 */
upErr CTreatmentInformation::setCooldownTime( twobytes uiCustom21 )
{
	m_uiCooldowntime = uiCustom21;
	return ERR_OK;
}

/**
 * @brief	This method sets the upper limit temperature for the idling
 *
 * @param	iUpperLimit New value in degrees C as int
 *
 * @return   Status as upErr
 */
upErr CTreatmentInformation::setTempUpperLimitIdle( int iUpperLimit )
{
	m_iTempUpperLimitIdle = iUpperLimit;
	return ERR_OK;
}

/**
 * @brief	This method sets the lower limit temperature for the idling
 *
 * @param	iLowerLimit New value in degrees C as int
 *
 * @return   Status as upErr
 */
upErr CTreatmentInformation::setTempLowerLimitIdle( int iLowerLimit )
{
	m_iTempLowerLimitIdle = iLowerLimit;
	return ERR_OK;
}

/**
 * @brief	This method sets the Custom8
 *
 * @param	iCustom8 New value
 *
 * @return   Status as upErr
 */
upErr CTreatmentInformation::setCustom8( int iCustom8 )
{
	m_stbCustom8 = iCustom8;
	return ERR_OK;
}

/**
 * @brief	This method sets the water flow rate for idling
 *
 * @param	iWrateIdle New value in microliters per minute as int
 *
 * @return   Status as upErr
 */
upErr CTreatmentInformation::setWrateIdle( int iWrateIdle )
{
	m_WrateIdle = iWrateIdle;
	return ERR_OK;
}

/**
 * @brief	This method sets the power for the idling
 *
 * @param	uiPowerIdle New value in VA (Volt Amperes) as twobytes
 *
 * @return   Status as upErr
 */
upErr CTreatmentInformation::setPowerIdle( twobytes uiPowerIdle )
{
	m_tbPowerIdle = uiPowerIdle;
	return ERR_OK;
}

/**
 * @brief	This method sets a value for the initial priming flow rate
 *
 * @param	iWratePrime1 New priming rate in mL/min as an int
 *
 * @return   Status as upErr
 */
upErr CTreatmentInformation::setWratePrime1( int iWratePrime1 )
{
	m_iWratePrime1 = iWratePrime1;
	return ERR_OK;
}

/**
 * @brief	This method sets a value for the second priming flow rate
 *
 * @param	iWratePrime2 New priming rate in mL/min as an int
 *
 * @return   Status as upErr
 */
upErr CTreatmentInformation::setWratePrime2( int iWratePrime2 )
{
	m_iWratePrime2 = iWratePrime2;
	return ERR_OK;
}

/**
 * @brief	This method sets the value for the initial priming flow total volume
 *
 * @param	tbVprime1 New priming volume point in mL as a twobytes
 *
 * @return   Status as upErr
 */
upErr CTreatmentInformation::setVprime1( twobytes tbVprime1 )
{
	m_uiVprime1 = tbVprime1;
	return ERR_OK;
}

/**
 * @brief	This method returns the number of steps in the delivery
 *
 * @return	Count set previously or zero if step count greater than maximum
 */
byte CTreatmentInformation::GetStepCount()
{
	if ( DELIVERY_STEPS_MAX < m_customDeliverySteps.count )
		{
		LOG_ERROR_RETURN( ERR_BAD_PARAM )
		return (byte) 0;
		}

	return m_customDeliverySteps.count;
}

/**
 * @brief	This method set the number of steps that will be used in a subsequent ablation. The count must be set before
 * 			giving the power, rate and time of the steps themselves.
 *
 * @param	bCount The number of steps to be used in a delivery as a byte
 *
 * @return  Status as upErr
 */
upErr CTreatmentInformation::SetStepCount( byte bCount )
{
	if ( DELIVERY_STEPS_MAX < bCount )
		return ERR_BAD_PARAM;

	m_customDeliverySteps.count = bCount;
	return ERR_OK;
}

/**
 * @brief	This method returns the water flow rate in microliters per minute for the given step
 *
 * @param bStep The step to fetch as an byte
 *
 * @return	Value according to treatment code or zero if step outside step count.
 */
int CTreatmentInformation::GetStepWrate( byte bStep )
{
	if ( DELIVERY_STEPS_MAX <= bStep || bStep >= GetStepCount() )
		{
		LOG_ERROR_RETURN( ERR_BAD_PARAM )
		return 0;
		}

	return m_customDeliverySteps.wRate[ bStep ];
}

/**
 * @brief	This method has the water rate in microliters per minute set for a delivery step.
 *
 * @param	bStep The step in the delivery as a byte
 * @param	iRate The water rate in uL/min as an int
 *
 * @return   Status as upErr
 */
upErr CTreatmentInformation::SetStepWrate( byte bStep, int iRate )
{
	if ( DELIVERY_STEPS_MAX < bStep )
		return ERR_BAD_PARAM;

	m_customDeliverySteps.wRate[ bStep ] = iRate;
	return ERR_OK;
}

/**
 * @brief	This method gets the time in milliseconds for the step.
 *
 * @param	bStep The step of interest as a byte
 *
 * @return   Time for step as a twobytes
 */
twobytes CTreatmentInformation::GetStepTime( byte bStep )
{
	if ( DELIVERY_STEPS_MAX <= bStep )
		{
		LOG_ERROR_RETURN( ERR_BAD_PARAM )
		return 0;
		}

	return m_customDeliverySteps.time[ bStep ];
}

/**
 * @brief	This method sets the time for the given step. Time is in milliseconds.
 *
 * @param	bStep The step to set as a byte
 * @param	tbTime The time in milliseconds as a twobytes
 *
 * @return   Status as upErr
 */
upErr CTreatmentInformation::SetStepTime( byte bStep, twobytes tbTime )
{
	if ( DELIVERY_STEPS_MAX < bStep )
		return ERR_BAD_PARAM;

	m_customDeliverySteps.time[ bStep ] = tbTime;
	return ERR_OK;
}

/**
 * @brief	This method gets the power for the given step in Watts.
 *
 * @param	bStep The step of interest as a byte
 *
 * @return   Power as a twobytes or zero if error
 */
twobytes CTreatmentInformation::GetStepPower( byte bStep )
{
	if ( DELIVERY_STEPS_MAX <= bStep )
		{
		LOG_ERROR_RETURN( ERR_BAD_PARAM )
		return 0;
		}

	return m_customDeliverySteps.power[ bStep ];
}

/**
 * @brief	This method sets the power for a given step. The power is in Watts.
 *
 * @param	bStep The step to set as a byte
 * @param	tbPower The power in Watts as a twobytes
 *
 * @return   Status as upErr
 */
upErr CTreatmentInformation::SetStepPower( byte bStep, twobytes tbPower )
{
	if ( DELIVERY_STEPS_MAX < bStep )
		return ERR_BAD_PARAM;

	m_customDeliverySteps.power[ bStep ] = tbPower;
	return ERR_OK;
}

/**
 * @brief	This method returns the entire step structure
 *
 * @param	pCustomDeliverySteps A pointer to a caller-allocated data structure of SDeliverySteps
 *
 * @return   Status as upErr
 */
upErr CTreatmentInformation::GetStepValues( SDeliverySteps *pCustomDeliverySteps )
{
	if ( 0 == pCustomDeliverySteps )
		return ERR_BAD_PARAM;

	*pCustomDeliverySteps = m_customDeliverySteps;
	return ERR_OK;
}

/**
 * @brief	This method sets the entire set of delivery steps at one time.
 *
 * @param	pCustomDeliverySteps A pointer to a SDeliverySteps that contains the new step data
 *
 * @return   Status as upErr
 */
upErr CTreatmentInformation::SetStepValues( SDeliverySteps *pCustomDeliverySteps )
{
	if ( 0 == pCustomDeliverySteps || DELIVERY_STEPS_MAX < pCustomDeliverySteps->count )
		return ERR_BAD_PARAM;

	m_customDeliverySteps = *pCustomDeliverySteps;
	return ERR_OK;
}

/**
 * @brief	This method sets the value for the second priming flow total volume
 *
 * @param	tbVprime2 New priming volume point in mL as a twobytes
 *
 * @return   Status as upErr
 */
upErr CTreatmentInformation::setVprime2( twobytes tbVprime2 )
{
	m_uiVprime2 = tbVprime2;
	return ERR_OK;
}

/**
 * @brief	This method check the treatment code for being in the known set
 *
 * @param	eTreatmentCode Model code as upTreatmentCode
 *
 * @return	ERR_OK if parameter was accepted
 */
upErr CTreatmentInformation::checkTreatmentCode( upTreatmentCode eTreatmentCode )
{
	switch ( eTreatmentCode )
		{

		// Good codes
		case TREATMENT_V_1_0:
		case TREATMENT_UPTAKE_DEV_1:
		case TREATMENT_V_1_1:
		case TREATMENT_CUSTOM:
		case TREATMENT_ENGINEERING:
			return ERR_OK;
			break;

			// Only allow EMC to be used in special build
#ifdef ALLOW_EMC_TREATMENT_CODE
			case TREATMENT_EMC:
			return ERR_OK;
			break;
#endif
			// Not accepted codes
		case TREATMENT_SPECIAL_1:
		case TREATMENT_SPECIAL_2:
		case TREATMENT_SPECIAL_3:
		case TREATMENT_SPECIAL_4:
		case TREATMENT_SHAM_V_1_0:

		default:
			break;
		}
	return ERR_BAD_PARAM;
}

// Private Class Functions


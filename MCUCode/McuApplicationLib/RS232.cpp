/**
 * @file	RS232.cpp
 * @par		Package: UGMcu
 * @par		Project: Union Generator
 * @par		Copyright (c) 2017, Uptake Medical Technology, Inc.
 * @author	Glen Sep 15, 2010 Created file
 * @brief	This file contains the RS232 interface code
 */

// Include Files
#include "RS232.h"
#include "UGMcuDefinitions.h"

// External Public Data

// File Scope Macros and Constants

// File Scope Type Definitions (Enums, Structs & Classes)
#define RX_USART0_BUF_MAX	(RF_RECV_BYTE_MAX)      // USART0 goes to ITherm box
#define TX_USART0_BUF_MAX	(RF_SEND_BYTE_MAX)
#define RX_USART1_BUF_MAX  	(SBC_RECV_BYTE_MAX)     // USART1 goes to SBC
#define TX_USART1_BUF_MAX  	(SBC_SEND_BYTE_MAX)

#define ENABLE_USART0_INTERRUPTS		// Enable TX and RX and interrupts on both
#define DISABLE_USART0_INTERRUPTS		// Enable TX and RX and disable interrupts
#define ENABLE_USART1_INTERRUPTS		// Enable TX and RX and interrupts on both
#define DISABLE_USART1_INTERRUPTS		// Enable TX and RX and disable interrupts

// File Scope Data
volatile byte f_usart0RxBuf[ RX_USART0_BUF_MAX ];
volatile byte f_usart0TxBuf[ TX_USART0_BUF_MAX ];
volatile byte f_usart1RxBuf[ RX_USART1_BUF_MAX ];
volatile byte f_usart1TxBuf[ TX_USART1_BUF_MAX ];

volatile byte f_usart0RxCnt = 0;
volatile byte f_usart0TxHead = 0;
volatile byte f_usart0TxTail = 0;
volatile byte f_usart1RxCnt = 0;
volatile byte f_usart1TxHead = 0;
volatile byte f_usart1TxTail = 0;

// File Scope Functions

/**
 * @brief	This is the interrupt function for receiving on USART0. It copies
 * 			each received byte into the buffer up to the maximum size and then discards
 * 			the received bytes
 *
 * @return   Status as upErr
 */

/**
 * @brief	This is the interrupt function for transmitting on USART0. It moves data from the
 * 			output queue and updates the head index as it goes. Data can be added to the queue
 * 			while in operation up to the maximum size of the buffer. Upon completion it sets
 * 			the head and tail indexes to zero.
 *
 * @return   Status as upErr
 */

/**
 * @brief	This is the interrupt function for receiving on USART1. It copies
 * 			each received byte into the buffer up to the maximum size and then discards
 * 			the received bytes
 *
 * @return   Status as upErr
 */

/**
 * @brief	This is the interrupt function for transmitting on USART1. It moves data from the
 * 			output queue and updates the head index as it goes. Data can be added to the queue
 * 			while in operation up to the maximum size of the buffer. Upon completion it sets
 * 			the head and tail indexes to zero.
 *
 * @return   Status as upErr
 */

// Public Functions

/**
 * @brief	This is the constructor for CRS232. It initializes the baud rate array.
 *
 */
CRS232::CRS232()
{
	m_baudRate[ RS232_BAUD_9600 ] = 9600;
	m_baudRate[ RS232_BAUD_38400 ] = 38400;
	m_baudRate[ RS232_BAUD_115200 ] = 115200;
}

/**
 * @brief	This method initializes a single UART, either 1 or 2
 *
 * @param   ePort	Selects which port will be used
 * @param   eBaud	The Baud rate enumeration
 * @param	eParity	The parity to use
 * @param   bBits	The number of data bits
 * @param   bStop	The number of stop bits
 *
 * @return   Status as upErr
 */
upErr CRS232::InitializePort( upRS232Port ePort, upRS232Baud eBaud, upRS232Parity eParity, byte bBits, byte bStop )
{
	long unsigned int liTemp;
	byte bParity = 0;

	// Compute clock divider for BAUD
	if ( eBaud >= RS232_BAUD_CNT )
		return ERR_ENUM_OUT_OF_RANGE;

	liTemp = SYSTEM_CLOCK_RATE;
	liTemp = liTemp / ( 16 * m_baudRate[ eBaud ] ) - 1;

	// Select Parity mask value
	switch ( eParity )
		{
		case RS232_PARITY_NONE:
			bParity = 0;
			break;
		case RS232_PARITY_EVEN:
			bParity = 2;
			break;
		case RS232_PARITY_ODD:
			bParity = 3;
			break;
		default:
			return ERR_ENUM_OUT_OF_RANGE;
		}

	// Select data bits mask
	if ( bBits >= 5 && bBits < 9 )
		{
		bBits = bBits - 5;
		}
	else if ( bBits == 9 )
		{
		bBits = 7;
		}
	else
		return ERR_BAD_PARAM;

	if ( ePort == RS232_USART0 )
		{
		ENABLE_USART0_INTERRUPTS
		}
	else
		{
		ENABLE_USART1_INTERRUPTS
		}

	return ERR_OK;
}

/**
 * @brief	This method initiates write of data out a serial port.
 *
 * 			The first byte is sent here and the
 * 			balance of the buffer is sent via an interrupt.
 *
 * @param	ePort Selects which serial port
 * @param	tbWrite Count of bytes to write
 * @param	pBytes Pointer to array of bytes to send
 *
 * @return   Status as upErr
 */

upErr CRS232::WritePort( upRS232Port ePort, twobytes tbWrite, byte *pBytes )
{
	byte i;

	// Write to USART0
	if ( RS232_USART0 == ePort )
		{

		// Error if too many bytes or zero
		if ( tbWrite > TX_USART0_BUF_MAX || 0 == pBytes )
			{
			return ERR_BAD_PARAM;
			}

		DISABLE_USART0_INTERRUPTS

		// If buffer was emptied then reset the indexes
		if ( f_usart0TxHead >= f_usart0TxTail )
			{
			f_usart0TxHead = 0;
			f_usart0TxTail = 0;
			}

		// Error if bytes would overrun
		if ( tbWrite + f_usart0TxTail > TX_USART0_BUF_MAX )
			{
			ENABLE_USART0_INTERRUPTS
			return ERR_NOT_READY;
			}

		// Add bytes to write buffer
		for ( i = 0; i < tbWrite; i++ )
			f_usart0TxBuf[ i + f_usart0TxTail ] = pBytes[ i ];

		// If buffer was empty the set the indexes and send the first byte
		if ( 0 == f_usart0TxTail )
			{
			f_usart0TxHead = 1;
			f_usart0TxTail = tbWrite;
			ENABLE_USART0_INTERRUPTS
			}

		// If there were bytes in the buffer then add the new to the count
		else
			{
			f_usart0TxTail = tbWrite + f_usart0TxTail;
			ENABLE_USART0_INTERRUPTS
			}

		} // USART0

	// Write to USART1
	else if ( RS232_USART1 == ePort )
		{

		// Error if too many bytes or zero
		if ( tbWrite > TX_USART1_BUF_MAX || 0 == pBytes )
			{
			return ERR_BAD_PARAM;
			}

		// Error if bytes would overrun
		DISABLE_USART1_INTERRUPTS

		// If buffer was emptied then reset the indexes
		if ( f_usart1TxHead >= f_usart1TxTail )
			{
			f_usart1TxHead = 0;
			f_usart1TxTail = 0;
			}

		if ( tbWrite + f_usart1TxTail > TX_USART1_BUF_MAX )
			{
			ENABLE_USART1_INTERRUPTS
			return ERR_NOT_READY;
			}

		// Add bytes to write buffer
		for ( i = 0; i < tbWrite; i++ )
			f_usart1TxBuf[ i + f_usart1TxTail ] = pBytes[ i ];

		// If buffer was empty the set the indexes and send the first byte
		if ( 0 == f_usart1TxTail )
			{
			f_usart1TxHead = 1;
			f_usart1TxTail = tbWrite;
			ENABLE_USART1_INTERRUPTS
			}

		// If there were bytes in the buffer then add the new count to the tail
		else
			{
			f_usart1TxTail = tbWrite + f_usart1TxTail;
			ENABLE_USART1_INTERRUPTS
			}

		} // USART1

	return ERR_OK;
}

/**
 * @brief	This method returns the data read by a serial port.
 *
 * 			The data is received
 * 			in an interrupt and here is copied from the receive buffer. The receive buffer
 * 			count is cleared after the copy is done. This call can wait for a count of ticks
 * 			to elapse for a specific number of bytes to arrive. If there are more bytes in the
 * 			receive buffer than are requested then the first N bytes are returned and the
 * 			balance is discarded.
 *
 * @param	ePort Selects which serial port
 * @param	tbGet Count of bytes to read
 * @param	pBytes Pointer to caller-allocated array of bytes to receive data
 * @param	ptbRead The count of bytes actually read
 * @param	bWait Milliseconds to wait or zero to not wait
 *
 * @return   Status as upErr
 */
upErr CRS232::ReadPort( upRS232Port ePort, twobytes tbGet, byte *pBytes, twobytes * ptbRead, byte bWait )
{
	byte i;
	tick tStart;
	twobytes tbDiff;
	twobytes tbRecvd;

	if ( ePort >= RS232_CNT )
		return ERR_ENUM_OUT_OF_RANGE;

	if ( ePort == RS232_USART0 )
		{
		DISABLE_USART0_INTERRUPTS
		tbRecvd = f_usart0RxCnt;
		ENABLE_USART0_INTERRUPTS
		}
	else
		{
		DISABLE_USART1_INTERRUPTS
		tbRecvd = f_usart1RxCnt;
		ENABLE_USART1_INTERRUPTS
		}

	if ( 0 != bWait && tbGet > tbRecvd )
		{
		GetTick( &tStart );
		while ( tbGet > tbRecvd )
			{
			GetTickElapsed( tStart, &tbDiff );
			if ( tbDiff > MS_TO_TICKS( bWait ) )
				{
				*ptbRead = 0;
				return ERR_TIMEOUT;
				}

			if ( ePort == RS232_USART0 )
				{
				DISABLE_USART0_INTERRUPTS
				tbRecvd = f_usart0RxCnt;
				ENABLE_USART0_INTERRUPTS
				}
			else
				{
				DISABLE_USART1_INTERRUPTS
				tbRecvd = f_usart1RxCnt;
				ENABLE_USART1_INTERRUPTS
				}
			}
		}

	// Read the bytes into the consumers buffer. Bytes past the requested count are discarded
	if ( RS232_USART0 == ePort )
		{
		DISABLE_USART0_INTERRUPTS
		for ( i = 0; i < f_usart0RxCnt && i < tbGet; i++ )
			pBytes[ i ] = f_usart0RxBuf[ i ];
		f_usart0RxCnt = 0;
		ENABLE_USART0_INTERRUPTS
		*ptbRead = i;
		}
	else if ( RS232_USART1 == ePort )
		{
		DISABLE_USART1_INTERRUPTS
		for ( i = 0; i < f_usart1RxCnt && i < tbGet; i++ )
			pBytes[ i ] = f_usart1RxBuf[ i ];
		f_usart1RxCnt = 0;
		ENABLE_USART1_INTERRUPTS
		*ptbRead = i;
		}

	return ERR_OK;
}

/**
 * @brief	This method returns the count of bytes in the buffer that are yet to be sent out the port.
 *
 * @param	ePort 	The enumeration of the port as upRS232Port
 * @param	ptbRead	A pointer to a twobytes to receive the count of bytes
 *
 * @return   ERR_BAD_PARAM if bad enum of zero pointer is supplied otherwise ERR_OK as upErr
 */
upErr CRS232::GetPendingSendCount( upRS232Port ePort, twobytes *ptbRead )
{
	upErr eErr = ERR_BAD_PARAM;

	// Check pointer
	if ( 0 == ptbRead )
		return eErr;

	// Get the count for the right port
	if ( RS232_USART0 == ePort )
		{
		DISABLE_USART0_INTERRUPTS
		*ptbRead = f_usart0TxTail - f_usart0TxHead;
		ENABLE_USART0_INTERRUPTS
		eErr = ERR_OK;
		}
	else if ( RS232_USART1 == ePort )
		{
		DISABLE_USART1_INTERRUPTS
		*ptbRead = f_usart1TxTail - f_usart1TxHead;
		ENABLE_USART1_INTERRUPTS
		eErr = ERR_OK;
		}

	return eErr;
}

/**
 * @brief	This method empties the read buffer for the selected port
 *
 * @param	ePort Selects which serial port to flush
 *
 * @return   Status as upErr
 */
upErr CRS232::FlushReadBuffer( upRS232Port ePort )
{
	upErr eErr = ERR_BAD_PARAM;

	// Empty the buffer
	if ( RS232_USART0 == ePort )
		{
		DISABLE_USART0_INTERRUPTS
		f_usart0RxCnt = 0;
		ENABLE_USART0_INTERRUPTS
		eErr = ERR_OK;
		}
	else if ( RS232_USART1 == ePort )
		{
		DISABLE_USART1_INTERRUPTS
		f_usart1RxCnt = 0;
		ENABLE_USART1_INTERRUPTS
		eErr = ERR_OK;
		}
	return eErr;
}

// Private Class Functions


/**
 * @file	AblationControl.cpp
 * @par		Package: UGMcu
 * @par		Project: Union Generator
 * @par		Copyright (c) 2017, Uptake Medical Technology, Inc.
 * @author	Glen Oct 26, 2010 Created file
 * @brief	This file contains the code for CAblationControl.
 */

// Include Files
#include "AblationControl.h"
#include "RFGenerator.h"
#include "GeneratorMonitor.h"
#include "CommonOperations.h"
#include "LogMessage.h"
#include "DigitalInputOutput.h"
#include "TreatmentInformation.h"
#include "OneWireMemory.h"
#include "TreatmentRecord.h"
#include "Temperature.h"
#include "WaterCntl.h"
#include "DeliveryOracle.h"

// External Public Data

// File Scope Macros and Constants

// File Scope Type Definitions (Enums, Structs & Classes)

// File Scope Data
tick f_tTotalTicks = 0;

// File Scope Functions

// Public Functions

/**
 * @brief	This is the constructor for AblationControl.
 *
 * This constructor assigns the initial variable values.
 *
 */
CAblationControl::CAblationControl()
{
	m_eAblState = ABLATE_STATE_COMPLETE;
	m_eTreatStatus = DELIVERY_PARTIAL_ERROR;
	m_tbAblationRunTicks = 0;
	m_startTick = 0;
	m_lastEndTick = 0;
}

/**
 * @brief	This method resets the state flag to have a new ablation begun
 * 			when Execute() is called.
 *
 * @return   Status as upErr
 */
upErr CAblationControl::StartAblation()
{
	upErr eErr;
	twobytes tbMillisecs;

	// Set flags and start delivery
	m_eAblState = ABLATE_STATE_START;
	m_eTreatStatus = DELIVERY_PARTIAL_ERROR;
	GetTick( &m_startTick );
	m_tbAblationRunTicks = 0;
	g_pComOp->SetStartState(); // Set with first step

	// Get delivery time
	LOG_ERROR_RETURN( eErr = g_pDelOracle->GetTotalTime( &tbMillisecs ) )
	f_tTotalTicks = MS_TO_TICKS( tbMillisecs );

	g_pLogMessage->LogErrorMessage( ERR_OK, "Planned total ms: ", (int) tbMillisecs );

	return eErr;
}

/**
 * @brief	This method performs the "middle" of the ablation operation which is where
 * 			the energy is injected into the tissue.
 *
 * It monitors the generator operation and delivery device trigger and checks that temperatures are within normal ranges. It also
 * monitors the elapsed time and will end the ablation once the full time has elapsed.
 *
 * @return   Status as upErr
 */
upErr CAblationControl::AblationMiddle()
{
	upErr eErr = ERR_OK;

	if ( m_tbAblationRunTicks >= f_tTotalTicks )
		{
		m_eAblState = ABLATE_STATE_STOP;
		m_eTreatStatus = DELIVERY_FULL;
		g_pLogMessage->LogMessage( "Complete treatment" );
		}

	else
		{
		eErr = g_pComOp->ConstantVaporFlow( m_tbAblationRunTicks,
		        (tick) MS_TO_TICKS( g_pTreatmentInfo->getTminValueTest() ) );
		if ( ERR_OK != eErr )
			{
			m_eAblState = ABLATE_STATE_ERROR;
			m_eTreatStatus = DELIVERY_PARTIAL_ERROR;
			g_pLogMessage->LogMessage( "Partial treatment: error" );
			}

		// Move ahead to next step
		else
			{
			LOG_ERROR_RETURN( eErr = g_pComOp->AdvancedDeliverySteps( m_tbAblationRunTicks ) )
			}
		}

	return eErr;
}

/**
 * @brief	This method is the top-level state machine for performing an ablation.
 *
 * This method uses a case statement based on the current state of the ablation. The state is maintained
 * in CAblationControl::m_eAblState. The state progresses through initiating flow, ablating, halting and updating information.
 * If an error is found then the error state is entered. @n@n
 * At ablation start,  the rod extension is checked. If the rod has connected the extend limit switch then the ablation is halted with an
 * error. If those checks pass then CCommonOperations::InitiateFlow() is called. The state of that method
 * is tracked for becoming upAblationStateEnum::GEN_STATE_COMPLETE or GEN_STATE_ERROR or the method returning an error. If an error
 * is found then the ablation is halted and the error is reported.  @n@n
 * If CCommonOperations::InitiateFlow() becomes
 * GEN_STATE_COMPLETE then the ablation proceeds to the actual ablation "middle" and state ABLATE_STATE_ABLATE.
 *
 * @return   Status as upErr
 */
upErr CAblationControl::PerformTreatment()
{
	upErr eErr = ERR_OK;
	bool_c bExt;
	bool_c bTrig;

	// Get the current tick and compute run time if in "start" or "ablate" states. Don't in "halt" or "update"
	if ( m_eAblState == ABLATE_STATE_START || m_eAblState == ABLATE_STATE_ABLATE )
		{
		GetTickElapsed( m_startTick, &m_tbAblationRunTicks );
		}

	// Check generator conditions
	upInterlockFlag eInterlockNeeded = INTLCK_ALL;
	g_pGeneratorMonitor->GetNeededInterlocks( OPER_STATE_DELIVERING, &eInterlockNeeded );
	LOG_ERROR_RETURN( eErr = g_pGeneratorMonitor->CheckGenerator( eInterlockNeeded ) )

	// Bad interlock or other error
	if ( ERR_OK != eErr )
		{
		m_eAblState = ABLATE_STATE_ERROR;
		m_eTreatStatus = DELIVERY_PARTIAL_ERROR;
		}

	// Check and debounce trigger
	else
		{
		g_pGeneratorMonitor->GetDebouncedTrigger( &bTrig );
		if ( false == bTrig )
			{
			m_eTreatStatus = DELIVERY_PARTIAL_NO_ERR;
			LOG_ERROR_RETURN( eErr = ERR_TRIGGER_OFF )
			m_eAblState = ABLATE_STATE_ERROR;
			}
		}

	// Main state machine for ablating
	switch ( m_eAblState )
		{

		// When starting using the shared InitiateFlow to get the coil hot and the pump running
		case ABLATE_STATE_START:

			// Check rod extension
			LOG_ERROR_RETURN( eErr = g_pDigitalInputOutput->GetInput( DIG_IN_MOTOR_EXTEND, &bExt ) )
			if ( bExt )
				{
				LOG_ERROR_RETURN( eErr = ERR_PUMP_AT_LIMIT )
				}

			// All ERR_OK SO FAR so start ablation
			if ( ERR_OK == eErr )
				{
				LOG_ERROR_RETURN( eErr = g_pComOp->StartDeliverySteps() )
				}

			// Error starting ablation or bad result from state of InitiateVaporImmediately()
			if ( ERR_OK != eErr || g_pComOp->GetSubState() != GEN_STATE_START )
				{
				m_eAblState = ABLATE_STATE_ERROR;
				}

			// Go to middle section
			else
				{
				m_eAblState = ABLATE_STATE_ABLATE;
				}
			break;

			// This is the "middle" of the treatment when the water is already flowing and the coil is hot
		case ABLATE_STATE_ABLATE:

			// Perform the ablation
			// The state is set to ABLATE_STATE_STOP inside AblationMiddle() when the full treatment has been done
			LOG_ERROR_RETURN( eErr = AblationMiddle() )
			break;

			// These cases are all handled in the next switch
			// Ablation complete this may never be used
		case ABLATE_STATE_COMPLETE:
			// No operation. Should not be used

		case ABLATE_STATE_ERROR:
		case ABLATE_STATE_STOP:
		default:

			break;

		} // switch ( m_eAblState )

	//
	// Handle termination with halt and update immediately
	//
	switch ( m_eAblState )
		{

		// Ablating so don't stop
		case ABLATE_STATE_START:
		case ABLATE_STATE_ABLATE:
		case ABLATE_STATE_COMPLETE:
			break;

			// This provides an opportunity for a controlled stop to the system
		case ABLATE_STATE_ERROR:
		case ABLATE_STATE_STOP:
		default:
			LOG_ERROR_RETURN( g_pComOp->HaltPumpAndRF() )
			SetLastAblationEndTime();

			// Ensure an update to the therapy record is located in both locations
			g_pTreatmentRecord->UpdateTreatmentRecord( m_eTreatStatus, m_tbAblationRunTicks );
			m_eAblState = ABLATE_STATE_COMPLETE;
			break;

		} // switch ( m_eAblState )

	return eErr;
}

/**
 * @brief	This method returns the percent completion of the ablation
 *
 * @param	pbProgress A pointer to receive the percentage as byte*
 *
 * @return   Status as upErr
 */
upErr CAblationControl::GetProgress( byte * pbProgress )
{
	tick tDiff;

	GetTickElapsed( m_startTick, &tDiff );
	*pbProgress = (byte) ( 100 * tDiff / f_tTotalTicks );
	if ( *pbProgress > 100 )
		*pbProgress = 100;
	return ERR_OK;
}

/**
 * @brief	This method set the last ablation end time for use with pre-filling
 *
 * @return   Status as upErr
 */
upErr CAblationControl::SetLastAblationEndTime()
{
	return GetTick( &m_lastEndTick );
}

/**
 * @brief	This method returns the operating state of ablation.
 *
 * @return   State of ablation operations as upAblationStateEnum
 */
upAblationStateEnum CAblationControl::GetAblationState()
{
	return m_eAblState;
}

// Private Class Functions


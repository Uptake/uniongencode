/**
 * @file	HandpiecePrime.cpp
 * @par		Package: UGMcu
 * @par		Project: Union Generator
 * @par		Copyright (c) 2017, Uptake Medical Technology, Inc.
 * @author	Glen Oct 26, 2010 Created file
 * @brief	This file contains the code for CHandpiecePrime
 */

// Include Files
#include "HandpiecePrime.h"
#include "TreatmentInformation.h"
#include "GeneratorMonitor.h"
#include "DigitalInputOutput.h"
#include "WaterCntl.h"
#include "LogMessage.h"
#include "Temperature.h"
#include "RFGenerator.h"
#include "CommonOperations.h"

// External Public Data

// File Scope Macros and Constants

// This conversion uses ul volume target and ml/min flow rate
// It is more formally SECS_TO_TICKS(  MIN_TO_SECS( ( vol / 1000 ) / rate ) )
// At this time the tick clock is 1000/sec so those factors fall out
#if TICK_CLOCK_RATE != 1000
Tick clock not 1000
#endif
// This leaves a simplified form using wide math for accuracy
#define VOLUME_AND_RATE_TO_TICKS( vol, rate )  ( MIN_TO_SECS( (unsignedfourbytes) vol ) / (unsignedfourbytes)rate )

// File Scope Type Definitions (Enums, Structs & Classes)

// File Scope Data

// File Scope Functions

// Public Functions

/**
 * @brief	This is the constructor for HandpiecePrime.
 *
 * 			It assigns default values to all member variables.
 *
 */
CHandpiecePrime::CHandpiecePrime()
{
	m_tStartTick = 0;
	m_tRunTicks = 0;
	m_tPriorPumpTicks = 0;
	m_tNewPumpTicks = 0;
	m_state = PRIME_START;
	m_tTotalPumpTicks = 0;
}

/**
 * @brief	This method sets the flow rate for a point in time during priming. It selects from the three stages of priming.
 *
 * @param	tElapsed Time since start of priming in ticks
 *
 * @return   Status as upErr
 */
upErr CHandpiecePrime::SetFlowRate( tick tElapsed )
{
	upErr eErr = ERR_OK;
	tick tStage1Ticks;
	tick tStage2Ticks;

	// Get the target volumes freshly as they can be changed on-the-fly from the UI
	twobytes tbV1 = g_pTreatmentInfo->getVprime1();
	twobytes tbV2 = g_pTreatmentInfo->getVprime2();

	// Ensure they are in order V1 < V2
	if ( tbV1 > tbV2 )
		{
		LOG_ERROR_RETURN( eErr = ERR_BAD_PARAM )
		m_tTotalPumpTicks = 0;
		return eErr;
		}

	// Compute time periods to pump for each stage
	tStage1Ticks = VOLUME_AND_RATE_TO_TICKS( tbV1, g_pTreatmentInfo->getWratePrime1() );
	tStage2Ticks = VOLUME_AND_RATE_TO_TICKS( tbV2 - tbV1, g_pTreatmentInfo->getWratePrime2() ) + tStage1Ticks;

	// Time to run prime is either total priming time for volume or, if bumping prime,
	// one additional second than done previosly
	if ( tStage2Ticks > m_tPriorPumpTicks )
		{
		m_tTotalPumpTicks = tStage2Ticks;
		}
	else
		{
		m_tTotalPumpTicks = m_tPriorPumpTicks + TICK_CLOCK_RATE;
		}

	// Figure out which stage it is in and set the flow rate
	if ( tElapsed < tStage1Ticks )
		{
		LOG_ERROR_RETURN( eErr = g_pWaterCntl->SetFlowRateMlPerMin( g_pTreatmentInfo->getWratePrime1() ) )
		}
	else if ( tElapsed < tStage2Ticks )
		{
		LOG_ERROR_RETURN( eErr = g_pWaterCntl->SetFlowRateMlPerMin( g_pTreatmentInfo->getWratePrime2() ) )
		}

	// "Bump" flow rate
	else
		{
		LOG_ERROR_RETURN( eErr = g_pWaterCntl->SetFlowRateMlPerMin( g_pTreatmentInfo->getWratePrime3() ) )
		}

	return eErr;
}

/**
 * @brief	This method starts the priming operation.
 *
 * It checks the pump extended bit and exits in error if it is set ON.
 * If there is water available, it starts the water flowing at WRateFast.
 * It also grabs the tick timer to mark the start of priming for time-out evaluations.@n
 * If any errors are found the next state is PRIME_ERROR.@n
 *
 * @return   Status as upErr
 */
upErr CHandpiecePrime::StartPriming()
{
	upErr eErr = ERR_ERROR;
	bool_c bExt;

	m_tRunTicks = 0;

	// Check rod extension
	LOG_ERROR_RETURN( eErr = g_pDigitalInputOutput->GetInput( DIG_IN_MOTOR_EXTEND, &bExt ) );
	if ( bExt )
		LOG_ERROR_RETURN( eErr = ERR_PUMP_AT_LIMIT )

	// Start water pumping
	if ( ERR_OK == eErr )
		{
		LOG_ERROR_RETURN( eErr = SetFlowRate( m_tPriorPumpTicks ) )
		}

	if ( ERR_OK == eErr )
		{
		// Tune the RDO if starting a fresh prime
		if ( 0 == m_tPriorPumpTicks )
			LOG_ERROR_RETURN( g_pRFGen->StartFreqSweep() )
		m_state = PRIME_MONITOR_FLOW;
		GetTick( &m_tStartTick );
		}
	else
		m_state = PRIME_ERROR;

	return eErr;
}

/**
 * @brief	This method monitors the water flow. Timeouts are checked to determine failure.
 * 			Once done then the steam flow is initiated.
 *
 * @return   Status as upErr
 */
upErr CHandpiecePrime::MonitorFlow()
{
	upErr eErr = ERR_ERROR;

	// Check for total time elapsed in multi-stage priming scheme
	if ( m_tRunTicks + m_tPriorPumpTicks >= m_tTotalPumpTicks )
		{
		m_state = PRIME_CONTROLLED_STOP;
		eErr = ERR_OK;
		}

	// Set flow rate
	else
		{
		LOG_ERROR_RETURN( eErr = SetFlowRate( m_tRunTicks + m_tPriorPumpTicks ) )
		}

	if ( ERR_OK != eErr )
		{
		m_state = PRIME_ERROR;
		}

	if ( m_state != PRIME_MONITOR_FLOW )
		{
		m_tNewPumpTicks = m_tRunTicks;
		GetTick( &m_tStartTick );
		}

	return eErr;
}

/**
 * @brief	This method is the top level for priming the handpiece.
 *
 * @return   Status as upErr
 */
upErr CHandpiecePrime::PerformPrime()
{
	upErr eErr = ERR_ERROR;
	bool_c bOn = true;

	// Update run ticks
	GetTickElapsed( m_tStartTick, &m_tRunTicks );

	// Check generator and interlocks
	upInterlockFlag eInterlockNeeded = INTLCK_ALL;
	g_pGeneratorMonitor->GetNeededInterlocks( OPER_STATE_HAND_PRIMING, &eInterlockNeeded );
	LOG_ERROR_RETURN( eErr = g_pGeneratorMonitor->CheckGenerator( eInterlockNeeded ) )
	if ( ERR_OK != eErr )
		{
		if ( PRIME_MONITOR_FLOW == m_state )
			{
			m_tNewPumpTicks = m_tRunTicks;
			GetTick( &m_tStartTick );
			}
		m_state = PRIME_ERROR;
		}

	else
		{
		// Get the debounced trigger
		LOG_ERROR_RETURN( eErr = g_pGeneratorMonitor->GetDebouncedTrigger( &bOn ) )
		if ( ERR_OK != eErr || false == bOn )
			{
			if ( PRIME_MONITOR_FLOW == m_state )
				{
				m_tNewPumpTicks = m_tRunTicks;
				GetTick( &m_tStartTick );
				}
			m_state = PRIME_FAILED;
			LOG_ERROR_RETURN( eErr = ERR_TRIGGER_OFF )
			}
		}

	// Act on state of priming
	switch ( m_state )
		{

		case PRIME_START:
			// Monitor connections while waiting for handpiece to be keyed
			LOG_ERROR_RETURN( eErr = StartPriming() )
			break;

		case PRIME_MONITOR_FLOW:
			// Wait for time outs to happen
			LOG_ERROR_RETURN( eErr = MonitorFlow() )
			break;

			// Should not get here. Included for completeness
		case PRIME_CONTROLLED_STOP:
			LOG_ERROR_RETURN( g_pComOp->HaltPumpAndRF() )
			m_state = PRIME_SUCCESS;
			break;

		case PRIME_SUCCESS:
		case PRIME_FAILED:
		case PRIME_ERROR:
		default:
			LOG_ERROR_RETURN( eErr = g_pComOp->HaltPumpAndRF() )
			m_state = PRIME_FAILED;
			break;
		}

	return eErr;
}

/**
 * @brief	This method sets the state flag to "start" for priming
 *
 * @return   Status as upErr
 */
upErr CHandpiecePrime::InitializePrime()
{
	upErr eErr = ERR_OK;

	m_state = PRIME_START;

	// If rod is retracted then clear any progress now
	bool_c bRet;
	g_pWaterCntl->GetRetracted( &bRet );
	if ( bRet )
		{
		m_tPriorPumpTicks = 0;
		m_tNewPumpTicks = 0;
		}

	// Add "new" ticks to "prior" ticks to get actual run total
	m_tPriorPumpTicks += m_tNewPumpTicks;
	m_tNewPumpTicks = 0;

	// Mark start of priming
	GetTick( &m_tStartTick );
	return eErr;
}

/**
 * @brief	This method returns the latest internal state of the priming operation
 *
 * @return   Status as upErr
 */
CHandpiecePrime::PrimeStepEnum CHandpiecePrime::GetState() const
{
	return m_state;
}

/**
 * @brief	This method returns the percent completion of the priming operation
 *
 * @param	pbProgress A pointer to receive the percentage as byte*
 *
 * @return   Status as upErr
 */
upErr CHandpiecePrime::GetProgress( byte * pbProgress )
{
	tick tDiff;

	GetTickElapsed( m_tStartTick, &tDiff );
	tDiff += m_tPriorPumpTicks;
	*pbProgress = (byte) ( 100 * tDiff / m_tTotalPumpTicks );
	if ( *pbProgress > 100 )
		*pbProgress = 100;
	return ERR_OK;
}

// Private Class Functions

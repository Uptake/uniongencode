/**
 * @file	TreatmentRecord.cpp
 * @par		Package: UGMcu
 * @par		Project: Union Generator
 * @par		Copyright (c) 2017, Uptake Medical Technology, Inc.
 * @author	Chris Keller Apr 2, 2011 Created file
 * @brief	This file contains the code contained in the CTreatmentRecord class
 */

// Include Files
#include "TreatmentRecord.h"
#include "SbcIntfc.h"
#include "DeliveryOracle.h"

// External Public Data
CTreatmentRecord * g_pTreatmentRecord = 0;

// File Scope Macros and Constants

// File Scope Type Definitions (Enums, Structs & Classes)
#define TREATMENT_TIME_RES  ( 100 ) //!< 100 ms resolution

// File Scope Data

// File Scope Functions

// Public Class Methods

/**
 * @brief	This is the constructor for TreatmentRecord.
 *
 * 			It captures the @a this handle to use in CTreatmentRecord
 *    		and initializes the member variables and used to store treatment data.
 *
 */
CTreatmentRecord::CTreatmentRecord()
{
	g_pTreatmentRecord = this;
	ClearStoredValues();
}

/**
 * @brief	This method creates a new record by clearing the stored data with information from COneWireMemory
 *    		and resets the entry count to 0.
 *
 *    		It also increases a record ID to create a unique identifier
 *    		for the record (currently unused).
 *
 * @return  none
 */
void CTreatmentRecord::StartNewRecord()
{
	ClearStoredValues();
	m_entryID = 0;
}

/**
 * @brief	This method increments the entry ID.
 *
 * 			This is used when creating a new treatment value.
 *
 * @return  none
 */
void CTreatmentRecord::NewEntry()
{
	m_entryID++;
}

/**
 * @brief	This method retrieves the current entry ID.
 *
 * @return  none
 */
byte CTreatmentRecord::GetEntry()
{
	return m_entryID;
}

/**
 * @brief	This method updates the SBC interface TREATMENTINFO message with information
 *    		pertaining to the last treatment performed.
 *
 * @return   Status as upErr
 */
upErr CTreatmentRecord::UpdateTreatmentInfoMessage()
{
	upErr eErr;
	STreatmentInfoMsg * treatmentInfoMsg;
	// get a pointer to the message contained within the SbcIntfc
	LOG_ERROR_RETURN( eErr = g_pSbcIntfc->GetMessage( (void**)&treatmentInfoMsg, MSG_TYPE_TREATMENTINFO ) );

	// set the treatment information from the current therapy session
	treatmentInfoMsg->treatIdx = m_treatIdx;
	treatmentInfoMsg->fullTreatments = m_fullTreatmentCount;
	treatmentInfoMsg->totalTreatments = m_initiatedTreatmentCount;
	treatmentInfoMsg->status = (byte) m_lastTreatmentStatus;
	treatmentInfoMsg->treatmentTime = m_lastTreatmentTime;
	treatmentInfoMsg->totalUseTime = m_therapyTime;
	treatmentInfoMsg->maxTreatCount = g_pTreatmentInfo->getDeliveryCount();
	treatmentInfoMsg->maxUseTime = g_pTreatmentInfo->getTvapor();
	return eErr;
}

/**
 * @brief	This method returns the number of full treatments performed by a delivery tool
 *
 * @return   Status as upErr
 */
twobytes CTreatmentRecord::GetRecordTreatmentCount()
{
	return m_fullTreatmentCount;
}

/**
 * @brief	This method sets the count of full treatments performed by the delivery tool.
 *
 * @param   tbCount The count of full treatments for the delivery tool
 *
 * @return   none
 */
void CTreatmentRecord::SetRecordTreatmentCount( twobytes tbCount )
{
	m_fullTreatmentCount = tbCount;
}

/**
 * @brief	This method returns the count(sum) of full and partial treatments performed by the delivery tool
 *
 * @return   Status as upErr
 */
twobytes CTreatmentRecord::GetInitiatedTreatmentCount()
{
	return m_initiatedTreatmentCount;
}

/**
 * @brief	This method set the count(sum) of full and partial treatments of the delivery tool.
 *
 * @param   tbCount The count of full treatments for the delivery tool
 *
 * @return   none
 */
void CTreatmentRecord::SetInitiatedTreatmentCount( twobytes tbCount )
{
	m_initiatedTreatmentCount = tbCount;
}

/**
 * @brief	This method returns the last treatment status
 *
 * @return   The last treatment status as upDeliveryStatus
 */
upDeliveryStatus CTreatmentRecord::GetLastTreatmentStatus()
{
	return m_lastTreatmentStatus;
}

/**
 * @brief	This method sets the last treatment status
 *
 * @param   eStatus The last treatment status to set
 *
 * @return  none
 */
void CTreatmentRecord::SetLastTreatmentStatus( upDeliveryStatus eStatus )
{
	m_lastTreatmentStatus = eStatus;
}

/**
 * @brief	This method returns the time of all treatments performed by the delivery tool
 *
 * @return   The current therapy time encompassing all treatments delivered
 */
unsignedfourbytes CTreatmentRecord::GetTherapyTime()
{
	return m_therapyTime;
}

/**
 * @brief	This method sets the total treatment time for the delivery tool.
 *
 * @param   ulTime The total therapy time encompassing all treatments delivered
 *
 * @return  none
 */
void CTreatmentRecord::SetTherapyTime( unsignedfourbytes ulTime )
{
	m_therapyTime = ulTime;
}

/**
 * @brief	This method returns the duration of the last treatment performed by the delivery tool
 *
 * @return   The current therapy time encompassing all treatments delivered
 */
twobytes CTreatmentRecord::GetLastTreatmentTime()
{
	return m_lastTreatmentTime;
}

/**
 * @brief	This method sets the duration of the last treatment performed by the delivery tool.
 *
 * @param   tbTime The duration of the last treatment in milliseconds
 *
 * @return  none
 */
void CTreatmentRecord::SetLastTreatmentTime( twobytes tbTime )
{
	m_lastTreatmentTime = tbTime;
}

// Private Class Methods

/**
 * @brief	This method resets the member variables used to store treatment information.
 *
 * @return  none
 */
void CTreatmentRecord::ClearStoredValues()
{
	m_treatIdx = 0;
	m_fullTreatmentCount = 0;
	m_initiatedTreatmentCount = 0;
	m_lastTreatmentTime = 0;
	m_therapyTime = 0;
	m_lastTreatmentStatus = DELIVERY_NONE;
}

/**
 * @brief	This method updates the therapy record with the latest treatment data.
 *
 * This is used for full or partial treatments and also is used to track the ON time of priming.
 *
 * @param eStatus Designates if the treatment was priming, full or partial as upDeliveryStatus
 * @param tbVaporOnTicks The number of ticks that vapor was ON during the priming or treatment
 *
 * @return   Status as upErr
 */
upErr CTreatmentRecord::UpdateTreatmentRecord( upDeliveryStatus eStatus, twobytes tbVaporOnTicks )
{
	upErr eErr = ERR_OK;
	twobytes tbTreatTime;

	NewEntry();

	// Round the vapor ON time to the nearest integral multiple of RND_ROBIN_TICK_CNT
	tbVaporOnTicks = TREATMENT_TIME_RES * ( ( tbVaporOnTicks + ( TREATMENT_TIME_RES >> 1 ) ) / TREATMENT_TIME_RES );

	// Warning: Time scales are mixed: Last treatment (ms), All treatments (s)
	SetLastTreatmentStatus( eStatus );

	switch ( eStatus )
		{

		case DELIVERY_FULL:
			// For full treatment use time from therapy info so total is perfect. The resolution is milliseconds.
			twobytes tbTotal;
			g_pDelOracle->GetTotalTime( &tbTotal );
			tbTreatTime = TICKS_TO_MS( SEC_TENTHS_TO_TICKS( tbTotal ) );

			// Increment full treatment count and set full treatment flag
			SetRecordTreatmentCount( GetRecordTreatmentCount() + 1 );
			SetInitiatedTreatmentCount( GetInitiatedTreatmentCount() + 1 );
			break;

		case DELIVERY_PARTIAL_NO_ERR:
		case DELIVERY_PARTIAL_ERROR:
			SetInitiatedTreatmentCount( GetInitiatedTreatmentCount() + 1 );

		case DELIVERY_PRIME:
			// only add treatment/use time if vapor prime
			tbTreatTime = (twobytes) TICKS_TO_MS( tbVaporOnTicks );
			break;

		default:
			break;
		}

	SetTherapyTime( GetTherapyTime() + tbTreatTime );
	SetLastTreatmentTime( tbTreatTime );

	// Update info for UI
	UpdateTreatmentInfoMessage();

	return eErr;
}


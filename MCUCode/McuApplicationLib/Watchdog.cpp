/**
 * @file	Watchdog.cpp
 * @par		Package: UGMcu
 * @par		Project: Union Generator
 * @par		Copyright (c) 2017, Uptake Medical Technology, Inc.
 * @author	Glen Sep 15, 2010 Created file
 * @brief	This file contains the code for determining when to kick the watchdog timers, CWatchdog
 */

// Include Files
#include "Watchdog.h"

// External Public Data

// File Scope Macros and Constants

// File Scope Type Definitions (Enums, Structs & Classes)
#define WATCHDOG_STROBE_VALUE	(0x1)

// File Scope Data

// File Scope Functions

// Public Functions

/**
 * @brief	This is the constructor for CWatchdog
 *
 */
CWatchdog::CWatchdog()
{
}

/**
 * @brief	This method determines if the software is executing properly and kicks the two
 * 			watchdog timers if it is
 *
 * @return   Status as upErr
 */

upErr CWatchdog::Execute()
{
	// strobe the CPLD watchdog
	if ( GetPassEnabled() )
		{
		byte bStrobe = WATCHDOG_STROBE_VALUE;
		// strobe the MCU watchdog


		// Strobe the CPLD watchdog
		MMAP_WRITE( MMAP_FUNC_WDOG_STROBE, &bStrobe );
		}

	return ERR_OK;
}

/**
 * @brief	This method initializes the watchdog timer hardware
 *
 * @return   Status as upErr
 */
upErr CWatchdog::InitializeHardware()
{
	upErr eErr = ERR_OK;
	byte bData[ 2 ] =
	{ 0xFF, 0xFF };

	// initialize the CPLD watchdog
	eErr = MMAP_WRITE( MMAP_FUNC_WDOG, bData );
	if ( eErr == ERR_OK )
		{
		eErr = MMapWriteByte( MMAP_FUNC_WDOG_STROBE, WATCHDOG_STROBE_VALUE );
		}
	// initialize the MCU internal watchdog
	return eErr;
}

/**
 * @brief	This method uses the watchdog time to reset the processor.
 *
 * An infitie loop is entered after enabling the watchdog timer. This method never returns.
 *
 * @param	bWDTCode The time to have the watchdog wait as a byte carying the WDTO_* code
 *
 */
void CWatchdog::ResetAfterWait( byte bWDTCode )
{
	while ( 1 )
		;
}

// Private Class Functions


/**
 * @file       RndRobinHelper.cpp
 * @par        Package: UGMcu
 * @par        Project: Union Generator
 * @par        Copyright (c) 2017, Uptake Medical Technology, Inc.
 * @author     Glen Sep 15, 2010 Created file
 * @brief      This file has the CRndRobinHelper stubs for the helper functions required in the Round Robin
 * 			   task organization.
 */

// Include Files
#include "UGMcuDefinitions.h"
#include "RndRobinHelper.h"
#include "DiagnosticLed.h"

// External Public Data

// File Scope Macros and Constants

// File Scope Type Definitions (Enums, Structs & Classes)

// File Scope Data
byte CRndRobinHelper::m_passFlag = 1;
byte CRndRobinHelper::m_passIndex = 0;
twobytes CRndRobinHelper::m_passCount = 0;

// File Scope Functions

// Public Functions

/**
 * @brief	This is the constructor for CRndRobinHelper.
 *
 * It sets the local pass mask (m_passMask) to PAS_MASK_ALL which is all bits ON.
 *
 */
CRndRobinHelper::CRndRobinHelper()
{
	m_passMask = PASS_MASK_ALL;
}

/**
 * @brief 	This is a stub that defines an interface for reading from the hardware.
 *
 * It must be overridden to be useful. The purpose would be to have new values read from the
 * component hardware that the child class controls. These methods will be called before the CSupervisor
 * Execute() method.
 *
 * @return ERROR as it was not overridden
 */
upErr CRndRobinHelper::ReadFromHardware()
{
	return ERR_ERROR;
}

/**
 * @brief 	This is a stub that defines an interface for writing to the hardware.
 *
 * It must be overridden to be useful. The purpose would be to have new values written to the
 * component hardware that the child class controls. These methods will be called after the
 * CSupervisor Execute() method.
 *
 * @return ERROR as it was not overridden
 */
upErr CRndRobinHelper::WriteToHardware()
{
	return ERR_ERROR;
}

/**
 * @brief 	This is a stub that defines an interface for initializing hardware.
 *
 * It must be overridden to be useful. The purpose would be to have the component hardware initialized
 * that the child class controls. This method would be called once following instantiation of all objects
 * so each class can access the resources of the others.
 *
 * @return ERROR as it was not overridden
 */
upErr CRndRobinHelper::InitializeHardware()
{
	return ERR_ERROR;
}

/**
 * @brief 	This is a stub that defines an interface for an "execute" operation.
 *
 * It must be overridden to be useful. The purpose would be to have the component perform
 * more complex internal processing than reading or writing hardware.
 *
 * @return ERROR as it was not overridden
 */
upErr CRndRobinHelper::Execute()
{
	return ERR_ERROR;
}


/**
 * @brief	This method rotates the pass flag ON bit circularly around the pass flag value.
 *
 * Only one bit is turned on at	any one time. The m_passFlag is shared between all instances
 * of CRndRobinHelper. The cycle is also tracked as an integer (0-7). @n
 * Calling this method increments m_passCount which is the total number of cycles executed. It is a two byte
 * value so wraps after 64k cycles. This is 5 hours of ON time. It is for debugging and simulation so
 * 5 hours is more than enough.
 *
 * @param pbPassIndex	The a pointer to a byte to receive the loop cycle index
 *
 * @return   Status as upErr
 */
upErr CRndRobinHelper::IncrementOnlyPassFlag( byte * pbPassIndex )
{
	m_passFlag <<= 1;
	m_passIndex++;
	if ( 0 == m_passFlag )
		{
		m_passFlag = 1;
		m_passIndex = 0;
		}
	*pbPassIndex = m_passIndex;
	return ERR_OK;
}

/**
 * @brief	This method rotates the pass flag ON bit circularly around the pass flag value and
 * 			increments the count of cycles.
 *
 * 			This calls IncrementOnlyPassFlag() to increment the pass mask and index.
 *
 * @param pbPassIndex	The a pointer to a byte to receive the loop cycle index
 *
 * @return   Status as upErr
 */
upErr CRndRobinHelper::IncrementPassFlag( byte * pbPassIndex )
{
	m_passCount++;
	return IncrementOnlyPassFlag( pbPassIndex );
}

/**
 * @brief	This method ORs the m_passFlag with the m_passMask for the helper instance.
 *
 * This support function is used to alternate operations inside the round robin tasks. For example,
 * if the result is a 1 (true) the child object may run a read or write operation on this iteration
 * of the round robin loop. The mask values allow load balancing between loops or other timing needs.
 *
 * @return   True if action is enabled for this pass
 */
bool_c CRndRobinHelper::GetPassEnabled()
{
	return ( ( m_passFlag & m_passMask ) ? true : false );
}

/**
 * @brief	This method resets the pass flag to 1
 *
 * @return	Status as upErr
 */
upErr CRndRobinHelper::ResetPassFlag()
{
	m_passFlag = 1;
	return ERR_OK;
}

/**
 * @brief	This method sets the mask for which passes will result in an enabled action.
 *
 * The mask is viewed from low to high and is 8 bits wide. A "1" in the bit means on
 * that pass the action (task) should be done. This is handled inside the child classes and
 * not by the scheduler itself.
 *
 * @param	bPassMask	Byte with bits set to 1 for passes on which to take an action
 *
 * @return	Status as upErr
 */
upErr CRndRobinHelper::SetPassMask( byte bPassMask )
{
	m_passMask = bPassMask;
	return ERR_OK;
}


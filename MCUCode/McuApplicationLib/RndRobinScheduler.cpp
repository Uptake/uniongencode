/**
 * @file	RndRobinScheduler.cpp
 * @par		Package: UGMcu
 * @par		Project: Union Generator
 * @par		Copyright (c) 2017, Uptake Medical Technology, Inc.
 * @author	Glen Sep 17, 2010 Created file
 * @brief	This file contains the round-robin scheduler code, CRndRobinScheduler
 */

// Include Files
#include "RndRobinScheduler.h"
#include "DiagnosticLed.h"
#include "DigitalInputOutput.h"
#include "Supervisor.h"
#include "Watchdog.h"
#include "MemMapIntfc.h"
#include "Temperature.h"
#include "RFGenerator.h"
#include "OneWireMemory.h"
#include "WaterCntl.h"
#include "SbcIntfc.h"
#include "TimeBase.h"
#include "TimerAccess.h"
#include "LogMessage.h"
#include "GeneratorMonitor.h"
#include "TreatmentInformation.h"
#include "HandpiecePrime.h"
#include "CommonOperations.h"
#include "AblationControl.h"
#include "TreatmentRecord.h"
#include "SoundCntl.h"
#include "string.h"
#include "stdio.h"
#include "SelfTest.h"
#include "DeliveryOracle.h"

// External Public Data

// File Scope Macros and Constants

// File Scope Type Definitions (Enums, Structs & Classes)

// File Scope Data

// Singletons without global pointers for sharing the object handle
CTimeBase f_timeBase;
CWatchdog f_watchdog;
CSupervisor f_supervisor;

// Declare singletons for object in round-robin scheduling that have global handles
CDiagnosticLed f_diagLed;
CDigitalInputOutput f_digIO;
CTemperature f_temperature;
CRFGenerator f_rfGenerator;
CWaterCntl f_waterCntl;

// These items are placed into the external memory
XMEM_SECTION
CSbcIntfc f_sbcIntfc;
XMEM_SECTION
CLogMessage f_logMsg;
XMEM_SECTION
COneWireMemory f_oneWire;
XMEM_SECTION
CSoundCntl f_soundCntl;

// Declare singletons of objects that have global handles but are not directly in the round-robin scheme
CGeneratorMonitor f_generatorMonitor;
CTreatmentInformation f_theraInfo;
CCommonOperations f_commonOperations;
CTreatmentRecord f_treatmentRecord;
CSelfTest f_selfTest;
CDeliveryOracle f_deliveryOracle;

// File Scope Functions

// Public Functions

/**
 * @brief	This is the constructor for CRndRobinScheduler. No functionality.
 *
 */
CRndRobinScheduler::CRndRobinScheduler()
{
	int i;

	for ( i = 0; i < PASS_CYCLE_CNT; i++ )
		{
		m_maxLoopTime[ i ] = 0;
		m_minLoopTime[ i ] = 0xFFFF;
		}
	m_loopTimePassCnt = 0;

}

/**
 * @brief	This method initializes all of the task objects by calling InitializeHardware on object as needed.
 *
 * This method also sets the diagnostic LED0 ON to show system operations. When it exists it sets
 * the LED1 diagnostic LED.@par
 * The ooperations also inclde an initial communication attempt with the SBC and kicking the watchdogs.
 *
 * @return   Status as upErr
 */
upErr CRndRobinScheduler::Initialize()
{
	upErr err;

	// Initialize the hardware
	LOG_ERROR_RETURN( err = f_diagLed.InitializeHardware() )
	LOG_ERROR_RETURN( err = f_diagLed.SetLED( DIAG_LED2, true ) )
	LOG_ERROR_RETURN( err = f_timeBase.InitializeHardware() )
	MsDelay( 2000 );

	LOG_ERROR_RETURN( err = f_watchdog.InitializeHardware() )
	LOG_ERROR_RETURN( err = f_digIO.InitializeHardware() )
	LOG_ERROR_RETURN( err = f_rfGenerator.InitializeHardware() )
	LOG_ERROR_RETURN( err = f_oneWire.InitializeHardware() )
	LOG_ERROR_RETURN( err = f_sbcIntfc.InitializeHardware() )
	LOG_ERROR_RETURN( err = f_temperature.InitializeHardware() )
	LOG_ERROR_RETURN( err = f_waterCntl.InitializeHardware() )
	LOG_ERROR_RETURN( err = f_soundCntl.InitializeHardware() )
	LOG_ERROR_RETURN( err = f_watchdog.Execute() )
	LOG_ERROR_RETURN( err = f_diagLed.SetLED( DIAG_LED3, true ) )

	// Log the MCU status register (MCUSR) value to the log.  The MCUSR contains
	// information regarding which reset source caused an MCU reset.

	return err;
}

/**
 * @brief	This method performs the round-robin scheduling.
 *
 * 			The order of task operations and reasoning for that is described
 * 			in the Real-Time Software Architecture document. [Ref 2]@par
 * 			In each loop, the hardware read operations
 * 			are done by calling ReadHardware on each object. Next the Supervisor is executed. Following
 * 			the Supervisor the WriteHardware calls are called to set new values into the hardware.
 * 			Following the hardware Write operations,
 * 			the SBC and 1-Wire memory are written and read. The final step is the watchdog timer operations.@par
 * 			The tick timer is used to limit loop to running only every RND_ROBIN_TICK_CNT ticks.@par
 * 			The execution loop is never exited.
 *
 * @return   Status as upErr
 */
upErr CRndRobinScheduler::Run()
{
	upErr err;
	time tSecs;
	tick tLoopTick;
	tick tLastLoopTick;
	tick tLoopStartTick;
	twobytes tbTickDiff;
	byte bPassIndex;

	tLoopStartTick = 0;
	tLastLoopTick = 0;
	tLoopTick = 0;

	// Start with even timer tick. This makes the simulation output logs more repeatable.
	do
		{
		GetTick( &tLoopStartTick );
		}
	while ( 0 != tLoopStartTick % RND_ROBIN_TICK_CNT );

	// Set to pass 1
	ResetPassFlag();
	f_logMsg.LogMessage( "Starting loop\n" );

	// Loop forever
	while ( 1 )
		{
		// Use second diagnostic LED to track passage of timer seconds and thrid for loop cycles
		// These should then blink at 1/2 Hz and 1/10th Hz
		GetTime( &tSecs );
		g_pDiagnosticLed->SetLED( DIAG_LED3, ( tSecs & 1 ) ? true : false );
		g_pDiagnosticLed->SetLED( DIAG_LED2, ( bPassIndex & 1 ) ? true : false );

		// Get new values from the hardware
		LOG_ERROR_RETURN( err = f_timeBase.ReadFromHardware() )
		// Do first to bump the simulation record to the latest sample
		LOG_ERROR_RETURN( err = f_temperature.ReadFromHardware() )
		LOG_ERROR_RETURN( err = g_pRFGen->ReadFromHardware() )
		LOG_ERROR_RETURN( err = f_waterCntl.ReadFromHardware() )
		LOG_ERROR_RETURN( err = f_digIO.ReadFromHardware() )
		LOG_ERROR_RETURN( err = f_oneWire.ReadFromHardware() )

		// Execute the treatment state machine including priming
		LOG_ERROR_RETURN( err = f_supervisor.Execute() )

		// Write new values, if any, to the hardware
		LOG_ERROR_RETURN( err = f_waterCntl.WriteToHardware() )
		LOG_ERROR_RETURN( err = g_pRFGen->WriteToHardware() )
		LOG_ERROR_RETURN( err = f_digIO.WriteToHardware() )

		// Move log message if possible
		// Send to SBC
		LOG_ERROR_RETURN( err = f_sbcIntfc.WriteToHardware() )

		// Get incoming information from the SBC
		LOG_ERROR_RETURN( err = f_sbcIntfc.ReadFromHardware() )

		// Update info on 1-wire bus
		LOG_ERROR_RETURN( err = f_oneWire.Execute() )

		// Control critical alarm output
		LOG_ERROR_RETURN( err = f_soundCntl.Execute() )
		// Kick the watchdog
		LOG_ERROR_RETURN( err = f_watchdog.Execute() )

		// Rotate pass flag mask
		IncrementPassFlag( &bPassIndex );

		// Wait for the next execution time
		GetTick( &tLoopTick );
		GetTickDiff( tLoopTick, tLoopStartTick, &tbTickDiff );

		// Track and log loop timing if not simulating
		if ( 0 == g_bIsSimulated )
			PerformLoopTiming( tbTickDiff, bPassIndex );

		// Report an error if the execution toERR_OK longer than the target loop time
		if ( tbTickDiff > RND_ROBIN_TICK_CNT )
			{
			LOG_ERROR_RETURN( ERR_RND_ROBIN_OVERRUN );
			f_supervisor.SetOverrunFlag();
			}

		// Wait for next loop time to occur
		tLastLoopTick = tLoopTick;
		while ( tbTickDiff < RND_ROBIN_TICK_CNT )
			{
			GetTick( &tLoopTick );
			if ( (byte) tLoopTick != (byte) tLastLoopTick )
				{
				GetTickDiff( tLoopTick, tLoopStartTick, &tbTickDiff );
				tLastLoopTick = tLoopTick;
				}
			}

		// Track start tick of loop
		tLoopStartTick = tLoopTick;
		} // While
	return ERR_OK;
}

// Private Class Functions

/**
 * @brief	This method loERR_OKs for the longest and shortest execution of the roundrobin loop. Each
 * 			pass through the loop may execute different sub-tasks so the passes are loERR_OKed at
 * 			separately
 *
 * @param	tbTickDiff The time taken to run the loop as a twobytes
 * @param	bPassIndex The index of the loop type
 *
 * @return   Status as upErr
 */
upErr CRndRobinScheduler::PerformLoopTiming( twobytes tbTickDiff, byte bPassIndex )
{

#define LOOP_TIMING_LENGTH	(80)
	// Find max and min for each loop
	if ( m_minLoopTime[ bPassIndex ] > tbTickDiff )
		m_minLoopTime[ bPassIndex ] = tbTickDiff;
	if ( m_maxLoopTime[ bPassIndex ] < tbTickDiff )
		m_maxLoopTime[ bPassIndex ] = tbTickDiff;

#ifndef SIM_OUTPUT_CAPTURE
	char sMsg[ LOOP_TIMING_LENGTH ];
	if ( m_loopTimePassCnt++ >= LOOP_TIMING_LOG_PERIOD )
		{
		byte iLen = 0;
		m_loopTimePassCnt = 0;
		strcpy( sMsg, "Pass:" );
		iLen = strlen( sMsg );
		for ( int i = 0; i < PASS_CYCLE_CNT; i++ )
			{
			snprintf( sMsg + iLen, LOOP_TIMING_LENGTH - iLen - 1, " %d:%u %u", i, m_maxLoopTime[ i ],
			        m_minLoopTime[ i ] );
			iLen = strlen( sMsg );
			m_maxLoopTime[ i ] = 0;
			m_minLoopTime[ i ] = 0xFFFF;
			}
		strcpy( sMsg + iLen, "\n" );
// TODO Use or not		g_pLogMessage->LogMessage( sMsg );
		}
#endif

	return ERR_OK;
}


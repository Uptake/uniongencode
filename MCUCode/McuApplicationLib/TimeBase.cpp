/**
 * @file	TimeBase.cpp
 * @par		Package: UGMcu
 * @par		Project: Union Generator
 * @par		Copyright (c) 2017, Uptake Medical Technology, Inc.
 * @author	Glen Sep 23, 2010 Created file
 * @brief	This file contains the time base code, CTimeBase and CTimerAccess
 */

// Include Files
#include "TimeBase.h"

// External Public Data

// File Scope Macros and Constants
#define ENABLE_OCR3_INTERRUPT		//!< Enable interrupt on A compare match
#define DISABLE_OCR3_INTERRUPT		//!< Disable interrupt on A compare match
// File Scope Type Definitions (Enums, Structs & Classes)

// File Scope Data
volatile tick f_tTick;
time f_tClock;

// File Scope Functions
/**
 * @brief	This function is the interrupt vector for the tick timer. It is executed based
 * 			on a compare A match on timer #1. It increments the tick by one on each call.
 *
 * @return   None
 */

// Public Functions

/**
 * @brief	This is the constructor for TimeBase. It clears the tick timer and the seconds count
 *
 */
CTimeBase::CTimeBase()
{
	f_tClock = 0;
	ClearTick();
}

/**
 * @brief	This method initializes the Timer1 to produce a periodic interrupt that allows
 * 			counting of ticks.
 *
 * 			Timer3 is used along with the compare A circuit on it. The time
 * 			period is set using macros for the input oscillator or crystal and the desired tick
 * 			period. The counter to Timer3 is cleared when a match using CompA is made. The interrupt
 * 			just needs to increment a counter on each call. It'll wrap after a few months but
 * 			that's rare and ERR_OK as it's handled in the application code and time difference
 * 			calculations.
 *
 * @return   Status as upErr
 */
upErr CTimeBase::InitializeHardware()
{
//	twobytes tbTemp;
	//byte bTemp;

	DISABLE_OCR3_INTERRUPT

	// Enable Interrupt
	ENABLE_OCR3_INTERRUPT

	return ERR_OK;
}

/**
 * @brief	This method is called from the round robin loop and increments the seconds timer.
 *
 * 			It uses a record of when the last second
 * 			elapsed to identify when the next second has passed. Interrupts are locked during
 * 			this evaluation
 *
 * @return   Status as upErr
 */
upErr CTimeBase::ReadFromHardware()
{
	DISABLE_OCR3_INTERRUPT

	// If f_tTick has wrapped then reset m_lastSecondTick to zero
	// Might lose a second but it's been running for 0xFFFFFFFF ticks
	if ( m_lastSecondTick > f_tTick + TICK_CLOCK_RATE )
		m_lastSecondTick = 0;

	// Increment clock for each second (TICK_CLOCK_RATE) since the last update was made
	// The while should never loop in normal operation as this is called on each round-robin cycle.
	// May be needed if long delay has been executed in the code at start-up
	while ( m_lastSecondTick < f_tTick )
		{
		m_lastSecondTick += TICK_CLOCK_RATE;
		f_tClock++;
		}

	ENABLE_OCR3_INTERRUPT
	return ERR_OK;
}

/**
 * @brief	This method clears the second count and tick count to zero.
 *
 * 	Interrupts are locked during this operation
 *
 * @return   Status as upErr
 */
upErr CTimeBase::ClearTick()
{
	f_tTick = 0;
	m_lastSecondTick = TICK_CLOCK_RATE; // Ticks right away
	return ERR_OK;
}

/**
 * @brief	This method set the tick timer to a specific value.
 *
 * 			It is provided to enhance unit testing only
 *
 * @param	tTick New tick value as tick
 *
 * @return   Status as upErr
 */
upErr CTimeBase::SetTick( tick tTick )
{
	DISABLE_OCR3_INTERRUPT

	f_tTick = tTick;

	ENABLE_OCR3_INTERRUPT

	return ERR_OK;
}

//
//
// CTimerAccess class definitions
//
//

/**
 * @brief	This is the constructor for CTimerAccess.
 *
 * 			It has no functionality.
 *
 */
CTimerAccess::CTimerAccess()
{
}

/**
 * @brief	This method returns the latest tick count.
 *
 * 			Interrupts are locked during this operation
 *
 * @param	ptTick A pointer to a tick to receive the count
 *
 * @return   Status as upErr
 */
upErr CTimerAccess::GetTick( tick *ptTick )
{
	DISABLE_OCR3_INTERRUPT
	*ptTick = f_tTick;
	ENABLE_OCR3_INTERRUPT
	return ERR_OK;
}

/**
 * @brief	This method gets the latest count of seconds.
 *
 * 			The count does not have an explicit
 * 			start point or basis. It will be set from the SBC and will be some date that the SBC
 * 			understands. The MCU code will accept whatever second count is provided and then increment it
 * 			every second that passes.
 *
 * @param	ptClock	A pointer to a time to receive the count of seconds
 *
 * @return   Status as upErr
 */
upErr CTimerAccess::GetTime( time *ptClock )
{
	*ptClock = f_tClock;
	return ERR_OK;
}

/**
 * @brief	This method sets the base number for the seconds timer.
 *
 * 			It will be set to zero by the
 * 			MCU and will later be set to a starting point that works for the SBC software. For example,
 * 			seconds since midnight on Jan 1, 1970 as for the typical "epoch" time base.
 *
 * @param	tClock	The count in seconds to seed the seconds timer
 *
 * @return   Status as upErr
 */
upErr CTimerAccess::SetTime( time tClock )
{
	f_tClock = tClock;
	return ERR_OK;
}

/**
 * @brief	This method computes the difference between an "old" tick time and the current tick as a two byte value.
 *
 * 			It returns true if the tick timer has wrapped and new was smaller than old. In this case the
 * 			time difference include one wrap increment although multiple could have occurred.
 * 			This method returns an unsigned two-byte value as, with a 1ms tick, it is unlikely that
 * 			more than 65536 ticks (65 seconds) have elapsed during a treatment.
 *
 * @param	tOld	The previously-sampled tick
 * @param	ptbDiff	A twobytes pointer to receive the difference between tOld and the current tick clock value
 *
 * @return   true if timer has wrapped and false if not
 */
bool_c CTimerAccess::GetTickElapsed( tick tOld, twobytes * ptbDiff )
{
	tick tNow;
	GetTick( &tNow );
	return GetTickDiff( tNow, tOld, ptbDiff );
}

/**
 * @brief	This method computes the difference between an "old" tick time and the current tick as a two byte value.
 *
 * 			It returns true if the tick timer has wrapped and new was smaller than old. In this case the
 * 			time difference include one wrap increment although multiple could have occurred.
 * 			This method returns a tick value as, with a 1ms tick.
 *
 * @param	tOld	The previously-sampled tick
 * @param	ptDiff	A tick pointer to receive the difference between tOld and the current tick clock value
 *
 * @return   true if timer has wrapped and false if not
 */
bool_c CTimerAccess::GetTickElapsed( tick tOld, tick * ptDiff )
{
	tick tNow;
	GetTick( &tNow );
	return GetTickDiff( tNow, tOld, ptDiff );
}

/**
 * @brief	This method computes the difference between two tick timers as a two byte value.
 *
 * 			It returns true if the tick timer has wrapped and new was smaller than old. In this case the
 * 			time difference include one wrap increment although multiple could have occurred.
 * 			This method returns an unsigned two-byte value as, with a 1ms tick, it is unlikely that
 * 			more than 65536 ticks (65 seconds) have elapsed during a treatment.
 *
 * @param	tNew	A tick that is more recent tick
 * @param	tOld	The tick that older or earlier in time
 * @param	ptbDiff	A twobytes pointer to receive the difference
 *
 * @return   true if timer has wrapped and false if not
 */
bool_c CTimerAccess::GetTickDiff( tick tNew, tick tOld, twobytes *ptbDiff )
{
	bool_c bWrap;
	tick tTickDiff;

	bWrap = GetTickDiff( tNew, tOld, &tTickDiff );
	*ptbDiff = (twobytes) tTickDiff;

	return bWrap;
}

/**
 * @brief	This method computes the difference between two tick timers as a tick type.
 *
 * 			It returns true if the tick timer has wrapped and new was smaller than old. In this case the
 * 			time difference include one wrap increment although multiple could have occurred.
 *
 * @param	tNew	A tick that is more recent tick
 * @param	tOld	The tick that older or earlier in time
 * @param	ptDiff	A tick pointer to receive the difference
 *
 * @return   true if timer has wrapped and false if not
 */
bool_c CTimerAccess::GetTickDiff( tick tNew, tick tOld, tick *ptDiff )
{
	bool_c bWrap = false;

	if ( tOld > tNew )
		{
		bWrap = true;
		*ptDiff = tNew + ( TICK_MAX - tOld ) + 1;
		}
	else
		{
		*ptDiff = ( tNew - tOld );
		}

	return bWrap;
}

/**
 * @brief	This method performs a delay and only returns after a number of milliseconds has elapsed.
 *
 * 			The maximum time limit is 64k tick counts. This may be 65 seconds if the ticks are 1 millisecond apart.
 *
 * @param	tbMsDelay Time to wait in milliseconds as a twobytes
 *
 * @return   Status as upErr
 */
upErr CTimerAccess::MsDelay( twobytes tbMsDelay )
{
	tick tFirst;
	twobytes tbDiff;
	twobytes tDelay;

	GetTick( &tFirst );
	tDelay = MS_TO_TICKS( tbMsDelay );

	do
		{
		GetTickElapsed( tFirst, &tbDiff );
		}
	while ( tbDiff < tDelay );

	return ERR_OK;
}

// Private Class Functions

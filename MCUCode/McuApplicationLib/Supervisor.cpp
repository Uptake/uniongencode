/**
 * @file	Supervisor.cpp
 * @par		Package: UGMcu
 * @par		Project: Union Generator
 * @par		Copyright (c) 2017, Uptake Medical Technology, Inc.
 * @author	Glen Sep 15, 2010 Created file
 * @brief	This file contains the code for the operational supervisor, CSupervisor
 */

// Include Files
#include "Supervisor.h"
#include "SbcIntfc.h"
#include "DiagnosticLed.h"
#include "DigitalInputOutput.h"
#include "RFGenerator.h"
#include "Temperature.h"
#include "WaterCntl.h"
#include "OneWireMemory.h"
#include "TreatmentInformation.h"
#include "LogMessage.h"
#include "GeneratorMonitor.h"
#include "CommonOperations.h"
#include "TreatmentRecord.h"
#include "SelfTest.h"
#include "SoundCntl.h"
#include "string.h"
#include "stdio.h"
#include "DeliveryOracle.h"

// External Public Data

// File Scope Macros and Constants

// File Scope Type Definitions (Enums, Structs & Classes)
#define UPPER_LIMIT_IDLE_OFFSET 	( 5 )		// Offset in degrees C to subtract from Upper Coil Idle Temperature Limit

// File Scope Data

// File Scope Functions

// Public Functions

/**
 * @brief	This is the constructor for CSupervisor. Sets member variables to zero.
 *
 */
CSupervisor::CSupervisor()
{
	SetInitValues();
}

/**
 * @brief	This method checks that a handpiece is attached and that it is viable for another treatment
 *
 * @return   Status as upErr
 */
upErr CSupervisor::CheckHandpiece()
{
	upErr eErr = ERR_OK;
	bool_c bPresent = false;
	upTreatmentCode eTherapyCode;
	time tFirstUseTime = 0;
	byte bOwRomId[ OW_ROM_SIZE ];
	twobytes tbVal;

	// See if one is attached
	g_pOneWireMemory->CheckForHandpiece( &bPresent );
	if ( false == bPresent )
		{
		// reset all DDevice states
		m_eDDeviceState = DD_STATE_DISCONNECTED;
		// don't reset prime state
		return ERR_NO_DDEVICE;
		}

	// Cannot continue if DDevice is disabled due to an error
	if ( DD_STATE_DISABLED == m_eDDeviceState )
		return ERR_DDEVICE_DISABLED;

	if ( DD_STATE_ATTACHED != m_eDDeviceState )
		{
		m_eDDeviceState = DD_STATE_ATTACHED;

		// Check therapy code
		g_pOneWireMemory->GetTherapyCode( &eTherapyCode );
		LOG_ERROR_RETURN( eErr = g_pTreatmentInfo->checkTreatmentCode( eTherapyCode ) )
		if ( ERR_OK != eErr )
			{
			g_pLogMessage->LogErrorMessageInt( eErr, "Bad code ", eTherapyCode );
			return ERR_DDEVICE_UNKNOWN;
			}
		// Set the model type
		g_pTreatmentInfo->setTreatmentCode( eTherapyCode );

		// First connection so set current time into 1-wire
		LOG_ERROR_RETURN( eErr = g_pOneWireMemory->GetFirstUseTime( &tFirstUseTime ) )
		if ( 0 == tFirstUseTime )
			{
			time tFirstUseTmp = 0;
			LOG_ERROR_RETURN( eErr = GetTime( &tFirstUseTmp ) )
			// Do not stamp the 1wire first use time until the sbc time has been set
			if ( 0 != tFirstUseTmp )
				{
				tFirstUseTime = tFirstUseTmp;
				LOG_ERROR_RETURN( g_pOneWireMemory->SetFirstUseTime( tFirstUseTime ) )
				}
			}

		// update the therapy information class and sbc setup message with model defaults
		// Note: The custom values are only reset when a "good" delivery device
		//		 is connected that's ROM ID differs from the last "good" device connected.
		g_pSbcIntfc->ResetCustomSetup();
		}

	// Check Treatment count
	g_pOneWireMemory->GetFullTreatmentCount( &tbVal );

#ifdef ALLOW_EMC_TREATMENT_CODE
	if ( TREATMENT_EMC != eTherapyCode )
#endif
		{
		bool_c bcLock = false;

		if ( g_pTreatmentInfo->getDeliveryCount() <= tbVal )
			{
			g_pLogMessage->LogErrorMessageInt( eErr, "No treatments left", (int) tbVal );
			return ERR_DDEVICE_EXPIRED_TREAT;
			}

		// Check Therapy time
		unsignedfourbytes ulTherapy;
		g_pOneWireMemory->GetTherapyTime( &ulTherapy );
		// Ttherapy scale is in seconds, ulTherapy is in milliseconds
		if ( SEC_TO_MS( g_pTreatmentInfo->getTvapor() ) <= ulTherapy )
			{
			g_pLogMessage->LogErrorMessage( eErr, "No treatment time left\n" );
			return ERR_DDEVICE_EXPIRED_TIME;
			}

		// Check for a device that has been disabled by loERR_OKing for a locked treatment bank
		g_pOneWireMemory->GetLockState( &bcLock );
		if ( bcLock )
			{
			g_pLogMessage->LogErrorMessage( eErr, "The device is locked\n" );
			return ERR_DDEVICE_INVALID;
			}
		}

	// Get serial number and use to determine delivery device change
	g_pOneWireMemory->GetRomId( bOwRomId );

	// If the ROM IDs are the same, the value returned is 0
	if ( 0 != memcmp( (void*) bOwRomId, (void*) m_lastOwRomId, OW_ROM_SIZE ) )
		{

		// Changed handpiece. Retract rod and invalidate prime
		g_pWaterCntl->StartRetractingPumpRod();
		SetCatheterPrimedFlag( false );

		// It's a new device, so reset saved states and store the new ROM ID locally
		memcpy( (void*) m_lastOwRomId, (void*) bOwRomId, OW_ROM_SIZE );

		// Seed record for this connection
		g_pTreatmentRecord->StartNewRecord();
		}

	return ERR_OK;
}

/**
 * @brief	This method assembles the current operating conditions and sends it to the SBC. The operating
 * 			conditions include all the numerical hardware readings, the input and output states, the
 * 			elapsed time and the operating state
 *
 * @return   Status as upErr
 */
upErr CSupervisor::SetOperatingConditions()
{
	upErr eErr;
	SOpconMsg * operCond;
	upInterlockFlag intTest;
	tick tNow;
	GetTick( &tNow );

	// get a pointer to the message contained within the SbcIntfc
	LOG_ERROR_RETURN( eErr = g_pSbcIntfc->GetMessage( (void** )&operCond, MSG_TYPE_OPCOND ) );

	// set the operating conditions
	g_pGeneratorMonitor->CheckInterlocks( false, &intTest, false );

	operCond->connMask = (byte) intTest;
	operCond->stbUnused0 = 0;
	operCond->stbUnused1 = (signedtwobytes) m_operState; /// TODO: Pass as an integer
	operCond->unusedState = (byte) 0;

	g_pTemperature->GetThermoTemp( THERMO_COIL, &( operCond->coilTemp ) );
	g_pTemperature->GetThermoTemp( THERMO_OUTLET, &( operCond->outletTemp ) );
	g_pTemperature->GetThermoTemp( THERMO_INTERIOR, &( operCond->intTemp ) );
	g_pTemperature->GetFilteredThermoTemp( THERMO_COIL, &( operCond->firCoilTemp ) );
	g_pTemperature->GetFilteredThermoTemp( THERMO_OUTLET, &( operCond->firOutletTemp ) );

	g_pRFGen->GetPowerOutput( &( operCond->tbRfPower ) );
#ifdef ENABLE_RDO_CURRENT_READ
	g_pRFGen->GetCurrentOutput( (twobytes*) &( operCond->stbUnused0 ) );
#endif
	g_pRFGen->GetTemperature( &( operCond->iRfTemperature ) );
	g_pRFGen->GetStatus( &( operCond->tbRfStatus ) );
	g_pRFGen->GetError( &( operCond->tbRfError ) );

	g_pWaterCntl->GetAppliedVelocity( &( operCond->waterRate ) );
	g_pWaterCntl->GetStepperPosition( &( operCond->encoderPos ), &( operCond->motorVelocity ) );

	g_pDigitalInputOutput->GetAllInput( &( operCond->digInput ) );
	g_pDigitalInputOutput->GetAllOutput( &( operCond->digOutput ) );

	g_pLogMessage->GetLogCount( &( operCond->logMsgs ) );
	g_pOneWireMemory->GetFullTreatmentCount( &( operCond->treatCnt ) );
	// user alert messages updated via SbcIntfc as well
	g_pLogMessage->GetUserMsgCount( &( operCond->userMsgs ) );

	operCond->runTicks = (twobytes) tNow;
	operCond->operState = m_operState;
	operCond->operSubState = m_ePendingSubState;
	operCond->maxTreatments = g_pTreatmentInfo->getDeliveryCount();
	operCond->progress = m_bProgress;

	if ( DD_STATE_ATTACHED == m_eDDeviceState )
		{
		unsignedfourbytes ulTherapyTime = 0;
		g_pOneWireMemory->GetTherapyTime( &ulTherapyTime );
		operCond->ddTreatRemaining = operCond->maxTreatments - operCond->treatCnt;
		operCond->ddTimeRemaining = SEC_TO_MS( g_pTreatmentInfo->getTvapor() ) - ulTherapyTime;
		}
	else
		{
		operCond->ddTreatRemaining = 0;
		operCond->ddTimeRemaining = 0;
		}

	return eErr;
}

/**
 * @brief	This method tests for either the trigger to be pulled or the handpiece to be removed.
 *
 * @param pbTrig A pointer to a bool_c to receive the trigger status. True => closed
 *
 * @return   Status as upErr
 */
upErr CSupervisor::WaitingForTrigger( bool_c * pbTrig )
{
	bool_c bTrig;
	upInterlockFlag eIntlck;
	upErr eErr;

	if ( 0 == pbTrig )
		return ERR_BAD_PARAM;

	*pbTrig = false;

	// Check RF power behavior in Connected and Ready states
	LOG_ERROR_RETURN( eErr = g_pGeneratorMonitor->CheckRF() )
	if ( ERR_OK != eErr )
		{
		return eErr;
		}

	// Check handpiece status
	LOG_ERROR_RETURN( eErr = CheckHandpiece() )
	if ( ERR_OK != eErr )
		{
		return eErr;
		}

	// Check Interlocks
	LOG_ERROR_RETURN( eErr = g_pGeneratorMonitor->CheckInterlocks( true, &eIntlck, false ) )
	if ( ERR_OK != eErr )
		{
		return eErr;
		}

	// Need all for priming but syringe is not needed for READY states
	upInterlockFlag eInterlockNeeded = INTLCK_ALL;
	g_pGeneratorMonitor->GetNeededInterlocks( m_operState, &eInterlockNeeded );
	if ( eInterlockNeeded != ( eInterlockNeeded & eIntlck ) )
		{
		LOG_ERROR_RETURN( eErr = ERR_INTERLOCK_MISSING )
		return eErr;
		}

	// Check for pump retraction request
	bool_c bRet;
	if ( g_pWaterCntl->GetRetracted( &bRet ) )
		{
		SetOperState( OPER_STATE_RELEASE );
		return ERR_OK;
		}

	LOG_ERROR_RETURN( g_pDigitalInputOutput->GetInput( DIG_IN_TRIGGER, &bTrig ) )

#ifdef ALLOW_EMC_TREATMENT_CODE
	// For THERPAY_EMC continue with automatic starting until count reached.
	upTreatmentCode eTherapyCode;
	g_pOneWireMemory->GetTherapyCode( &eTherapyCode );
	if ( TREATMENT_EMC == eTherapyCode && ( OPER_STATE_READY == m_operState || OPER_STATE_PENDING == m_operState ) )
		{
		bool_c bExt = false;
		g_pDigitalInputOutput->GetInput( DIG_IN_MOTOR_EXTEND, &bExt );
		if ( 0 < m_emcTestCycle && m_emcTestCycle < g_pTreatmentInfo->getEmcCycles() && !bExt )
			{
			tick tTick;
			twobytes tDiff;
			GetTick( &tTick );
			if ( 0 == m_lastEnd )
			m_lastEnd = tTick;
			GetTickDiff( tTick, m_lastEnd, &tDiff );
			bTrig = ( TICKS_TO_MS( tDiff ) < g_pTreatmentInfo->getTrest() ? false : true );
			}
		else
			{
			m_emcTestCycle = 0;
			}
		}
#endif

	// LoERR_OK for going ON after being off
	if ( bTrig )
		{
		if ( false == m_lastTrig )
			{
			tick tDiff;
			GetTickElapsed( m_lastEnd, &tDiff );

			// Wait Tdeb debounce time before going back ON
			if ( TICKS_TO_MS( tDiff ) >= g_pTreatmentInfo->getTdeb() )
				{
				*pbTrig = true;
#ifdef ALLOW_EMC_TREATMENT_CODE
				// May want to wait out Trest period
				if ( TREATMENT_EMC == eTherapyCode )
					{
					if ( OPER_STATE_READY == m_operState || m_operState == OPER_STATE_PENDING)
					m_emcTestCycle++;
					}
#endif
				}
			}
		}
	else
		m_lastTrig = bTrig;

	return eErr;
}

/**
 * @brief	This method handles the "Test" state.
 *
 * @return   Status as upErr
 */
upErr CSupervisor::TestState()
{
	upErr eErr;
	upGenericState eState;

	// Retract if requested
	bool_c retractBool;
	g_pWaterCntl->PerformRodRetraction( &retractBool );

	// Perform test and get progress
	LOG_ERROR_RETURN( eErr = g_pSelfTest->SystemTest( &eState ) )
	if ( g_tbImageCrc )
		g_pSelfTest->GetProgress( &m_bProgress );

	switch ( eErr )
		{
		case ERR_OK:
			if ( GEN_STATE_END == eState )
				{
				SetOperState( OPER_STATE_IDLE );
				}
			break;

			// Bad self test means generator does not function
		default:
			SetOperState( OPER_STATE_ERROR );
			break;
		}

	return eErr;
}

/**
 * @brief	This method is the idle state of the MCU. It waits for the handpiece to be fully connected.
 * 			It powers-off the RF AC supply as that isn't needed when the handpiece is disconnected.
 *
 * @return   Status as upErr
 */
upErr CSupervisor::IdleState()
{
	upErr eErr;
	upLEDState eLed;
	upInterlockFlag eIntlck;

	// Set LED state if not showing WARN
	g_pDigitalInputOutput->GetLEDState( &eLed );
	if ( LED_WARN != eLed )
		g_pDigitalInputOutput->SetLEDState( LED_POWER );

	// Over temp, SBC and RF generator checks
	LOG_ERROR_RETURN( eErr = CheckCriticalMonitoring() )
	if ( ERR_OK != eErr )
		{
		SetOperState( OPER_STATE_ERROR );
		return eErr;
		}

	// Check interlocks. If all made then go to Connected or Ready as appropriate
	// Only turn AC for RF ON if timer for it to reset has elapsed
	tick tDiff;
	GetTickElapsed( m_lastEnd, &tDiff );

	// Check for extend bit and retraction
	if ( ERR_OK != ( eErr = CheckSyringe() ) )
		{
		SetOperState( OPER_STATE_RELEASE );
		return eErr;
		}

	bool_c bRfOn = ( tDiff > g_pRFGen->GetACOffTicks() ) ? true : false;

	LOG_ERROR_RETURN( eErr = g_pGeneratorMonitor->CheckInterlocks( false, &eIntlck, bRfOn ) )

	// SBC version verification should occur once after the SBC interlock has been established
	if ( 0 != ( eIntlck & INTLCK_SBC ) && !m_bVersionsVerified )
		{

// Reset MCUSR once SBC communication is up so previous status will be reported
		LOG_ERROR_RETURN( eErr = g_pComOp->VerifyCodeVersions() )
		if ( ERR_OK != eErr )
			{
			SetOperState( OPER_STATE_ERROR );
			return eErr;
			}
		// Enable 1-wire monitor only when versions have been verified
		m_bVersionsVerified = true;
		g_pOneWireMemory->EnableOwMonitor( true );
		}

	// Check status of handpiece
	eErr = CheckHandpiece();
	switch ( eErr )
		{
		case ERR_OK:
			if ( INTLCK_ALL == eIntlck )
				{
				// Set LED off so new state is shown in LEDs
				if ( true == m_bCatheterPrimed )
					{
					g_pDigitalInputOutput->SetLEDState( LED_READY );
					SetOperState( OPER_STATE_PENDING );
					}
				else
					{
					g_pDigitalInputOutput->SetLEDState( LED_POWER );
					SetOperState( OPER_STATE_CNCTD );
					}
				}

			break;

		case ERR_DDEVICE_INVALID:
		case ERR_DDEVICE_UNKNOWN:
		case ERR_THERMO_ERROR:
			DisableDeliveryDevice( eErr, false );
			break;

			// Play the disabled tone
		case ERR_DDEVICE_EXPIRED_TREAT:
		case ERR_DDEVICE_EXPIRED_TIME:
			DisableDeliveryDevice( eErr, true );
			break;

			// This handles the conditions of there being no delivery device
			// or the device has been disabled by the supervisor due to an error
		case ERR_NO_DDEVICE:
		case ERR_DDEVICE_DISABLED:
			eErr = ERR_OK;
			break;

		default:
			break;
		}

	return eErr;
}

/**
 * @brief	This method handles the "Connected" state
 *
 * @return   Status as upErr
 */
upErr CSupervisor::ConnectedState()
{
	upErr eErr;
	bool_c bTrig = false;
	upLEDState eLed;

	// Set LED state if not showing WARN
	g_pDigitalInputOutput->GetLEDState( &eLed );
	if ( LED_WARN != eLed )
		g_pDigitalInputOutput->SetLEDState( LED_POWER );

	// Check for extend bit and retraction
	if ( ERR_OK != ( eErr = CheckSyringe() ) )
		{
		SetOperState( OPER_STATE_RELEASE );
		return eErr;
		}

	// Over temp, SBC and RF generator checks
	LOG_ERROR_RETURN( eErr = CheckCriticalMonitoring() )
	if ( ERR_OK != eErr )
		{
		SetOperState( OPER_STATE_ERROR );
		return eErr;
		}

	// Check whether the RF generator should be reset
	if ( g_pRFGen->ResetRequired() )
		{
		SetOperState( OPER_STATE_IDLE );
		return ERR_RF_GEN_RESET;
		}

	switch ( eErr = WaitingForTrigger( &bTrig ) )
		{
		case ERR_OK:
			if ( bTrig )
				{
				m_lastTrig = true;
				upSbcState eSbcState;
				LOG_ERROR_RETURN( eErr = g_pSbcIntfc->ReadSbcState( &eSbcState ) )
				if ( ERR_OK == eErr )
					{
					// Start priming if showing the connected screen or one of the technician diagnostics
					if ( SBC_STATE_TECH_DIAG == eSbcState || SBC_STATE_PRIME == eSbcState )
						{
						SetOperState( OPER_STATE_HAND_PRIMING );
						g_pDigitalInputOutput->SetLEDState( LED_PRIME );
						LOG_ERROR_RETURN( eErr = m_handpiecePrime.InitializePrime() )
						m_handpiecePrime.GetProgress( &m_bProgress );
						}
					else
						{
						LOG_ERROR_RETURN( eErr = ERR_SBC_STATE_WRONG )
						}
					}
				}
			break;

			// Remap thermo disconnect to full error
		case ERR_THERMO_INTERLOCK_MISSING:
			eErr = ERR_THERMO_ERROR;
			DisableDeliveryDevice( eErr, false );
			break;

		case ERR_THERMO_ERROR:
		case ERR_DDEVICE_INVALID:
		case ERR_DDEVICE_UNKNOWN:
			DisableDeliveryDevice( eErr, false );
			break;

		case ERR_DDEVICE_EXPIRED_TREAT:
		case ERR_DDEVICE_EXPIRED_TIME:
			DisableDeliveryDevice( eErr, true );
			break;

			// RF Generator needs to be reset
		case ERR_RF_GEN_RESET:
			SetOperState( OPER_STATE_IDLE );
			break;

			// RF Generator has failed. Go to terminate
		case ERR_RF_GEN_POWER_LEVEL:
		case ERR_EXCESSIVE_TEMP:
		case ERR_RF_GEN_NOT_FOUND:
		case ERR_RF_GEN_COMM_FATAL:
			SetOperState( OPER_STATE_ERROR );
			break;

			// Rod fully extended
		case ERR_PUMP_AT_LIMIT:
			g_pWaterCntl->StartRetractingPumpRod();
			SetOperState( OPER_STATE_RELEASE );
			break;

			// Disconnected something
		case ERR_NO_DDEVICE:
		case ERR_NOT_READY:
		case ERR_INTERLOCK_MISSING:
			SetOperState( OPER_STATE_IDLE );
			break;

		default:
			LOG_ERROR_RETURN( eErr )
			SetOperState( OPER_STATE_IDLE );
			break;
		}

	return eErr;
}

/**
 * @brief	This method handles the "Priming" state
 *
 * @return   Status as upErr
 */
upErr CSupervisor::HandpiecePrimingState()
{
	upErr eErr;

	// Over temp, SBC and RF generator checks
	LOG_ERROR_RETURN( eErr = CheckCriticalMonitoring() )
	if ( ERR_OK != eErr )
		{
		SetOperState( OPER_STATE_ERROR );
		return eErr;
		}

	// Don't perform an RFResetCheck() here as you may lose
	// state information in the priming state machine.
	// RfResetCheck() is called in PerformPrime() so catch
	// it in the switch statement instead.
	LOG_ERROR_RETURN( eErr = m_handpiecePrime.PerformPrime() )
	switch ( eErr )
		{
		// Push to Ready if primed
		case ERR_OK:
			if ( CHandpiecePrime::PRIME_SUCCESS == m_handpiecePrime.GetState() )
				{
				SetOperState( OPER_STATE_HAND_PRIMED );
				GetTick( &m_lastEnd );
				}
			else if ( CHandpiecePrime::PRIME_FAILED == m_handpiecePrime.GetState() )
				{
				SetOperState( OPER_STATE_CNCTD );
				SetHandpiecePrimedFlag( false );
				}
			break;

		case ERR_RF_GEN_RESET:
			SetOperState( OPER_STATE_IDLE );
			break;

			// Disable delivery device for these errors
		case ERR_THERMO_ERROR:
		case ERR_DDEVICE_INVALID:
		case ERR_DDEVICE_UNKNOWN:
		case ERR_DDEVICE_COIL:
			DisableDeliveryDevice( eErr, false );
			break;

			// Play the disabled tone
		case ERR_THERMO_UNDER_TEMP:
		case ERR_DDEVICE_EXPIRED_TREAT:
		case ERR_DDEVICE_EXPIRED_TIME:
			DisableDeliveryDevice( eErr, true );
			break;

			// Go to terminate state for the following errors
		case ERR_EXCESSIVE_TEMP:
		case ERR_GENERATOR_OVER_TEMP:
		case ERR_IPC_CRITICAL_TIMEOUT:
		case ERR_IPC_CONNECTION_TIMEOUT:
		case ERR_RF_GEN_POWER_LEVEL:
		case ERR_INTERNAL_FAULT:
		case ERR_RF_GEN_NOT_FOUND:
		case ERR_RF_GEN_COMM_FATAL:
			SetOperState( OPER_STATE_ERROR );
			SetHandpiecePrimedFlag( false );
			break;

			// Rod fully extended
		case ERR_PUMP_AT_LIMIT:
			SetOperState( OPER_STATE_RELEASE );
			g_pWaterCntl->StartRetractingPumpRod();
			break;

			// Not an error but warn regardless
		case ERR_TRIGGER_OFF:
			GetTick( &m_lastEnd );
			SetOperState( OPER_STATE_CNCTD );
			break;

			// Over temp persist at start of priming
		case ERR_THERMO_OVER_TEMP_PERSIST:
			LOG_ERROR_RETURN( eErr = ERR_THERMO_OVER_TEMP )
			SetOperState( OPER_STATE_CNCTD );
			break;

		default:
			GetTick( &m_lastEnd );
			SetOperState( OPER_STATE_CNCTD );
			LOG_ERROR_RETURN( eErr )
			break;
		}

	m_handpiecePrime.GetProgress( &m_bProgress );
	return eErr;
}

/**
 * @brief	This method controls the MCU while in the Handpiece Primed state. It is waiting
 * 			for the user to confirm that the handpiece prime was successful.
 *
 * @return   Status as upErr
 */
upErr CSupervisor::HandpiecePrimed()
{
	upErr eErr = ERR_OK;
	bool_c bTrig = false;
	upLEDState eLed;

	//* TODO Remove for final product
	int iValue;
	iValue = g_pTreatmentInfo->getCustom8();
	if ( 2 == iValue )
		{
		SetHandpiecePrimedFlag( true );
		}
	else if ( 1 == iValue )
		{
		SetHandpiecePrimedFlag( false );
		LOG_ERROR_RETURN( eErr = SetOperState( OPER_STATE_CNCTD ) )
		}

	// Get last command from SBC and check for pass/fail of prime
	upSbcCmd eSbcCmd = SBC_CMD_UNDEFINED;
	LOG_ERROR_RETURN( eErr = g_pSbcIntfc->GetLastCmnd( &eSbcCmd ) )

	if ( SBC_CMD_PRIMED == eSbcCmd )
		{
		g_pSbcIntfc->SetLastCmnd( SBC_CMD_UNDEFINED );
		SetHandpiecePrimedFlag( true );
		}
	else if ( SBC_CMD_NOT_PRIMED == eSbcCmd )
		{
		g_pSbcIntfc->SetLastCmnd( SBC_CMD_UNDEFINED );
		SetHandpiecePrimedFlag( false );
		LOG_ERROR_RETURN( eErr = SetOperState( OPER_STATE_CNCTD ) )
		}

	// Set LED state if not showing WARN
	g_pDigitalInputOutput->GetLEDState( &eLed );
	if ( LED_WARN != eLed )
		g_pDigitalInputOutput->SetLEDState( LED_POWER );

	// Check for extend bit and retraction
	if ( ERR_OK != ( eErr = CheckSyringe() ) )
		{
		SetOperState( OPER_STATE_RELEASE );
		return eErr;
		}

	// Over temp, SBC and RF generator checks
	LOG_ERROR_RETURN( eErr = CheckCriticalMonitoring() )
	if ( ERR_OK != eErr )
		{
		SetOperState( OPER_STATE_ERROR );
		return eErr;
		}

	// Check whether the RF generator should be reset
	if ( g_pRFGen->ResetRequired() )
		{
		SetOperState( OPER_STATE_IDLE );
		return ERR_RF_GEN_RESET;
		}

	switch ( eErr = WaitingForTrigger( &bTrig ) )
		{
		case ERR_OK:
			if ( bTrig && m_bHandpiecePrimed )
				{
				m_lastTrig = true;
				upSbcState eSbcState;
				LOG_ERROR_RETURN( eErr = g_pSbcIntfc->ReadSbcState( &eSbcState ) )
				if ( ERR_OK == eErr )
					{
					// Start priming if showing the connected screen or one of the technician diagnostics
					if ( SBC_STATE_TECH_DIAG == eSbcState || SBC_STATE_PRIME == eSbcState )
						{
						SetOperState( OPER_STATE_CATH_PRIMING );
						g_pDigitalInputOutput->SetLEDState( LED_PRIME );
						LOG_ERROR_RETURN( eErr = m_catheterPrime.InitializePrime() )
						m_catheterPrime.GetProgress( &m_bProgress );
						}
					else
						{
						LOG_ERROR_RETURN( eErr = ERR_SBC_STATE_WRONG )
						}
					}
				}
			break;

			// Remap thermo disconnect to full error
		case ERR_THERMO_INTERLOCK_MISSING:
			eErr = ERR_THERMO_ERROR;
			DisableDeliveryDevice( eErr, false );
			break;

		case ERR_THERMO_ERROR:
		case ERR_DDEVICE_INVALID:
		case ERR_DDEVICE_UNKNOWN:
			DisableDeliveryDevice( eErr, false );
			break;

		case ERR_DDEVICE_EXPIRED_TREAT:
		case ERR_DDEVICE_EXPIRED_TIME:
			DisableDeliveryDevice( eErr, true );
			break;

			// RF Generator needs to be reset
		case ERR_RF_GEN_RESET:
			SetOperState( OPER_STATE_IDLE );
			break;

			// RF Generator has failed. Go to terminate
		case ERR_RF_GEN_POWER_LEVEL:
		case ERR_EXCESSIVE_TEMP:
		case ERR_RF_GEN_NOT_FOUND:
		case ERR_RF_GEN_COMM_FATAL:
			SetOperState( OPER_STATE_ERROR );
			break;

			// Rod fully extended
		case ERR_PUMP_AT_LIMIT:
			g_pWaterCntl->StartRetractingPumpRod();
			SetOperState( OPER_STATE_RELEASE );
			break;

			// Disconnected something
		case ERR_NO_DDEVICE:
		case ERR_NOT_READY:
		case ERR_INTERLOCK_MISSING:
			SetOperState( OPER_STATE_IDLE );
			break;

		default:
			LOG_ERROR_RETURN( eErr )
			SetOperState( OPER_STATE_IDLE );
			break;
		}

	return eErr;
}

/**
 * @brief	This method controls the MCU while in the Catheter Priming state
 *
 * @return   Status as upErr
 */
upErr CSupervisor::CatheterPrimingState()
{
	upErr eErr = ERR_OK;

	// Over temp, SBC and RF generator checks
	LOG_ERROR_RETURN( eErr = CheckCriticalMonitoring() )
	if ( ERR_OK != eErr )
		{
		SetOperState( OPER_STATE_ERROR );
		return eErr;
		}

	// Don't perform an RFResetCheck() here as you may lose
	// state information in the priming state machine.
	// RfResetCheck() is called in PerformPrime() so catch
	// it in the switch statement instead.
	LOG_ERROR_RETURN( eErr = m_catheterPrime.PerformPrime() )
	m_catheterPrime.GetProgress( &m_bProgress );
	switch ( eErr )
		{
		// Push to Ready if primed
		case ERR_OK:
			if ( CCatheterPrime::PRIME_SUCCESS == m_catheterPrime.GetState() )
				{
				m_ablationCntl.SetLastAblationEndTime();
				GetTick( &m_lastEnd );
				SetCatheterPrimedFlag( true );
				SetOperState( OPER_STATE_SETUP );
				}
			else if ( CCatheterPrime::PRIME_FAILED == m_catheterPrime.GetState() )
				{
				SetOperState( OPER_STATE_HAND_PRIMED );
				SetCatheterPrimedFlag( false );
				}
			break;

		case ERR_RF_GEN_RESET:
			SetOperState( OPER_STATE_IDLE );
			break;

			// Disable delivery device for these errors
		case ERR_THERMO_ERROR:
		case ERR_DDEVICE_INVALID:
		case ERR_DDEVICE_UNKNOWN:
		case ERR_DDEVICE_COIL:
			DisableDeliveryDevice( eErr, false );
			break;

			// Play the disabled tone
		case ERR_THERMO_UNDER_TEMP:
		case ERR_DDEVICE_EXPIRED_TREAT:
		case ERR_DDEVICE_EXPIRED_TIME:
			DisableDeliveryDevice( eErr, true );
			break;

			// Go to terminate state for the following errors
		case ERR_EXCESSIVE_TEMP:
		case ERR_GENERATOR_OVER_TEMP:
		case ERR_IPC_CRITICAL_TIMEOUT:
		case ERR_IPC_CONNECTION_TIMEOUT:
		case ERR_RF_GEN_POWER_LEVEL:
		case ERR_INTERNAL_FAULT:
		case ERR_RF_GEN_NOT_FOUND:
		case ERR_RF_GEN_COMM_FATAL:
			SetOperState( OPER_STATE_ERROR );
			break;

			// Rod fully extended
		case ERR_PUMP_AT_LIMIT:
			SetOperState( OPER_STATE_RELEASE );
			SetCatheterPrimedFlag( false );
			break;

			// Not an error but warn regardless
		case ERR_TRIGGER_OFF:
			GetTick( &m_lastEnd );
			SetOperState( OPER_STATE_HAND_PRIMED );
			break;

			// Over temp persist at start of priming
		case ERR_THERMO_OVER_TEMP_PERSIST:
			LOG_ERROR_RETURN( eErr = ERR_THERMO_OVER_TEMP )
			SetOperState( OPER_STATE_HAND_PRIMED );
			break;

		default:
			GetTick( &m_lastEnd );
			SetOperState( OPER_STATE_CNCTD );
			LOG_ERROR_RETURN( eErr )
			break;
		}

	return eErr;
}

/**
 * @brief	This method controls the MCU while in the Setup state
 *
 * @return   Status as upErr
 */
upErr CSupervisor::SetupState()
{
	upErr eErr = ERR_OK;
	upSbcCmd eSbcCmd = SBC_CMD_UNDEFINED;
	byte bEnter = false;
	bool_c bTrig = false;
	upLEDState eLed;

	// Set LED state if not showing WARN
	g_pDigitalInputOutput->GetLEDState( &eLed );
	if ( LED_WARN != eLed )
		g_pDigitalInputOutput->SetLEDState( LED_POWER );

	// Over temp, SBC and RF generator checks
	LOG_ERROR_RETURN( eErr = CheckCriticalMonitoring() )
	if ( ERR_OK != eErr )
		{
		SetOperState( OPER_STATE_ERROR );
		return eErr;
		}

	// Check whether the RF generator should be reset
	if ( g_pRFGen->ResetRequired() )
		{
		SetOperState( OPER_STATE_IDLE );
		return ERR_RF_GEN_RESET;
		}

	// Check for extend bit and retraction
	if ( ERR_OK != ( eErr = CheckSyringe() ) )
		{
		SetOperState( OPER_STATE_RELEASE );
		return eErr;
		}

	// Wait for confirmation and set time
	//* TODO Remove for final product
	int iValue;
	iValue = g_pTreatmentInfo->getCustom17();
	if ( 2 == iValue )
		{
		bEnter = true;
		}

	// Get last command from SBC and check for Enter operation
	LOG_ERROR_RETURN( eErr = g_pSbcIntfc->GetLastCmnd( &eSbcCmd ) )

	if ( SBC_CMD_SETUP == eSbcCmd )
		{
		g_pSbcIntfc->SetLastCmnd( SBC_CMD_UNDEFINED );
		bEnter = true;
		}

	// TODO use time sent from UI and double-check it
	if ( bEnter )
		{
		STreatmentSelection sTreat;
		sTreat.Ttreat = g_pTreatmentInfo->getTdelivery();
		sTreat.TtreatStatus = ERR_OK;
		LOG_ERROR_RETURN( eErr = g_pDelOracle->CheckSelection( sTreat ) )
		if ( ERR_OK == eErr )
			{
			LOG_ERROR_RETURN( eErr = g_pDelOracle->NewSelection( sTreat ) )
			if ( ERR_OK == eErr )
				{
				LOG_ERROR_RETURN( SetOperState( OPER_STATE_PENDING ) )
				return eErr;
				}
			}
		}

	// Call WaitingForTrigger to catch OFF trigger and other operational errors. Ignore trigger transitions
	switch ( eErr = WaitingForTrigger( &bTrig ) )
		{

		case ERR_OK:

			// Perform coil idling
			LOG_ERROR_RETURN( eErr = PerformCoilIdle() )
			if ( ERR_OK != eErr )
				{
				return eErr;
				}

			// Perform checks and compute progress
			eErr = g_pGeneratorMonitor->PendingReadyCheck( m_lastTrig, m_lastEnd, &m_ePendingSubState, &m_bProgress );

			// Don't report persisting temp error
			if ( ERR_THERMO_OVER_TEMP_PERSIST == eErr )
				eErr = ERR_OK;

			// If ERR_OK then DON'T go the Ready
			// if ( PENDING_READY_ERR_OK == m_ePendingSubState )
			//	SetOperState( OPER_STATE_READY );
			break;

			// RF Generator needs to be reset
		case ERR_RF_GEN_RESET:
			SetOperState( OPER_STATE_IDLE );
			break;

			// RF Generator has failed. Go to terminate
		case ERR_RF_GEN_POWER_LEVEL:
		case ERR_EXCESSIVE_TEMP:
		case ERR_RF_GEN_NOT_FOUND:
		case ERR_RF_GEN_COMM_FATAL:
			SetOperState( OPER_STATE_ERROR );
			break;

		case ERR_THERMO_ERROR:
		case ERR_THERMO_INTERLOCK_MISSING:
		case ERR_DDEVICE_INVALID:
		case ERR_DDEVICE_UNKNOWN:
			DisableDeliveryDevice( eErr, false );
			break;

			// Play the disabled tone
		case ERR_DDEVICE_EXPIRED_TREAT:
		case ERR_DDEVICE_EXPIRED_TIME:
			DisableDeliveryDevice( eErr, true );
			break;

		case ERR_DDEVICE_DISABLED:
		case ERR_NO_DDEVICE:
		case ERR_INTERLOCK_MISSING:
			SetOperState( OPER_STATE_IDLE );
			break;

		default:
			LOG_ERROR_RETURN( eErr )
			SetOperState( OPER_STATE_IDLE );
			break;
		}

	return eErr;
}

/**
 * @brief	This method handles the "Pending Ready" state.
 *
 * @return   Status as upErr
 */
upErr CSupervisor::PendingState()
{
	upErr eErr;
	bool_c bTrig = false;
	upLEDState eLed;

	// Set LED state if not showing WARN
	g_pDigitalInputOutput->GetLEDState( &eLed );
	if ( LED_WARN != eLed )
		g_pDigitalInputOutput->SetLEDState( LED_POWER );

	// Over temp, SBC and RF generator checks
	LOG_ERROR_RETURN( eErr = CheckCriticalMonitoring() )
	if ( ERR_OK != eErr )
		{
		SetOperState( OPER_STATE_ERROR );
		return eErr;
		}

	// Check whether the RF generator should be reset
	if ( g_pRFGen->ResetRequired() )
		{
		SetOperState( OPER_STATE_IDLE );
		return ERR_RF_GEN_RESET;
		}

	// Check for extend bit and retraction
	if ( ERR_OK != ( eErr = CheckSyringe() ) )
		{
		SetOperState( OPER_STATE_RELEASE );
		return eErr;
		}

	// Call WaitingForTrigger to catch OFF trigger and other operational errors
	switch ( eErr = WaitingForTrigger( &bTrig ) )
		{

		case ERR_OK:

			// Perform coil idling
			LOG_ERROR_RETURN( eErr = PerformCoilIdle() )
			if ( ERR_OK != eErr )
				{
				return eErr;
				}

			// Perform checks and compute progress
			eErr = g_pGeneratorMonitor->PendingReadyCheck( m_lastTrig, m_lastEnd, &m_ePendingSubState, &m_bProgress );

			// Don't report persisting temp error
			if ( ERR_THERMO_OVER_TEMP_PERSIST == eErr )
				eErr = ERR_OK;

			// If ERR_OK then go the Ready
			if ( PENDING_READY_ERR_OK == m_ePendingSubState )
				SetOperState( OPER_STATE_READY );
			break;

			// RF Generator needs to be reset
		case ERR_RF_GEN_RESET:
			SetOperState( OPER_STATE_IDLE );
			break;

			// RF Generator has failed. Go to terminate
		case ERR_RF_GEN_POWER_LEVEL:
		case ERR_EXCESSIVE_TEMP:
		case ERR_RF_GEN_NOT_FOUND:
		case ERR_RF_GEN_COMM_FATAL:
			SetOperState( OPER_STATE_ERROR );
			break;

		case ERR_THERMO_ERROR:
		case ERR_THERMO_INTERLOCK_MISSING:
		case ERR_DDEVICE_INVALID:
		case ERR_DDEVICE_UNKNOWN:
			DisableDeliveryDevice( eErr, false );
			break;

			// Play the disabled tone
		case ERR_DDEVICE_EXPIRED_TREAT:
		case ERR_DDEVICE_EXPIRED_TIME:
			DisableDeliveryDevice( eErr, true );
			break;

		case ERR_DDEVICE_DISABLED:
		case ERR_NO_DDEVICE:
		case ERR_INTERLOCK_MISSING:
			SetOperState( OPER_STATE_IDLE );
			break;

		default:
			LOG_ERROR_RETURN( eErr )
			SetOperState( OPER_STATE_IDLE );
			break;
		}

	return eErr;
}

/**
 * @brief	This method handles the "Ready" state.
 *
 * @return   Status as upErr
 */
upErr CSupervisor::ReadyState()
{
	upErr eErr = ERR_OK;
	bool_c bTrig = false;
	upLEDState eLed;

	// Set LED state if not showing WARN
	g_pDigitalInputOutput->GetLEDState( &eLed );
	if ( LED_WARN != eLed )
		g_pDigitalInputOutput->SetLEDState( LED_POWER );

	//* TODO Remove for final product
	int iValue;
	iValue = g_pTreatmentInfo->getCustom15();
	if ( 2 == iValue )
		{
		LOG_ERROR_RETURN( eErr = SetOperState( OPER_STATE_PRECOND ) )
		}
	else if ( 1 == iValue )
		{
		LOG_ERROR_RETURN( eErr = SetOperState( OPER_STATE_SETUP ) )
		}

	// Get last command from SBC and check for pass/fail of prime
	upSbcCmd eSbcCmd = SBC_CMD_UNDEFINED;
	LOG_ERROR_RETURN( eErr = g_pSbcIntfc->GetLastCmnd( &eSbcCmd ) )

	if ( SBC_CMD_DELIVERY_UNLOCK == eSbcCmd )
		{
		g_pSbcIntfc->SetLastCmnd( SBC_CMD_UNDEFINED );
		LOG_ERROR_RETURN( eErr = SetOperState( OPER_STATE_PRECOND ) )
		return eErr;
		}
	else if ( SBC_CMD_SETUP == eSbcCmd )
		{
		g_pSbcIntfc->SetLastCmnd( SBC_CMD_UNDEFINED );
		LOG_ERROR_RETURN( eErr = SetOperState( OPER_STATE_SETUP ) )
		return eErr;
		}

	// Perform coil idling
	LOG_ERROR_RETURN( eErr = PerformCoilIdle() )
	if ( ERR_OK != eErr )
		{
		return eErr;
		}
	// Over temp, SBC and RF generator checks
	LOG_ERROR_RETURN( eErr = CheckCriticalMonitoring() )
	if ( ERR_OK != eErr )
		{
		SetOperState( OPER_STATE_ERROR );
		return eErr;
		}

	// Check whether the RF generator should be reset
	if ( g_pRFGen->ResetRequired() )
		{
		SetOperState( OPER_STATE_IDLE );
		return ERR_RF_GEN_RESET;
		}

	// Check for extend bit and retraction
	if ( ERR_OK != ( eErr = CheckSyringe() ) )
		{
		SetOperState( OPER_STATE_RELEASE );
		return eErr;
		}

	// Call WaitingForTrigger to catch OFF trigger and other operational errors
	switch ( eErr = WaitingForTrigger( &bTrig ) )
		{

		case ERR_OK:

			// Perform coil idling
			LOG_ERROR_RETURN( eErr = PerformCoilIdle() )
			if ( ERR_OK != eErr )
				{
				return eErr;
				}
			break;

			// RF Generator needs to be reset
		case ERR_RF_GEN_RESET:
			SetOperState( OPER_STATE_IDLE );
			break;

			// RF Generator has failed. Go to terminate
		case ERR_RF_GEN_POWER_LEVEL:
		case ERR_EXCESSIVE_TEMP:
		case ERR_RF_GEN_NOT_FOUND:
		case ERR_RF_GEN_COMM_FATAL:
			SetOperState( OPER_STATE_ERROR );
			break;

		case ERR_THERMO_ERROR:
		case ERR_THERMO_INTERLOCK_MISSING:
		case ERR_DDEVICE_INVALID:
		case ERR_DDEVICE_UNKNOWN:
			DisableDeliveryDevice( eErr, false );
			break;

			// Play the disabled tone
		case ERR_DDEVICE_EXPIRED_TREAT:
		case ERR_DDEVICE_EXPIRED_TIME:
			DisableDeliveryDevice( eErr, true );
			break;

		case ERR_DDEVICE_DISABLED:
		case ERR_NO_DDEVICE:
		case ERR_INTERLOCK_MISSING:
			SetOperState( OPER_STATE_IDLE );
			break;

		default:
			LOG_ERROR_RETURN( eErr )
			SetOperState( OPER_STATE_IDLE );
			break;
		}

	return eErr;
}

/**
 * @brief	This method controls the MCU while in the Precondition state
 *
 * @return   Status as upErr
 */
upErr CSupervisor::PreconditionState()
{
	upErr eErr = ERR_OK;
	bool_c bTrig = false;
	upLEDState eLed;

	// Set LED state if not showing WARN
	g_pDigitalInputOutput->GetLEDState( &eLed );
	if ( LED_WARN != eLed )
		g_pDigitalInputOutput->SetLEDState( LED_READY );

	// Over temp, SBC and RF generator checks
	LOG_ERROR_RETURN( eErr = CheckCriticalMonitoring() )
	if ( ERR_OK != eErr )
		{
		SetOperState( OPER_STATE_ERROR );
		return eErr;
		}

	// Check whether the RF generator should be reset
	if ( g_pRFGen->ResetRequired() )
		{
		SetOperState( OPER_STATE_IDLE );
		return ERR_RF_GEN_RESET;
		}

	// Check for extend bit and retraction
	LOG_ERROR_RETURN( eErr = CheckSyringe() )
	if ( ERR_OK != eErr )
		{
		SetOperState( OPER_STATE_RELEASE );
		return eErr;
		}

	// Check for precond period timeout
	tick tDiff;
	tick tTotal = SEC_TENTHS_TO_TICKS( g_pTreatmentInfo->getTprecond() );
	GetTickElapsed( m_tStateStart, &tDiff );
	if ( tTotal <= tDiff )
		{
		LOG_ERROR_RETURN( eErr = ERR_PRECOND_TIMEOUT )
		SetOperState( OPER_STATE_READY );
		return eErr;
		}

	// Check for still being ready
	LOG_ERROR_RETURN(
	        eErr = g_pGeneratorMonitor->PendingReadyCheck( m_lastTrig, m_lastEnd, &m_ePendingSubState, &m_bProgress ) )

	// Compute percentage complete and clamp at 100
	m_bProgress = (byte) ( ( 100 * tDiff ) / tTotal );
	if ( m_bProgress > 100 )
		m_bProgress = 100;

	// TODO What's this for?
	if ( ERR_OK != eErr || PENDING_READY_ERR_OK != m_ePendingSubState )
		{

		// Switch to Pending state
		SetOperState( OPER_STATE_PENDING );

		// Handle error
		if ( ERR_OK != eErr )
			{
			LOG_ERROR_RETURN( g_pComOp->HaltPumpAndRF() )
			}

		// Exit in both cases
		return eErr;
		}

	// Check trigger and other generator stati
	switch ( eErr = WaitingForTrigger( &bTrig ) )
		{

		case ERR_OK:
			if ( bTrig )
				{

				//
				// TODO remove from here and use time sent from UI in setup phase
				//
#ifdef ENABLE_EASY_ENGINEERING_MODE
					{
					STreatmentSelection sTreat;
					sTreat.Ttreat = g_pTreatmentInfo->getTdelivery();
					sTreat.TtreatStatus = ERR_OK;
					LOG_ERROR_RETURN( eErr = g_pDelOracle->CheckSelection( sTreat ) )
					if ( ERR_OK == eErr )
						{
						LOG_ERROR_RETURN( eErr = g_pDelOracle->NewSelection( sTreat ) )
						if ( ERR_OK != eErr )
							{
							LOG_ERROR_RETURN( SetOperState( OPER_STATE_SETUP ) )
							return eErr;
							}
						}
					}
#endif

				m_lastTrig = true;
				g_pDigitalInputOutput->SetLEDState( LED_TREAT );
				LOG_ERROR_RETURN( m_ablationCntl.StartAblation() )
				SetOperState( OPER_STATE_DELIVERING );
				}

			// Perform coil idling
			else
				{
				LOG_ERROR_RETURN( eErr = PerformCoilIdle() )
				if ( ERR_OK != eErr )
					{
					return eErr;
					}
				}
			break;

			// RF Generator needs to be reset
		case ERR_RF_GEN_RESET:
			SetOperState( OPER_STATE_IDLE );
			break;

			// RF Generator has failed. Go to terminate
		case ERR_RF_GEN_POWER_LEVEL:
		case ERR_EXCESSIVE_TEMP:
		case ERR_RF_GEN_NOT_FOUND:
		case ERR_RF_GEN_COMM_FATAL:
			SetOperState( OPER_STATE_ERROR );
			break;

		case ERR_THERMO_ERROR:
		case ERR_THERMO_INTERLOCK_MISSING:
		case ERR_DDEVICE_INVALID:
		case ERR_DDEVICE_UNKNOWN:
			DisableDeliveryDevice( eErr, false );
			break;

			// Play the disabled tone
		case ERR_DDEVICE_EXPIRED_TREAT:
		case ERR_DDEVICE_EXPIRED_TIME:
			DisableDeliveryDevice( eErr, true );
			break;

		case ERR_DDEVICE_DISABLED:
		case ERR_NO_DDEVICE:
		case ERR_INTERLOCK_MISSING:
			SetOperState( OPER_STATE_IDLE );
			break;

		default:
			LOG_ERROR_RETURN( eErr )
			SetOperState( OPER_STATE_IDLE );
			break;
		}

	return eErr;
}

/**
 * @brief	This method handles the "Treating" state
 *
 * @return   Status as upErr
 */
upErr CSupervisor::DeliveringState()
{
	upErr eErr;
	// Always reset the critical timer to disallow IPC timeout errors during treatment
	g_pSbcIntfc->ResetCriticalTimer();

	// Over temp, SBC and RF generator checks
	LOG_ERROR_RETURN( eErr = CheckCriticalMonitoring() )
	if ( ERR_OK != eErr )
		{
		SetOperState( OPER_STATE_ERROR );
		return eErr;
		}

	// Check whether the RF generator should be reset
	if ( g_pRFGen->ResetRequired() )
		{
		SetOperState( OPER_STATE_IDLE );
		return ERR_RF_GEN_RESET;
		}

	// Call into CAblationControl
	LOG_ERROR_RETURN( eErr = m_ablationCntl.PerformTreatment() )

	// Act on error
	switch ( eErr )
		{
		// RF Generator has failed or other serious fault. Go to terminate
		case ERR_EXCESSIVE_TEMP:
		case ERR_GENERATOR_OVER_TEMP:
		case ERR_IPC_CRITICAL_TIMEOUT:
		case ERR_IPC_CONNECTION_TIMEOUT:
		case ERR_RF_GEN_POWER_LEVEL:
		case ERR_INTERNAL_FAULT:
		case ERR_RF_GEN_NOT_FOUND:
		case ERR_RF_GEN_COMM_FATAL:
			SetOperState( OPER_STATE_ERROR );
			break;

			// If there is a thermocouple, undertemp error, or other coil error, disable the device
			// and return to idle
		case ERR_THERMO_OVER_TEMP:
			eErr = ERR_ABLATE_OVER_TEMP;
			DisableDeliveryDevice( eErr, false );
			break;

		case ERR_THERMO_ERROR:
		case ERR_DDEVICE_COIL:
			DisableDeliveryDevice( eErr, false );
			break;

			// Play the disable tone
		case ERR_THERMO_UNDER_TEMP:
			DisableDeliveryDevice( eErr, true );
			break;

			// If there is an interlock error or other error beside ERR_TRIGGER_OFF go to Idle
		case ERR_INTERLOCK_MISSING:
			SetOperState( OPER_STATE_IDLE );
			break;

		case ERR_OK:
			break;

		case ERR_PUMP_AT_LIMIT:
			SetOperState( OPER_STATE_RELEASE );
			break;

		default:
			g_pComOp->HaltPumpAndRF();
			SetOperState( OPER_STATE_SETUP );
			break;
		}

	// Check for completion
	if ( ERR_OK != eErr )
		{
		GetTick( &m_lastEnd );
		}
	else if ( ABLATE_STATE_COMPLETE == m_ablationCntl.GetAblationState() )
		{
		GetTick( &m_lastEnd );
		SetOperState( OPER_STATE_SETUP );

		// Check if Temperature is close to or more than the coil idle upper limit at end of treatment
		int iUpperLimitIdleWithOffset = g_pTreatmentInfo->getTempUpperLimitIdle() - UPPER_LIMIT_IDLE_OFFSET;
		int iDuration = MS_TO_RND_ROBIN_CYC( g_pTreatmentInfo->getTtempWindow() );
		int iCoilTemp;
		int iOutletTemp;
		LOG_ERROR_RETURN( g_pTemperature->GetFilteredThermoTemp( THERMO_COIL, &iCoilTemp ) )
		LOG_ERROR_RETURN( g_pTemperature->GetFilteredThermoTemp( THERMO_OUTLET, &iOutletTemp ) )
		if ( ( iCoilTemp > iUpperLimitIdleWithOffset ) || ( iOutletTemp > iUpperLimitIdleWithOffset ) )
			{
			// Set m_bOverTempCnt value to indicate that temperature is close to coil idle upper limit,
			// require wait until temperature drops below lower band (i.e. remain in PendingReady state)
			g_pGeneratorMonitor->SetOverTempCnt( iDuration );
			LOG_ERROR_RETURN( ERR_THERMO_OVER_TEMP_PERSIST )
			}
		}

	m_ablationCntl.GetProgress( &m_bProgress );

	return eErr;
}

/**
 * @brief	This method controls the MCU while in the Release state
 *
 * @return   Status as upErr
 */
upErr CSupervisor::ReleaseState()
{
	bool_c bRet = false;
	bool_c bSyringe;
	upLEDState eLed;

	// Set LED state if not showing WARN
	g_pDigitalInputOutput->GetLEDState( &eLed );
	if ( LED_WARN != eLed )
		g_pDigitalInputOutput->SetLEDState( LED_PRIME );

	// Retract
	g_pWaterCntl->PerformRodRetraction( &bRet );

	// Check for syringe presence
	g_pDigitalInputOutput->GetInput( DIG_IN_SYR_PRESENT, &bSyringe );

	// When retracted and syringe released go to Idle
	if ( true == bRet && false == bSyringe )
		{
		SetCatheterPrimedFlag( false );
		SetHandpiecePrimedFlag( false );
		SetOperState( OPER_STATE_IDLE );
		}

	return ERR_OK;
}

/**
 * @brief	This method handles the "Terminate" state
 *
 * 			The operations are to shut off RF power, set LEDs
 * 			and halt the stepper motor. An error tone is produced from the MCU board
 * 			on the first execution.
 *
 * @return   Status as upErr
 */
upErr CSupervisor::ErrorState()
{
	upErr eErr = ERR_OK;

	// Check version if/when SBC comes up
	if ( !m_bVersionsVerified )
		{
		// Check for SBC communication
		upSbcState eSbcState;
		LOG_ERROR_RETURN( eErr = g_pSbcIntfc->ReadSbcState( &eSbcState ) )
		if ( ERR_OK == eErr && SBC_STATE_OOS != eSbcState )
			{
			LOG_ERROR_RETURN( eErr = g_pComOp->VerifyCodeVersions() )
			m_bVersionsVerified = true;
			}
		}

	g_pSoundCntl->OutputToneOnce();
	g_pDigitalInputOutput->SetLEDState( LED_TERMINATE );
	g_pComOp->HaltPumpAndRF();
	g_pRFGen->SetACInput( false );

	return eErr;
}

/**
 * @brief	This method controls the operation of the Union Generator
 *
 * @return   Status as upErr
 */
upErr CSupervisor::Execute()
{
	byte bUserCnt = 0;
	// Always set fresh operating conditions
	SetOperatingConditions();

#ifdef SIM_OUTPUT_CAPTURE
	// Set Treatment info
	g_pSbcIntfc->SendTextOperatingConditions();
#endif

	// If the user message queue has reached the maximum, terminate
	g_pLogMessage->GetUserMsgCount( &bUserCnt );
	if ( bUserCnt >= USER_MSG_MAX_CNT )
		{
		LOG_ERROR_RETURN( ERR_ERROR )
		SetOperState( OPER_STATE_ERROR );
		}

	// Always check the round-robin scheduler for overruns
	if ( m_bRoundRobinOverrun )
		{
		LOG_ERROR_RETURN( ERR_ERROR )
		SetOperState( OPER_STATE_ERROR );
		}

	// Check the health of the SpiBus
	if ( g_pTemperature->GetHwFault() )
		{
		LOG_ERROR_RETURN( ERR_ERROR )
		SetOperState( OPER_STATE_ERROR );
		}

	// Select operating state of MCU
	switch ( m_operState )
		{

		// After first cycle go to self test
		case OPER_STATE_BOOT:
			SetOperState( OPER_STATE_TEST );
			break;

		case OPER_STATE_TEST:
			LOG_ERROR_RETURN( TestState() )
			break;

		case OPER_STATE_IDLE:
			LOG_ERROR_RETURN( IdleState() )
			break;

		case OPER_STATE_CNCTD:
			LOG_ERROR_RETURN( ConnectedState() )
			break;

		case OPER_STATE_HAND_PRIMING:
			LOG_ERROR_RETURN( g_pLogMessage->ReturnUserMsgCode( HandpiecePrimingState(), OPER_STATE_HAND_PRIMING ) )
			break;

		case OPER_STATE_HAND_PRIMED:
			LOG_ERROR_RETURN( g_pLogMessage->ReturnUserMsgCode( HandpiecePrimed(), OPER_STATE_HAND_PRIMED ) )
			break;

		case OPER_STATE_CATH_PRIMING:
			LOG_ERROR_RETURN( g_pLogMessage->ReturnUserMsgCode( CatheterPrimingState(), OPER_STATE_CATH_PRIMING ) )
			break;

		case OPER_STATE_SETUP:
			LOG_ERROR_RETURN( g_pLogMessage->ReturnUserMsgCode( SetupState(), OPER_STATE_SETUP ) )
			break;

		case OPER_STATE_PENDING:
			LOG_ERROR_RETURN( g_pLogMessage->ReturnUserMsgCode( PendingState(), OPER_STATE_PENDING ) )
			break;

		case OPER_STATE_READY:
			LOG_ERROR_RETURN( g_pLogMessage->ReturnUserMsgCode( ReadyState(), OPER_STATE_READY ) )
			break;

		case OPER_STATE_PRECOND:
			LOG_ERROR_RETURN( g_pLogMessage->ReturnUserMsgCode( PreconditionState(), OPER_STATE_PRECOND ) )
			break;

		case OPER_STATE_DELIVERING:
			LOG_ERROR_RETURN( g_pLogMessage->ReturnUserMsgCode( DeliveringState(), OPER_STATE_DELIVERING ) )
			break;

		case OPER_STATE_RELEASE:
			LOG_ERROR_RETURN( g_pLogMessage->ReturnUserMsgCode( ReleaseState(), OPER_STATE_RELEASE ) )
			break;

		default:
			LOG_ERROR_RETURN( ERR_ENUM_OUT_OF_RANGE )
			SetOperState( OPER_STATE_ERROR );
			LOG_ERROR_RETURN( ErrorState() )
			break;

		case OPER_STATE_ERROR:
			LOG_ERROR_RETURN( ErrorState() )
			break;

		}

	return ERR_OK;
}

/**
 * @brief	This method sets the initial values for starting the Supervisor.
 */
void CSupervisor::SetInitValues()
{
	m_operState = OPER_STATE_BOOT;
	m_ePendingSubState = PENDING_READY_ERR_OK;
	m_lastEnd = 0;
	m_lastTrig = true;
	m_eDDeviceState = DD_STATE_DISCONNECTED;
	m_bCatheterPrimed = false;
#ifdef ALLOW_EMC_TREATMENT_CODE
	m_emcTestCycle = 0;
#endif
	m_bRoundRobinOverrun = false;
	m_bVersionsVerified = false;
	m_bProgress = 0;
	m_tStateStart = 0;
	for ( int i = 0; i < OW_ROM_SIZE; i++ )
		m_lastOwRomId[ i ] = 0;
}

/**
 * @brief	This method sets the flag indicating that a Round Robin Overrun error occurred.
 *
 * @return   void
 */
void CSupervisor::SetOverrunFlag()
{
	m_bRoundRobinOverrun = true;
}

/**
 * @brief	This method contains logic to determine if Idling should be performed.
 *
 * 			Do idling after priming and the last treatment is not a prime with the DD "attached" and
 * 			a retraction neither requested or the last operation.
 *
 * @return   true if should idle or false otherwise
 */
bool_c CSupervisor::CoilIdleAllowed()
{
	bool_c bRet;
	return ( m_bCatheterPrimed && DD_STATE_ATTACHED == m_eDDeviceState
	        && false == g_pWaterCntl->PerformRodRetraction( &bRet ) ) ? true : false;
}

/**
 * @brief	This method disables the attached delivery device.
 *
 * 	This action is to set the state to disabled This will
 * 	only be done once per attachment of a delivery device.
 *  A disabled tone may sound dependent on the event
 *  that is requesting the device be disabled.
 *
 * @param	eErr Error code that is causing the invalidation as upErr
 * @param   bPlayTone A boolean flag indicating whether a disabled tone should play
 *
 * @return   Status as upErr
 */
upErr CSupervisor::DisableDeliveryDevice( upErr eErr, bool_c bPlayTone )
{

	// Only send the bad delivery tool message once per tool
	if ( DD_STATE_ATTACHED == m_eDDeviceState )
		{
		LOG_ERROR_RETURN( eErr )
		g_pOneWireMemory->DisableTool();
		}

	SetOperState( OPER_STATE_IDLE );
	m_eDDeviceState = DD_STATE_DISABLED;

	if ( bPlayTone )
		g_pSoundCntl->OutputInvalidateTone();

	return ERR_OK;
}

/**
 * @brief	This method wraps setting the operating state of the Supervisor.
 *
 * 			It applies additional logic when transitioning between states.
 *
 * @param	eOperState The next operating state as upOperState
 *
 * @return   Status as upErr
 */
upErr CSupervisor::SetOperState( upOperState eOperState )
{
	upErr eErr = ERR_OK;

	switch ( eOperState )
		{

		case OPER_STATE_BOOT:
			break;

		case OPER_STATE_TEST:
			g_pOneWireMemory->EnableOwMonitor( false );
			m_bProgress = 0;
			break;

		case OPER_STATE_IDLE:
			if ( eOperState != m_operState )
				LOG_ERROR_RETURN( eErr = g_pComOp->HaltPumpAndRF() )
			g_pRFGen->SetACInput( false ); // Turn RF OFF
			GetTick( &m_lastEnd ); // Get time of turning AC off
			m_lastTrig = true;
			if ( true == m_bVersionsVerified )
				g_pOneWireMemory->EnableOwMonitor( true );
			break;

		case OPER_STATE_CNCTD:
			SetHandpiecePrimedFlag( false );
			if ( eOperState != m_operState )
				LOG_ERROR_RETURN( eErr = g_pComOp->HaltPumpAndRF() )
			if ( OPER_STATE_IDLE == m_operState )
				m_lastTrig = true;
			g_pOneWireMemory->EnableOwMonitor( true );
			break;

		case OPER_STATE_HAND_PRIMING:
			SetHandpiecePrimedFlag( false );
			m_bProgress = 0;
			break;

		case OPER_STATE_HAND_PRIMED:
			if ( eOperState != m_operState )
				LOG_ERROR_RETURN( eErr = g_pComOp->HaltPumpAndRF() )
			break;

		case OPER_STATE_CATH_PRIMING:
			g_pOneWireMemory->EnableOwMonitor( false );
			m_bProgress = 0;
			break;

		case OPER_STATE_SETUP:
			g_pOneWireMemory->EnableOwMonitor( true );
			if ( CoilIdleAllowed() )
				LOG_ERROR_RETURN( eErr = g_pComOp->StartCoilIdle() )
			break;

		case OPER_STATE_PENDING:
			// The only way to re-enable idling is through the treating state
			g_pOneWireMemory->EnableOwMonitor( true );
			break;

		case OPER_STATE_READY:
			g_pOneWireMemory->EnableOwMonitor( true );
			break;

		case OPER_STATE_PRECOND:
			g_pOneWireMemory->EnableOwMonitor( true );
			break;

		case OPER_STATE_DELIVERING:
			// Ensure that the one-wire check for presence is off during an ablation
			g_pOneWireMemory->EnableOwMonitor( false );
			m_bProgress = 0;
			break;

		case OPER_STATE_RELEASE:
			if ( eOperState != m_operState )
				LOG_ERROR_RETURN( eErr = g_pComOp->HaltPumpAndRF() )
			g_pWaterCntl->StartRetractingPumpRod();
			break;

		case OPER_STATE_ERROR:
			g_pOneWireMemory->EnableOwMonitor( false );
			g_pRFGen->SetACInput( false );
			break;

		default:
			g_pOneWireMemory->EnableOwMonitor( false );
			m_operState = OPER_STATE_ERROR;
			return ERR_BAD_PARAM;
			break;
		}

	if ( m_operState != eOperState )
		{
		GetTick( &m_tStateStart );
		}

	m_operState = eOperState;
	return eErr;
}

/**
 * @brief	This method returns the existing operating state
 *
 * @return   Operating state of this class as upOperState
 */
upOperState CSupervisor::GetOperState()
{
	return m_operState;
}

/**
 * @brief	This method sets a local flag for the Handpiece being primed. Invalidating
 * 			the handpiece also invalidates the catheter.
 *
 * @param	bPrimed The primed flag bool_c
 *
 * @return   Status as ERR_OK
 */
upErr CSupervisor::SetHandpiecePrimedFlag( bool_c bPrimed )
{
	m_bHandpiecePrimed = bPrimed;
	if ( false == bPrimed )
		{
		m_bCatheterPrimed = false;
		}

	return ERR_OK;
}

/**
 * @brief	This method sets a local flag for the Catheter being primed.
 *
 * @param	bPrimed The primed flag bool_c
 *
 * @return   Status as ERR_OK
 */
upErr CSupervisor::SetCatheterPrimedFlag( bool_c bPrimed )
{
	m_bCatheterPrimed = bPrimed;
	return ERR_OK;
}

/**
 * @brief	This method loERR_OKs at the syringe switches and states and changes operating state
 * 			accordingly. It will invalidate priming if the rod is being retracted.
 *
 * @return   Status as upErr
 */
upErr CSupervisor::CheckSyringe()
{

	upErr eErr = ERR_OK;

	// Identify if user requested a retraction. Change state and invalidate the prime if it was
	bool_c retractBool;
	if ( g_pWaterCntl->GetRetracted( &retractBool ) )
		{
		eErr = ERR_RETRACTING_PUMP;
		}

	// If not retracting then check for extend bit. This prevents multiple errors of ERR_PUMP_AT_LIMIT
	else
		{
		// Return extended error code if no other errors
		bool_c bExt;
		LOG_ERROR_RETURN( eErr = g_pDigitalInputOutput->GetInput( DIG_IN_MOTOR_EXTEND, &bExt ) );
		if ( ERR_OK == eErr && bExt )
			{
			LOG_ERROR_RETURN( eErr = ERR_PUMP_AT_LIMIT )
			}
		}

	return eErr;
}

/**
 * @brief	This method performs the coil idling and maps low-level errors to user errors
 *
 * @return   Status as upErr
 */
upErr CSupervisor::PerformCoilIdle()
{
	upErr eErr = ERR_OK;

	if ( CoilIdleAllowed() )
		{
		LOG_ERROR_RETURN( eErr = g_pComOp->ApplyCoilIdle( m_lastEnd ) )
		switch ( eErr )
			{
			case ERR_IDLE_THERMO_OVER_TEMP:
				LOG_ERROR_RETURN( eErr = ERR_THERMO_OVER_TEMP )
				// Play the disable tone
				DisableDeliveryDevice( eErr, true );
				break;

			case ERR_OK:
			case ERR_THERMO_ERROR:
				break;

			default:
				LOG_ERROR_RETURN( eErr = ERR_ENUM_OUT_OF_RANGE )
				SetOperState( OPER_STATE_ERROR );
				break;

			}
		}
	else
		{
		LOG_ERROR_RETURN( eErr = g_pComOp->HaltPumpAndRF() )
		}

	return eErr;
}

/**
 * @brief	This method calls CGeneratorMonitor::PerformCriticalMonitoring and then takes CSupervisor-level
 * 			actions based on any errors found.
 *
 * @return   Status as upErr
 */
upErr CSupervisor::CheckCriticalMonitoring()
{
	upErr eErr;

	LOG_ERROR_RETURN( eErr = g_pGeneratorMonitor->PerformCriticalMonitoring() )

	switch ( eErr )
		{

		case ERR_OK:
			break;

			// Persist is ignored
		case ERR_THERMO_OVER_TEMP_PERSIST:
			eErr = ERR_OK;
			break;

			// Reset the RF generator
		case ERR_RF_GEN_RESET:
			// Set to ERR_OK to not trigger a critical error
			eErr = ERR_OK;
			SetOperState( OPER_STATE_IDLE );
			break;

			// All others
		default:
			SetOperState( OPER_STATE_ERROR );
			break;

		}

	return eErr;
}

// Private Class Functions

# This script adds an MCU version number to the
# Version.h header file.  The paths are hard
# coded and use the C:\EclipseWorkspaces development
# path. 

# The script requires Python 2.7.

# Last updated 11/04/2011, Chris

import sys
import fileinput
import os
import re


os.system('svn info | grep URL > buildversion.txt')
f = open('./buildversion.txt','r')
svnTmp = f.readline()
svnTmp = svnTmp.strip()
splitter = re.compile(r'\/')
urlList = splitter.split(svnTmp)
# default to date/time
svnVersion = "\"V_\" __DATE__ \"_\" __TIME__"

if urlList[-2] == "branches":
    svnVersion = "\"" + urlList[-1] + "\""
    
filename = "C:\EclipseWorkspaces\Common\include\Version.h"
searchPtn = "MCU_VERSION_ID"
replaceLineOrig = "#define MCU_VERSION_ID "
replaceLineFull = replaceLineOrig + svnVersion 

readFile = 'C:/EclipseWorkspaces/Common/include/Version.h'

for line in fileinput.FileInput(readFile,inplace=1):
        if re.search(searchPtn, line):
            print replaceLineFull.strip()
        else:
            print line.strip()
fileinput.close()
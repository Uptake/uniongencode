REM @echo OFF
REM Comment out preceding echo OFF to have commands shown
REM
REM This is the main script for the periodic MCU build. It receives the path to the SVN trunk, branch or tag to build as parameter #1
REM
FOR /F "tokens=1-4 delims=/ " %%I IN ('DATE /t') DO SET mydate=%%J%%K%%L
set BuildDir=Build%mydate%
set Log=BuildLog.txt
set Summ=BuildSummary.txt

echo Performing checkout, build and test runs: %date% %time% 
echo SVN path: %1 

set SUCCESS=successfully
set RESULT=%SUCCESS%
set FAILED=failed and

REM clean and create directory
rm -rf %BuildDir%
mkdir %BuildDir%

REM Move the Makefile to the new directory
cp Makefile %BuildDir%
cd %BuildDir%

REM Time-stamp start of build
echo Starting build of: %1 at %date% %time% >> %Log%
echo Starting build of: %1 at %date% %time% >> %Summ%


REM Use make to get the target image. The path to the image is the variable SVN_PATH
make Checkout SVN_PATH=%1 >> %Log% 2>&1
IF ERRORLEVEL 1 (
echo Checkout failed %date% %time% >> %Summ%
cd ..
exit /b )

REM Get revision information for ELF inputs
set SCL=SoftwareControlList.txt
@echo Software Control List for %1 on %date% %time% > %SCL%
svn list -v -R --depth files Common/include/*.h >> %SCL% 2>>%Log%
svn list -v -R --depth files CvasMcuElf/*.cpp >> %SCL% 2>>%Log%
svn list -v -R --depth files McuApplicationLib/*.cpp >> %SCL% 2>>%Log%
svn list -v -R --depth files McuApplicationLib/Include/*.h >> %SCL% 2>>%Log%
svn list -v -R --depth files McuHdwLib/*.cpp >> %SCL% 2>>%Log%
svn list -v -R --depth files RfGenRdo/*.cpp >> %SCL% 2>>%Log%


REM Make the source code
make Build >> %Log% 2>&1
IF ERRORLEVEL 1 (
echo Build failed %date% %time% >> %Summ%
set RESULT=%FAILED%
)


REM Put summary info into summary log
echo Build completed: %date% %time% >> %Log% 2>&1
echo Build %RESULT% completed: %date% %time% >> %Summ% 

cd ..

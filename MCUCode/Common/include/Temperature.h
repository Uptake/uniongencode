/**
 * @file	Temperature.h
 * @par		Package: UGMcu
 * @par		Project: Union Generator
 * @par		Copyright (c) 2017, Uptake Medical Technology, Inc.
 * @author	Glen Sep 15, 2010 Created file
 * @brief	This file declares the class for reading thermocouple temperatures, CTemperature
 *
 */

#ifndef TEMPERATURE_H_
#define TEMPERATURE_H_

// Include Files
#include "SpiDriver.h"
#include "RndRobinHelper.h"
#include "CalibrateTwoLinearPieces.h"
#include "McuInfo.h"

// Public Macros and Constants
#define TEMPERATURE_SPI_BYTES   (2)		//!< Number of data bytes for all ADCs that provide temperature values
#define TEMPERATURE_FIR_COUNT   (3)		//!< The count of sample to use in the Finite Impulse Response filter of temperatures

// Public Type Definitions (Enums, Structs & Classes)
class CTemperature;
extern CTemperature * g_pTemperature; //!< Globally accessible pointer to single object of CTemperature

/**
 * This enumeration identifies one of four thermocouples available on the Rev A through G hardware
 */
typedef enum upTempMeasEnum
{
	THERMO_OUTLET = 0,
	THERMO_COIL,
	THERMO_INTERIOR,
	THERMO_SPARE,
	THERMO_CNT
} upTempMeas;

/**
 * Need this intermediate typedef to make Doxygen happy with the inherited template
 */
typedef CCalibrateTwoLinearPieces<upTempMeas, THERMO_CNT, int> CThermoCal;

/**
 * @class	CTemperature
 * @brief	Reads the thermocouple temperatures available on the SPI bus.
 *
 * 			The temperatures are read in the
 * 			ReadFromHardware() call. The get calls access cached values. The temperatures are converted from
 * 			raw values to degrees Celsius. A linear calibration is applied to each thermocouple reading. The readings
 * 			are also processed using an FIR filter. Both immediate and filtered values are available.
 *
 * Virtual Functions Overridden:
 * 	- InitializeHardware
 * 	- ReadFromHardware
 *
 * External Data Members:
 *  - g_pTemperature
 *
 */
class CTemperature: private CThermoCal, private CMcuInfo, private CSpiDriver, private CRndRobinHelper
{

private:
	byte m_thermoRaw[ THERMO_CNT ][ TEMPERATURE_SPI_BYTES ]; //<! The raw data as read from the SPI bus
	int m_firBuffer[ THERMO_CNT ][ TEMPERATURE_FIR_COUNT ]; //<! The buffer for FIR application
	int m_LastTemps[ THERMO_CNT ]; //<! Calibrated EGUs
	int m_FilteredTemps[ THERMO_CNT ]; //<! Calibrated and filtered EGUs
	upErr m_LastErrs[ THERMO_CNT ]; //<! Error from last read
	upErr ConvertThermoTemp( upTempMeas eTemp );
	bool_c m_bHwFault;

public:
	CTemperature();
	upErr InitializeHardware();
	upErr ReadFromHardware();
	upErr GetThermoTemp( upTempMeas eTemp, int * pfTemp );
	upErr GetFilteredThermoTemp( upTempMeas eTemp, int * pfTemp );
	bool_c GetHwFault();
};

// Inline Functions (follows class definition)

#endif /* TEMPERATURE_H_ */

/**
 * @file	CalibrateInterface.h
 * @par		Package: UGMcu
 * @par		Project: Union Generator
 * @par		Copyright (c) 2017, Uptake Medical Technology, Inc.
 * @author	Glen Oct 18, 2011 Created file
 * @brief	This file contains the definition for CCalibrateInterface which is a pure virtual interface class
 */

#ifndef CALIBRATEINTERFACE_H_
#define CALIBRATEINTERFACE_H_

// Include Files
#include "UGError.h"
#include "UGMcuTypes.h"

// Public Macros and Constants
#define LINEAR_CAL_SCALE_FACTOR			(4096) //!< Factor for converting real into integer
#define CNVT_REAL_TO_LINEAR_M(x)		( (long int) ( x * (float) LINEAR_CAL_SCALE_FACTOR ) ) //!< Convenience macro for converting real into integer for 'm' term
#define CNVT_REAL_TO_LINEAR_B(x)		( (long int) ( x * (float) LINEAR_CAL_SCALE_FACTOR ) ) //!< Convenience macro for converting real into integer for 'b' term


// Public Type Definitions (Enums, Structs & Classes)

/**
 * @class	CCalibrateInterface
 * @brief	This class provides an interface definition for calibration.
 *
 *			This is inherited by the classes that provide calibration so that those classes provide uniform interfaces.
 *			This promotes flexibility and re-use as the application code is unaware when the base calibration class is switched.
 *			All of the methods are pure virtual except for the constructor. The constructor has no functionality.
 *
 *
 * This class is a template and accepts four template parameters:
 * - SELECTOR - The type of the variable used to select the calibration data. Likely an enumeration.
 * - MAXCNT - An integer that sizes the stored array and is use to test supplied selectors.
 * - DATATYPE - The type of data being calibrated. This could be a signed or unsigned number of 1, 2 or 4 bytes.
 * - CALDATA - The type of the calibration data. This may be a simple type for an offset or scaling or a structure of data for more complex calibration
 *
 * Virtual Functions Overridden: None @n@n
 *
 * External Data Members: None @n@n
 *
 */
template<typename SELECTOR, int MAXCNT, typename DATATYPE, typename CALDATA>
class CCalibrateInterface
{

public:
	/**
	 * @brief	This is the constructor. It has no functionality.
	 *
	 */
	CCalibrateInterface(){};

	/**
	 * @brief	This is the destructor. It has no functionality.
	 *
	 */
	virtual ~CCalibrateInterface(){};

	/**
	 * @brief	This pure virtual method defines a method for setting calibration factors for a specific selection.
	 *
	 * @param	eSel Chooses the entry to get the cal factors as a SELECTOR
	 * @param	calData The calibration factors as a CALDATA
	 *
	 * @return   ERR_BAD_PARAM if the eSel is out-of-range otherwise ERR_OK
	 */
	virtual upErr SetFactors( SELECTOR eSel, CALDATA calData ) = 0;

	/**
	 * @brief	This pure virtual method defines a method for setting of calibration factors for a specific entry.
	 *
	 * @param	eSel Chooses the entry of the cal factors as a SELECTOR
	 * @param	pCalData A pointer to a CALDATA to receive the calibration factors
	 *
	 * @return   ERR_BAD_PARAM if the eSel is out-of-range or if the pointer is 0 otherwise ERR_OK
	 */
	virtual upErr GetFactors( SELECTOR eSel, CALDATA * pCalData ) = 0;

	/**
	 * @brief	This pure virtual method defines a method for applying a calibration to a value for a specific calibration factor selection.
	 *
	 * @param	eSel Chooses the entry of the cal factors as a SELECTOR
	 * @param	iRaw The uncalibrated value as a DATATYE
	 * @param 	piCalibrated A pointer to a DATATYPE to receive the calibrated value
	 *
	 * @return   ERR_BAD_PARAM if the eSel is out-of-range or if the pointer is 0 otherwise ERR_OK
	 */
	virtual upErr ApplyCal( SELECTOR eSel, DATATYPE iRaw, DATATYPE * piCalibrated ) = 0;

	/**
	 * @brief	This pure virtual method defines a method for setting calibration factors for the zeroth selection.
	 *
	 * @param	calData The calibration factors as a CALDATA
	 *
	 * @return   Status as upErr
	 */
	virtual upErr SetFactors( CALDATA calData ) = 0;

	/**
	 * @brief	This pure virtual method defines a method for retrieving the calibration factors for the zeroth entry.
	 *
	 * @param	pCalData A pointer to a CALDATA to receive the calibration factors
	 *
	 * @return   ERR_BAD_PARAM if the pointer is 0 otherwise ERR_OK
	 */
	virtual upErr GetFactors( CALDATA * pCalData ) = 0;

	/**
	 * @brief	This pure virtual method defines a method to apply a calibration to a value for the zeroth calibration factor selection.
	 *
	 * @param	iRaw The uncalibrated value as a DATATYE
	 * @param 	piCalibrated A pointer to a DATATYPE to receive the calibrated value
	 *
	 * @return   ERR_BAD_PARAM if the pointer is 0 otherwise ERR_OK
	 */
	virtual upErr ApplyCal( DATATYPE iRaw, DATATYPE * piCalibrated ) = 0;

};

#endif /* CALIBRATEINTFACE_H_ */


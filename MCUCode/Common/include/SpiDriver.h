/**
 * @file	SpiDriver.h
 * @par		Package: UGMcu
 * @par		Project: Union Generator
 * @par		Copyright (c) 2017, Uptake Medical Technology, Inc.
 * @author	Glen Sep 15, 2010 Created file
 * @brief	This file declares the CSpiDriver class that reads the chips on the SPI bus
 */

#ifndef SPIDRIVER_H_
#define SPIDRIVER_H_

// Include Files
#include "UGMcuDefinitions.h"
#include "UGMcuTypes.h"
#include "UGError.h"

// Public Type Definitions (Enums, Structs & Classes)

/**
 * @brief	This enumeration identifies the chips on the SPI bus
 *
 */
typedef enum upSpiChipsEnum
{
	SPI_NO_CHIP = 0,
	SPI_THERMO_OUTLET,
	SPI_THERMO_INTERIOR,
	SPI_UNUSED_1,
	SPI_UNUSED_2,
	SPI_THERMO_COIL,
	SPI_THERMO_SPARE,
	SPI_UNUSED_3,
	SPI_CHIP_CNT
} upSpiChips;

/**
 * @class	CSpiDriver
 * @brief	Low-level driver for the SPI bus.
 *
 * It configures the chip ports for the SPI operations. During a read, it asserts the chip selects for the target chip and then
 * 			reads the prescribed number of bytes off the bus
 *
 * Virtual Functions Overridden: None @n@n
 *
 * External Data Members: None @n@n
 *
 */
class CSpiDriver
{
private:
	static bool_c m_bInitialized;					//!< This flag is used to prevent re-initialization by multiple child objects
	static byte m_bSpiChipSelect[ SPI_CHIP_CNT ];	//!< This maps the general enumeration to specific SPI bus enable selections
	upErr AssertChipSelect( upSpiChips eChip );

public:
	CSpiDriver();

protected:
	upErr InitializeSpi();
	upErr ReadSpi( upSpiChips eChip, int iBytes, byte * byteBuf );
};

#endif /* SPIDRIVER_H_ */

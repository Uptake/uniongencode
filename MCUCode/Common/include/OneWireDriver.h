/**
 * @file	OneWireDriver.h
 * @par		Package: UGMcu
 * @par		Project: Union Generator
 * @par		Copyright (c) 2017, Uptake Medical Technology, Inc.
 * @author	Glen Sep 16, 2010 Created file
 * @author  Chris Feb 3, 2011 Added driver code
 * @brief	This file declares the interface to the low-level 1-Wire driver
 */

#ifndef ONEWIREDRIVER_H_
#define ONEWIREDRIVER_H_

// Include Files
#include "RndRobinHelper.h"
#include "TimeBase.h"
#include "UGError.h"
#include "UGMcuTypes.h"
#include "UGMcuEnum.h"
#include "UGMcuDefinitions.h"
#include "LogMessage.h"

// Public Macros and Constants


// Public Type Definitions (Enums, Structs & Classes)
#define TWI_CLOCK_RATE ( 400000UL )
#define TWI_MIN_TWBR ( 10 )
#define OW_ROM_SIZE (8)
#define OW_SCPAD_MAX_BYTES (32)
#define TWI_RX_BUF_MAX	(20)
#define TWI_TX_BUF_MAX  (20)
#define DS2482_CMD_RESET (0xF0)
#define DS2482_SLAVE_ADDR (0x30)
#define DS2482_READ (0x01)
#define DS2482_WRITE  (0x00)
#define TWI_START (0x08)
#define DS2482_RESET_MODE (0x10)
#define DS2482_PPD (0x02)
#define DS2482_SET_RDPTR  (0xE1)
#define DS2482_PTR_RD_DATA (0xE1)
#define DS2482_PTR_CONFIG (0xC3)
#define DS2482_WRITE_CONFIG (0xD2)
#define DS2482_CMD_1WRB (0x96)
#define DS2482_PTR_STATUS (0xF0)
#define DS2482_CMD_1WRST (0xB4)
#define DS2482_CMD_1WWB (0xA5)
#define DS2482_CONFIGURATION (0xE1)
#define OW_READ_ROM (0x33)
#define OW_WRITE_SCRATCHPAD (0x0F)
#define OW_READ_SCRATCHPAD (0xAA)
#define OW_COPY_SCRATCHPAD (0x55)
#define OW_SKIP_ROM (0xCC)
#define OW_READ_MEMORY (0xF0)
#define OW_BUSY ( 0x01 )
#define OW_WRITE_PROTECT (0x55)
#define FLAG_STOP (1)
#define FLAG_NO_STOP (0)
#define OW_DATA_SIZE	( 160 )
#define OW_FACTORY_SIZE ( 63 )
/**
 * Definitions of TWI (effectively I2C) transmit states
 */
typedef enum
{
	I2C_TX_START,
	I2C_TX_REPSTART,
	I2C_TX_DATA,
	I2C_TX_DATA_ACK,
	I2C_TX_STOP,
	I2C_TX_CNT
} upI2cTxStates;


/**
 * Indicates mode of 1-wire operation
 */
typedef enum upOwProgramStateEnum
{
	OW_PRG_MONITOR,
	OW_PRG_ACTIVE
} upOwProgramState;


#define OW_MAX_POLLS ( 1000 )
#define WRITE_PASS_1 ( 0 )
#define WRITE_PASS_2 ( 1 )
#define WRITE_PASS_3 ( 2 )
#define WRITE_PASS_4 ( 3 )

/**
 * @class	COneWireDriver
 * @brief	Bridges the TWI (I2C) port to the 1-wire driver: Maxim DS2482-100
 *
 * Virtual Functions Overridden:
 * 	- ReadFromHardware
 * 	- WriteToHardware
 * 	- InitializeHardware
 *
 * External Data Members: None @n@n
 *
 */
class COneWireDriver: protected CTimerAccess
{

private:
	byte m_romInfo[ OW_ROM_SIZE ];
	byte m_twiBuf[ TWI_TX_BUF_MAX ];
	byte m_twiMsgSize;
	byte m_twiRxCnt;
	byte m_twiBufPtr;
	byte m_twiRepStart;
	byte m_twiState;
	byte m_owPresence;
	byte m_bOffsetByte;
	bool_c m_bHwResponse;
	// Simulation member variables
	byte m_bOwRomIdByte; //!< A variable to store the last byte of the ROM ID for simulator use
	bool_c m_owProgrammed; //!< A flag to indicate that the one-wire is programmed
	bool_c m_owRomIdSet; //!< A flag to indicate that the last byte of the ROM ID has been set
	bool_c m_owStartProgramFlag; //!< A flag to indicate that the programming state-machine is active
	twobytes m_tbIndexSim; //!< An index pointing to the current line of sim data read
	twobytes m_simAddIndex; //!< The address that the sim data should be read into
	byte m_oneWireBufData[ OW_DATA_SIZE ]; //!< A buffer simulating the memory space before the factory portion of the ow memory
	byte m_oneWireBufFactory[ OW_FACTORY_SIZE ]; //!< A buffer simulating the factory portion of the OW memory

public:
	COneWireDriver();
	upErr ReadFromHardware();
	upErr InitializeHardware();

protected:
	upErr GetOwPresence( bool_c * bOwPresence );
	upErr OwReadMem( twobytes iStartAddress, byte * pData, byte bNumBytes );
	upErr OwWriteMem( twobytes iStartAddress, byte * pbData, byte bNumBytes, byte bPass );
	upErr OwGetRomInfo( byte * pbRomInfo );
	upErr ReadyNewOneWire();
	bool_c ReturnHwHealth();
private:
	void ClearRomInfo();
	upErr I2CConfigMaster();
	byte I2CTxcBusy();
	byte I2CGetStateInfo();
	upErr I2CCheckForDevice( byte * pbStatusReg );

	// need an ISR for TWI
	byte I2CTransmit( upI2cTxStates eTxState );
	upErr I2CSendCommand( byte * pbCommands, byte bCmdSize, byte * pbRtnRead, byte bRtnSize, byte bStopFlag );
	upErr OwReadBytes( byte * pbReadBytes, int iNumBytes );
	upErr OwReset( byte * pbOwStatus );
	upErr OwSendReadRom();
	upErr OwSendSkipRom();
	upErr OwWriteByte( byte bByte );
	upErr OwWriteScPad( byte * pTarget, byte * pData, byte bNumBytes );
	upErr OwReadScPad( byte * pTarget, byte * pOffset, byte * pData, byte bNumBytes );
	upErr OwCopyScPad( byte * pTarget, byte bOffset );
	upErr OwPresenceCheck( bool_c * bPresence );
	upErr OwBusyPoll( twobytes * pPollCount, byte bStopFlag );
	byte CrcMask( byte bitNumber );
	byte OwPerformCrc( byte * pBytes, byte numBytes );
	upErr DsConfig( byte * pbConfigReg );
	// These methods are only used in the HdwSim driver
	void WriteToBuffer( twobytes iAddress, byte bByte );
	void ReadFromBuffer( twobytes iAddress, byte * pbByte );
	void ClearOwData();
	upErr GetSimData();
};

// Inline Functions (follows class definition)
/**
 * @brief	This function masks all bits but the bit number in question
 *
 * @param   bitNumber The bit number to keep unmasked
 *
 * @return  The mask is returned
 */
inline byte COneWireDriver::CrcMask( byte bitNumber )
{
	byte maskReturn = 0x01;
	return ( maskReturn << bitNumber );
}

#endif /* ONEWIREDRIVER_H_ */

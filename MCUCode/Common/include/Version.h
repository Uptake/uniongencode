/**
* @file		Version.h
* @par		Package: UGMcu
* @par		Project: Union Generator
* @par		Copyright (c) 2017, Uptake Medical Technology, Inc.
* @author	Chris Keller Nov 2, 2011 Created file
* @brief	This file contains the version ID of the MCU real-time software and SBC simulation software
*/

#ifndef VERSION_H_
#define VERSION_H_

// Public Macros and Constants
#define VERSION_SIMULATION_SBC "0.7.89"					//!< SBC simulation version defined here to be shared with SimUG implementation
#define VERSION_TODAY_MCU "V_" __DATE__ "_" __TIME__

// Change this line when releasing a new version
#define MCU_VERSION_ID VERSION_MCU_CURRENT 		//!< Version of MCU software. It is autogenerated for development builds and hard-coded for releases

// CPLD
#define VERSION_SIMULATION_CPLD "1-1-100-1"
#define VERSION_5_14_11_CPLD "5-14-11-B1"
#define VERSION_8_7_12_CPLD "8-7-12-B1"

// System versions
#define VERSION_SYSTEM_CURRENT "V0.3.7"
#define VERSION_MCU_CURRENT "MCU_V0.3.7"
#define VERSION_SBC_CURRENT "0.1.0" //"0.1.0.21537"
#define VERSION_CPLD_CURRENT VERSION_5_14_11_CPLD // Old CPLD
#define VERSION_SYSTEM_LAB_REVF "Eng Build w/F MCU"
#define VERSION_SYSTEM_LAB_REVG "Eng Build w/G MCU"
#define VERSION_SIMULATION_SYSTEM "V0.2.99"

#endif /* VERSION_H_ */

/**
 * @file	CalibrateTwoLinearPieces.h
 * @par		Package: UGMcu
 * @par		Project: Union Generator
 * @par		Copyright (c) 2017, Uptake Medical Technology, Inc.
 * @author	Glen Oct 24, 2011 Created file
 * @brief	This file contains the definition and declaration for CCalibrateTwoLinearPieces
 */

#ifndef CALIBRATETWOLINEARPIECES_H_
#define CALIBRATETWOLINEARPIECES_H_

// Include Files
#include "CalibrateInterface.h"

// Public Macros and Constants

// Public Type Definitions (Enums, Structs & Classes)

/**
 * @brief	This structure contains the two sets of m and b terms and breakpoint for two-part linear calibration.
 *
 * It is used for calibrating a single parameter which is likely and engineering unit. Both the m and b are long integers and 4 bytes.
 * Both the m and b terms are multiplied by LINEAR_CAL_SCALE_FACTOR before being stored here to get fractional values.
 * The "Lower" value is used for values below the breakpoint and the "Upper"
 * values are used at and above the breakpoint. The breakpoint is a long integer whatever the DATATYPE is.
 *
 */
struct STwoLinearPiecesLongIntFactors
{
	long int ilMslopeLower;
	long int ilBoffsetLower;
	long int ilBreakPoint;
	long int ilMslopeUpper;
	long int ilBoffsetUpper;
};

/**
 * @class	CCalibrateTwoLinearPieces
 * @brief	This class provides a linear calibration function using a simple @a y = @a mx + @a b equation in two
 * 			pieces on either side of a breakpoint.
 *
 * The math is all done in integers. The @a m factors that is supplied must be the desired value multiplied
 * by LINEAR_CAL_SCALE_FACTOR. This class can handle between 1 and n sets of calibration data at one time.
 * It is intended to be used with an enumeration selection that has a specific set of calibration factors applied to
 * a single value.
 *
 * This class is a template based on CCalibrateInterface. It accepts three template parameters which it shares to that parent class:
 * - SELECTOR - The type of the variable used to select the calibration data. Likely an enumeration.
 * - MAXCNT - An integer that sizes the stored array and is use to test supplied selectors.
 * - DATATYPE - The type of data being calibrated. This could be a signed or unsigned number of 1, 2 or 4 bytes.
 *
 * Virtual Functions Overridden:
 * - SetFactors
 * - GetFactors
 * - ApplyCal
 * - SetFactors
 * - GetFactors
 * - ApplyCal
 *
 * External Data Members: None @n@n
 *
 */
template<typename SELECTOR, int MAXCNT, typename DATATYPE>
class CCalibrateTwoLinearPieces: public CCalibrateInterface<SELECTOR, MAXCNT, DATATYPE, STwoLinearPiecesLongIntFactors>
{
private:
	STwoLinearPiecesLongIntFactors calData[ MAXCNT ]; //!< The data storage for calibration factors

protected:
	upErr ApplyCal( SELECTOR eSel, DATATYPE iRaw, DATATYPE * piCalibrated );
	upErr ApplyCal( DATATYPE iRaw, DATATYPE * piCalibrated );

public:
	CCalibrateTwoLinearPieces();
	upErr SetFactors( SELECTOR eSel, STwoLinearPiecesLongIntFactors calData );
	upErr GetFactors( SELECTOR eSel, STwoLinearPiecesLongIntFactors * pCalData );
	upErr SetFactors( STwoLinearPiecesLongIntFactors calData );
	upErr GetFactors( STwoLinearPiecesLongIntFactors * pCalData );

};

// Inline Functions (follows class definition)

/**
 * @brief	This method is the constructor for CCalibrateTwoLinearPieces.
 *
 * It sets the MAXCNT of stored calibration factors to a 1-to-1 relationship. That is, effectively, @a m = 1 and @a b = 0.
 * This is accomplished by setting the @a m term to LINEAR_CAL_SCALE_FACTOR and the @a b term to 0.
 *
 */
template<typename SELECTOR, int MAXCNT, typename DATATYPE>
CCalibrateTwoLinearPieces<SELECTOR, MAXCNT, DATATYPE>::CCalibrateTwoLinearPieces()
{
	for ( int i = 0; i < MAXCNT; i++ )
		{
		calData[ i ].ilMslopeLower = CNVT_REAL_TO_LINEAR_M( 1 );
		calData[ i ].ilBoffsetLower = CNVT_REAL_TO_LINEAR_B( 0 );
		calData[ i ].ilMslopeUpper = CNVT_REAL_TO_LINEAR_M( 1 );
		calData[ i ].ilBoffsetUpper = CNVT_REAL_TO_LINEAR_B( 0 );
		calData[ i ].ilBreakPoint = 0;
		}
}

/**
 * @brief	This method sets calibration factors for a specific selection.
 *
 * The calibration factors are provided as a STwoLinearPiecesLongIntFactors structure. The @a m and @b must be scaled by LINEAR_CAL_SCALE_FACTOR
 * from its floating-point value. Macros are provided for this purpose.
 *
 * @param	eSel Chooses the entry to get the cal factors as a SELECTOR
 * @param	calFactors The calibration factors as a STwoLinearPiecesLongIntFactors
 *
 * @return   ERR_BAD_PARAM if the eSel is out-of-range otherwise ERR_OK
 */
template<typename SELECTOR, int MAXCNT, typename DATATYPE>
upErr CCalibrateTwoLinearPieces<SELECTOR, MAXCNT, DATATYPE>::SetFactors( SELECTOR eSel,
        STwoLinearPiecesLongIntFactors calFactors )
{
	if ( eSel >= MAXCNT || eSel < 0 )
		return ERR_BAD_PARAM;
	calData[ (int) eSel ] = calFactors;
	return ERR_OK;
}

/**
 * @brief	This method retrieves a set of calibration factors for a specific entry.
 *
 * @param	eSel Chooses the entry of the cal factors as a SELECTOR
 * @param	pCalFactors A pointer to a STwoLinearPiecesLongIntFactors struct to receive the calibration factors
 *
 * @return   ERR_BAD_PARAM if the eSel is out-of-range or if the pointer is 0 otherwise ERR_OK
 */
template<typename SELECTOR, int MAXCNT, typename DATATYPE>
upErr CCalibrateTwoLinearPieces<SELECTOR, MAXCNT, DATATYPE>::GetFactors( SELECTOR eSel,
        STwoLinearPiecesLongIntFactors *pCalFactors )
{
	if ( eSel >= MAXCNT || eSel < 0 || 0 == pCalFactors )
		return ERR_BAD_PARAM;
	*pCalFactors = calData[ (int) eSel ];
	return ERR_OK;
}

/**
 * @brief	This method applies a linear calibration to a value for a specific calibration factor selection.
 *
 * @param	eSel Chooses the entry of the cal factors as a SELECTOR
 * @param	iRaw The uncalibrated value as a DATATYE
 * @param 	piCalibrated A pointer to a DATATYPE to receive the calibrated value
 *
 * @return   ERR_BAD_PARAM if the eSel is out-of-range or if the pointer is 0 otherwise ERR_OK
 */
template<typename SELECTOR, int MAXCNT, typename DATATYPE>
upErr CCalibrateTwoLinearPieces<SELECTOR, MAXCNT, DATATYPE>::ApplyCal( SELECTOR eSel, DATATYPE iRaw, DATATYPE *piCalibrated )
{
	long int ilRaw = (long int) iRaw;
	byte bSel = (byte) eSel;

	if ( bSel >= MAXCNT || 0 == piCalibrated )
		return ERR_BAD_PARAM;

	if ( ilRaw < calData[ bSel ].ilBreakPoint )
		{
		*piCalibrated = (DATATYPE) ( ( ilRaw * calData[ bSel ].ilMslopeLower + calData[ bSel ].ilBoffsetLower )
		        / (long int) LINEAR_CAL_SCALE_FACTOR );
		}
	else
		{
		*piCalibrated = (DATATYPE) ( ( ilRaw * calData[ bSel ].ilMslopeUpper + calData[ bSel ].ilBoffsetUpper )
		        / (long int) LINEAR_CAL_SCALE_FACTOR );
		}
	return ERR_OK;
}

/**
 * @brief	This method sets calibration factors for the zeroth selection.
 *
 * The calibration factors are provided as an STwoLinearPiecesLongIntFactors structure. The @a m and @b must be scaled by LINEAR_CAL_SCALE_FACTOR
 * from its floating-point value. Macros are provided for this purpose.
 *
 * @param	calFactors The calibration factors as a STwoLinearPiecesLongIntFactors
 *
 * @return   Status as upErr
 */
template<typename SELECTOR, int MAXCNT, typename DATATYPE>
upErr CCalibrateTwoLinearPieces<SELECTOR, MAXCNT, DATATYPE>::SetFactors( STwoLinearPiecesLongIntFactors calFactors )
{
	return SetFactors( (SELECTOR) 0, calFactors );
}

/**
 * @brief	This method retrieves the calibration factors for the zeroth entry.
 *
 * @param	pCalFactors A pointer to a STwoLinearPiecesLongIntFactors struct to receive the calibration factors
 *
 * @return   ERR_BAD_PARAM if the pointer is 0 otherwise ERR_OK
 */
template<typename SELECTOR, int MAXCNT, typename DATATYPE>
upErr CCalibrateTwoLinearPieces<SELECTOR, MAXCNT, DATATYPE>::GetFactors( STwoLinearPiecesLongIntFactors *pCalFactors )
{
	return GetFactors( (SELECTOR) 0, pCalFactors );
}

/**
 * @brief	This method applies a linear calibration to a value for the zeroth calibration factor selection.
 *
 * @param	iRaw The uncalibrated value as a DATATYE
 * @param 	piCalibrated A pointer to a DATATYPE to receive the calibrated value
 *
 * @return   ERR_BAD_PARAM if the pointer is 0 otherwise ERR_OK
 */
template<typename SELECTOR, int MAXCNT, typename DATATYPE>
upErr CCalibrateTwoLinearPieces<SELECTOR, MAXCNT, DATATYPE>::ApplyCal( DATATYPE iRaw, DATATYPE *piCalibrated )
{
	return ApplyCal( (SELECTOR) 0, iRaw, piCalibrated );
}

#endif /* CALIBRATETWOLINEARPIECES_H_ */

/**
 * @file	Messages.h
 * @par		Package: UGMcu
 * @par		Project: Union Generator
 * @par		Copyright (c) 2017, Uptake Medical Technology, Inc.
 * @author	Chris Keller Dec 20, 2010 Created file
 * @brief	This file contains message type specific structs and enums for the SBC interface
 */

#ifndef MESSAGES_H_
#define MESSAGES_H_

// Include Files
#include "UGMcuDefinitions.h"

// Public Type Definitions (Enums, Structs & Classes)

// These definitions are only used along with this header file
#if defined (__i686__)
typedef int signedfourbytes;
typedef short int signedtwobytes;
#else
typedef long int signedfourbytes;
typedef int signedtwobytes;
#endif

// Public Macros and Constants

// Message definitions
#define VERSION_MSG_LENGTH_SERIAL ( 20 )	//!< Length of serial comm version
#define VERSION_MSG_LENGTH_MCU ( 20 )		//!< Length of MCU field
#define VERSION_MSG_LENGTH_CPLD ( 20 )		//!< Length of CPLD version field
#define VERSION_MSG_LENGTH_SYSTEM ( 20 )	//!< Length for system version field
#define VERSION_MSG_LENGTH_SBC ( 20 )		//!< Length of version for the SBC
#define OW_LENGTH_MODEL ( 16 )				//!< Length of model string
#define OW_LENGTH_LOT  ( 12 )          		//!< The current mfgr lot size is 8 chars.  4 bytes are reserved for future use.


// Serial Interface definitions
#define MAX_MESSAGE_SIZE (sizeof(UAllSbcStructs)/sizeof(byte))
#define PROTOCOL_OVERHEAD (8)
#define MAX_PACKET_SIZE (MAX_MESSAGE_SIZE + PROTOCOL_OVERHEAD)
#define PACKET_QUEUE_MAX_SIZE (MAX_PACKET_SIZE)
#define PACKET_START_FLAG (0xFE)
#define PACKET_END_FLAG (0xFD)

/**
 * @brief This enum defines the types of data and subsequently the type of packet in a message
 *
 */
typedef enum upMessageTypesEnum
{
	MSG_TYPE_SBCCOND = 0x00,
	MSG_TYPE_OPCOND = 0x01,
	MSG_TYPE_LOG = 0x02,
	MSG_TYPE_SETUP = 0x03,
	MSG_TYPE_MCUINFO = 0x04,
	MSG_TYPE_HPINFO = 0x05,
	MSG_TYPE_SBCCMD = 0x06,
	MSG_TYPE_TREATMENTINFO = 0x07,
	MSG_TYPE_USERMSG = 0x08,
	MSG_TYPE_DELIVERYSTEPS = 0xA0,
	MSG_TYPE_TREATSELECT = 0xA1,
	MSG_TYPE_CNT,
	MSG_TYPE_NOTSPECIFIED = 0xFF
} upMessageTypes;

/**
 * @brief Serial Interface frame indicies
 */
typedef enum upFrameIndicesEnum
{
	FRAME_START_FLAG = 0,
	FRAME_SEQUENCE,
	FRAME_MSGTYPE,
	FRAME_REQUEST,
	FRAME_LENGTH,
	FRAME_PAYLOAD
} upFrameIndices;

/**
 * @brief Serial Interface intra-packet states
 */
typedef enum upPacketStatesEnum
{
	SYNCH = 0,
	SEQUENCEACK,
	MSGTYPE,
	REQUEST,
	LENGTH,
	DATA,
	CHECKSUM1,
	CHECKSUM2,
	END,
	PACKET_SUCCESSFUL,
	PACKET_ERROR,
	NACK_RECEIVED,
	STATE_CNT
} upPacketStates;

/**
 * @brief This structure defines the contents of the "MSG_TYPE_SBCCOND" packet
 */
struct SSBCCond
{
	byte screenState;
	uint32_t sbcTime;
	char sbcVersion[ VERSION_MSG_LENGTH_SBC ];
};

/**
 * @brief This structure defines the contents of the Operating Condition "MSG_TYPE_OPCOND" packet
 */
struct SOpconMsg
{
	byte operState;
	byte operSubState;
	byte connMask;
	twobytes treatCnt;
	twobytes maxTreatments;
	signedfourbytes waterRate;
	signedtwobytes stbUnused0;
	signedtwobytes stbUnused1;
	signedtwobytes coilTemp;
	signedtwobytes outletTemp;
	signedtwobytes intTemp;
	twobytes tbRfPower;
	signedtwobytes iRfTemperature;
	twobytes tbRfStatus;
	twobytes tbRfError;
	byte digInput;
	twobytes digOutput;
	signedtwobytes motorVelocity;
	signedfourbytes encoderPos;
	byte logMsgs;
	byte userMsgs;
	twobytes runTicks;
	byte progress;
	signedtwobytes firCoilTemp;
	signedtwobytes firOutletTemp;
	byte unusedState;
	twobytes ddTreatRemaining;
	unsignedfourbytes ddTimeRemaining;
};

/**
 * @brief This structure defines the contents of the "MSG_TYPE_LOG" packet
 */
typedef struct SLogMsg
{
	byte message[ LOG_MESSAGE_LENGTH ];
	byte msgLength;
} SLogMsgType;

/**
 * @brief This structure defines the contents of the "MSG_TYPE_SETUP" packet
 */
struct SSetupMsg
{
	signedtwobytes CoilTempLower;
	signedtwobytes CoilUpperTemp;
	signedtwobytes CoilTempShutdownTol;
	signedtwobytes OutletTempMax;
	signedtwobytes OutletTempMin;
	signedtwobytes OutletTempShutdownTol;
	signedtwobytes MaxInternalTemp;
	signedtwobytes OverTempBand;
	signedtwobytes stbCustom14;
	twobytes tbCustom24;
	twobytes tbCustom25;
	signedtwobytes WrateIdle;
	signedtwobytes WratePrime1;
	signedtwobytes WratePrime2;
	signedtwobytes WratePrime3;
	twobytes Vprime1;
	twobytes Vprime2;
	twobytes tbCustom23;
	signedtwobytes stbCustom15;
	signedtwobytes stbCustom16;
	signedtwobytes stbCustom17;
	signedtwobytes stbCustom18;
	signedtwobytes stbCustom19;
	signedtwobytes stbCustom20;
	byte bCustom22;
	byte Tdeb;
	byte bCustom6;
	twobytes Trest;
	twobytes TminValueTest;
	twobytes TminPrimeValueTest;
	twobytes uiCustom13;
	twobytes uiCustom21;
	twobytes TtempWindow;
	twobytes Tdelivery;
	twobytes Tvapor;
	twobytes DeliveryCount;
	twobytes RFpowerTol;
	twobytes RFpowerIdle;
	twobytes uiCustom7;
	twobytes Asyringe;
	twobytes EmcCycles;
	twobytes uiCustom12;
	twobytes Tprecond;
	twobytes TdeliveryMin;
	twobytes TdeliveryMax;
	twobytes uiCustom4;
	twobytes uiCustom5;
	signedtwobytes TempUpperLimitIdle;
	signedtwobytes TempLowerLimitIdle;
	signedtwobytes stbCustom8;
	signedtwobytes stbCustom9;
	signedtwobytes stbCustom10;
};

/**
 * @brief This structure defines the contents of the "MSG_TYPE_MCUINFO" packet
 */
struct SMcuInfo
{
	char mcuSerial[ VERSION_MSG_LENGTH_SERIAL ];
	char mcuVersion[ VERSION_MSG_LENGTH_MCU ];
	char cpldVersion[ VERSION_MSG_LENGTH_CPLD ];
	char systemVersion[ VERSION_MSG_LENGTH_SYSTEM ];
};

/**
 * @brief This structure defines the contents of the "MSG_TYPE_HPINFO" packet
 */
struct SHpInfo
{
	byte therapyCode;
	char lotNumber[ OW_LENGTH_LOT ];
	unsignedfourbytes hpSerial;
	char handpieceModel[ OW_LENGTH_MODEL ];
	unsignedfourbytes connectTime;
	unsignedfourbytes therapyTime; // milliseconds
	twobytes treatmentCnt;
	twobytes maxTreatCount;
	twobytes maxUseTime; // seconds
};

/**
 * @brief This structure defines the contents of the "MSG_TYPE_SBCCMD" packet
 */
struct SSbcCmd
{
	byte command;
};

/**
 * @brief This structure defines the contents of the "MSG_TYPE_TREATMENTINFO" packet
 */
struct STreatmentInfoMsg
{
	twobytes treatIdx;
	twobytes fullTreatments;
	twobytes totalTreatments; // prime not counted here
	byte status;
	twobytes treatmentTime; // time of the last treatment in milliseconds
	unsignedfourbytes totalUseTime; // milliseconds
	twobytes maxTreatCount;
	twobytes maxUseTime; // seconds
};

/**
 * @brief This structure defines the contents of the "MSG_TYPE_USERMSG" packet
 */
struct SUserMsg
{
	twobytes code;
	byte severity;
	int value1;
	int value2;
};

/**
 * @brief This structure defines the contents of the "MSG_TYPE_DELIVERYSTEPS" packet that carries the multi-step delivery parameters
 */
struct SDeliverySteps
{
	byte count;
	twobytes time[ DELIVERY_STEPS_MAX ];
	twobytes power[ DELIVERY_STEPS_MAX ];
	signedtwobytes wRate[ DELIVERY_STEPS_MAX ];
};

/**
 * @brief This structure defines the contents of the "MSG_TYPE_TREATMENT_SELECT" packet that carries the multi-step delivery parameters
 */
struct STreatmentSelection
{
	twobytes TtreatStatus; 	//!< The status of the parameter as determined by the sender of the packet
	twobytes Ttreat; //!< The time selection is tenths of a second. 3 seconds is a value of 30.
	twobytes unused2Status; //!< The status of the parameter as determined by the sender of the packet
	twobytes unused2;		//!< Unused value 2
	twobytes unused3Status; //!< The status of the parameter as determined by the sender of the packet
	twobytes unused3;		//!< Unused value 3
	twobytes unused4Status; //!< The status of the parameter as determined by the sender of the packet
	twobytes unused4;		//!< Unused value 4
};

/**
 * @brief This union is created to find the largest structure for
 * sizing buffers.
 *
 * It is unused otherwise and is not declared as a variable.
 */
typedef union
{
	struct SOpconMsg sOpCon;
	struct SSBCCond sSbcState;
	struct SSetupMsg sSetup;
	struct SLogMsg sLog;
	struct SMcuInfo sMcuInfo;
	struct SHpInfo sHpInfo;
	struct STreatmentInfoMsg sTreat;
	struct SUserMsg sUser;
	struct SSbcCmd sSbcCmd;
	struct SDeliverySteps sDeliverySteps;
	struct STreatmentSelection sTreatmentConfig;
} UAllSbcStructs;

// Inline Functions (follows class definition)


#endif /* MESSAGES_H_ */

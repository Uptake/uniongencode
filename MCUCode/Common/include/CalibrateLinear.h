/**
 * @file	CalibrateLinear.h
 * @par		Package: UGMcu
 * @par		Project: Union Generator
 * @par		Copyright (c) 2017, Uptake Medical Technology, Inc.
 * @author	Glen Oct 24, 2011 Created file
 * @brief	This file contains contains the declaration and definition of the CCalibrateLinear template.
 */

#ifndef CALIBRATELINEAR_H_
#define CALIBRATELINEAR_H_

// Include Files
#include "CalibrateInterface.h"

// Public Macros and Constants

// Public Type Definitions (Enums, Structs & Classes)

/**
 * @brief	This structure contains the m and b terms for linear calibration.
 *
 * It is used for calibrating a single parameter which is likely and engineering unit. Both the m and b are long integers and 4 bytes.
 * Both the m and b terms are multiplied by LINEAR_CAL_SCALE_FACTOR before being stored here to get fractional values.
 *
 */
struct SLinearLongIntFactors
{
	long int ilMslope;
	long int ilBoffset;
};

/**
 * @class	CCalibrateLinear
 * @brief	This class provides a linear calibration function using a simple @a y = @a mx + @a b equation.
 *
 * The math is all done in integers. The @a m factor and @a b offset that are supplied must be the desired value multiplied
 * by LINEAR_CAL_SCALE_FACTOR. The macros CNVT_REAL_TO_LINEAR_M and CNVT_REAL_TO_LINEAR_B are provided to do this. It is recommended
 * to use the macros in case the underlying implementation is changed.
 * @n
 * This class can handle between 1 and n sets of calibration data at one time.
 * It is expected to be used with an enumeration selection that has a specific set of calibration factors applied to
 * a single value. The data size must be allocated to be the size of the largest enumeration value and not the count of enumerations.
 * @n
 * This class is a template based on CCalibrateInterface. It accepts three template parameters which it shares to that parent class:
 * - SELECTOR - The type of the variable used to select the calibration data. Likely an enumeration but can be any integral type.
 * - MAXCNT - An integer that sizes the stored array and is use to range-test supplied selectors.
 * - DATATYPE - The type of data being calibrated. This could be an signed or unsigned numeric type of 1, 2 or 4 bytes.
 *
 * Virtual Functions Overridden:
 * - SetFactors
 * - GetFactors
 * - ApplyCal
 * - SetFactors
 * - GetFactors
 * - ApplyCal
 *
 * External Data Members: None @n@n
 *
 */
template<typename SELECTOR, int MAXCNT, typename DATATYPE>
class CCalibrateLinear: public CCalibrateInterface<SELECTOR, MAXCNT, DATATYPE, SLinearLongIntFactors>
{
private:
	SLinearLongIntFactors calData[ MAXCNT ]; //!< The data storage for calibration factors

protected:
	upErr ApplyCal( SELECTOR eSel, DATATYPE iRaw, DATATYPE * piCalibrated );
	upErr ApplyCal( DATATYPE iRaw, DATATYPE * piCalibrated );

public:
	CCalibrateLinear();
	upErr SetFactors( SELECTOR eSel, SLinearLongIntFactors calData );
	upErr GetFactors( SELECTOR eSel, SLinearLongIntFactors * pCalData );
	upErr SetFactors( SLinearLongIntFactors calData );
	upErr GetFactors( SLinearLongIntFactors * pCalData );

};

// Inline Functions (follows class definition)

/**
 * @brief	This method is the constructor for CCalibrateLinear.
 *
 * It sets the MAXCNT of stored calibration factors to a 1-to-1 relationship. That is, effectively, @a m = 1 and @a b = 0.
 * This is accomplished by setting the @a m term to LINEAR_CAL_SCALE_FACTOR and the @a b term to 0.
 *
 */
template<typename SELECTOR, int MAXCNT, typename DATATYPE>
CCalibrateLinear<SELECTOR, MAXCNT, DATATYPE>::CCalibrateLinear()
{
	for ( int i = 0; i < MAXCNT; i++ )
		{
		calData[ i ].ilMslope = CNVT_REAL_TO_LINEAR_M( 1 );
		calData[ i ].ilBoffset = CNVT_REAL_TO_LINEAR_B( 0 );
		}
}

/**
 * @brief	This method sets calibration factors for a specific selection.
 *
 * The calibration factors are provided as a SLinearLongIntFactors structure. The @a m and @b must be scaled by LINEAR_CAL_SCALE_FACTOR
 * from its floating-point value. Macros are provided for this purpose.
 *
 * @param	eSel Chooses the entry to get the cal factors as a SELECTOR
 * @param	calFactors The calibration factors as a SLinearLongIntFactors
 *
 * @return   ERR_BAD_PARAM if the eSel is out-of-range otherwise ERR_OK
 */
template<typename SELECTOR, int MAXCNT, typename DATATYPE>
upErr CCalibrateLinear<SELECTOR, MAXCNT, DATATYPE>::SetFactors( SELECTOR eSel, SLinearLongIntFactors calFactors )
{
	if ( eSel >= MAXCNT || eSel < 0 )
		return ERR_BAD_PARAM;
	calData[ (int) eSel ] = calFactors;
	return ERR_OK;
}

/**
 * @brief	This method retrieves a set of calibration factors for a specific entry.
 *
 * @param	eSel Chooses the entry of the cal factors as a SELECTOR
 * @param	pCalFactors A pointer to a SLinearLongIntFactors struct to receive the calibration factors
 *
 * @return   ERR_BAD_PARAM if the eSel is out-of-range or if the pointer is 0 otherwise ERR_OK
 */
template<typename SELECTOR, int MAXCNT, typename DATATYPE>
upErr CCalibrateLinear<SELECTOR, MAXCNT, DATATYPE>::GetFactors( SELECTOR eSel, SLinearLongIntFactors *pCalFactors )
{
	if ( eSel >= MAXCNT || eSel < 0 || 0 == pCalFactors )
		return ERR_BAD_PARAM;
	*pCalFactors = calData[ (int) eSel ];
	return ERR_OK;
}

/**
 * @brief	This method applies a linear calibration to a value for a specific calibration factor selection.
 *
 * @param	eSel Chooses the entry of the cal factors as a SELECTOR
 * @param	iRaw The uncalibrated value as a DATATYE
 * @param 	piCalibrated A pointer to a DATATYPE to receive the calibrated value
 *
 * @return   ERR_BAD_PARAM if the eSel is out-of-range or if the pointer is 0 otherwise ERR_OK
 */
template<typename SELECTOR, int MAXCNT, typename DATATYPE>
upErr CCalibrateLinear<SELECTOR, MAXCNT, DATATYPE>::ApplyCal( SELECTOR eSel, DATATYPE iRaw, DATATYPE *piCalibrated )
{
	if ( eSel >= MAXCNT || 0 == piCalibrated )
		return ERR_BAD_PARAM;

	*piCalibrated = (DATATYPE) ( ( (long int) iRaw * (long int) calData[ (int) eSel ].ilMslope + calData[ (int) eSel ].ilBoffset  )
	        / (long int) LINEAR_CAL_SCALE_FACTOR );
	return ERR_OK;
}

/**
 * @brief	This method sets calibration factors for the zeroth selection.
 *
 * The calibration factors are provided as an SLinearLongIntFactors structure. The @a m and @b must be scaled by LINEAR_CAL_SCALE_FACTOR
 * from its floating-point value. Macros are provided for this purpose.
 *
 * @param	calFactors The calibration factors as a SLinearLongIntFactors
 *
 * @return   Status as upErr
 */
template<typename SELECTOR, int MAXCNT, typename DATATYPE>
upErr CCalibrateLinear<SELECTOR, MAXCNT, DATATYPE>::SetFactors( SLinearLongIntFactors calFactors )
{
	return SetFactors( (SELECTOR) 0, calFactors );
}

/**
 * @brief	This method retrieves the calibration factors for the zeroth entry.
 *
 * @param	pCalFactors A pointer to a SLinearLongIntFactors struct to receive the calibration factors
 *
 * @return   ERR_BAD_PARAM if the pointer is 0 otherwise ERR_OK
 */
template<typename SELECTOR, int MAXCNT, typename DATATYPE>
upErr CCalibrateLinear<SELECTOR, MAXCNT, DATATYPE>::GetFactors( SLinearLongIntFactors *pCalFactors )
{
	return GetFactors( (SELECTOR) 0, pCalFactors );
}

/**
 * @brief	This method applies a linear calibration to a value for the zeroth calibration factor selection.
 *
 * @param	iRaw The uncalibrated value as a DATATYE
 * @param 	piCalibrated A pointer to a DATATYPE to receive the calibrated value
 *
 * @return   ERR_BAD_PARAM if the pointer is 0 otherwise ERR_OK
 */
template<typename SELECTOR, int MAXCNT, typename DATATYPE>
upErr CCalibrateLinear<SELECTOR, MAXCNT, DATATYPE>::ApplyCal( DATATYPE iRaw, DATATYPE *piCalibrated )
{
	return ApplyCal( (SELECTOR) 0, iRaw, piCalibrated );
}

#endif /* CALIBRATELINEAR_H_ */

/**
 * @file       UGError.h
 * @par        Package: UGMcu
 * @par        Project: Union Generator
 * @par        Copyright (c) 2017, Uptake Medical Technology, Inc.
 * @author     Glen Sep 15, 2010 Created file
 * @brief      This file defines the error enumeration upErr
 */

#ifndef UGMCUERROR_H_
#define UGMCUERROR_H_

/**
 * This macro will cause a return() to be executed if the result is not ERR_OK
 */
#define RETURN_IF_ERROR( x ) { upErr rtnErr=x; if ( ERR_OK != rtnErr ) return rtnErr; }

#define USER_ERROR_START ( 200 )	//!< Errors below this value do not produce a user log message for use by the SBC
/**
 * This enumeration includes all of the error codes returned by methods.
 *
 * User errors have assigned values starting at 200
 */
typedef enum upErrEnum
{
	ERR_OK = 1,

	// Errors without user alerts

	ERR_ERROR = 2, // Common errors in general use
	ERR_UNSPECIFIED,
	ERR_ENUM_OUT_OF_RANGE,
	ERR_TIMEOUT,
	ERR_NOT_READY,
	ERR_BAD_PARAM, // need to watch out for this one, config could cause this

	// IPC errors
	ERR_IPC_PAYLOAD_OVERFLOW = 20,
	ERR_IPC_START_FLAG_FAIL,
	ERR_IPC_END_FLAG_FAIL,
	ERR_IPC_MINOR_TIMEOUT,
	ERR_IPC_OUT_QUEUE_FULL,
	ERR_IPC_QUEUE_ERROR,
	ERR_IPC_RECV_UNEXPECTED,

	// 1-Wire errors
	ERR_OW_DATA_READ_ERROR = 30,
	ERR_OW_DATA_WRITE_ERROR,
	ERR_OW_EVENT_Q_FULL,
	ERR_OW_LOCKED,

	// User message errors
	ERR_USER_MSG_QUEUE_FULL = 40,
	ERR_USER_MSG_QUEUE_EMPTY,

	// RF Generator errors
	ERR_RF_GEN_BAD_RESP = 50,
	ERR_RF_GEN_BAD_CHCK,
	ERR_RF_GEN_NOT_READY,
	ERR_LOCAL_RF_GEN_OPER_FAILURE,
	ERR_LOCAL_RF_GEN_POWER_LEVEL,
	ERR_LOCAL_RF_GEN_COMM_FATAL,

	// Operating condition errors
	ERR_THERMO_UNDER_TEMP = 60,
	ERR_THERMO_OVER_TEMP,
	ERR_THERMO_OVER_TEMP_PERSIST,
	ERR_IDLE_THERMO_UNDER_TEMP,
	ERR_IDLE_THERMO_OVER_TEMP,

	// Quag SPI errors
	ERR_QSPI_READ = 60,
	ERR_QSPI_WRITE,
	ERR_QSPI_CRC,

	// USB errors
	ERR_USB_READ = 60,
	ERR_USB_WRITE,
	ERR_USB_DEVICE,

	// Specific errors in general use
	ERR_LOG_QUEUE_FULL = 100,
	ERR_TRIGGER_OFF,
	ERR_INTERLOCK_MISSING,
	ERR_SBC_STATE_WRONG,
	ERR_EEPROM_BUFFER_FULL,
	ERR_NO_DDEVICE,
	ERR_DDEVICE_DISABLED,
	ERR_BOARD_ASSEMBLY_UNKNOWN,
	ERR_THERMO_INTERLOCK_MISSING,
	ERR_RETRACTING_PUMP,
	ERR_NO_STEPS,

	// User errors
	ERR_DDEVICE_READ_ERROR = 200,
	ERR_DDEVICE_WRITE_ERROR = 205,
	ERR_THERMO_ERROR = 210,
	ERR_DDEVICE_UNKNOWN = 215,
	ERR_DDEVICE_COIL = 218,
	ERR_DDEVICE_EXPIRED_TREAT = 220,
	ERR_DDEVICE_INVALID = 225,
	ERR_DDEVICE_EXPIRED_TIME = 230,
	ERR_PRIME_TEMP_LOW = 235,
	ERR_PRIME_WP_LOW = 240,
	ERR_PRIME_WP_HIGH = 241,
	ERR_PRIME_OVER_TEMP = 245,
	ERR_PRIME_TRIGGER_OFF = 250,
	ERR_ABLATE_UNDER_TEMP = 255,
	ERR_TREAT_WP_HIGH = 260,
	ERR_TREAT_WP_LOW = 265,
	ERR_PUMP_AT_LIMIT = 270,
	ERR_ABLATE_OVER_TEMP = 280,
	ERR_IDLE_UNDER_TEMP_USR = 285,

	// RF generator reset error
	ERR_RF_GEN_RESET = 350,

	// Delivery time out-of-range
	ERR_BAD_TIME = 360,
	ERR_PRECOND_TIMEOUT = 361,

	// Terminal errors
	ERR_RF_GEN_POWER_LEVEL = 400,
	ERR_RF_GEN_NOT_FOUND = 405,
	ERR_IPC_CRITICAL_TIMEOUT = 425,
	ERR_IPC_CONNECTION_TIMEOUT = 426,
	ERR_RF_GEN_COMM_FATAL = 430,
	ERR_RND_ROBIN_OVERRUN = 435,
	ERR_SELFTEST_CPLD = 440, // internal communication error
	ERR_I2C_DEVICE_ERROR = 450,
	ERR_SELFTEST_WATER_PUMP = 460,
	ERR_EXCESSIVE_TEMP = 470,
	ERR_VERSION_CHECK = 475,
	ERR_INTERNAL_FAULT = 480,
	ERR_GENERATOR_OVER_TEMP = 485,
	ERR_RF_GEN_OPER_FAILURE = 490,
	UG_ERROR_CNT
} upErr;

#endif /*UGMCUERROR_H_*/

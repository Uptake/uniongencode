/**
 * @file	UGMcuTypes.h
 * @par		Package: UGMcu
 * @par		Project: Union Generator
 * @par		Copyright (c) 2017, Uptake Medical Technology, Inc.
 * @author	Glen Sep 15, 2010 Created file
 * @brief	This file declares custom types for the Union Generator MCU software
 */

#ifndef UGMCUTYPES_H_
#define UGMCUTYPES_H_

// Include Files
#include "stdint.h"

// Public Type Definitions (Enums, Structs & Classes)
typedef uint8_t byte;		//!< Define "byte" type as an 8-bit unsigned value
typedef uint16_t twobytes;	//!< Define "twobytes" type as a 16-bit unsigned value
typedef uint32_t time;		//!< Define "time" type as a 32-bit unsigned value
typedef uint32_t tick;		//!< Define "tick" type as a 32-bit unsigned value
typedef uint8_t bool_c;		//!< Define "bool_c" type as an 8-bit unsigned value
typedef uint32_t unsignedfourbytes;	//!< Define "unsignedfourbytes" type as a 32-bit unsigned value
#define TICK_MAX	( ~(tick) 0 )	//!< This is the maximum value for a tick type as it is an unsigned value
#define TIME_MAX	( ~(time) 0 ) //!< This is the maximum value for a time type as it is an unsigned value

#endif /* UGMCUTYPES_H_ */

/**
 * @file	UGMcuEnum.h
 * @par		Package: UGMcu
 * @par		Project: Union Generator
 * @par		Copyright (c) 2017, Uptake Medical Technology, Inc.
 * @author	Glen Oct 18, 2010 Created file
 * @brief	This file contains enumerations for states in the supervisor and handpiece codes
 */

#ifndef UGMCUENUM_H_
#define UGMCUENUM_H_

// Public Macros and Constants

/**
 * The top-level states for the generator
 */
typedef enum upOperStateEnum
{
	OPER_STATE_BOOT = 0,
	OPER_STATE_TEST,
	OPER_STATE_IDLE,
	OPER_STATE_CNCTD,
	OPER_STATE_HAND_PRIMING,
	OPER_STATE_READY,
	OPER_STATE_DELIVERING,
	OPER_STATE_ERROR,
	OPER_STATE_PENDING,
	OPER_STATE_HAND_PRIMED,
	OPER_STATE_CATH_PRIMING,
	OPER_STATE_SETUP,
	OPER_STATE_PRECOND,
	OPER_STATE_RELEASE,
	OPER_STATE_CNT
} upOperState;

/**
 * Sub-states when in OPER_STATE_PENDING
 */
typedef enum upPendingReadyEnum
{
	PENDING_READY_ERR_OK = 0,
	PENDING_READY_WAIT = 1,
	PENDING_READY_WRONG_SBC = 2,
	PENDING_READY_OVER_TEMP = 3,
	PENDING_READY_TRIG_ON = 4,
	PENDING_READY_CNT
} upPendingReady;

/**
 * Indicates the reason and results of an ablation operation including priming, full delivery and partial delivery cases.
 */
typedef enum upDeliveryStatusEnum
{
	DELIVERY_PARTIAL_NO_ERR = 0x00,
	DELIVERY_FULL = 0x01,
	DELIVERY_PRIME = 0x02,
	DELIVERY_PARTIAL_ERROR = 0x03,
	DELIVERY_NONE = 0xFF,
} upDeliveryStatus;


/**
 * Code that will be read to specify the therapy for the tool that is attached
 */
typedef enum upTreatmentCodeEnum
{
	TREATMENT_UNSET = 0,
	TREATMENT_CUSTOM = 1,
	TREATMENT_V_1_0 = 2,
	TREATMENT_SPECIAL_1 = 3,
	TREATMENT_SPECIAL_2 = 4,
	TREATMENT_SPECIAL_3 = 5,
	TREATMENT_SPECIAL_4 = 6,
	TREATMENT_EMC = 7,
	TREATMENT_UPTAKE_DEV_1 = 8,
	TREATMENT_V_1_1 = 9,
	TREATMENT_SHAM_V_1_0 = 10,
	TREATMENT_ENGINEERING = 11,
	TREATMENT_CNT = 12,
#ifdef ALLOW_EMC_TREATMENT_CODE
	TREATMENT_DEFAULT = TREATMENT_EMC
#else
	TREATMENT_DEFAULT = TREATMENT_V_1_0
#endif
	} upTreatmentCode;

/**
 * Generic state definition for use in small or shared sub-states
 */
typedef enum upGenericStateEnumType
{
	GEN_STATE_START,
	GEN_STATE_FIRST_ACTION,
	GEN_STATE_SECOND_ACTION,
	GEN_STATE_THIRD_ACTION,
	GEN_STATE_FOURTH_ACTION,
	GEN_STATE_FIFTH_ACTION,
	GEN_STATE_SIXTH_ACTION,
	GEN_STATE_SEVENTH_ACTION,
	GEN_STATE_EIGHTH_ACTION,
	GEN_STATE_NINETH_ACTION,
	GEN_STATE_TENTH_ACTION,
	GEN_STATE_END,
	GEN_STATE_COMPLETE,
	GEN_STATE_ERROR,
	GEN_STATE_COUNT
} upGenericState;

/**
 * The states of the SBC GUI
 */
typedef enum upSbcStateEnum
{
	SBC_STATE_OOS = 0,
	SBC_STATE_PRIME = 3,
	SBC_STATE_DELIVERY = 4,
	SBC_STATE_TECH_DIAG = 5,
	SBC_STATE_OTHER = 6,
	SBC_STATE_CNT
} upSbcState;

#endif /* UGMCUENUM_H_ */

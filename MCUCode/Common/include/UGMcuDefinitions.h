/**
 * @file	UGMcuDefinitions.h
 * @par		Package: UGMcu
 * @par		Project: Union Generator
 * @par		Copyright (c) 2017, Uptake Medical Technology, Inc.
 * @author	Glen Sep 17, 2010 Created file
 * @brief	This file contains global definitions of compiler directives, values and conversion macros used across the code base
 */

#ifndef UGMCUDEFINITIONS_H_
#define UGMCUDEFINITIONS_H_


// Public Macros and Constants


// External Memory Support
#define XMEM_START_ADDR		(0x8000)	//!< Lowest address of the external SRAM memory
#define XMEM_END_ADDR		(0xFFFF)	//!< Highest address of the external SRAM
#define XMEM_SECTION __attribute__ ((section (".xmem"))) //!< Fragment that places following variable or object into external RAM

//
// Build options
//

// Define this symbol to allow EMC therapy code
//#define ALLOW_EMC_TREATMENT_CODE	//!< Enables the EMC option for repetitive automatic treatment execution and allows the EMC therapy code

// Define for lab and special use
//#define SIM_OUTPUT_CAPTURE          	(1)
#define DISABLE_CRC_FOR_DEBUGGING 	  	(1)
#define DISABLE_RDO_VERSION_CHECK 	  	(1)
#define ENABLE_EASY_ENGINEERING_MODE	(1)
#define RDO_SPECIAL_CAL_600				(1)
#define ENABLE_RDO_CURRENT_READ			(1)
#define ENABLE_RDO_CURRENT_CNTL			(1)

//
#define RF_GEN_ERROR_TERMINATE_CNT 		(3)				// Teminate on the third RF generator error
#define DELIVERY_STEPS_MAX (30)				//!< Maximum number of steps allowed in a delivery

// Numerical definitions for time base
#define F_CPU (100000)
#define SYSTEM_CLOCK_RATE       ( F_CPU )	//!< Internal symbol for CPU clock rate
#define TICK_CLOCK_RATE         (1000)		//!< The clock rate for the tick timer in Hertz
#define SECS_PER_MIN			(60)		//!< Number of seconds in a minute
#define TENTHS_PER_SEC			(10)		//!< Tenths of seconds in a second
#define MS_PER_TENTHS			(100)		//!< Milliseconds in a tenths of a second
#define MS_PER_SEC				(1000)		//!< Milliseconds in a second
#define SEC_TO_MIN(x)         	( (x) / SECS_PER_MIN )		//!< Conversion of seconds to minutes
#define MIN_TO_SECS(x)         	( (x) * SECS_PER_MIN )		//!< Conversion of minutes to seconds
#define SEC_TO_MS(x)         	( (x) * (time) MS_PER_SEC )		//!< Conversion of seconds to milliseconds
#define SECS_TO_TICKS(x)        ( (x) * (tick) TICK_CLOCK_RATE )	//!< Conversion of seconds to ticks
#define MS_TO_TICKS(x)          ( (x) * ( TICK_CLOCK_RATE / MS_PER_SEC ) ) 	//!< Conversion of milliseconds to ticks
#define TICKS_TO_MS(x)          ( (x) * ( MS_PER_SEC / TICK_CLOCK_RATE ) )	//!< Conversion of ticks to milliseconds
#define TICKS_TO_SEC(x)         ( (x) / TICK_CLOCK_RATE )					//!< Conversion of ticks to seconds
#define TICKS_TO_SEC_TENTHS(x)  ( TICKS_TO_SEC( TENTHS_PER_SEC * (tick) x ) )		//!< Conversion of ticks to tenths of seconds
#define SEC_TENTHS_TO_TICKS(x)  ( MS_TO_TICKS( MS_PER_TENTHS * x ) )		//!< Conversion of ticks to tenths of seconds
#define SEC_TENTHS_TO_MS(x)  	( MS_PER_TENTHS * x ) 						//!< Conversion of tenths of seconds to milliseconds
#define RND_ROBIN_LOOP_MS       (50)										//!< Milliseconds for each round robin loop time
#define RND_ROBIN_LOOP_HZ       ( MS_PER_SEC / RND_ROBIN_LOOP_MS )			//!< Round robin period is Hertz
#define RND_ROBIN_TICK_CNT      ( MS_TO_TICKS( RND_ROBIN_LOOP_MS ) )		//!< Ticks in each round robin cycle
#define	MS_TO_RND_ROBIN_CYC(x)	( x / RND_ROBIN_LOOP_MS )					//!< Conversion of milliseconds to round robin cycles

// Base values
#define BASE_10 (10)

// Log Message dimensions
#define LOG_MESSAGE_LENGTH	(128)	//!< Maximum length of a message in the log
#define LOG_MESSAGE_CNT		(32)	//!< Count of messages in the log

// External data
extern int g_bIsSimulated; //!< Flag set to 1 in main.cpp if a simulation build and 0 otherwise
extern int g_bTestAccess; //!< Flag set to 0 in main.cpp for product builds and 1 if libraries are used for testing

// Inline Functions (follows class definition)

#endif /* UGMCUDEFINITIONS_H_ */

REM @echo OFF
REM Comment out preceding echo OFF to have commands shown
REM
REM This is the main script for the periodic MCU build and test. 
REM
FOR /F "tokens=1-4 delims=/ " %%I IN ('DATE /t') DO SET mydate=%%J%%K%%L
set BuildDir=Build%mydate%
set Log=BuildLog.txt
set Summ=BuildSummary.txt

IF "%1" == "Send" (
echo Sending build results via mail
python pymailer.py "Overnight Build" "%BuildDir%\BuildSummary.txt"
exit /b )

REM Get and build the source code
call McuBuildScript %1
IF ERRORLEVEL 1 (
echo Build failed: %date% %time% >> %Summ%
) ELSE (
echo Build succeeded: %date% %time% >> %Summ%
)


REM Move to the build directory
cd %BuildDir%

REM Make the CVAS simulators and the simulation ELF
make BuildSim >> %Log% 2>&1
IF ERRORLEVEL 1 (
echo Build failed %date% %time% >> %Summ%
) ELSE (
echo Additional build succeeded: %date% %time% >> %Summ%
)


REM Perform Unit Tests
make UnitTest >> %Log% 2>&1
IF ERRORLEVEL 1 (
echo Unit and Integration tests failed: %date% %time% >> %Summ%
) ELSE (
echo Unit and Integration tests passed: %date% %time% >> %Summ%
)

REM Do the DV tests
make DVTest >> %Log% 2>&1
IF ERRORLEVEL 1 (
echo DV tests failed: %date% %time% >> %Summ%
) ELSE (
echo DV tests passed: %date% %time% >> %Summ%
)

REM Generate the SDD using Doxygen
make GenerateDocs >> %Log% 2>&1
IF ERRORLEVEL 1 (
echo Doxygen generation failed: %date% %time% >> %Summ%
) ELSE (
echo Doxygen generation successful: %date% %time% >> %Summ%
)

echo Build and test completed: %date% %time% >> %Summ% 

cd ..

# This script emails the status of the build
# using Gmail's SMTP server and a dedicated
# email address.  

# The script requires at least Python 2.7 and
# the smtplib (usually included in the Python install)

# Last updated 1/23/2013, Chris

# Command line arguments: 
#	SUBJECT = ARG[1]
#   MESSAGE (COPIED FROM A FILE) = ARG[2]
#   SEND TO PHONES FLAG = ARG[2] or ARG[3] (to send text message enter: "phonehome")
#	ADDITIONAL ATTACHMENT = ARG[3] (if no 'phonehome'), or ARG[4] (if 'phonehome')

# example script args:
#
# 1) python pymail.py "Build Success/Fail @ Time" "./BuildSummary.txt" "./Log.txt"
#
#
# 2) python pymail.py "SUCCESS! <or BUILD FAILED> @ TIME" phonehome

import sys
import smtplib  
from email.mime.multipart import MIMEMultipart
from email.mime.text import MIMEText

msgAttach = ""
msgFile = ""

numArgs = len(sys.argv)
if numArgs < 3:
	print "\nI need at least 2 arguments:\n\tThe first is the subject\n\tThe second is the message"
	print "\nThe third argument is optional: \n\ttype \'phonehome\' without quotes to send text messages\n"
	print "The third or fourth argument is an optional plain text email file attachment\n"
	quit()
 
fromaddr = 'MrCronMailer@gmail.com'
toaddrs = ['ckeller@wynedge.com','matt@nxthera.com', 'bbachmeier@nxthera.com', 'kurtb@revis-assoc.com'] 
tophone = ['6122292571@messaging.sprintpcs.com','16128597952@tmomail.net']
# check for the "phonehome" flag
# default to email send
sendto = toaddrs

# if you just want to text, usually just send the subject of the text
if sys.argv[2] == "phonehome":
		sendto = tophone	
else:
	fp2 = open(sys.argv[2])
	msgFile = MIMEText(fp2.read(), _subtype = 'plain')
	fp2.close()

# TODO: This logic needs cleaning up.  Just keep attaching until the end of the arg list 	
if numArgs > 3:
	if sys.argv[3] == "phonehome":
		sendto  = tophone
	else:
		fp = open(sys.argv[numArgs - 1])
		msgAttach = MIMEText(fp.read(), _subtype = 'plain')
		fp.close()

msg = sys.argv[1]  
COMMASPACE = ', '

msg = MIMEMultipart()
msg['Subject'] = sys.argv[1]
msg['From'] = fromaddr
msg['To'] = COMMASPACE.join(sendto)
if len(msgFile) > 0:
	msg.attach(msgFile)
else:
	print "no message attached\n"
# Make an actual attachment if the thid arg isn't 'phonehome' 
if len(msgAttach) > 0:
	msg.attach(msgAttach)

# Credentials   
username = 'MrCronMailer'  
password = 'wynedgecvas'  
  
# The actual mail send  
server = smtplib.SMTP('smtp.gmail.com:587')  
server.starttls()  
server.login(username,password)
server.sendmail(fromaddr, sendto, msg.as_string())  
server.quit()  
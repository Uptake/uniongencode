#!/usr/bin/perl

#edit the Version.h file and update the the MCU_VERSION_ID with
#the proper version.  Version information is obtained by
#evaluating whether the repository location corresponds to
#the trunk or the branch.  The version produced in each case is:
#
#
# The Trunk: Use the current date/time
#
# A Branch: This is a release build.  The version should match
#           the branch folder name.

open(MYINPUTFILE, "<./include/Version.h");
open(MYOUTPUTFILE, ">./include/Version.h.tmp");

$folder = `svn info | grep URL`;
chomp($folder);
@tokens = split(/\//, $folder);
$branchOrTrunk = @tokens[-2];
$endfolder = @tokens[-1];
#print "$folder\n";

while (<MYINPUTFILE>)
{
    my($line) = $_;

    chomp($line);

    if($line =~ /#define MCU_VERSION_ID/)
    {
        # are we on the trunk or on a branch?
        if ($branchOrTrunk =~ /branches/)
        {
            $line = "#define MCU_VERSION_ID \"$endfolder\"";
        }
        else
        {
            $line = "#define MCU_VERSION_ID \"V_\" __DATE__ \"_\" __TIME__";
        }
    }
    print MYOUTPUTFILE "$line\n";
    #print "$line\n";
}

system("cp ./include/Version.h.tmp ./include/Version.h");
system("rm ./include/Version.h.tmp");


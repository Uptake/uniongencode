@echo OFF
REM Comment out preceding echo OFF to have commands shown
REM
REM This is the top-level script for the periodic MCU build. It calls McuBuildScript.bat with
REM the path to the SVN trunk, tag or branch to build. Accordingly, this file is modified for each
REM distinct build that will be made.
REM
McuBuildScript https://apps.wynedge.com/svn/nxthera/CVAS/MCU/trunk/
REM McuBuildAndTest https://apps.wynedge.com/svn/nxthera/CVAS/MCU/trunk/

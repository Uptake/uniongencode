/*
 * Placeholder.h
 *
 *  Created on: Mar 16, 2017
 *      Author: Glen
 */

#ifndef PLACEHOLDER_H_
#define PLACEHOLDER_H_

class Placeholder {
public:
	Placeholder();
	virtual ~Placeholder();
};

#endif /* PLACEHOLDER_H_ */

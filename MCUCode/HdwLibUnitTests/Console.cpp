/**
 * @file       Console.cpp
 * @par        Package: HdwLibUnitTests
 * @par        Project: CVAS Generator
 * @par        Copyright (c) 2016, Uptake Medical, Inc.
 * @author     Chris Mar 21, 2012 Created file
 * @brief      This file contains the console display functions
 */

// Include Files
#include "Console.h"
#include "RS232.h"
#include "CvasMcuError.h"
#include "stdio.h"
#include "stdlib.h"
#include "string.h"
#include "TimerAccess.h"
#include "CvasMcuDefinitions.h"
#include "Temperature.h"
#include "LogMessage.h"
#include "OneWireDriver.h"
#include "SpiDriver.h"

// External Public Data

// File Scope Macros and Constants

// File Scope Type Definitions (Enums, Structs & Classes)
#define ASCII_ENTER 13
#define ASCII_BACKSPACE 8
#define ASCII_ZERO 48
#define ASCII_NINE 57
#define ASCII_SPACE 32
#define MAX_CHAR_CAPTURE 5
#define NO_SELECTION -1
#define TRUE (1)
#define FALSE (0)
#define LINEFEED (1)
#define NO_LINEFEED (0)

#define CLEAR 0
#define SET 1

// File Scope Data
const char * sUnderline = "   ------------------------------";
const char * sUnderlineLeft = "------------------------------";
const char * sReturn = "Return to Main Menu";
const char * sAddressOut = "Address out of range\r\n\r\n";
const char * sTimeout = "Timeout (4uS increments)";

byte outBuffer[ OUT_BUFFER_MAX ];

// Instantiate objects under test
CTemperature 	cTemperature;

//COneWireDriver	cOneWireDriver;
CSpiDriver		cSpiDriver;
CMemMapIntfc	cMemMapIntfc;
CTimeBase		cTimeBase;
CTimerAccess	cTimerAccess;


// File Scope Functions

/**
 * @brief	Class constructor
 */
CConsole::CConsole()
{
	m_eDefaultCom = RS232_SBC;
}


//====================================================================================================
//									CONSOLE MENUS
//====================================================================================================

//==================================  MAIN MENU  ==================================
/**
 * @brief   This function displays the main hardware test menu
 *
 * @return   Status as upCvasErr
 */
upCvasErr CConsole::ConsoleMainMenu()
{
	upCvasErr err = OK;
	bool	inputIsGood = false;
	long userSelection = NO_SELECTION;	// Long to accomodate input numbers > 16-bit

	while ( userSelection == NO_SELECTION )
	{
		//ConsoleHello();
		//err = ConsoleCarriageReturn( 1 );
		err = ConsoleOutput( "\r\nMain Menu", LINEFEED );
		err = ConsoleOutput( sUnderlineLeft, LINEFEED );
		err = ConsoleOutputIndexed( 1, "Go To Temperature Sub-Menu", LINEFEED, false );
		err = ConsoleOutputIndexed( 3, "Go To One Wire Sub-Menu", LINEFEED, false );
		err = ConsoleOutputIndexed( 4, "Go To Mem Map Sub-Menu", LINEFEED, false );

		// SPI seems fully covered by Temperature. Disable for now.
		//err = ConsoleOutputIndexed( 5, "Go To SPI Sub-Menu", LINEFEED, false );

		err = ConsolePrompt( &userSelection, false );
		err = ConsoleCarriageReturn( 1 );
	}

	switch ( userSelection )
	{
		case 1:
			inputIsGood = true;
			err = ConsoleTemperatureSubMenu();
			break;
		case 3:
			inputIsGood = true;
			err = ConsoleOneWireSubMenu();
			break;
		case 4:
			inputIsGood = true;
			err = ConsoleMMapSubMenu();
			break;
		//case 5:
		//	SPI seems fully covered by Temperature. Disable for now.
		//	inputIsGood = true;
		//	err = ConsoleSpiSubMenu();
		//	break;
		default:
			break;
	}

	if( !inputIsGood )
		ConsoleOutput( "\r\nUnrecognized Input", LINEFEED );
	else if(err != OK)
	{
		sprintf( (char*)outBuffer, "\r\nMain Menu Error: %d", err );
		ConsoleOutput( (char*)outBuffer, LINEFEED );
	}

	return err;
}


//==================================  TEMPERATURE SUB-MENU  ==================================

/**
 * @brief	This function displays the console Temperature sub-menu
 *
 * @return   Status as upCvasErr
 */
upCvasErr CConsole::ConsoleTemperatureSubMenu()
{
	upCvasErr err = OK;
	bool inputIsGood = false;
	long userSubSelection = NO_SELECTION;	// Long to accomodate input numbers > 16-bit

	while ( userSubSelection != 0 )
		{
		inputIsGood = false;
		//err = ConsoleCarriageReturn( 2 );
		err = ConsoleOutput( "\r\n   Temperature Sub-Menu", LINEFEED );
		err = ConsoleOutput( sUnderline, LINEFEED );
		err = ConsoleOutputIndexed( 1, "Initialize Hardware", LINEFEED, true );
		err = ConsoleOutputIndexed( 2, "Read From Hardware", LINEFEED, true );
		err = ConsoleOutputIndexed( 3, "Get All Temperatures", LINEFEED, true );
		err = ConsoleOutputIndexed( 4, "Get All Filtered Temperatures", LINEFEED, true );
		err = ConsoleOutputIndexed( 0, sReturn, LINEFEED, true );

		err = ConsolePrompt( &userSubSelection, true );
		err = ConsoleCarriageReturn( 1 );

		switch ( userSubSelection )
			{
			case 0:
				inputIsGood = true;
				break;
			case 1:
				inputIsGood = true;
				err = Temperature1();
				break;
			case 2:
				inputIsGood = true;
				err = Temperature2();
				break;
			case 3:
				inputIsGood = true;
				err = Temperature3();
				break;
			case 4:
				inputIsGood = true;
				err = Temperature4();
				break;
			default:
				break;
			}

		if( !inputIsGood )
			ConsoleOutput( "\r\nUnrecognized Input", LINEFEED );
		else
		{
			if(err != OK)
			{
				sprintf( (char*)outBuffer, "Sub Menu Error: %d", err );
				ConsoleOutput( (char*)outBuffer, LINEFEED );
			}
			else if ( userSubSelection != 0 )	// No OK if returning to Main Menu
				ConsoleOutput( "OK", LINEFEED );
		}

	}

	return err;

}

//==================================  ONE WIRE SUB-MENU  ==================================

/**
 * @brief	This function displays the console One Wire sub-menu
 *
 * @return   Status as upCvasErr
 */
upCvasErr CConsole::ConsoleOneWireSubMenu()
{
	upCvasErr err = OK;
	bool inputIsGood = false;
	long userSubSelection = NO_SELECTION;	// Long to accomodate input numbers > 16-bit
	long userInput1 = NO_SELECTION;	// Long to accomodate input numbers > 16-bit
	long userInput2 = NO_SELECTION;	// Long to accomodate input numbers > 16-bit

	while ( userSubSelection != 0 )
		{
		inputIsGood = false;
		//err = ConsoleCarriageReturn( 2 );
		err = ConsoleOutput( "\r\n   One Wire Sub-Menu", LINEFEED );
		err = ConsoleOutput( sUnderline, LINEFEED );
		err = ConsoleOutputIndexed( 1, "Initialize Hardware", LINEFEED, true );
		err = ConsoleOutputIndexed( 2, "Read From Hardware", LINEFEED, true );
		err = ConsoleOutputIndexed( 3, "Return Hw Health", LINEFEED, true );
		err = ConsoleOutputIndexed( 4, "Get OW Presence", LINEFEED, true );
		err = ConsoleOutputIndexed( 5, "OW Get Rom Info", LINEFEED, true );
		err = ConsoleOutputIndexed( 6, "Ready New One Wire", LINEFEED, true );
		err = ConsoleOutputIndexed( 7, "Read One Page", LINEFEED, true );
		err = ConsoleOutputIndexed( 8, "Read Bytes", LINEFEED, true );
		err = ConsoleOutputIndexed( 91, "Write Bytes - Step 1", LINEFEED, true );
		err = ConsoleOutputIndexed( 92, "Write Bytes - Step 2", LINEFEED, true );
		err = ConsoleOutputIndexed( 93, "Write Bytes - Step 3", LINEFEED, true );
		err = ConsoleOutputIndexed( 94, "Write Bytes - Step 4", LINEFEED, true );
		err = ConsoleOutputIndexed( 0, sReturn, LINEFEED, true );

		err = ConsolePrompt( &userSubSelection, true );
		err = ConsoleCarriageReturn( 1 );

//	upCvasErr OwReadMem( twobytes iStartAddress, byte * pData, byte bNumBytes );
//	upCvasErr OwWriteMem( twobytes iStartAddress, byte * pbData, byte bNumBytes, byte bPass );

		switch ( userSubSelection )
			{
			case 0:
				inputIsGood = true;
				break;
			case 1:
				inputIsGood = true;
				err = OneWire1();
				break;
			case 2:
				inputIsGood = true;
				err = OneWire2();
				break;
			case 3:
				inputIsGood = true;
				err = OneWire3();
				break;
			case 4:
				inputIsGood = true;
				err = OneWire4();
				break;
			case 5:
				inputIsGood = true;
				err = OneWire5();
				break;
			case 6:
				inputIsGood = true;
				err = OneWire6();
				break;
			case 7:
				err = ConsolePrompt2( "\n\r   Page (0 to 79) ", &userInput1, false );
				err = ConsoleCarriageReturn( 1 );

				if( userInput1 >= 0 && userInput1 <= 79 )
				{
					inputIsGood = true;
					err = OneWire7( userInput1 );
				}
				break;
			case 8:
				err = ConsolePrompt2( "\n\r   Address (0 to 2623) ", &userInput1, false );
				err = ConsoleCarriageReturn( 1 );

				err = ConsolePrompt2( "\n\r   Bytes (0 to 100) ", &userInput2, false );
				err = ConsoleCarriageReturn( 1 );

				if( ( userInput1 >= 0 && userInput1 <= 2623 ) &&
					( userInput2 >= 0 && userInput2 <= 100 ) )
				{
					inputIsGood = true;
					err = OneWire8( userInput1, userInput2 );
				}
				break;

			case 91:
				err = ConsolePrompt2( "\n\r   Address (0 to 2623) ", &userInput1, false );
				err = ConsoleCarriageReturn( 1 );

				err = ConsolePrompt2( "\n\r   Bytes (0 to 100) ", &userInput2, false );
				err = ConsoleCarriageReturn( 1 );

				if( ( userInput1 >= 0 && userInput1 <= 2623 ) &&
					( userInput2 >= 0 && userInput2 <= 100 ) )
				{
					inputIsGood = true;
					err = OneWire9( userInput1, userInput2, WRITE_PASS_1 );
				}
				break;

			case 92:
				inputIsGood = true;
				err = OneWire9( userInput1, userInput2, WRITE_PASS_2 );
				break;

			case 93:
				inputIsGood = true;
				err = OneWire9( userInput1, userInput2, WRITE_PASS_3 );
				break;

			case 94:
				inputIsGood = true;
				err = OneWire9( userInput1, userInput2, WRITE_PASS_4 );
				break;

			default:
				break;
			}

		if( !inputIsGood )
			ConsoleOutput( "\r\nUnrecognized Input", LINEFEED );
		else
		{
			if(err != OK)
			{
				sprintf( (char*)outBuffer, "Sub Menu Error: %d", err );
				ConsoleOutput( (char*)outBuffer, LINEFEED );
			}
			else if ( userSubSelection != 0 )	// No OK if returning to Main Menu
				ConsoleOutput( "OK", LINEFEED );
		}

	}

	return err;

}


//==================================  MEM MAP SUB-MENU  ==================================

/**
 * @brief	This function displays the console Mem Map sub-menu
 *
 * @return   Status as upCvasErr
 */
upCvasErr CConsole::ConsoleMMapSubMenu()
{
	upCvasErr err = OK;
	bool inputIsGood = false;
	long userSubSelection = NO_SELECTION;	// Long to accomodate input numbers > 16-bit

	while ( userSubSelection != 0 )
		{
		inputIsGood = false;
		//err = ConsoleCarriageReturn( 2 );
		err = ConsoleOutput( "\r\n   Mem Map Sub-Menu", LINEFEED );
		err = ConsoleOutput( sUnderline, LINEFEED );
		err = ConsoleOutputIndexed( 1, "Mem Map Dump", LINEFEED, true );

		err = ConsoleOutput( "   --- Mem Map Read ---", LINEFEED );
		err = ConsoleOutputIndexed( 21, "Mem Map Read All", LINEFEED, true );
		err = ConsoleOutputIndexed( 22, "Read and Decode Signals", LINEFEED, true );
		
		err = ConsoleOutput( "   --- Watchdog ---", LINEFEED );
		err = ConsoleOutput( "   31/32: WDOG 0x0000/0x1234", LINEFEED );
		err = ConsoleOutput( "   33/34: WDOG Strobe 0x00/0x01", LINEFEED );
		
		err = ConsoleOutput( "   --- Encoder ---", LINEFEED );
		err = ConsoleOutput( "   41/42: Encoder Clear 0x00/0x01", LINEFEED );
		err = ConsoleOutputIndexed( 43, "Encoder Read", LINEFEED, true );

		err = ConsoleOutput( "   --- System Controls ---", LINEFEED );
		err = ConsoleOutput( "   51/52: Unused 1 On/Off", LINEFEED );
		err = ConsoleOutput( "   53/54: RF Enable On/Off", LINEFEED );
		err = ConsoleOutput( "   55/56: RF Power On/Off", LINEFEED );
		err = ConsoleOutput( "   57/58: Unused 1 On/Off", LINEFEED );
		err = ConsoleOutputIndexed( 59, "All Outputs Off", LINEFEED, true );

		err = ConsoleOutput( "   --- System LEDs ---", LINEFEED );
		err = ConsoleOutput( "   61/62: Green On/Off", LINEFEED );
		err = ConsoleOutput( "   63/64: Amber On/Off", LINEFEED );
		err = ConsoleOutput( "   65/66: Blue Left On/Off", LINEFEED );
		err = ConsoleOutput( "   67/68: Blue Right On/Off", LINEFEED );
		err = ConsoleOutputIndexed( 69, "All LEDs Off", LINEFEED, true );

		err = ConsoleOutputIndexed( 0, sReturn, LINEFEED, true );

		err = ConsolePrompt( &userSubSelection, true );
		err = ConsoleCarriageReturn( 1 );

		switch ( userSubSelection )
			{
			case 0:
				inputIsGood = true;
				break;

			// Dump Memory
			case 1:		
				inputIsGood = true;
				err = MMap1();
				break;

			// Read
			case 21:	
				inputIsGood = true;
				err = MMap2( 1 );
				break;

			case 22:	
				inputIsGood = true;
				err = MMap2( 2 );
				break;

			// Watchdog
			case 31:	
				inputIsGood = true;
				err = MMap3( 1 );
				break;
			case 32:
				inputIsGood = true;
				err = MMap3( 2 );
				break;
			case 33:
				inputIsGood = true;
				err = MMap3( 3 );
				break;
			case 34:
				inputIsGood = true;
				err = MMap3( 4 );
				break;

			// Encoder
			case 41:				
				inputIsGood = true;
				err = MMap4( 1 );
				break;
			case 42:
				inputIsGood = true;
				err = MMap4( 2 );
				break;
			case 43:
				inputIsGood = true;
				err = MMap4( 3 );
				break;
			
			// System Controls
			case 51:
				inputIsGood = true;
				err = MMap5( 1 );
				break;
			case 52:
				inputIsGood = true;
				err = MMap5( 2 );
				break;
			case 53:
				inputIsGood = true;
				err = MMap5( 3 );
				break;
			case 54:
				inputIsGood = true;
				err = MMap5( 4 );
				break;
			case 55:
				inputIsGood = true;
				err = MMap5( 5 );
				break;
			case 56:
				inputIsGood = true;
				err = MMap5( 6 );
				break;
			case 57:
				inputIsGood = true;
				err = MMap5( 7 );
				break;
			case 58:
				inputIsGood = true;
				err = MMap5( 8 );
				break;
			case 59:
				inputIsGood = true;
				err = MMap5( 9 );
				break;

			// System LEDs
			case 61:
				inputIsGood = true;
				err = MMap6( 1 );
				break;
			case 62:
				inputIsGood = true;
				err = MMap6( 2 );
				break;
			case 63:
				inputIsGood = true;
				err = MMap6( 3 );
				break;
			case 64:
				inputIsGood = true;
				err = MMap6( 4 );
				break;
			case 65:
				inputIsGood = true;
				err = MMap6( 5 );
				break;
			case 66:
				inputIsGood = true;
				err = MMap6( 6 );
				break;
			case 67:
				inputIsGood = true;
				err = MMap6( 7 );
				break;
			case 68:
				inputIsGood = true;
				err = MMap6( 8 );
				break;
			case 69:
				inputIsGood = true;
				err = MMap6( 9 );
				break;

			default:
				break;
			}

		if( !inputIsGood )
			ConsoleOutput( "\r\nUnrecognized Input", LINEFEED );
		else
		{
			if(err != OK)
			{
				sprintf( (char*)outBuffer, "Sub Menu Error: %d", err );
				ConsoleOutput( (char*)outBuffer, LINEFEED );
			}
			else if ( userSubSelection != 0 )	// No OK if returning to Main Menu
				ConsoleOutput( "OK", LINEFEED );
		}

	}

	return err;

}


//==================================  SPI SUB-MENU  ==================================

/**
 * @brief	This function displays the console SPI sub-menu
 * @brief	SPI seems to be fully covered by Temperature. Disable for now.
 *
 * @return   Status as upCvasErr
 *
upCvasErr CConsole::ConsoleSpiSubMenu()
{
	upCvasErr err = OK;
	bool inputIsGood = false;
	long userSubSelection = NO_SELECTION;	// Long to accomodate input numbers > 16-bit

	while ( userSubSelection != 0 )
		{
		inputIsGood = false;
		//err = ConsoleCarriageReturn( 2 );
		err = ConsoleOutput( "\r\n   SPI Sub-Menu", LINEFEED );
		err = ConsoleOutput( sUnderline, LINEFEED );
		err = ConsoleOutputIndexed( 1, "Initialize SPI", LINEFEED, true );
		err = ConsoleOutputIndexed( 2, "Read SPI", LINEFEED, true );
		err = ConsoleOutputIndexed( 3, "TBD", LINEFEED, true );
		err = ConsoleOutputIndexed( 0, sReturn, LINEFEED, true );

		err = ConsolePrompt( &userSubSelection, true );
		err = ConsoleCarriageReturn( 1 );

		switch ( userSubSelection )
			{
			case 0:
				inputIsGood = true;
				break;
			case 1:
				inputIsGood = true;
				err = Spi1();
				break;
			case 2:
				inputIsGood = true;
				err = Spi2();
				break;
			case 3:
				inputIsGood = true;
				err = Spi3();
				break;
			default:
				break;
			}

		if( !inputIsGood )
			ConsoleOutput( "\r\nUnrecognized Input", LINEFEED );
		else
		{
			if(err != OK)
			{
				sprintf( (char*)outBuffer, "Sub Menu Error: %d", err );
				ConsoleOutput( (char*)outBuffer, LINEFEED );
			}
			else if ( userSubSelection != 0 )	// No OK if returning to Main Menu
				ConsoleOutput( "OK", LINEFEED );
		}

	}

	return err;
*/


//====================================================================================================
//									TEMPERATURE TEST METHODS
//====================================================================================================
//public:
//	CTemperature();
//	upCvasErr InitializeHardware();
//	upCvasErr ReadFromHardware();
//	upCvasErr GetThermoTemp( upTempMeas eTemp, int * pfTemp );
//	upCvasErr GetFilteredThermoTemp( upTempMeas eTemp, int * pfTemp );

/**
 * @brief	This function responds to a Temperature sub-menu selection of '1'.
 *
 * @return   Status as upCvasErr
 */
upCvasErr CConsole::Temperature1()
{
	upCvasErr err = OK;

	//err = ConsoleCarriageReturn( 1 );
	err = ConsoleOutput( "\r\nInitializeHardware()", LINEFEED );

	err = cTemperature.InitializeHardware();

	return err;
}


/**
 * @brief	This function responds to a Temperature sub-menu selection of '2'.
 *
 * @return   Status as upCvasErr
 */
upCvasErr CConsole::Temperature2()
{
	upCvasErr err = OK;

	err = ConsoleOutput( "\r\nReadFromHardware()", LINEFEED );
	err = cTemperature.ReadFromHardware();

	return err;
}


/**
 * @brief	This function responds to a Temperature sub-menu selection of '3'.
 *
 * @return   Status as upCvasErr
 */
upCvasErr CConsole::Temperature3()
{
	upCvasErr err = OK;
	int iTemp[THERMO_CNT];
	int iErr[4];
	int i=0;

	err = ConsoleOutput( "\r\nGetThermoTemp(ALL)", LINEFEED );

	for (i=0; i<4; i++)
	{
		iErr[i] = cTemperature.GetThermoTemp( (upTempMeas)i, &iTemp[i] );
	}

	sprintf( (char*)outBuffer, "   Channel: OUTLET  COIL INTER SPARE");
	err = ConsoleOutput( (char*)outBuffer, LINEFEED );
	sprintf( (char*)outBuffer, "   Temp:     %5d %5d %5d %5d", iTemp[0], iTemp[1], iTemp[2], iTemp[3]);
	err = ConsoleOutput( (char*)outBuffer, LINEFEED );
	sprintf( (char*)outBuffer, "   Error:    %5d %5d %5d %5d", iErr[0], iErr[1], iErr[2], iErr[3]);
	err = ConsoleOutput( (char*)outBuffer, LINEFEED );

	return err;
}


/**
 * @brief	This function responds to a Temperature sub-menu selection of '4'.
 *
 * @return   Status as upCvasErr
 */
upCvasErr CConsole::Temperature4()
{
	upCvasErr err = OK;
	int iTemp[THERMO_CNT];
	int iErr[4];;
	int i=0;

	err = ConsoleOutput( "\r\nGetFilteredThermoTemp(ALL)", LINEFEED );

	for (i=0; i<4; i++)
	{
		iErr[i] = cTemperature.GetFilteredThermoTemp( (upTempMeas)i, &iTemp[i] );
	}

	sprintf( (char*)outBuffer, "   Channel: OUTLET  COIL INTER SPARE");
	err = ConsoleOutput( (char*)outBuffer, LINEFEED );
	sprintf( (char*)outBuffer, "   Temp:     %5d %5d %5d %5d", iTemp[0], iTemp[1], iTemp[2], iTemp[3]);
	err = ConsoleOutput( (char*)outBuffer, LINEFEED );
	sprintf( (char*)outBuffer, "   Error:    %5d %5d %5d %5d", iErr[0], iErr[1], iErr[2], iErr[3]);
	err = ConsoleOutput( (char*)outBuffer, LINEFEED );

	return err;
}



//====================================================================================================
//									ONE WIRE TEST METHODS
//====================================================================================================
//public:
//	COneWireDriver();
//	upCvasErr ReadFromHardware();
//	upCvasErr InitializeHardware();
//protected:
//	upCvasErr GetOwPresence( bool_c * bOwPresence );
//	upCvasErr OwReadMem( twobytes iStartAddress, byte * pData, byte bNumBytes );
//	upCvasErr OwWriteMem( twobytes iStartAddress, byte * pbData, byte bNumBytes, byte bPass );
//	upCvasErr OwGetRomInfo( byte * pbRomInfo );
//	upCvasErr ReadyNewOneWire();
//	bool_c ReturnHwHealth();

/**
 * @brief	This function responds to a One Wire sub-menu selection of '1'.
 * @brief	It initializes TimeBase and TimerAccess, and then OneWireDriver.
 *
 * @return   Status as upCvasErr
 */
upCvasErr CConsole::OneWire1()
{
	upCvasErr err = OK;
	upCvasErr iErr = OK;

	err = ConsoleOutput( "\r\n   InitializeHardware() Wait 2 seconds...", LINEFEED );

	cTimeBase.InitializeHardware();
	cTimerAccess.MsDelay(2000);
	iErr = COneWireDriver::InitializeHardware();

	sprintf( (char*)outBuffer, "   Error: %d", iErr);
	err = ConsoleOutput( (char*)outBuffer, LINEFEED );

	return err;
}


/**
 * @brief	This function responds to a One Wire sub-menu selection of '2'.
 * @brief	It reads from the OW hardware.
 *
 * @return   Status as upCvasErr
 */
upCvasErr CConsole::OneWire2()
{
	upCvasErr err = OK;
	upCvasErr iErr = OK;

//	err = ConsoleOutput( "\r\nReadFromHardware()", LINEFEED );
	iErr = COneWireDriver::ReadFromHardware();

	sprintf( (char*)outBuffer, "   Read HW error: %d", iErr);
	err = ConsoleOutput( (char*)outBuffer, LINEFEED );

	return err;
}


/**
 * @brief	This function responds to a One Wire sub-menu selection of '3'.
 * @brief	It checks the OW hardware health.
 *
 * @return   Status as upCvasErr
 */
upCvasErr CConsole::OneWire3()
{
	upCvasErr err = OK;
	bool_c	bHwIsHealthy;

//	err = ConsoleOutput( "\r\nReturnHwHealth()", LINEFEED );
	bHwIsHealthy = ReturnHwHealth();

	sprintf( (char*)outBuffer, "   IsHealthy: %s", bHwIsHealthy?"Yes":"No" );
	err = ConsoleOutput( (char*)outBuffer, LINEFEED );

	return err;
}


/**
 * @brief	This function responds to a One Wire sub-menu selection of '4'.
 * @brief	It checks for OW presence.
 *
 * @return   Status as upCvasErr
 */
upCvasErr CConsole::OneWire4()
{
	upCvasErr err = OK;
	upCvasErr iErr = OK;
	bool_c	bOwIsPresent;

//	err = ConsoleOutput( "\r\nGetOwPresence()", LINEFEED );
	iErr = GetOwPresence( &bOwIsPresent );

	sprintf( (char*)outBuffer, "   IsPresent: %s   Error: %d", bOwIsPresent?"Yes":"No", iErr);
	err = ConsoleOutput( (char*)outBuffer, LINEFEED );

	return err;
}


/**
 * @brief	This function responds to a One Wire sub-menu selection of '5'.
 * @brief	It retrieves the OW rom info.
 *
 * @return   Status as upCvasErr
 */
upCvasErr CConsole::OneWire5()
{
	upCvasErr err = OK;
	upCvasErr iErr = OK;
	byte bRomInfo[OW_ROM_SIZE];
	byte i;

	for( i=0; i < OW_ROM_SIZE; i++ )
	{
	   	bRomInfo[i] = i+1;
	}

//	err = ConsoleOutput( "\r\nOwGetRomInfo()", LINEFEED );
	iErr = OwGetRomInfo( bRomInfo );

	for( i=0; i < OW_ROM_SIZE; i++ )
	{
	   	sprintf( (char*)outBuffer, "   bRomInfo[%d] = %02x", i, bRomInfo[i]);
		err = ConsoleOutput( (char*)outBuffer, LINEFEED );
	}
	sprintf( (char*)outBuffer, "   Error: %d", iErr);
	err = ConsoleOutput( (char*)outBuffer, LINEFEED );

	return err;
}


/**
 * @brief	This function responds to a One Wire sub-menu selection of '6'.
 * @brief	It readies a new OW.
 *
 * @return   Status as upCvasErr
 */
upCvasErr CConsole::OneWire6()
{
	upCvasErr err = OK;
	upCvasErr iErr = OK;


//	err = ConsoleOutput( "\r\nReadyNewOneWire()", LINEFEED );
	iErr = ReadyNewOneWire();

	sprintf( (char*)outBuffer, "   Ready new OW error: %d", iErr);
	err = ConsoleOutput( (char*)outBuffer, LINEFEED );

	return err;
}


/**
 * @brief	This function responds to a One Wire sub-menu selection of '7'.
 * @brief	It reads a page of OW memory.
 *
 * @return   Status as upCvasErr
 */
upCvasErr CConsole::OneWire7( int action )
{
	upCvasErr err = OK;
	upCvasErr iErr = OK;
	twobytes iStartAddress;
	byte bData[32];
	byte bNumBytes;
	byte i;

	for( i=0; i<32; i++ )
		bData[i] = 0xFF;

	iStartAddress = action * 32;
	bNumBytes = 32;
	iErr = OwReadMem( iStartAddress, bData, bNumBytes );

	for( i=0; i<32; i++ )
	{
		sprintf( (char*)outBuffer, "   Address %4x: %02x", iStartAddress+i, bData[i] );
		err = ConsoleOutput( (char*)outBuffer, LINEFEED );
	}
	sprintf( (char*)outBuffer, "   Read OW error: %d", iErr);
	err = ConsoleOutput( (char*)outBuffer, LINEFEED );

	return err;
}


/**
 * @brief	This function responds to a One Wire sub-menu selection of '8'.
 * @brief	It reads an area of OW memory.
 *
 * @return   Status as upCvasErr
 */
upCvasErr CConsole::OneWire8( int input1, int input2 )
{
	upCvasErr err = OK;
	upCvasErr iErr = OK;
	twobytes iStartAddress;
	byte bData[32];
	byte bNumBytes;
	byte i;

	for( i=0; i<32; i++ )
		bData[i] = 0xFF;

	iStartAddress = input1;
	bNumBytes = input2;
	iErr = OwReadMem( iStartAddress, bData, bNumBytes );

	if( 0 == iErr )
	{
		for( i = 0; i < bNumBytes; i++ )
		{
			sprintf( (char*)outBuffer, "   Address %4x: %02x", iStartAddress+i, bData[i] );
			err = ConsoleOutput( (char*)outBuffer, LINEFEED );
		}
	}
	sprintf( (char*)outBuffer, "   Read OW error: %d", iErr);
	err = ConsoleOutput( (char*)outBuffer, LINEFEED );

	return err;
}


/**
 * @brief	This function responds to a One Wire sub-menu selection of '9x'.
 * @brief	It executes one OW state machine step to write an area of OW memory.
 *
 * @return   Status as upCvasErr
 */
upCvasErr CConsole::OneWire9( int input1, int input2, byte bStep )
{
//	upCvasErr OwWriteMem( twobytes iStartAddress, byte * pbData, byte bNumBytes, byte bPass );
	upCvasErr err = OK;
	upCvasErr iErr = OK;
	twobytes iStartAddress;
	byte bData[32];
	byte bNumBytes;
	byte i;

	iStartAddress = input1;
	bNumBytes = input2;

	for( i=0; i<32; i++ )
		bData[i] = i + 1;

	sprintf( (char*)outBuffer, "   Address: %4x, NumBytes: %d, Step: %d", iStartAddress, bNumBytes, bStep );
	err = ConsoleOutput( (char*)outBuffer, LINEFEED );

	iErr = OwWriteMem( iStartAddress, bData, bNumBytes, bStep );

	sprintf( (char*)outBuffer, "   Write OW error: %d", iErr);
	err = ConsoleOutput( (char*)outBuffer, LINEFEED );

	return err;
}


//====================================================================================================
//									MEM MAP TEST METHODS
//====================================================================================================
//public:
//	CMemMapIntfc();
//	upCvasErr MMapRead( upMMapFunction eFunction, byte * pbBytes, byte bRdSize );
//	upCvasErr MMapWrite( upMMapFunction eFunction, byte * pbBytes, byte bWrSize );
//	upCvasErr MMapWriteByte( upMMapFunction eFunction, byte bByte );
//	MMAP_FUNC_WDOG = 0,			2 bytes
//	MMAP_FUNC_WDOG_STROBE,		1 byte
//	MMAP_FUNC_ENCODER_CLEAR,	1 byte
//	MMAP_FUNC_ENCODER,			3 bytes
//	MMAP_FUNC_HTEMP,			2 bytes
//	MMAP_FUNC_LTEMP,			2 bytes
//	MMAP_FUNC_SYSTEM_SIGNALS,	1 byte
//	MMAP_FUNC_SYSTEM_LED,		1 byte
//	MMAP_FUNC_SYSTEM_CONTROLS,	1 byte
//	MMAP_FUNC_CPLD_VERSION,		4 bytes
//	MMAP_FUNC_COUNT


/**
 * @brief	This function responds to a Mem Map sub-menu selection of '1'.
 * @brief	It dumps the I/O memory by direct reads of Extended SRAM.
 *
 * @return   Status as upCvasErr
 */
upCvasErr CConsole::MMap1()
{
	upCvasErr err = OK;
	byte bScratch1 = 0;
	int i = 0;

	err = ConsoleOutput( "\r\nMemory Map Dump", LINEFEED );
	for (i = MMAP_LOW_ADDRESS; i <= MMAP_HIGH_ADDRESS; i++)
	{
		bScratch1 = *(char*)i;
		sprintf( (char*)outBuffer, "   Address %4x: %02x", i, bScratch1 );
		err = ConsoleOutput( (char*)outBuffer, LINEFEED );
	}
	
	return err;
}


/**
 * @brief	This function responds to a Mem Map sub-menu selection of '2x'.
 * @brief	It uses MMapRead to read each I/0 function and checks the error code.
 * @brief	It also uses MMapRead to read and decode system signals.
 *
 * @return   Status as upCvasErr
 */
upCvasErr CConsole::MMap2( int action )
{
	upCvasErr err = OK;
	byte bValue = 0;
	int iValue = 0;
	long lValue = 0;

	if ( 1 == action )	// Read all values
	{
		err = ConsoleOutput( "\r\nMMapRead(ALL) - Values in Hex", LINEFEED );

		iValue = 0;
		err = cMemMapIntfc.MMapRead( MMAP_FUNC_WDOG, (byte *) &iValue, (byte)2 );
		sprintf( (char*)outBuffer, "   Wdog:     %04x     Error: %4d", iValue, err );
		err = ConsoleOutput( (char*)outBuffer, LINEFEED );

		bValue = 0;
		err = cMemMapIntfc.MMapRead( MMAP_FUNC_WDOG_STROBE, (byte *) &bValue, (byte)1 );
		sprintf( (char*)outBuffer, "   Wdog Stb: %02x       Error: %4d", bValue, err );
		err = ConsoleOutput( (char*)outBuffer, LINEFEED );

		bValue = 0;
		err = cMemMapIntfc.MMapRead( MMAP_FUNC_ENCODER_CLEAR, (byte *) &bValue, (byte)1 );
		sprintf( (char*)outBuffer, "   Enc Clr:  %02x       Error: %4d", bValue, err );
		err = ConsoleOutput( (char*)outBuffer, LINEFEED );

		lValue = 0;
		err = cMemMapIntfc.MMapRead( MMAP_FUNC_ENCODER, (byte *) &lValue, (byte)3 );
		sprintf( (char*)outBuffer, "   Encoder:  %06lx   Error: %4d", lValue, err );
		err = ConsoleOutput( (char*)outBuffer, LINEFEED );

		bValue = 0;
		err = cMemMapIntfc.MMapRead( MMAP_FUNC_SYSTEM_SIGNALS, (byte *) &bValue, (byte)1 );
		sprintf( (char*)outBuffer, "   Signals:  %02x       Error: %4d", bValue, err );
		err = ConsoleOutput( (char*)outBuffer, LINEFEED );

		bValue = 0;
		err = cMemMapIntfc.MMapRead( MMAP_FUNC_SYSTEM_LED, (byte *) &bValue, (byte)1 );
		sprintf( (char*)outBuffer, "   LEDs:     %02x       Error: %4d", bValue, err );
		err = ConsoleOutput( (char*)outBuffer, LINEFEED );

		bValue = 0;
		err = cMemMapIntfc.MMapRead( MMAP_FUNC_SYSTEM_CONTROLS, (byte *) &bValue, (byte)1 );
		sprintf( (char*)outBuffer, "   Controls: %02x       Error: %4d", bValue, err );
		err = ConsoleOutput( (char*)outBuffer, LINEFEED );

		lValue = 0;
		err = cMemMapIntfc.MMapRead( MMAP_FUNC_CPLD_VERSION, (byte *) &lValue, (byte)4 );
		sprintf( (char*)outBuffer, "   CPLD Ver: %08lx Error: %4d", lValue, err );
		err = ConsoleOutput( (char*)outBuffer, LINEFEED );

		lValue = 0;
		err = cMemMapIntfc.MMapRead( (upMMapFunction)(MMAP_FUNC_CPLD_VERSION+1), (byte *) &lValue, (byte)4 );
		sprintf( (char*)outBuffer, "   CPLD+1:   %08lx Error: %4d", lValue, err );
		err = ConsoleOutput( (char*)outBuffer, LINEFEED );
	}
	else	// Read and decode system signals
	{
		bValue = 0;
		err = cMemMapIntfc.MMapRead( MMAP_FUNC_SYSTEM_SIGNALS, (byte *) &bValue, (byte)1 );
		sprintf( (char*)outBuffer, "   Signals:  %02x Error: %4d", bValue, err );
		err = ConsoleOutput( (char*)outBuffer, LINEFEED );

		sprintf( (char*)outBuffer, "      Bit 7: %s", ( bValue & 0b10000000 ) ? "ON" : "OFF" );
		err = ConsoleOutput( (char*)outBuffer, LINEFEED );
		sprintf( (char*)outBuffer, "      Trig: %s", ( bValue & 0b01000000 ) ? "ON" : "OFF" );
		err = ConsoleOutput( (char*)outBuffer, LINEFEED );
		sprintf( (char*)outBuffer, "      Bit 5: %s", ( bValue & 0b00100000 ) ? "ON" : "OFF" );
		err = ConsoleOutput( (char*)outBuffer, LINEFEED );
		sprintf( (char*)outBuffer, "      Ext: %s", ( ~bValue & 0b00010000 ) ? "ON" : "OFF" );
		err = ConsoleOutput( (char*)outBuffer, LINEFEED );
		sprintf( (char*)outBuffer, "      Retr: %s", ( ~bValue & 0b00001000 ) ? "ON" : "OFF" );
		err = ConsoleOutput( (char*)outBuffer, LINEFEED );
		sprintf( (char*)outBuffer, "      Syr: %s", ( ~bValue & 0b00000100 ) ? "ON" : "OFF" );
		err = ConsoleOutput( (char*)outBuffer, LINEFEED );
		sprintf( (char*)outBuffer, "      Bit 1: %s", ( bValue & 0b00000010 ) ? "ON" : "OFF" );
		err = ConsoleOutput( (char*)outBuffer, LINEFEED );
		sprintf( (char*)outBuffer, "      H2O: %s", ( ~bValue & 0b00000001 ) ? "ON" : "OFF" );
		err = ConsoleOutput( (char*)outBuffer, LINEFEED );
	}

	return err;
}


/**
 * @brief	This function responds to a Mem Map sub-menu selection of '3x'.
 * @brief	It exercises the watchdog functions.
 *
 * @return   Status as upCvasErr
 */
upCvasErr CConsole::MMap3( int action )
{
	upCvasErr err = OK;
	int iVal = 0x0000;
	byte bVal = 0x00;

	switch( action )
	{
		case 1:
			iVal = 0x0000;
			err = cMemMapIntfc.MMapWrite(MMAP_FUNC_WDOG, (byte *) &iVal, (byte)2 );
			sprintf( (char*)outBuffer, "   WDOG:     %4x     Error: %5x", iVal, err );
			break;
		case 2:
			iVal = 0x1234;
			err = cMemMapIntfc.MMapWrite(MMAP_FUNC_WDOG, (byte *) &iVal, (byte)2 );
			sprintf( (char*)outBuffer, "   WDOG:     %4x     Error: %5x", iVal, err );
			break;
		case 3:
			bVal = 0x00;
			err = cMemMapIntfc.MMapWriteByte( MMAP_FUNC_WDOG_STROBE, bVal );
			sprintf( (char*)outBuffer, "   WDOG Strb:     %2x     Error: %5x", bVal, err );
			break;
		case 4:
			iVal = 0x01;
			err = cMemMapIntfc.MMapWriteByte( MMAP_FUNC_WDOG_STROBE, bVal );
			sprintf( (char*)outBuffer, "   WDOG Strb:     %2x     Error: %5x", bVal, err );
			break;
		default:
			break;
	}

	err = ConsoleOutput( (char*)outBuffer, LINEFEED );

	return err;
}


/**
 * @brief	This function responds to a Mem Map sub-menu selection of '4x'.
 * @brief	It exercises the encoder functions.
 *
 * @return   Status as upCvasErr
 */
upCvasErr CConsole::MMap4( int action )
{
	upCvasErr err = OK;
	byte bVal = 0x00;
	long lValue = 0;
	
	switch( action )
	{
		case 2:
			bVal = 0x01;
			// Fall through
		case 1:
			err = cMemMapIntfc.MMapWriteByte( MMAP_FUNC_ENCODER_CLEAR, bVal );
			sprintf( (char*)outBuffer, "   Encoder Clear:     %2x     Error: %5x", bVal, err );
			break;
		case 3:
			err = cMemMapIntfc.MMapRead( MMAP_FUNC_ENCODER, (byte *) &lValue, (byte)3 );
			sprintf( (char*)outBuffer, "   Encoder:  %06lx   Error: %4d", lValue, err );
			break;
		default:
			break;
	}

	err = ConsoleOutput( (char*)outBuffer, LINEFEED );

	return err;
}


/**
 * @brief	This function responds to a Mem Map sub-menu selection of '5x'.
 * @brief	It exercises the System Controls functions.
 *
 * @return   Status as upCvasErr
 */
upCvasErr CConsole::MMap5( int action )
{
	upCvasErr err = OK;
	upCvasErr wErr = OK;
	upCvasErr rErr = OK;
	byte bVal = 0x00;

	rErr = cMemMapIntfc.MMapRead( MMAP_FUNC_SYSTEM_CONTROLS, (byte *) &bVal, (byte)1 );
	sprintf( (char*)outBuffer, "   Controls: %02x       Error: %4d", bVal, err );
	err = ConsoleOutput( (char*)outBuffer, LINEFEED );

	switch( action )
	{
		case 1:
			bVal |= 0b00000001;
			wErr = cMemMapIntfc.MMapWriteByte( MMAP_FUNC_SYSTEM_CONTROLS, bVal );
//			sprintf( (char*)outBuffer, "Unused 1 On,  Error: %5x", wErr );
			break;
		case 2:
			bVal &= 0b11111110;
			wErr = cMemMapIntfc.MMapWriteByte( MMAP_FUNC_SYSTEM_CONTROLS, bVal );
//			sprintf( (char*)outBuffer, "Unused 1 Pump Off,  Error: %5x", wErr );
			break;
		case 3:
			bVal |= 0b00000010;
			wErr = cMemMapIntfc.MMapWriteByte( MMAP_FUNC_SYSTEM_CONTROLS, bVal );
//			sprintf( (char*)outBuffer, "RF Enable On,  Error: %5x", wErr );
			break;
		case 4:
			bVal &= 0b11111101;
			wErr = cMemMapIntfc.MMapWriteByte( MMAP_FUNC_SYSTEM_CONTROLS, bVal );
//			sprintf( (char*)outBuffer, "RF Enable Off,  Error: %5x", wErr );
			break;
		case 5:
			bVal |= 0b00000100;
			wErr = cMemMapIntfc.MMapWriteByte( MMAP_FUNC_SYSTEM_CONTROLS, bVal );
//			sprintf( (char*)outBuffer, "RF AC Power On,  Error: %5x", wErr );
			break;
		case 6:
			bVal &= 0b11111011;
			wErr = cMemMapIntfc.MMapWriteByte( MMAP_FUNC_SYSTEM_CONTROLS, bVal );
//			sprintf( (char*)outBuffer, "RF AC Power Off,  Error: %5x", wErr );
			break;
		case 7:
			bVal |= 0b00010000;
			wErr = cMemMapIntfc.MMapWriteByte( MMAP_FUNC_SYSTEM_CONTROLS, bVal );
//			sprintf( (char*)outBuffer, "Unused 1 On,  Error: %5x", wErr );
			break;
		case 8:
			bVal &= 0b11101111;
			wErr = cMemMapIntfc.MMapWriteByte( MMAP_FUNC_SYSTEM_CONTROLS, bVal );
//			sprintf( (char*)outBuffer, "Unused 1 Off,  Error: %5x", wErr );
			break;
		case 9:
			bVal &= 0b11100000;
			wErr = cMemMapIntfc.MMapWriteByte( MMAP_FUNC_SYSTEM_CONTROLS, bVal );
//			sprintf( (char*)outBuffer, "All Outputs Off,  Error: %5x", wErr );
			break;
		default:
			break;
	}

//	err = ConsoleOutput( (char*)outBuffer, LINEFEED );

	rErr = cMemMapIntfc.MMapRead( MMAP_FUNC_SYSTEM_CONTROLS, (byte *) &bVal, (byte)1 );
	sprintf( (char*)outBuffer, "   Controls: %02x, wErr: %d, rErr: %d", bVal, wErr, rErr );
	err = ConsoleOutput( (char*)outBuffer, LINEFEED );

	return err;
}


/**
 * @brief	This function responds to a Mem Map sub-menu selection of '6x'.
 * @brief	It exercises the System LEDs functions.
 *
 * @return   Status as upCvasErr
 */
upCvasErr CConsole::MMap6( int action )
{
	upCvasErr err = OK;
	upCvasErr wErr = OK;
	upCvasErr rErr = OK;
	byte bVal = 0x00;

	rErr = cMemMapIntfc.MMapRead( MMAP_FUNC_SYSTEM_LED, (byte *) &bVal, (byte)1 );
//	sprintf( (char*)outBuffer, "   LEDs: %02x       Error: %4d", bVal, err );
//	err = ConsoleOutput( (char*)outBuffer, LINEFEED );

	switch( action )
	{
		case 1:
			bVal |= 0b00000011;
			wErr = cMemMapIntfc.MMapWriteByte( MMAP_FUNC_SYSTEM_LED, bVal );
//			sprintf( (char*)outBuffer, "Green LEDs On,  Error: %5x", err );
			break;
		case 2:
			bVal &= 0b11111100;
			wErr = cMemMapIntfc.MMapWriteByte( MMAP_FUNC_SYSTEM_LED, bVal );
//			sprintf( (char*)outBuffer, "Green LEDs Off,  Error: %5x", err );
			break;
		case 3:
			bVal |= 0b00001100;
			wErr = cMemMapIntfc.MMapWriteByte( MMAP_FUNC_SYSTEM_LED, bVal );
//			sprintf( (char*)outBuffer, "Amber LEDs On,  Error: %5x", err );
			break;
		case 4:
			bVal &= 0b11110011;
			wErr = cMemMapIntfc.MMapWriteByte( MMAP_FUNC_SYSTEM_LED, bVal );
//			sprintf( (char*)outBuffer, "Amber LEDs Off,  Error: %5x", err );
			break;
		case 5:
			bVal |= 0b01010000;
			wErr = cMemMapIntfc.MMapWriteByte( MMAP_FUNC_SYSTEM_LED, bVal );
//			sprintf( (char*)outBuffer, "Blue left LEDs On,  Error: %5x", err );
			break;
		case 6:
			bVal &= 0b10101111;
			wErr = cMemMapIntfc.MMapWriteByte( MMAP_FUNC_SYSTEM_LED, bVal );
//			sprintf( (char*)outBuffer, "Blue left LEDs Off,  Error: %5x", err );
			break;
		case 7:
			bVal |= 0b10100000;
			wErr = cMemMapIntfc.MMapWriteByte( MMAP_FUNC_SYSTEM_LED, bVal );
//			sprintf( (char*)outBuffer, "Blue right LEDs On,  Error: %5x", err );
			break;
		case 8:
			bVal &= 0b01011111;
			wErr = cMemMapIntfc.MMapWriteByte( MMAP_FUNC_SYSTEM_LED, bVal );
//			sprintf( (char*)outBuffer, "Blue right LEDs Off,  Error: %5x", err );
			break;
		case 9:
			bVal &= 0b00000000;
			wErr = cMemMapIntfc.MMapWriteByte( MMAP_FUNC_SYSTEM_LED, bVal );
//			sprintf( (char*)outBuffer, "All LEDs Off,  Error: %5x", err );
			break;
		default:
			break;
	}

	rErr = cMemMapIntfc.MMapRead( MMAP_FUNC_SYSTEM_LED, (byte *) &bVal, (byte)1 );
	sprintf( (char*)outBuffer, "   LEDs: %02x, WErr: %d, rErr: %d", bVal, wErr, rErr );
	err = ConsoleOutput( (char*)outBuffer, LINEFEED );

	return err;
}


//====================================================================================================
//									SPI TEST METHODS
//	SPI seems fully covered by Temperature. Disable for now.
//====================================================================================================
//public:
//	CSpiDriver();
//protected:
//	upCvasErr InitializeSpi();
//	upCvasErr ReadSpi( upSpiChips eChip, int iBytes, byte * byteBuf );

/**
 * @brief	This function responds to a SPI sub-menu selection of '1'.
 *
 * @return   Status as upCvasErr
 *
upCvasErr CConsole::Spi1()
{
	upCvasErr err = OK;

	err = ConsoleOutput( "\r\nInitializeSpi(Protected)", LINEFEED );

//	err = cSpiDriver.InitializeSpi();

	return err;
}
*/

/**
 * @brief	This function responds to a SPI sub-menu selection of '2'.
 *
 * @return   Status as upCvasErr
 *
upCvasErr CConsole::Spi2()
{
	upCvasErr err = OK;

	err = ConsoleOutput( "\r\nReadSpi(Protected)", LINEFEED );
	//err = cSpiDriver.ReadSpi();

	return err;
}
*/


/**
 * @brief	This function responds to a SPI sub-menu selection of '3'.
 *
 * @return   Status as upCvasErr
 *
upCvasErr CConsole::Spi3()
{
	upCvasErr err = OK;

	err = ConsoleOutput( "\r\nSpiTBD()", LINEFEED );
	
	return err;
}
*/


//====================================================================================================
//									CONSOLE SUPPORT METHODS
//====================================================================================================

/**
 * @brief	This function outputs a string to the console using ConsoleOutput. Prior
 * 			to that call an index is prepended to the line.
 *
 * @param	bIndex	The index number to add to the head of the line
 * @param   outLine A string to output to the console
 * @param   lineFlag A flag to indicate the addition of a linefeed
 * @param   indent A flag that adds line indent if true
 *
 * @return   Status as upCvasErr
 */
upCvasErr CConsole::ConsoleOutputIndexed( byte bIndex, const char * outLine, byte lineFlag, bool indent )
{
	char charBuffer[ OUT_BUFFER_MAX ];

	if( indent )
		sprintf( charBuffer, "   %u. ", bIndex );
	else
		sprintf( charBuffer, "%u. ", bIndex );

	strcat( charBuffer, outLine );
	return ConsoleOutput( charBuffer, lineFlag );
}

/**
 * @brief	This function outputs a string to the console with \r\n appended
 * 			via RS-232 through the specified UARTn port.
 * @param   outLine A string to output to the console
 * @param   lineFlag A flag to indicate the addition of a linefeed
 * @param   ePort The UARTn port used for output
 * @param
 * @return   Status as upCvasErr
 */
upCvasErr CConsole::ConsoleOutput( const char * outLine, byte lineFlag )
{
	upCvasErr err = OK;
	int bufferSize;
	byte outBuffer[ OUT_BUFFER_MAX ];

	//	err = ConsoleCarriageReturn( newLinesPre, ePort );
	bufferSize = strlen( outLine );
	memcpy( (void*) outBuffer, (void*) outLine, bufferSize );
	if ( lineFlag )
		{
		memcpy( (void*) ( outBuffer + bufferSize ), "\r\n", 3 );
		bufferSize += 2;
		}


	while( ERR_NOT_READY == ( err = WritePort( m_eDefaultCom, bufferSize, outBuffer ) ) );
		//	WaitForClearBuffer( ePort );
	//	err = ConsoleCarriageReturn( newLinesPost, ePort );
	return err;
}

upCvasErr CConsole::InitializeHardware()
{
	upCvasErr eErr = OK;

	eErr = InitializePort( RS232_SBC, RS232_BAUD_115200, RS232_PARITY_NONE, 8, 2 );
	eErr = InitializePort( RS232_RF_GEN, RS232_BAUD_115200, RS232_PARITY_NONE, 8, 2 );

	return eErr;
}


/**
 * @brief	This function displays a welcome screen sent via RS-232
 *          through the specified UARTn port.
 *
 * @return   Status as upCvasErr
 */
upCvasErr CConsole::ConsoleHello()
{
	upCvasErr err;

	err = ConsoleCarriageReturn( 2 );
	err = ConsoleOutput( "**************************************", LINEFEED );
	err = ConsoleOutput( "*    Uptake FBG Unit Test Console    *", LINEFEED );
	err = ConsoleOutput( "**************************************", LINEFEED );

	return err;
}

/**
 * @brief	This function displays a prompt and captures user input
 *			up to a maximum amount.  Only numerical values are
 *			stored.
 *
 * @param	ePort The UARTn port used for output
 * @param   userSelection The user selection captured through UARTn
 *
 * @return   The selection from the user
 */
upCvasErr CConsole::ConsolePrompt( long * userSelection, bool indent )
{
	upCvasErr err;
	byte singleCharRead = 0;
	char tempUserInput[ MAX_CHAR_CAPTURE + 1 ];
	twobytes bytesRead;
	int i = 0;

	if( indent )
		err = ConsoleOutput( "   --> ", NO_LINEFEED );
	else
		err = ConsoleOutput( "--> ", NO_LINEFEED );
	
	// capture data from the user until an carriage return is entered
	while ( singleCharRead != ASCII_ENTER )
		{
		singleCharRead = 0;
		bytesRead = 0;
		// loop forever until a character is read
		while ( bytesRead == 0 )
			{
			err = ReadPort( m_eDefaultCom, 1, &singleCharRead, &bytesRead, 100 );
			}
		// check for a numeric entry
		if ( singleCharRead >= ASCII_ZERO && singleCharRead <= ASCII_NINE && i < MAX_CHAR_CAPTURE )
			{
			err = ConsoleEcho( singleCharRead );
			tempUserInput[ i++ ] = singleCharRead;
			}
		// if the captured character is a backspace, then echo and subtract
		// 1 from the capture index
		else if ( ASCII_BACKSPACE == singleCharRead  )
			{
			if ( i > 0 )
				{
				// erase character in the space the cursor resides on
				// as well as the one behind it
				err = ConsoleEcho( (char) ASCII_SPACE );
				err = ConsoleEcho( (char) ASCII_BACKSPACE );
				err = ConsoleEcho( (char) ASCII_BACKSPACE );
				err = ConsoleEcho( (char) ASCII_SPACE );
				err = ConsoleEcho( (char) ASCII_BACKSPACE );
				i--;
				}
			}
		}
	if ( i > 0 )
		{
		tempUserInput[ i ] = '\0';
		// convert user input to a number
		*userSelection = strtoul( tempUserInput, NULL, 10 );
		}
	else
		*userSelection = NO_SELECTION;
	return err;
}

/**
 * @brief	This function displays a prompt and captures user input
 *			up to a maximum amount.  Only numerical values are
 *			stored.
 *
 * @param	ePort The UARTn port used for output
 * @param   userSelection The user selection captured through UARTn
 *
 * @return   The selection from the user
 */
upCvasErr CConsole::ConsolePrompt2( const char *prompt, long * userSelection, bool indent )
{
	upCvasErr err;
	byte singleCharRead = 0;
	char tempUserInput[ MAX_CHAR_CAPTURE + 1 ];
	twobytes bytesRead;
	int i = 0;

	err = ConsoleOutput( prompt , NO_LINEFEED );
	if( indent )
		err = ConsoleOutput( "   --> ", NO_LINEFEED );
	else
		err = ConsoleOutput( "--> ", NO_LINEFEED );
	
	// capture data from the user until an carriage return is entered
	while ( singleCharRead != ASCII_ENTER )
		{
		singleCharRead = 0;
		bytesRead = 0;
		// loop forever until a character is read
		while ( bytesRead == 0 )
			{
			err = ReadPort( m_eDefaultCom, 1, &singleCharRead, &bytesRead, 100 );
			}
		// check for a numeric entry
		if ( singleCharRead >= ASCII_ZERO && singleCharRead <= ASCII_NINE && i < MAX_CHAR_CAPTURE )
			{
			err = ConsoleEcho( singleCharRead );
			tempUserInput[ i++ ] = singleCharRead;
			}
		// if the captured character is a backspace, then echo and subtract
		// 1 from the capture index
		else if ( ASCII_BACKSPACE == singleCharRead  )
			{
			if ( i > 0 )
				{
				// erase character in the space the cursor resides on
				// as well as the one behind it
				err = ConsoleEcho( (char) ASCII_SPACE );
				err = ConsoleEcho( (char) ASCII_BACKSPACE );
				err = ConsoleEcho( (char) ASCII_BACKSPACE );
				err = ConsoleEcho( (char) ASCII_SPACE );
				err = ConsoleEcho( (char) ASCII_BACKSPACE );
				i--;
				}
			}
		}
	if ( i > 0 )
		{
		tempUserInput[ i ] = '\0';
		// convert user input to a number
		*userSelection = strtoul( tempUserInput, NULL, 10 );
		}
	else
		*userSelection = NO_SELECTION;
	return err;
}

/**
 * @brief   This function displays a single character via RS-232
 *
 * @param	outputChar The character to display
 * @param   ePort The UARTn port used for output
 *
 * @return   Status as upCvasErr
 */
upCvasErr CConsole::ConsoleEcho( char outputChar )
{
	upCvasErr eErr = OK;
	byte tempBuffer[ 1 ];

	tempBuffer[ 0 ] = (byte) outputChar;

	while ( ERR_NOT_READY == ( eErr = WritePort( m_eDefaultCom, 1, tempBuffer ) ) );

	return eErr;
}


/**
 * @brief	This function displays a 16-bit value returned from the MAX6675
 * 			in the form of a binary number.  Special bits are separated from
 * 			the 12-bit digitized thermocouple reading.
 *
 * @param	rawVale The raw value to display
 *
 * @return   Status as upCvasErr
 */
upCvasErr CConsole::ConsoleRawToBinaryOutput( int rawValue )
{
	upCvasErr err = OK;
	byte outBuffer[ 16 ];
	int i = 0;

	sprintf( (char*) outBuffer, "%d", rawValue >> 15 & 0x01 );
	err = ConsoleOutput( (char*) outBuffer, NO_LINEFEED );
	err = ConsoleOutput( " ", NO_LINEFEED );
	for ( i = 1; i < 13; i++ )
		{
		sprintf( (char*) outBuffer, "%d", rawValue >> ( 15 - i ) & 0x01 );
		err = ConsoleOutput( (char*) outBuffer, NO_LINEFEED );
		}
	err = ConsoleOutput( " ", NO_LINEFEED );
	for ( i = 13; i < 16; i++ )
		{
		sprintf( (char*) outBuffer, "%d", rawValue >> ( 15 - i ) & 0x01 );
		err = ConsoleOutput( (char*) outBuffer, NO_LINEFEED );
		}
	return err;
}

/**
 * @brief	This function sends a specified number of carriage
 * 			returns to the console.
 *
 * @param	newLines The number of line feeds to transmit
 * @param	ePort The UARTn port used for output
 *
 * @return   Status as upCvasErr
 */
upCvasErr CConsole::ConsoleCarriageReturn( byte newLines )
{
	upCvasErr err = OK;

	char outChar = '\r';
	byte i;

	err = ConsoleEcho( outChar );
	outChar = '\n';
	for ( i = 0; i < newLines; i++ )
		{
		err = ConsoleEcho( outChar );
		}

	return err;
}


/**
 * @file       main.cpp
 * @par        Package: HdwLibUnitTests
 * @par        Project: CVAS Generator
 * @par        Copyright (c) 2016, Uptake Medical, Inc.
 * @author     Chris Mar 21, 2012 Created file
 * @brief      This file contains the main function which is the entry point for the program
 *
 */

// Include  Files

#include "CvasMcuDefinitions.h"
#include "CvasMcuTypes.h"
#include "avr/io.h"
#include "avr/interrupt.h"
#include "Console.h"

/**
 * @mainpage CVAS Real-Time Hardware Library Unit Tests
 *
 * @section intro_sec Introduction
 *
 * This document give a detailed description unit test console for the hardware library unit tests. \n
 *
 *
 *
 * @section Reference_sec References
 *	[1] 1242-001 Software Requirement Specification Rezum generator
 *
 *	[2] 1219-001 Design Document, CVAS Real-Time Software Architecture
 *
 *	[3] 1220-001 CVAS Inter-Processor Communication Protocol Specification
 *
 *	[4] 1086-002 Detailed Operating Procedure, Software Coding Standard Rev. A
 *
 *  [5] 1247-001 Requirement Specification, BPH Convective Vapor Ablation System Custom Hardware, Rev 3
 *
 *  [6] 1217-001 Design Document, CVAS Generator System Architecture Specification, Rev. 7
 *
 *  [7] 1256-001 Design Document, CVAS Generator Real-Time Control Procedures, Rev, 5
 *
 *
 *
 * @section overview_sec Design Overview
 *
 */
// External Public Data

// This flag controls execution that is dependent on being on real hardware or not
int g_bIsSimulated = 0;

// File Scope Macros and Constants

// File Scope Type Definitions (Enums, Structs & Classes)

// initialize XMEM interface immediately after reset
void Init_EXRAM( void ) __attribute__ ((naked))
__attribute__ ((section (".init3")));
void Init_EXRAM( void )
{
	XMCRA = ( 1 << SRE ) | ( 1 << SRW11 ) | ( 1 << SRW10 ) | ( 1 << SRW01 ) | ( 1 << SRW00 );
	// disabling bus keeper, set PORTC as MSB address byte
	XMCRB = 0;
#if ! defined(CVAS_SIM)
	for ( byte * pByte = (byte *) XMEM_END_ADDR; pByte >= (byte *) XMEM_START_ADDR; pByte-- )
		*pByte = 0;
#endif
}

// File Scope Data
FUSES =
{ ( FUSE_SUT1 ), // .low 0xEF
  ( FUSE_SPIEN & FUSE_JTAGEN ), // .high 0x9F
  ( FUSE_BODLEVEL2 ) }; // .extended BOD level 0xEF for 4.1V


// File Scope Functions

// Public Functions

/**
 * @brief	This method is used by the compiler to represent pure virtual methods.
 *
 * It should never be called as a pure virtual method that is not overridden is a compile or link error.
 *
 * @return   none
 */
extern "C" void __cxa_pure_virtual()
{
	while ( 1 )
		;
}

/**
 * @brief	This is the entry point for the program. It instantiates the unit test console
 * 			after initializing the necessary hardware.
 *
 * @return   Status as upCvasErr
 */

int main()
{
	upCvasErr err;
	CConsole cConsole;
	// Set clock prescaler to no divider
	CLKPR = ( 1 << CLKPCE );
	CLKPR = 0;

	// Enable interrupts
	sei();

	// Initialize the hardware via the console
	err = cConsole.InitializeHardware();
	cConsole.ConsoleHello();

	while (1)
	{
	err = cConsole.ConsoleMainMenu();
	}

	return 0;
}

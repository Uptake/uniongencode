/**
 * @file       Console.h
 * @par        Package: HdwLibUnitTests
 * @par        Project: CVAS Generator
 * @par        Copyright (c) 2016, Uptake Medical, Inc.
 * @author     Chris Mar 21, 2012 Created file
 * @brief      This file contains the console display function prototypes
 */

#ifndef CONSOLE_H_
#define CONSOLE_H_

// Include Files
#include "CvasMcuError.h"
#include "CvasMcuDefinitions.h"
#include "CvasMcuTypes.h"
#include "RS232.h"
#include "OneWireDriver.h"

// Public Macros and Constants

// Public Type Definitions (Enums, Structs & Classes)
#define OUT_BUFFER_MAX (50)

class CConsole : private CRS232, public COneWireDriver
{
private:
	upRS232Port m_eDefaultCom;

	// Console Prototypes
	upCvasErr ConsoleOutput( const char * outLine, byte lineFlag );
	upCvasErr ConsoleOutputIndexed( byte bIndex, const char * outLine, byte lineFlag, bool indent );
	upCvasErr ConsolePrompt( long * userSelection, bool indent );
	upCvasErr ConsolePrompt2( const char * prompt, long * userSelection, bool indent );
	upCvasErr ConsoleEcho( char outputChar );
	upCvasErr ConsoleRawToBinaryOutput( int rawValue );
	upCvasErr ConsoleCarriageReturn( byte newLines );

	// Temperature Prototypes
	upCvasErr ConsoleTemperatureSubMenu();
	upCvasErr Temperature1();
	upCvasErr Temperature2();
	upCvasErr Temperature3();
	upCvasErr Temperature4();

	// One Wire Prototypes
	upCvasErr ConsoleOneWireSubMenu();
	upCvasErr OneWire1();
	upCvasErr OneWire2();
	upCvasErr OneWire3();
	upCvasErr OneWire4();
	upCvasErr OneWire5();
	upCvasErr OneWire6();
	upCvasErr OneWire7( int action );
	upCvasErr OneWire8( int input1, int input2 );
	upCvasErr OneWire9( int input1, int input2, byte step );

	// Mem Map Prototypes
	upCvasErr ConsoleMMapSubMenu();
	upCvasErr MMap1();
	upCvasErr MMap2( int action );
	upCvasErr MMap3( int action );
	upCvasErr MMap4( int action );
	upCvasErr MMap5( int action );
	upCvasErr MMap6( int action );

	// SPI Prototypes
	// SPI seems fully covered by Temperature. Disable for now.
	//upCvasErr ConsoleSpiSubMenu();
	//upCvasErr Spi1();
	//upCvasErr Spi2();
	//upCvasErr Spi3();

	
public:
	CConsole();
	upCvasErr InitializeHardware();
	upCvasErr ConsoleHello();
	upCvasErr ConsoleMainMenu();
};
#endif /* CONSOLE_H_ */


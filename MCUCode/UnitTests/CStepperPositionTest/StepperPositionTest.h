/**
 * @file	StepperPositionTest.h
 * @par		Package: CvasMcu
 * @par		Project: CVAS Generator
 * @par		Copyright (c) 2016, Uptake Medical, Inc.
 * @author	Glen Mar 25, 2011 Created file
 * @brief	This file contains
 */

#ifndef STEPPERPOSITIONTEST_H_
#define STEPPERPOSITIONTEST_H_

// Include Files
#include "McUnit.h"
#include "TestMain.h"
#include "UnitTestIntfc.h"
#include "StepperPosition.h"

// Public Macros and Constants

// Public Type Definitions (Enums, Structs & Classes)

/**
 * @class	CStepperPositionTest
 * @brief	This class provides tests of the CStepperPosition class
 *
 * Virtual Functions Overridden: @n@n
 *  UnitTestListSetup - Set up list of Suites, Cases and Tests to be execute @n
 * 	CommonTesthandle - Called to have tests run @n
 *
 * External Data Members: None @n@n
 *
 */


class CStepperPositionTest: private CMcUnit<CStepperPositionTest>, public CTestMain, public CUnitTestIntfc, private CStepperPosition
{
public:
	CStepperPositionTest();
	int CommonTestHandle();
	void UnitTestListSetup();
	// Case 1: Invocation Tests
	MCUNIT_TEST_DEC( AllPublicMethods_Invocation );
	// Case 2: Parameter Tests
	//MCUNIT_TEST_DEC( <Method>_Parameter );
	// Case 3: Interaction Tests
	MCUNIT_TEST_DEC( StepperPositionEncoder_Interaction );

	SMcUnitTest sMcUnitTestList[3];	// Increment index for each test added, regardless of the case in which it falls
	SMcUnitCase sMcUnitCase[3];
	SMcUnitCase * sMcUnitCaseList[4];
	SMcUnitSuite sMcUnitTestSuite;

};

// Inline Functions (follows class definition)


#endif /* STEPPERPOSITIONTEST_H_ */

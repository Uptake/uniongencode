/**
 * @file	StepperPositionTest.cpp
 * @par		Package: CvasMcu
 * @par		Project: CVAS Generator
 * @par		Copyright (c) 2016, Uptake Medical, Inc.
 * @author	Glen Mar 25, 2011 Created file
 * @brief	This file contains
 */

// Include Files
#include "StepperPositionTest.h"
#include "SimDataAccess.h"

// External Public Data

// File Scope Macros and Constants

// File Scope Type Definitions (Enums, Structs & Classes)

// File Scope Data
static CStepperPositionTest f_CStepperPositionTest; // Instantiate one so it registers itself

// File Scope Functions

// Public Functions

/**
 * @brief	This is the constructor for StepperPositionTest
 *
 */
CStepperPositionTest::CStepperPositionTest() :
	CMcUnit<CStepperPositionTest> ( this ), CTestMain( (CUnitTestIntfc*) this )
{
}

/**
 * @brief	This method calls into CMcUnit to have the tests run.
 *
 * @return   Always 1
 */
int CStepperPositionTest::CommonTestHandle()
{
	McUnitSuiteRun( &sMcUnitTestSuite );
	McUnitSuiteReport( &sMcUnitTestSuite, DEPTH_TEST_REPORT );
	return 1;
}

/**
 * @brief	This method sets up the lists of tests to run
 *
 */
void CStepperPositionTest::UnitTestListSetup()
{
	int iList = 0;
	int iCase = 0;

	// Set up Case 1: Invocation Tests
	ADD_TEST_CASE( sMcUnitCase, iCase, sMcUnitTestList + iList, 1, "Invocation Tests", "Invocation Test failure")
	ADD_TEST_LIST( sMcUnitTestList, iList, &CStepperPositionTest::AllPublicMethods_Invocation, "All Public Methods invocation", "unexpected result" )

	// Set up Case 2: Parameter Tests
	ADD_TEST_CASE( sMcUnitCase, iCase, sMcUnitTestList + iList, 0, "Parameter Tests (No tests defined)", "")
	//Delete the preceding line and uncomment the two lines below to begin adding Parameter Tests
	//ADD_TEST_CASE( sMcUnitCase, iCase, sMcUnitTestList + iList, 1, "Parameter Tests", "Parameter Test failure")
	//ADD_TEST_LIST( sMcUnitTestList, iList, &CStepperPositionTest::<Method>_Parameter, "<Method>() parameter", "unexpected result" )

	// Set up Case 3: Interaction Tests
	ADD_TEST_CASE( sMcUnitCase, iCase, sMcUnitTestList + iList, 1, "Interaction Tests", "Interaction Test failure")
	ADD_TEST_LIST( sMcUnitTestList, iList, &CStepperPositionTest::StepperPositionEncoder_Interaction, "Stepper Position Encoder Interaction", "unexpected result" )

	MAKE_TEST_SUITE( sMcUnitTestSuite, sMcUnitCaseList, sMcUnitCase, iCase, "CStepperPositionTest" )
}

//=============================================================================
// CASE 1: INVOCATION TESTS
//=============================================================================

/**
* @brief	This method tests the invocation of all public methods with valid parameters.
*
* @return  result as an int: 0 (UNIT_TEST_PASSED) if passed, not 0 if failed
*/
MCUNIT_TEST( CStepperPositionTest, AllPublicMethods_Invocation )
{
	int iResult = UNIT_TEST_PASSED;
	int	iInvocations = 0;
	long int liPosition;
	int iVelocity;

	// Invoke and check return codes
	iResult |= McUnitAssertIsEqual( OK, ReadStepperPosition( (twobytes)100 ), "ReadStepperPosition()" );
	iInvocations++;

	iResult |= McUnitAssertIsEqual( OK, ResetStepperPosition(), "ResetStepperPosition()" );
	iInvocations++;

	// Expect position and velocity to initially be zero
	iResult |= McUnitAssertIsEqual( OK, GetStepperPosition( &liPosition, &iVelocity ), "GetStepperPosition()" );
	iResult |= McUnitAssertIsEqual( 0, (int)liPosition, "Position value" );
	iResult |= McUnitAssertIsEqual( 0, iVelocity, "Velocity value" );
	iInvocations++;

	LogOneValue("Number of methods tested: %d", iInvocations);

	return iResult;
}


//=============================================================================
// CASE 2: PARAMETER TESTS
//=============================================================================

/**
* @brief	This method tests the passed parameter(s) of TBD.
*
* @return  result as an int: 0 (UNIT_TEST_PASSED) if passed, not 0 if failed
*/

//MCUNIT_TEST( CStepperPositionTest, <Method>_Parameter )
//{
//}


//=============================================================================
// CASE 3: INTERACTION TESTS
//=============================================================================

/**
* @brief	This method tests the interaction of stepper position with the encoder.
*
* @return  result as an int: 0 (UNIT_TEST_PASSED) if passed, not 0 if failed
*/
MCUNIT_TEST( CStepperPositionTest, StepperPositionEncoder_Interaction )
{
	int iResult = UNIT_TEST_PASSED;
	long int liPosition;
	int iVelocity;
	SHdwSimData sHdwSimData;

	sHdwSimData.m_encoderPos = 1000;
	f_simDataAccess.SetSimData( &sHdwSimData );

	iResult |= McUnitAssertIsEqual( OK, ResetStepperPosition(), "ResetStepperPosition()" );

	sHdwSimData.m_encoderPos = 1000;
	f_simDataAccess.SetSimData( &sHdwSimData );

	iResult |= McUnitAssertIsEqual( OK, ReadStepperPosition( RND_ROBIN_TICK_CNT ), "ReadStepperPosition()" );
	iResult |= McUnitAssertIsEqual( OK, ReadStepperPosition( RND_ROBIN_TICK_CNT ), "ReadStepperPosition()" );
	iResult |= McUnitAssertIsEqual( OK, ReadStepperPosition( RND_ROBIN_TICK_CNT ), "ReadStepperPosition()" );
	iResult |= McUnitAssertIsEqual( OK, ReadStepperPosition( RND_ROBIN_TICK_CNT ), "ReadStepperPosition()" );
	iResult |= McUnitAssertIsEqual( OK, ReadStepperPosition( RND_ROBIN_TICK_CNT ), "ReadStepperPosition()" );

	iResult |= McUnitAssertIsEqual( OK, GetStepperPosition( &liPosition, &iVelocity ), "GetStepperPosition()" );
	iResult |= McUnitAssertIsEqual( 1000, (int)liPosition, "Position value" );
	iResult |= McUnitAssertIsEqual( 0, iVelocity, "Velocity value" );

	return iResult;
}


// Private Class Functions



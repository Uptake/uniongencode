/**
 * @file	TimeBaseTest.cpp
 * @par		Package: CvasMcu
 * @par		Project: CVAS Generator
 * @par		Copyright (c) 2016, Uptake Medical, Inc.
 * @author	Kurt Jan 12, 2012 Created file
 * @brief	This file contains unit tests for CTimeBase
 */

// Include Files
#include "TimeBaseTest.h"

// External Public Data

// File Scope Macros and Constants

// File Scope Type Definitions (Enums, Structs & Classes)

// File Scope Data
static CTimeBaseTest f_TimeBaseTest; // Instantiate one so it registers itself

// File Scope Functions

// Public Functions

/**
 * @brief	This is the constructor for CTimeBaseTest
 *
 */

CTimeBaseTest::CTimeBaseTest() :
	CMcUnit<CTimeBaseTest> ( this ), CTestMain( (CUnitTestIntfc*) this )
{
}


/**
 * @brief	This method calls into CMcUnit to have the tests run.
 *
 * @return   Always 1
 */
int CTimeBaseTest::CommonTestHandle()
{
	McUnitSuiteRun( &sMcUnitTestSuite );
	McUnitSuiteReport( &sMcUnitTestSuite, DEPTH_TEST_REPORT );
	return 1;
}

/**
 * @brief	This method sets up the lists of tests to run
 *
 */
void CTimeBaseTest::UnitTestListSetup()
{

	int iList = 0;
	int iCase = 0;

	// Set up Case 1: Invocation Tests
	ADD_TEST_CASE( sMcUnitCase, iCase, sMcUnitTestList + iList, 1, "Invocation Tests", "Invocation Test failure")
	ADD_TEST_LIST( sMcUnitTestList, iList, &CTimeBaseTest::AllPublicMethods_Invocation, "All Public Methods invocation", "unexpected result" )

	// Set up Case 2: Parameter Tests
	ADD_TEST_CASE( sMcUnitCase, iCase, sMcUnitTestList + iList, 2, "Parameter Tests", "Parameter Test failure")
	ADD_TEST_LIST( sMcUnitTestList, iList, &CTimeBaseTest::GetTickDiff_Parameter, "Get Tick Diff parameter", "unexpected result" )
	ADD_TEST_LIST( sMcUnitTestList, iList, &CTimeBaseTest::SetGetTime_Parameter, "Set/Get Time parameter", "unexpected result" )

	// Set up Case 3: Interaction Tests
	ADD_TEST_CASE( sMcUnitCase, iCase, sMcUnitTestList + iList, 1, "Interaction Tests", "Interaction Test failure")
	ADD_TEST_LIST( sMcUnitTestList, iList, &CTimeBaseTest::TimeHardware_Interaction, "Time/Hardware Interaction", "unexpected result" )

	MAKE_TEST_SUITE( sMcUnitTestSuite, sMcUnitCaseList, sMcUnitCase, iCase, "CTimeBaseTest" )

}

//=============================================================================
// CASE 1: INVOCATION TESTS
//=============================================================================

/**
 * @brief	This method tests the invocation of all public methods with valid parameters.
 *
 * @return  result as an int: 0 (UNIT_TEST_PASSED) if passed, not 0 if failed
 */
MCUNIT_TEST( CTimeBaseTest, AllPublicMethods_Invocation )
{
 	int iResult = UNIT_TEST_PASSED;
 	time timeValue;
 	tick tickValue;
 	twobytes tbValue;
 	int	iInvocations = 0;

	// Invoke and check return codes (if there is one)
	iResult |= McUnitAssertIsEqual( OK, CTimeBase::InitializeHardware(), "InitializeHardware()" );
	iInvocations++;

	iResult |= McUnitAssertIsEqual( OK, CTimeBase::ReadFromHardware(), "ReadFromHardware()" );
	iInvocations++;

	iResult |= McUnitAssertIsEqual( OK, CTimeBase::GetTime( &timeValue ), "GetTime()" );
	iInvocations++;

	iResult |= McUnitAssertIsEqual( OK, CTimeBase::SetTime( timeValue ), "SetTime()" );
	iInvocations++;

	iResult |= McUnitAssertIsEqual( OK, CTimeBase::GetTick( &tickValue ), "GetTick()" );
	iInvocations++;

	iResult |= McUnitAssertIsEqual( (bool_c)false, CTimeBase::GetTickElapsed( (tick)0, &tbValue ), "GetTickElapsed() two bytes returned" );
	iInvocations++;

	iResult |= McUnitAssertIsEqual( (bool_c)false, CTimeBase::GetTickElapsed( (tick)0, &tickValue ), "GetTickElapsed() tick returned" );
	iInvocations++;

	// Check the two byte difference as well since it is known
	iResult |= McUnitAssertIsEqual( (bool_c)false, CTimeBase::GetTickDiff( (tick)100, (tick)42, &tbValue ), "GetTickDiff() two bytes returned" );
	iResult |= McUnitAssertIsEqual( (twobytes)58, tbValue, "Two byte difference" );
	iInvocations++;

	// Check the tick difference as well since it is known
	iResult |= McUnitAssertIsEqual( (bool_c)false, CTimeBase::GetTickDiff( (tick)200, (tick)99, &tickValue ), "GetTickDiff() tick returned" );
	iResult |= McUnitAssertIsEqual( (tick)101, tickValue, "Tick difference" );
	iInvocations++;

	iResult |= McUnitAssertIsEqual( OK, CTimeBase::MsDelay( (twobytes)10 ), "MsDelay()" );
	iInvocations++;

	LogOneValue("Number of methods tested: %d", iInvocations);

	return iResult;
}


//=============================================================================
// CASE 2: PARAMETER TESTS
//=============================================================================

/**
 * @brief	This method tests the passed parameter(s) of the GetTickDiff() method.
 * 			Rollover and non-rollover cases checked for min and max difference values for each data type.
 *
 * @return  result as an int: 0 (UNIT_TEST_PASSED) if passed, not 0 if failed
 */

MCUNIT_TEST( CTimeBaseTest, GetTickDiff_Parameter )
{
	int iResult = UNIT_TEST_PASSED;
	tick tickValue = 0;
	twobytes tbValue = 0;

	// Check minimum for tick data type with rollover
	// bWrap should be set to true to indicate the timer rolled over; difference should be 1
	iResult |= McUnitAssertIsEqual( (bool_c)true, GetTickDiff( (tick)0, (tick)0xFFFFFFFF, &tickValue ), "Min tick diff w/ rollover, flag value");
	iResult |= McUnitAssertIsEqual( (tick)1, tickValue, "Min tick diff w/ rollover, difference value" );

	// Check minimum for tick data type without rollover
	// bWrap should be set to false to indicate no rollover; difference should be 0
	iResult |= McUnitAssertIsEqual( (bool_c)false, GetTickDiff( (tick)0xF0000000, (tick)0xF0000000, &tickValue ), "Min tick diff no rollover, flag value");
	iResult |= McUnitAssertIsEqual( (tick)0, tickValue, "Min tick diff no rollover, difference value" );

	// Check maximum for tick data type with rollover
	// bWrap should be set to true to indicate timer rolled; difference should be 0xFFFFFFFF
	iResult |= McUnitAssertIsEqual( (bool_c)true, GetTickDiff( (tick)0xF0000000, (tick)0xF0000001, &tickValue ), "Max tick diff w/ rollover, flag value");
	iResult |= McUnitAssertIsEqual( (tick)0xFFFFFFFF, tickValue, "Max tick diff w/ rollover, difference value" );

	// Check maximum for tick data type without rollover
	// bWrap should be set to false to indicate no rollover; difference should be 0xFFFFFFFF
	iResult |= McUnitAssertIsEqual( (bool_c)false, GetTickDiff( (tick)0xFFFFFFFF, (tick)0, &tickValue ), "Max tick diff no rollover, flag value");
	iResult |= McUnitAssertIsEqual( (tick)0xFFFFFFFF, tickValue, "Max tick diff value no rollover, difference value" );

	// Check minimum for twobyte type with rollover
	// bWrap should be set to true to indicate the timer rolled over; difference should be 1
	iResult |= McUnitAssertIsEqual( (bool_c)true, GetTickDiff( (twobytes)0, (twobytes)0xFFFF, &tbValue ), "Min twobyte diff w/ rollover, flag value");
	iResult |= McUnitAssertIsEqual( (twobytes)1, tbValue, "Min twobyte diff w/ rollover, difference value" );

	// Check minimum for twobyte type without rollover
	// bWrap should be set to false to indicate no rollover; difference should be 0
	iResult |= McUnitAssertIsEqual( (bool_c)false, GetTickDiff( (twobytes)0xF000, (twobytes)0xF000, &tbValue ), "Min twobyte diff no rollover, flag value");
	iResult |= McUnitAssertIsEqual( (twobytes)0, tbValue, "Min twobyte diff no rollover, difference value" );

	// Check maximum for twobyte data type with rollover
	// bWrap should be set to true to indicate timer rolled; difference should be 0xFFFF
	iResult |= McUnitAssertIsEqual( (bool_c)true, GetTickDiff( (twobytes)0xF000, (twobytes)0xF001, &tbValue ), "Max twobyte diff w/ rollover, flag value");
	iResult |= McUnitAssertIsEqual( (twobytes)0xFFFF, tbValue, "Max twobyte diff w/ rollover, difference value" );

	// Check maximum for twobyte data type without rollover
	// bWrap should be set to false to indicate no rollover; difference should be 0xFFFF
	iResult |= McUnitAssertIsEqual( (bool_c)false, GetTickDiff( (twobytes)0xFFFF, (twobytes)0, &tbValue ), "Max twobyte diff no rollover, flag value");
	iResult |= McUnitAssertIsEqual( (twobytes)0xFFFF, tbValue, "Max twobyte diff no rollover, difference value" );

	return iResult;
}

/**
 * @brief	This method tests the passed parameter(s) of the SetTime() and GetTime() methods.
 * 			It tests the range of values by filling with ones from LSB to MSB
 *
 * @return  result as an int: 0 (UNIT_TEST_PASSED) if passed, not 0 if failed
 */

MCUNIT_TEST( CTimeBaseTest, SetGetTime_Parameter )
{
	int iResult = UNIT_TEST_PASSED;
	time timeSetValue = 0;
	time timeGetValue = 0x55AA55AA;

	for (int i = 0; i <= 32; i++ )
	{
		SetTime( timeSetValue );
		GetTime( &timeGetValue );
		iResult |= McUnitAssertIsEqual( timeSetValue, timeGetValue, "Time Set/Get pair" );

		// Shift and fill with ones up to 32 bits
		timeSetValue <<= 1;
		timeSetValue++;
	}

	return iResult;
}

//=============================================================================
// CASE 3: INTERACTION TESTS
//=============================================================================

/**
 * @brief	This method tests the interaction of GetTick() and GetTime() methods with the hardware.
 *
 * @return  result as an int: 0 (UNIT_TEST_PASSED) if passed, not 0 if failed
 */

MCUNIT_TEST( CTimeBaseTest, TimeHardware_Interaction )
{
	int iResult = UNIT_TEST_PASSED;
	tick tickStart = 0xAA;
	tick tickEnd = 0xDD;
	tick tickDiff = 0;
	int iDelay = 10;
	time timeStart = 0xAA;
	time timeEnd = 0xDD;

	// Check a few tick delays
	for( int i = 0; i < 3; i++ )
	{
		GetTick( &tickStart );
		MsDelay( iDelay );
		GetTick( &tickEnd );

		GetTickDiff( tickEnd, tickStart, &tickDiff );
		iResult |= McUnitAssertIsEqual( (tick)iDelay, tickDiff, "Tick Delay Value in mSec" );
		iDelay *= 10;
	}

	// Check a few time delays
	for( int j = 1; j <= 3; j++ )
	{
		CTimeBase::ReadFromHardware();
		GetTime( &timeStart );
		MsDelay( 1000*j );
		CTimeBase::ReadFromHardware();
		GetTime( &timeEnd );

		iResult |= McUnitAssertIsEqual( (time)j, timeEnd - timeStart, "Time Delay Value in seconds" );
	}

	return iResult;
}
/*
 *  * @brief	This method tests the increment of the seconds timer
 * @brief   via the ReadFromHardware() method in CTimeBase
 *
 * @param	_test An SMcUnitTest* to test
 *
 * @return   result as an int
MCUNIT_TEST( CTimeBaseTest, UnitTestIncrSecondsTimer )
{
	int result = UNIT_TEST_PASSED;
	time tFirstTime = 0xAA;
	time tIncrementedTime = 0xDD;
	CTimeBase::ReadFromHardware();
	GetTime( &tFirstTime );
	MsDelay( 1000 );
	CTimeBase::ReadFromHardware();
	GetTime( &tIncrementedTime );
	result = McUnitAssertIsEqual( tIncrementedTime, (time) ( tFirstTime + 1 ) );
	if ( UNIT_TEST_FAILED == result )
		{
		char tempStr[ 40 ];
		snprintf( tempStr, 40, "seconds timer failed, reads: %lu\n", tIncrementedTime );
		f_simDataAccess.PrintToSimulatorLog( tempStr );
		}
	return result;
}
*/

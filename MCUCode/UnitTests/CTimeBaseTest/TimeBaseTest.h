/**
 * @file	TimeBaseTest.h
 * @par		Package: CvasMcu
 * @par		Project: CVAS Generator
 * @par		Copyright (c) 2016, Uptake Medical, Inc.
 * @author	Kurt Jan 12, 2012 Created file
 * @brief	This file contains unit tests for CTimeBase
 */

#ifndef TIMEBASETEST_H_
#define TIMEBASETEST_H_

// Include Files
#include <TimeBase.h>
#include "McUnit.h"
#include "UnitTestIntfc.h"
#include "TestMain.h"

// Public Macros and Constants

// Public Type Definitions (Enums, Structs & Classes)

/**
 * @class	CTimeBaseTest
 * @brief	This class provides unit tests for CTimeBase
 *
 * Virtual Functions Overridden: @n@n
 *  UnitTestListSetup - Set up list of Suites, Cases and Tests to be execute @n
 * 	CommonTesthandle - Called to have tests run @n
 *
 * External Data Members: None @n@n
 *
 */


class CTimeBaseTest: private CMcUnit<CTimeBaseTest>, public CUnitTestIntfc, public CTestMain, private CTimeBase
{
public:
	CTimeBaseTest();
	int CommonTestHandle();
	void UnitTestListSetup();
	// Case 1: Invocation Tests
	MCUNIT_TEST_DEC( AllPublicMethods_Invocation );
	// Case 2: Parameter Tests
	MCUNIT_TEST_DEC( GetTickDiff_Parameter );
	MCUNIT_TEST_DEC( SetGetTime_Parameter );
	// Case 3: Interaction Tests
	MCUNIT_TEST_DEC( TimeHardware_Interaction );

	SMcUnitTest sMcUnitTestList[4];	// Increment index for each test added, regardless of the case in which it falls
	SMcUnitCase sMcUnitCase[3];
	SMcUnitCase * sMcUnitCaseList[4];
	SMcUnitSuite sMcUnitTestSuite;
};

// Inline Functions (follows class definition)


#endif /* TIMEBASETEST_H_ */

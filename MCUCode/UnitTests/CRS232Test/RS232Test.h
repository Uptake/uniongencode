/**
 * @file	RS232Test.h
 * @par		Package: CvasMcu
 * @par		Project: CVAS Generator
 * @par		Copyright (c) 2016, Uptake Medical, Inc.
 * @author	Kurt Dec 29, 2011 Created file
 * @brief	This file contains unit tests for CRS232
 */

#ifndef RS232TEST_H_
#define RS232TEST_H_

// Include Files
#include <RS232.h>
#include "McUnit.h"
#include "UnitTestIntfc.h"
#include "TestMain.h"

// Public Macros and Constants

// Public Type Definitions (Enums, Structs & Classes)

/**
 * @class	CRS232Test
 * @brief	This class provides unit tests for CRS232
 *
 * Virtual Functions Overridden: @n@n
 *  UnitTestListSetup - Set up list of Suites, Cases and Tests to be execute @n
 * 	CommonTesthandle - Called to have tests run @n
 *
 * External Data Members: None @n@n
 *
 */

class CRS232Test: private CMcUnit<CRS232Test> , public CUnitTestIntfc, public CTestMain, private CRS232
{
public:
	CRS232Test();
	int CommonTestHandle();
	void UnitTestListSetup();

	// Case 1: Invocation Tests
	MCUNIT_TEST_DEC( AllPublicMethods_Invocation );
	// Case 2: Parameter Tests
	MCUNIT_TEST_DEC( InitializePort_Parameter );
	MCUNIT_TEST_DEC( WritePort_Parameter );
	MCUNIT_TEST_DEC( ReadPort_Parameter );
	MCUNIT_TEST_DEC( FlushReadBuffer_Parameter );
	// Case 3: Interaction Tests
	//MCUNIT_TEST_DEC( <Name>_Interaction );

	SMcUnitTest sMcUnitTestList[ 5 ]; // Increment index for each test added, regardless of the case in which it falls
	SMcUnitCase sMcUnitCase[ 3 ];
	SMcUnitCase * sMcUnitCaseList[ 4 ];
	SMcUnitSuite sMcUnitTestSuite;
private:
	upCvasErr UnloadBuffer( upRS232Port ePort );
};

// Inline Functions (follows class definition)


#endif /* RS232TEST_H_ */

/**
 * @file	RS232Test.cpp
 * @par		Package: CvasMcu
 * @par		Project: CVAS Generator
 * @par		Copyright (c) 2016, Uptake Medical, Inc.
 * @author	Kurt Jan 4, 2012 Created file
 * @brief	This file contains unit tests for CRS232
 */

// Include Files
#include "RS232Test.h"
#include <avr/interrupt.h>

// External Public Data

// File Scope Macros and Constants

// File Scope Type Definitions (Enums, Structs & Classes)

// File Scope Data
static CRS232Test f_RS232Test; // Instantiate one so it registers itself

// File Scope Functions

// Public Functions

/**
 * @brief	This is the constructor for CRS232Test
 *
 */

CRS232Test::CRS232Test() :
	CMcUnit<CRS232Test> ( this ), CTestMain( (CUnitTestIntfc*) this )
{
}

/**
 * @brief	This method calls into CMcUnit to have the tests run.
 *
 * @return   Always 1
 */
int CRS232Test::CommonTestHandle()
{
	sei();
	McUnitSuiteRun( &sMcUnitTestSuite );
	McUnitSuiteReport( &sMcUnitTestSuite, DEPTH_TEST_REPORT );
	return 1;
}

/**
 * @brief	This method sets up the lists of tests to run
 *
 */
void CRS232Test::UnitTestListSetup()
{

	int iList = 0;
	int iCase = 0;

	// Set up Case 1: Invocation Tests
	ADD_TEST_CASE( sMcUnitCase, iCase, sMcUnitTestList + iList, 1, "Invocation Tests", "Invocation Test failure")
	ADD_TEST_LIST( sMcUnitTestList, iList, &CRS232Test::AllPublicMethods_Invocation, "All Public Methods invocation", "unexpected result" )

	// Set up Case 2: Parameter Tests
	ADD_TEST_CASE( sMcUnitCase, iCase, sMcUnitTestList + iList, 4, "Parameter Tests", "Parameter Test failure")
	ADD_TEST_LIST( sMcUnitTestList, iList, &CRS232Test::InitializePort_Parameter, "InitializePort() parameter", "unexpected result" )
	ADD_TEST_LIST( sMcUnitTestList, iList, &CRS232Test::WritePort_Parameter, "WritePort() parameter", "unexpected result" )
	ADD_TEST_LIST( sMcUnitTestList, iList, &CRS232Test::ReadPort_Parameter, "ReadPort() parameter", "unexpected result" )
	ADD_TEST_LIST( sMcUnitTestList, iList, &CRS232Test::FlushReadBuffer_Parameter, "FlushReadBuffer() parameter", "unexpected result" )

	// Set up Case 3: Integration
	ADD_TEST_CASE( sMcUnitCase, iCase, sMcUnitTestList + iList, 0, "Interaction Tests (No tests defined)", "")
	//Delete the preceding line and uncomment the two lines below to begin adding Interaction Tests
	//ADD_TEST_CASE( sMcUnitCase, iCase, sMcUnitTestList + iList, 1, "Interaction Tests", "Interaction Test failure")
	//ADD_TEST_LIST( sMcUnitTestList, iList, &<Method>_Interaction, "<Name> Interaction", "unexpected result" )

	MAKE_TEST_SUITE( sMcUnitTestSuite, sMcUnitCaseList, sMcUnitCase, iCase, "CRS232Test" )

}

//=============================================================================
// CASE 1: INVOCATION TESTS
//=============================================================================

/**
 * @brief	This method tests the invocation of all public methods with valid parameters.
 *
 * @return   result as an int
 */
MCUNIT_TEST( CRS232Test, AllPublicMethods_Invocation )
{
	int iResult = UNIT_TEST_PASSED;
	twobytes numBytesToSend = 2;
	byte pSendBuffer[ RF_SEND_BYTE_MAX ];
	twobytes tBytesToRead = RF_RECV_BYTE_MAX / 2;
	byte pRecvBuffer[ RF_RECV_BYTE_MAX ];
	twobytes tBytesRead;
	byte bWaitMS = 0;
	twobytes tbCount;
	upCvasErr eErr;

	// Invoke and check return code for port = 0, baud = 0, parity = 0, data bits = 5, and stop bits = 1
	eErr = InitializePort( (upRS232Port) 0, (upRS232Baud) 0, (upRS232Parity) 0, (byte) 5, (byte) 1 );
	iResult |= McUnitAssertIsEqual( OK, eErr, "InitializePort()" );

	// Parameters used in method invocation

	// Fill buffer with valid iTherm bytes
	for ( int i = 0; i < RF_SEND_BYTE_MAX; i++ )
		pSendBuffer[ i ] = 'o';

	// Invoke and check return code for RS232_USART0, numBytes = 2, and pBytesToWrite = a valid buffer
	eErr = WritePort( RS232_USART0, numBytesToSend, pSendBuffer );
	iResult |= McUnitAssertIsEqual( OK, eErr, "WritePort()" );

	// Invoke and check return code for ePort = RS232_USART0, tbGet = half of max, and pWait = 0
	eErr = ReadPort( RS232_USART0, tBytesToRead, pRecvBuffer, &tBytesRead, bWaitMS );
	iResult |= McUnitAssertIsEqual( OK, eErr, "ReadPort()" );

	// Invoke and check return code for ePort = RS232_USART0
	eErr = GetPendingSendCount( RS232_USART0, &tbCount );
	iResult |= McUnitAssertIsEqual( OK, eErr, "GetPendingSendCount()" );

	// Invoke and check return code for ePort = RS232_USART0
	eErr = FlushReadBuffer( RS232_USART0 );
	iResult |= McUnitAssertIsEqual( OK, eErr, "FlushReadBuffer()" );

	return iResult;
}

//=============================================================================
// CASE 2: PARAMETER TESTS
//=============================================================================

/**
 * @brief	This method tests the passed parameter(s) of the InitializePort() method.
 *			It checks return code for baud = 0 to 7, parity = 0 to 7, and data bits = 0 to 15.
 *			Baud is checked first, then parity, then data bits.
 *			The port and stop bits are not checked.  The test just toggles each between 0 and 1.
 *
 * @return   result as an int
 */

MCUNIT_TEST( CRS232Test, InitializePort_Parameter )
{
	int iResult = UNIT_TEST_PASSED;
	upCvasErr eErr;
	int iCombos = 0; // Count of combinations

	// Invoke and check return code for baud = 0 to 4, parity = 0 to 4, and data bits = 3 to 11
	// Baud is checked first, then parity, then data bits
	// The port and stop bits are not checked.  The test just toggles them between 0 and 1.
	// TODO Should RS232 port and/or stop bits be bounds checked in InitializePort()?
	for ( int i = 0; i < 5; i++ ) // Cycle through good and bad values for Baud
		for ( int j = 0; j < 5; j++ ) // Cycle through good and bad values for Parity
			for ( int k = 3; k < 12; k++ ) // Cycle through good and bad values for data bits
				{
				eErr = InitializePort( upRS232Port( k % 2 ), upRS232Baud( i ), upRS232Parity( j ), byte( k ), byte( k
				        % 2 ) );
				iCombos++;

				// Check for correct return code and OR results from each combination tested
				if ( i >= RS232_BAUD_CNT || j >= RS232_PARITY_CNT )
					iResult |= McUnitAssertIsEqual( ERR_ENUM_OUT_OF_RANGE, eErr, "Enum out of range" );
				else if ( k < 5 || k > 9 ) // Data bits is valid if it is between 5 and 9, inclusive.
					iResult |= McUnitAssertIsEqual( ERR_BAD_PARAM, eErr, "Bad parameter" );
				else
					iResult |= McUnitAssertIsEqual( OK, eErr, "Parameters are OK" );
				}

	LogOneValue( "Combinations tested: %d", iCombos );

	// Specifically confirm the expected settings
	iResult |= McUnitAssertIsEqual( OK, InitializePort( RS232_USART0, RS232_BAUD_115200, RS232_PARITY_NONE, 8, 1 ), \
			"InitializePort(Port0, 115200, None, 8, 1)" );
	iResult |= McUnitAssertIsEqual( OK, InitializePort( RS232_USART1, RS232_BAUD_115200, RS232_PARITY_NONE, 8, 1 ), \
			"InitializePort(Port1, 115200, None, 8, 1)" );

	return iResult;
}

/**
 * @brief	This method tests the passed parameter(s) of the WritePort() method.
 *			It tests writing to each port, and an invalid port.
 *			For each port, it tests valid cases of sending one byte, half a buffer, and a full buffer.
 *			For each port, it tests invalid cases of sending one more than a full buffer, and also a null pointer as a buffer.
 *
 * @return   result as an int
 */
MCUNIT_TEST( CRS232Test, WritePort_Parameter )
{
	int iResult = UNIT_TEST_PASSED;
	upCvasErr eErr;
	upRS232Port RS232_port;
	int bufferSize;
	byte pBuffer0[ RF_SEND_BYTE_MAX ];
	byte pBuffer1[ SBC_SEND_BYTE_MAX ];
	byte *pBytesToWrite;
	unsigned int i;
	twobytes numBytes;

	// Fill buffer 0 with valid iTherm bytes
	for ( i = 0; i < RF_SEND_BYTE_MAX; i++ )
		pBuffer0[ i ] = 'o';

	// Fill buffer 1 with bytes
	for ( i = 0; i < SBC_SEND_BYTE_MAX; i++ )
		pBuffer1[ i ] = i;

	// TODO Should there be a check for invalid port in WritePort()?
	// Test the same cases for each port
	for ( i = 0; i <= 1; i++ )
		{
		if ( 0 == i )
			{ // USART0 uses buffer0
			RS232_port = RS232_USART0;
			pBytesToWrite = pBuffer0;
			bufferSize = RF_SEND_BYTE_MAX;
			}
		else
			{ // USART1 uses buffer1
			RS232_port = RS232_USART1;
			pBytesToWrite = pBuffer1;
			bufferSize = SBC_SEND_BYTE_MAX;
			}

		// Test case of one byte
		numBytes = 1;
		eErr = WritePort( RS232_port, numBytes, pBytesToWrite );
		iResult |= McUnitAssertIsEqual( OK, eErr, "Send one byte" );

		// Test case of half buffer size
		numBytes = bufferSize / 2;
		eErr = WritePort( RS232_port, numBytes, pBytesToWrite );
		iResult |= McUnitAssertIsEqual( OK, eErr, "Send half buffer" );

		McUnitAssertIsEqual( OK, UnloadBuffer( RS232_port ), "Unload buffer" );

		// Test case of full buffer
		numBytes = bufferSize;
		eErr = WritePort( RS232_port, numBytes, pBytesToWrite );
		iResult |= McUnitAssertIsEqual( OK, eErr, "Send full buffer" );

		McUnitAssertIsEqual( OK, UnloadBuffer( RS232_port ), "Unload buffer" );

		// Test case of more than buffer size
		numBytes = bufferSize + 1;
		eErr = WritePort( RS232_port, numBytes, pBytesToWrite );
		iResult |= McUnitAssertIsEqual( ERR_BAD_PARAM, eErr, "Full buffer plus 1" );

		McUnitAssertIsEqual( OK, UnloadBuffer( RS232_port ), "Unload buffer" );

		// Test null pointer case
		numBytes = 1;
		pBytesToWrite = 0;
		eErr = WritePort( RS232_port, numBytes, pBytesToWrite );
		iResult |= McUnitAssertIsEqual( ERR_BAD_PARAM, eErr, "Null pointer" );
	}

	return iResult;
}

/**
 * @brief	This method tests the passed parameter(s) of the ReadPort() method.
 *			It tests reading from each port, and an invalid port.
 *
 * @return   result as an int
 */
MCUNIT_TEST( CRS232Test, ReadPort_Parameter )
{
#define BUFFER_SIZE	100

	int iResult = UNIT_TEST_PASSED;
	upCvasErr eErr;

	// Parameters used in method invocation
	upRS232Port RS232_port;
	twobytes tBytesToRead;
	byte pBuffer[ BUFFER_SIZE ];
	twobytes tBytesRead;
	byte bWaitMS;

	// Call each port and check return value
	for ( int i = 0; i <= 2; i++ )
	{
		// Set up passed parameters
		RS232_port = (upRS232Port) i;
		tBytesToRead = 10;
		bWaitMS = 0;

		// Check return codes for port 0, port 1, undefined
		eErr = ReadPort( RS232_port, tBytesToRead, pBuffer, &tBytesRead, bWaitMS );
		if ( i < 2 )
			iResult |= McUnitAssertIsEqual( OK, eErr, "Port enum valid" );
		else
			iResult |= McUnitAssertIsEqual( ERR_ENUM_OUT_OF_RANGE, eErr, "Port enum out of range" );
	}

	return iResult;
}

/**
 * @brief	This method tests the passed parameter(s) of the FlushReadBuffer() method.
 *			It tests flushing each port, and an invalid port.
 *
 * @return   result as an int
 */
MCUNIT_TEST( CRS232Test, FlushReadBuffer_Parameter )
{
	int iResult = UNIT_TEST_PASSED;
	upCvasErr eErr;

	// Parameters used in method invocation
	upRS232Port RS232_port;

	// Call each port and check return value
	// TODO should error code be ENUM_OUT_OF_RANGE for consistency?
	for ( int i = 0; i <= 2; i++ )
	{
		// Set up passed parameters
		RS232_port = (upRS232Port) i;

		// Check return codes for port 0, port 1, undefined
		eErr = FlushReadBuffer( RS232_port );
		if ( i < 2 )
			iResult |= McUnitAssertIsEqual( OK, eErr, "Port enum valid" );
		else
			iResult |= McUnitAssertIsEqual( ERR_BAD_PARAM, eErr, "Port enum out of range" );
	}

	return iResult;
}

//=========================================================================================================================
// Local functions
//=========================================================================================================================

/**
 * @brief	This method waits until all bytes are snet or times out
 *
 * @param	ePort 	The port to unload as upRS232Port
 *
 * @return   Status as upCvasErr
 */
upCvasErr CRS232Test::UnloadBuffer( upRS232Port ePort )
{
	twobytes tbBytes;
	uint32_t tbCount = (uint32_t) 160000;
	upCvasErr eErr;

	do
		{
		eErr = GetPendingSendCount( ePort, &tbBytes );
		tbCount--;
		}
	while ( OK == eErr && 0 != tbBytes && 0 != tbCount );

	if ( 0 == tbCount )
		return ERR_TIMEOUT;

	return OK;
}

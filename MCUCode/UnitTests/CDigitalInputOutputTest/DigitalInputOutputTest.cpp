/**
 * @file	DigitalInputOutputTest.cpp
 * @par		Package: CvasMcu
 * @par		Project: CVAS Generator
 * @par		Copyright (c) 2016, Uptake Medical, Inc.
 * @author	Kurt Dec 29, 2011 Created file
 * @brief	This file contains unit tests for CDigitalInputOutput
 */

// Include Files
#include "DigitalInputOutputTest.h"

// External Public Data

// File Scope Macros and Constants

// File Scope Type Definitions (Enums, Structs & Classes)

// File Scope Data
static CDigitalInputOutputTest f_CDigitalInputOutputTest; // Instantiate one so it registers itself

// File Scope Functions

// Public Functions

/**
 * @brief	This is the constructor for CDigitalInputOutputTest
 *
 */

CDigitalInputOutputTest::CDigitalInputOutputTest() :
	CMcUnit<CDigitalInputOutputTest> ( this ), CTestMain( (CUnitTestIntfc*) this )
{
}


/**
 * @brief	This method calls into CMcUnit to have the tests run.
 *
 * @return   Always 1
 */
int CDigitalInputOutputTest::CommonTestHandle()
{
	McUnitSuiteRun( &sMcUnitTestSuite );
	McUnitSuiteReport( &sMcUnitTestSuite, DEPTH_TEST_REPORT );
	return 1;
}

/**
 * @brief	This method sets up the lists of tests to run
 *
 */
void CDigitalInputOutputTest::UnitTestListSetup()
{

	int iList = 0;
	int iCase = 0;

	// Set up Case 1: Invocation Tests
	ADD_TEST_CASE( sMcUnitCase, iCase, sMcUnitTestList + iList, 1, "Invocation Tests", "Invocation Test failure")
	ADD_TEST_LIST( sMcUnitTestList, iList, &CDigitalInputOutputTest::AllPublicMethods_Invocation, "AllPublicMethods() invocation", "unexpected result"  )

	// Set up Case 2: Parameter Tests
	ADD_TEST_CASE( sMcUnitCase, iCase, sMcUnitTestList + iList, 2, "Parameter Tests", "Parameter Test failure")
	ADD_TEST_LIST( sMcUnitTestList, iList, &CDigitalInputOutputTest::GetOutput_Parameter, "GetOutput() parameter", "unexpected result" )
	ADD_TEST_LIST( sMcUnitTestList, iList, &CDigitalInputOutputTest::GetInput_Parameter, "GetInput() parameter", "unexpected result" )

	// Set up Case 3: Parameter Tests
	ADD_TEST_CASE( sMcUnitCase, iCase, sMcUnitTestList + iList, 2, "Interaction Tests", "Interaction Test failure")
	ADD_TEST_LIST( sMcUnitTestList, iList, &CDigitalInputOutputTest::DigitalOutputs_Interaction, "Digital Outputs interaction", "unexpected result" )
	ADD_TEST_LIST( sMcUnitTestList, iList, &CDigitalInputOutputTest::DigitalInputs_Interaction, "Digital Inputs interaction", "unexpected result" )

	MAKE_TEST_SUITE( sMcUnitTestSuite, sMcUnitCaseList, sMcUnitCase, iCase, "CDigitalInputOutputTest" )

}

//=============================================================================
// CASE 1: INVOCATION TESTS
//=============================================================================

/**
 * @brief	This method tests the invocation of all public methods with valid parameters.
 *
 * @return  result as an int: 0 (UNIT_TEST_PASSED) if passed, not 0 if failed
 */
MCUNIT_TEST( CDigitalInputOutputTest, AllPublicMethods_Invocation )
{
 	int iResult = UNIT_TEST_PASSED;
	bool_c bVal = 0;
	twobytes tVal = 0;
	upCvasLEDState eState;
 	int	iInvocations = 0;

	// Invoke and check return codes (if there is one)
	iResult |= McUnitAssertIsEqual( OK, InitializeHardware(), "InitializeHardware()" );
	iInvocations++;

	iResult |= McUnitAssertIsEqual( OK, ReadFromHardware(), "ReadFromHardware()" );
	iInvocations++;

	iResult |= McUnitAssertIsEqual( OK, WriteToHardware(), "WriteToHardware()" );
	iInvocations++;

	iResult |= McUnitAssertIsEqual( OK, SetOutput( (upDigitalOutputs)0, bVal ), "SetOutput()" );
	iInvocations++;

	iResult |= McUnitAssertIsEqual( OK, GetOutput( (upDigitalOutputs)0, &bVal ), "GetOutput()" );
	iInvocations++;

	iResult |= McUnitAssertIsEqual( OK, GetInput( (upDigitalInputs) 0, &bVal ), "GetInput()" );
	iInvocations++;

	iResult |= McUnitAssertIsEqual( OK, GetAllOutput( &tVal ), "GetAllOutput()" );
	iInvocations++;

	iResult |= McUnitAssertIsEqual( OK, GetAllInput( &bVal ), "GetAllInput()" );
	iInvocations++;

	iResult |= McUnitAssertIsEqual( OK, SetLEDState( (upCvasLEDStateEnum)0 ), "SetLEDState()" );
	iInvocations++;

	iResult |= McUnitAssertIsEqual( OK, GetLEDState( &eState ), "GetLEDState()" );
	iInvocations++;

	LogOneValue("Number of methods tested: %d", iInvocations);

	return iResult;
}


//=============================================================================
// CASE 2: PARAMETER TESTS
//=============================================================================

/**
 * @brief	This method tests the passed parameter(s) of the GetOutput() method
 *
 * @return  result as an int: 0 if passed, not 0 if failed
 */
MCUNIT_TEST( CDigitalInputOutputTest, GetOutput_Parameter )
{
	int iResult = UNIT_TEST_PASSED;
	upCvasErr eErr;
	bool_c bVal = 0;
	int i = 0;

	// Invoke and check return code for up to 16 output bits and OR the results
	for( i=0; i<16; i++ )
	{
		eErr = GetOutput( upDigitalOutputs(i), &bVal );

		// Check for correct return code
		if ( i < DIG_OUT_CNT )
		{
			iResult |= McUnitAssertIsEqual( OK, eErr, "GetOutput(), index in range" );
		}
		else
			iResult |= McUnitAssertIsEqual( ERR_ENUM_OUT_OF_RANGE, eErr, "GetOutput(), index out of range" );
	}

	return iResult;
}


/**
 * @brief	This method tests the passed parameter(s) of the GetInput() method
 *
 * @return  result as an int: 0 if passed, not 0 if failed
 */
MCUNIT_TEST( CDigitalInputOutputTest, GetInput_Parameter )
{
	int iResult = UNIT_TEST_PASSED;
	upCvasErr eErr;
	bool_c bVal = 0;
	int i = 0;

	// Invoke and check return code for up to 10 input bits and OR the results

	// Get all inputs as individual bits
	for( i=0; i<10; i++ )
	{
		eErr = GetInput( upDigitalInputs(i), &bVal );

		// Check for correct return code
		if ( i < DIG_IN_CNT )
		{
			iResult |= McUnitAssertIsEqual( OK, eErr, "GetInput(), index in range" );
		}
		else
			iResult |= McUnitAssertIsEqual( ERR_ENUM_OUT_OF_RANGE, eErr, "GetInput(), index out of range" );
	}

	return iResult;
}


//=============================================================================
// CASE 3: INTERACTION TESTS
//=============================================================================

/**
 * @brief	This method tests the interactions of digital outputs.
 * 			It sets outputs and verifies GetAllOutput(), GetOutput(), and the hardware register value.
 *
 * @return  result as an int: 0 if passed, not 0 if failed
 */
MCUNIT_TEST( CDigitalInputOutputTest, DigitalOutputs_Interaction )
{
	int result = UNIT_TEST_PASSED;
	twobytes tDigOut = 0, tBitsOut = 0;
	bool_c bState = 0;
	int i = 0;
	volatile uint8_t *pSystemLEDs = (volatile uint8_t *)0x110D;
	volatile uint8_t *pSystemControls = (volatile uint8_t *)0x110E;

/*	Bit pattern and enumeration copied here for clarity
	BIT 0: DIG_OUT_UNUSED_1 = 0,
	BIT 1: DIG_OUT_RF_ENABLE = 1,
	BIT 2: DIG_OUT_RF_AC_POWER,
	BIT 3: DIG_OUT_UNUSED_2,
	BIT 4: DIG_OUT_UNUSED_3,
		   DIG_OUT_CONTROL_CNT,
	BIT 5: DIG_OUT_LED0 = DIG_OUT_CONTROL_CNT,
	BIT 6: DIG_OUT_LED1,
	BIT 7: DIG_OUT_LED2,
	BIT 8: DIG_OUT_LED3,
	BIT 9: DIG_OUT_LED4,
	BIT A: DIG_OUT_LED5,
	BIT B: DIG_OUT_LED6,
	BIT C: DIG_OUT_LED7,
	DIG_OUT_CNT, // The following values are re-used so don't add to the total
 *
	BIT 5: DIG_OUT_GREEN_LED1 = DIG_OUT_LED0,
	BIT 6: DIG_OUT_GREEN_LED2 = DIG_OUT_LED1,
	BIT 7: DIG_OUT_AMBER_LED1 = DIG_OUT_LED2,
	BIT 8: DIG_OUT_AMBER_LED2 = DIG_OUT_LED3,
	BIT 9: DIG_OUT_BLUE_LED1 = DIG_OUT_LED4,
	BIT A: DIG_OUT_BLUE_LED2 = DIG_OUT_LED6, <- Note order swap
	BIT B: DIG_OUT_BLUE_LED3 = DIG_OUT_LED5, <- Note order swap
	BIT C: DIG_OUT_BLUE_LED4 = DIG_OUT_LED7,
 */

	// Set outputs to all zero
	SetOutput( DIG_OUT_UNUSED_1, (bool_c)0 );
	SetOutput( DIG_OUT_RF_ENABLE, (bool_c)0 );
	SetOutput( DIG_OUT_RF_AC_POWER, (bool_c)0 );
	SetOutput( DIG_OUT_UNUSED_2, (bool_c)0 );
	SetOutput( DIG_OUT_UNUSED_3, (bool_c)0 );
	SetLEDState( LED_OFF );
	WriteToHardware();	// Must be called before evaluating LED outputs

	// Get all outputs as two bytes and also as individual bits
	GetAllOutput( &tDigOut );
	for( i=0; i<DIG_OUT_CNT; i++ )
	{
		GetOutput( upDigitalOutputs(i), &bState );
		if (bState)
			tBitsOut += (1<<i);  // Build individual bits into 2 bytes
	}

	result |= McUnitAssertIsEqual( (twobytes)0, tDigOut, "One GetAllOutput() call, outputs cleared" );
	result |= McUnitAssertIsEqual( (twobytes)0, tBitsOut, "Individual GetOutput(), outputs cleared" );

	result |= McUnitAssertIsEqual( (uint8_t)0, *pSystemControls, "System controls address, outputs cleared");
	result |= McUnitAssertIsEqual( (uint8_t)0, *pSystemLEDs, "System LEDs address, outputs cleared");

	// Set outputs to a bit pattern
	SetOutput( DIG_OUT_UNUSED_1, (bool_c)1 );
	SetOutput( DIG_OUT_RF_ENABLE, (bool_c)0 );
	SetOutput( DIG_OUT_RF_AC_POWER, (bool_c)1 );
	SetOutput( DIG_OUT_UNUSED_2, (bool_c)0 );
	SetOutput( DIG_OUT_UNUSED_3, (bool_c)1 );
	SetLEDState( LED_WARN );
	WriteToHardware();	// Must be called before evaluating LED outputs

	// Get all outputs as two bytes and also as individual bits
	GetAllOutput( &tDigOut );
	for( i=0; i<DIG_OUT_CNT; i++ )
	{
		GetOutput( upDigitalOutputs(i), &bState );
		if (bState)
			tBitsOut += (1<<i);  // Build individual bits into 2 bytes
	}
											// FEDCBA9876543210
	result |= McUnitAssertIsEqual( (twobytes)0b0000101110010101, tDigOut, "One GetAllOutput() call, some outputs set" );
	result |= McUnitAssertIsEqual( (twobytes)0x0B95, tBitsOut, "Individual GetOutput() calls, some outputs set" );

										   // 76543210
	result |= McUnitAssertIsEqual( (uint8_t)0b00010101, *pSystemControls, "System controls address, some outputs set");
	result |= McUnitAssertIsEqual( (uint8_t)0b01011100, *pSystemLEDs, "System LEDs address, some outputs set");

	return result;
}

/**
 * @brief	This method tests the interactions of digital inputs.
 * 			It sets simulated inputs and verifies GetAllInput() and GetInput().
 *
 * @return  result as an int: 0 if passed, not 0 if failed
 */

MCUNIT_TEST( CDigitalInputOutputTest, DigitalInputs_Interaction )
{
	int result = UNIT_TEST_PASSED;
	byte bDigIn = 0, bBitsIn = 0;
	bool_c bState = 0;
	int i = 0;
	SHdwSimData sHdwSimData;

	// Set some inputs; Lower byte is the valid one
	sHdwSimData.m_digInput = 0x3C;
	f_simDataAccess.SetSimData( &sHdwSimData );
	ReadFromHardware();

	// Get all inputs as a byte; order is per the defines
	GetAllInput( &bDigIn );

	// Get all inputs as individual bits; order is per the enumeration
	for( i=0; i<DIG_IN_CNT; i++ )
	{
		GetInput( upDigitalInputs(i), &bState );
		if (bState)
			bBitsIn += (1<<i);  // Build individual bits into byte
	}

	result |= McUnitAssertIsEqual( (byte)0b00111100, bDigIn, "One GetAllInput() call" );
	result |= McUnitAssertIsEqual( (byte)0b01001110, bBitsIn, "Individual GetInput() calls" );


	// Set some inputs; Lower byte is the valid one
	sHdwSimData.m_digInput = 0xA5;	// Note:  Lower byte is the valid one.
	f_simDataAccess.SetSimData( &sHdwSimData );
	ReadFromHardware();

	// Get all inputs as a byte; order is per the defines
	GetAllInput( &bDigIn );

	// Get all inputs as individual bits; order is per the enumeration
	bBitsIn = 0;
	for( i=0; i<DIG_IN_CNT; i++ )
	{
		GetInput( upDigitalInputs(i), &bState );
		if (bState)
			bBitsIn += (1<<i);  // Build individual bits into byte
	}

	result |= McUnitAssertIsEqual( (byte)0b10100101, bDigIn, "One GetAllInput() call" );
	result |= McUnitAssertIsEqual( (byte)0b01010010, bBitsIn, "Individual GetInput() calls" );

	return result;
}

// Private Class Functions



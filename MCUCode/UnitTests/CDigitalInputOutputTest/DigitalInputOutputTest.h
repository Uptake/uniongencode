/**
 * @file	DigitalInputOutputTest.h
 * @par		Package: CvasMcu
 * @par		Project: CVAS Generator
 * @par		Copyright (c) 2016, Uptake Medical, Inc.
 * @author	Kurt Dec 29, 2011 Created file
 * @brief	This file contains unit tests for CDigitalInputOutput
 */

#ifndef DIGITALINPUTOUTPUTTEST_H_
#define DIGITALINPUTOUTPUTTEST_H_

// Include Files
#include <DigitalInputOutput.h>
#include "McUnit.h"
#include "UnitTestIntfc.h"
#include "TestMain.h"
//#include "MemMapIntfc.h"

// Public Macros and Constants

// Public Type Definitions (Enums, Structs & Classes)

/**
 * @class	CDigitalInputOutputTest
 * @brief	This class provides unit tests for CDigitalInputOutput
 *
 * Virtual Functions Overridden: @n@n
 *  UnitTestListSetup - Set up list of Suites, Cases and Tests to be execute @n
 * 	CommonTesthandle - Called to have tests run @n
 *
 * External Data Members: None @n@n
 *
 */


class CDigitalInputOutputTest: private CMcUnit<CDigitalInputOutputTest>, public CUnitTestIntfc, public CTestMain, private CDigitalInputOutput
{
public:
	CDigitalInputOutputTest();
	int CommonTestHandle();
	void UnitTestListSetup();
	// Case 1: Invocation Tests
	MCUNIT_TEST_DEC( AllPublicMethods_Invocation );
	// Case 2: Parameter Tests
	MCUNIT_TEST_DEC( GetOutput_Parameter );
	MCUNIT_TEST_DEC( GetInput_Parameter );
	// Case 3: Interaction Tests
	MCUNIT_TEST_DEC( DigitalOutputs_Interaction );
	MCUNIT_TEST_DEC( DigitalInputs_Interaction );
	SMcUnitTest sMcUnitTestList[5];
	SMcUnitCase sMcUnitCase[3];
	SMcUnitCase * sMcUnitCaseList[4];
	SMcUnitSuite sMcUnitTestSuite;
};

// Inline Functions (follows class definition)


#endif /* DIGITALINPUTOUTPUTTEST_H_ */

/**
 * @file	ExtendedRunTest.cpp
 * @par		Package: CvasMcu
 * @par		Project: CVAS Generator
 * @par		Copyright (c) 2016, Uptake Medical, Inc.
 * @author	Glen April 26, 2012 Created file
 * @brief	This file contains integration tests for treatment runs
 */

// Include Files
#include "ExtendedRunTest.h"
#include <avr/interrupt.h>
#include "ObjectSingletons.h"

// External Public Data

// File Scope Macros and Constants
#define DATA_FILE_LOC "DataFiles/"

// File Scope Type Definitions (Enums, Structs & Classes)

// File Scope Data
static CExtendedRunTest f_ExtendedRunTest; // Instantiate one so it registers itself

// File Scope Functions

// Public Functions

/**
 * @brief	This is the constructor for CExtendedRunTest
 *
 */

CExtendedRunTest::CExtendedRunTest() :
	CMcUnit<CExtendedRunTest> ( this ), CTestMain( (CUnitTestIntfc*) this )
{
	m_tLoopTick = 0;
	m_tLastLoopTick = 0;
	m_tLoopStartTick = 0;
}

/**
 * @brief	This method calls into CMcUnit to have the tests run.
 *
 * @return   Always 1
 */
int CExtendedRunTest::CommonTestHandle()
{
	sei();
	McUnitSuiteRun( &sMcUnitTestSuite );
	McUnitSuiteReport( &sMcUnitTestSuite, DEPTH_TEST_REPORT );
	return 1;
}

/**
 * @brief	This method sets up the lists of tests to run
 *
 */
void CExtendedRunTest::UnitTestListSetup()
{

	int iList = 0;
	int iCase = 0;

	// Set up Case 1
	ADD_TEST_CASE( sMcUnitCase, iCase, sMcUnitTestList + iList, 1, "Integration Tests", "Test failure")
	ADD_TEST_LIST( sMcUnitTestList, iList, &CExtendedRunTest::FullUseRun, "Run Fully through 15 treatments", "unexpected result" )
	
	MAKE_TEST_SUITE( sMcUnitTestSuite, sMcUnitCaseList, sMcUnitCase, iCase, "CExtendedRunTest" )

}

//=============================================================================
// CASE 1: INVOCATION TESTS
//=============================================================================

/**
 * @brief	This method tests using self-test, priming and two treatments.
 *
 * @return   result as an int
 */

MCUNIT_TEST( CExtendedRunTest, FullUseRun )
{
	upOperState eNewState;
	upOperState eLastState = OPER_STATE_BOOT;
	byte bTreatCount = 0;
	int iResult = UNIT_TEST_PASSED;
	CRndRobinHelper rndRobinHelper;

	m_tLoopStartTick = 0;
	m_tLastLoopTick = 0;
	m_tLoopTick = 0;

	iResult |= LoadSelfTestThroughPrime();
	iResult |= LoadBalanceOfTwoTreatments();

	Initialize();

	// Start with even timer tick. This makes the simulation output logs more repeatable.
	do
		{
		GetTick( &m_tLoopStartTick );
		}
	while ( 0 != m_tLoopStartTick % RND_ROBIN_TICK_CNT );

	// Set to pass 1
	ResetPassFlag();

	// Loop forever
	f_simDataAccess.SetOpcondDecim( 10 );
	m_setupHelpers.ResetStackPointer();
	while ( f_simDataAccess.GetSampleCount() > GetPassCount() )
		{
		iResult |= m_setupHelpers.CheckStack( false );

		iResult = RunOneLoop();

		iResult |= ForceLoopTime();

		eNewState = GetOperState();

		if ( OPER_STATE_DELIVERING == eLastState && OPER_STATE_PENDING == eNewState )
			{
			bTreatCount++;
			ClearLoadOneTreatment();
			m_setupHelpers.ReportStack();
			m_setupHelpers.SbcSendAllLogs();
			}
			
		eLastState = eNewState;

		if ( bTreatCount > 15 )
			{
			f_simDataAccess.PrintToSimulatorLog( "Halting test" );
			return iResult;
			}
		} // While

	m_setupHelpers.ReportStack();
	m_setupHelpers.SbcSendAllLogs();
	return iResult;
}

/**
 * @brief	This method performs the "guts" of the round-robin loop by doing the ReadFromHardware(),
 * 			Execute(),
 *
 * @return   Status as int
 */
int CExtendedRunTest::RunOneLoop()
{
	int iResult = UNIT_TEST_PASSED;
	//	time tSecs;
	//	byte bPassIndex;

	// Use second diagnostic LED to track passage of timer seconds and thrid for loop cycles
	// These should then blink at 1/2 Hz and 1/10th Hz
	//GetTime( &tSecs );
	//g_pDiagnosticLed->SetLED( DIAG_LED3, ( tSecs & 1 ) ? true : false );
	//g_pDiagnosticLed->SetLED( DIAG_LED2, ( bPassIndex & 1 ) ? true : false );

	// Get new values from the hardware
	iResult |= McUnitAssertIsEqual( OK, g_timeBase.ReadFromHardware(), "g_timeBase.ReadFromHardware", false );
	// Do first to bump the simulation record to the latest sample
	iResult |= McUnitAssertIsEqual( OK, g_pTemperature->ReadFromHardware(), "g_pTemperature->ReadFromHardware", false );
	iResult |= McUnitAssertIsEqual( OK, g_pRFGen->ReadFromHardware(), "g_pRFGen->ReadFromHardware", false );
	iResult |= McUnitAssertIsEqual( OK, g_pWaterCntl->ReadFromHardware(), "g_pWaterCntl->ReadFromHardware", false );
	iResult |= McUnitAssertIsEqual( OK, g_pDigitalInputOutput->ReadFromHardware(), "g_pDigitalInputOutput->ReadFromHardware", false );
	iResult |= McUnitAssertIsEqual( OK, g_pOneWireMemory->ReadFromHardware(), "g_pOneWireMemory->ReadFromHardware", false );

	// Execute the treatment state machine including priming
	iResult |= McUnitAssertIsEqual( OK, CSupervisor::Execute(), "Execute", false );

	// Write new values, if any, to the hardware
	iResult |= McUnitAssertIsEqual( OK, g_pWaterCntl->WriteToHardware(), "g_pWaterCntl->WriteToHardware", false );
	iResult |= McUnitAssertIsEqual( OK, g_pRFGen->WriteToHardware(), "g_pRFGen->WriteToHardware", false );
	iResult |= McUnitAssertIsEqual( OK, g_pDigitalInputOutput->WriteToHardware(), "g_pDigitalInputOutput->WriteToHardware", false );

	// Move log message if possible
	// Send to SBC
	iResult |= McUnitAssertIsEqual( OK,g_pSbcIntfc->WriteToHardware(), "g_pSbcIntfc->WriteToHardware", false );

	// Get incoming information from the SBC
	iResult |= McUnitAssertIsEqual( OK, g_pSbcIntfc->ReadFromHardware(), "g_pSbcIntfc->ReadFromHardware", false );

	// Update EEPROM
	//	iResult |= McUnitAssertIsEqual( OK, f_eepromAccess.WriteToHardware(),"f_eepromAccess.WriteToHardware",false );

	// Update info on 1-wire bus
	iResult |= McUnitAssertIsEqual( OK, g_pOneWireMemory->Execute(), "g_pOneWireMemory->Execute", false );

	// Control critical alarm output
	iResult |= McUnitAssertIsEqual( OK, g_pSoundCntl->Execute(), "g_pSoundCntl->Execute", false );
	// Kick the watchdog
	iResult |= McUnitAssertIsEqual( OK, g_watchdog.Execute(), "f_watchdog.Execute", false );

	// Track and log loop timing if not simulating
	//	if ( 0 == g_bIsSimulated )
	//	PerformLoopTiming( tbTickDiff, bPassIndex );

	return iResult;
}

/**
 * @brief	This method
 *
 * @return   Status as int
 */
int CExtendedRunTest::ForceLoopTime()
{
	int iResult = UNIT_TEST_PASSED;
	twobytes tbTickDiff;
	byte bPassIndex;

	// Wait for next loop time to occur
	m_tLastLoopTick = m_tLoopTick;

	// Rotate pass flag mask
	IncrementPassFlag( &bPassIndex );

	// Force time elapsed of RND_ROBIN_TICK_CNT - 2 and then wait the last 2 tick
	// This gives time to run timer
	m_setupHelpers.SetTick( m_tLoopStartTick + RND_ROBIN_TICK_CNT - 2 );

	// Wait for the next execution time
	GetTick( &m_tLoopTick );
	GetTickDiff( m_tLoopTick, m_tLoopStartTick, &tbTickDiff );

	// Report an error if the execution took longer than the target loop time
	if ( tbTickDiff > RND_ROBIN_TICK_CNT )
		{
		LOG_ERROR_RETURN( ERR_RND_ROBIN_OVERRUN );
		CSupervisor::SetOverrunFlag();
		}

	while ( tbTickDiff < RND_ROBIN_TICK_CNT )
		{
		GetTick( &m_tLoopTick );
		if ( (byte) m_tLoopTick != (byte) m_tLastLoopTick )
			{
			GetTickDiff( m_tLoopTick, m_tLoopStartTick, &tbTickDiff );
			m_tLastLoopTick = m_tLoopTick;
			}
		}

	// Track start tick of loop
	m_tLoopStartTick = m_tLoopTick;
	return iResult;
}

/**
 * @brief	This method initializes all of the task objects by calling InitializeHardware on object as needed.
 *
 *
 * @return   Status as upCvasErr
 */
int CExtendedRunTest::Initialize()
{
	int iResult = UNIT_TEST_PASSED;

	// Initialize the hardware
	iResult |= McUnitAssertIsEqual( OK, g_pDiagnosticLed->InitializeHardware(), "g_pDiagnosticLed InitializeHardware" );
	iResult |= McUnitAssertIsEqual( OK, g_timeBase.InitializeHardware(), "g_timeBase.InitializeHardware" );
	//	iResult |= McUnitAssertIsEqual( OK, f_watchdog.InitializeHardware(), "f_watchdog.InitializeHardware" );
	iResult |= McUnitAssertIsEqual( OK, g_pDigitalInputOutput->InitializeHardware(), "g_pDigitalInputOutput->InitializeHardware" );
	iResult |= McUnitAssertIsEqual( OK, g_pRFGen->InitializeHardware(), "g_pRFGen->InitializeHardware" );
	iResult |= McUnitAssertIsEqual( OK, g_pOneWireMemory->InitializeHardware(), "g_pOneWireMemory->InitializeHardware" );
	iResult |= McUnitAssertIsEqual( OK, g_pSbcIntfc->InitializeHardware(), "g_pSbcIntfc->InitializeHardware" );
	iResult |= McUnitAssertIsEqual( OK, g_pTemperature->InitializeHardware(), "g_pTemperature->InitializeHardware" );
	iResult |= McUnitAssertIsEqual( OK, g_pWaterCntl->InitializeHardware(), "g_pWaterCntl->InitializeHardware" );
	iResult |= McUnitAssertIsEqual( OK, g_pSoundCntl->InitializeHardware(), "g_pSoundCntl->InitializeHardware" );

	return iResult;
}

/**
 * @brief	This method
 *
 * @return   Status as int
 */
int CExtendedRunTest::LoadSelfTestThroughPrime()
{
	int iResult = UNIT_TEST_PASSED;
	f_simDataAccess.LoadDataFile( DATA_FILE_LOC"SelfTest.csv", true );
	//f_simDataAccess.LoadDataFile( DATA_FILE_LOC"Idle.csv", false );
	f_simDataAccess.LoadDataFile( DATA_FILE_LOC"FreshCode2OneWire.csv", false );
	f_simDataAccess.LoadDataFile( DATA_FILE_LOC"Idle.csv", false );
	f_simDataAccess.LoadDataFile( DATA_FILE_LOC"Connected.csv", false );
	f_simDataAccess.LoadDataFile( DATA_FILE_LOC"Priming.csv", false );
	return iResult;
}

/**
 * @brief	This method
 *
 * @return   Status as int
 */
int CExtendedRunTest::LoadBalanceOfTwoTreatments()
{
	int iResult = UNIT_TEST_PASSED;
	f_simDataAccess.LoadDataFile( DATA_FILE_LOC"PendingReady1.csv", false );
	f_simDataAccess.LoadDataFile( DATA_FILE_LOC"Ready1.csv", false );
	f_simDataAccess.LoadDataFile( DATA_FILE_LOC"Treatment1.csv", false );
	f_simDataAccess.LoadDataFile( DATA_FILE_LOC"PendingReady2.csv", false );
	//f_simDataAccess.LoadDataFile( DATA_FILE_LOC"Ready2.csv", false );
	//f_simDataAccess.LoadDataFile( DATA_FILE_LOC"Treatment2.csv", false );
	//f_simDataAccess.LoadDataFile( DATA_FILE_LOC"PendingReady3.csv", false );
	//f_simDataAccess.LoadDataFile( DATA_FILE_LOC"Ready3.csv", false );
	return iResult;
}

/**
 * @brief	This method
 *
 * @return   Status as int
 */
int CExtendedRunTest::ClearLoadOneTreatment()
{
	int iResult = UNIT_TEST_PASSED;
	f_simDataAccess.LoadDataFile( DATA_FILE_LOC"PendingReady2.csv", true );
	f_simDataAccess.LoadDataFile( DATA_FILE_LOC"Ready2.csv", false );
	f_simDataAccess.LoadDataFile( DATA_FILE_LOC"Treatment2.csv", false );
	f_simDataAccess.LoadDataFile( DATA_FILE_LOC"PendingReady3.csv", false );
	//f_simDataAccess.LoadDataFile( DATA_FILE_LOC"Ready2.csv", false );
	f_simDataAccess.GoToNextRecord();
	f_simDataAccess.GoToNextRecord();
	f_simDataAccess.GoToNextRecord();
	return iResult;
}

/**
 * @brief	This method
 *
 * @return   Status as int
 */
int CExtendedRunTest::ClearPartialTreatment()
{
	int iResult = UNIT_TEST_PASSED;
	f_simDataAccess.LoadDataFile( DATA_FILE_LOC"PendingReady1.csv", true );
	f_simDataAccess.LoadDataFile( DATA_FILE_LOC"Ready1.csv", false );
	f_simDataAccess.LoadDataFile( DATA_FILE_LOC"Treatment1.csv", false );
	f_simDataAccess.LoadDataFile( DATA_FILE_LOC"PendingReady2.csv", false );
	f_simDataAccess.LoadDataFile( DATA_FILE_LOC"Ready2.csv", false );
	f_simDataAccess.GoToNextRecord();
	f_simDataAccess.GoToNextRecord();
	f_simDataAccess.GoToNextRecord();
	return iResult;
}

//=========================================================================================================================
// Local functions
//=========================================================================================================================


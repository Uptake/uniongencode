/**
 * @file	ExtendedRunTest.h
 * @par		Package: CvasMcu
 * @par		Project: CVAS Generator
 * @par		Copyright (c) 2016, Uptake Medical, Inc.
 * @author	Glen Dec 29, 2011 Created file
 * @brief	This file contains integration tests for extended runs fo treatments.
 */

#ifndef EXTENDEDRUNTEST_H_
#define EXTENDEDRUNTEST_H_

// Include Files
#include <RS232.h>
#include "McUnit.h"
#include "UnitTestIntfc.h"
#include "TestMain.h"
#include "SetupHelpers.h"
#include "Supervisor.h"

// Public Macros and Constants

// Public Type Definitions (Enums, Structs & Classes)

/**
 * @class	CExtendedRunTest
 * @brief	This class provides unit tests for long runs of successful treatments.
 *
 * 			To achieve these in a shorter time the time base is "hijacked" and is forced to
 * 			the next loop time once the software in the round robin loop has completed. This
 * 			achieves a 10x increase in through-put.
 *
 * 			During the runs the stack is checked for growth.
 *
 * Virtual Functions Overridden: @n@n
 *  UnitTestListSetup - Set up list of Suites, Cases and Tests to be execute @n
 * 	CommonTesthandle - Called to have tests run @n
 *
 * External Data Members: None @n@n
 *
 */
class CExtendedRunTest: private CMcUnit<CExtendedRunTest> , public CUnitTestIntfc, public CTestMain, private CSupervisor
{

public:
	CExtendedRunTest();
	int CommonTestHandle();
	void UnitTestListSetup();

	// Tests
	MCUNIT_TEST_DEC( FullUseRun );
	MCUNIT_TEST_DEC( RunUntilDDExpired );
	MCUNIT_TEST_DEC( RunUntilDDTimeExpired );

	SMcUnitTest sMcUnitTestList[ 5 ]; // Increment index for each test added, regardless of the case in which it falls
	SMcUnitCase sMcUnitCase[ 3 ];
	SMcUnitCase * sMcUnitCaseList[ 4 ];
	SMcUnitSuite sMcUnitTestSuite;

private:
	CSetupHelpers m_setupHelpers;
	tick m_tLoopTick;
	tick m_tLastLoopTick;
	tick m_tLoopStartTick;
	int RunOneLoop();
	int ForceLoopTime();
	int Initialize();
	int LoadSelfTestThroughPrime();
	int LoadBalanceOfTwoTreatments();
	int ClearLoadOneTreatment();
	int ClearPartialTreatment();
};

// Inline Functions (follows class definition)


#endif /* EXTENDEDRUNTEST_H_ */

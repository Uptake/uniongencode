/**
 * @file	CalibrateTwoLinearPiecesTest.cpp
 * @par		Package: CvasMcu
 * @par		Project: CVAS Generator
 * @par		Copyright (c) 2016, Uptake Medical, Inc.
 * @author	Glen May 7, 2012 Created file
 * @brief	This file contains the code for CCalibrateTwoLinearPiecesTest that tests CCalibrateTwoLinearPieces
 */

// Include Files
#include "CalibrateTwoLinearPiecesTest.h"

// External Public Data

// File Scope Macros and Constants
static CCalibrateTwoLinearPiecesTest f_CalibrateTwoLinearPiecesTest; // Instantiate one so it registers itself

// File Scope Type Definitions (Enums, Structs & Classes)

// File Scope Data

// File Scope Functions

// Public Functions

/**
 * @brief	This is the constructor for CalibrateTwoLinearPiecesTest
 *
 */
#include "CalibrateTwoLinearPiecesTest.h"

CCalibrateTwoLinearPiecesTest::CCalibrateTwoLinearPiecesTest() :
	CMcUnit<CCalibrateTwoLinearPiecesTest> ( this ), CTestMain( (CUnitTestIntfc*) this )
{
}

/**
 * @brief	This method calls into CMcUnit to have the tests run.
 *
 * @return   Always 1
 */
int CCalibrateTwoLinearPiecesTest::CommonTestHandle()
{
	McUnitSuiteRun( &sMcUnitTestSuite );
	McUnitSuiteReport( &sMcUnitTestSuite, DEPTH_TEST_REPORT );
	return 1;
}

/**
 * @brief	This method sets up the lists of tests to run
 *
 */
void CCalibrateTwoLinearPiecesTest::UnitTestListSetup()
{

	int iList = 0;
	int iCase = 0;

	// Set up Case 1: Invocation Tests
	ADD_TEST_CASE( sMcUnitCase, iCase, sMcUnitTestList + iList, 1, "Invocation Tests", "Invocation Test failure")
	ADD_TEST_LIST( sMcUnitTestList, iList, &CCalibrateTwoLinearPiecesTest::AllPublicMethods_Invocation, "All Public Methods invocation", "unexpected result" )

	// Set up Case 2: Parameter Tests
	ADD_TEST_CASE( sMcUnitCase, iCase, sMcUnitTestList + iList, 1, "Parameter Tests", "Parameter Test failure")
	ADD_TEST_LIST( sMcUnitTestList, iList, &CCalibrateTwoLinearPiecesTest::AllPublicMethodsDefault_Parameter, "All Public Methods Default parameter", "unexpected result" )

	// Set up Case 3: Interaction Tests
	ADD_TEST_CASE( sMcUnitCase, iCase, sMcUnitTestList + iList, 0, "Interaction Tests (No tests defined)", "")
	//Delete the preceding line and uncomment the two lines below to begin adding Interaction Tests
	//ADD_TEST_CASE( sMcUnitCase, iCase, sMcUnitTestList + iList, 1, "Interaction Tests", "Interaction Test failure")
	//ADD_TEST_LIST( sMcUnitTestList, iList, &CCalibrateTwoLinearPiecesTest::<Name>_Interaction, "<Name> Interaction", "unexpected result" )

	MAKE_TEST_SUITE( sMcUnitTestSuite, sMcUnitCaseList, sMcUnitCase, iCase, "CCalibrateTwoLinearPiecesTest" )

}

//=============================================================================
// CASE 1: INVOCATION TESTS
//=============================================================================

/**
 * @brief	This method tests the invocation of all public methods with valid parameters.
 *
 * @return  result as an int: 0 (UNIT_TEST_PASSED) if passed, not 0 if failed
 */
MCUNIT_TEST( CCalibrateTwoLinearPiecesTest, AllPublicMethods_Invocation )
{
	int iResult = UNIT_TEST_PASSED;
	int iInvocations = 0;
	int iRes;
	STwoLinearPiecesLongIntFactors intFactors;

	intFactors.ilBoffsetLower = 0;
	intFactors.ilMslopeLower = 0;
	intFactors.ilBreakPoint = 0;
	intFactors.ilBoffsetUpper = 0;
	intFactors.ilMslopeUpper = 0;

	iResult |= McUnitAssertIsEqual( OK, SetFactors( 0, intFactors ), "SetFactors" );
	iInvocations++;

	iResult |= McUnitAssertIsEqual( OK, GetFactors( 0, &intFactors ), "GetFactors" );
	iInvocations++;

	iResult |= McUnitAssertIsEqual( OK, SetFactors( intFactors ), "SetFactors" );
	iInvocations++;

	iResult |= McUnitAssertIsEqual( OK, GetFactors( &intFactors ), "GetFactors" );
	iInvocations++;

	iResult |= McUnitAssertIsEqual( 0, ApplyCal( 0, 0, &iRes ), "ApplyCal" );
	iInvocations++;

	iResult |= McUnitAssertIsEqual( 0, ApplyCal( 0, &iRes ), "ApplyCal" );
	iInvocations++;

	LogOneValue( "Number of methods tested: %d", iInvocations );

	return iResult;
}

//=============================================================================
// CASE 2: PARAMETER TESTS
//=============================================================================

/**
 * @brief	This method tests the default passed parameter(s) for all public methods.
 *
 * @return  result as an int: 0 (UNIT_TEST_PASSED) if passed, not 0 if failed
 */

MCUNIT_TEST( CCalibrateTwoLinearPiecesTest, AllPublicMethodsDefault_Parameter )
{
	int iResult = UNIT_TEST_PASSED;
	int iInvocations = 0;
	STwoLinearPiecesLongIntFactors intFactors;

	intFactors.ilBoffsetLower = 0;
	intFactors.ilMslopeLower = 0;
	intFactors.ilBreakPoint = 0;
	intFactors.ilBoffsetUpper = 0;
	intFactors.ilMslopeUpper = 0;

	// Use bad setter indexes
	iResult |= McUnitAssertIsEqual( ERR_BAD_PARAM, SetFactors( ELEMENT_COUNT, intFactors ), "SetFactors - too many" );
	iInvocations++;
	iResult |= McUnitAssertIsEqual( ERR_BAD_PARAM, SetFactors( -1, intFactors ), "SetFactors - negative" );
	iInvocations++;

	// Use bad getter indexes and pointer
	iResult |= McUnitAssertIsEqual( ERR_BAD_PARAM, GetFactors( ELEMENT_COUNT, &intFactors ), "GetFactors - too many" );
	iInvocations++;
	iResult |= McUnitAssertIsEqual( ERR_BAD_PARAM, GetFactors( -1, &intFactors ), "GetFactors - negative" );
	iInvocations++;
	iResult |= McUnitAssertIsEqual( ERR_BAD_PARAM, GetFactors( 0, 0 ), "GetFactors - zero pointer" );
	iInvocations++;
	iResult |= McUnitAssertIsEqual( ERR_BAD_PARAM, GetFactors( 0 ), "GetFactors - zero pointer" );
	iInvocations++;

	// Do set/get twice with different values
	for ( int j = 7; j < 17; j += 10 )
		{
		// Load different call factors into all available entries
		for ( int i = 0; i < ELEMENT_COUNT; i++ )
			{
			intFactors.ilBoffsetLower = CNVT_REAL_TO_LINEAR_B( i * 11 + j );
			intFactors.ilMslopeLower = CNVT_REAL_TO_LINEAR_M( 100 - i - j );
			intFactors.ilBreakPoint = i * 10;
			intFactors.ilBoffsetUpper = CNVT_REAL_TO_LINEAR_B( i * 12 + j );
			intFactors.ilMslopeUpper = CNVT_REAL_TO_LINEAR_M( 101 - i - j );

			iResult |= McUnitAssertIsEqual( OK, SetFactors( i, intFactors ), "SetFactors" );
			iInvocations++;

			}

		// Check zeroth cal factors
		iResult |= McUnitAssertIsEqual( OK, GetFactors( &intFactors ), "GetFactors" );
		iInvocations++;

		iResult |= McUnitAssertIsEqual( CNVT_REAL_TO_LINEAR_B( 0 * 11 + j ), intFactors.ilBoffsetLower, "LINEAR_B" );
		iResult |= McUnitAssertIsEqual( CNVT_REAL_TO_LINEAR_M( 100 - 0 - j ), intFactors.ilMslopeLower, "LINEAR_M" );
		iResult |= McUnitAssertIsEqual( 0 * 10, (int) intFactors.ilBreakPoint, "BreakPoint" );
		iResult |= McUnitAssertIsEqual( CNVT_REAL_TO_LINEAR_B( 0 * 12 + j ), intFactors.ilBoffsetUpper, "LINEAR_B" );
		iResult |= McUnitAssertIsEqual( CNVT_REAL_TO_LINEAR_M( 101 - 0 - j ), intFactors.ilMslopeUpper, "LINEAR_M" );

		// Check all entries
		for ( int i = 0; i < ELEMENT_COUNT; i++ )
			{
			intFactors.ilBoffsetLower = -1;
			intFactors.ilMslopeLower = -1;
			intFactors.ilBreakPoint = -1;
			intFactors.ilBoffsetUpper = -1;
			intFactors.ilMslopeUpper = -1;

			iResult |= McUnitAssertIsEqual( OK, GetFactors( i, &intFactors ), "GetFactors" );
			iInvocations++;

			iResult |= McUnitAssertIsEqual( CNVT_REAL_TO_LINEAR_B( i * 11 + j ), intFactors.ilBoffsetLower, "LINEAR_B" );
			iResult |= McUnitAssertIsEqual( CNVT_REAL_TO_LINEAR_M( 100 - i - j ), intFactors.ilMslopeLower, "LINEAR_M" );
			iResult |= McUnitAssertIsEqual( i * 10, (int) intFactors.ilBreakPoint, "BreakPoint" );
			iResult |= McUnitAssertIsEqual( CNVT_REAL_TO_LINEAR_B( i * 12 + j ), intFactors.ilBoffsetUpper, "LINEAR_B" );
			iResult |= McUnitAssertIsEqual( CNVT_REAL_TO_LINEAR_M( 101 - i - j ), intFactors.ilMslopeUpper, "LINEAR_M" );
			}
		}

	// Set new zeroth cal factors
	intFactors.ilBoffsetLower = CNVT_REAL_TO_LINEAR_B( 7 );
	intFactors.ilMslopeLower = CNVT_REAL_TO_LINEAR_M( 105 );
	intFactors.ilBreakPoint = 109;
	intFactors.ilBoffsetUpper = CNVT_REAL_TO_LINEAR_B( 8 );
	intFactors.ilMslopeUpper = CNVT_REAL_TO_LINEAR_M( 106 );

	iResult |= McUnitAssertIsEqual( OK, SetFactors( intFactors ), "SetFactors" );
	iInvocations++;

	iResult |= McUnitAssertIsEqual( OK, GetFactors( &intFactors ), "GetFactors" );
	iInvocations++;

	iResult |= McUnitAssertIsEqual( CNVT_REAL_TO_LINEAR_B( 7 ), intFactors.ilBoffsetLower, "LINEAR_B" );
	iResult |= McUnitAssertIsEqual( CNVT_REAL_TO_LINEAR_M( 105 ), intFactors.ilMslopeLower, "LINEAR_M" );
	iResult |= McUnitAssertIsEqual( 109, (int) intFactors.ilBreakPoint, "BreakPoint" );
	iResult |= McUnitAssertIsEqual( CNVT_REAL_TO_LINEAR_B( 8 ), intFactors.ilBoffsetUpper, "LINEAR_B" );
	iResult |= McUnitAssertIsEqual( CNVT_REAL_TO_LINEAR_M( 106 ), intFactors.ilMslopeUpper, "LINEAR_M" );

	// Check calculation
	for ( int j = 1; j < 10; j++ )
		{
		float fMup = -2.0 + ( 0.357 * (float) j );
		float fBup = -23.0 + ( 40.312 * (float) j );
		float fMlow = -2.0 + ( 0.357 * (float) j );
		float fBlow = -23.0 + ( 40.312 * (float) j );
		int iBreak = j * 27;
		intFactors.ilBoffsetUpper = CNVT_REAL_TO_LINEAR_B( fBup );
		intFactors.ilMslopeUpper = CNVT_REAL_TO_LINEAR_M( fMup );
		intFactors.ilBoffsetLower = CNVT_REAL_TO_LINEAR_B( fBlow );
		intFactors.ilMslopeLower = CNVT_REAL_TO_LINEAR_M( fMlow );
		intFactors.ilBreakPoint = iBreak;

		for ( int i = 0; i < ELEMENT_COUNT; i++ )
			{
			int iUncal = -31 + i * 31;
			int iRes;
			int iRes2;

			iResult |= McUnitAssertIsEqual( OK, SetFactors( i, intFactors ), "SetFactors", false );
			iInvocations++;

			iResult |= McUnitAssertIsEqual( OK, ApplyCal( i, iUncal, &iRes ), "ApplyCal", false );
			iInvocations++;

			float fTemp;

			if ( iUncal < iBreak )
				fTemp = ( fMlow * (float) iUncal + fBlow );
			else
				fTemp = ( fMup * (float) iUncal + fBup );
			iRes2 = (int) fTemp;
			int iAssert = McUnitAssertIsEqual( iRes2, iRes, "Applied cal" );

			if ( UNIT_TEST_PASSED != iAssert )
				{
				iResult |= iAssert;
				LogOneValue( "iUncal: %d", iUncal );
				LogOneValue( "i: %d", i );
				LogOneValue( "j: %d", j );
				}

			}

		}
	LogOneValue( "Number of tests: %d", iInvocations );

	return iResult;
}

//=============================================================================
// CASE 3: INTERACTION TESTS
//=============================================================================

/**
 * @brief	This method tests the interaction of TBD.
 *
 * @return  result as an int: 0 (UNIT_TEST_PASSED) if passed, not 0 if failed
 */

//MCUNIT_TEST( CCalibrateTwoLinearPiecesTest, <Name>_Interaction )
//{
//}


// Private Class Functions



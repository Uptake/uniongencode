/**
 * @file	OneWireMemoryTest.cpp
 * @par		Package: CvasMcu
 * @par		Project: CVAS Generator
 * @par		Copyright (c) 2016, Uptake Medical, Inc.
 * @author	Kurt Jan 12, 2012 Created file
 * @brief	This file contains unit tests for COneWireMemory
 */

// Include Files
#include "OneWireMemoryTest.h"
#include "SimDataAccess.h"
#include "TimeBase.h"
#include "ObjectSingletons.h"

// External Public Data

// File Scope Macros and Constants

// File Scope Type Definitions (Enums, Structs & Classes)

// File Scope Data
static COneWireMemoryTest f_OneWireMemoryTest; // Instantiate one so it registers itself

// File Scope Functions

// Public Functions

/**
 * @brief	This is the constructor for COneWireMemoryTest
 *
 */

COneWireMemoryTest::COneWireMemoryTest() :
	CMcUnit<COneWireMemoryTest> ( this ), CTestMain( (CUnitTestIntfc*) this )
{
}


/**
 * @brief	This method calls into CMcUnit to have the tests run.
 *
 * @return   Always 1
 */
int COneWireMemoryTest::CommonTestHandle()
{
	McUnitSuiteRun( &sMcUnitTestSuite );
	McUnitSuiteReport( &sMcUnitTestSuite, DEPTH_TEST_REPORT );
	return 1;
}

/**
 * @brief	This method sets up the lists of tests to run
 *
 */
void COneWireMemoryTest::UnitTestListSetup()
{

	int iList = 0;
	int iCase = 0;

	// Set up Case 1: Invocation Tests
	ADD_TEST_CASE( sMcUnitCase, iCase, sMcUnitTestList + iList, 1, "Invocation Tests", "Invocation Test failure")
	ADD_TEST_LIST( sMcUnitTestList, iList, &COneWireMemoryTest::AllPublicMethods_Invocation, "All Public Methods invocation", "unexpected result" )

	// Set up Case 2: Parameter Tests
	ADD_TEST_CASE( sMcUnitCase, iCase, sMcUnitTestList + iList, 1, "Parameter Tests", "Parameter Test failure")
	ADD_TEST_LIST( sMcUnitTestList, iList, &COneWireMemoryTest::AllPublicMethodsDefault_Parameter, "All Public Methods Default parameter", "unexpected result" )

	// Set up Case 3: Interaction Tests
	ADD_TEST_CASE( sMcUnitCase, iCase, sMcUnitTestList + iList, 1, "Interaction Tests (No tests defined)", "")
	//Delete the preceding line and uncomment the two lines below to begin adding Interaction Tests
	//ADD_TEST_CASE( sMcUnitCase, iCase, sMcUnitTestList + iList, 1, "Interaction Tests", "Interaction Test failure")
	ADD_TEST_LIST( sMcUnitTestList, iList, &COneWireMemoryTest::OwConnect_Interaction, "OwConnect Interaction", "unexpected result" )

	MAKE_TEST_SUITE( sMcUnitTestSuite, sMcUnitCaseList, sMcUnitCase, iCase, "COneWireMemoryTest" )

}

//=============================================================================
// CASE 1: INVOCATION TESTS
//=============================================================================

/**
 * @brief	This method tests the invocation of all public methods with valid parameters.
 *
 * @return  result as an int: 0 (UNIT_TEST_PASSED) if passed, not 0 if failed
 */
MCUNIT_TEST( COneWireMemoryTest, AllPublicMethods_Invocation )
{
	int iResult = UNIT_TEST_PASSED;
	bool_c boolValue;
	upTreatmentCode eValue;
	uint32_t i32Value;
	byte byteValue;
	twobytes tbTreatCnt;
	byte byteArray[4] = {1, 2, 3, 4};
	time timeValue;
	unsignedfourbytes ulValue;
	char model[OW_LENGTH_MODEL];
	int	iInvocations = 0;

	// Invoke and check return codes (if there is one)
	iResult |= McUnitAssertIsEqual( OK, g_pOneWireMemory->InitializeHardware(), "InitializeHardware()" );
	iInvocations++;

	iResult |= McUnitAssertIsEqual( OK, g_pOneWireMemory->ReadFromHardware(), "ReadFromHardware()" );
	iInvocations++;

	iResult |= McUnitAssertIsEqual( OK, CheckHardware(), "CheckHardware()" );
	iInvocations++;

	iResult |= McUnitAssertIsEqual( OK, CheckForHandpiece( &boolValue ), "CheckForHandpiece()" );
	iInvocations++;

	iResult |= McUnitAssertIsEqual( OK, GetTherapyCode( &eValue ), "GetTherapyCode()" );
	iInvocations++;

	iResult |= McUnitAssertIsEqual( OK, GetSerialNumber( &i32Value ), "GetSerialNumber()" );
	iInvocations++;

	iResult |= McUnitAssertIsEqual( OK, GetFullTreatmentCount( &tbTreatCnt ), "GetFullTreatmentCount()" );
	iInvocations++;

	iResult |= McUnitAssertIsEqual( OK, g_pOneWireMemory->GetTherapyTime( &ulValue ), "GetTherapyTime()" );
	iInvocations++;

	iResult |= McUnitAssertIsEqual( OK, SetFirstUseTime( (time)0 ), "SetFirstUseTime()" );
	iInvocations++;

	iResult |= McUnitAssertIsEqual( OK, GetFirstUseTime( &timeValue ), "GetFirstUseTime()" );
	iInvocations++;

	iResult |= McUnitAssertIsEqual( OK, GetLockState( &boolValue ), "GetLockState()" );
	iInvocations++;

	iResult |= McUnitAssertIsEqual( OK, GetModel( model ), "GetModel()" );
	iInvocations++;

	iResult |= McUnitAssertIsEqual( OK, GetRomId( &byteValue ), "GetRomId()" );
	iInvocations++;

	iResult |= McUnitAssertIsEqual( OK, WriteAccess( 0x100, byteArray, 4, 0 ), "WriteAccess()" );
	iInvocations++;

	iResult |= McUnitAssertIsEqual( OK, Execute(), "Execute()" );
	iInvocations++;

	EnableOwMonitor( false );
	iResult |= McUnitAssertIsEqual( 0, 0, "void EnableOwMonitor()" );
	iInvocations++;

	// Check return codes with monitor disabled for affected methods
	iResult |= McUnitAssertIsEqual( OK, Execute(), "Execute() with monitor disabled" );
	iResult |= McUnitAssertIsEqual( OK, g_pOneWireMemory->ReadFromHardware(), "ReadFromHardware() with monitor disabled" );
	EnableOwMonitor( true );	// Re-enable for next test

	LogOneValue("Number of methods tested: %d", iInvocations);

	return iResult;
}



//=============================================================================
// CASE 2: PARAMETER TESTS
//=============================================================================

/**
 * @brief	This method tests the default passed parameter(s) for all public methods.
 *
 * @return  result as an int: 0 (UNIT_TEST_PASSED) if passed, not 0 if failed
 */

MCUNIT_TEST( COneWireMemoryTest, AllPublicMethodsDefault_Parameter )
{
	int iResult = UNIT_TEST_PASSED;
	bool_c boolValue;
	upTreatmentCode eValue;
	uint32_t i32Value;
	byte byteValue;
	twobytes tbTreatCnt;
	time timeValue;
	unsignedfourbytes ulValue;
	char model[OW_LENGTH_MODEL];
	int	iInvocations = 0;

	// Invoke and check return parameters with default values
	CheckForHandpiece( &boolValue );
	iResult |= McUnitAssertIsEqual( (uint8_t)0, boolValue, "Handpiece presence" );
	iInvocations++;

	GetTherapyCode( &eValue );
	iResult |= McUnitAssertIsEqual( 0, eValue, "Therapy code" );
	iInvocations++;

	GetSerialNumber( &i32Value );
	iResult |= McUnitAssertIsEqual( (uint32_t)0, i32Value, "Serial number" );
	iInvocations++;

	GetFullTreatmentCount( &tbTreatCnt );
	iResult |= McUnitAssertIsEqual( (twobytes)0, tbTreatCnt, "Full treatment count" );
	iInvocations++;

	g_pOneWireMemory->GetTherapyTime( &ulValue );
	iResult |= McUnitAssertIsEqual( (unsignedfourbytes)0, ulValue, "Therapy time" );
	iInvocations++;

	GetFirstUseTime( &timeValue );
	iResult |= McUnitAssertIsEqual( (uint32_t)0, timeValue, "First Use Time" );
	iInvocations++;

	SetFirstUseTime( (time)1234567890 );
	GetFirstUseTime( &timeValue );
	iResult |= McUnitAssertIsEqual( (uint32_t)1234567890, timeValue, "First Use Time #2" );
	iInvocations++;

	GetLockState( &boolValue );
	iResult |= McUnitAssertIsEqual( (uint8_t)0, boolValue, "Lock state" );
	iInvocations++;

	GetModel( model );
	iResult |= McUnitAssertIsEqual( (int)0, (int)strnlen( model, OW_LENGTH_MODEL ), "Model number string" );
	iInvocations++;

	GetRomId( &byteValue );
	iResult |= McUnitAssertIsEqual( (uint8_t)0, byteValue, "ROM ID" );
	iInvocations++;

	LogOneValue("Number of methods tested: %d", iInvocations);

	return iResult;
}


//=============================================================================
// CASE 3: INTERACTION TESTS
//=============================================================================

/**
 * @brief	This method tests the interaction of TBD.
 *
 * @return  result as an int: 0 (UNIT_TEST_PASSED) if passed, not 0 if failed
 */

MCUNIT_TEST( COneWireMemoryTest, OwConnect_Interaction )
{
	int iResult = UNIT_TEST_PASSED;

	// Test 1 : Check that the one-wire debounce ensures a not ready status for 300ms
	// 			After a connection is made

	// Test 1 : Setup
	iResult |= McUnitAssertIsOK( g_timeBase.InitializeHardware(), "g_timeBase.InitializeHardware" );

	// Connect 1-wire memory. File has 50 data rows
	iResult |= McUnitAssertIsEqual( OK, f_simDataAccess.LoadDataFile( "DataFiles/FreshCode2OneWire.csv", true ),
	        "Load file DataFiles/FreshCode2OneWire.csv", false );

	g_pOneWireMemory->InitializeHardware();
	g_pOneWireMemory->EnableOwMonitor( true );

	// Read in the 49 rows of data
	for ( int i = 0; i < 49; i++ )
		{
		iResult |= McUnitAssertIsEqual( OK, g_timeBase.ReadFromHardware(), "f_timeBase.ReadFromHardware", false );
		MsDelay( (twobytes) 2 );
		iResult |= McUnitAssertIsEqual( OK, g_pOneWireMemory->ReadFromHardware(), "g_pOneWireMemory->ReadFromHardware",
		        false );
		iResult |= McUnitAssertIsEqual( OK, g_pOneWireMemory->Execute(), "g_pOneWireMemory->Execute", false );
		iResult |= McUnitAssertIsEqual( OK, f_simDataAccess.GoToNextRecord(), "f_simDataAccess.GoToNextRecord", false );
		}

	// Test 1: Procedure
	// The device is now programmed, and a presence check will return true
	// Since it takes 350ms for presence detection, the one-wire should not be
	// in the ready state immediately.

	// Let presence remain high for 350ms
	for ( int i = 0; i < 7; i++ )
		{
		bool_c owReady = false;
		iResult |= McUnitAssertIsEqual( OW_STATE_DISCONNECTED, GetOwState(), "One wire DISCONNECTED", true );
		iResult |= McUnitAssertIsEqual( OK, g_timeBase.ReadFromHardware(), "f_timeBase.ReadFromHardware", false );
		MsDelay( (twobytes) 50 );
		iResult |= McUnitAssertIsEqual( OK, g_pOneWireMemory->ReadFromHardware(), "g_pOneWireMemory->ReadFromHardware",
				        false );
		iResult |= McUnitAssertIsEqual( OK, g_pOneWireMemory->Execute(), "g_pOneWireMemory->Execute", false );
		CheckForHandpiece( &owReady );
		iResult |= McUnitAssertTrue( !owReady, "OneWire Not Ready", true );
		}

	// After 350ms the one-wire should be connected
	iResult |= McUnitAssertIsEqual( OW_STATE_CONNECTED, GetOwState(), "One wire CONNECTED", true );

	// Test 2: Simulate the one-wire connecting after 350ms of presence

	// Test 2: Procedure
	// Now that the one-wire is CONNECTED, the one-wire should be ready after reading in 5 banks of memory
	for ( int i = 0; i <= 4; i++ )
		{
		iResult |= McUnitAssertIsEqual( OW_STATE_CONNECTED, GetOwState(), "One wire not ready", false );
		iResult |= McUnitAssertIsEqual( OK, g_timeBase.ReadFromHardware(), "f_timeBase.ReadFromHardware", false );
		MsDelay( (twobytes) 50 );
		iResult |= McUnitAssertIsEqual( OK, g_timeBase.ReadFromHardware(), "f_timeBase.ReadFromHardware", false );
		iResult |= McUnitAssertIsEqual( OK, g_pOneWireMemory->ReadFromHardware(), "g_pOneWireMemory->ReadFromHardware",
						false );
		iResult |= McUnitAssertIsEqual( OK, g_pOneWireMemory->Execute(), "g_pOneWireMemory->Execute", false );
		}

	// The one-wire should be ready after reading in the banks of memory
	bool_c owReady = false;
	iResult |= McUnitAssertIsEqual( OW_STATE_READY, GetOwState(), "One-wire is READY", true );
	CheckForHandpiece( &owReady );
	iResult |= McUnitAssertIsEqual( (bool_c)true, owReady, "Handpiece is available to the supervisor", true );

	// Test 3: Loss of presence should change the state to DISCONNECTED

	// Test 3: Setup
	SHdwSimData * pHdwSimData;
	twobytes tbIndex = 0;

	f_simDataAccess.GetSimData( &pHdwSimData, &tbIndex );
	pHdwSimData->m_owPresence = 0;
	f_simDataAccess.SetSimData( pHdwSimData );

	// Test 3: Procedure
	iResult |= McUnitAssertIsEqual( OK, g_pOneWireMemory->ReadFromHardware(), "g_pOneWireMemory->ReadFromHardware",
							false );
	iResult |= McUnitAssertIsEqual( OK, g_pOneWireMemory->Execute(), "g_pOneWireMemory->Execute", false );
	// Must ReadFromHardware() & Execute() twice to get the main state machine back to idle
	iResult |= McUnitAssertIsEqual( OK, g_pOneWireMemory->ReadFromHardware(), "g_pOneWireMemory->ReadFromHardware",
					false );
	iResult |= McUnitAssertIsEqual( OK, g_pOneWireMemory->Execute(), "g_pOneWireMemory->Execute", false );
	CheckForHandpiece( &owReady );
	iResult |= McUnitAssertIsEqual( (bool_c)false, owReady, "Handpiece is unavailable to the supervisor", true );
	iResult |= McUnitAssertIsEqual( OW_STATE_DISCONNECTED, GetOwState(), "One wire DISCONNECTED", true );

	return iResult;
}


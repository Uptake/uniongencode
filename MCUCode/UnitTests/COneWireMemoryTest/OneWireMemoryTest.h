/**
 * @file	OneWireMemoryTest.h
 * @par		Package: CvasMcu
 * @par		Project: CVAS Generator
 * @par		Copyright (c) 2016, Uptake Medical, Inc.
 * @author	Kurt Jan 12, 2012 Created file
 * @brief	This file contains unit tests for COneWireMemory
 */

#ifndef ONEWIREMEMORYTEST_H_
#define ONEWIREMEMORYTEST_H_

// Include Files
#include <OneWireMemory.h>
#include "McUnit.h"
#include "UnitTestIntfc.h"
#include "TestMain.h"
#include "SetupHelpers.h"

// Referenced classes
#include <TreatmentRecord.h>

// Public Macros and Constants

// Public Type Definitions (Enums, Structs & Classes)

/**
 * @class	COneWireMemoryTest
 * @brief	This class provides unit tests for COneWireMemory
 *
 * Virtual Functions Overridden: @n@n
 *  UnitTestListSetup - Set up list of Suites, Cases and Tests to be execute @n
 * 	CommonTesthandle - Called to have tests run @n
 *
 * External Data Members: None @n@n
 *
 */
class COneWireMemoryTest: private CMcUnit<COneWireMemoryTest>, public CUnitTestIntfc, public CTestMain, private COneWireMemory
{
public:
	COneWireMemoryTest();
	int CommonTestHandle();
	void UnitTestListSetup();
	// Case 1: Invocation Tests
	MCUNIT_TEST_DEC( AllPublicMethods_Invocation );
	// Case 2: Parameter Tests
	MCUNIT_TEST_DEC( AllPublicMethodsDefault_Parameter );
	// Case 3: Interaction Tests
	MCUNIT_TEST_DEC( OwConnect_Interaction );

	SMcUnitTest sMcUnitTestList[3];	// Increment index for each test added, regardless of the case in which it falls
	SMcUnitCase sMcUnitCase[3];
	SMcUnitCase * sMcUnitCaseList[4];
	SMcUnitSuite sMcUnitTestSuite;
};

// Inline Functions (follows class definition)


#endif /* ONEWIREMEMORYTEST_H_ */

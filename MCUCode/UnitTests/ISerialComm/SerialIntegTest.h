/**
 * @file	SerialIntegTest.h
 * @par		Package: CvasMcu
 * @par		Project: CVAS Generator
 * @par		Copyright (c) 2016, Uptake Medical, Inc.
 * @author	Glen Dec 29, 2011 Created file
 * @brief	This file contains unit tests for CRS232
 */

#ifndef SERIALINTEGTEST_H_
#define SERIALINTEGTEST_H_

// Include Files
#include <RS232.h>
#include "McUnit.h"
#include "UnitTestIntfc.h"
#include "TestMain.h"
#include "SetupHelpers.h"

// Public Macros and Constants

// Public Type Definitions (Enums, Structs & Classes)

/**
 * @class	CSerialIntegTest
 * @brief	This class provides unit tests for CRS232
 *
 * Virtual Functions Overridden: @n@n
 *  UnitTestListSetup - Set up list of Suites, Cases and Tests to be execute @n
 * 	CommonTesthandle - Called to have tests run @n
 *
 * External Data Members: None @n@n
 *
 */
class CSerialIntegTest: private CMcUnit<CSerialIntegTest> , public CUnitTestIntfc, public CTestMain
{
public:
	CSerialIntegTest();
	int CommonTestHandle();
	void UnitTestListSetup();

	// Tests
	MCUNIT_TEST_DEC( NormalTraffic );
	MCUNIT_TEST_DEC( GoodSbc_BadRfGen );
	MCUNIT_TEST_DEC( BadSbc_GoodRfGen );
	MCUNIT_TEST_DEC( CommBadIntermittant );
	MCUNIT_TEST_DEC( VariableCommCounts );


	SMcUnitTest sMcUnitTestList[ 5 ]; // Increment index for each test added, regardless of the case in which it falls
	SMcUnitCase sMcUnitCase[ 3 ];
	SMcUnitCase * sMcUnitCaseList[ 4 ];
	SMcUnitSuite sMcUnitTestSuite;
private:
	CSetupHelpers m_setupHelpers;
};

// Inline Functions (follows class definition)


#endif /* SERIALINTEGTEST_H_ */

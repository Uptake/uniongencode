/**
 * @file	SerialIntegTest.cpp
 * @par		Package: CvasMcu
 * @par		Project: CVAS Generator
 * @par		Copyright (c) 2016, Uptake Medical, Inc.
 * @author	Glen April 26, 2012 Created file
 * @brief	This file contains integration tests for serial communication
 */

// Include Files
#include "SerialIntegTest.h"
#include <avr/interrupt.h>
#include "ObjectSingletons.h"
#include "RFGenerator.h"

// External Public Data

// File Scope Macros and Constants

// File Scope Type Definitions (Enums, Structs & Classes)

// File Scope Data
static CSerialIntegTest f_SerialIntegTest; // Instantiate one so it registers itself

// File Scope Functions

// Public Functions

/**
 * @brief	This is the constructor for CSerialIntegTest
 *
 */

CSerialIntegTest::CSerialIntegTest() :
	CMcUnit<CSerialIntegTest> ( this ), CTestMain( (CUnitTestIntfc*) this )
{
}

/**
 * @brief	This method calls into CMcUnit to have the tests run.
 *
 * @return   Always 1
 */
int CSerialIntegTest::CommonTestHandle()
{
	sei();
	McUnitSuiteRun( &sMcUnitTestSuite );
	McUnitSuiteReport( &sMcUnitTestSuite, DEPTH_TEST_REPORT );
	return 1;
}

/**
 * @brief	This method sets up the lists of tests to run
 *
 */
void CSerialIntegTest::UnitTestListSetup()
{

	int iList = 0;
	int iCase = 0;

	// Set up Case 1
	ADD_TEST_CASE( sMcUnitCase, iCase, sMcUnitTestList + iList, 5, "Integration Tests", "Test failure")
	ADD_TEST_LIST( sMcUnitTestList, iList, &CSerialIntegTest::NormalTraffic, "Normal traffic", "unexpected result" )
	ADD_TEST_LIST( sMcUnitTestList, iList, &CSerialIntegTest::GoodSbc_BadRfGen, "Good Sbc and Bad RfGen", "unexpected result" )
	ADD_TEST_LIST( sMcUnitTestList, iList, &CSerialIntegTest::BadSbc_GoodRfGen, "Bad Sbc and Good RfGen", "unexpected result" )
	ADD_TEST_LIST( sMcUnitTestList, iList, &CSerialIntegTest::CommBadIntermittant, "Intermittant communication errors", "unexpected result" )
	ADD_TEST_LIST( sMcUnitTestList, iList, &CSerialIntegTest::VariableCommCounts, "Variable counts of commands with errors", "unexpected result" )

	MAKE_TEST_SUITE( sMcUnitTestSuite, sMcUnitCaseList, sMcUnitCase, iCase, "CSerialIntegTest" )

}

/**
 * @brief	This method tests successful operation of serial communication of the RF generator and SBC interface
 * 			being called alternately.
 *
 * @return   result as an int
 */
MCUNIT_TEST( CSerialIntegTest, NormalTraffic )
{
	int iResult = UNIT_TEST_PASSED;

	iResult |= McUnitAssertIsEqual( OK, g_timeBase.InitializeHardware(), "g_timeBase.InitializeHardware" );
	iResult |= McUnitAssertIsEqual( OK, g_pDigitalInputOutput->InitializeHardware(),
	        "g_pDigitalInputOutput->InitializeHardware" );
	iResult |= McUnitAssertIsEqual( OK, g_pRFGen->InitializeHardware(), "g_pRFGen->InitializeHardware" );
	iResult |= McUnitAssertIsEqual( OK, g_pSbcIntfc->InitializeHardware(), "g_pSbcIntfc->InitializeHardware" );

	iResult |= m_setupHelpers.SbcInitAndConnect();
	iResult |= m_setupHelpers.InterlockAll();
	f_simDataAccess.SetOpcondDecim( 10 );

	iResult |= m_setupHelpers.SbcReadWrite();
	iResult |= m_setupHelpers.SbcReadWrite();
	iResult |= m_setupHelpers.SbcReadWrite();

	iResult |= m_setupHelpers.StartConnectToRFGen( RF_GEN_POWER_UP_CYCLE_COUNT + RF_GEN_COMPLETE_POWER_UP );
	iResult |= m_setupHelpers.RFGenWriteRead();
	iResult |= m_setupHelpers.RFGenWriteRead();
	for ( int i = 0; i < 100; i++ )
		{
		iResult |= m_setupHelpers.RFGenWriteRead();
		iResult |= m_setupHelpers.SbcReadWrite();
		}

	iResult |= m_setupHelpers.SbcSendAllLogs();

	return iResult;
}

/**
 * @brief	This method tests that the SBC interface continues to operate correctly while the RF generator suffers a series
 * 			of communication failures
 *
 * @return   result as an int
 */

MCUNIT_TEST( CSerialIntegTest, GoodSbc_BadRfGen )
{
	int iResult = UNIT_TEST_PASSED;
	SHdwSimData * pHdwSimData;
	twobytes index;
	f_simDataAccess.GetSimData( &pHdwSimData, &index );

	// Wake up SBC, connect RDO and perform some normal communication
	iResult |= m_setupHelpers.SbcReadWrite();
	iResult |= m_setupHelpers.SbcReadWrite();
	iResult |= m_setupHelpers.SbcReadWrite();

	iResult |= m_setupHelpers.StartConnectToRFGen( RF_GEN_POWER_UP_CYCLE_COUNT + RF_GEN_COMPLETE_POWER_UP );
	iResult |= m_setupHelpers.RFGenWriteRead();
	iResult |= m_setupHelpers.RFGenWriteRead();
	for ( int i = 0; i < 10; i++ )
		{
		iResult |= m_setupHelpers.RFGenWriteRead();
		iResult |= m_setupHelpers.SbcReadWrite();
		}

	// Have the RF Generator send a bad response
	pHdwSimData->m_bRfGenAction = UART_BAD_RESP;
	f_simDataAccess.SetSimData( pHdwSimData );
	for ( int i = 0; i < COMM_ERR_TOLERANCE - 1; i++ )
		{
		iResult |= m_setupHelpers.RFGenWriteRead();
		iResult |= m_setupHelpers.SbcReadWrite();
		}

	iResult |= m_setupHelpers.RFGenWriteRead( OK, ERR_RF_GEN_RESET );
	iResult |= m_setupHelpers.SbcReadWrite();

	// Re-start the RF Generator
	iResult |= m_setupHelpers.StartConnectToRFGen( RF_GEN_POWER_UP_CYCLE_COUNT + RF_GEN_COMPLETE_POWER_UP );
	iResult |= m_setupHelpers.RFGenWriteRead();

	for ( int i = 0; i < COMM_ERR_TOLERANCE; i++ )
		{
		iResult |= m_setupHelpers.RFGenWriteRead();
		iResult |= m_setupHelpers.SbcReadWrite();
		}

	// Have the RF Generator refuse to answer
	pHdwSimData->m_bRfGenAction = UART_NO_RESP;
	f_simDataAccess.SetSimData( pHdwSimData );
	for ( int i = 0; i < COMM_ERR_TOLERANCE - 1; i++ )
		{
		iResult |= m_setupHelpers.RFGenWriteRead();
		iResult |= m_setupHelpers.SbcReadWrite();
		}

	iResult |= m_setupHelpers.RFGenWriteRead( OK, ERR_RF_GEN_RESET );
	iResult |= m_setupHelpers.SbcReadWrite();

	// Re-start communications
	iResult |= m_setupHelpers.StartConnectToRFGen( RF_GEN_POWER_UP_CYCLE_COUNT + RF_GEN_COMPLETE_POWER_UP );
	for ( int i = 0; i < 20; i++ )
		{
		iResult |= m_setupHelpers.RFGenWriteRead( OK, OK );
		iResult |= m_setupHelpers.SbcReadWrite();
		}

	iResult |= m_setupHelpers.SbcSendAllLogs();

	return iResult;
}

/**
 * @brief	This method tests that the RF generator continues to operate normally while the SBC has
 * 			a series of failures.
 *
 * @return   result as an int
 */
MCUNIT_TEST( CSerialIntegTest, BadSbc_GoodRfGen )
{
	int iResult = UNIT_TEST_PASSED;
	SHdwSimData * pHdwSimData;
	twobytes index;
	f_simDataAccess.GetSimData( &pHdwSimData, &index );

	//	iResult |= m_setupHelpers.SbcInitAndConnect();
	//	iResult |= m_setupHelpers.InterlockAll();

	pHdwSimData->m_bRfGenAction = UART_NORMAL;
	f_simDataAccess.SetSimData( pHdwSimData );
	iResult |= m_setupHelpers.StartConnectToRFGen( RF_GEN_POWER_UP_CYCLE_COUNT + RF_GEN_COMPLETE_POWER_UP );
	iResult |= m_setupHelpers.RFGenWriteRead();

	iResult |= m_setupHelpers.SbcReadWrite();
	iResult |= m_setupHelpers.RFGenWriteRead();

	for ( int i = 0; i < 8; i++ )
		{
		if ( 0 == i % 4 )
			pHdwSimData->m_sbcAction = UART_NORMAL;
		else if ( 1 == i % 4 )
			pHdwSimData->m_sbcAction = UART_BAD_VERSION;
		else if ( 2 == i % 4 )
			pHdwSimData->m_sbcAction = UART_NO_RESP;
		else
			pHdwSimData->m_sbcAction = UART_BAD_CHECKSUM;
		f_simDataAccess.SetSimData( pHdwSimData );

		for ( int j = 0; j < 8; j++ )
			{
			iResult |= m_setupHelpers.RFGenWriteRead();
			iResult |= m_setupHelpers.SbcReadWrite();
			}
		pHdwSimData->m_sbcAction = UART_NORMAL;
		f_simDataAccess.SetSimData( pHdwSimData );
		iResult |= m_setupHelpers.SbcReadWrite();
		iResult |= m_setupHelpers.SbcReadWrite();
		}

	pHdwSimData->m_sbcAction = UART_NORMAL;
	f_simDataAccess.SetSimData( pHdwSimData );
	iResult |= m_setupHelpers.SbcSendAllLogs();

	return iResult;
}

/**
 * @brief	This method tests that each CRS232 user operates correctly and recovers correctly while both
 * 			RF generator and SBC interface have a series of pseudo-random failures.
 *
 * @return   result as an int
 */
MCUNIT_TEST( CSerialIntegTest, CommBadIntermittant )
{
	int iResult = UNIT_TEST_PASSED;
	SHdwSimData * pHdwSimData;
	twobytes index;
	f_simDataAccess.GetSimData( &pHdwSimData, &index );

	//	iResult |= m_setupHelpers.SbcInitAndConnect();
	//	iResult |= m_setupHelpers.InterlockAll();

	pHdwSimData->m_bRfGenAction = UART_NORMAL;
	f_simDataAccess.SetSimData( pHdwSimData );
	iResult |= m_setupHelpers.StartConnectToRFGen( RF_GEN_POWER_UP_CYCLE_COUNT + RF_GEN_COMPLETE_POWER_UP );
	iResult |= m_setupHelpers.RFGenWriteRead();

	iResult |= m_setupHelpers.SbcReadWrite();
	iResult |= m_setupHelpers.RFGenWriteRead();

	for ( int i = 0; i < 32; i++ )
		{
		if ( 0 == i % 4 )
			pHdwSimData->m_sbcAction = UART_NO_RESP;
		else if ( 1 == i % 4 )
			pHdwSimData->m_sbcAction = UART_BAD_VERSION;
		else if ( 2 == i % 4 )
			pHdwSimData->m_sbcAction = UART_SEND_GARBAGE;
		else
			pHdwSimData->m_sbcAction = UART_PAD_GARBAGE;

		if ( 0 == i % 3 )
			pHdwSimData->m_bRfGenAction = UART_NO_RESP;
		else if ( 1 == i % 3 )
			pHdwSimData->m_bRfGenAction = UART_BAD_CHECKSUM;
		else if ( 2 == i % 3 )
			pHdwSimData->m_bRfGenAction = UART_SEND_GARBAGE;

		f_simDataAccess.SetSimData( pHdwSimData );

		for ( int j = 0; j < COMM_ERR_TOLERANCE - 1; j++ )
			{
			iResult |= m_setupHelpers.RFGenWriteRead();
			iResult |= m_setupHelpers.SbcReadWrite();
			}

		pHdwSimData->m_sbcAction = UART_NORMAL;
		pHdwSimData->m_bRfGenAction = UART_NORMAL;
		f_simDataAccess.SetSimData( pHdwSimData );
		iResult |= m_setupHelpers.SbcReadWrite();
		iResult |= m_setupHelpers.RFGenWriteRead();
		iResult |= m_setupHelpers.SbcReadWrite();
		iResult |= m_setupHelpers.SbcSendAllLogs(); // Clear errors
		}

	pHdwSimData->m_sbcAction = UART_NORMAL;
	f_simDataAccess.SetSimData( pHdwSimData );
	iResult |= m_setupHelpers.SbcSendAllLogs();

	return iResult;
}

/**
 * @brief	This method tests that the consumers of CRS232 operate correctly in different patterns
 * 			of read/write usage besides strict alternating.
 *
 * @return   result as an int
 */
MCUNIT_TEST( CSerialIntegTest, VariableCommCounts )
{
	byte iCounts[] =
	{ 1, 4, 7, 3, 11, 2, 17, 6, 5 };

#define COUNT_VALUES (sizeof(iCounts)/sizeof(byte))
	int iResult = UNIT_TEST_PASSED;
	SHdwSimData * pHdwSimData;
	twobytes index;
	f_simDataAccess.GetSimData( &pHdwSimData, &index );

	pHdwSimData->m_sbcAction = UART_NORMAL;
	pHdwSimData->m_bRfGenAction = UART_NORMAL;
	f_simDataAccess.SetSimData( pHdwSimData );
	iResult |= m_setupHelpers.StartConnectToRFGen( RF_GEN_POWER_UP_CYCLE_COUNT + RF_GEN_COMPLETE_POWER_UP );
	iResult |= m_setupHelpers.RFGenWriteRead();

	iResult |= m_setupHelpers.SbcReadWrite();
	iResult |= m_setupHelpers.RFGenWriteRead();

	for ( int i = 0; i < 32; i++ )
		{

		for ( int j = 0; j < iCounts[ i % COUNT_VALUES ]; j++ )
			{
			iResult |= m_setupHelpers.SbcReadWrite();
			}

		for ( int j = 0; j < iCounts[ COUNT_VALUES - ( i % COUNT_VALUES ) - 1 ]; j++ )
			{
			iResult |= m_setupHelpers.RFGenWriteRead();
			}
		}

	pHdwSimData->m_sbcAction = UART_NORMAL;
	f_simDataAccess.SetSimData( pHdwSimData );
	iResult |= m_setupHelpers.SbcSendAllLogs();

	return iResult;
}

//=========================================================================================================================
// Local functions
//=========================================================================================================================


/*
 * GenMonTest.h
 *
 *  Created on: Apr 5, 2012
 *      Author: Chris Keller
 */

#ifndef GENMONTEST_H_
#define GENMONTEST_H_

#include "CvasMcuError.h"
#include "CvasMcuTypes.h"
#include "CvasMcuEnum.h"
#include "TimerAccess.h"
#include "GeneratorMonitor.h"

/**
 * @class	CGenMonTest
 *
 * @brief	This class is a mock that overrides certain methods from CGeneratorMonitor.
 *
 * 			This allows an instance of CGenMonTest to replace the globally-used CGeneratorMonitor
 * 			with those methods performing tightly-controlled operations. The methods from CGeneratorMonitor
 * 			are overridden with new implementation. Additional methods were added to control hte behavior
 * 			of the new, overriding "mock" methods.
 *
 * Virtual Functions Overridden:
 * - CheckGenerator
 * - CheckInterlocks
 * - GetDebouncedTrigger
 * - CheckRF
 * - CheckOverTemp
 * - PerformCriticalMonitoring
 *
 * External Data Members: None @n@n
 *
 */
class CGenMonTest: public CGeneratorMonitor
{
private:
	tick m_tDebounceOffTick;
	bool_c m_bLastDebounce;
	byte m_bPowerOnErrorCnt;
	byte m_bPowerRunningErrorCnt;
	byte m_bPowerRangeErrorCnt;
	byte m_bOverTempCnt;
	upCvasErr CheckSbcIntfc(); // TODO: Remove as is unused
	upCvasErr CheckCriticalTemperatures();

public:
	CGenMonTest();
	virtual ~CGenMonTest(){}
	virtual upCvasErr CheckGenerator( upInterlockFlag eInter );
	virtual upCvasErr CheckInterlocks( bool_c bThermoUnconnError, upInterlockFlag * pInter, bool_c bEnableRfAC );
	virtual upCvasErr GetDebouncedTrigger( bool_c * pbOn );
	virtual upCvasErr CheckRF();
	virtual upCvasErr CheckOverTemp();
	virtual upCvasErr PerformCriticalMonitoring();

	// Additional unit test interface methods and variables
	upCvasErr m_eUnitErr;
	upInterlockFlag m_eUnitInter;
	bool_c m_bUnitBool;

	void SetInterlocks( upInterlockFlag pInter );
	void SetError( upCvasErr eErrRtn );
	void SetBool( bool_c bUnitBoolParam );
	void ResetState();
};

#endif /* GENMONTEST_H_ */

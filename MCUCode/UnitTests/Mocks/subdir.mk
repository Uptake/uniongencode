################################################################################
# Automatically-generated file. Do not edit!
################################################################################

# Add inputs and outputs from these tool invocations to the build variables 
CPP_SRCS += \
./$(wildcard *.cpp)


OBJS += \
./$(CPP_SRCS:.cpp=.o)


CPP_DEPS += \
./$(CPP_SRCS:.cpp=.d)


# Each subdirectory must supply rules for building sources it contributes
%.o: ./%.cpp
	@echo 'Building file: $<'
	@echo 'Invoking: AVR C++ Compiler'
	avr-g++ -I"./" -I"../../Common/include" -I"../../McuApplicationLib/include" $(MOCK_LIST) -Wall -g2 -gdwarf-2 -O0 -fpack-struct -fshort-enums -funsigned-char -funsigned-bitfields -fno-exceptions -mmcu=at90can128 -DF_CPU=14745600UL -MMD -MP -MF"$(@:%.o=%.d)" -MT"$(@:%.o=%.d)" -c -o"$@" "$<"
	@echo 'Finished building: $<'
	@echo ' '



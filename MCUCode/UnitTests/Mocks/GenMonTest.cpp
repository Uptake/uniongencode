/*
 * GenMonTest.cpp
 *
 *  Created on: Apr 5, 2012
 *      Author: Chris Keller
 */

#include "GenMonTest.h"

CGenMonTest::CGenMonTest()
{

}

upCvasErr CGenMonTest::CheckCriticalTemperatures()
{
	return m_eUnitErr;
}

upCvasErr CGenMonTest::CheckGenerator( upInterlockFlag eInter )
{
	return m_eUnitErr;
}

upCvasErr CGenMonTest::CheckInterlocks( bool_c bThermoUnconnError, upInterlockFlag * pInter, bool_c bEnableRfAC )
{
	*pInter = m_eUnitInter;
	return m_eUnitErr;
}

upCvasErr CGenMonTest::GetDebouncedTrigger( bool_c * pbOn )
{
	*pbOn = m_bUnitBool;
	return m_eUnitErr;
}

upCvasErr CGenMonTest::CheckRF()
{
	return m_eUnitErr;
}
upCvasErr CGenMonTest::CheckOverTemp()
{
	return m_eUnitErr;
}
upCvasErr CGenMonTest::PerformCriticalMonitoring()
{
	return m_eUnitErr;
}

void CGenMonTest::SetInterlocks( upInterlockFlag eInter )
{
	m_eUnitInter = eInter;
}

void CGenMonTest::SetError( upCvasErr eErrRtn )
{
	m_eUnitErr = eErrRtn;
}

void CGenMonTest::SetBool( bool_c bUnitBool )
{
	m_bUnitBool = bUnitBool;
}

void CGenMonTest::ResetState()
{
	m_bUnitBool = false;
	m_eUnitInter = INTLCK_NONE;
	m_eUnitErr = OK;
}

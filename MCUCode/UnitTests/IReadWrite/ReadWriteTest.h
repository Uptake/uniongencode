/**
 * @file	ReadWriteTest.h
 * @par		Package: CvasMcu
 * @par		Project: CVAS Generator
 * @par		Copyright (c) 2016, Uptake Medical, Inc.
 * @author	Glen Dec 29, 2011 Created file
 * @brief	This file contains unit tests for CRS232
 */

#ifndef READWRITETEST_H_
#define READWRITETEST_H_

// Include Files
#include "McUnit.h"
#include "UnitTestIntfc.h"
#include "TestMain.h"
#include "SetupHelpers.h"

// Public Macros and Constants

// Public Type Definitions (Enums, Structs & Classes)

/**
 * @class	CReadWriteTest
 * @brief	This class provides unit tests for CRS232
 *
 * Virtual Functions Overridden: @n@n
 *  UnitTestListSetup - Set up list of Suites, Cases and Tests to be execute @n
 * 	CommonTesthandle - Called to have tests run @n
 *
 * External Data Members: None @n@n
 *
 */

class CReadWriteTest: private CMcUnit<CReadWriteTest> , public CUnitTestIntfc, public CTestMain
{
private:
	CSetupHelpers m_setupHelpers;
	int InitializeHardware();
	int PerformReadWrite( bool_c bShow );

public:
	CReadWriteTest();
	int CommonTestHandle();
	void UnitTestListSetup();

	// Case 1: Invocation Tests
	MCUNIT_TEST_DEC( StackTest );
	MCUNIT_TEST_DEC( DataTest );

	SMcUnitTest sMcUnitTestList[ 5 ]; // Increment index for each test added, regardless of the case in which it falls
	SMcUnitCase sMcUnitCase[ 3 ];
	SMcUnitCase * sMcUnitCaseList[ 4 ];
	SMcUnitSuite sMcUnitTestSuite;
private:
};

// Inline Functions (follows class definition)


#endif /* READWRITETEST_H_ */

/**
 * @file	ReadWriteTest.cpp
 * @par		Package: CvasMcu
 * @par		Project: CVAS Generator
 * @par		Copyright (c) 2016, Uptake Medical, Inc.
 * @author	Glen April 26, 2012 Created file
 * @brief	This file contains integration tests for serial communication
 */

// Include Files
#include "stdlib.h"
#include "ReadWriteTest.h"
#include "ObjectSingletons.h"

// External Public Data

// File Scope Macros and Constants

// File Scope Type Definitions (Enums, Structs & Classes)

// File Scope Data
static CReadWriteTest f_ReadWriteTest; // Instantiate one so it registers itself

// File Scope Functions

// Public Functions

/**
 * @brief	This is the constructor for CReadWriteTest
 *
 */

CReadWriteTest::CReadWriteTest() :
	CMcUnit<CReadWriteTest> ( this ), CTestMain( (CUnitTestIntfc*) this )
{
}

/**
 * @brief	This method initializes all of the task objects by calling InitializeHardware on object as needed.
 *
 *
 * @return   Status as upCvasErr
 */
int CReadWriteTest::InitializeHardware()
{
	int iResult = UNIT_TEST_PASSED;

	// Initialize the hardware
	iResult |= McUnitAssertIsEqual( OK, g_pDiagnosticLed->InitializeHardware(), "g_pDiagnosticLed InitializeHardware" );
	iResult |= McUnitAssertIsEqual( OK, g_timeBase.InitializeHardware(), "g_timeBase.InitializeHardware" );
	//	iResult |= McUnitAssertIsEqual( OK, g_watchdog.InitializeHardware(), "f_watchdog.InitializeHardware" );
	iResult |= McUnitAssertIsEqual( OK, g_pDigitalInputOutput->InitializeHardware(), "g_pDigitalInputOutput->InitializeHardware" );
	iResult |= McUnitAssertIsEqual( OK, g_pRFGen->InitializeHardware(), "g_pRFGen->InitializeHardware" );
	iResult |= McUnitAssertIsEqual( OK, g_pOneWireMemory->InitializeHardware(), "g_pOneWireMemory->InitializeHardware" );
	iResult |= McUnitAssertIsEqual( OK, g_pSbcIntfc->InitializeHardware(), "g_pSbcIntfc->InitializeHardware" );
	iResult |= McUnitAssertIsEqual( OK, g_pTemperature->InitializeHardware(), "g_pTemperature->InitializeHardware" );
	iResult |= McUnitAssertIsEqual( OK, g_pWaterCntl->InitializeHardware(), "g_pWaterCntl->InitializeHardware" );
	iResult |= McUnitAssertIsEqual( OK, g_pSoundCntl->InitializeHardware(), "g_pSoundCntl->InitializeHardware" );

	// Send to SBC
	m_setupHelpers.MsDelay( (twobytes) 2 );
	iResult |= McUnitAssertIsEqual( OK, g_pSbcIntfc->WriteToHardware(), "g_pSbcIntfc->WriteToHardware", true );


	f_simDataAccess.SetOpcondDecim( 10 );

	return iResult;
}

/**
 * @brief	This method sets up the lists of tests to run
 *
 */
int CReadWriteTest::PerformReadWrite( bool_c bShow )
{
	int iResult = UNIT_TEST_PASSED;

	// Get new values from the hardware
	iResult |= McUnitAssertIsEqual( OK, g_timeBase.ReadFromHardware(), "g_timeBase.ReadFromHardware", bShow );
	// Do first to bump the simulation record to the latest sample
	iResult |= McUnitAssertIsEqual( OK, g_pTemperature->ReadFromHardware(), "g_pTemperature->ReadFromHardware", bShow );
	iResult |= McUnitAssertIsEqual( OK, g_pRFGen->ReadFromHardware(), "g_pRFGen->ReadFromHardware", bShow );
	iResult |= McUnitAssertIsEqual( OK, g_pWaterCntl->ReadFromHardware(), "g_pWaterCntl->ReadFromHardware", bShow );
	iResult |= McUnitAssertIsEqual( OK, g_pDigitalInputOutput->ReadFromHardware(), "g_pDigitalInputOutput->ReadFromHardware", bShow );
	iResult |= McUnitAssertIsEqual( OK, g_pOneWireMemory->ReadFromHardware(), "g_pOneWireMemory->ReadFromHardware", bShow );

	// Write new values, if any, to the hardware
	iResult |= McUnitAssertIsEqual( OK, g_pWaterCntl->WriteToHardware(), "g_pWaterCntl->WriteToHardware", bShow );
	iResult |= McUnitAssertIsEqual( OK, g_pRFGen->WriteToHardware(), "g_pRFGen->WriteToHardware", bShow );
	iResult |= McUnitAssertIsEqual( OK, g_pDigitalInputOutput->WriteToHardware(), "g_pDigitalInputOutput->WriteToHardware", bShow );

	// Move log message if possible
	// Get incoming information from the SBC
	m_setupHelpers.MsDelay( (twobytes) 2 );
	iResult |= McUnitAssertIsEqual( OK, g_pSbcIntfc->ReadFromHardware(), "g_pSbcIntfc->ReadFromHardware", bShow );

	// Send to SBC
	m_setupHelpers.MsDelay( (twobytes) 2 );
	iResult |= McUnitAssertIsEqual( OK, g_pSbcIntfc->WriteToHardware(), "g_pSbcIntfc->WriteToHardware", bShow );

	return iResult;
}

/**
 * @brief	This method calls into CMcUnit to have the tests run.
 *
 * @return   Always 1
 */
int CReadWriteTest::CommonTestHandle()
{
	//	sei();
	McUnitSuiteRun( &sMcUnitTestSuite );
	McUnitSuiteReport( &sMcUnitTestSuite, DEPTH_TEST_REPORT );
	return 1;
}

/**
 * @brief	This method sets up the lists of tests to run
 *
 */
void CReadWriteTest::UnitTestListSetup()
{

	int iList = 0;
	int iCase = 0;

	// Set up Case 1: Integration
	ADD_TEST_CASE( sMcUnitCase, iCase, sMcUnitTestList + iList, 2, "Interaction Tests", "failure")
	ADD_TEST_LIST( sMcUnitTestList, iList, &CReadWriteTest::DataTest, "Data test", "unexpected result" )
	ADD_TEST_LIST( sMcUnitTestList, iList, &CReadWriteTest::StackTest, "Stack test", "unexpected result" )

	MAKE_TEST_SUITE( sMcUnitTestSuite, sMcUnitCaseList, sMcUnitCase, iCase, "CReadWriteTest" )

}

//=============================================================================
// CASE 1: Integration TESTS
//=============================================================================

/**
 * @brief	This method tests
 *
 * @return   result as an int
 */
MCUNIT_TEST( CReadWriteTest, StackTest )
{
	int iResult = UNIT_TEST_PASSED;
	bool_c bShow = true;
	InitializeHardware();

	for ( int j = 0; j < 3; j++ )
		{
		bShow = true;
		m_setupHelpers.LoadSelfTestFile();
		for ( int i = 0; i < 400; i++ )
			{
			iResult |= PerformReadWrite( bShow );
			iResult |= m_setupHelpers.CheckStack( bShow );
			m_setupHelpers.SetOperatingConditions( OPER_STATE_CNT, 0 );
			bShow = false;
			f_simDataAccess.GoToNextRecord();
			}
		}

	m_setupHelpers.ReportStack();
	iResult |= m_setupHelpers.SbcSendAllLogs();
	g_timeBase.MsDelay( 20 );
	return iResult;
}
/**
 * @brief	This method tests
 *
 * @return   result as an int
 */
MCUNIT_TEST( CReadWriteTest, DataTest )
{
	int iResult = UNIT_TEST_PASSED;
	InitializeHardware();
	bool_c bShow = true;
	SOpconMsg sOpCon1;
	SOpconMsg sOpCon2;

	for ( int i = 0; i < 10; i++ )
		{
		iResult |= PerformReadWrite( bShow );
		m_setupHelpers.SetOperatingConditions( OPER_STATE_CNT, 0 );
		bShow = false;
		}

	m_setupHelpers.GetOpCond( &sOpCon1 );
	bShow = true;

	for ( int i = 0; i < 100; i++ )
		{
		iResult |= PerformReadWrite( bShow );
		m_setupHelpers.SetOperatingConditions( OPER_STATE_CNT, 0 );
		m_setupHelpers.GetOpCond( &sOpCon1 );
		iResult |= McUnitAssertTrue( memcmp( &sOpCon1, &sOpCon2, sizeof(SOpconMsg) ) ? true : false, "Opcons equal",
		        bShow );
		bShow = false;
		}

	m_setupHelpers.SbcSendAllLogs();
	g_timeBase.MsDelay( 20 );
	return iResult;
}


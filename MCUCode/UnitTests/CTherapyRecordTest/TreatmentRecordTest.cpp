/**
 * @file	TreatmentRecordTest.cpp
 * @par		Package: CvasMcu
 * @par		Project: CVAS Generator
 * @par		Copyright (c) 2016, Uptake Medical, Inc.
 * @author	Kurt Jan 12, 2012 Created file
 * @brief	This file contains unit tests for CTreatmentRecord
 */

// Include Files
#include "TreatmentRecordTest.h"

// External Public Data

// File Scope Macros and Constants

// File Scope Type Definitions (Enums, Structs & Classes)

// File Scope Data
static CTreatmentRecordTest f_TreatmentRecordTest; // Instantiate one so it registers itself

// File Scope Functions

// Public Functions

/**
 * @brief	This is the constructor for CTreatmentRecordTest
 *
 */

CTreatmentRecordTest::CTreatmentRecordTest() :
	CMcUnit<CTreatmentRecordTest> ( this ), CTestMain( (CUnitTestIntfc*) this )
{
}


/**
 * @brief	This method calls into CMcUnit to have the tests run.
 *
 * @return   Always 1
 */
int CTreatmentRecordTest::CommonTestHandle()
{
	McUnitSuiteRun( &sMcUnitTestSuite );
	McUnitSuiteReport( &sMcUnitTestSuite, DEPTH_TEST_REPORT );
	return 1;
}

/**
 * @brief	This method sets up the lists of tests to run
 *
 */
void CTreatmentRecordTest::UnitTestListSetup()
{

	int iList = 0;
	int iCase = 0;

	// Set up Case 1: Invocation Tests
	ADD_TEST_CASE( sMcUnitCase, iCase, sMcUnitTestList + iList, 1, "Invocation Tests", "Invocation Test failure")
	ADD_TEST_LIST( sMcUnitTestList, iList, &CTreatmentRecordTest::AllPublicMethods_Invocation, "All Public Methods invocation", "unexpected result" )

	// Set up Case 2: Parameter Tests
	ADD_TEST_CASE( sMcUnitCase, iCase, sMcUnitTestList + iList, 1, "Parameter Tests", "Parameter Test failure")
	ADD_TEST_LIST( sMcUnitTestList, iList, &CTreatmentRecordTest::UpdateTreatmentRecord_Parameter, "UpdateTreatmentRecord() parameter", "unexpected result" )

	// Set up Case 3: Interaction Tests
	ADD_TEST_CASE( sMcUnitCase, iCase, sMcUnitTestList + iList, 0, "Interaction Tests (No tests defined)", "")
	//Delete the preceding line and uncomment the two lines below to begin adding Interaction Tests
	//ADD_TEST_CASE( sMcUnitCase, iCase, sMcUnitTestList + iList, 1, "Interaction Tests", "Interaction Test failure")
	//ADD_TEST_LIST( sMcUnitTestList, iList, &CTreatmentRecordTest::<Name>_Interaction, "<Name> Interaction", "unexpected result" )

	MAKE_TEST_SUITE( sMcUnitTestSuite, sMcUnitCaseList, sMcUnitCase, iCase, "CTreatmentRecordTest" )

}

//=============================================================================
// CASE 1: INVOCATION TESTS
//=============================================================================

/**
 * @brief	This method tests the invocation of all public methods with valid parameters.
 *
 * @return  result as an int: 0 (UNIT_TEST_PASSED) if passed, not 0 if failed
 */
MCUNIT_TEST( CTreatmentRecordTest, AllPublicMethods_Invocation )
{
 	int iResult = UNIT_TEST_PASSED;
 	int	iInvocations = 0;

 	// TODO should void functions be changed?
	// Invoke and check return codes (if there is one)
 	g_pTreatmentRecord->StartNewRecord(); //	Void function; no return values to verify
	iResult |= McUnitAssertIsEqual( 0, 0, "void StartNewRecord()" );
	iInvocations++;

	g_pTreatmentRecord->NewEntry(); //	Void function; no return values to verify
	iResult |= McUnitAssertIsEqual( 0, 0, "void NewEntry()" );
	iInvocations++;

	// First record ID should be 1
	iResult |= McUnitAssertIsEqual( (byte)1, g_pTreatmentRecord->GetEntry(), "GetEntry()" );
	iInvocations++;

	iResult |= McUnitAssertIsEqual( OK, g_pTreatmentRecord->UpdateTreatmentInfoMessage(), "UpdateTreatmentInfoMessage()" );
	iInvocations++;

	g_pTreatmentRecord->SetRecordTreatmentCount( (twobytes)11 ); //	Void function; no return values to verify
	iResult |= McUnitAssertIsEqual( 0, 0, "void SetRecordTreatmentCount()" );
	iInvocations++;

	// Read back value stored by set
	iResult |= McUnitAssertIsEqual( (twobytes)11, g_pTreatmentRecord->GetRecordTreatmentCount(), "GetRecordTreatmentCount()" );
	iInvocations++;

	g_pTreatmentRecord->SetInitiatedTreatmentCount( (byte)22 ); //	Void function; no return values to verify
	iResult |= McUnitAssertIsEqual( 0, 0, "void SetInitiatedTreatmentCount()" );
	iInvocations++;

	// Read back value stored by set
	iResult |= McUnitAssertIsEqual( (twobytes)22, g_pTreatmentRecord->GetInitiatedTreatmentCount(), "GetInitiatedTreatmentCount()" );
	iInvocations++;

	g_pTreatmentRecord->SetLastTreatmentStatus( DELIVERY_FULL ); //	Void function; no return values to verify
	iResult |= McUnitAssertIsEqual( 0, 0, "void SetLastTreatmentStatus()" );
	iInvocations++;

	// Read back value stored by set
	iResult |= McUnitAssertIsEqual( DELIVERY_FULL, g_pTreatmentRecord->GetLastTreatmentStatus(), "GetLastTreatmentStatus()" );
	iInvocations++;

	g_pTreatmentRecord->SetLastTreatmentTime( (twobytes)1111 ); //	Void function; no return values to verify
	iResult |= McUnitAssertIsEqual( 0, 0, "void SetLastTreatmentTime()" );
	iInvocations++;

	// Read back value stored by set
	iResult |= McUnitAssertIsEqual( (twobytes)1111, g_pTreatmentRecord->GetLastTreatmentTime(), "GetLastTreatmentTime()" );
	iInvocations++;

	g_pTreatmentRecord->SetTherapyTime( (unsignedfourbytes)2222 ); //	Void function; no return values to verify
	iResult |= McUnitAssertIsEqual( 0, 0, "void SetTherapyTime()" );
	iInvocations++;

	// Read back value stored by set
	iResult |= McUnitAssertIsEqual( (unsignedfourbytes)2222, g_pTreatmentRecord->GetTherapyTime(), "GetTherapyTime()" );
	iInvocations++;

	iResult |= McUnitAssertIsEqual( OK, g_pTreatmentRecord->UpdateTreatmentRecord( DELIVERY_PARTIAL_ERROR, (twobytes)3333 ), "UpdateTreatmentRecord()" );
	iInvocations++;
	//	TODO UpdateTreatmentRecord() is under private methods in the .cpp file, but it is public

	LogOneValue("Number of methods tested: %d", iInvocations);

	return iResult;
}


//=============================================================================
// CASE 2: PARAMETER TESTS
//=============================================================================

/**
 * @brief	This method tests the passed parameter(s) of the UpdateTreatmentRecord() method.
 * 			It calls the method for a full and partial treatment, and retrieves and checks
 * 			member variables to verify internal processing.
 *
 * @return  result as an int: 0 (UNIT_TEST_PASSED) if passed, not 0 if failed
 */
MCUNIT_TEST( CTreatmentRecordTest, UpdateTreatmentRecord_Parameter )
{
#define FULL_TREATMENT ( 30000 )
#define PARTIAL_TREATMENT ( 6750 )
#define PRIME_TIME ( 10000 )
#define PARTIAL_ROUNDED ( PARTIAL_TREATMENT + 50 )
	int iResult = UNIT_TEST_PASSED;
 	upDeliveryStatus eTreatStatus;
 	byte bRecord;
 	twobytes tbTreatCount, tbInitiatedCount;
 	twobytes tbLastTreatTime;
 	unsignedfourbytes ulTherapyTime;

 	// Get a baseline of stored data
 	bRecord = g_pTreatmentRecord->GetEntry();
	tbTreatCount = g_pTreatmentRecord->GetRecordTreatmentCount();
	tbInitiatedCount = g_pTreatmentRecord->GetInitiatedTreatmentCount();
	eTreatStatus = g_pTreatmentRecord->GetLastTreatmentStatus();
	tbLastTreatTime = g_pTreatmentRecord->GetLastTreatmentTime();
	ulTherapyTime = g_pTreatmentRecord->GetTherapyTime();

	// Update a therapy record based on a full run of 6.750 seconds
	// Expect record, treatment count, and initiated count all to be bumped
	// Expect treatment time to be 8.7 seconds, and therapy time to be bumped by 8.7 seconds
	// Expect treatment type to be full treatment
	iResult |= McUnitAssertIsEqual( OK, g_pTreatmentRecord->UpdateTreatmentRecord( DELIVERY_FULL, (twobytes)(PARTIAL_TREATMENT) ), "Update with full run of 6.666 seconds" );

	iResult |= McUnitAssertIsEqual( (twobytes)(tbTreatCount+1), g_pTreatmentRecord->GetRecordTreatmentCount(), "Full treatment count" );

	iResult |= McUnitAssertIsEqual( (twobytes)(tbInitiatedCount+1), g_pTreatmentRecord->GetInitiatedTreatmentCount(), "Initiated treatment count" );

	iResult |= McUnitAssertIsEqual( (byte)(bRecord+1), g_pTreatmentRecord->GetEntry(), "Record number" );

	iResult |= McUnitAssertIsEqual( DELIVERY_FULL, g_pTreatmentRecord->GetLastTreatmentStatus(), "Treatment type" );

	iResult |= McUnitAssertIsEqual( (twobytes)FULL_TREATMENT, g_pTreatmentRecord->GetLastTreatmentTime(), "Treatment time" );

	iResult |= McUnitAssertIsEqual( (unsignedfourbytes)(ulTherapyTime+FULL_TREATMENT), g_pTreatmentRecord->GetTherapyTime(), "Therapy time" );

	// Update a therapy record based on a partial run of 6.750 seconds
	// Expect record and initiated count to be bumped, but not treatment count
	// Expect treatment time to be 6.750 seconds, and therapy time to be bumped by 50 milliseconds to meet 0.1 second resolution
	// Expect treatment type to be partial treatment
	iResult |= McUnitAssertIsEqual( OK, g_pTreatmentRecord->UpdateTreatmentRecord( DELIVERY_PARTIAL_ERROR, (twobytes)PARTIAL_TREATMENT ), "Update with partial run of 6.666 seconds" );

	iResult |= McUnitAssertIsEqual( (twobytes)(tbTreatCount+1), g_pTreatmentRecord->GetRecordTreatmentCount(), "Full treatment count" );

	iResult |= McUnitAssertIsEqual( (twobytes)(tbInitiatedCount+1+1), g_pTreatmentRecord->GetInitiatedTreatmentCount(), "Initiated treatment count" );

	iResult |= McUnitAssertIsEqual( (byte)(bRecord+1+1), g_pTreatmentRecord->GetEntry(), "Record number" );

	iResult |= McUnitAssertIsEqual( DELIVERY_PARTIAL_ERROR, g_pTreatmentRecord->GetLastTreatmentStatus(), "Treatment type" );

	iResult |= McUnitAssertIsEqual( (twobytes)(PARTIAL_ROUNDED), g_pTreatmentRecord->GetLastTreatmentTime(), "Treatment time" );

	iResult |= McUnitAssertIsEqual( (unsignedfourbytes)(ulTherapyTime+PARTIAL_ROUNDED+FULL_TREATMENT), g_pTreatmentRecord->GetTherapyTime(), "Therapy time" );

	// Update a therapy record based on a vapor priming time run of 10 seconds
	// Expect record to be bumped, but not initiated or full treatment count
	// Expect treatment time to be 10 seconds, and therapy time to be bumped by 10 seconds (truncated)
	// Expect treatment type to be prime
	iResult |= McUnitAssertIsEqual( OK, g_pTreatmentRecord->UpdateTreatmentRecord( DELIVERY_PRIME, (twobytes)PRIME_TIME ), "Update with prime run of 10 seconds" );

	iResult |= McUnitAssertIsEqual( (twobytes)(tbTreatCount+1), g_pTreatmentRecord->GetRecordTreatmentCount(), "Full treatment count" );

	iResult |= McUnitAssertIsEqual( (twobytes)(tbInitiatedCount+1+1), g_pTreatmentRecord->GetInitiatedTreatmentCount(), "Initiated treatment count" );

	iResult |= McUnitAssertIsEqual( (byte)(bRecord+1+1+1), g_pTreatmentRecord->GetEntry(), "Record number" );

	iResult |= McUnitAssertIsEqual( DELIVERY_PRIME, g_pTreatmentRecord->GetLastTreatmentStatus(), "Treatment type" );

	iResult |= McUnitAssertIsEqual( (twobytes)(PRIME_TIME), g_pTreatmentRecord->GetLastTreatmentTime(), "Treatment time" );

	iResult |= McUnitAssertIsEqual( (unsignedfourbytes)(ulTherapyTime+PRIME_TIME+PARTIAL_ROUNDED+FULL_TREATMENT), g_pTreatmentRecord->GetTherapyTime(), "Therapy time" );


	return iResult;
}


//=============================================================================
// CASE 3: INTERACTION TESTS
//=============================================================================

/**
 * @brief	This method tests the interaction of TBD.
 *
 * @return  result as an int: 0 (UNIT_TEST_PASSED) if passed, not 0 if failed
 */

//MCUNIT_TEST( CTreatmentRecordTest, <Name>_Interaction )
//{
//}


/**
 * @file	TreatmentRecordTest.h
 * @par		Package: CvasMcu
 * @par		Project: CVAS Generator
 * @par		Copyright (c) 2016, Uptake Medical, Inc.
 * @author	Kurt Jan 12, 2012 Created file
 * @brief	This file contains unit tests for CTreatmentRecord
 */

#ifndef TreatmentRecordTEST_H_
#define TreatmentRecordTEST_H_

// Include Files
#include <TreatmentRecord.h>
#include "McUnit.h"
#include "UnitTestIntfc.h"
#include "TestMain.h"

// Referenced classes
#include <SbcIntfc.h>
#include <OneWireMemory.h>
#include <TreatmentInformation.h>

// Public Macros and Constants

// Public Type Definitions (Enums, Structs & Classes)

/**
 * @class	CTreatmentRecordTest
 * @brief	This class provides unit tests for CTreatmentRecord
 *
 * Virtual Functions Overridden: @n@n
 *  UnitTestListSetup - Set up list of Suites, Cases and Tests to be execute @n
 * 	CommonTesthandle - Called to have tests run @n
 *
 * External Data Members: None @n@n
 *
 */


class CTreatmentRecordTest: private CMcUnit<CTreatmentRecordTest>, public CUnitTestIntfc, public CTestMain, private CTreatmentRecord
{
public:
	CTreatmentRecordTest();
	int CommonTestHandle();
	void UnitTestListSetup();
	// Case 1: Invocation Tests
	MCUNIT_TEST_DEC( AllPublicMethods_Invocation );
	// Case 2: Parameter Tests
	MCUNIT_TEST_DEC( UpdateTreatmentRecord_Parameter );
	// Case 3: Interaction Tests
	//MCUNIT_TEST_DEC( <Name>_Interaction );

	SMcUnitTest sMcUnitTestList[2];	// Increment index for each test added, regardless of the case in which it falls
	SMcUnitCase sMcUnitCase[3];
	SMcUnitCase * sMcUnitCaseList[4];
	SMcUnitSuite sMcUnitTestSuite;
};

// Inline Functions (follows class definition)


#endif /* TreatmentRecordTEST_H_ */

/**
 * @file	HandpiecePrimeTest.h
 * @par		Package: CvasMcu
 * @par		Project: CVAS Generator
 * @par		Copyright (c) 2016, Uptake Medical, Inc.
 * @author	Kurt Jan 12, 2012 Created file
 * @brief	This file contains unit tests for CHandpiecePrime
 */

#ifndef HANDPIECEPRIMETEST_H_
#define HANDPIECEPRIMETEST_H_

// Include Files
#include <HandpiecePrime.h>
#include "McUnit.h"
#include "UnitTestIntfc.h"
#include "TestMain.h"
#include "SetupHelpers.h"

// Referenced classes
#include <TreatmentInformation.h>
#include <WaterCntl.h>
#include <DigitalInputOutput.h>
#include <GeneratorMonitor.h>
#include <RFGenerator.h>
#include <TreatmentRecord.h>

// Public Macros and Constants

// Public Type Definitions (Enums, Structs & Classes)

/**
 * @class	CHandpiecePrimeTest
 * @brief	This class provides unit tests for CHandpiecePrime
 *
 * Virtual Functions Overridden: @n@n
 *  UnitTestListSetup - Set up list of Suites, Cases and Tests to be execute @n
 * 	CommonTesthandle - Called to have tests run @n
 *
 * External Data Members: None @n@n
 *
 */
class CHandpiecePrimeTest: private CMcUnit<CHandpiecePrimeTest> , public CUnitTestIntfc, public CTestMain,
        private CHandpiecePrime
{

	CSetupHelpers m_setupHelpers;

public:
	CHandpiecePrimeTest();
	virtual ~CHandpiecePrimeTest(){}
	int CommonTestHandle();
	void UnitTestListSetup();
	// Case 1: Invocation Tests
	MCUNIT_TEST_DEC( AllPublicMethods_Invocation );
	// Case 2: Parameter Tests
	//MCUNIT_TEST_DEC( <Method>_Parameter );
	// Case 3: Interaction Tests
	MCUNIT_TEST_DEC( Setup_for_Interaction );
	MCUNIT_TEST_DEC( HandpiecePrime_FullRun );

	SMcUnitTest sMcUnitTestList[ 8 ]; // Increment index for each test added, regardless of the case in which it falls
	SMcUnitCase sMcUnitCase[ 5 ];
	SMcUnitCase * sMcUnitCaseList[ 5 ];
	SMcUnitSuite sMcUnitTestSuite;
};

// Inline Functions (follows class definition)


#endif /* HANDPIECEPRIMETEST_H_ */

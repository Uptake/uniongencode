/**
 * @file	HandpiecePrimeTest.cpp
 * @par		Package: CvasMcu
 * @par		Project: CVAS Generator
 * @par		Copyright (c) 2016, Uptake Medical, Inc.
 * @author	Kurt Jan 12, 2012 Created file
 * @brief	This file contains unit tests for CHandpiecePrime
 */

// Include Files
#include "HandpiecePrimeTest.h"
#include "ObjectSingletons.h"
#include "Temperature.h"
#include "GeneratorMonitor.h"
#include "TreatmentRecord.h"

// External Public Data

// File Scope Macros and Constants
#define DATA_FILE_LOC "DataFiles/"

// File Scope Type Definitions (Enums, Structs & Classes)

// File Scope Data
static CHandpiecePrimeTest f_HandpiecePrimeTest; // Instantiate one so it registers itself


// File Scope Functions

// Public Functions

/**
 * @brief	This is the constructor for CHandpiecePrimeTest
 *
 */

CHandpiecePrimeTest::CHandpiecePrimeTest() :
	CMcUnit<CHandpiecePrimeTest> ( this ), CTestMain( (CUnitTestIntfc*) this )
{
}

/**
 * @brief	This method calls into CMcUnit to have the tests run.
 *
 * @return   Always 1
 */
int CHandpiecePrimeTest::CommonTestHandle()
{
	McUnitSuiteRun( &sMcUnitTestSuite );
	McUnitSuiteReport( &sMcUnitTestSuite, DEPTH_TEST_REPORT );
	return 1;
}

/**
 * @brief	This method sets up the lists of tests to run
 *
 */
void CHandpiecePrimeTest::UnitTestListSetup()
{

	int iList = 0;
	int iCase = 0;

	// Set up Case 1: Invocation Tests
	ADD_TEST_CASE( sMcUnitCase, iCase, sMcUnitTestList + iList, 1, "Invocation Tests", "Invocation Test failure")
	ADD_TEST_LIST( sMcUnitTestList, iList, &CHandpiecePrimeTest::AllPublicMethods_Invocation, "All Public Methods invocation", "unexpected result" )

	// Set up Case 2: Parameter Tests
	ADD_TEST_CASE( sMcUnitCase, iCase, sMcUnitTestList + iList, 0, "Parameter Tests (No tests defined)", "")
	//Delete the preceding line and uncomment the two lines below to begin adding Parameter Tests
	//ADD_TEST_CASE( sMcUnitCase, iCase, sMcUnitTestList + iList, 1, "Parameter Tests", "Parameter Test failure")
	//ADD_TEST_LIST( sMcUnitTestList, iList, &CHandpiecePrimeTest::<Method>_Parameter, "<Method>() parameter", "unexpected result" )

	// Set up Case 3: Interaction Tests
	ADD_TEST_CASE( sMcUnitCase, iCase, sMcUnitTestList + iList, 2, "Interaction Tests", "")
	ADD_TEST_LIST( sMcUnitTestList, iList, &CHandpiecePrimeTest::Setup_for_Interaction, "Setup_for_Interaction", "unexpected result" )
	ADD_TEST_LIST( sMcUnitTestList, iList, &CHandpiecePrimeTest::HandpiecePrime_FullRun, "HandpiecePrime_FullRun", "unexpected result" )

	MAKE_TEST_SUITE( sMcUnitTestSuite, sMcUnitCaseList, sMcUnitCase, iCase, "CHandpiecePrimeTest" )

}

//=============================================================================
// CASE 1: INVOCATION TESTS
//=============================================================================

/**
 * @brief	This method tests the invocation of all public methods with valid parameters.
 *
 * @return  result as an int: 0 (UNIT_TEST_PASSED) if passed, not 0 if failed
 */
MCUNIT_TEST( CHandpiecePrimeTest, AllPublicMethods_Invocation )
{
	int iResult = UNIT_TEST_PASSED;
	byte bProgress;
	int iInvocations = 0;

	// Pull in shared objects
	iResult |= McUnitAssertIsOK( g_timeBase.InitializeHardware(), "g_timeBase.InitializeHardware", false );

	// Invoke and check return codes (if there is one)
	iResult |= McUnitAssertIsEqual( PRIME_START, CHandpiecePrime::GetState(), "GetState()" );
	iInvocations++;

	iResult |= McUnitAssertIsOK( InitializePrime(), "StartPrime()" );
	iInvocations++;

	// Expect interlock error when initial conditions have not been established
	iResult |= McUnitAssertIsEqual( ERR_INTERLOCK_MISSING, PerformPrime(), "PerformPrime()" );
	iInvocations++;

	// Expect no progress
	iResult |= McUnitAssertIsOK( GetProgress( &bProgress ), "GetProgress()" );
	iResult |= McUnitAssertIsEqual( (byte) 0, bProgress, "Progress value" );
	iInvocations++;

	LogOneValue( "Number of methods tested: %d", iInvocations );

	m_setupHelpers.SbcSendAllLogs();

	return iResult;
}

//=============================================================================
// CASE 2: PARAMETER TESTS
//=============================================================================

/**
 * @brief	This method tests the passed parameter(s) of the <Method>() method.
 *
 * @return  result as an int: 0 (UNIT_TEST_PASSED) if passed, not 0 if failed
 */

//MCUNIT_TEST( CHandpiecePrimeTest, <Method>_Parameter )
//{
//}


//=============================================================================
// CASE 3: INTERACTION TESTS
//=============================================================================

/**
 * @brief	This method tests the interaction of TBD.
 *
 * @return  result as an int: 0 (UNIT_TEST_PASSED) if passed, not 0 if failed
 */

MCUNIT_TEST( CHandpiecePrimeTest, Setup_for_Interaction )
{
	int iResult = UNIT_TEST_PASSED;

	iResult |= McUnitAssertIsOK( g_timeBase.InitializeHardware(), "g_timeBase.InitializeHardware" );
	iResult |= McUnitAssertIsOK( g_pTemperature->InitializeHardware(), "g_pTemperature->InitializeHardware" );
	iResult |= McUnitAssertIsOK( g_pWaterCntl->InitializeHardware(), "g_pWaterCntl->InitializeHardware" );
	iResult |= McUnitAssertIsOK( g_pDigitalInputOutput->InitializeHardware(),
	        "g_pDigitalInputOutput->InitializeHardware" );
	iResult |= McUnitAssertIsEqual( UNIT_TEST_PASSED, m_setupHelpers.SbcInitAndConnect(),
	        "m_setupHelpers.SbcInitAndConnect" );

	m_setupHelpers.SbcSendAllLogs();

	return iResult;
}

/**
 * @brief	This method tests the interaction of TBD.
 *
 * @return  result as an int: 0 (UNIT_TEST_PASSED) if passed, not 0 if failed
 */

MCUNIT_TEST( CHandpiecePrimeTest, HandpiecePrime_FullRun )
{
	int iResult = UNIT_TEST_PASSED;
	byte bProgress;
	twobytes i = 0;
	PrimeStepEnum eState;
	tick tFirst;

	iResult |= McUnitAssertIsEqual( UNIT_TEST_PASSED, m_setupHelpers.InterlockAll(), "InterlockAll", true );
	iResult |= McUnitAssertIsEqual( UNIT_TEST_PASSED, f_simDataAccess.LoadDataFile( DATA_FILE_LOC"Priming.csv", true ),
	        "LoadDataFile" );
	iResult |= McUnitAssertIsOK( InitializePrime(), "StartPrime" );

	eState = GetState();
	iResult |= McUnitAssertIsEqual( PRIME_START, eState, "GetState", false );
	m_setupHelpers.GetTick( &tFirst );

	// Run priming
	do
		{
		tFirst += RND_ROBIN_TICK_CNT;
		m_setupHelpers.SetTick( tFirst );
		iResult |= McUnitAssertIsOK( f_simDataAccess.GoToNextRecord(), "GoToNextRecord", false );
		iResult |= McUnitAssertIsEqual( UNIT_TEST_PASSED, m_setupHelpers.ReadSimDataRecord(), "ReadSimDataRecord",
		        false );
		iResult |= McUnitAssertIsOK( PerformPrime(), "PerformPrime", false );
		iResult |= McUnitAssertIsOK( g_pWaterCntl->WriteToHardware(), "g_pWaterCntl->WriteToHardware", false );
		eState = GetState();
		iResult |= McUnitAssertIsOK( GetProgress( &bProgress ), "GetProgress", false );
		iResult |= McUnitAssertIsEqual( UNIT_TEST_PASSED,
		        m_setupHelpers.SbcSendAllLogs( OPER_STATE_HAND_PRIMING, bProgress ), "SbcSendAllLogs", false );
		}
	while ( i++ < f_simDataAccess.GetSampleCount() && eState != PRIME_SUCCESS && eState != PRIME_FAILED && eState
	        != PRIME_ERROR );

	iResult |= McUnitAssertIsEqual( PRIME_SUCCESS, eState, "GetState", false );
	iResult |= McUnitAssertIsEqual( (twobytes) 10000, g_pTreatmentRecord->GetLastTreatmentTime(), "GetLastTreatmentTime" );
	iResult |= McUnitAssertIsOK( GetProgress( &bProgress ), "GetProgress" );
	iResult |= McUnitAssertIsEqual( (byte) 100, bProgress, "bProgress" );

	iResult |= McUnitAssertIsEqual( UNIT_TEST_PASSED, m_setupHelpers.SbcSendAllLogs(), "SbcSendAllLogs", false );

	return iResult;
}


/**
 * @file	CommonOperationsTest.h
 * @par		Package: CvasMcu
 * @par		Project: CVAS Generator
 * @par		Copyright (c) 2016, Uptake Medical, Inc.
 * @author	Kurt Jan 12, 2012 Created file
 * @brief	This file contains unit tests for CCommonOperations
 */

#ifndef COMMONOPERATIONSTEST_H_
#define COMMONOPERATIONSTEST_H_

// Include Files
#include <CommonOperations.h>
#include "McUnit.h"
#include "UnitTestIntfc.h"
#include "TestMain.h"
#include "GenMonTest.h"
#include "SetupHelpers.h"

// Referenced classes
#include <GeneratorMonitor.h>
#include <RFGenerator.h>
#include <WaterCntl.h>
#include <SbcIntfc.h>
#include <TimeBase.h>

// Public Macros and Constants

// Public Type Definitions (Enums, Structs & Classes)

/**
 * @class	CCommonOperationsTest
 * @brief	This class provides unit tests for CCommonOperations
 *
 * Virtual Functions Overridden: @n@n
 *  UnitTestListSetup - Set up list of Suites, Cases and Tests to be execute @n
 * 	CommonTesthandle - Called to have tests run @n
 *
 * External Data Members: None @n@n
 *
 */


class CCommonOperationsTest: public CTimeBase, private CMcUnit<CCommonOperationsTest>, public CUnitTestIntfc, public CTestMain, private CCommonOperations
{
public:
	CCommonOperationsTest();
	virtual ~CCommonOperationsTest(){}
	int CommonTestHandle();
	void UnitTestListSetup();
	// Case 1: Invocation Tests
	MCUNIT_TEST_DEC( AllPublicMethods_Invocation );
	// Case 2: Parameter Tests
	//MCUNIT_TEST_DEC( <Method>_Parameter );
	// Case 3: Interaction Tests
    MCUNIT_TEST_DEC( HaltPump_Interaction );
    MCUNIT_TEST_DEC( InitiateVaporImmediately_Interaction );
    MCUNIT_TEST_DEC( ConstantVaporFlow_Interaction );
private:
    SHdwSimData m_sHdwSimData;
    upCvasErr LocalRoundRobin( twobytes tbNumLoops );
    void SetStandardTemps();
    void ResetSensorData();
    CSetupHelpers m_setupHelpers;
public:
	SMcUnitTest sMcUnitTestList[4];	// Increment index for each test added, regardless of the case in which it falls
	SMcUnitCase sMcUnitCase[3];
	SMcUnitCase * sMcUnitCaseList[4];
	SMcUnitSuite sMcUnitTestSuite;
	CGenMonTest cGenMonTest;
};


// Inline Functions (follows class definition)


#endif /* COMMONOPERATIONSTEST_H_ */

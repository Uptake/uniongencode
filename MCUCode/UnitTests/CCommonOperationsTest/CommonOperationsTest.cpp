/**
 * @file	CommonOperationsTest.cpp
 * @par		Package: CvasMcu
 * @par		Project: CVAS Generator
 * @par		Copyright (c) 2016, Uptake Medical, Inc.
 * @author	Kurt Jan 12, 2012 Created file
 * @brief	This file contains unit tests for CCommonOperations
 */

// Include Files

#include "CommonOperationsTest.h"
#include "Temperature.h"
#include "string.h"
#include "stdio.h"
// External Public Data
// File Scope Macros and Constants

// File Scope Type Definitions (Enums, Structs & Classes)

// File Scope Data
static CCommonOperationsTest f_CommonOperationsTest; // Instantiate one so it registers itself

// File Scope Functions

// Public Functions

/**
 * @brief	This is the constructor for CCommonOperationsTest
 *
 */

CCommonOperationsTest::CCommonOperationsTest() :
	CMcUnit<CCommonOperationsTest> ( this ), CTestMain( (CUnitTestIntfc*) this )
{
	// Point to the mock
	g_pGeneratorMonitor = &cGenMonTest;
}

/**
 * @brief	This method calls into CMcUnit to have the tests run.
 *
 * @return   Always 1
 */
int CCommonOperationsTest::CommonTestHandle()
{
	McUnitSuiteRun( &sMcUnitTestSuite );
	McUnitSuiteReport( &sMcUnitTestSuite, DEPTH_TEST_REPORT );
	return 1;
}

/**
 * @brief	This method sets up the lists of tests to run
 *
 */
void CCommonOperationsTest::UnitTestListSetup()
{

	int iList = 0;
	int iCase = 0;

	// Set up Case 1: Invocation Tests
	ADD_TEST_CASE( sMcUnitCase, iCase, sMcUnitTestList + iList, 1, "Invocation Tests", "Invocation Test failure")
	ADD_TEST_LIST( sMcUnitTestList, iList, &CCommonOperationsTest::AllPublicMethods_Invocation, "All Public Methods invocation", "unexpected result" )

	// Set up Case 2: Parameter Tests
	ADD_TEST_CASE( sMcUnitCase, iCase, sMcUnitTestList + iList, 0, "Parameter Tests (No tests defined)", "")
	//Delete the preceding line and uncomment the two lines below to begin adding Parameter Tests
	//ADD_TEST_CASE( sMcUnitCase, iCase, sMcUnitTestList + iList, 1, "Parameter Tests", "Parameter Test failure")
	//ADD_TEST_LIST( sMcUnitTestList, iList, &CCommonOperationsTest::<Method>_Parameter, "<Method>() parameter", "unexpected result" )

	// Set up Case 3: Interaction Tests
	ADD_TEST_CASE( sMcUnitCase, iCase, sMcUnitTestList + iList, 3, "Interaction Tests", "Interaction Test failure")
	ADD_TEST_LIST( sMcUnitTestList, iList, &CCommonOperationsTest::HaltPump_Interaction, "HaltPump_Interaction", "unexpected result" )
	ADD_TEST_LIST( sMcUnitTestList, iList, &CCommonOperationsTest::InitiateVaporImmediately_Interaction, "InitiateVaporImmediately_Interaction", "unexpected result" )
	ADD_TEST_LIST( sMcUnitTestList, iList, &CCommonOperationsTest::ConstantVaporFlow_Interaction, "ConstantVaporFlow_Interaction", "unexpected result" )

	MAKE_TEST_SUITE( sMcUnitTestSuite, sMcUnitCaseList, sMcUnitCase, iCase, "CCommonOperationsTest" )

}

//=============================================================================
// CASE 1: INVOCATION TESTS
//=============================================================================

/**
 * @brief	This method tests the invocation of all public methods with valid parameters.
 *
 * @return  result as an int: 0 (UNIT_TEST_PASSED) if passed, not 0 if failed
 */
MCUNIT_TEST( CCommonOperationsTest, AllPublicMethods_Invocation )
{
#define MINIMUM_MCU_VERSION_STRING_LENGTH	3	// Format is V_mmm dd yyyy_hh:mm:ss
#define	MINIMUM_SYSTEM_VERSION_STRING_LENGTH 3	// Initialized to "unset"
	int iResult = UNIT_TEST_PASSED;
	int iInvocations = 0;
	char versionStr[ 30 ] = "NO VERSION HAS BEEN SET";

	// Test setup
	cGenMonTest.ResetState();

	// Invoke and check return codes (if there is one)
	iResult |= McUnitAssertIsEqual( GEN_STATE_START, GetSubState(), "GetSubState" );
	iInvocations++;

	// Can't verify the value of the version, so just confirm the expected length
	strcpy( versionStr, GetMcuVersion() );
	iResult |= McUnitAssertTrue( MINIMUM_MCU_VERSION_STRING_LENGTH < (int) strlen( versionStr ),
	        "GetMcuVersion() string length" );
	iInvocations++;

	// Can't verify the value of the version, so just confirm the expected length
	strcpy( versionStr, GetSystemVersion() ); // TODO Does this get set somewhere?
	iResult |= McUnitAssertTrue( MINIMUM_SYSTEM_VERSION_STRING_LENGTH < (int) strlen( versionStr ),
	        "GetSystemVersion() string length" );
	iInvocations++;

	iResult |= McUnitAssertIsOK( SetStartState(), "SetStartState" );
	iInvocations++;

	// Expect an error code when the initial conditions are not set up
	iResult |= McUnitAssertIsOK( InitiateVaporImmediately( 0, 0 ), "InitiateVaporImmediately" );
	iInvocations++;

	iResult |= McUnitAssertIsOK( HaltPumpAndRF(), "HaltPumpAndRF" );
	iInvocations++;

	iResult |= McUnitAssertIsOK( GetRtcVersions(), "GetRtcVersions" );
	iInvocations++;

	// Expect a version mismatch
	iResult |= McUnitAssertIsEqual( ERR_VERSION_CHECK, VerifyCodeVersions(), "VerifyCodeVersions" );
	iInvocations++;


	LogOneValue( "Number of methods tested: %d", iInvocations );

	return iResult;
}

//=============================================================================
// CASE 2: PARAMETER TESTS
//=============================================================================


//=============================================================================
// CASE 3: INTERACTION TESTS
//=============================================================================

/**
 * @brief	This method tests the interaction of the HaltPumpAndRf method and
 * 			the pump and RF generator controller classes.
 *
 * @return  result as an int: 0 (UNIT_TEST_PASSED) if passed, not 0 if failed
 */

MCUNIT_TEST( CCommonOperationsTest, HaltPump_Interaction )
{
#define INIT_VELOCITY ( 5000 )
	long int pliVelocity = 0;
	int iResult = UNIT_TEST_PASSED;
	bool_c bPbOn = false;

	// Test setup
	cGenMonTest.ResetState();

	// Set the target velocity to some value > 0
	iResult
	        |= McUnitAssertIsOK( g_pWaterCntl->SetTargetVelocity( INIT_VELOCITY ), "SetTargetVelocity( INIT_VELOCITY )" );

	// Assert that the target velocity is set
	iResult |= McUnitAssertIsOK( g_pWaterCntl->GetTargetVelocity( &pliVelocity ), "GetTargetVelocity( &pliVelocity )" );
	iResult |= McUnitAssertTrue( pliVelocity > 0, "Set velocity > 0" );

	// Set and get the RF output as enabled.
	iResult |= McUnitAssertIsOK( g_pRFGen->SetOutputEnable( true ), "g_pRFGen->SetOutputEnable( true )" );

	// Ensure RF output is enabled
	iResult |= McUnitAssertIsOK( g_pRFGen->GetOutputEnable( &bPbOn ), "GetOutputEnable( &bPbOn )" );
	iResult |= McUnitAssertIsEqual( (bool_c) true, bPbOn, "Assert output enable: true" );

	// Halt the pump and disable RF
	iResult |= McUnitAssertIsOK( HaltPumpAndRF(), "HaltPumpAndRF" );

	// Ensure the RF output is disabled
	iResult |= McUnitAssertIsOK( g_pRFGen->GetOutputEnable( &bPbOn ), "GetOutputEnable( &bPbOn )" );
	iResult |= McUnitAssertIsEqual( (bool_c) false, bPbOn, "Assert output enable: false" );

	// Assert that the pump is halted
	iResult |= McUnitAssertIsOK( g_pWaterCntl->GetTargetVelocity( &pliVelocity ), "GetTargetVelocity( &pliVelocity )" );
	iResult |= McUnitAssertTrue( 0 == pliVelocity, "Assert that velocity == 0" );

	return iResult;
}

/**
 * @brief	This method tests the interaction of the HaltPumpAndRf method and
 * 			the pump and RF generator controller classes.
 *
 * @return  result as an int: 0 (UNIT_TEST_PASSED) if passed, not 0 if failed
 */
MCUNIT_TEST( CCommonOperationsTest, InitiateVaporImmediately_Interaction )
{
#define WRATE_TEST ( 1000 )
#define POWER_TEST ( 100 )
	int iResult = UNIT_TEST_PASSED;
	long int waterRate = 0;
	bool_c bPbOn = true;

	// Set the state and levels and make sure the changes are applied
	SetSubState( GEN_STATE_START );
	iResult |= McUnitAssertIsOK( InitiateVaporImmediately( WRATE_TEST, POWER_TEST ), "TIV5: OK returned" );
	iResult |= McUnitAssertIsEqual( GEN_STATE_COMPLETE, GetSubState(), "TIV5: GEN_STATE_COMPLETE == substate" );
	g_pRFGen->GetOutputEnable( &bPbOn );
	iResult |= McUnitAssertIsEqual( (bool_c) true, bPbOn, "TIV5: Assert RF output enable: true" );
	g_pWaterCntl->GetFlowRateUlPerMin( &waterRate );
	iResult |= McUnitAssertIsEqual( (long int) WRATE_TEST, waterRate, "TIV5: Water rate set" );
	iResult |= McUnitAssertIsEqual( (twobytes) 100, g_pRFGen->GetPowerRequest(), "TIV5: Power set" );

	return iResult;
}

/**
 * @brief	This method tests the interaction of the HaltPumpAndRf method and
 * 			the pump and RF generator controller classes.
 *
 * @return  result as an int: 0 (UNIT_TEST_PASSED) if passed, not 0 if failed
 */
MCUNIT_TEST( CCommonOperationsTest, ConstantVaporFlow_Interaction )
{
#define WRATE_TEST ( 1000 )
#define POWER_TEST ( 100 )
	int iResult = UNIT_TEST_PASSED;
	long int pliVelocity = 0;
	bool_c bPbOn = false;

	// Turn on RF
	g_pRFGen->SetOutputEnable( true );
	g_pWaterCntl->SetTargetVelocity( INIT_VELOCITY );

	// Test 3 : Procedure
	iResult |= McUnitAssertIsOK( g_pRFGen->GetOutputEnable( &bPbOn ), "TCV3: GetOutputEnable( &bPbOn )" );
	iResult |= McUnitAssertIsEqual( (bool_c) false, bPbOn, "TCV3: Assert output enable: false" );
	g_pWaterCntl->GetTargetVelocity( &pliVelocity );
	iResult |= McUnitAssertTrue( 0 == pliVelocity, "TCV3: Assert that velocity == 0" );

	// Test 4 : ADC read error: Return an internal fault

	// Test 4 : Setup
	cGenMonTest.ResetState();
	cGenMonTest.SetInterlocks( INTLCK_ALL );
	cGenMonTest.SetBool( true );
	SetSubState( GEN_STATE_START );

	SetStandardTemps();

	f_simDataAccess.SetSimData( &m_sHdwSimData );

	g_pWaterCntl->InitializeHardware();
	g_pTemperature->InitializeHardware();
	g_pWaterCntl->ReadFromHardware();

	// TODO: Comments are not clear concerning why some errors halt the pump and RF and why others do not
	// Test 4 : Procedure.  Note, this does not currently turn off RF
	iResult |= McUnitAssertIsEqual( ERR_INTERNAL_FAULT, ConstantVaporFlow( 0, 0 ), "TCV4: ERR_INTERNAL_FAULT returned" );

	// Test 5:
	cGenMonTest.ResetState();
	cGenMonTest.SetInterlocks( INTLCK_ALL );
	cGenMonTest.SetBool( true );
	SetSubState( GEN_STATE_START );
	m_sHdwSimData.m_unused0 = g_pTreatmentInfo->getDeliveryMin() - 1;

	f_simDataAccess.SetSimData( &m_sHdwSimData );
	g_pWaterCntl->ReadFromHardware();

	// Test 6 : Thermo error: coil undertemp

	// Test 6 : Setup
	cGenMonTest.ResetState();
	cGenMonTest.SetInterlocks( INTLCK_ALL );
	cGenMonTest.SetBool( true );
	SetSubState( GEN_STATE_START );
	SetStandardTemps();

	m_sHdwSimData.m_coilTemp = g_pTreatmentInfo->getCoilTempLower() - 1;
	f_simDataAccess.SetSimData( &m_sHdwSimData );
	// Multiple reads due to FIR
	g_pTemperature->ReadFromHardware();
	g_pTemperature->ReadFromHardware();
	g_pTemperature->ReadFromHardware();
	g_pTemperature->ReadFromHardware();

	// Test 6 : Procedure
	iResult |= McUnitAssertIsEqual( ERR_THERMO_UNDER_TEMP, ConstantVaporFlow( 0, 0 ),
	        "TCV6: ERR_THERMO_UNDER_TEMP returned (coil)" );

	// Test 7 : Thermo error: outlet undertemp

	// Test 7 : Setup
	cGenMonTest.ResetState();
	cGenMonTest.SetInterlocks( INTLCK_ALL );
	cGenMonTest.SetBool( true );
	SetSubState( GEN_STATE_START );
	SetStandardTemps();
	m_sHdwSimData.m_outletTemp = g_pTreatmentInfo->getOutletTempMin() - 1;
	f_simDataAccess.SetSimData( &m_sHdwSimData );
	// Multiple reads due to FIR
	g_pTemperature->ReadFromHardware();
	g_pTemperature->ReadFromHardware();
	g_pTemperature->ReadFromHardware();
	g_pTemperature->ReadFromHardware();

	// Test 7 : Procedure
	iResult |= McUnitAssertIsEqual( ERR_THERMO_UNDER_TEMP, ConstantVaporFlow( 0, 0 ),
	        "TCV7: ERR_THERMO_UNDER_TEMP returned (outlet)" );

	// Test 8 : Do not perform min checks unless ( tbElapsedTicks >= tbMinValueCheckTicks )

	// Test 8 : Setup
	cGenMonTest.ResetState();
	cGenMonTest.SetInterlocks( INTLCK_ALL );
	cGenMonTest.SetBool( true );
	SetSubState( GEN_STATE_START );
	SetStandardTemps();
	m_sHdwSimData.m_outletTemp = g_pTreatmentInfo->getOutletTempMin() - 1;
	f_simDataAccess.SetSimData( &m_sHdwSimData );
	// Multiple reads due to FIR
	g_pTemperature->ReadFromHardware();
	g_pTemperature->ReadFromHardware();
	g_pTemperature->ReadFromHardware();
	g_pTemperature->ReadFromHardware();

	// Test 8 : Procedure
	iResult |= McUnitAssertIsOK( ConstantVaporFlow( 0, 1 ), "TCV8: OK returned" );

	// Test 9: Thermo error: ADC error

	// Test 9 : Setup
	cGenMonTest.ResetState();
	cGenMonTest.SetInterlocks( INTLCK_ALL );
	cGenMonTest.SetBool( true );
	SetSubState( GEN_STATE_START );
	SetStandardTemps();
	m_sHdwSimData.m_outletTemp = g_pTreatmentInfo->getOutletTempMin() - 1;
	m_sHdwSimData.m_coilTemp = 0;
	f_simDataAccess.SetSimData( &m_sHdwSimData );
	// Multiple reads due to FIR
	g_pTemperature->ReadFromHardware();

	// Test 9 : Procedure
	iResult |= McUnitAssertIsEqual( ERR_THERMO_ERROR, ConstantVaporFlow( 1, 0 ), "TCV9: ERR_THERMO_ERROR returned" );

	// Test X: Go path

	// Test X: Test Setup
	cGenMonTest.ResetState();
	cGenMonTest.SetInterlocks( INTLCK_ALL );
	cGenMonTest.SetBool( true );
	SetSubState( GEN_STATE_START );
	SetStandardTemps();

	// Test X: Procedure
	iResult |= McUnitAssertIsOK( ConstantVaporFlow( 0, 0 ), "TCVX: OK returned" );

	return iResult;
}


/**
 * @brief	This method clears the temperatures to levels that will be successful
 * 			during the treatment in m_sHdwSimData
 *
 */
void CCommonOperationsTest::SetStandardTemps()
{
	m_sHdwSimData.m_unused1 = 0;
	m_sHdwSimData.m_unused0 = g_pTreatmentInfo->getDeliveryMin() + 1;
	m_sHdwSimData.m_coilTemp = g_pTreatmentInfo->getCoilTempLower() + 1;
	m_sHdwSimData.m_outletTemp = g_pTreatmentInfo->getOutletTempMin() + 1;

	f_simDataAccess.SetSimData( &m_sHdwSimData );

	g_pWaterCntl->InitializeHardware();
	g_pTemperature->InitializeHardware();
	g_pWaterCntl->ReadFromHardware();

	g_pTemperature->ReadFromHardware();
	g_pTemperature->ReadFromHardware();
	g_pTemperature->ReadFromHardware();
}

/**
 * @brief	This method clears the temperatures in the local m_sHdwSimData
 *
 */
void CCommonOperationsTest::ResetSensorData()
{
	m_sHdwSimData.m_unused1 = 0;
	m_sHdwSimData.m_unused0 = 0;
	m_sHdwSimData.m_coilTemp = 0;
	m_sHdwSimData.m_outletTemp = 0;
}

//MCUNIT_TEST( CCommonOperationsTest, <Name>_Interaction )
//{
//}


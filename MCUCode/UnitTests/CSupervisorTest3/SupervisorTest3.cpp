/**
 * @file	SupervisorTest3.cpp
 * @par		Package: CvasMcu
 * @par		Project: CVAS Generator
 * @par		Copyright (c) 2016, Uptake Medical, Inc.
 * @author	Kurt Jan 12, 2012 Created file
 * @brief	This file contains unit tests for CSupervisor
 */

// Include Files
#include "SupervisorTest3.h"
#include "ObjectSingletons.h"
#include "SimDataAccess.h"

// External Public Data

// File Scope Macros and Constants

// File Scope Type Definitions (Enums, Structs & Classes)

// File Scope Data
CSupervisorTest3 f_SupervisorTest3; // Instantiate one so it registers itself

// File Scope Functions

// Public Functions

/**
 * @brief	This is the constructor for CSupervisorTest3
 *
 */

CSupervisorTest3::CSupervisorTest3() :
	CMcUnit<CSupervisorTest3> ( this ), CTestMain( (CUnitTestIntfc*) this )
{
	m_pHdwSimData = NULL;
	m_tbIndex = 0;
}

/**
 * @brief	This method calls into CMcUnit to have the tests run.
 *
 * @return   Always 1
 */
int CSupervisorTest3::CommonTestHandle()
{
	McUnitSuiteRun( &sMcUnitTestSuite );
	McUnitSuiteReport( &sMcUnitTestSuite, DEPTH_TEST_REPORT );
	return 1;
}

/**
 * @brief	This method sets up the lists of tests to run
 *
 */
void CSupervisorTest3::UnitTestListSetup()
{
	int iList = 0;
	int iCase = 0;

	// Set up Case 3: Interaction Tests
	ADD_TEST_CASE( sMcUnitCase, iCase, sMcUnitTestList + iList, 2, "PendingReady State", "Interaction Test failure")
	ADD_TEST_LIST( sMcUnitTestList, iList, &CSupervisorTest3::Setup_for_Interaction, "Setup_for_Interaction", 0x0 )
	ADD_TEST_LIST( sMcUnitTestList, iList, &CSupervisorTest3::PendingReadyState_Interaction, "PendingReadyState_Interaction", 0x0 )

	ADD_TEST_CASE( sMcUnitCase, iCase, sMcUnitTestList + iList, 1, "TerminateState", "Interaction Test failure")
	ADD_TEST_LIST( sMcUnitTestList, iList, &CSupervisorTest3::TerminateState_Interaction, "TerminateState_Interaction", 0x0 )

	MAKE_TEST_SUITE( sMcUnitTestSuite, sMcUnitCaseList, sMcUnitCase, iCase, "CSupervisorTest3" )
}

//=============================================================================
// CASE 3: INTERACTION TESTS
//=============================================================================


/**
 * @brief	This method tests the interaction of TBD.
 *
 * @return  result as an int: 0 (UNIT_TEST_PASSED) if passed, not 0 if failed
 */
MCUNIT_TEST( CSupervisorTest3, Setup_for_Interaction )
{
	int iResult = UNIT_TEST_PASSED;

	iResult |= McUnitAssertIsOK( g_timeBase.InitializeHardware(), "g_timeBase.InitializeHardware" );
	iResult |= McUnitAssertIsOK( g_pTemperature->InitializeHardware(), "g_pTemperature->InitializeHardware" );
	iResult |= McUnitAssertIsOK( g_pWaterCntl->InitializeHardware(), "g_pWaterCntl->InitializeHardware" );
	iResult |= McUnitAssertIsOK( g_pDigitalInputOutput->InitializeHardware(),
	        "g_pDigitalInputOutput->InitializeHardware" );
	iResult |= m_setupHelpers.SbcInitAndConnect();

	m_setupHelpers.SbcSendAllLogs( GetOperState() );

	return iResult;
}

/**
 * @brief	This method tests the interaction of TBD.
 *
 * @return  result as an int: 0 (UNIT_TEST_PASSED) if passed, not 0 if failed
 */
MCUNIT_TEST( CSupervisorTest3, PendingReadyState_Interaction )
{
	int iResult = UNIT_TEST_PASSED;
	tick tFirst = 0;
	upCvasLEDState eLed;
	long int ilRate = 99;
	bool_c bOn = true;

	// Set up
	iResult |= SetForPendingReady();

	//
	// Still pending
	LogString( "Still pending" );
	iResult |= McUnitAssertIsOK( GetTick( &tFirst ), "GetTick" );
	m_setupHelpers.SetTick( tFirst );

	iResult |= McUnitAssertIsOK( PendingState(), "PendingReadyState" );
	iResult |= McUnitAssertIsEqual( OPER_STATE_PENDING, GetOperState(), "GetOperState PendingReady" );
	iResult |= McUnitAssertIsOK( g_pDigitalInputOutput->GetLEDState( &eLed ), "GetLEDState" );
	iResult |= McUnitAssertIsEqual( LED_POWER, eLed, "LED_POWER" );

	//
	// Terminate upon critical error, internal over temp
	LogString( "Terminate upon critical" );
	f_simDataAccess.GetSimData( &m_pHdwSimData, &m_tbIndex );
	m_pHdwSimData->m_intTemp = g_pTreatmentInfo->getMaxInternalTemp() * TEMPERATURE_FIR_COUNT + 10;
	m_pHdwSimData->m_digInput |= ( 1 << DIG_IN_TRIGGER_BIT );
	iResult |= m_setupHelpers.SetAndReadSimDataRecord( m_pHdwSimData );
	iResult |= McUnitAssertIsEqual( ERR_GENERATOR_OVER_TEMP, PendingState(), "PendingReadyState" );
	iResult |= McUnitAssertIsEqual( OPER_STATE_ERROR, GetOperState(), "GetOperState Terminate" );
	m_setupHelpers.SbcSendAllLogs( GetOperState() );

	m_pHdwSimData->m_intTemp = g_pTreatmentInfo->getMaxInternalTemp() - 1;
	iResult |= m_setupHelpers.SetAndReadSimDataRecord( m_pHdwSimData );

	//
	// Go to CONNECTED if retract in progress
	LogString( "Go to CONNECTED" );
	iResult |= SetForPendingReady();

	iResult |= McUnitAssertIsOK( g_pWaterCntl->StartRetractingPumpRod(), "StartRetractingPumpRod" );
	iResult |= McUnitAssertIsEqual( ERR_RETRACTING_PUMP, PendingState(), "PendingReadyState" );
	// The operating state should be IDLE since the pump is being retracted
	iResult |= McUnitAssertIsEqual( OPER_STATE_IDLE, GetOperState(), "GetOperState Idle: Pump retracted" );
	iResult |= McUnitAssertIsOK( g_pWaterCntl->PerformRodRetraction( &bOn ), "PerformRodRetraction" );

	// Check for NO ApplyCoilIdle when last operation was PRIMING
	LogString( "Check for NO ApplyCoilIdle" );
	iResult |= McUnitAssertIsOK( SetOperState( OPER_STATE_DELIVERING ), "SetOperState" );
	iResult |= McUnitAssertIsOK( g_pTreatmentRecord->UpdateTreatmentRecord( DELIVERY_PRIME, 0 ), "UpdateTreatmentRecord" );
	iResult |= SetForPendingReady();
	iResult |= McUnitAssertIsOK( PendingState(), "PendingReadyState" );
	iResult |= McUnitAssertIsOK( g_pWaterCntl->GetTargetVelocity( &ilRate ), "GetTargetVelocity" );
	iResult |= McUnitAssertIsEqual( (long int) 0, ilRate, "Rate" );
	iResult |= McUnitAssertIsOK( g_pRFGen->GetOutputEnable( &bOn ), "GetOutputEnable" );
	iResult |= McUnitAssertIsEqual( (bool_c) false, bOn, "RF OFF" );

	// Check for ApplyCoilIdle when following a treatment
	LogString( "Check for ApplyCoilIdle" );
	f_simDataAccess.GetSimData( &m_pHdwSimData, &m_tbIndex );
	m_pHdwSimData->m_digInput |= ( 1 << DIG_IN_TRIGGER_BIT );
	iResult |= m_setupHelpers.SetAndReadSimDataRecord( m_pHdwSimData );
	iResult |= McUnitAssertIsOK( GetTick( &tFirst ), "GetTick" );
	iResult |= McUnitAssertIsOK( SetOperState( OPER_STATE_DELIVERING ), "SetOperState" );
	iResult |= McUnitAssertIsOK( DeliveringState(), "TreatingState" );
	iResult |= McUnitAssertIsOK( g_pTreatmentRecord->UpdateTreatmentRecord( DELIVERY_FULL, 10 ), "UpdateTreatmentRecord" );
	iResult |= McUnitAssertIsEqual( OPER_STATE_PENDING, GetOperState(), "GetOperState PendingReady" );

	m_setupHelpers.SetTick( tFirst + SEC_TENTHS_TO_TICKS( g_pTreatmentInfo->getTrest() ) / 2 );

	iResult |= McUnitAssertIsOK( PendingState(), "PendingReadyState" );
	iResult |= McUnitAssertIsEqual( OPER_STATE_PENDING, GetOperState(), "GetOperState PendingReady" );
	iResult |= McUnitAssertIsOK( PendingState(), "PendingReadyState" ); // Do twice
	iResult |= McUnitAssertIsEqual( OPER_STATE_PENDING, GetOperState(), "GetOperState PendingReady" );
	ilRate = 0;
	iResult |= McUnitAssertIsOK( g_pWaterCntl->GetTargetVelocity( &ilRate ), "GetTargetVelocity" );
	iResult |= McUnitAssertTrue( 0 < ilRate, "Rate" );
	iResult |= McUnitAssertIsOK( g_pRFGen->GetOutputEnable( &bOn ), "GetOutputEnable" );
	iResult |= McUnitAssertIsEqual( (bool_c) true, bOn, "RF ON" );

// Flush code removed

	// RF Gen error and go to TERMINATE
	LogString( "RF Gen error" );

	// Go to IDLE for Pump at limit and retract rod
	LogString( "IDLE for ERR_PUMP_AT_LIMIT" );
	iResult |= SetForPendingReady();
	f_simDataAccess.GetSimData( &m_pHdwSimData, &m_tbIndex );
	m_pHdwSimData->m_digInput |= ( 1 << DIG_IN_MOTOR_EXTEND_BIT );
	iResult |= m_setupHelpers.SetAndReadSimDataRecord( m_pHdwSimData );

	iResult |= McUnitAssertIsEqual( ERR_PUMP_AT_LIMIT, PendingState(), "PendingReadyState" );
	iResult |= McUnitAssertIsEqual( OPER_STATE_IDLE, GetOperState(), "GetOperState IDLE: Pump at limit" );
	iResult |= McUnitAssertIsEqual( OW_STATE_READY, g_pOneWireMemory->GetOwState(), "GetOwState" );
	iResult |= McUnitAssertIsOK( CheckHandpiece(), "CheckHandpiece" );
	iResult |= McUnitAssertIsEqual( UNIT_TEST_PASSED, m_setupHelpers.SbcSendAllLogs( GetOperState() ),
	        "SbcSendAllLogs", false );

	// Go to IDLE for Interlock missing
	LogString( "IDLE for Interlock missing" );
	iResult |= SetForPendingReady();
	f_simDataAccess.GetSimData( &m_pHdwSimData, &m_tbIndex );
//	m_pHdwSimData->m_digInput &= ~( 1 << DIG_IN_UNUSED_0_BIT );
	iResult |= m_setupHelpers.SetAndReadSimDataRecord( m_pHdwSimData );
	iResult |= McUnitAssertIsEqual( ERR_INTERLOCK_MISSING, PendingState(), "PendingReadyState" );
	iResult |= McUnitAssertIsEqual( OPER_STATE_IDLE, GetOperState(), "GetOperState IDLE" );
	iResult |= McUnitAssertIsEqual( OW_STATE_READY, g_pOneWireMemory->GetOwState(), "GetOwState" );
	iResult |= McUnitAssertIsOK( CheckHandpiece(), "CheckHandpiece" );
	iResult |= McUnitAssertIsEqual( UNIT_TEST_PASSED, m_setupHelpers.SbcSendAllLogs( GetOperState() ),
	        "SbcSendAllLogs", false );

	// Go to IDLE for Thermo Interlock
	LogString( "IDLE for Thermo Interlock" );
	iResult |= SetForPendingReady();
	f_simDataAccess.GetSimData( &m_pHdwSimData, &m_tbIndex );
	m_pHdwSimData->m_outletTemp = 0;
	iResult |= m_setupHelpers.SetAndReadSimDataRecord( m_pHdwSimData );

	iResult |= McUnitAssertIsEqual( ERR_THERMO_INTERLOCK_MISSING, PendingState(), "PendingReadyState" );
	iResult |= McUnitAssertIsEqual( OPER_STATE_IDLE, GetOperState(), "GetOperState IDLE" );
	iResult |= McUnitAssertIsEqual( OW_STATE_CONNECTED, g_pOneWireMemory->GetOwState(), "GetOwState" );
	iResult |= McUnitAssertIsEqual( ERR_NO_DDEVICE, CheckHandpiece(), "CheckHandpiece" );
	iResult |= McUnitAssertIsEqual( UNIT_TEST_PASSED, m_setupHelpers.SbcSendAllLogs( GetOperState() ),
	        "SbcSendAllLogs", false );

	//
	// Switch to READY
	LogString( "Switch to READY" );
	iResult |= SetForPendingReady();
	iResult |= McUnitAssertIsOK( PendingState(), "PendingReadyState" );

	iResult |= McUnitAssertIsOK( GetTick( &tFirst ), "GetTick" );
	m_setupHelpers.SetTick( tFirst + SEC_TENTHS_TO_TICKS( g_pTreatmentInfo->getTrest() ) + RND_ROBIN_TICK_CNT );

	iResult |= McUnitAssertIsOK( PendingState(), "PendingReadyState" );
	iResult |= McUnitAssertIsEqual( OPER_STATE_READY, GetOperState(), "GetOperState READY" );
	iResult |= McUnitAssertIsOK( g_pDigitalInputOutput->GetLEDState( &eLed ), "GetLEDState" );
	iResult |= McUnitAssertIsEqual( LED_POWER, eLed, "LED_POWER" );

	iResult |= McUnitAssertIsEqual( UNIT_TEST_PASSED, m_setupHelpers.SbcSendAllLogs( GetOperState() ),
	        "SbcSendAllLogs", false );

	return iResult;
}

/**
 * @brief	This method tests the interaction of TBD.
 *
 * @return  result as an int: 0 (UNIT_TEST_PASSED) if passed, not 0 if failed
 */
MCUNIT_TEST( CSupervisorTest3, TerminateState_Interaction )
{
	int iResult = UNIT_TEST_PASSED;
	upCvasLEDState eLed;

	// Run a good one
	iResult |= McUnitAssertIsOK( g_pDigitalInputOutput->SetLEDState( LED_OFF ), "SetLEDState" );
	iResult |= McUnitAssertIsOK( SetOperState( OPER_STATE_ERROR ), "SetOperState" );
	iResult |= McUnitAssertIsEqual( OPER_STATE_ERROR, GetOperState(), "GetOperState Terminate" );

	iResult |= McUnitAssertIsEqual( UNIT_TEST_PASSED, m_setupHelpers.InterlockAll(), "InterlockAll", false );

	tick tFirst;
	iResult |= McUnitAssertIsOK( GetTick( &tFirst ), "GetTick" );

	for ( int i = 0; i < 50; i++, tFirst += RND_ROBIN_TICK_CNT )
		{
		m_setupHelpers.SetTick( tFirst );
		iResult |= McUnitAssertIsOK( ErrorState(), "TerminateState" );

		iResult |= McUnitAssertIsOK( g_pDigitalInputOutput->GetLEDState( &eLed ), "GetLEDState" );
		iResult |= McUnitAssertIsEqual( LED_TERMINATE, eLed, "LED_TERMINATE" );
		iResult |= McUnitAssertIsEqual( CSoundCntl::SOUND_CRITICAL, g_pSoundCntl->GetSoundState(), "GetSoundState" );
		long int liVel;
		iResult |= McUnitAssertIsOK( g_pWaterCntl->GetTargetVelocity( &liVel ), "GetTargetVelocity" );
		iResult |= McUnitAssertIsEqual( (byte) 0, (byte) liVel, "Velocity zero" );
		bool_c bOn;
		iResult |= McUnitAssertIsOK( g_pRFGen->GetACInput( &bOn ), "GetACInput" );
		iResult |= McUnitAssertIsEqual( (byte) 0, (byte) bOn, "GetACInput zero" );
		}

	m_setupHelpers.SbcSendAllLogs( GetOperState() );

	return iResult;
}

/**
 * @brief	This method sets up the system for a test of PnedingReadyState()
 *
 * @return   Status as int
 */
int CSupervisorTest3::SetForPendingReady()
{
	int iResult = UNIT_TEST_PASSED;

	iResult |= McUnitAssertIsOK( g_pDigitalInputOutput->SetLEDState( LED_OFF ), "SetLEDState" );
	iResult |= McUnitAssertIsEqual( UNIT_TEST_PASSED, m_setupHelpers.InterlockAll(), "InterlockAll", false );

	//
	// Still pending
	f_simDataAccess.GetSimData( &m_pHdwSimData, &m_tbIndex );
	m_pHdwSimData->m_digInput |= ( 1 << DIG_IN_SYR_PRESENT_BIT );
	m_pHdwSimData->m_digInput |= ( 1 << DIG_IN_UNUSED_0_BIT );
	m_pHdwSimData->m_digInput &= ~( 1 << DIG_IN_TRIGGER_BIT );
	m_pHdwSimData->m_digInput |= ( 1 << DIG_IN_MOTOR_RETRACT_BIT );
	m_pHdwSimData->m_digInput &= ~( 1 << DIG_IN_MOTOR_EXTEND_BIT );
	// itherm	m_pHdwSimData->m_tbRfGenStatus = 0xA6;
	m_pHdwSimData->m_tbRfGenStatus = 0x06;
	m_pHdwSimData->m_tbRfGenPower = 0;
	m_pHdwSimData->m_intTemp = g_pTreatmentInfo->getMaxInternalTemp() - 10;
	m_pHdwSimData->m_outletTemp = ( g_pTreatmentInfo->getTempUpperLimitIdle() + g_pTreatmentInfo->getTempLowerLimitIdle() ) / 2;
	m_pHdwSimData->m_coilTemp = m_pHdwSimData->m_outletTemp;
	f_simDataAccess.SetSimData( m_pHdwSimData );
	for ( int i = 0; i < TEMPERATURE_FIR_COUNT; i++ )
		iResult |= m_setupHelpers.ReadSimDataRecord();

	iResult |= McUnitAssertIsOK( SetCatheterPrimedFlag( true ), "SetPrimedFlag" );
	iResult |= McUnitAssertIsOK( SetOperState( OPER_STATE_PENDING ), "SetOperState" );
	iResult |= McUnitAssertIsEqual( OPER_STATE_PENDING, GetOperState(), "GetOperState PendingReady" );
	iResult |= m_setupHelpers.SendOpcondToSbc( GetOperState() );
	iResult |= m_setupHelpers.SendOpcondToSbc( GetOperState() );

	return iResult;
}

/**
 * @file	WaterCntlTest.h
 * @par		Package: CvasMcu
 * @par		Project: CVAS Generator
 * @par		Copyright (c) 2016, Uptake Medical, Inc.
 * @author	Kurt Jan 12, 2012 Created file
 * @brief	This file contains unit tests for CWaterCntl
 */

#ifndef WATERCNTLTEST_H_
#define WATERCNTLTEST_H_

// Include Files
#include <WaterCntl.h>
#include "McUnit.h"
#include "UnitTestIntfc.h"
#include "TestMain.h"

// Referenced classes
#include <TreatmentInformation.h>
#include <DigitalInputOutput.h>

// Public Macros and Constants

// Public Type Definitions (Enums, Structs & Classes)

/**
 * @class	CWaterCntlTest
 * @brief	This class provides unit tests for CWaterCntl
 *
 * Virtual Functions Overridden: @n@n
 *  UnitTestListSetup - Set up list of Suites, Cases and Tests to be execute @n
 * 	CommonTesthandle - Called to have tests run @n
 *
 * External Data Members: None @n@n
 *
 */
class CWaterCntlTest: private CMcUnit<CWaterCntlTest>, public CUnitTestIntfc, public CTestMain, private CWaterCntl
{
public:
	CWaterCntlTest();
	int CommonTestHandle();
	void UnitTestListSetup();
	// Case 1: Invocation Tests
	MCUNIT_TEST_DEC( AllPublicMethods_Invocation );
	// Case 2: Parameter Tests
	MCUNIT_TEST_DEC( RetractPumpRod_Parameter );
	MCUNIT_TEST_DEC( ExtendPumpRod_Parameter );
	// Case 3: Interaction Tests
	//MCUNIT_TEST_DEC( <Name>_Interaction );

	SMcUnitTest sMcUnitTestList[3];	// Increment index for each test added, regardless of the case in which it falls
	SMcUnitCase sMcUnitCase[3];
	SMcUnitCase * sMcUnitCaseList[4];
	SMcUnitSuite sMcUnitTestSuite;
};

// Inline Functions (follows class definition)


#endif /* WATERCNTLTEST_H_ */

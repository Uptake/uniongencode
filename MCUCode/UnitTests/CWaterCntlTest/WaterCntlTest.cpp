/**
 * @file	WaterCntlTest.cpp
 * @par		Package: CvasMcu
 * @par		Project: CVAS Generator
 * @par		Copyright (c) 2016, Uptake Medical, Inc.
 * @author	Kurt Jan 12, 2012 Created file
 * @brief	This file contains unit tests for CWaterCntl
 */

// Include Files
#include "WaterCntlTest.h"

// External Public Data

// File Scope Macros and Constants

// File Scope Type Definitions (Enums, Structs & Classes)

// File Scope Data
static CWaterCntlTest f_WaterCntlTest; // Instantiate one so it registers itself

// Referenced classes
//CTreatmentInformation f_theraInfo;
//CDigitalInputOutput f_digIO;

// File Scope Functions

// Public Functions

/**
 * @brief	This is the constructor for CWaterCntlTest
 *
 */

CWaterCntlTest::CWaterCntlTest() :
	CMcUnit<CWaterCntlTest> ( this ), CTestMain( (CUnitTestIntfc*) this )
{
}

/**
 * @brief	This method calls into CMcUnit to have the tests run.
 *
 * @return   Always 1
 */
int CWaterCntlTest::CommonTestHandle()
{
	McUnitSuiteRun( &sMcUnitTestSuite );
	McUnitSuiteReport( &sMcUnitTestSuite, DEPTH_TEST_REPORT );
	return 1;
}

/**
 * @brief	This method sets up the lists of tests to run
 *
 */
void CWaterCntlTest::UnitTestListSetup()
{

	int iList = 0;
	int iCase = 0;

	// Set up Case 1: Invocation Tests
	ADD_TEST_CASE( sMcUnitCase, iCase, sMcUnitTestList + iList, 1, "Invocation Tests", "Invocation Test failure")
	ADD_TEST_LIST( sMcUnitTestList, iList, &CWaterCntlTest::AllPublicMethods_Invocation, "All Public Methods invocation", "unexpected result" )

	// Set up Case 2: Parameter Tests
	ADD_TEST_CASE( sMcUnitCase, iCase, sMcUnitTestList + iList, 2, "Parameter Tests", "Parameter Test failure")
	ADD_TEST_LIST( sMcUnitTestList, iList, &CWaterCntlTest::RetractPumpRod_Parameter, "RetractPumpRod() parameter", "unexpected result" )
	ADD_TEST_LIST( sMcUnitTestList, iList, &CWaterCntlTest::ExtendPumpRod_Parameter, "ExtendPumpRod() parameter", "unexpected result" )

	// Set up Case 3: Interaction Tests
	ADD_TEST_CASE( sMcUnitCase, iCase, sMcUnitTestList + iList, 0, "Interaction Tests (No tests defined)", "")
	//Delete the preceding line and uncomment the two lines below to begin adding Interaction Tests
	//ADD_TEST_CASE( sMcUnitCase, iCase, sMcUnitTestList + iList, 1, "Interaction Tests", "Interaction Test failure")
	//ADD_TEST_LIST( sMcUnitTestList, iList, &CWaterCntlTest::<Name>_Interaction, "<Name> Interaction", "unexpected result" )

	MAKE_TEST_SUITE( sMcUnitTestSuite, sMcUnitCaseList, sMcUnitCase, iCase, "CWaterCntlTest" )

}

//=============================================================================
// CASE 1: INVOCATION TESTS
//=============================================================================

/**
 * @brief	This method tests the invocation of all public methods with valid parameters.
 *
 * @return  result as an int: 0 (UNIT_TEST_PASSED) if passed, not 0 if failed
 */
MCUNIT_TEST( CWaterCntlTest, AllPublicMethods_Invocation )
{
	int iResult = UNIT_TEST_PASSED;
	bool_c bIsExtended = false;
	int iInvocations = 0;

	// Invoke and check return codes (if there is one)

	iResult |= McUnitAssertIsEqual( OK, g_pWaterCntl->InitializeHardware(), "InitializeHardware()" );
	iInvocations++;

	iResult |= McUnitAssertIsEqual( OK, g_pWaterCntl->WriteToHardware(), "WriteToHardware()" );
	iInvocations++;

	iResult |= McUnitAssertIsEqual( OK, SetFlowRateUlPerMin( 5000 ), "SetFlowRateUlPerMin()" );
	iInvocations++;
	// TODO No bounds checking on passed values

	iResult |= McUnitAssertIsEqual( OK, SetFlowRateMlPerMin( 5 ), "SetFlowRateMlPerMin()" );
	iInvocations++;
	// TODO No bounds checking on passed values

	iResult |= McUnitAssertIsEqual( OK, g_pWaterCntl->ReadFromHardware(), "ReadFromHardware()" );
	iInvocations++;

	iResult |= McUnitAssertIsEqual( OK, StartRetractingPumpRod(), "RetractPumpRod()" );
	iInvocations++;

	iResult |= McUnitAssertIsEqual( OK, ExtendPumpRod( &bIsExtended ), "ExtendPumpRod()" );
	iInvocations++;

	iResult |= McUnitAssertIsEqual( OK, PerformRodRetraction( &bIsExtended ), "PerformRodRetraction()" );
	iInvocations++;

	LogOneValue( "Number of methods tested: %d", iInvocations );

	return iResult;
}

//=============================================================================
// CASE 2: PARAMETER TESTS
//=============================================================================

/**
 * @brief	This method tests the passed parameter(s) of the RetractPumpRod() method.
 *
 * @return  result as an int: 0 (UNIT_TEST_PASSED) if passed, not 0 if failed
 */

MCUNIT_TEST( CWaterCntlTest, RetractPumpRod_Parameter )
{
	int iResult = UNIT_TEST_PASSED;
	bool_c bIsRetracted = false;
	SHdwSimData sHdwSimData; // Structure to simulate inputs

	// Check different states of pump rod retraction
	sHdwSimData.m_digInput |= 0x0008; // Set motor retracted bit
	f_simDataAccess.SetSimData( &sHdwSimData );
	g_pDigitalInputOutput->ReadFromHardware();

	iResult |= McUnitAssertIsEqual( OK, StartRetractingPumpRod(), "RetractPumpRod() with rod retracted" );

	iResult |= McUnitAssertIsEqual( OK, PerformRodRetraction( &bIsRetracted ),
	        "PerformRodRetraction() with rod extended" );
	iResult |= McUnitAssertIsEqual( (bool_c) true, bIsRetracted, "IsRetracted value" ); // Expect return of "true"

	sHdwSimData.m_digInput &= ~0x0008; // Clear motor retracted bit
	f_simDataAccess.SetSimData( &sHdwSimData );
	g_pDigitalInputOutput->ReadFromHardware();

	iResult |= McUnitAssertIsEqual( OK, StartRetractingPumpRod(), "StartRetractingPumpRod() with rod not retracted" );

	iResult |= McUnitAssertIsEqual( OK, PerformRodRetraction( &bIsRetracted ),
	        "PerformRodRetraction() with rod not extended" );
	iResult |= McUnitAssertIsEqual( (bool_c) false, bIsRetracted, "IsRetracted value" ); // Expect return of "false"

	return iResult;
}

/**
 * @brief	This method tests the passed parameter(s) of the ExtendPumpRod() method.
 *
 * @return  result as an int: 0 (UNIT_TEST_PASSED) if passed, not 0 if failed
 */

MCUNIT_TEST( CWaterCntlTest, ExtendPumpRod_Parameter )
{
	int iResult = UNIT_TEST_PASSED;
	bool_c bIsExtended = false;
	SHdwSimData sHdwSimData; // Structure to simulate inputs

	// Check different states of pump rod extension
	sHdwSimData.m_digInput |= 0x0010; // Set motor extended bit
	f_simDataAccess.SetSimData( &sHdwSimData );
	g_pDigitalInputOutput->ReadFromHardware();

	iResult |= McUnitAssertIsEqual( OK, ExtendPumpRod( &bIsExtended ), "ExtendPumpRod() with rod extended" );
	iResult |= McUnitAssertIsEqual( (bool_c) true, bIsExtended, "IsExtended value" ); // Expect return of "true"

	sHdwSimData.m_digInput &= ~0x0010; // Clear motor retracted bit
	f_simDataAccess.SetSimData( &sHdwSimData );
	g_pDigitalInputOutput->ReadFromHardware();

	iResult |= McUnitAssertIsEqual( OK, ExtendPumpRod( &bIsExtended ), "ExtendPumpRod() with rod not extended" );
	iResult |= McUnitAssertIsEqual( (bool_c) false, bIsExtended, "IsExtended value" ); // Expect return of "false"

	return iResult;
}

//=============================================================================
// CASE 3: INTERACTION TESTS
//=============================================================================

/**
 * @brief	This method tests the interaction of TBD.
 *
 * @return  result as an int: 0 (UNIT_TEST_PASSED) if passed, not 0 if failed
 */

//MCUNIT_TEST( CWaterCntlTest, <Name>_Interaction )
//{
//}


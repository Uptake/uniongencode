/**
 * @file	SbcSerialIntfcTest.cpp
 * @par		Package: CvasMcu
 * @par		Project: CVAS Generator
 * @par		Copyright (c) 2016, Uptake Medical, Inc.
 * @author	Kurt Jan 12, 2012 Created file
 * @brief	This file contains unit tests for CSbcSerialIntfc
 */

// Include Files
#include "SbcSerialIntfcTest.h"

// External Public Data

// File Scope Macros and Constants

// File Scope Type Definitions (Enums, Structs & Classes)

// File Scope Data
static CSbcSerialIntfcTest f_SbcSerialIntfcTest; // Instantiate one so it registers itself

// File Scope Functions

// Public Functions

/**
 * @brief	This is the constructor for CSbcSerialIntfcTest
 *
 */

CSbcSerialIntfcTest::CSbcSerialIntfcTest() :
	CMcUnit<CSbcSerialIntfcTest> ( this ), CTestMain( (CUnitTestIntfc*) this )
{
}


/**
 * @brief	This method calls into CMcUnit to have the tests run.
 *
 * @return   Always 1
 */
int CSbcSerialIntfcTest::CommonTestHandle()
{
	McUnitSuiteRun( &sMcUnitTestSuite );
	McUnitSuiteReport( &sMcUnitTestSuite, DEPTH_TEST_REPORT );
	return 1;
}

/**
 * @brief	This method sets up the lists of tests to run
 *
 */
void CSbcSerialIntfcTest::UnitTestListSetup()
{

	int iList = 0;
	int iCase = 0;

	// Set up Case 1: Invocation Tests
	ADD_TEST_CASE( sMcUnitCase, iCase, sMcUnitTestList + iList, 1, "Invocation Tests", "Invocation Test failure")
	ADD_TEST_LIST( sMcUnitTestList, iList, &CSbcSerialIntfcTest::AllPublicMethods_Invocation, "All Public Methods invocation", "unexpected result" )

	// Set up Case 2: Parameter Tests
	ADD_TEST_CASE( sMcUnitCase, iCase, sMcUnitTestList + iList, 0, "Parameter Tests (No tests defined)", "")
	//Delete the preceding line and uncomment the two lines below to begin adding Parameter Tests
	//ADD_TEST_CASE( sMcUnitCase, iCase, sMcUnitTestList + iList, 1, "Parameter Tests", "Parameter Test failure")
	//ADD_TEST_LIST( sMcUnitTestList, iList, &CSbcSerialIntfcTest::<Method>_Parameter, "<Method>() parameter", "unexpected result" )

	// Set up Case 3: Interaction Tests
	ADD_TEST_CASE( sMcUnitCase, iCase, sMcUnitTestList + iList, 0, "Interaction Tests (No tests defined)", "")
	//Delete the preceding line and uncomment the two lines below to begin adding Interaction Tests
	//ADD_TEST_CASE( sMcUnitCase, iCase, sMcUnitTestList + iList, 1, "Interaction Tests", "Interaction Test failure")
	//ADD_TEST_LIST( sMcUnitTestList, iList, &CSbcSerialIntfcTest::<Name>_Interaction, "<Name> Interaction", "unexpected result" )

	MAKE_TEST_SUITE( sMcUnitTestSuite, sMcUnitCaseList, sMcUnitCase, iCase, "CSbcSerialIntfcTest" )

}

//=============================================================================
// CASE 1: INVOCATION TESTS
//=============================================================================

/**
 * @brief	This method tests the invocation of All Public Methods with valid parameters.
 *
 * @return  result as an int: 0 (UNIT_TEST_PASSED) if passed, not 0 if failed
 */
MCUNIT_TEST( CSbcSerialIntfcTest, AllPublicMethods_Invocation )
{
	int iResult = UNIT_TEST_PASSED;
	twobytes	tbValue;
	byte	bArray[SBC_SEND_BYTE_MAX];
	upMessageTypes eMsgType;
	byte bValue = 0;
	bool_c	boolFlag;
	int	iInvocations = 0;

	// Invoke and check return codes (if there is one)
	iResult |= McUnitAssertIsEqual( OK, InitializeHardware(), "InitializeHardware()" );
	iInvocations++;

	iResult |= McUnitAssertIsEqual( OK, WriteToHardware(), "WriteToHardware()" );
	iInvocations++;

	iResult |= McUnitAssertIsEqual( OK, ReadFromHardware(), "ReadFromHardware()" );
	iInvocations++;

	iResult |= McUnitAssertIsEqual( (bool_c) false, GetLastAckStatus(), "GetLastAckStatus()" );
	iInvocations++;

	iResult |= McUnitAssertIsEqual( (bool_c) false, PacketReceived(), "PacketReceived()" );
	iInvocations++;

	iResult |= McUnitAssertIsEqual( OK, GetTimeoutFlag(), "GetTimeoutFlag()" );
	iInvocations++;

	iResult |= McUnitAssertIsEqual( OK, ResetCriticalTimestamp(), "ResetCriticalTimestamp()" );
	iInvocations++;

	tbValue = SBC_SEND_BYTE_MAX;
	iResult |= McUnitAssertIsEqual( OK, WriteBuffer( tbValue, bArray ), "WriteBuffer(max count)" );
	tbValue++;
	iResult |= McUnitAssertIsEqual( ERR_BAD_PARAM, WriteBuffer( tbValue, bArray ), "WriteBuffer(max count + 1)" );
	iInvocations++;

	iResult |= McUnitAssertIsEqual( OK, ReturnNextMsgType( &eMsgType ), "ReturnNextMsgType()" );
	iResult |= McUnitAssertIsEqual( MSG_TYPE_NOTSPECIFIED, eMsgType, "MsgType value" );
	iInvocations++;

	iResult |= McUnitAssertIsEqual( OK, GetLastSbcRequest( bArray ), "GetLastSbcRequest()" );
	iInvocations++;

	iResult |= McUnitAssertIsEqual( OK, CheckWaitForReplyFlag( &boolFlag ), "CheckWaitForReplyFlag()" );
	iResult |= McUnitAssertIsEqual( (bool_c) false, boolFlag, "WaitFlag value" );
	iInvocations++;

	iResult |= McUnitAssertIsEqual( ERR_NOT_READY, PacketBufferToMessage( bArray, tbValue ), "PacketBufferToMessage()" );
	iInvocations++;

	tbValue = MAX_MESSAGE_SIZE;
	iResult |= McUnitAssertIsEqual( OK, MessageToPacketBuffer( bValue, bArray, tbValue, bValue ), "MessageToPacketBuffer()" );
	tbValue++;
	iResult |= McUnitAssertIsEqual( ERR_IPC_PAYLOAD_OVERFLOW, MessageToPacketBuffer( bValue, bArray, tbValue, bValue ), "MessageToPacketBuffer()" );
	iInvocations++;

	LogOneValue("Number of methods tested: %d", iInvocations);

	return iResult;
}


//=============================================================================
// CASE 2: PARAMETER TESTS
//=============================================================================

/**
 * @brief	This method tests the passed parameter(s) of the <Method>() method.
 *
 * @return  result as an int: 0 (UNIT_TEST_PASSED) if passed, not 0 if failed
 */

//MCUNIT_TEST( CSbcSerialIntfcTest, <Method>_Parameter )
//{
//}


//=============================================================================
// CASE 3: INTERACTION TESTS
//=============================================================================

/**
 * @brief	This method tests the interaction of TBD.
 *
 * @return  result as an int: 0 (UNIT_TEST_PASSED) if passed, not 0 if failed
 */

//MCUNIT_TEST( CSbcSerialIntfcTest, <Name>_Interaction )
//{
//}


/**
 * @file	SupervisorTest2.cpp
 * @par		Package: CvasMcu
 * @par		Project: CVAS Generator
 * @par		Copyright (c) 2016, Uptake Medical, Inc.
 * @author	Kurt Jan 12, 2012 Created file
 * @brief	This file contains unit tests for CSupervisor
 */

// Include Files
#include "SupervisorTest2.h"
#include "ObjectSingletons.h"
#include "SimDataAccess.h"

// External Public Data

// File Scope Macros and Constants

// File Scope Type Definitions (Enums, Structs & Classes)

// File Scope Data
CSupervisorTest2 f_SupervisorTest2; // Instantiate one so it registers itself

// File Scope Functions

// Public Functions

/**
 * @brief	This is the constructor for CSupervisorTest2
 *
 */

CSupervisorTest2::CSupervisorTest2() :
	CMcUnit<CSupervisorTest2> ( this ), CTestMain( (CUnitTestIntfc*) this )
{
}

/**
 * @brief	This method calls into CMcUnit to have the tests run.
 *
 * @return   Always 1
 */
int CSupervisorTest2::CommonTestHandle()
{
	McUnitSuiteRun( &sMcUnitTestSuite );
	McUnitSuiteReport( &sMcUnitTestSuite, DEPTH_TEST_REPORT );
	return 1;
}

/**
 * @brief	This method sets up the lists of tests to run
 *
 */
void CSupervisorTest2::UnitTestListSetup()
{
	int iList = 0;
	int iCase = 0;

	// Set up Case 3: Interaction Tests
	ADD_TEST_CASE( sMcUnitCase, iCase, sMcUnitTestList + iList, 3, "Setup and WaitingForTrigger", "Interaction Test failure")
	ADD_TEST_LIST( sMcUnitTestList, iList, &CSupervisorTest2::Setup_for_Interaction, "Setup", 0x0 )
	ADD_TEST_LIST( sMcUnitTestList, iList, &CSupervisorTest2::WaitingForTrigger_OnOff_RFGen_Interaction, "WaitingForTrigger ON/OFF RFGen err", 0x0 )
	ADD_TEST_LIST( sMcUnitTestList, iList, &CSupervisorTest2::WaitingForTrigger_Interaction, "WaitingForTrigger Pump retract, handpiece and interlocks", 0x0 )

	ADD_TEST_CASE( sMcUnitCase, iCase, sMcUnitTestList + iList, 1, "CheckHandpiece", "Interaction Test failure")
	ADD_TEST_LIST( sMcUnitTestList, iList, &CSupervisorTest2::CheckHandpiece_Interaction, "CheckHandpiece Interaction", 0x0 )

	MAKE_TEST_SUITE( sMcUnitTestSuite, sMcUnitCaseList, sMcUnitCase, iCase, "CSupervisorTest2" )

}

//=============================================================================
// CASE 3: INTERACTION TESTS
//=============================================================================


/**
 * @brief	This method tests the interaction of TBD.
 *
 * @return  result as an int: 0 (UNIT_TEST_PASSED) if passed, not 0 if failed
 */
MCUNIT_TEST( CSupervisorTest2, Setup_for_Interaction )
{
	int iResult = UNIT_TEST_PASSED;

	iResult |= McUnitAssertIsOK( g_timeBase.InitializeHardware(), "g_timeBase.InitializeHardware" );
	iResult |= McUnitAssertIsOK( g_pTemperature->InitializeHardware(), "g_pTemperature->InitializeHardware" );
	iResult |= McUnitAssertIsOK( g_pWaterCntl->InitializeHardware(), "g_pWaterCntl->InitializeHardware" );
	iResult |= McUnitAssertIsOK( g_pDigitalInputOutput->InitializeHardware(),
	        "g_pDigitalInputOutput->InitializeHardware" );
	iResult |= m_setupHelpers.SbcInitAndConnect();

	m_setupHelpers.SbcSendAllLogs();

	return iResult;
}

/**
 * @brief	This method tests the interaction of WaitingForTrigger normal and delayed ON/OFF behavior and
 * 			error exit for RF Generator failureit.
 *
 * @return  result as an int: 0 (UNIT_TEST_PASSED) if passed, not 0 if failed
 */
MCUNIT_TEST( CSupervisorTest2, WaitingForTrigger_OnOff_RFGen_Interaction )
{
	int iResult = UNIT_TEST_PASSED;
	SHdwSimData * pHdwSimData;
	twobytes tbIndex;
	bool_c bOn;
	tick tFirst;
	tick tNewNow;

	// Zero for param
	iResult |= McUnitAssertIsEqual( ERR_BAD_PARAM, WaitingForTrigger( 0 ), "Bad param" );

	iResult |= McUnitAssertIsOK( SetOperState( OPER_STATE_IDLE ), "SetOperState" );

	iResult |= m_setupHelpers.InterlockAll();

	f_simDataAccess.GetSimData( &pHdwSimData, &tbIndex );
	pHdwSimData->m_digInput &= ~( 1 << DIG_IN_TRIGGER_BIT );
	pHdwSimData->m_digInput |= ( 1 << DIG_IN_MOTOR_RETRACT_BIT );
	pHdwSimData->m_coilTemp = 10;
	pHdwSimData->m_outletTemp = 10;
	pHdwSimData->m_intTemp = 10;
	pHdwSimData->m_tbRfGenPower = 0;
	// itherm	pHdwSimData->m_tbRfGenStatus = 0xA6;
	pHdwSimData->m_tbRfGenStatus = 0x06;
	f_simDataAccess.SetSimData( pHdwSimData );

	// Check trigger OFF and retracted

	g_pDigitalInputOutput->ReadFromHardware();
	iResult |= McUnitAssertIsOK( GetTick( &tFirst ), "GetTick" );
	for ( int i = 0; i < 5; i++ )
		{
		iResult |= m_setupHelpers.RFGenWriteRead();
		iResult |= McUnitAssertIsOK( WaitingForTrigger( &bOn ), "WaitingForTrigger", false );
		iResult |= McUnitAssertIsEqual( (uint8_t) false, bOn, "Trigger false", false  );
		m_setupHelpers.SetTick( tFirst + i * RND_ROBIN_TICK_CNT );
		}

	// Check trigger ON
	pHdwSimData->m_digInput |= ( 1 << DIG_IN_TRIGGER_BIT );
	f_simDataAccess.SetSimData( pHdwSimData );
	g_pDigitalInputOutput->ReadFromHardware();
	iResult |= McUnitAssertIsOK( GetTick( &tFirst ), "GetTick" );
	for ( int i = 0; i < 20; i++ )
		{
		iResult |= McUnitAssertIsOK( WaitingForTrigger( &bOn ), "WaitingForTrigger", false );
		iResult |= McUnitAssertIsEqual( (uint8_t) true, bOn, "Trigger true", false  );
		m_setupHelpers.SetTick( tFirst + i * RND_ROBIN_TICK_CNT );
		}

	// Check trigger OFF with delay
	pHdwSimData->m_digInput &= ~( 1 << DIG_IN_TRIGGER_BIT );
	f_simDataAccess.SetSimData( pHdwSimData );
	iResult |= m_setupHelpers.RFGenWriteRead();
	g_pDigitalInputOutput->ReadFromHardware();
	for ( int j = 0; j < TEMPERATURE_FIR_COUNT; j++ )
		iResult |= McUnitAssertIsOK( g_pTemperature->ReadFromHardware(), "g_pTemperature->ReadFromHardware" );

	iResult |= McUnitAssertIsEqual( ERR_TRIGGER_OFF, HandpiecePrimingState(), "PrimingState" ); // re-seed m_lastEnd
	iResult |= McUnitAssertIsOK( SetOperState( OPER_STATE_READY ), "SetOperState" );
	iResult |= McUnitAssertIsOK( GetTick( &tFirst ), "GetTick" );
	tNewNow = tFirst;
	for ( int i = 0; i < 5; i++ )
		{
		iResult |= m_setupHelpers.RFGenWriteRead();
		iResult |= McUnitAssertIsOK( WaitingForTrigger( &bOn ), "WaitingForTrigger", false );
		iResult |= McUnitAssertIsEqual( (uint8_t) false, bOn, "Trigger false", false  );
		tNewNow += RND_ROBIN_TICK_CNT;
		m_setupHelpers.SetTick( tNewNow );
		}

	// Check trigger ON requires delay in READY mode
	pHdwSimData->m_digInput |= ( 1 << DIG_IN_TRIGGER_BIT );
	f_simDataAccess.SetSimData( pHdwSimData );
	g_pDigitalInputOutput->ReadFromHardware();
	iResult |= m_setupHelpers.SendOpcondToSbc( GetOperState() );
	for ( twobytes i = 0; i < SEC_TENTHS_TO_TICKS( g_pTreatmentInfo->getTrest() ) / RND_ROBIN_TICK_CNT + 10; i++ )
		{
		iResult |= McUnitAssertIsOK( WaitingForTrigger( &bOn ), "WaitingForTrigger", false );
		if ( tNewNow - tFirst < SEC_TENTHS_TO_TICKS( g_pTreatmentInfo->getTrest() ) )
			iResult |= McUnitAssertIsEqual( (uint8_t) false, bOn, "Trigger false", false  );
		else
			iResult |= McUnitAssertIsEqual( (uint8_t) true, bOn, "Trigger true", false  );
		tNewNow += RND_ROBIN_TICK_CNT;
		m_setupHelpers.SetTick( tNewNow );
		}

	m_setupHelpers.SbcSendAllLogs();

	return iResult;
}

/**
 * @brief	This method tests the interaction of WaitingForTrigger normal and delayed ON/OFF behavior and
 * 			error exit for RF Generator failureit.
 *
 * @return  result as an int: 0 (UNIT_TEST_PASSED) if passed, not 0 if failed
 */
MCUNIT_TEST( CSupervisorTest2, WaitingForTrigger_Interaction )
{
	int iResult = UNIT_TEST_PASSED;
	SHdwSimData * pHdwSimData;
	twobytes tbIndex;
	bool_c bOn;
	tick tFirst;

	f_simDataAccess.GetSimData( &pHdwSimData, &tbIndex );
	pHdwSimData->m_bRfGenAction = UART_NORMAL;
	// itherm	pHdwSimData->m_tbRfGenStatus = 0xA6;
	pHdwSimData->m_tbRfGenStatus = 0x06;
	pHdwSimData->m_tbRfGenPower = 0;
	f_simDataAccess.SetSimData( pHdwSimData );

	// Zero for param
	iResult |= McUnitAssertIsEqual( ERR_BAD_PARAM, WaitingForTrigger( 0 ), "Bad param" );

	iResult |= McUnitAssertIsOK( SetOperState( OPER_STATE_IDLE ), "SetOperState" );

	iResult |= m_setupHelpers.InterlockAll();

	// Check trigger OFF when retracting
	g_pWaterCntl->StartRetractingPumpRod();
	f_simDataAccess.GetSimData( &pHdwSimData, &tbIndex );
	pHdwSimData->m_digInput |= ( 1 << DIG_IN_TRIGGER_BIT );
	pHdwSimData->m_digInput &= ~( 1 << DIG_IN_MOTOR_RETRACT_BIT );
	// itherm	pHdwSimData->m_tbRfGenStatus = 0xA6;
	pHdwSimData->m_tbRfGenStatus = 0x06;
	f_simDataAccess.SetSimData( pHdwSimData );
	g_pDigitalInputOutput->ReadFromHardware();
	iResult |= McUnitAssertIsOK( GetTick( &tFirst ), "GetTick" );
	for ( int i = 0; i < 5; i++ )
		{
		iResult |= m_setupHelpers.RFGenWriteRead();
		iResult |= McUnitAssertIsOK( WaitingForTrigger( &bOn ), "WaitingForTrigger", false );
		iResult |= McUnitAssertIsEqual( (uint8_t) false, bOn, "Trigger false" );
		m_setupHelpers.SetTick( tFirst + i * RND_ROBIN_TICK_CNT );
		m_setupHelpers.SendOpcondToSbc( GetOperState() );
		}

	// Check for returning OFF and invalidating prime when retracted and primed
	g_pWaterCntl->StartRetractingPumpRod();
	pHdwSimData->m_digInput |= ( 1 << DIG_IN_TRIGGER_BIT );
	pHdwSimData->m_digInput &= ~( 1 << DIG_IN_MOTOR_RETRACT_BIT );
	f_simDataAccess.SetSimData( pHdwSimData );
	g_pDigitalInputOutput->ReadFromHardware();
	iResult |= McUnitAssertIsOK( GetTick( &tFirst ), "GetTick" );
	for ( int i = 0; i < 5; i++ )
		{
		SetCatheterPrimedFlag( true );
		iResult |= McUnitAssertIsOK( SetOperState( OPER_STATE_HAND_PRIMING ), "SetOperState" );
		iResult |= McUnitAssertIsOK( WaitingForTrigger( &bOn ), "WaitingForTrigger", false );
		iResult |= McUnitAssertIsEqual( (uint8_t) false, bOn, "Trigger false" );
		iResult |= McUnitAssertIsEqual( OPER_STATE_CNCTD, GetOperState(), "GetOperState" );
		m_setupHelpers.SetTick( tFirst + i * RND_ROBIN_TICK_CNT );
		}

	// Check for OFF and error when not interlocked
	// OFF and clear retract action
	g_pWaterCntl->StartRetractingPumpRod();
	pHdwSimData->m_digInput &= ~( 1 << DIG_IN_TRIGGER_BIT );
	pHdwSimData->m_digInput |= ( 1 << DIG_IN_MOTOR_RETRACT_BIT );
	f_simDataAccess.SetSimData( pHdwSimData );

	iResult |= McUnitAssertIsOK( SetOperState( OPER_STATE_CNCTD ), "SetOperState" );
	g_pDigitalInputOutput->ReadFromHardware();
	iResult |= McUnitAssertIsOK( WaitingForTrigger( &bOn ), "WaitingForTrigger", false );
	iResult |= McUnitAssertIsEqual( (uint8_t) false, bOn, "Trigger false" );

	// Trigger ON
	pHdwSimData->m_digInput |= ( 1 << DIG_IN_TRIGGER_BIT );
	f_simDataAccess.SetSimData( pHdwSimData );
	g_pDigitalInputOutput->ReadFromHardware();
	iResult |= McUnitAssertIsOK( WaitingForTrigger( &bOn ), "WaitingForTrigger", false );
	iResult |= McUnitAssertIsEqual( (uint8_t) true, bOn, "Trigger true" );

	// Check for OFF and error when bad handpiece
	// Reset to good so ON
	f_simDataAccess.SetSimData( pHdwSimData );
	g_pDigitalInputOutput->ReadFromHardware();
	iResult |= McUnitAssertIsOK( WaitingForTrigger( &bOn ), "WaitingForTrigger", false );
	iResult |= McUnitAssertIsEqual( (uint8_t) true, bOn, "Trigger true" );

	// Cause error for handpiece and now OFF
	g_pOneWireMemory->DisableTool();
	f_simDataAccess.SetSimData( pHdwSimData );
	g_pDigitalInputOutput->ReadFromHardware();
	iResult |= McUnitAssertIsOK( GetTick( &tFirst ), "GetTick" );
	for ( int i = 0; i < 5; i++ )
		{
		iResult |= McUnitAssertIsEqual( ERR_NO_DDEVICE, WaitingForTrigger( &bOn ), "WaitingForTrigger", false );
		iResult |= McUnitAssertIsEqual( (uint8_t) false, bOn, "Trigger false" );
		m_setupHelpers.SetTick( tFirst + i * RND_ROBIN_TICK_CNT );
		}

	m_setupHelpers.SbcSendAllLogs();

	return iResult;
}

/**
 * @brief	This method tests the interaction of TBD.
 *
 * @return  result as an int: 0 (UNIT_TEST_PASSED) if passed, not 0 if failed
 */
MCUNIT_TEST( CSupervisorTest2, CheckHandpiece_Interaction )
{
	int iResult = UNIT_TEST_PASSED;

	// Run a good one
	iResult |= m_setupHelpers.InterlockAll();
	iResult |= McUnitAssertIsOK( CheckHandpiece(), "CheckHandpiece" );

	// Change delivery device
	// TODO: can't change romInfo in simulated one-wire memory

	// Exhausted therapy time
#define TREAT_SECS	(9)
	unsignedfourbytes ulTherapyTime;
	g_pOneWireMemory->ClearOwValues();
	iResult |= McUnitAssertIsOK( g_pOneWireMemory->GetTherapyTime( &ulTherapyTime ), "GetTherapyTime" );
	for ( ; ulTherapyTime < g_pTreatmentInfo->getTvapor(); ulTherapyTime += TREAT_SECS )
		{
		iResult |= McUnitAssertIsOK( g_pTreatmentRecord->UpdateTreatmentRecord( DELIVERY_PARTIAL_ERROR,
		        MS_TO_TICKS( SEC_TO_MS(TREAT_SECS)) ), "UpdateTreatmentRecord" );
		for ( int i = 0; i < 5; i++ )
			iResult |= McUnitAssertIsOK( g_pOneWireMemory->Execute(), "Execute", false );
		}
	iResult |= McUnitAssertIsEqual( ERR_DDEVICE_EXPIRED_TIME, CheckHandpiece(), "CheckHandpiece" );

	// Exhausted full treatment count
	twobytes tbCnt;
	iResult |= McUnitAssertIsOK( g_pOneWireMemory->GetFullTreatmentCount( &tbCnt ), "GetFullTreatmentCount" );
	for ( ; tbCnt < g_pTreatmentInfo->getDeliveryCount(); tbCnt++ )
		{
		iResult |= McUnitAssertIsOK( g_pTreatmentRecord->UpdateTreatmentRecord( DELIVERY_FULL, MS_TO_TICKS( 1 )),
		        "UpdateTreatmentRecord" );
		for ( int i = 0; i < 5; i++ )
			iResult |= McUnitAssertIsOK( g_pOneWireMemory->Execute(), "Execute", false );
		}
	iResult |= McUnitAssertIsEqual( ERR_DDEVICE_EXPIRED_TREAT, CheckHandpiece(), "CheckHandpiece" );

	// Run a good one
	iResult |= McUnitAssertIsOK( g_pOneWireMemory->DisableTool(), "DisableTool" );
	g_pOneWireMemory->InitializeHardware();
	g_pOneWireMemory->ClearOwValues();
	g_pTreatmentRecord->StartNewRecord();
	g_pTreatmentRecord->ClearStoredValues();
	iResult |= m_setupHelpers.InterlockAll();
	iResult |= McUnitAssertIsOK( CheckHandpiece(), "CheckHandpiece" );
	iResult |= McUnitAssertIsOK( g_pOneWireMemory->GetTherapyTime( &ulTherapyTime ), "GetTherapyTime" );
	iResult |= McUnitAssertIsEqual( (unsignedfourbytes) 0, ulTherapyTime, "therapy secs" );

	// Device attached - is the first use
	time tTime;
	time tTime2;
	SetInitValues();
	iResult |= McUnitAssertIsOK( g_pOneWireMemory->SetFirstUseTime( 0 ), "SetFirstUseTime" );
	iResult |= McUnitAssertIsOK( CheckHandpiece(), "CheckHandpiece" );
	iResult |= McUnitAssertIsOK( g_pOneWireMemory->GetFirstUseTime( &tTime ), "GetFirstUseTime" );
	iResult |= McUnitAssertTrue( 0 != tTime, "First use Time should be changed" );

	// Device attached - not first use
	iResult |= McUnitAssertIsOK( g_pOneWireMemory->GetFirstUseTime( &tTime2 ), "GetFirstUseTime" );
	SetInitValues();
	iResult |= McUnitAssertIsOK( CheckHandpiece(), "CheckHandpiece" );
	iResult |= McUnitAssertIsOK( g_pOneWireMemory->GetFirstUseTime( &tTime ), "GetFirstUseTime" );
	iResult |= McUnitAssertTrue( tTime2 == tTime, "First use Time should not be changed" );

	// Device attached - bad therapy code
	SetInitValues();
	g_pOneWireMemory->ClearOwValues();
	iResult |= McUnitAssertIsEqual( ERR_DDEVICE_UNKNOWN, CheckHandpiece(), "CheckHandpiece" );

	// Delivery device disabled
	SetInitValues();

	// No Delivery device attached
	g_pOneWireMemory->DisableTool();
	iResult |= McUnitAssertIsEqual( ERR_NO_DDEVICE, CheckHandpiece(), "CheckHandpiece" );

	m_setupHelpers.SbcSendAllLogs();

	return iResult;
}


/**
 * @file	SupervisorTest2.h
 * @par		Package: CvasMcu
 * @par		Project: CVAS Generator
 * @par		Copyright (c) 2016, Uptake Medical, Inc.
 * @author	Kurt Jan 12, 2012 Created file
 * @brief	This file contains unit tests for CSupervisor
 */

#ifndef SUPERVISORTEST2_H_
#define SUPERVISORTEST2_H_

// Include Files
#include <Supervisor.h>
#include "McUnit.h"
#include "UnitTestIntfc.h"
#include "TestMain.h"
#include "SetupHelpers.h"

// Referenced classes

// Public Macros and Constants

// Public Type Definitions (Enums, Structs & Classes)

/**
 * @class	CSupervisorTest2
 * @brief	This class provides unit tests for CSupervisor
 *
 * Virtual Functions Overridden: @n@n
 *  UnitTestListSetup - Set up list of Suites, Cases and Tests to be execute @n
 * 	CommonTesthandle - Called to have tests run @n
 *
 * External Data Members: None @n@n
 *
 */
class CSupervisorTest2: private CMcUnit<CSupervisorTest2> , public CUnitTestIntfc, public CTestMain, private CSupervisor
{
	CSetupHelpers m_setupHelpers;

public:
	CSupervisorTest2();
	int CommonTestHandle();
	void UnitTestListSetup();

	// Case 3: Interaction Tests
	MCUNIT_TEST_DEC( Setup_for_Interaction );
	MCUNIT_TEST_DEC( WaitingForTrigger_Interaction );
	MCUNIT_TEST_DEC( WaitingForTrigger_OnOff_RFGen_Interaction );
	MCUNIT_TEST_DEC( CheckHandpiece_Interaction );

	SMcUnitTest sMcUnitTestList[ 23 ]; // Increment index for each test added, regardless of the case in which it falls
	SMcUnitCase sMcUnitCase[ 11 ];
	SMcUnitCase * sMcUnitCaseList[ 11 ];
	SMcUnitSuite sMcUnitTestSuite;
};

// Inline Functions (follows class definition)


#endif /* SUPERVISORTEST2_H_ */

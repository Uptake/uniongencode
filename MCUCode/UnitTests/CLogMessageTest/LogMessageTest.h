/**
 * @file	LogMessageTest.h
 * @par		Package: CvasMcu
 * @par		Project: CVAS Generator
 * @par		Copyright (c) 2016, Uptake Medical, Inc.
 * @author	Chris Dec 30, 2011 Created file
 * @brief	This file contains
 */

#ifndef LOGMESSAGETEST_H_
#define LOGMESSAGETEST_H_

// Include Files
#include "McUnit.h"
#include "TestMain.h"
#include "UnitTestIntfc.h"

// Public Macros and Constants

// Public Type Definitions (Enums, Structs & Classes)

/**
 * @class	CLogMessageTest
 * @brief	This class provides unit tests for the CLogMessage class
 *
 * Virtual Functions Overridden: @n@n
 *  UnitTestListSetup - Set up list of Suites, Cases and Tests to be execute @n
 * 	CommonTesthandle - Called to have tests run @n
 *
 * External Data Members: None @n@n
 *
 */


class CLogMessageTest: private CMcUnit<CLogMessageTest>, public CTestMain, public CUnitTestIntfc
{
public:
	CLogMessageTest();
	int CommonTestHandle();
	void UnitTestListSetup();
	// Case 1: Invocation Tests
	MCUNIT_TEST_DEC( AllPublicMethods_Invocation );
	// Case 2: Parameter Tests
	//MCUNIT_TEST_DEC( <Method>_Parameter );
	// Case 3: Interaction Tests
	MCUNIT_TEST_DEC( AddLogs_Interaction );

	SMcUnitTest sMcUnitTestList[2];	// Increment index for each test added, regardless of the case in which it falls
	SMcUnitCase sMcUnitCase[3];
	SMcUnitCase * sMcUnitCaseList[4];
	SMcUnitSuite sMcUnitTestSuite;

private:
	// Helper test methods
	void SetupOrTearDownLogTest();

};

// Inline Functions (follows class definition)


#endif /* LOGMESSAGETEST_H_ */

/**
 * @file	LogMessageTest.cpp
 * @par		Package: CvasMcu
 * @par		Project: CVAS Generator
 * @par		Copyright (c) 2016, Uptake Medical, Inc.
 * @author	Chris Dec 30, 2011 Created file
 * @brief	This file contains unit tests for the CLogMessage class
 */

// Include Files
#include "LogMessageTest.h"
#include "SimDataAccess.h"
#include "CvasMcuDefinitions.h"
#include "LogMessage.h"

// External Public Data

// File Scope Macros and Constants

// File Scope Type Definitions (Enums, Structs & Classes)

// File Scope Data
XMEM_SECTION
static CLogMessageTest f_CLogMessageTest; // Instantiate one so it registers itself

// File Scope Functions

// Public Functions

/**
 * @brief	This is the constructor for CLogMessageTest
 *
 */
CLogMessageTest::CLogMessageTest() :
	CMcUnit<CLogMessageTest> ( this ), CTestMain( (CUnitTestIntfc*) this )
{
}

/**
 * @brief	This method calls into CMcUnit to have the tests run.
 *
 * @return   Always 1
 */
int CLogMessageTest::CommonTestHandle()
{
	McUnitSuiteRun( &sMcUnitTestSuite );
	McUnitSuiteReport( &sMcUnitTestSuite, DEPTH_TEST_REPORT );
	return 1;
}

/**
 * @brief	This method sets up the lists of tests to run
 *
 */
void CLogMessageTest::UnitTestListSetup()
{
	int iList = 0;
	int iCase = 0;

	// Set up Case 1: Invocation Tests
	ADD_TEST_CASE( sMcUnitCase, iCase, sMcUnitTestList + iList, 1, "Invocation Tests", "Invocation Test failure")
	ADD_TEST_LIST( sMcUnitTestList, iList, &CLogMessageTest::AllPublicMethods_Invocation, "All Public Methods invocation", "unexpected result" )

	// Set up Case 2: Parameter Tests
	ADD_TEST_CASE( sMcUnitCase, iCase, sMcUnitTestList + iList, 0, "Parameter Tests (No tests defined)", "")
	//Delete the preceding line and uncomment the two lines below to begin adding Parameter Tests
	//ADD_TEST_CASE( sMcUnitCase, iCase, sMcUnitTestList + iList, 1, "Parameter Tests", "Parameter Test failure")
	//ADD_TEST_LIST( sMcUnitTestList, iList, &CLogMessageTest::<Method>_Parameter, "<Method>() parameter", "unexpected result" )

	// Set up Case 3: Interaction Tests
	ADD_TEST_CASE( sMcUnitCase, iCase, sMcUnitTestList + iList, 1, "Interaction Tests", "Interaction Test failure")
	ADD_TEST_LIST( sMcUnitTestList, iList, &CLogMessageTest::AddLogs_Interaction, "Add Logs Interaction", "unexpected result" )

	MAKE_TEST_SUITE( sMcUnitTestSuite, sMcUnitCaseList, sMcUnitCase, iCase, "CLogMessageTest" )

}

//=============================================================================
// CASE 1: INVOCATION TESTS
//=============================================================================

/**
 * @brief	This method tests the invocation of all public methods with valid parameters.
 *
 * @return  result as an int: 0 (UNIT_TEST_PASSED) if passed, not 0 if failed
 */
MCUNIT_TEST( CLogMessageTest, AllPublicMethods_Invocation )
{
	int iResult = UNIT_TEST_PASSED;
	byte bLogCount;
	byte bUserMsgCount;
	byte bLength;
	char sMessage[LOG_MESSAGE_LENGTH];
	SUserMsg userMsgStruct;
	int	iInvocations = 0;

	// Invoke and check return codes (if there is one)
	iResult |= McUnitAssertIsEqual( OK, g_pLogMessage->LogMessage( "Message" ), "LogMessage( Msg )" );
	iInvocations++;

	iResult |= McUnitAssertIsEqual( OK, g_pLogMessage->LogErrorMessage( (upCvasErr)1, "Err+Msg" ), \
			"LogMessage( Err, Msg )" );
	iInvocations++;

	iResult |= McUnitAssertIsEqual( OK, g_pLogMessage->LogErrorMessage( (upCvasErr)2, "Err+Msg+Line", 100 ), \
			"LogMessage( Err, Msg, Line )" );
	iInvocations++;

	iResult |= McUnitAssertIsEqual( OK, g_pLogMessage->LogErrorMessageInt( (upCvasErr)2, "Err+Msg+Int", 321 ), \
			"LogMessageInt( Err, Msg, Value )" );
	iInvocations++;

	iResult |= McUnitAssertIsEqual( OK, g_pLogMessage->LogErrorMessage( (upCvasErr)3, "Err+Msg1", "+Msg2" ), \
			"LogMessage( Err, Msg1, Msg2 )" );
	iInvocations++;

	iResult |= McUnitAssertIsEqual( OK, g_pLogMessage->LogErrorMessage( (upCvasErr)4, "Err+Msg1", "+Msg2", 200 ), \
			"LogMessage( Err, Msg1, Msg2, Line )" );
	iInvocations++;

	iResult |= McUnitAssertIsEqual( OK, g_pLogMessage->GetLogCount( &bLogCount ), "GetLogCount()" );
	iResult |= McUnitAssertIsEqual( (byte) 6, bLogCount, "Log count" );
	iInvocations++;

	iResult |= McUnitAssertIsEqual( OK, g_pLogMessage->PopLogMessage(), "PopLogMessage()" );
	iInvocations++;

	// First message added was popped.  Peek at second message.  Logged message string is: "   0 : 00000 Err: 0001 Err+Msg"
	iResult |= McUnitAssertIsEqual( OK, g_pLogMessage->PeekLogMessage( sMessage, &bLength ), "PeekLogMessage()" );
	iResult |= McUnitAssertIsEqual( (byte)31, bLength, "Message length for LogMessage( Err, Msg )" );
	iInvocations++;

	// Expect user message queue to be empty
	iResult |= McUnitAssertIsEqual( OK, g_pLogMessage->GetUserMsgCount( &bUserMsgCount ), "GetUserMsgCount()" );
	iResult |= McUnitAssertIsEqual( (byte)0, bUserMsgCount, "User message count" );
	iInvocations++;

	// Expect user message queue to be empty
	iResult |= McUnitAssertIsEqual( ERR_USER_MSG_QUEUE_EMPTY, g_pLogMessage->PopUserMessage(), "PopUserMessage()" );
	iInvocations++;

	// Expect user message queue to be empty
	iResult |= McUnitAssertIsEqual( ERR_USER_MSG_QUEUE_EMPTY, g_pLogMessage->PeekUserMessage( &userMsgStruct ), "PeekUserMessage()" );
	iInvocations++;

	// Expect passed error code to be returned
	iResult |= McUnitAssertIsEqual( (upCvasErr)1, g_pLogMessage->ReturnUserMsgCode( (upCvasErr)1, (upOperState)1 ), "ReturnUserMsgCode()" );
	iInvocations++;

	LogOneValue("Number of methods tested: %d", iInvocations);

	return iResult;
}


//=============================================================================
// CASE 3: INTERACTION TESTS
//=============================================================================

/**
 * @brief	This method tests the interaction of adding log messages to the message queue.
 *
 * @return  result as an int: 0 (UNIT_TEST_PASSED) if passed, not 0 if failed
 */
MCUNIT_TEST( CLogMessageTest, AddLogs_Interaction )
{
	int iResult = UNIT_TEST_PASSED;
	upCvasErr eErr;
	byte bLogCount = 0;

	SetupOrTearDownLogTest();	// Clear out the log

	g_pLogMessage->GetLogCount( &bLogCount );

	// Keep adding logs until you hit the maximum
	for (byte count = bLogCount; count < LOG_MESSAGE_CNT; count++ )
	{
		eErr = g_pLogMessage->LogMessage("Log message\n");
		g_pLogMessage->GetLogCount( &bLogCount );

		if( (LOG_MESSAGE_CNT - 1) == count )
		{
			iResult |= McUnitAssertIsEqual( ERR_LOG_QUEUE_FULL, eErr, "Log full" );
			iResult |= McUnitAssertIsEqual( (byte)(LOG_MESSAGE_CNT - 1), bLogCount, "Can't add a message to the log" );
		}
		else
			iResult |= McUnitAssertIsEqual( (byte)(count + 1), bLogCount, "Add a message to the log" );
	}

	SetupOrTearDownLogTest();	// Clear out the log

	return iResult;
}


// Private Class Functions (Test helpers)

/**
 * @brief	This method clears out the log buffer
 *
 * @return   void
 */
void CLogMessageTest::SetupOrTearDownLogTest()
{
	byte bLogCount = 0;

	g_pLogMessage->GetLogCount( &bLogCount );

	for( byte i = 0; i < bLogCount; i++ )
		g_pLogMessage->PopLogMessage();

}



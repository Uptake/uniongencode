/**
 * @file	StepperVelocityTest.cpp
 * @par		Package: CvasMcu
 * @par		Project: CVAS Generator
 * @par		Copyright (c) 2016, Uptake Medical, Inc.
 * @author	Kurt Jan 24, 2012 Created file
 * @brief	This file contains unit tests for CStepperVelocity
 */

// Include Files
#include "StepperVelocityTest.h"
#include <DigitalInputOutput.h>

// External Public Data

// File Scope Macros and Constants

// File Scope Type Definitions (Enums, Structs & Classes)

// File Scope Data
CStepperVelocityTest f_StepperVelocityTest; // Instantiate one so it registers itself

// File Scope Functions

// Public Functions

/**
 * @brief	This is the constructor for CStepperVelocityTest
 *
 */

CStepperVelocityTest::CStepperVelocityTest() :
	CMcUnit<CStepperVelocityTest> ( this ), CTestMain( (CUnitTestIntfc*) this )
{
}

/**
 * @brief	This method calls into CMcUnit to have the tests run.
 *
 * @return   Always 1
 */
int CStepperVelocityTest::CommonTestHandle()
{
	McUnitSuiteRun( &sMcUnitTestSuite );
	McUnitSuiteReport( &sMcUnitTestSuite, DEPTH_TEST_REPORT );
	return 1;
}

/**
 * @brief	This method sets up the lists of tests to run
 *
 */
void CStepperVelocityTest::UnitTestListSetup()
{

	int iList = 0;
	int iCase = 0;

	// Set up Case 1: Invocation Tests
	ADD_TEST_CASE( sMcUnitCase, iCase, sMcUnitTestList + iList, 1, "Invocation Tests", "Invocation Test failure")
	ADD_TEST_LIST( sMcUnitTestList, iList, &CStepperVelocityTest::AllPublicMethods_Invocation, "All Public Methods() invocation", "unexpected result" )

	// Set up Case 2: Parameter Tests
	ADD_TEST_CASE( sMcUnitCase, iCase, sMcUnitTestList + iList, 1, "Parameter Tests", "Parameter Test failure")
	ADD_TEST_LIST( sMcUnitTestList, iList, &CStepperVelocityTest::GetSetVelocityMethods_Parameter, "Get/Set Velocity Methods parameter", "unexpected result" )

	// Set up Case 3: Interaction Tests
	ADD_TEST_CASE( sMcUnitCase, iCase, sMcUnitTestList + iList, 1, "Interaction Tests", "Interaction Test failure")
	ADD_TEST_LIST( sMcUnitTestList, iList, &CStepperVelocityTest::VelocityAndHardware_Interaction, "Velocity and Hardware Interaction", "unexpected result" )

	MAKE_TEST_SUITE( sMcUnitTestSuite, sMcUnitCaseList, sMcUnitCase, iCase, "CStepperVelocityTest" )

}

//=============================================================================
// CASE 1: INVOCATION TESTS
//=============================================================================

/**
 * @brief	This method tests the invocation of all public methods with valid parameters.
 *
 * @return  result as an int: 0 (UNIT_TEST_PASSED) if passed, not 0 if failed
 */
MCUNIT_TEST( CStepperVelocityTest, AllPublicMethods_Invocation )
{
	int iResult = UNIT_TEST_PASSED;
	long int ilLongIntValue;
	int iInvocations = 0;

	// Invoke and check return codes (if there is one)
	iResult |= McUnitAssertIsOK( InitializeVelocityHardware(), "InitializeVelocityHardware()" );
	iInvocations++;

	iResult |= McUnitAssertIsOK( WriteVelocityToHardware(), "WriteVelocityToHardware()" );
	iInvocations++;

	iResult |= McUnitAssertIsOK( GetTargetVelocity( &ilLongIntValue ), "GetTargetVelocity()" );
	iInvocations++;

	iResult |= McUnitAssertIsOK( SetTargetVelocity( ilLongIntValue = 1111 ), "SetTargetVelocity()" );
	iInvocations++;

	iResult |= McUnitAssertIsOK( GetAppliedVelocity( &ilLongIntValue ), "GetAppliedVelocity()" );
	iInvocations++;

	iResult |= McUnitAssertIsOK( HaltStepper(), "HaltStepper()" );
	iInvocations++;

	LogOneValue( "Number of methods tested: %d", iInvocations );

	return iResult;
}

//=============================================================================
// CASE 2: PARAMETER TESTS
//=============================================================================

/**
 * @brief	This method tests the passed parameter(s) of the Get/Set Velocity methods.
 *
 * @return  result as an int: 0 (UNIT_TEST_PASSED) if passed, not 0 if failed
 */

MCUNIT_TEST( CStepperVelocityTest, GetSetVelocityMethods_Parameter )
{
	int iResult = UNIT_TEST_PASSED;
	long int ilTargetValue;
	long int ilAppliedValue;
	int iMethods = 0;
	long int TARGET_VALUE = 99999;
	int iCount = 0;

	// Expect initial values for Target and Applied Velocity to be zero
	iResult |= McUnitAssertIsOK( InitializeVelocityHardware(), "InitializeVelocityHardware()" );
	iResult |= McUnitAssertIsOK( WriteVelocityToHardware(), "WriteVelocityToHardware()" );
	iResult |= McUnitAssertIsOK( GetTargetVelocity( &ilTargetValue ), "GetTargetVelocity()" );
	iResult |= McUnitAssertIsEqual( (int32_t) 0, ilTargetValue, "Target velocity" );
	iResult |= McUnitAssertIsOK( GetAppliedVelocity( &ilAppliedValue ), "GetAppliedVelocity()" );
	iResult |= McUnitAssertIsEqual( (int32_t) 0, ilAppliedValue, "Applied velocity" );

	// Now set the Target Velocity to something; get it back and verify
	// TODO No bounds checking on setting velocity values
	iResult |= McUnitAssertIsOK( SetTargetVelocity( ilTargetValue = TARGET_VALUE ), "SetTargetVelocity()" );
	iResult |= McUnitAssertIsOK( GetTargetVelocity( &ilTargetValue ), "GetTargetVelocity()" );
	iResult |= McUnitAssertIsEqual( (int32_t) TARGET_VALUE, ilTargetValue, "Target velocity" );
	iMethods += 2; // Target Set/Get verified

	// Applied velocity should still be zero
	iResult |= McUnitAssertIsOK( GetAppliedVelocity( &ilAppliedValue ), "GetAppliedVelocity()" );
	iResult |= McUnitAssertIsEqual( (int32_t) 0, ilAppliedValue, "Applied velocity" );

	// Write velocity to hardware repeatedly and the applied velocity should eventually match the target
	while ( ilAppliedValue < ilTargetValue )
		{
		iResult |= McUnitAssertIsOK( WriteVelocityToHardware(), "WriteVelocityToHardware()", false );
		iResult |= McUnitAssertIsOK( GetAppliedVelocity( &ilAppliedValue ), "GetAppliedVelocity()", false );
		if ( iCount++ > 100 )
			break; // Safety catch to avoid an endless loop
		}
	iResult |= McUnitAssertIsEqual( (int32_t) TARGET_VALUE, ilAppliedValue, "Applied velocity" );
	iMethods++; // Applied Get verified

	LogOneValue( "Number of methods tested: %d", iMethods );

	iResult |= McUnitAssertIsOK( HaltStepper(), "HaltStepper()" );

	return iResult;
}

//=============================================================================
// CASE 3: INTERACTION TESTS
//=============================================================================

/**
 * @brief	This method tests the interaction of velocity settings with the hardware.
 * 			It sets positive and negative velocities and verifies expected results
 * 			when the pump rod is fully extended or retracted.
 *
 * @return  result as an int: 0 (UNIT_TEST_PASSED) if passed, not 0 if failed
 */

MCUNIT_TEST( CStepperVelocityTest, VelocityAndHardware_Interaction )
{
	int iResult = UNIT_TEST_PASSED;
	long int ilTargetValue;
	long int ilAppliedValue;
	long int TARGET_VALUE = 500; // Use a small value to get to target in one hardware write
	SHdwSimData sHdwSimData; // Structure to simulate inputs

	// Expect initial values for Target and Applied Velocity to be zero
	iResult |= McUnitAssertIsOK( InitializeVelocityHardware(), "InitializeVelocityHardware()" );
	iResult |= McUnitAssertIsOK( WriteVelocityToHardware(), "WriteVelocityToHardware()" );
	iResult |= McUnitAssertIsOK( GetTargetVelocity( &ilTargetValue ), "GetTargetVelocity()" );
	iResult |= McUnitAssertIsEqual( (int32_t) 0, ilTargetValue, "Target velocity" );
	iResult |= McUnitAssertIsOK( GetAppliedVelocity( &ilAppliedValue ), "GetAppliedVelocity()" );
	iResult |= McUnitAssertIsEqual( (int32_t) 0, ilAppliedValue, "Applied velocity" );

	// Set a positve target value
	iResult |= McUnitAssertIsOK( SetTargetVelocity( ilTargetValue = TARGET_VALUE ), "SetTargetVelocity()" );
	iResult |= McUnitAssertIsOK( GetTargetVelocity( &ilTargetValue ), "GetTargetVelocity()" );
	iResult |= McUnitAssertIsEqual( (int32_t) TARGET_VALUE, ilTargetValue, "Target velocity" );

	// Simulate pump rod extended and check for at limit error
	sHdwSimData.m_digInput = 0x0010; // Set motor extended bit
	f_simDataAccess.SetSimData( &sHdwSimData );
	g_pDigitalInputOutput->ReadFromHardware();
	iResult |= McUnitAssertIsEqual( ERR_PUMP_AT_LIMIT, WriteVelocityToHardware(), "WriteVelocityToHardware()" );

	// Clear pump rod extended and verify error is gone
	sHdwSimData.m_digInput &= ~0x0010; // Clear motor extended bit
	f_simDataAccess.SetSimData( &sHdwSimData );
	g_pDigitalInputOutput->ReadFromHardware();
	iResult |= McUnitAssertIsOK( InitializeVelocityHardware(), "InitializeVelocityHardware()" );
	iResult |= McUnitAssertIsOK( WriteVelocityToHardware(), "WriteVelocityToHardware()" );

	// Set a target value
	iResult |= McUnitAssertIsOK( SetTargetVelocity( ilTargetValue = TARGET_VALUE ), "SetTargetVelocity()" );
	iResult |= McUnitAssertIsOK( WriteVelocityToHardware(), "WriteVelocityToHardware()" );

	// Get applied value and verify applied velocity was reached
	iResult |= McUnitAssertIsOK( GetAppliedVelocity( &ilAppliedValue ), "GetAppliedVelocity()" );
	iResult |= McUnitAssertIsEqual( (int32_t) TARGET_VALUE, ilAppliedValue, "Applied velocity" );

	// Set a negative target value
	iResult |= McUnitAssertIsOK( SetTargetVelocity( ilTargetValue = -1 * TARGET_VALUE ), "SetTargetVelocity()" );
	iResult |= McUnitAssertIsOK( GetTargetVelocity( &ilAppliedValue ), "GetTargetVelocity()" );
	iResult |= McUnitAssertIsEqual( ( int32_t )( -1 * TARGET_VALUE ), ilAppliedValue, "Applied velocity" );
	iResult |= McUnitAssertIsOK( WriteVelocityToHardware(), "WriteVelocityToHardware()" );

	// Get applied value and verify applied velocity was reached
	iResult |= McUnitAssertIsOK( GetAppliedVelocity( &ilAppliedValue ), "GetAppliedVelocity()" );
	iResult |= McUnitAssertIsEqual( ( int32_t )( -1 * TARGET_VALUE ), ilAppliedValue, "Applied velocity" );

	// Simulate pump rod retracted
	sHdwSimData.m_digInput |= 0x0008; // Set motor retracted bit
	f_simDataAccess.SetSimData( &sHdwSimData );
	g_pDigitalInputOutput->ReadFromHardware();

	// Double the negative target value
	iResult |= McUnitAssertIsOK( SetTargetVelocity( ilTargetValue = -2 * TARGET_VALUE ), "SetTargetVelocity()" );
	iResult |= McUnitAssertIsOK( WriteVelocityToHardware(), "WriteVelocityToHardware()" );

	// Get applied value and verify applied velocity is zero because of retracted rod
	iResult |= McUnitAssertIsOK( GetAppliedVelocity( &ilAppliedValue ), "GetAppliedVelocity()" );
	iResult |= McUnitAssertIsEqual( (int32_t) 0, ilAppliedValue, "Applied velocity" );

	return iResult;
}

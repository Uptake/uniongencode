/**
 * @file	StepperVelocityTest.h
 * @par		Package: CvasMcu
 * @par		Project: CVAS Generator
 * @par		Copyright (c) 2016, Uptake Medical, Inc.
 * @author	Kurt Jan 24, 2012 Created file
 * @brief	This file contains unit tests for CStepperVelocity
 */

#ifndef STEPPERVELOCITYTEST_H_
#define STEPPERVELOCITYTEST_H_

// Include Files
#include "McUnit.h"
#include "TestMain.h"
#include "UnitTestIntfc.h"
#include "StepperVelocity.h"

// Referenced classes


// Public Macros and Constants

// Public Type Definitions (Enums, Structs & Classes)

/**
 * @class	CStepperVelocityTest
 * @brief	This class provides unit tests for CStepperVelocity
 *
 * Virtual Functions Overridden: @n@n
 *  UnitTestListSetup - Set up list of Suites, Cases and Tests to be execute @n
 * 	CommonTesthandle - Called to have tests run @n
 *
 * External Data Members: None @n@n
 *
 */
class CStepperVelocityTest: private CMcUnit<CStepperVelocityTest>, public CTestMain, public CUnitTestIntfc, private CStepperVelocity
{
public:
	CStepperVelocityTest();
	int CommonTestHandle();
	void UnitTestListSetup();
	// Case 1: Invocation Tests
	MCUNIT_TEST_DEC( AllPublicMethods_Invocation );
	// Case 2: Parameter Tests
	MCUNIT_TEST_DEC( GetSetVelocityMethods_Parameter );
	// Case 3: Interaction Tests
	MCUNIT_TEST_DEC( VelocityAndHardware_Interaction );

	SMcUnitTest sMcUnitTestList[3];	// Increment index for each test added, regardless of the case in which it falls
	SMcUnitCase sMcUnitCase[3];
	SMcUnitCase * sMcUnitCaseList[4];
	SMcUnitSuite sMcUnitTestSuite;
};

// Inline Functions (follows class definition)


#endif /* STEPPERVELOCITYTEST_H_ */

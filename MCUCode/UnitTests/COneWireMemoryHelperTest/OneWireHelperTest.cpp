/**
 * @file	OneWireHelperTest.cpp
 * @par		Package: CvasMcu
 * @par		Project: CVAS Generator
 * @par		Copyright (c) 2016, Uptake Medical, Inc.
 * @author	Glen Mar 25, 2011 Created file
 * @brief	This file contains unit tests for the COneWireMemoryHelper class
 */

// Include Files
#include "OneWireHelperTest.h"
#include "SimDataAccess.h"


// External Public Data

// File Scope Macros and Constants

// File Scope Type Definitions (Enums, Structs & Classes)

// File Scope Data
static COneWireMemoryHelperTest f_COneWireHelperTest; // Instantiate one so it registers itself

// File Scope Functions

// Public Functions

/**
 * @brief	This is the constructor for StepperPositionTest
 *
 */
COneWireMemoryHelperTest::COneWireMemoryHelperTest() :
	CMcUnit<COneWireMemoryHelperTest> ( this ), CTestMain( (CUnitTestIntfc*) this )
{
}

/**
 * @brief	This method calls into CMcUnit to have the tests run.
 *
 * @return   Always 1
 */
int COneWireMemoryHelperTest::CommonTestHandle()
{
	McUnitSuiteRun( &sMcUnitTestSuite );
	McUnitSuiteReport( &sMcUnitTestSuite, DEPTH_TEST_REPORT );
	return 1;
}

void COneWireMemoryHelperTest::UnitTestListSetup()
{

	int iList = 0;
	int iCase = 0;

	// Set up Case 1: Invocation Tests
	ADD_TEST_CASE( sMcUnitCase, iCase, sMcUnitTestList + iList, 1, "Invocation Tests", "Invocation Test failure")
	ADD_TEST_LIST( sMcUnitTestList, iList, &COneWireMemoryHelperTest::AllPublicMethods_Invocation, "All Public Methods invocation", "unexpected result" )

	// Set up Case 2: Parameter Tests
	ADD_TEST_CASE( sMcUnitCase, iCase, sMcUnitTestList + iList, 0, "Parameter Tests (No tests defined)", "")
	//Delete the preceding line and uncomment the two lines below to begin adding Parameter Tests
	//ADD_TEST_CASE( sMcUnitCase, iCase, sMcUnitTestList + iList, 1, "Parameter Tests", "Parameter Test failure")
	//ADD_TEST_LIST( sMcUnitTestList, iList, &COneWireMemoryHelperTest::<Method>_Parameter, "<Method>() parameter", "unexpected result" )

	// Set up Case 3: Interaction Tests
	ADD_TEST_CASE( sMcUnitCase, iCase, sMcUnitTestList + iList, 1, "Interaction Tests", "Interaction Test failure")
	ADD_TEST_LIST( sMcUnitTestList, iList, &COneWireMemoryHelperTest::PopFromQueue_Interaction, "Pop From Queue Interaction", "unexpected result" )

	MAKE_TEST_SUITE( sMcUnitTestSuite, sMcUnitCaseList, sMcUnitCase, iCase, "COneWireMemoryHelperTest" )

}

//=============================================================================
// CASE 1: INVOCATION TESTS
//=============================================================================

/**
 * @brief	This method tests the invocation of all public methods with valid parameters.
 *
 * @return  result as an int: 0 (UNIT_TEST_PASSED) if passed, not 0 if failed
 */
MCUNIT_TEST( COneWireMemoryHelperTest, AllPublicMethods_Invocation )
{
	int iResult = UNIT_TEST_PASSED;
	SOwState eState;
	int	iInvocations = 0;

	// Invoke and check return codes (if there is one)
	eState = f_COneWireHelperTest.GetCurrentState();
	iResult |= McUnitAssertIsEqual( OW_STATE_IDLE, eState.eTopState, "GetCurrentState()" );
	iInvocations++;

	eState = f_COneWireHelperTest.GetNextTopState();
	iResult |= McUnitAssertIsEqual( OW_STATE_IDLE, eState.eTopState, "GetNextTopState()" );
	iInvocations++;

	eState = f_COneWireHelperTest.IncrementSubStatePass();
	iResult |= McUnitAssertIsEqual( OW_STATE_IDLE, eState.eTopState, "IncrementSubStatePass()" );
	iInvocations++;

	eState = f_COneWireHelperTest.SetSubState( GEN_STATE_START );
	iResult |= McUnitAssertIsEqual( OW_STATE_IDLE, eState.eTopState, "SetSubState()" );
	iInvocations++;

	eState = f_COneWireHelperTest.SetSubState( GEN_STATE_START );
	iResult |= McUnitAssertIsEqual( OW_STATE_IDLE, eState.eTopState, "SetSubState()" );
	iInvocations++;

	iResult |= McUnitAssertIsEqual( OK, f_COneWireHelperTest.AddEvent( OW_EVENT_NEW_MEMORY ), "AddEvent()" );
	iInvocations++;

	ResetEventQ();	// This function is void, but is needed to reset the queue for subsequent tests.
	iResult |= McUnitAssertIsEqual( OK, OK, "void ResetEventQ()" );
	iInvocations++;

	LogOneValue("Number of methods tested: %d", iInvocations);

	return iResult;
}



//=============================================================================
// CASE 2: PARAMETER TESTS
//=============================================================================

/**
 * @brief	This method tests the passed parameter(s) of COneWireMemoryHelper.
 *
 * @return  result as an int: 0 (UNIT_TEST_PASSED) if passed, not 0 if failed
 */

//MCUNIT_TEST( Test, <Method>_Parameter )
//{
//}


//=============================================================================
// CASE 3: INTERACTION TESTS
//=============================================================================

/**
 * @brief	This method tests the interaction of adding and popping events.
 *
 * @return  result as an int: 0 (UNIT_TEST_PASSED) if passed, not 0 if failed
 */
MCUNIT_TEST( COneWireMemoryHelperTest, PopFromQueue_Interaction )
{
	SOwState eState;
	int iResult = UNIT_TEST_PASSED;

	// Add 4 events
	iResult |= McUnitAssertIsEqual( OK, f_COneWireHelperTest.AddEvent( OW_EVENT_NEW_MEMORY ), "Add event" );
	iResult |= McUnitAssertIsEqual( OK, f_COneWireHelperTest.AddEvent( OW_EVENT_STAMP_FIRST_USE ), "Add event" );
	iResult |= McUnitAssertIsEqual( OK, f_COneWireHelperTest.AddEvent( OW_EVENT_DATA_CHANGE ), "Add event" );
	iResult |= McUnitAssertIsEqual( OK, f_COneWireHelperTest.AddEvent( OW_EVENT_LOCK_DEVICE ), "Add event" );

	// Pop 4 events and verify them
	eState = f_COneWireHelperTest.GetNextTopState();
	iResult |= McUnitAssertIsEqual( OW_STATE_READ_ALL, eState.eTopState, "Pop event and check state" );
	eState = f_COneWireHelperTest.GetNextTopState();
	iResult |= McUnitAssertIsEqual( OW_STATE_WRITE_FIRSTUSE, eState.eTopState, "Pop event and check state" );
	eState = f_COneWireHelperTest.GetNextTopState();
	iResult |= McUnitAssertIsEqual( OW_STATE_WRITE_TREATMENT, eState.eTopState, "Pop event and check state" );
	eState= f_COneWireHelperTest.GetNextTopState();
	iResult |= McUnitAssertIsEqual( OW_STATE_WRITE_LOCKDOWN, eState.eTopState, "Pop event and check state" );

	// Add 2 events
	iResult |= McUnitAssertIsEqual( OK, f_COneWireHelperTest.AddEvent( OW_EVENT_STAMP_FIRST_USE ), "Add event" );
	iResult |= McUnitAssertIsEqual( OK, f_COneWireHelperTest.AddEvent( OW_EVENT_DATA_CHANGE ), "Add event" );

	// Pop 1 event and verify it
	eState = f_COneWireHelperTest.GetNextTopState();
	iResult |= McUnitAssertIsEqual( OW_STATE_WRITE_FIRSTUSE, eState.eTopState, "Pop event and check state" );

	// Add 1 more event
	iResult |= McUnitAssertIsEqual( OK, f_COneWireHelperTest.AddEvent( OW_EVENT_LOCK_DEVICE ), "Add event" );

	// Pop 2 events and verify them
	eState = f_COneWireHelperTest.GetNextTopState();
	iResult |= McUnitAssertIsEqual( OW_STATE_WRITE_TREATMENT, eState.eTopState, "Pop event and check state"  );
	eState = f_COneWireHelperTest.GetNextTopState();
	iResult |= McUnitAssertIsEqual( OW_STATE_WRITE_LOCKDOWN, eState.eTopState, "Pop event and check state"  );

	// Verify there are no events left
	eState = f_COneWireHelperTest.GetNextTopState();
	iResult |= McUnitAssertIsEqual( OW_STATE_IDLE, eState.eTopState, "Pop event and check state"  );

	return iResult;
}

// Private Class Functions



/**
 * @file	RFGeneratorTest.cpp
 * @par		Package: CvasMcu
 * @par		Project: CVAS Generator
 * @par		Copyright (c) 2016, Uptake Medical, Inc.
 * @author	Kurt Jan 12, 2012 Created file
 * @brief	This file contains unit tests for CRFGenerator
 */

// Include Files
#include "RFGeneratorTest.h"
#include "ObjectSingletons.h"

// External Public Data

// File Scope Macros and Constants

// File Scope Type Definitions (Enums, Structs & Classes)
#define ITHERM_START_UP_RW (1)
#define RDO_START_UP_RW  (7)

// File Scope Data
static CRFGeneratorTest f_RFGeneratorTest; // Instantiate one so it registers itself

// File Scope Functions

// Public Functions

/**
 * @brief	This is the constructor for CRFGeneratorTest
 *
 */

CRFGeneratorTest::CRFGeneratorTest() :
	CMcUnit<CRFGeneratorTest> ( this ), CTestMain( (CUnitTestIntfc*) this )
{
}

/**
 * @brief	This method calls into CMcUnit to have the tests run.
 *
 * @return   Always 1
 */
int CRFGeneratorTest::CommonTestHandle()
{
	g_timeBase.InitializeHardware();

	McUnitSuiteRun( &sMcUnitTestSuite );
	McUnitSuiteReport( &sMcUnitTestSuite, DEPTH_TEST_REPORT );
	return 1;
}

/**
 * @brief	This method sets up the lists of tests to run
 *
 */
void CRFGeneratorTest::UnitTestListSetup()
{
	int iList = 0;
	int iCase = 0;

	// Set up Case 1: Invocation Tests
	ADD_TEST_CASE( sMcUnitCase, iCase, sMcUnitTestList + iList, 1, "Invocation Tests", "Invocation Test failure")
	ADD_TEST_LIST( sMcUnitTestList, iList, &CRFGeneratorTest::AllPublicMethods_Invocation, "All Public Methods invocation", "unexpected result" )

	// Set up Case 2: Parameter Tests
	ADD_TEST_CASE( sMcUnitCase, iCase, sMcUnitTestList + iList, 1, "Parameter Tests", "Parameter Test failure")
	ADD_TEST_LIST( sMcUnitTestList, iList, &CRFGeneratorTest::SetGetMethods_Parameter, "Set/get methods parameter", "unexpected result" )

	// Set up Case 3: Interaction Tests
	ADD_TEST_CASE( sMcUnitCase, iCase, sMcUnitTestList + iList, 17, "Interaction Tests", "Interaction Test failure")
	ADD_TEST_LIST( sMcUnitTestList, iList, &CRFGeneratorTest::SetACInput_Interaction, "SetACInput Interaction", "unexpected result" )
	ADD_TEST_LIST( sMcUnitTestList, iList, &CRFGeneratorTest::RFGen_On_Fail_On_InitCmd_UART_NO_RESP, "Test Init Cmd:UART_NO_RESP", "unexpected result" )
	ADD_TEST_LIST( sMcUnitTestList, iList, &CRFGeneratorTest::RFGen_On_Fail_On_InitCmd_UART_BAD_CHECKSUM, "Test Init Cmd:UART_BAD_CHECKSUM", "unexpected result" )
	ADD_TEST_LIST( sMcUnitTestList, iList, &CRFGeneratorTest::RFGen_On_Fail_On_InitCmd_UART_BAD_RESP, "Test Init Cmd:UART_BAD_RESP", "unexpected result" )
	ADD_TEST_LIST( sMcUnitTestList, iList, &CRFGeneratorTest::RFGen_On_Fail_On_InitCmd_UART_BAD_COUNT, "Test Init Cmd:UART_BAD_COUNT", "unexpected result" )
	ADD_TEST_LIST( sMcUnitTestList, iList, &CRFGeneratorTest::RFGen_On_Fail_On_InitCmd_UART_SEND_GARBAGE, "Test Init Cmd:UART_SEND_GARBAGE", "unexpected result" )
	ADD_TEST_LIST( sMcUnitTestList, iList, &CRFGeneratorTest::RFGen_On_Fail_On_InitCmd_UART_PAD_GARBAGE, "Test Init Cmd:UART_PAD_GARBAGE", "unexpected result" )
	ADD_TEST_LIST( sMcUnitTestList, iList, &CRFGeneratorTest::RFGen_On_Fail_On_InitSetup_UART_NO_RESP, "Test Init Setup: UART_NO_RESP", "unexpected result" )
	ADD_TEST_LIST( sMcUnitTestList, iList, &CRFGeneratorTest::RFGen_On_Fail_On_InitSetup_UART_BAD_CHECKSUM, "Test Init Setup: UART_BAD_CHECKSUM", "unexpected result" )
	ADD_TEST_LIST( sMcUnitTestList, iList, &CRFGeneratorTest::RFGen_On_Fail_On_InitSetup_UART_BAD_RESP, "Test Init Setup: UART_BAD_RESP", "unexpected result" )
	ADD_TEST_LIST( sMcUnitTestList, iList, &CRFGeneratorTest::RFGen_On_Fail_On_InitSetup_UART_BAD_COUNT, "Test Init Setup: UART_BAD_COUNT", "unexpected result" )
	ADD_TEST_LIST( sMcUnitTestList, iList, &CRFGeneratorTest::RFGen_On_Fail_On_InitSetup_UART_SEND_GARBAGE, "Test Init Setup: UART_SEND_GARBAGE", "unexpected result" )
	ADD_TEST_LIST( sMcUnitTestList, iList, &CRFGeneratorTest::RFGen_On_Fail_On_InitSetup_UART_PAD_GARBAGE, "Test Init Setup: UART_PAD_GARBAGE", "unexpected result" )
	ADD_TEST_LIST( sMcUnitTestList, iList, &CRFGeneratorTest::RFGen_Status_Command_Components, "RFGen Status Command Components", "unexpected result" )
	ADD_TEST_LIST( sMcUnitTestList, iList, &CRFGeneratorTest::RFGen_On_Fail_Not_Found , "RFGen Not Found", "unexpected result" )
	ADD_TEST_LIST( sMcUnitTestList, iList, &CRFGeneratorTest::RFGen_On_Comm_Fatal, "RFGen Start Comm Fatal", "unexpected result" )
	ADD_TEST_LIST( sMcUnitTestList, iList, &CRFGeneratorTest::RFGen_PowerSet, "RFGen SetPower", "unexpected result" )
	MAKE_TEST_SUITE( sMcUnitTestSuite, sMcUnitCaseList, sMcUnitCase, iCase, "CRFGeneratorTest" )

}

/**
 * @brief	This method
 *
 * @param	eWrite Expected return value for WriteToHardware as upCvasErr
 * @param	eRead Expected return value for ReadFromHardware as upCvasErr
 * @param	eErr Expected return value for GetError as upCvasErr
 *
 * @return   Status as upCvasErr
 */
int CRFGeneratorTest::PerformWriteReadErrorCheck( upCvasErr eWrite, upCvasErr eRead, upCvasErr eErr )
{
	int iResult = UNIT_TEST_PASSED;
	twobytes tbErr;

	iResult |= McUnitAssertIsEqual( eWrite, g_pRFGen->WriteToHardware(), "WriteToHardware" );
	iResult |= McUnitAssertIsEqual( eRead, g_pRFGen->ReadFromHardware(), "ReadFromHardware" );
	iResult |= McUnitAssertIsEqual( eErr, g_pRFGen->GetError( &tbErr ), "GetError" );

	return iResult;
}

//=============================================================================
// CASE 1: INVOCATION TESTS
//=============================================================================

/**
 * @brief	This method tests the invocation of all public methods with valid parameters.
 *
 * @return  result as an int: 0 (UNIT_TEST_PASSED) if passed, not 0 if failed
 */
MCUNIT_TEST( CRFGeneratorTest, AllPublicMethods_Invocation )
{
	int iResult = UNIT_TEST_PASSED;
	twobytes tbValue = 1111;
	bool_c bValue;
	int iValue = 2222;
	int iInvocations = 0;

	// Invoke and check return codes (if there is one)
	iResult |= McUnitAssertIsEqual( OK, g_pRFGen->InitializeHardware(), "InitializeHardware" );
	iInvocations++;

	iResult |= McUnitAssertIsEqual( OK, g_pRFGen->ReadFromHardware(), "ReadFromHardware" );
	iInvocations++;

	iResult |= McUnitAssertIsEqual( OK, g_pRFGen->WriteToHardware(), "WriteToHardware" );
	iInvocations++;

	// Expect initial value to be zero
	iResult |= McUnitAssertIsEqual( OK, g_pRFGen->GetPowerOutput( &tbValue ), "GetPowerOutput" );
	iResult |= McUnitAssertIsEqual( 0, (int) tbValue, "Power output" );
	iInvocations++;

	// Expect initial value to be "not ready"
	iResult |= McUnitAssertIsEqual( ERR_RF_GEN_NOT_READY, g_pRFGen->GetError( &tbValue ), "GetError" );
	iResult |= McUnitAssertIsEqual( 0, (int) tbValue, "Error value" );
	iInvocations++;

	// Expect initial value to be zero
	iResult |= McUnitAssertIsEqual( OK, g_pRFGen->GetStatus( &tbValue ), "GetStatus" );
	iResult |= McUnitAssertIsEqual( 0, (int) tbValue, "Status value" );
	iInvocations++;

	// Expect initial value to be zero
	iResult |= McUnitAssertIsEqual( OK, g_pRFGen->GetTemperature( &iValue ), "GetTemperature" );
	iResult |= McUnitAssertIsEqual( 0, iValue, "Temperature value" );
	iInvocations++;

	// Expect initial value to be false
	// TODO Parameter prefix wrong in definition
	iResult |= McUnitAssertIsEqual( OK, g_pRFGen->GetApplyingPower( &bValue ), "GetApplyingPower" );
	iResult |= McUnitAssertIsEqual( 0, (int) bValue, "Applying power value" );
	iInvocations++;

	// Expect initial value to be false
	iResult |= McUnitAssertIsEqual( OK, g_pRFGen->GetOutputEnable( &bValue ), "GetOutputEnable" );
	iResult |= McUnitAssertIsEqual( 0, (int) bValue, "Output enable value" );
	iInvocations++;

	iResult |= McUnitAssertIsEqual( OK, g_pRFGen->SetOutputEnable( true ), "SetOutputEnable" );
	iInvocations++;

	// Expect initial value to be false
	iResult |= McUnitAssertIsEqual( OK, g_pRFGen->GetACInput( &bValue ), "GetACInput" );
	iResult |= McUnitAssertIsEqual( 0, (int) bValue, "AC input value" );
	iInvocations++;

	iResult |= McUnitAssertIsEqual( OK, g_pRFGen->SetACInput( true ), "SetACInput" );
	iInvocations++;

	// Expect initial value to be zero
	iResult |= McUnitAssertIsEqual( 0, (int) g_pRFGen->GetPowerRequest(), "GetPowerRequest" );
	iInvocations++;

	iResult |= McUnitAssertIsEqual( OK, g_pRFGen->SetPowerRequest( (twobytes) 10 ), "SetPowerRequest" );
	iInvocations++;

	LogOneValue( "Number of methods tested: %d", iInvocations );

	// Init SBC for logging
	iResult |= McUnitAssertIsEqual( UNIT_TEST_PASSED, m_setupHelpers.SbcInitAndConnect(),
	        "m_setupHelpers.SbcInitAndConnect" );
	iResult |= m_setupHelpers.SbcSendAllLogs();

	return iResult;
}

//=============================================================================
// CASE 2: PARAMETER TESTS
//=============================================================================

/**
 * @brief	This method tests the passed parameter(s) of the Set/Get methods.
 *			These include OutputEnable, ACInput, and PowerRequest methods.
 * @return  result as an int: 0 (UNIT_TEST_PASSED) if passed, not 0 if failed
 */

MCUNIT_TEST( CRFGeneratorTest, SetGetMethods_Parameter )
{
	int iResult = UNIT_TEST_PASSED;
	bool_c bValue;
	int iInvocations = 0;

	g_pRFGen->SetOutputEnable( true );
	g_pRFGen->GetOutputEnable( &bValue );
	iResult |= McUnitAssertIsEqual( 1, (int) bValue, "Output enable value when enabled" );
	g_pRFGen->SetOutputEnable( false );
	g_pRFGen->GetOutputEnable( &bValue );
	iResult |= McUnitAssertIsEqual( 0, (int) bValue, "Output enable value when disabled" );
	iInvocations++;

	SetACInput( true );
	g_pRFGen->GetACInput( &bValue );
	iResult |= McUnitAssertIsEqual( 1, (int) bValue, "AC Input value when enabled" );
	SetACInput( false );
	g_pRFGen->GetACInput( &bValue );
	iResult = McUnitAssertIsEqual( 0, (int) bValue, "AC Input value when disabled" );
	iInvocations++;

	g_pRFGen->SetPowerRequest( (twobytes) 222 ); // TODO no bounds checking
	iResult |= McUnitAssertIsEqual( 222, (int) g_pRFGen->GetPowerRequest(), "Power request value" );
	iInvocations++;

	LogOneValue( "Number of set/get method pairs tested: %d", iInvocations );

	iResult |= McUnitAssertIsOK( g_pComOp->VerifyCodeVersions(), "VerifyCodeVersions" );
	iResult |= m_setupHelpers.SbcSendAllLogs();

	return iResult;
}

//=============================================================================
// CASE 3: INTERACTION TESTS
//=============================================================================

/**
 * @brief	This method tests the interaction of SetACInput.
 *
 * @return  result as an int: 0 (UNIT_TEST_PASSED) if passed, not 0 if failed
 */
MCUNIT_TEST( CRFGeneratorTest, SetACInput_Interaction )
{
	int iResult = UNIT_TEST_PASSED;
	twobytes tbErr;

	SetInitValues();
	iResult = m_setupHelpers.StartConnectToRFGen( RF_GEN_POWER_UP_CYCLE_COUNT + RF_GEN_COMPLETE_POWER_UP );

	iResult |= PerformWriteReadErrorCheck( OK, OK, OK );
	iResult |= PerformWriteReadErrorCheck( OK, OK, OK );
	iResult |= PerformWriteReadErrorCheck( OK, OK, OK );

	iResult |= McUnitAssertIsEqual( OK, g_pRFGen->SetACInput( false ), "SetACInput( false )" );

	iResult |= McUnitAssertIsEqual( OK, g_pRFGen->WriteToHardware(), "WriteToHardware" );
	iResult |= McUnitAssertIsEqual( OK, g_pRFGen->ReadFromHardware(), "ReadFromHardware" );
	iResult |= McUnitAssertIsEqual( ERR_RF_GEN_NOT_READY, g_pRFGen->GetError( &tbErr ), "GetError" );

	iResult = m_setupHelpers.StartConnectToRFGen( RF_GEN_POWER_UP_CYCLE_COUNT + RF_GEN_COMPLETE_POWER_UP );

	iResult |= PerformWriteReadErrorCheck( OK, OK, OK );

	iResult |= m_setupHelpers.SbcSendAllLogs();

	return iResult;
}

/**
 * @brief	This method tests the interaction of SetACInput.
 *
 * @return  result as an int: 0 (UNIT_TEST_PASSED) if passed, not 0 if failed
 */
MCUNIT_TEST( CRFGeneratorTest, RFGen_On_Fail_On_InitCmd_UART_NO_RESP )
{
	int iResult = UNIT_TEST_PASSED;
	SHdwSimData * pHdwSimData;
	twobytes tbIndex;

	SetInitValues();
	iResult = m_setupHelpers.StartConnectToRFGen( RF_GEN_POWER_UP_CYCLE_COUNT );

	f_simDataAccess.GetSimData( &pHdwSimData, &tbIndex );
	pHdwSimData->m_sbcAction = UART_NORMAL;
	pHdwSimData->m_bRfGenAction = UART_NO_RESP;
	f_simDataAccess.SetSimData( pHdwSimData );

	iResult |= PerformWriteReadErrorCheck( OK, OK, ERR_RF_GEN_NOT_READY );
	iResult |= PerformWriteReadErrorCheck( OK, OK, ERR_RF_GEN_NOT_READY );
	iResult |= PerformWriteReadErrorCheck( OK, OK, ERR_RF_GEN_NOT_READY );
	iResult |= PerformWriteReadErrorCheck( OK, OK, ERR_RF_GEN_NOT_READY );
	iResult |= PerformWriteReadErrorCheck( OK, OK, ERR_RF_GEN_NOT_READY );

	iResult |= m_setupHelpers.SbcSendAllLogs();

	return iResult;
}

/**
 * @brief	This method tests the interaction of SetACInput.
 *
 * @return  result as an int: 0 (UNIT_TEST_PASSED) if passed, not 0 if failed
 */
MCUNIT_TEST( CRFGeneratorTest, RFGen_On_Fail_On_InitCmd_UART_BAD_CHECKSUM )
{
	int iResult = UNIT_TEST_PASSED;
	SHdwSimData * pHdwSimData;
	twobytes tbIndex;

	SetInitValues();
	iResult = m_setupHelpers.StartConnectToRFGen( RF_GEN_POWER_UP_CYCLE_COUNT );

	// Initial command doesn't have a checksum so passes and then fails in setup
	//	iResult |= McUnitAssertIsEqual( OK, GetRfGenModel( &eModel ), "GetRfGenModel", true );
	//for ( int i = 0; i < ( eModel == RF_MODEL_ITHERM ? 1 : 5 ); i++ )
	iResult |= PerformWriteReadErrorCheck( OK, OK, ERR_RF_GEN_NOT_READY );

	f_simDataAccess.GetSimData( &pHdwSimData, &tbIndex );
	pHdwSimData->m_sbcAction = UART_NORMAL;
	pHdwSimData->m_bRfGenAction = UART_BAD_CHECKSUM;
	f_simDataAccess.SetSimData( pHdwSimData );
	for ( uint8_t i = 0; i < COMM_ERR_TOLERANCE - 1; i++ )
		iResult |= PerformWriteReadErrorCheck( ERR_RF_GEN_BAD_CHCK, ERR_RF_GEN_BAD_CHCK, ERR_RF_GEN_NOT_READY );
	iResult |= PerformWriteReadErrorCheck( ERR_RF_GEN_COMM_FATAL, OK, ERR_RF_GEN_NOT_READY );
	iResult |= PerformWriteReadErrorCheck( OK, OK, ERR_RF_GEN_NOT_READY );

	iResult |= m_setupHelpers.SbcSendAllLogs();

	return iResult;
}

/**
 * @brief	This method tests the interaction of SetACInput.
 *
 * @return  result as an int: 0 (UNIT_TEST_PASSED) if passed, not 0 if failed
 */
MCUNIT_TEST( CRFGeneratorTest, RFGen_On_Fail_On_InitCmd_UART_BAD_RESP )
{
	int iResult = UNIT_TEST_PASSED;
	SHdwSimData * pHdwSimData;
	twobytes tbIndex;

	SetInitValues();
	iResult = m_setupHelpers.StartConnectToRFGen( RF_GEN_POWER_UP_CYCLE_COUNT );

	f_simDataAccess.GetSimData( &pHdwSimData, &tbIndex );
	pHdwSimData->m_sbcAction = UART_NORMAL;
	pHdwSimData->m_bRfGenAction = UART_BAD_RESP;
	f_simDataAccess.SetSimData( pHdwSimData );

	iResult |= PerformWriteReadErrorCheck( OK, OK, ERR_RF_GEN_NOT_READY );
	iResult |= PerformWriteReadErrorCheck( OK, OK, ERR_RF_GEN_NOT_READY );
	iResult |= PerformWriteReadErrorCheck( OK, OK, ERR_RF_GEN_NOT_READY );
	iResult |= PerformWriteReadErrorCheck( OK, OK, ERR_RF_GEN_NOT_READY );

	iResult |= m_setupHelpers.SbcSendAllLogs();

	return iResult;
}

/**
 * @brief	This method tests the interaction of SetACInput.
 *
 * @return  result as an int: 0 (UNIT_TEST_PASSED) if passed, not 0 if failed
 */
MCUNIT_TEST( CRFGeneratorTest, RFGen_On_Fail_On_InitCmd_UART_BAD_COUNT )
{
	int iResult = UNIT_TEST_PASSED;
	SHdwSimData * pHdwSimData;
	twobytes tbIndex;

	SetInitValues();
	iResult = m_setupHelpers.StartConnectToRFGen( RF_GEN_POWER_UP_CYCLE_COUNT );

	// Send initial command
	iResult |= PerformWriteReadErrorCheck( OK, OK, ERR_RF_GEN_NOT_READY );

	f_simDataAccess.GetSimData( &pHdwSimData, &tbIndex );
	pHdwSimData->m_sbcAction = UART_NORMAL;
	pHdwSimData->m_bRfGenAction = UART_BAD_COUNT;
	f_simDataAccess.SetSimData( pHdwSimData );

	for ( uint8_t i = 0; i < COMM_ERR_TOLERANCE - 1; i++ )
		iResult |= PerformWriteReadErrorCheck( ERR_RF_GEN_BAD_RESP, ERR_RF_GEN_BAD_RESP, ERR_RF_GEN_NOT_READY );
	iResult |= PerformWriteReadErrorCheck( ERR_RF_GEN_COMM_FATAL, OK, ERR_RF_GEN_NOT_READY );

	iResult |= PerformWriteReadErrorCheck( OK, OK, ERR_RF_GEN_NOT_READY );
	iResult |= PerformWriteReadErrorCheck( OK, OK, ERR_RF_GEN_NOT_READY );

	iResult |= m_setupHelpers.SbcSendAllLogs();

	return iResult;
}

/**
 * @brief	This method tests the interaction of SetACInput.
 *
 * @return  result as an int: 0 (UNIT_TEST_PASSED) if passed, not 0 if failed
 */
MCUNIT_TEST( CRFGeneratorTest, RFGen_On_Fail_On_InitCmd_UART_SEND_GARBAGE )
{
	int iResult = UNIT_TEST_PASSED;
	SHdwSimData * pHdwSimData;
	twobytes tbIndex;

	SetInitValues();
	iResult = m_setupHelpers.StartConnectToRFGen( RF_GEN_POWER_UP_CYCLE_COUNT );

	f_simDataAccess.GetSimData( &pHdwSimData, &tbIndex );
	pHdwSimData->m_sbcAction = UART_NORMAL;
	pHdwSimData->m_bRfGenAction = UART_SEND_GARBAGE;
	f_simDataAccess.SetSimData( pHdwSimData );

	iResult |= PerformWriteReadErrorCheck( OK, OK, ERR_RF_GEN_NOT_READY );
	iResult |= PerformWriteReadErrorCheck( OK, OK, ERR_RF_GEN_NOT_READY );
	iResult |= PerformWriteReadErrorCheck( OK, OK, ERR_RF_GEN_NOT_READY );
	iResult |= PerformWriteReadErrorCheck( OK, OK, ERR_RF_GEN_NOT_READY );

	iResult |= m_setupHelpers.SbcSendAllLogs();

	return iResult;
}

/**
 * @brief	This method tests the interaction of SetACInput.
 *
 * @return  result as an int: 0 (UNIT_TEST_PASSED) if passed, not 0 if failed
 */
MCUNIT_TEST( CRFGeneratorTest, RFGen_On_Fail_On_InitCmd_UART_PAD_GARBAGE )
{
	int iResult = UNIT_TEST_PASSED;
	SHdwSimData * pHdwSimData;
	twobytes tbIndex;

	SetInitValues();
	iResult = m_setupHelpers.StartConnectToRFGen( RF_GEN_POWER_UP_CYCLE_COUNT );

	f_simDataAccess.GetSimData( &pHdwSimData, &tbIndex );
	pHdwSimData->m_sbcAction = UART_NORMAL;
	pHdwSimData->m_bRfGenAction = UART_PAD_GARBAGE;
	f_simDataAccess.SetSimData( pHdwSimData );

	upRFGeneratorModel eModel;
	iResult |= McUnitAssertIsEqual( OK, g_pRFGen->GetRfGenModel( &eModel ), "GetRfGenModel", true );
	for ( int i = 0; i < ( eModel == RF_MODEL_ITHERM ? ITHERM_START_UP_RW : RDO_START_UP_RW ); i++ )
		iResult |= PerformWriteReadErrorCheck( OK, OK, ERR_RF_GEN_NOT_READY );
	iResult |= PerformWriteReadErrorCheck( OK, OK, OK );
	iResult |= PerformWriteReadErrorCheck( OK, OK, OK );
	iResult |= PerformWriteReadErrorCheck( OK, OK, OK );

	iResult |= m_setupHelpers.SbcSendAllLogs();

	return iResult;
}

/**
 * @brief	This method tests the interaction of SetACInput.
 *
 * @return  result as an int: 0 (UNIT_TEST_PASSED) if passed, not 0 if failed
 */
MCUNIT_TEST( CRFGeneratorTest, RFGen_On_Fail_On_InitSetup_UART_NO_RESP )
{
	int iResult = UNIT_TEST_PASSED;
	SHdwSimData * pHdwSimData;
	twobytes tbIndex;

	SetInitValues();
	iResult = m_setupHelpers.StartConnectToRFGen( RF_GEN_POWER_UP_CYCLE_COUNT + ITHERM_SEND_INITIAL_COMMAND );

	f_simDataAccess.GetSimData( &pHdwSimData, &tbIndex );
	pHdwSimData->m_sbcAction = UART_NORMAL;
	pHdwSimData->m_bRfGenAction = UART_NO_RESP;
	f_simDataAccess.SetSimData( pHdwSimData );

	for ( uint8_t i = 0; i < COMM_ERR_TOLERANCE - 1; i++ )
		iResult |= PerformWriteReadErrorCheck( ERR_TIMEOUT, ERR_TIMEOUT, ERR_RF_GEN_NOT_READY );
	iResult |= PerformWriteReadErrorCheck( ERR_RF_GEN_COMM_FATAL, OK, ERR_RF_GEN_NOT_READY );
	iResult |= PerformWriteReadErrorCheck( OK, OK, ERR_RF_GEN_NOT_READY );

	iResult |= m_setupHelpers.SbcSendAllLogs();

	return iResult;
}

/**
 * @brief	This method tests the interaction of SetACInput.
 *
 * @return  result as an int: 0 (UNIT_TEST_PASSED) if passed, not 0 if failed
 */
MCUNIT_TEST( CRFGeneratorTest, RFGen_On_Fail_On_InitSetup_UART_BAD_CHECKSUM )
{
	int iResult = UNIT_TEST_PASSED;
	SHdwSimData * pHdwSimData;
	twobytes tbIndex;

	SetInitValues();
	iResult = m_setupHelpers.StartConnectToRFGen( RF_GEN_POWER_UP_CYCLE_COUNT + ITHERM_SEND_INITIAL_COMMAND );

	f_simDataAccess.GetSimData( &pHdwSimData, &tbIndex );
	pHdwSimData->m_sbcAction = UART_NORMAL;
	pHdwSimData->m_bRfGenAction = UART_BAD_CHECKSUM;
	f_simDataAccess.SetSimData( pHdwSimData );

	for ( uint8_t i = 0; i < COMM_ERR_TOLERANCE - 1; i++ )
		iResult |= PerformWriteReadErrorCheck( ERR_RF_GEN_BAD_CHCK, ERR_RF_GEN_BAD_CHCK, ERR_RF_GEN_NOT_READY );
	iResult |= PerformWriteReadErrorCheck( ERR_RF_GEN_COMM_FATAL, OK, ERR_RF_GEN_NOT_READY );
	iResult |= PerformWriteReadErrorCheck( OK, OK, ERR_RF_GEN_NOT_READY );

	iResult |= m_setupHelpers.SbcSendAllLogs();

	return iResult;
}

/**
 * @brief	This method tests the interaction of SetACInput.
 *
 * @return  result as an int: 0 (UNIT_TEST_PASSED) if passed, not 0 if failed
 */
MCUNIT_TEST( CRFGeneratorTest, RFGen_On_Fail_On_InitSetup_UART_BAD_RESP )
{
	int iResult = UNIT_TEST_PASSED;
	SHdwSimData * pHdwSimData;
	twobytes tbIndex;

	SetInitValues();
	iResult = m_setupHelpers.StartConnectToRFGen( RF_GEN_POWER_UP_CYCLE_COUNT + ITHERM_SEND_INITIAL_COMMAND );

	f_simDataAccess.GetSimData( &pHdwSimData, &tbIndex );
	pHdwSimData->m_sbcAction = UART_NORMAL;
	pHdwSimData->m_bRfGenAction = UART_BAD_RESP;
	f_simDataAccess.SetSimData( pHdwSimData );

	for ( uint8_t i = 0; i < COMM_ERR_TOLERANCE - 1; i++ )
		iResult |= PerformWriteReadErrorCheck( ERR_RF_GEN_BAD_RESP, ERR_RF_GEN_BAD_RESP, ERR_RF_GEN_NOT_READY );
	iResult |= PerformWriteReadErrorCheck( ERR_RF_GEN_COMM_FATAL, OK, ERR_RF_GEN_NOT_READY );
	iResult |= PerformWriteReadErrorCheck( OK, OK, ERR_RF_GEN_NOT_READY );

	iResult |= m_setupHelpers.SbcSendAllLogs();

	return iResult;
}

/**
 * @brief	This method tests the interaction of SetACInput.
 *
 * @return  result as an int: 0 (UNIT_TEST_PASSED) if passed, not 0 if failed
 */
MCUNIT_TEST( CRFGeneratorTest, RFGen_On_Fail_On_InitSetup_UART_BAD_COUNT )
{
	int iResult = UNIT_TEST_PASSED;
	SHdwSimData * pHdwSimData;
	twobytes tbIndex;

	SetInitValues();
	iResult = m_setupHelpers.StartConnectToRFGen( RF_GEN_POWER_UP_CYCLE_COUNT + ITHERM_SEND_INITIAL_COMMAND );

	f_simDataAccess.GetSimData( &pHdwSimData, &tbIndex );
	pHdwSimData->m_sbcAction = UART_NORMAL;
	pHdwSimData->m_bRfGenAction = UART_BAD_COUNT;
	f_simDataAccess.SetSimData( pHdwSimData );

	// No count error in setup
	for ( uint8_t i = 0; i < COMM_ERR_TOLERANCE - 1; i++ )
		iResult |= PerformWriteReadErrorCheck( ERR_RF_GEN_BAD_RESP, ERR_RF_GEN_BAD_RESP, ERR_RF_GEN_NOT_READY );
	iResult |= PerformWriteReadErrorCheck( ERR_RF_GEN_COMM_FATAL, OK, ERR_RF_GEN_NOT_READY );

	iResult |= PerformWriteReadErrorCheck( OK, OK, ERR_RF_GEN_NOT_READY );
	iResult |= PerformWriteReadErrorCheck( OK, OK, ERR_RF_GEN_NOT_READY );

	iResult |= m_setupHelpers.SbcSendAllLogs();

	return iResult;
}

/**
 * @brief	This method tests the interaction of SetACInput.
 *
 * @return  result as an int: 0 (UNIT_TEST_PASSED) if passed, not 0 if failed
 */
MCUNIT_TEST( CRFGeneratorTest, RFGen_On_Fail_On_InitSetup_UART_SEND_GARBAGE )
{
	int iResult = UNIT_TEST_PASSED;
	SHdwSimData * pHdwSimData;
	twobytes tbIndex;

	SetInitValues();
	iResult = m_setupHelpers.StartConnectToRFGen( RF_GEN_POWER_UP_CYCLE_COUNT + ITHERM_SEND_INITIAL_COMMAND );

	f_simDataAccess.GetSimData( &pHdwSimData, &tbIndex );
	pHdwSimData->m_sbcAction = UART_NORMAL;
	pHdwSimData->m_bRfGenAction = UART_SEND_GARBAGE;
	f_simDataAccess.SetSimData( pHdwSimData );

	for ( uint8_t i = 0; i < COMM_ERR_TOLERANCE - 1; i++ )
		iResult |= PerformWriteReadErrorCheck( ERR_RF_GEN_BAD_RESP, ERR_RF_GEN_BAD_RESP, ERR_RF_GEN_NOT_READY );
	iResult |= PerformWriteReadErrorCheck( ERR_RF_GEN_COMM_FATAL, OK, ERR_RF_GEN_NOT_READY );
	iResult |= PerformWriteReadErrorCheck( OK, OK, ERR_RF_GEN_NOT_READY );

	iResult |= m_setupHelpers.SbcSendAllLogs();

	return iResult;
}

/**
 * @brief	This method tests the interaction of SetACInput.
 *
 * @return  result as an int: 0 (UNIT_TEST_PASSED) if passed, not 0 if failed
 */
MCUNIT_TEST( CRFGeneratorTest, RFGen_On_Fail_On_InitSetup_UART_PAD_GARBAGE )
{
	int iResult = UNIT_TEST_PASSED;
	SHdwSimData * pHdwSimData;
	twobytes tbIndex;

	SetInitValues();
	iResult = m_setupHelpers.StartConnectToRFGen( RF_GEN_POWER_UP_CYCLE_COUNT + ITHERM_SEND_INITIAL_COMMAND );

	f_simDataAccess.GetSimData( &pHdwSimData, &tbIndex );

	upRFGeneratorModel eModel;
	iResult |= McUnitAssertIsEqual( OK, g_pRFGen->GetRfGenModel( &eModel ), "GetRfGenModel", true );
	for ( int i = 0; i < ( eModel == RF_MODEL_ITHERM ? 0 : RDO_START_UP_RW - 1 ); i++ )
		iResult |= PerformWriteReadErrorCheck( OK, OK, ERR_RF_GEN_NOT_READY );
	iResult |= PerformWriteReadErrorCheck( OK, OK, OK );
	iResult |= PerformWriteReadErrorCheck( OK, OK, OK );
	iResult |= PerformWriteReadErrorCheck( OK, OK, OK );

	pHdwSimData->m_sbcAction = UART_NORMAL;
	pHdwSimData->m_sbcAction = UART_NORMAL;
	pHdwSimData->m_bRfGenAction = UART_PAD_GARBAGE;
	f_simDataAccess.SetSimData( pHdwSimData );

	iResult |= PerformWriteReadErrorCheck( OK, OK, OK );
	iResult |= PerformWriteReadErrorCheck( OK, OK, OK );
	iResult |= PerformWriteReadErrorCheck( OK, OK, OK );

	iResult |= m_setupHelpers.SbcSendAllLogs();

	return iResult;
}

/**
 * @brief	This method tests the interaction of SetACInput.
 *
 * @return  result as an int: 0 (UNIT_TEST_PASSED) if passed, not 0 if failed
 */
MCUNIT_TEST( CRFGeneratorTest, RFGen_Status_Command_Components )
{
	int iResult = UNIT_TEST_PASSED;
	SHdwSimData sHdwSimData;

	SetInitValues();
	iResult = m_setupHelpers.StartConnectToRFGen( RF_GEN_POWER_UP_CYCLE_COUNT + RF_GEN_COMPLETE_POWER_UP );

	iResult |= PerformWriteReadErrorCheck( OK, OK, OK );
	iResult |= PerformWriteReadErrorCheck( OK, OK, OK );

	sHdwSimData.m_tbRfGenPower = 120;
	sHdwSimData.m_tbRfGenStatus = 0x06;
	sHdwSimData.m_bRfGenError = 0x00;
	f_simDataAccess.SetSimData( &sHdwSimData );
	twobytes tbPower;

	iResult |= PerformWriteReadErrorCheck( OK, OK, OK );

	iResult |= McUnitAssertIsEqual( OK, g_pRFGen->GetPowerOutput( &tbPower ), "GetPowerOutput" );
	iResult |= McUnitAssertIsEqual( (twobytes) 120, tbPower, "GetPowerOutput value" );

	iResult |= McUnitAssertIsEqual( OK, g_pRFGen->GetStatus( &tbPower ), "GetStatus" );
	iResult |= McUnitAssertIsEqual( (twobytes) 0x06, tbPower, "GetStatus value" );

	sHdwSimData.m_tbRfGenPower = 320;
	sHdwSimData.m_tbRfGenStatus = 0x05;
	f_simDataAccess.SetSimData( &sHdwSimData );

	iResult |= PerformWriteReadErrorCheck( OK, OK, OK );

	iResult |= McUnitAssertIsEqual( OK, g_pRFGen->GetPowerOutput( &tbPower ), "GetPowerOutput" );
	iResult |= McUnitAssertIsEqual( (twobytes) 320, tbPower, "GetPowerOutput value" );

	iResult |= McUnitAssertIsEqual( OK, g_pRFGen->GetStatus( &tbPower ), "GetStatus" );
	iResult |= McUnitAssertIsEqual( (twobytes) 0x05, tbPower, "GetStatus value" );

	iResult |= m_setupHelpers.SbcSendAllLogs();

	return iResult;
}

/**
 * @brief	This method tests the interaction of SetACInput.
 *
 * @return  result as an int: 0 (UNIT_TEST_PASSED) if passed, not 0 if failed
 */
MCUNIT_TEST( CRFGeneratorTest, RFGen_On_Fail_Not_Found )
{
	int iResult = UNIT_TEST_PASSED;
	twobytes tbErr;
	SHdwSimData * pHdwSimData;
	twobytes tbIndex;

	SetInitValues();
	f_simDataAccess.GetSimData( &pHdwSimData, &tbIndex );

	iResult |= McUnitAssertIsEqual( OK, g_pRFGen->InitializeHardware(), "InitializeHardware" );

	iResult |= McUnitAssertIsEqual( OK, g_pRFGen->SetACInput( false ), "SetACInput( false )" );
	iResult |= McUnitAssertIsEqual( OK, g_pRFGen->SetACInput( true ), "SetACInput( true )" );

	pHdwSimData->m_sbcAction = UART_NORMAL;
	pHdwSimData->m_bRfGenAction = UART_NO_RESP;
	f_simDataAccess.SetSimData( pHdwSimData );

	for ( int i = 0; i < RF_GEN_POWER_UP_CYCLE_COUNT + RF_GEN_POWER_UP_CYCLE_MAX; i++ )
		{
		iResult
		        |= McUnitAssertIsEqual( OK, g_pRFGen->WriteToHardware(), "Waiting for response: WriteToHardware", false );
		iResult |= McUnitAssertIsEqual( OK, g_pRFGen->ReadFromHardware(), "Waiting for response: ReadFromHardware",
		        false );
		iResult |= McUnitAssertIsEqual( ERR_RF_GEN_NOT_READY, g_pRFGen->GetError( &tbErr ),
		        "Waiting for response: GetError", false );
		}

	iResult |= McUnitAssertIsEqual( ERR_RF_GEN_NOT_FOUND, g_pRFGen->WriteToHardware(), "WriteToHardware" );
	iResult |= McUnitAssertIsEqual( OK, g_pRFGen->ReadFromHardware(), "ReadFromHardware" );
	iResult |= McUnitAssertIsEqual( ERR_RF_GEN_NOT_READY, g_pRFGen->GetError( &tbErr ), "GetError" );
	iResult |= McUnitAssertIsEqual( ERR_RF_GEN_NOT_FOUND, g_pRFGen->GetStatus( &tbErr ), "GetStatus" );

	iResult |= McUnitAssertIsEqual( OK, g_pRFGen->WriteToHardware(), "WriteToHardware" );
	iResult |= McUnitAssertIsEqual( OK, g_pRFGen->ReadFromHardware(), "ReadFromHardware" );
	iResult |= McUnitAssertIsEqual( ERR_RF_GEN_NOT_READY, g_pRFGen->GetError( &tbErr ), "GetError" );
	iResult |= McUnitAssertIsEqual( ERR_RF_GEN_NOT_FOUND, g_pRFGen->GetStatus( &tbErr ), "GetStatus" );

	iResult |= McUnitAssertIsEqual( OK, g_pRFGen->WriteToHardware(), "WriteToHardware" );
	iResult |= McUnitAssertIsEqual( OK, g_pRFGen->ReadFromHardware(), "ReadFromHardware" );
	iResult |= McUnitAssertIsEqual( ERR_RF_GEN_NOT_READY, g_pRFGen->GetError( &tbErr ), "GetError" );
	iResult |= McUnitAssertIsEqual( ERR_RF_GEN_NOT_FOUND, g_pRFGen->GetStatus( &tbErr ), "GetStatus" );

	iResult |= m_setupHelpers.SbcSendAllLogs();

	return iResult;
}

/**
 * @brief	This method tests the interaction of SetACInput.
 *
 * @return  result as an int: 0 (UNIT_TEST_PASSED) if passed, not 0 if failed
 */
MCUNIT_TEST( CRFGeneratorTest, RFGen_On_Comm_Fatal )
{
	int iResult = UNIT_TEST_PASSED;
	twobytes tbErr;
	SHdwSimData * pHdwSimData;
	twobytes tbIndex;

	SetInitValues();
	iResult = m_setupHelpers.StartConnectToRFGen( RF_GEN_POWER_UP_CYCLE_COUNT + RF_GEN_COMPLETE_POWER_UP );

	f_simDataAccess.GetSimData( &pHdwSimData, &tbIndex );
	pHdwSimData->m_sbcAction = UART_NORMAL;
	pHdwSimData->m_bRfGenAction = UART_BAD_RESP;
	f_simDataAccess.SetSimData( pHdwSimData );

	iResult |= PerformWriteReadErrorCheck( OK, OK, OK );

	pHdwSimData->m_sbcAction = UART_NORMAL;
	pHdwSimData->m_bRfGenAction = UART_NO_RESP;
	f_simDataAccess.SetSimData( pHdwSimData );

	iResult |= PerformWriteReadErrorCheck( OK, OK, OK );

	pHdwSimData->m_sbcAction = UART_NORMAL;
	pHdwSimData->m_bRfGenAction = UART_BAD_CHECKSUM;
	f_simDataAccess.SetSimData( pHdwSimData );

	iResult |= PerformWriteReadErrorCheck( OK, ERR_RF_GEN_RESET, ERR_RF_GEN_RESET );
	iResult |= PerformWriteReadErrorCheck( OK, ERR_RF_GEN_RESET, ERR_RF_GEN_RESET );

	iResult |= McUnitAssertIsEqual( ERR_RF_GEN_RESET, g_pRFGen->GetStatus( &tbErr ), "GetStatus" );

	iResult |= m_setupHelpers.SbcSendAllLogs();

	return iResult;
}

/**
 * @brief	This method tests the interaction of SetPowerRequest
 *
 * @return  result as an int: 0 (UNIT_TEST_PASSED) if passed, not 0 if failed
 */
MCUNIT_TEST( CRFGeneratorTest, RFGen_PowerSet )
{
	int iResult = UNIT_TEST_PASSED;
	twobytes tbErr;
	SHdwSimData * pHdwSimData;
	twobytes tbIndex;

	f_simDataAccess.GetSimData( &pHdwSimData, &tbIndex );
	pHdwSimData->m_sbcAction = UART_NORMAL;
	pHdwSimData->m_bRfGenAction = UART_NORMAL;
	f_simDataAccess.SetSimData( pHdwSimData );

	SetInitValues();
	iResult = m_setupHelpers.StartConnectToRFGen( RF_GEN_POWER_UP_CYCLE_COUNT + RF_GEN_COMPLETE_POWER_UP );

	iResult |= McUnitAssertIsEqual( OK, g_pRFGen->SetOutputEnable( true ), "SetOutputEnable" );
	iResult |= PerformWriteReadErrorCheck( OK, OK, OK );
	iResult |= PerformWriteReadErrorCheck( OK, OK, OK );
	iResult |= m_setupHelpers.SbcSendAllLogs();

	iResult |= McUnitAssertIsEqual( OK, g_pRFGen->SetPowerRequest( 120 ), "SetPowerRequest" );
	iResult |= PerformWriteReadErrorCheck( OK, OK, OK );
	iResult |= PerformWriteReadErrorCheck( OK, OK, OK );
	iResult |= m_setupHelpers.SbcSendAllLogs();

	pHdwSimData->m_sbcAction = UART_NORMAL;
	pHdwSimData->m_bRfGenAction = UART_BAD_RESP;
	f_simDataAccess.SetSimData( pHdwSimData );
	iResult |= McUnitAssertIsEqual( OK, g_pRFGen->SetPowerRequest( 70 ), "SetPowerRequest" );

	for ( uint8_t i = 0; i < COMM_ERR_TOLERANCE - 1; i++ )
		iResult |= PerformWriteReadErrorCheck( OK, OK, OK );
	iResult |= m_setupHelpers.SbcSendAllLogs();

	iResult |= PerformWriteReadErrorCheck( OK, ERR_RF_GEN_RESET, ERR_RF_GEN_RESET );
	g_pRFGen->SetACInput( false );

	iResult |= m_setupHelpers.SbcSendAllLogs();
	iResult |= PerformWriteReadErrorCheck( OK, OK, ERR_RF_GEN_NOT_READY );
	iResult |= m_setupHelpers.SbcSendAllLogs();
	iResult |= PerformWriteReadErrorCheck( OK, OK, ERR_RF_GEN_NOT_READY );
	iResult |= m_setupHelpers.SbcSendAllLogs();

	iResult |= McUnitAssertIsEqual( OK, g_pRFGen->GetStatus( &tbErr ), "GetStatus" );

	iResult |= m_setupHelpers.SbcSendAllLogs();

	return iResult;
}


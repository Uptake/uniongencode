/**
 * @file	RFGeneratorTest.h
 * @par		Package: CvasMcu
 * @par		Project: CVAS Generator
 * @par		Copyright (c) 2016, Uptake Medical, Inc.
 * @author	Kurt Jan 12, 2012 Created file
 * @brief	This file contains unit tests for CRFGenerator
 */

#ifndef RFGENERATORTEST_H_
#define RFGENERATORTEST_H_

// Include Files
#include <RFGenerator.h>
#include "McUnit.h"
#include "UnitTestIntfc.h"
#include "TestMain.h"
#include "SetupHelpers.h"

// Referenced classes

// Public Macros and Constants

// Public Type Definitions (Enums, Structs & Classes)

/**
 * @class	CRFGeneratorTest
 * @brief	This class provides unit tests for CRFGenerator
 *
 * Virtual Functions Overridden: @n@n
 *  UnitTestListSetup - Set up list of Suites, Cases and Tests to be execute @n
 * 	CommonTesthandle - Called to have tests run @n
 *
 * External Data Members: None @n@n
 *
 */
class CRFGeneratorTest:  private CMcUnit<CRFGeneratorTest> , private CUnitTestIntfc, public CTestMain,
        private CRFGenerator
{
CSetupHelpers m_setupHelpers;

private:

	// Case 1: Invocation Tests
	MCUNIT_TEST_DEC( AllPublicMethods_Invocation );
	// Case 2: Parameter Tests
	MCUNIT_TEST_DEC( SetGetMethods_Parameter );
	// Case 3: Interaction Tests
	MCUNIT_TEST_DEC( SetACInput_Interaction );
	MCUNIT_TEST_DEC( RFGen_On_Fail_On_InitCmd_UART_NO_RESP );
	MCUNIT_TEST_DEC( RFGen_On_Fail_On_InitCmd_UART_BAD_CHECKSUM );
	MCUNIT_TEST_DEC( RFGen_On_Fail_On_InitCmd_UART_BAD_RESP );
	MCUNIT_TEST_DEC( RFGen_On_Fail_On_InitCmd_UART_BAD_COUNT );
	MCUNIT_TEST_DEC( RFGen_On_Fail_On_InitCmd_UART_SEND_GARBAGE );
	MCUNIT_TEST_DEC( RFGen_On_Fail_On_InitCmd_UART_PAD_GARBAGE );
	MCUNIT_TEST_DEC( RFGen_On_Fail_On_InitSetup_UART_NO_RESP );
	MCUNIT_TEST_DEC( RFGen_On_Fail_On_InitSetup_UART_BAD_CHECKSUM );
	MCUNIT_TEST_DEC( RFGen_On_Fail_On_InitSetup_UART_BAD_RESP );
	MCUNIT_TEST_DEC( RFGen_On_Fail_On_InitSetup_UART_BAD_COUNT );
	MCUNIT_TEST_DEC( RFGen_On_Fail_On_InitSetup_UART_SEND_GARBAGE );
	MCUNIT_TEST_DEC( RFGen_On_Fail_On_InitSetup_UART_PAD_GARBAGE );
	MCUNIT_TEST_DEC( RFGen_Status_Command_Components );
	MCUNIT_TEST_DEC( RFGen_On_Fail_Not_Found );
	MCUNIT_TEST_DEC( RFGen_On_Comm_Fatal );
	MCUNIT_TEST_DEC( RFGen_PowerSet );
	SMcUnitTest sMcUnitTestList[ 20 ]; // Increment index for each test added, regardless of the case in which it falls
	SMcUnitCase sMcUnitCase[ 3 ];
	SMcUnitCase * sMcUnitCaseList[ 4 ];
	SMcUnitSuite sMcUnitTestSuite;

	int PerformWriteReadErrorCheck( upCvasErr eWrite, upCvasErr eRead, upCvasErr eErr );

public:
	CRFGeneratorTest();
	int CommonTestHandle();
	void UnitTestListSetup();

};

// Inline Functions (follows class definition)


#endif /* RFGENERATORTEST_H_ */

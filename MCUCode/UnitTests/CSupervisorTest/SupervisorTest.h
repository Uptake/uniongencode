/**
 * @file	SupervisorTest.h
 * @par		Package: CvasMcu
 * @par		Project: CVAS Generator
 * @par		Copyright (c) 2016, Uptake Medical, Inc.
 * @author	Kurt Jan 12, 2012 Created file
 * @brief	This file contains unit tests for CSupervisor
 */

#ifndef SUPERVISORTEST_H_
#define SUPERVISORTEST_H_

// Include Files
#include <Supervisor.h>
#include "McUnit.h"
#include "UnitTestIntfc.h"
#include "TestMain.h"
#include "SetupHelpers.h"

// Referenced classes

// Public Macros and Constants

// Public Type Definitions (Enums, Structs & Classes)

/**
 * @class	CSupervisorTest
 * @brief	This class provides unit tests for CSupervisor
 *
 * Virtual Functions Overridden: @n@n
 *  UnitTestListSetup - Set up list of Suites, Cases and Tests to be execute @n
 * 	CommonTesthandle - Called to have tests run @n
 *
 * External Data Members: None @n@n
 *
 */
class CSupervisorTest: private CMcUnit<CSupervisorTest> , public CUnitTestIntfc, public CTestMain, private CSupervisor
{
	CSetupHelpers m_setupHelpers;

public:
	CSupervisorTest();
	int CommonTestHandle();
	void UnitTestListSetup();

	// Case 1: Invocation Tests
	MCUNIT_TEST_DEC( AllPublicMethods_Invocation );

	// Case 2: Parameter Tests
	MCUNIT_TEST_DEC( OperState_Parameter );

	// Case 3: Interaction Tests
	MCUNIT_TEST_DEC( Setup_for_Interaction );
	MCUNIT_TEST_DEC( TestFail_Interaction );
	MCUNIT_TEST_DEC( TestSuccess_Interaction );
	MCUNIT_TEST_DEC( Idle_SbcTimeout_Interaction );
	MCUNIT_TEST_DEC( IdleSbcNoInterlock_Interaction );
	MCUNIT_TEST_DEC( IdleToConnect_Interaction );
	MCUNIT_TEST_DEC( IdleRfGenTerm_Interaction );

	SMcUnitTest sMcUnitTestList[ 16 ]; // Increment index for each test added, regardless of the case in which it falls
	SMcUnitCase sMcUnitCase[ 11 ];
	SMcUnitCase * sMcUnitCaseList[ 11 ];
	SMcUnitSuite sMcUnitTestSuite;
};

// Inline Functions (follows class definition)


#endif /* SUPERVISORTEST_H_ */

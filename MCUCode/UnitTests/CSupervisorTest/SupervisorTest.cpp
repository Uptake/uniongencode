/**
 * @file	SupervisorTest.cpp
 * @par		Package: CvasMcu
 * @par		Project: CVAS Generator
 * @par		Copyright (c) 2016, Uptake Medical, Inc.
 * @author	Kurt Jan 12, 2012 Created file
 * @brief	This file contains unit tests for CSupervisor
 */

// Include Files
#include "SupervisorTest.h"
#include "ObjectSingletons.h"
#include "SimDataAccess.h"
#include "SbcSerialIntfc.h"
#include "RFGenerator.h"

// External Public Data

// File Scope Macros and Constants

// File Scope Type Definitions (Enums, Structs & Classes)

// File Scope Data
CSupervisorTest f_SupervisorTest; // Instantiate one so it registers itself

// File Scope Functions

// Public Functions

/**
 * @brief	This is the constructor for CSupervisorTest
 *
 */

CSupervisorTest::CSupervisorTest() :
	CMcUnit<CSupervisorTest> ( this ), CTestMain( (CUnitTestIntfc*) this )
{
}

/**
 * @brief	This method calls into CMcUnit to have the tests run.
 *
 * @return   Always 1
 */
int CSupervisorTest::CommonTestHandle()
{
	McUnitSuiteRun( &sMcUnitTestSuite );
	McUnitSuiteReport( &sMcUnitTestSuite, DEPTH_TEST_REPORT );
	return 1;
}

/**
 * @brief	This method sets up the lists of tests to run
 *
 */
void CSupervisorTest::UnitTestListSetup()
{
	int iList = 0;
	int iCase = 0;

	// Set up Case 1: Invocation Tests
	ADD_TEST_CASE( sMcUnitCase, iCase, sMcUnitTestList + iList, 1, "Invocation Tests", "Invocation Test failure")
	ADD_TEST_LIST( sMcUnitTestList, iList, &CSupervisorTest::AllPublicMethods_Invocation, "All Public Methods invocation", 0x0 )

	// Set up Case 2: Parameter Tests
	ADD_TEST_CASE( sMcUnitCase, iCase, sMcUnitTestList + iList, 1, "Parameter Tests", "Parameter Test failure")
	ADD_TEST_LIST( sMcUnitTestList, iList, &CSupervisorTest::OperState_Parameter, "OperState_Parameter parameter", 0x0 )

	// Set up Case 3: Interaction Tests
	ADD_TEST_CASE( sMcUnitCase, iCase, sMcUnitTestList + iList, 1, "Setup", "Interaction Test failure")
	ADD_TEST_LIST( sMcUnitTestList, iList, &CSupervisorTest::Setup_for_Interaction, "Setup", 0x0 )

	ADD_TEST_CASE( sMcUnitCase, iCase, sMcUnitTestList + iList, 2, "TestState", "Interaction Test failure")
	ADD_TEST_LIST( sMcUnitTestList, iList, &CSupervisorTest::TestFail_Interaction, "TestFail", 0x0 )
	ADD_TEST_LIST( sMcUnitTestList, iList, &CSupervisorTest::TestSuccess_Interaction, "TestSuccess", 0x0 )

	ADD_TEST_CASE( sMcUnitCase, iCase, sMcUnitTestList + iList, 4, "IdleState", "Interaction Test failure")
	ADD_TEST_LIST( sMcUnitTestList, iList, &CSupervisorTest::Idle_SbcTimeout_Interaction, "Idle Sbc Timeout", 0x0 )
	ADD_TEST_LIST( sMcUnitTestList, iList, &CSupervisorTest::IdleSbcNoInterlock_Interaction, "Idle SBC no Interlock", 0x0 )
	ADD_TEST_LIST( sMcUnitTestList, iList, &CSupervisorTest::IdleToConnect_Interaction, "Idle to Connect", 0x0 )
	ADD_TEST_LIST( sMcUnitTestList, iList, &CSupervisorTest::IdleRfGenTerm_Interaction, "Idle RF Gen error Terminate", 0x0 )

	MAKE_TEST_SUITE( sMcUnitTestSuite, sMcUnitCaseList, sMcUnitCase, iCase, "CSupervisorTest" )

}

//=============================================================================
// CASE 1: INVOCATION TESTS
//=============================================================================

/**
 * @brief	This method tests the invocation of All Public Methods with valid parameters.
 *
 * @return  result as an int: 0 (UNIT_TEST_PASSED) if passed, not 0 if failed
 */
MCUNIT_TEST( CSupervisorTest, AllPublicMethods_Invocation )
{
	int iResult = UNIT_TEST_PASSED;
	int iInvocations = 0;

	// Invoke and check return codes (if there is one)
	iResult |= McUnitAssertIsOK( SetOperatingConditions(), "SetOperatingConditions" );
	iInvocations++;

	bool_c bOn;
	iResult |= McUnitAssertIsEqual( ERR_NO_DDEVICE, WaitingForTrigger( &bOn ), "WaitingForTrigger" );
	iInvocations++;

	iResult |= McUnitAssertIsEqual( ERR_NO_DDEVICE, CheckHandpiece(), "CheckHandpiece" );
	iInvocations++;

	iResult |= McUnitAssertIsOK( SetOperState( OPER_STATE_BOOT ), "SetOperState" );
	iInvocations++;

	iResult |= McUnitAssertIsEqual( OPER_STATE_BOOT, GetOperState(), "GetOperState" );
	iInvocations++;

	iResult |= McUnitAssertIsOK( TestState(), "TestState" );
	iInvocations++;

	iResult |= McUnitAssertIsOK( IdleState(), "IdleState" );
	iInvocations++;

	iResult |= McUnitAssertIsEqual( ERR_NO_DDEVICE, ConnectedState(), "ConnectedState" );
	iInvocations++;

	iResult |= McUnitAssertIsEqual( ERR_INTERLOCK_MISSING, HandpiecePrimingState(), "PrimingState" );
	iInvocations++;

	iResult |= McUnitAssertIsOK( ReadyState(), "ReadyState" );
	iInvocations++;

	iResult |= McUnitAssertIsEqual( ERR_INTERLOCK_MISSING, DeliveringState(), "TreatingState" );
	iInvocations++;

	iResult |= McUnitAssertIsOK( Execute(), "Execute" );
	iInvocations++;

	iResult |= McUnitAssertIsOK( ErrorState(), "TerminateState" );
	iInvocations++;

	SetOverrunFlag();
	iResult |= McUnitAssertIsOK( OK, "void SetOverrunFlag" );
	iInvocations++;

	// Call Execute() again with overrun flag set
	iResult |= McUnitAssertIsOK( Execute(), "Execute() with overrun flag set" );

	LogOneValue( "Number of methods tested: %d", iInvocations );

	return iResult;
}

//=============================================================================
// CASE 2: PARAMETER TESTS
//=============================================================================

/**
 * @brief	This method tests the passed parameter(s) of CSupervisor.
 *
 * @return  result as an int: 0 (UNIT_TEST_PASSED) if passed, not 0 if failed
 */

MCUNIT_TEST( CSupervisorTest, OperState_Parameter )
{
	int iResult = UNIT_TEST_PASSED;
	int iInvocations = 0;
	bool_c bOn;

	iResult |= McUnitAssertIsOK( SetOperState( OPER_STATE_BOOT ), "SetOperState BOOT" );
	iResult |= McUnitAssertIsEqual( OPER_STATE_BOOT, GetOperState(), "GetOperState BOOT" );
	iInvocations++;

	iResult |= McUnitAssertIsOK( g_pRFGen->SetACInput( true ), "g_pRFGen->SetACInput" );
	iResult |= McUnitAssertIsOK( SetOperState( OPER_STATE_IDLE ), "SetOperState IDLE" );
	iResult |= McUnitAssertIsEqual( OPER_STATE_IDLE, GetOperState(), "GetOperState IDLE" );
	iResult |= McUnitAssertIsOK( g_pRFGen->GetACInput( &bOn ), "g_pRFGen->GetACInput" );
	iResult |= McUnitAssertIsEqual( (bool_c) false, bOn, "ACInput false" );
	iInvocations++;

	iResult |= McUnitAssertIsOK( SetOperState( OPER_STATE_CNCTD ), "SetOperState CNCTD" );
	iResult |= McUnitAssertIsEqual( OPER_STATE_CNCTD, GetOperState(), "GetOperState CNCTD" );
	iInvocations++;

	iResult |= McUnitAssertIsOK( SetOperState( OPER_STATE_HAND_PRIMING ), "SetOperState PRIMING" );
	iResult |= McUnitAssertIsEqual( OPER_STATE_HAND_PRIMING, GetOperState(), "GetOperState PRIMING" );
	iInvocations++;

	iResult |= McUnitAssertIsOK( SetOperState( OPER_STATE_READY ), "SetOperState READY" );
	iResult |= McUnitAssertIsEqual( OPER_STATE_READY, GetOperState(), "GetOperState READY" );
	iInvocations++;

	iResult |= McUnitAssertIsOK( SetOperState( OPER_STATE_ERROR ), "SetOperState TERMINATE" );
	iResult |= McUnitAssertIsEqual( OPER_STATE_ERROR, GetOperState(), "GetOperState TERMINATE" );
	iInvocations++;

	iResult |= McUnitAssertIsOK( SetOperState( OPER_STATE_TEST ), "SetOperState TEST" );
	iResult |= McUnitAssertIsEqual( OPER_STATE_TEST, GetOperState(), "GetOperState TEST" );
	iInvocations++;

	iResult |= McUnitAssertIsOK( SetOperState( OPER_STATE_DELIVERING ), "SetOperState TREATING" );
	iResult |= McUnitAssertIsEqual( OPER_STATE_DELIVERING, GetOperState(), "GetOperState TREATING" );
	iInvocations++;

	iResult |= McUnitAssertIsOK( SetOperState( OPER_STATE_PENDING ), "SetOperState PENDING" );
	iResult |= McUnitAssertIsEqual( OPER_STATE_PENDING, GetOperState(), "GetOperState PENDING" );
	iInvocations++;

	iResult |= McUnitAssertIsEqual( ERR_BAD_PARAM, SetOperState( (upOperState) 111 ), "SetOperState Error" );
	iResult |= McUnitAssertIsEqual( OPER_STATE_ERROR, GetOperState(), "GetOperState TERMINATE" );
	iInvocations++;

	iResult |= McUnitAssertIsEqual( iInvocations, 1 + OPER_STATE_CNT, "OPER_STATE_CNT+1" );

	LogOneValue( "Number of methods tested: %d", iInvocations );

	return iResult;
}

//=============================================================================
// CASE 3: INTERACTION TESTS
//=============================================================================


/**
 * @brief	This method tests the interaction of TBD.
 *
 * @return  result as an int: 0 (UNIT_TEST_PASSED) if passed, not 0 if failed
 */

MCUNIT_TEST( CSupervisorTest, Setup_for_Interaction )
{
	int iResult = UNIT_TEST_PASSED;

	iResult |= McUnitAssertIsOK( g_timeBase.InitializeHardware(), "g_timeBase.InitializeHardware" );
	iResult |= McUnitAssertIsOK( g_pTemperature->InitializeHardware(), "g_pTemperature->InitializeHardware" );
	iResult |= McUnitAssertIsOK( g_pWaterCntl->InitializeHardware(), "g_pWaterCntl->InitializeHardware" );
	iResult |= McUnitAssertIsOK( g_pDigitalInputOutput->InitializeHardware(),
	        "g_pDigitalInputOutput->InitializeHardware" );
	iResult |= McUnitAssertIsEqual( UNIT_TEST_PASSED, m_setupHelpers.SbcInitAndConnect(), "SbcInitAndConnect" );

	m_setupHelpers.SbcSendAllLogs();

	return iResult;
}
/**
 * @brief	This method tests the interaction of TBD.
 *
 * @return  result as an int: 0 (UNIT_TEST_PASSED) if passed, not 0 if failed
 */

MCUNIT_TEST( CSupervisorTest, TestFail_Interaction )
{
	int iResult = UNIT_TEST_PASSED;

	// Go through without change
	iResult |= McUnitAssertIsOK( SetOperState( OPER_STATE_TEST ), "SetOperState" );
	iResult |= McUnitAssertIsEqual( OPER_STATE_TEST, GetOperState(), "GetOperState" );

	iResult |= McUnitAssertIsOK( TestState(), "TestState" );
	iResult |= McUnitAssertIsEqual( OPER_STATE_TEST, GetOperState(), "GetOperState" );

	// Force failure in self test
	iResult |= McUnitAssertIsOK( g_pSelfTest->SetTestCycle( SELF_TEST_CYCLE_CNT + 1 ), "g_pSelfTest->SetTestCycle" );

	iResult |= McUnitAssertIsEqual( ERR_SELFTEST_WATER_PUMP, TestState(), "TestState" );
	iResult |= McUnitAssertIsEqual( OPER_STATE_ERROR, GetOperState(), "GetOperState" );

	iResult |= McUnitAssertIsOK( g_pSelfTest->SetTestCycle( 0 ), "g_pSelfTest->SetTestCycle" );

	m_setupHelpers.SbcSendAllLogs();

	return iResult;
}

/**
 * @brief	This method tests the interaction of TBD.
 *
 * @return  result as an int: 0 (UNIT_TEST_PASSED) if passed, not 0 if failed
 */

MCUNIT_TEST( CSupervisorTest, TestSuccess_Interaction )
{
	int iResult = UNIT_TEST_PASSED;
	twobytes tbSamples;

	// Run self test to successful completion
	iResult |= McUnitAssertIsOK( SetOperState( OPER_STATE_TEST ), "SetOperState" );
	iResult |= McUnitAssertIsEqual( OPER_STATE_TEST, GetOperState(), "GetOperState" );

	iResult |= McUnitAssertIsEqual( UNIT_TEST_PASSED, m_setupHelpers.LoadSelfTestFile(), "LoadSelfTestFile" );
	tbSamples = f_simDataAccess.GetSampleCount();
	iResult |= McUnitAssertTrue( (bool_c) ( SELF_TEST_CYCLE_CNT < tbSamples ), "Samples" );
	iResult |= McUnitAssertIsOK( g_pSelfTest->SetTestCycle( 0 ), "g_pSelfTest->SetTestCycle" );

	for ( twobytes i = 0; i < SELF_TEST_CYCLE_CNT; i++ )
		{
		iResult |= McUnitAssertIsEqual( UNIT_TEST_PASSED, m_setupHelpers.ReadSimDataRecord(), "ReadSimDataRecord",
		        false );

		iResult |= McUnitAssertIsOK( SetOperatingConditions(), "SetOperatingConditions", false );
		iResult |= McUnitAssertIsOK( TestState(), "TestState", false );
		iResult |= McUnitAssertIsEqual( OPER_STATE_TEST, GetOperState(), "GetOperState", false );

		iResult |= McUnitAssertIsEqual( UNIT_TEST_PASSED, m_setupHelpers.SbcReadWrite(), "SbcReadWrite", false );
		iResult |= McUnitAssertIsOK( g_pWaterCntl->WriteToHardware(), "g_pWaterCntl->WriteToHardware", false );
		iResult |= McUnitAssertIsEqual( UNIT_TEST_PASSED, f_simDataAccess.GoToNextRecord(), "GoToNextRecord", false );
		}

	iResult |= McUnitAssertIsOK( TestState(), "TestState" );
	iResult |= McUnitAssertIsEqual( OPER_STATE_IDLE, GetOperState(), "GetOperState" );

	m_setupHelpers.SbcSendAllLogs();

	return iResult;
}

/**
 * @brief	This method tests the interaction of TBD.
 *
 * @return  result as an int: 0 (UNIT_TEST_PASSED) if passed, not 0 if failed
 */
MCUNIT_TEST( CSupervisorTest, Idle_SbcTimeout_Interaction )
{
	int iResult = UNIT_TEST_PASSED;
	tick tFirst;
	SHdwSimData * pHdwSimData;
	twobytes tbIndex;

	// Run a No Device good pass
	iResult |= McUnitAssertIsOK( g_pDigitalInputOutput->SetLEDState( LED_OFF ), "SetLEDState" );
	iResult |= McUnitAssertIsOK( SetOperState( OPER_STATE_IDLE ), "SetOperState" );
	iResult |= McUnitAssertIsEqual( OPER_STATE_IDLE, GetOperState(), "GetOperState Idle" );

	iResult |= McUnitAssertIsOK( IdleState(), "IdleState" );
	iResult |= McUnitAssertIsEqual( OPER_STATE_IDLE, GetOperState(), "GetOperState Idle" );

	upCvasLEDState eLed;
	iResult |= McUnitAssertIsOK( g_pDigitalInputOutput->GetLEDState( &eLed ), "GetLEDState" );
	iResult |= McUnitAssertIsEqual( LED_POWER, eLed, "LED State" );

	// SBC timeout
	iResult |= McUnitAssertIsEqual( UNIT_TEST_PASSED, m_setupHelpers.SbcReadWrite(), "SbcReadWrite", false );
	f_simDataAccess.GetSimData( &pHdwSimData, &tbIndex );
	pHdwSimData->m_sbcAction = UART_NO_RESP;
	f_simDataAccess.SetSimData( pHdwSimData );
	iResult |= McUnitAssertIsEqual( UNIT_TEST_PASSED, m_setupHelpers.SbcReadWrite(), "SbcReadWrite", false );

	iResult |= McUnitAssertIsOK( GetTick( &tFirst ), "GetTick" );
	m_setupHelpers.SetTick( tFirst + SBC_CRITICAL_TIMEOUT_TICKS + RND_ROBIN_TICK_CNT );

	iResult |= McUnitAssertIsEqual( ERR_IPC_CRITICAL_TIMEOUT, g_pSbcIntfc->ReadFromHardware(),
	        "g_pSbcIntfc->ReadFromHardware", false );
	MsDelay( (twobytes) 2 );
	iResult |= McUnitAssertIsOK( g_pSbcIntfc->WriteToHardware(), "g_pSbcIntfc->WriteToHardware", false );
	MsDelay( (twobytes) 15 );

	//
	iResult |= McUnitAssertIsOK( SetOperState( OPER_STATE_IDLE ), "SetOperState" );
	iResult |= McUnitAssertIsEqual( OPER_STATE_IDLE, GetOperState(), "GetOperState" );

	iResult |= McUnitAssertIsEqual( ERR_IPC_CRITICAL_TIMEOUT, IdleState(), "IdleState" );
	iResult |= McUnitAssertIsEqual( OPER_STATE_ERROR, GetOperState(), "GetOperState Terminate" );

	iResult |= McUnitAssertIsOK( g_pSbcIntfc->InitializeHardware(), "g_pSbcIntfc->InitializeHardware", false );
	m_setupHelpers.SbcSendAllLogs();

	return iResult;
}

/**
 * @brief	This method tests the interaction of TBD.
 *
 * @return  result as an int: 0 (UNIT_TEST_PASSED) if passed, not 0 if failed
 */
MCUNIT_TEST( CSupervisorTest, IdleSbcNoInterlock_Interaction )
{
	int iResult = UNIT_TEST_PASSED;
	SHdwSimData * pHdwSimData;
	twobytes tbIndex;

	// Run a good one
	iResult |= McUnitAssertIsOK( g_pDigitalInputOutput->SetLEDState( LED_OFF ), "SetLEDState" );
	iResult |= McUnitAssertIsOK( SetOperState( OPER_STATE_IDLE ), "SetOperState" );
	iResult |= McUnitAssertIsEqual( OPER_STATE_IDLE, GetOperState(), "GetOperState Idle" );

	iResult |= McUnitAssertIsOK( IdleState(), "IdleState" );
	iResult |= McUnitAssertIsEqual( OPER_STATE_IDLE, GetOperState(), "GetOperState Idle" );

	upCvasLEDState eLed;
	iResult |= McUnitAssertIsOK( g_pDigitalInputOutput->GetLEDState( &eLed ), "GetLEDState" );
	iResult |= McUnitAssertIsEqual( LED_POWER, eLed, "LED State" );

	// SBC re-connect
	f_simDataAccess.GetSimData( &pHdwSimData, &tbIndex );
	pHdwSimData->m_sbcAction = UART_NORMAL;
	f_simDataAccess.SetSimData( pHdwSimData );
	iResult |= McUnitAssertIsEqual( UNIT_TEST_PASSED, m_setupHelpers.SbcReadWrite(), "SbcReadWrite", false );
	iResult |= McUnitAssertIsEqual( UNIT_TEST_PASSED, m_setupHelpers.SbcReadWrite(), "SbcReadWrite", false );

	//
	iResult |= McUnitAssertIsOK( SetOperState( OPER_STATE_IDLE ), "SetOperState" );
	iResult |= McUnitAssertIsEqual( OPER_STATE_IDLE, GetOperState(), "GetOperState" );

	iResult |= McUnitAssertIsOK( IdleState(), "IdleState" );
	iResult |= McUnitAssertIsEqual( OPER_STATE_IDLE, GetOperState(), "GetOperState Terminate" );

	m_setupHelpers.SbcSendAllLogs();

	return iResult;
}

/**
 * @brief	This method tests the interaction of TBD.
 *
 * @return  result as an int: 0 (UNIT_TEST_PASSED) if passed, not 0 if failed
 */
MCUNIT_TEST( CSupervisorTest, IdleToConnect_Interaction )
{
	int iResult = UNIT_TEST_PASSED;

	// Run a good one
	iResult |= McUnitAssertIsOK( g_pDigitalInputOutput->SetLEDState( LED_OFF ), "SetLEDState" );
	iResult |= McUnitAssertIsOK( SetOperState( OPER_STATE_IDLE ), "SetOperState" );
	iResult |= McUnitAssertIsEqual( OPER_STATE_IDLE, GetOperState(), "GetOperState Idle" );

	iResult |= McUnitAssertIsOK( IdleState(), "IdleState" );
	iResult |= McUnitAssertIsEqual( OPER_STATE_IDLE, GetOperState(), "GetOperState Idle" );

	upCvasLEDState eLed;
	iResult |= McUnitAssertIsOK( g_pDigitalInputOutput->GetLEDState( &eLed ), "GetLEDState" );
	iResult |= McUnitAssertIsEqual( LED_POWER, eLed, "LED State" );

	// Interlock all
	iResult |= McUnitAssertIsEqual( UNIT_TEST_PASSED, m_setupHelpers.InterlockAll(), "InterlockAll", false );

	iResult |= McUnitAssertIsOK( IdleState(), "IdleState" );
	iResult |= McUnitAssertIsEqual( OPER_STATE_CNCTD, GetOperState(), "GetOperState Connected" );
	iResult |= McUnitAssertIsOK( IdleState(), "IdleState" );
	iResult |= McUnitAssertIsEqual( OPER_STATE_CNCTD, GetOperState(), "GetOperState Connected" );

	//
	iResult |= McUnitAssertIsOK( SetOperState( OPER_STATE_IDLE ), "SetOperState" );
	iResult |= McUnitAssertIsEqual( OPER_STATE_IDLE, GetOperState(), "GetOperState" );

	iResult |= McUnitAssertIsOK( IdleState(), "IdleState" );
	iResult |= McUnitAssertIsEqual( OPER_STATE_IDLE, GetOperState(), "GetOperState" );

	m_setupHelpers.SbcSendAllLogs();

	return iResult;
}

/**
 * @brief	This method tests the interaction of TBD.
 *
 * @return  result as an int: 0 (UNIT_TEST_PASSED) if passed, not 0 if failed
 */
MCUNIT_TEST( CSupervisorTest, IdleRfGenTerm_Interaction )
{
	int iResult = UNIT_TEST_PASSED;
	twobytes tbErr;
	SHdwSimData * pHdwSimData;
	twobytes tbIndex;

	// Run a good one
	iResult |= McUnitAssertIsOK( g_pDigitalInputOutput->SetLEDState( LED_OFF ), "SetLEDState" );
	iResult |= McUnitAssertIsOK( SetOperState( OPER_STATE_IDLE ), "SetOperState" );
	iResult |= McUnitAssertIsEqual( OPER_STATE_IDLE, GetOperState(), "GetOperState Idle" );

	for ( int iReset = 1; iReset < RF_GEN_ERROR_TERMINATE_CNT; iReset++ )
		{
		iResult |= McUnitAssertIsOK( IdleState(), "IdleState" );
		iResult |= McUnitAssertIsEqual( OPER_STATE_IDLE, GetOperState(), "GetOperState Idle" );

		upCvasLEDState eLed;
		iResult |= McUnitAssertIsOK( g_pDigitalInputOutput->GetLEDState( &eLed ), "GetLEDState" );
		if ( 1 == iReset )
			iResult |= McUnitAssertIsEqual( LED_POWER, eLed, "LED State" );
		else
			iResult |= McUnitAssertIsEqual( LED_WARN, eLed, "LED State" );

		// Interlock all
		iResult |= McUnitAssertIsEqual( UNIT_TEST_PASSED, m_setupHelpers.InterlockAll(), "InterlockAll", false );

		// Fail-out RF generator
		f_simDataAccess.GetSimData( &pHdwSimData, &tbIndex );
		pHdwSimData->m_bRfGenAction = UART_NO_RESP;
		f_simDataAccess.SetSimData( pHdwSimData );

		for ( int i = 0; i < COMM_ERR_TOLERANCE - 1; i++ )
			{
			iResult |= McUnitAssertIsEqual( UNIT_TEST_PASSED, m_setupHelpers.RFGenWriteRead(), "RFGenWriteRead", false );
			}
		iResult |= McUnitAssertIsOK( g_pRFGen->WriteToHardware(), "WriteToHardware" );
		iResult |= McUnitAssertIsEqual( ERR_RF_GEN_RESET, g_pRFGen->ReadFromHardware(), "ReadFromHardware" );
		iResult |= McUnitAssertIsEqual( ERR_RF_GEN_RESET, g_pRFGen->GetError( &tbErr ), "GetError" );

		if ( iReset < RF_GEN_ERROR_TERMINATE_CNT )
			{
			iResult |= McUnitAssertIsEqual( OK, IdleState(), "IdleState" );
			// TODO should terminate	iResult |= McUnitAssertIsEqual( OPER_STATE_ERROR, GetOperState(), "GetOperState Terminate" );
			iResult |= McUnitAssertIsEqual( OPER_STATE_IDLE, GetOperState(), "GetOperState IDLE" );
			}
		else
			{
			iResult |= McUnitAssertIsOK( IdleState(), "IdleState" );
			// TODO should terminate	iResult |= McUnitAssertIsEqual( OPER_STATE_ERROR, GetOperState(), "GetOperState Terminate" );
			iResult |= McUnitAssertIsEqual( OPER_STATE_ERROR, GetOperState(), "GetOperState Terminate" );
			}
		}

	m_setupHelpers.SbcSendAllLogs();

	return iResult;
}

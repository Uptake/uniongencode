/**
 * @file	SelfTestTest.h
 * @par		Package: CvasMcu
 * @par		Project: CVAS Generator
 * @par		Copyright (c) 2016, Uptake Medical, Inc.
 * @author	Kurt Jan 12, 2012 Created file
 * @brief	This file contains unit tests for CSelfTest
 */

#ifndef SELFTESTTEST_H_
#define SELFTESTTEST_H_

// Include Files
#include <SelfTest.h>
#include "McUnit.h"
#include "UnitTestIntfc.h"
#include "TestMain.h"

// Referenced classes
#include <DigitalInputOutput.h>
#include <WaterCntl.h>
#include <SoundCntl.h>
#include <OneWireMemory.h>

// Public Macros and Constants

// Public Type Definitions (Enums, Structs & Classes)

/**
 * @class	CSelfTestTest
 * @brief	This class provides unit tests for CSelfTest
 *
 * Virtual Functions Overridden: @n@n
 *  UnitTestListSetup - Set up list of Suites, Cases and Tests to be execute @n
 * 	CommonTesthandle - Called to have tests run @n
 *
 * External Data Members: None @n@n
 *
 */
class CSelfTestTest: private CMcUnit<CSelfTestTest>, public CUnitTestIntfc, public CTestMain, private CSelfTest
{
public:
	CSelfTestTest();
	virtual ~CSelfTestTest(){}
	int CommonTestHandle();
	void UnitTestListSetup();
	// Case 1: Invocation Tests
	MCUNIT_TEST_DEC( AllPublicMethods_Invocation );
	// Case 2: Parameter Tests
	//MCUNIT_TEST_DEC( <Method>_Parameter );
	// Case 3: Interaction Tests
	//MCUNIT_TEST_DEC( <Name>_Interaction );

	SMcUnitTest sMcUnitTestList[1];	// Increment index for each test added, regardless of the case in which it falls
	SMcUnitCase sMcUnitCase[3];
	SMcUnitCase * sMcUnitCaseList[4];
	SMcUnitSuite sMcUnitTestSuite;
};

// Inline Functions (follows class definition)


#endif /* SELFTESTTEST_H_ */

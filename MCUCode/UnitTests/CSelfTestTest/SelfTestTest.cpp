/**
 * @file	SelfTestTest.cpp
 * @par		Package: CvasMcu
 * @par		Project: CVAS Generator
 * @par		Copyright (c) 2016, Uptake Medical, Inc.
 * @author	Kurt Jan 12, 2012 Created file
 * @brief	This file contains unit tests for CSelfTest
 */

// Include Files
#include "SelfTestTest.h"

// External Public Data

// File Scope Macros and Constants

// File Scope Type Definitions (Enums, Structs & Classes)

// File Scope Data
static CSelfTestTest f_SelfTestTest; // Instantiate one so it registers itself

// File Scope Functions

// Public Functions

/**
 * @brief	This is the constructor for CSelfTestTest
 *
 */

CSelfTestTest::CSelfTestTest() :
	CMcUnit<CSelfTestTest> ( this ), CTestMain( (CUnitTestIntfc*) this )
{
}


/**
 * @brief	This method calls into CMcUnit to have the tests run.
 *
 * @return   Always 1
 */
int CSelfTestTest::CommonTestHandle()
{
	McUnitSuiteRun( &sMcUnitTestSuite );
	McUnitSuiteReport( &sMcUnitTestSuite, DEPTH_TEST_REPORT );
	return 1;
}

/**
 * @brief	This method sets up the lists of tests to run
 *
 */
void CSelfTestTest::UnitTestListSetup()
{

	int iList = 0;
	int iCase = 0;

	// Set up Case 1: Invocation Tests
	ADD_TEST_CASE( sMcUnitCase, iCase, sMcUnitTestList + iList, 1, "Invocation Tests", "Invocation Test failure")
	ADD_TEST_LIST( sMcUnitTestList, iList, &CSelfTestTest::AllPublicMethods_Invocation, "All Public Methods invocation", "unexpected result" )

	// Set up Case 2: Parameter Tests
	ADD_TEST_CASE( sMcUnitCase, iCase, sMcUnitTestList + iList, 0, "Parameter Tests (No tests defined)", "")
	//Delete the preceding line and uncomment the two lines below to begin adding Parameter Tests
	//ADD_TEST_CASE( sMcUnitCase, iCase, sMcUnitTestList + iList, 1, "Parameter Tests", "Parameter Test failure")
	//ADD_TEST_LIST( sMcUnitTestList, iList, &CSelfTestTest::<Method>_Parameter, "<Method>() parameter", "unexpected result" )

	// Set up Case 3: Interaction Tests
	ADD_TEST_CASE( sMcUnitCase, iCase, sMcUnitTestList + iList, 0, "Interaction Tests (No tests defined)", "")
	//Delete the preceding line and uncomment the two lines below to begin adding Interaction Tests
	//ADD_TEST_CASE( sMcUnitCase, iCase, sMcUnitTestList + iList, 1, "Interaction Tests", "Interaction Test failure")
	//ADD_TEST_LIST( sMcUnitTestList, iList, &CSelfTestTest::<Name>_Interaction, "<Name> Interaction", "unexpected result" )

	MAKE_TEST_SUITE( sMcUnitTestSuite, sMcUnitCaseList, sMcUnitCase, iCase, "CSelfTestTest" )

}

//=============================================================================
// CASE 1: INVOCATION TESTS
//=============================================================================

/**
 * @brief	This method tests the invocation of All Public Methods with valid parameters.
 *
 * @return  result as an int: 0 (UNIT_TEST_PASSED) if passed, not 0 if failed
 */
MCUNIT_TEST( CSelfTestTest, AllPublicMethods_Invocation )
{
 	int iResult = UNIT_TEST_PASSED;
 	upGenericState nextState;
 	byte bProgress;
 	int	iInvocations = 0;

  	// Invoke and check return codes
	iResult |= McUnitAssertIsEqual( OK, MemTest(), "MemTest()" );
	iInvocations++;

	// Expect CRC test to fail because full application is not resident
	iResult |= McUnitAssertIsEqual( ERROR, CrcTest(), "CrcTest()" );
	iInvocations++;

	iResult |= McUnitAssertIsEqual( OK, SystemTest( &nextState ), "SystemTest()" );
	iInvocations++;
	//LogOneValue( "nextState: %d", nextState );

	iResult |= McUnitAssertIsEqual( OK, GetProgress( &bProgress ), "GetProgress()" );
	iInvocations++;
	//LogOneValue( "Progress: %d", bProgress );

	LogOneValue("Number of methods tested: %d", iInvocations);

	return iResult;

}


//=============================================================================
// CASE 2: PARAMETER TESTS
//=============================================================================

/**
 * @brief	This method tests the passed parameter(s) of the <Method>() method.
 *
 * @return  result as an int: 0 (UNIT_TEST_PASSED) if passed, not 0 if failed
 */

//MCUNIT_TEST( CSelfTestTest, <Method>_Parameter )
//{
//}


//=============================================================================
// CASE 3: INTERACTION TESTS
//=============================================================================

/**
 * @brief	This method tests the interaction of TBD.
 *
 * @return  result as an int: 0 (UNIT_TEST_PASSED) if passed, not 0 if failed
 */

//MCUNIT_TEST( CSelfTestTest, <Name>_Interaction )
//{
//}


/**
 * @file	CalibrateLinearTest.h
 * @par		Package: CvasMcu
 * @par		Project: CVAS Generator
 * @par		Copyright (c) 2016, Uptake Medical, Inc.
 * @author	Glen May 7, 2012 Created file
 * @brief	This file contains the definition of the tests for CCalibrateLinear in CCalibrateLinearTest
 */

#ifndef CALIBRATELINEARTEST_H_
#define CALIBRATELINEARTEST_H_

// Include Files
#include "CalibrateLinear.h"
#include "McUnit.h"
#include "UnitTestIntfc.h"
#include "TestMain.h"

// Public Macros and Constants

// Public Type Definitions (Enums, Structs & Classes)

/**
 * @class	CCalibrateLinearTest
 * @brief	This class provides tests of CCalibrateLinear. It uses an instantiation of CCalibrateLinear with
 * 			five elements and with an integer SELECTOR and integer data type.
 *
 * Virtual Functions Overridden: @n@n
 *  UnitTestListSetup - Set up list of Suites, Cases and Tests to be execute @n
 * 	CommonTesthandle - Called to have tests run @n
 *
 * External Data Members: None
 *
 */
#define ELEMENT_COUNT	(5)
typedef CCalibrateLinear<int, ELEMENT_COUNT, int> CCalibrateLinearFiveIntElements;

class CCalibrateLinearTest: private CMcUnit<CCalibrateLinearTest> , public CUnitTestIntfc, public CTestMain,
        private CCalibrateLinearFiveIntElements
{
public:
	CCalibrateLinearTest();
	int CommonTestHandle();
	void UnitTestListSetup();
	// Case 1: Invocation Tests
	MCUNIT_TEST_DEC( AllPublicMethods_Invocation );
	// Case 2: Parameter Tests
	MCUNIT_TEST_DEC( AllPublicMethodsDefault_Parameter );
	// Case 3: Interaction Tests
	//MCUNIT_TEST_DEC( <Name>_Interaction );

	SMcUnitTest sMcUnitTestList[ 2 ]; // Increment index for each test added, regardless of the case in which it falls
	SMcUnitCase sMcUnitCase[ 3 ];
	SMcUnitCase * sMcUnitCaseList[ 4 ];
	SMcUnitSuite sMcUnitTestSuite;
};

// Inline Functions (follows class definition)


#endif /* CALIBRATELINEARTEST_H_ */

/**
 * @file	SbcIntfcTest.cpp
 * @par		Package: CvasMcu
 * @par		Project: CVAS Generator
 * @par		Copyright (c) 2016, Uptake Medical, Inc.
 * @author	Kurt Jan 12, 2012 Created file
 * @brief	This file contains unit tests for CSbcIntfc
 */

// Include Files
#include "SbcIntfcTest.h"

// External Public Data

// File Scope Macros and Constants

// File Scope Type Definitions (Enums, Structs & Classes)

// File Scope Data
static CSbcIntfcTest f_SbcIntfcTest; // Instantiate one so it registers itself

// File Scope Functions

// Public Functions

/**
 * @brief	This is the constructor for CSbcIntfcTest
 *
 */

CSbcIntfcTest::CSbcIntfcTest() :
	CMcUnit<CSbcIntfcTest> ( this ), CTestMain( (CUnitTestIntfc*) this )
{
}


/**
 * @brief	This method calls into CMcUnit to have the tests run.
 *
 * @return   Always 1
 */
int CSbcIntfcTest::CommonTestHandle()
{
	McUnitSuiteRun( &sMcUnitTestSuite );
	McUnitSuiteReport( &sMcUnitTestSuite, DEPTH_TEST_REPORT );
	return 1;
}

/**
 * @brief	This method sets up the lists of tests to run
 *
 */
void CSbcIntfcTest::UnitTestListSetup()
{

	int iList = 0;
	int iCase = 0;

	// Set up Case 1: Invocation Tests
	ADD_TEST_CASE( sMcUnitCase, iCase, sMcUnitTestList + iList, 1, "Invocation Tests", "Invocation Test failure")
	ADD_TEST_LIST( sMcUnitTestList, iList, &CSbcIntfcTest::AllPublicMethods_Invocation, "All Public Methods invocation", "unexpected result" )

	// Set up Case 2: Parameter Tests
	ADD_TEST_CASE( sMcUnitCase, iCase, sMcUnitTestList + iList, 0, "Parameter Tests (No tests defined)", "")
	//Delete the preceding line and uncomment the two lines below to begin adding Parameter Tests
	//ADD_TEST_CASE( sMcUnitCase, iCase, sMcUnitTestList + iList, 1, "Parameter Tests", "Parameter Test failure")
	//ADD_TEST_LIST( sMcUnitTestList, iList, &CSbcIntfcTest::<Method>_Parameter, "<Method>() parameter", "unexpected result" )

	// Set up Case 3: Interaction Tests
	ADD_TEST_CASE( sMcUnitCase, iCase, sMcUnitTestList + iList, 0, "Interaction Tests (No tests defined)", "")
	//Delete the preceding line and uncomment the two lines below to begin adding Interaction Tests
	//ADD_TEST_CASE( sMcUnitCase, iCase, sMcUnitTestList + iList, 1, "Interaction Tests", "Interaction Test failure")
	//ADD_TEST_LIST( sMcUnitTestList, iList, &CSbcIntfcTest::<Name>_Interaction, "<Name> Interaction", "unexpected result" )

	MAKE_TEST_SUITE( sMcUnitTestSuite, sMcUnitCaseList, sMcUnitCase, iCase, "CSbcIntfcTest" )

}

//=============================================================================
// CASE 1: INVOCATION TESTS
//=============================================================================

/**
 * @brief	This method tests the invocation of All Public Methods with valid parameters.
 *
 * @return  result as an int: 0 (UNIT_TEST_PASSED) if passed, not 0 if failed
 */
MCUNIT_TEST( CSbcIntfcTest, AllPublicMethods_Invocation )
{
	int iResult = UNIT_TEST_PASSED;
	int	iInvocations = 0;
	int	iMsgSize = 10;
	byte pbMsg[iMsgSize];
	upSbcState	eSbcState;
	byte bValue = 0;
	void * pMessageStruct;

	// Invoke and check return codes (if there is one)
	iResult |= McUnitAssertIsEqual( OK, g_pSbcIntfc->InitializeHardware(), "InitializeHardware()" );
	iInvocations++;

	g_pSbcIntfc->ResetCustomSetup();
	iResult |= McUnitAssertIsEqual( OK, OK, "void ResetCustomSetup()" );
	iInvocations++;

	iResult |= McUnitAssertIsEqual( OK, g_pSbcIntfc->CheckForTimeout(), "CheckForTimeout()" );
	iInvocations++;

	iResult |= McUnitAssertIsEqual( OK, g_pSbcIntfc->ResetCriticalTimer(), "ResetCriticalTimer()" );
	iInvocations++;

	strncpy( (char *)pbMsg, "Hello SBC", iMsgSize );
	iResult |= McUnitAssertIsEqual( OK, g_pSbcIntfc->SendTextMessage( iMsgSize, pbMsg ), "SendTextMessage()" );
	iInvocations++;

	iResult |= McUnitAssertIsEqual( OK, g_pSbcIntfc->ReadSbcState( &eSbcState ), "ReadSbcState()" );
	iResult |= McUnitAssertIsEqual( SBC_STATE_OOS, eSbcState, "SbcState value" );
	iInvocations++;

	iResult |= McUnitAssertIsEqual( MSG_TYPE_NOTSPECIFIED, g_pSbcIntfc->GetLastMsgReceived(), "GetLastMsgReceived()" );
	iInvocations++;

	iResult |= McUnitAssertIsEqual( OK, g_pSbcIntfc->GetMessage( &pMessageStruct, MSG_TYPE_NOTSPECIFIED ), "GetMessage()" );
	iInvocations++;

	iResult |= McUnitAssertIsEqual( OK, g_pSbcIntfc->SendTextOperatingConditions(), "SendTextOperatingConditions()" );
	iInvocations++;

	iResult |= McUnitAssertIsEqual( OK, SetNextSbcRequest( bValue ), "SetNextSbcRequest()" );
	iInvocations++;

	iResult |= McUnitAssertIsEqual( OK, SetNextMcuRequest( bValue ), "SetNextMcuRequest()" );
	iInvocations++;

	iResult |= McUnitAssertIsEqual( OK, g_pSbcIntfc->ReadFromHardware(), "ReadFromHardware()" );
	iInvocations++;

	iResult |= McUnitAssertIsEqual( OK, g_pSbcIntfc->WriteToHardware(), "WriteToHardware()" );
	iInvocations++;

	LogOneValue("Number of methods tested: %d", iInvocations);

	return iResult;
}


//=============================================================================
// CASE 2: PARAMETER TESTS
//=============================================================================

/**
 * @brief	This method tests the passed parameter(s) of the <Method>() method.
 *
 * @return  result as an int: 0 (UNIT_TEST_PASSED) if passed, not 0 if failed
 */

//MCUNIT_TEST( CSbcIntfcTest, <Method>_Parameter )
//{
//}


//=============================================================================
// CASE 3: INTERACTION TESTS
//=============================================================================

/**
 * @brief	This method tests the interaction of TBD.
 *
 * @return  result as an int: 0 (UNIT_TEST_PASSED) if passed, not 0 if failed
 */

//MCUNIT_TEST( CSbcIntfcTest, <Name>_Interaction )
//{
//}


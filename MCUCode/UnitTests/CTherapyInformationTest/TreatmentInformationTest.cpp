/**
 * @file	TherapyInformationTest.cpp
 * @par		Package: CvasMcu
 * @par		Project: CVAS Generator
 * @par		Copyright (c) 2016, Uptake Medical, Inc.
 * @author	Kurt Jan 12, 2012 Created file
 * @brief	This file contains unit tests for CTreatmentInformation
 */

// Include Files
#include "TreatmentInformationTest.h"

// External Public Data

// File Scope Macros and Constants

// File Scope Type Definitions (Enums, Structs & Classes)
static CTherapyInformationTest f_TherapyInformationTest; // Instantiate one so it registers itself

// File Scope Data

// File Scope Functions

// Public Functions

/**
 * @brief	This is the constructor for CTherapyInformationTest
 *
 */

CTherapyInformationTest::CTherapyInformationTest() :
		CMcUnit<CTherapyInformationTest>( this ), CTestMain( (CUnitTestIntfc*) this )
{
}

/**
 * @brief	This method calls into CMcUnit to have the tests run.
 *
 * @return   Always 1
 */
int CTherapyInformationTest::CommonTestHandle()
{
	McUnitSuiteRun( &sMcUnitTestSuite );
	McUnitSuiteReport( &sMcUnitTestSuite, DEPTH_TEST_REPORT );
	return 1;
}

/**
 * @brief	This method sets up the lists of tests to run
 *
 */
void CTherapyInformationTest::UnitTestListSetup()
{

	int iList = 0;
	int iCase = 0;

	// Set up Case 1: Invocation Tests
	ADD_TEST_CASE( sMcUnitCase, iCase, sMcUnitTestList + iList, 3, "Invocation Tests", "Invocation Test failure" )
	ADD_TEST_LIST( sMcUnitTestList, iList, &CTherapyInformationTest::AllGetMethods_Invocation, "AllGetMethods() invocation",
	        "unexpected result" )
	ADD_TEST_LIST( sMcUnitTestList, iList, &CTherapyInformationTest::AllSetMethods_Invocation, "AllSetMethods() invocation",
	        "unexpected result" )
	ADD_TEST_LIST( sMcUnitTestList, iList, &CTherapyInformationTest::AllTherapyCodeMethods_Invocation,
	        "AllTherapyCodeMethods() invocation", "unexpected result" )

	// Set up Case 2: Parameter Tests
	ADD_TEST_CASE( sMcUnitCase, iCase, sMcUnitTestList + iList, 2, "Parameter Tests", "Parameter Test failure" )
	ADD_TEST_LIST( sMcUnitTestList, iList, &CTherapyInformationTest::setTherapyCode_Parameter, "setTherapyCode() parameter",
	        "unexpected result" )
	ADD_TEST_LIST( sMcUnitTestList, iList, &CTherapyInformationTest::checkTherapyCode_Parameter,
	        "checkTherapyCode() parameter", "unexpected result" )

	// Set up Case 3: Interaction Tests
	ADD_TEST_CASE( sMcUnitCase, iCase, sMcUnitTestList + iList, 0, "Interaction Tests (No tests defined)", "" )
	//Delete the preceding line and uncomment the two lines below to begin adding Interaction Tests
	//ADD_TEST_CASE( sMcUnitCase, iCase, sMcUnitTestList + iList, 1, "Interaction Tests", "Interaction Test failure")
	//ADD_TEST_LIST( sMcUnitTestList, iList, &CTherapyInformationTest::<Name>_Interaction, "<Name> Interaction", "unexpected result" )

	MAKE_TEST_SUITE( sMcUnitTestSuite, sMcUnitCaseList, sMcUnitCase, iCase, "CTherapyInformationTest" )

}

//=============================================================================
// CASE 1: INVOCATION TESTS
//=============================================================================

/**
 * @brief	This method tests the invocation of all of the get<TherapyParameter>() methods with valid parameters.
 *			Note: getTreatmentCode() is tested in AllTherapyCodeMethods_Invocation.
 *
 * @return  result as an int: 0 (UNIT_TEST_PASSED) if passed, not 0 if failed
 */
MCUNIT_TEST( CTherapyInformationTest, AllGetMethods_Invocation )
{
	int iResult = UNIT_TEST_PASSED;
	int iInvocations = 0;

	// Invoke and check return value for each Get() method
	iResult |= McUnitAssertIsEqual( 50, getMaxInternalTemp(), "getMaxInternalTemp" );
	iInvocations++;

	iResult |= McUnitAssertIsEqual( 50, getCoilTempLower(), "getCoilTempLower" );
	iInvocations++;

	iResult |= McUnitAssertIsEqual( 50, getOverTempBand(), "getOverTempBand" );
	iInvocations++;

	iResult |= McUnitAssertIsEqual( 250, getCoilTempUpper(), "getCoilTempUpper" );
	iInvocations++;

	iResult |= McUnitAssertIsEqual( (twobytes) 10, getRfPowerTol(), "getRfPowerTol" );
	iInvocations++;

#ifdef	ENABLE_EASY_ENGINEERING_MODE
	iResult |= McUnitAssertIsEqual( 2, getCustom15(), "getCustom15" );
#else
	iResult |= McUnitAssertIsEqual( 0, getCustom15(), "getCustom15" );
#endif
	iInvocations++;

	iResult |= McUnitAssertIsEqual( 100, getCoilIdleTempSetpoint(), "getCustom16" );
	iInvocations++;

#ifdef	ENABLE_EASY_ENGINEERING_MODE
	iResult |= McUnitAssertIsEqual( 2, getCustom17(), "getCustom17" );
#else
	iResult |= McUnitAssertIsEqual( 0, getCustom17(), "getCustom17" );
#endif
	iInvocations++;

	iResult |= McUnitAssertIsEqual( (byte) 0, getCustom22(), "getCustom22" );
	iInvocations++;

#ifdef ENABLE_EASY_ENGINEERING_MODE
	iResult |= McUnitAssertIsEqual( (twobytes) 50, getTrest(), "getTrest" );
#else
	iResult |= McUnitAssertIsEqual( (twobytes) 1200, getTrest(), "getTrest" );
#endif
	iInvocations++;

	iResult |= McUnitAssertIsEqual( (twobytes) 197, getAsyringe(), "getAsyringe" );
	iInvocations++;

	iResult |= McUnitAssertIsEqual( (byte) 250, getTdeb(), "getTdeb" );
	iInvocations++;

	iResult |= McUnitAssertIsEqual( 150, getOutletTempMax(), "getOutletTempMax" );
	iInvocations++;

	iResult |= McUnitAssertIsEqual( 30, getOutletTempMin(), "getOutletTempMin" );
	iInvocations++;

	iResult |= McUnitAssertIsEqual( (twobytes) 15, getDeliveryCount(), "getTreatCount" );
	iInvocations++;

	iResult |= McUnitAssertIsEqual( (byte) 0, getCustom6(), "getCustom6" );
	iInvocations++;

	iResult |= McUnitAssertIsEqual( (twobytes) 300, getTvapor(), "getTtreat" );
	iInvocations++;

	iResult |= McUnitAssertIsEqual( (twobytes) 300, getTvapor(), "getTtherapy" );
	iInvocations++;

	iResult |= McUnitAssertIsEqual( (twobytes) 30, getDeliveryMin(), "getDeliveryMin" );
	iInvocations++;

	iResult |= McUnitAssertIsEqual( 7000, getWrateDelivery(), "getWrateDelivery" );
	iInvocations++;

	iResult |= McUnitAssertIsEqual( 0, getCustom19(), "getCustom19" );
	iInvocations++;

	iResult |= McUnitAssertIsEqual( (twobytes) 100, getDeliveryMax(), "getDeliveryMax" );
	iInvocations++;

	iResult |= McUnitAssertIsEqual( 70, getWratePrime1(), "getWratePrime1" );
	iInvocations++;

	iResult |= McUnitAssertIsEqual( 25, getWratePrime2(), "getWratePrime2" );
	iInvocations++;

	iResult |= McUnitAssertIsEqual( 30, getWratePrime3(), "getWratePrime3" );
	iInvocations++;

	iResult |= McUnitAssertIsEqual( (twobytes) 350, getPowerDelivery(), "getPowerDelivery" );
	iInvocations++;

	iResult |= McUnitAssertIsEqual( 150, getCoilTempShutdownTol(), "getCoilTempShutdownTol" );
	iInvocations++;

	iResult |= McUnitAssertIsEqual( 150, getOutletTempShutdownTol(), "getOutletTempShutdownTol" );
	iInvocations++;

	iResult |= McUnitAssertIsEqual( (twobytes) 5000, getTminValueTest(), "getTminValueTest" );
	iInvocations++;

	iResult |= McUnitAssertIsEqual( (twobytes) 6000, getTminPrimeValueTest(), "getTminPrimeValueTest" );
	iInvocations++;

	iResult |= McUnitAssertIsEqual( (twobytes) 1, getEmcCycles(), "getEmcCycles" );
	iInvocations++;

	iResult |= McUnitAssertIsEqual( (twobytes) 6000, getVprime1(), "getVprime1" );
	iInvocations++;

	iResult |= McUnitAssertIsEqual( (twobytes) 9000, getVprime2(), "getVprime2" );
	iInvocations++;

	iResult |= McUnitAssertIsEqual( (twobytes) 200, getTprecond(), "getTprecond" );
	iInvocations++;

	iResult |= McUnitAssertIsEqual( (twobytes) 500, getTtempWindow(), "getTtempWindow" );
	iInvocations++;

	iResult |= McUnitAssertIsEqual( (twobytes) 300, getCooldownTime(), "getCooldownTime" );
	iInvocations++;

	iResult |= McUnitAssertIsEqual( 80, getWrateIdle(), "getWrateIdle" );
	iInvocations++;

#ifdef ENABLE_EASY_ENGINEERING_MODE
	iResult |= McUnitAssertIsEqual( 2, getCustom8(), "getCustom8" );
#else
	iResult |= McUnitAssertIsEqual( 0, getCustom8(), "getCustom8" );
#endif
	iInvocations++;

	iResult |= McUnitAssertIsEqual( 35, getTempLowerLimitIdle(), "getTempLowerLimitIdle" );
	iInvocations++;

	iResult |= McUnitAssertIsEqual( 225, getTempUpperLimitIdle(), "getTempUpperLimitIdle" );
	iInvocations++;

	iResult |= McUnitAssertIsEqual( (twobytes) 10, getPowerIdle(), "getPowerIdle" );
	iInvocations++;

	iResult |= McUnitAssertIsEqual( 0, getCustom18(), "getCustom18" );
	iInvocations++;

	LogOneValue( "Number of methods tested: %d", iInvocations );

	return iResult;
}

/**
 * @brief	This method tests the invocation of all of the set<TherapyParameter>() methods with valid parameters.
 *			Note: setTreatmentCode() is tested in AllTherapyCodeMethods_Invocation.
 *
 * @return  result as an int: 0 (UNIT_TEST_PASSED) if passed, not 0 if failed
 */
MCUNIT_TEST( CTherapyInformationTest, AllSetMethods_Invocation )
{
	int iResult = UNIT_TEST_PASSED;
	int iInvocations = 0;

	// Invoke and check return code for each Set() method

	iResult |= McUnitAssertIsEqual( OK, setMaxInternalTemp( (int) 55 ), "setMaxInternalTemp" );
	iInvocations++;

	iResult |= McUnitAssertIsEqual( OK, setCoilTempLower( (int) 110 ), "setCoilTempLower" );
	iInvocations++;

	iResult |= McUnitAssertIsEqual( OK, setOverTempBand( (int) 20 ), "setOverTempBand" );
	iInvocations++;

	iResult |= McUnitAssertIsEqual( OK, setCoilTempUpper( (int) 250 ), "setCoilTempUpper" );
	iInvocations++;

	iResult |= McUnitAssertIsEqual( OK, setRfPowerTol( (twobytes) 10 ), "setRfPowerTol" );
	iInvocations++;

	iResult |= McUnitAssertIsEqual( OK, setCustom15( (int) 0 ), "setCustom15" );
	iInvocations++;

	iResult |= McUnitAssertIsEqual( OK, setCoilIdleTempSetpoint( (int) 0 ), "setCustom16" );
	iInvocations++;

	iResult |= McUnitAssertIsEqual( OK, setCustom17( (int) 0 ), "setCustom17" );
	iInvocations++;

	iResult |= McUnitAssertIsEqual( OK, setCustom22( (byte) 10 ), "setTprimeFlow" );
	iInvocations++;

	iResult |= McUnitAssertIsEqual( OK, setTrest( (twobytes) 2000 ), "setTrest" );
	iInvocations++;

	iResult |= McUnitAssertIsEqual( OK, setAsyringe( (twobytes) 197 ), "setAsyringe" );
	iInvocations++;

	iResult |= McUnitAssertIsEqual( OK, setTdeb( (byte) 250 ), "setTdeb" );
	iInvocations++;

	iResult |= McUnitAssertIsEqual( OK, setOutletTempMax( (int) 300 ), "setOutletTempMax" );
	iInvocations++;

	iResult |= McUnitAssertIsEqual( OK, setOutletTempMin( (int) 90 ), "setOutletTempMin" );
	iInvocations++;

	iResult |= McUnitAssertIsEqual( OK, setDeliveryCount( (byte) 15 ), "setTreatCount" );
	iInvocations++;

	iResult |= McUnitAssertIsEqual( OK, setCustom6( (byte) 0 ), "setCustom6" );
	iInvocations++;

	iResult |= McUnitAssertIsEqual( OK, setTdelivery( (twobytes) 100 ), "setTvapor" );
	iInvocations++;

	iResult |= McUnitAssertIsEqual( OK, setTvapor( (twobytes) 200 ), "setTtherapy" );
	iInvocations++;

	iResult |= McUnitAssertIsEqual( OK, setDeliveryMin( (int) 500 ), "setDeliveryMin" );
	iInvocations++;

	iResult |= McUnitAssertIsEqual( OK, setWrateDelivery( (int) 0 ), "setCustom20" );
	iInvocations++;

	iResult |= McUnitAssertIsEqual( OK, setCustom19( (int) 3102 ), "setCustom19" );
	iInvocations++;

	iResult |= McUnitAssertIsEqual( OK, setDeliveryMax( (int) 3500 ), "setDeliveryMax" );
	iInvocations++;

	iResult |= McUnitAssertIsEqual( OK, setWratePrime1( (int) 30 ), "setWratePrime1" );
	iInvocations++;

	iResult |= McUnitAssertIsEqual( OK, setWratePrime2( (int) 30 ), "setWratePrime2" );
	iInvocations++;

	iResult |= McUnitAssertIsEqual( OK, setWratePrime3( (int) 30 ), "setWratePrime3" );
	iInvocations++;

	iResult |= McUnitAssertIsEqual( OK, setPowerDelivery( 0 ), "setCustom7" );
	iInvocations++;

	iResult |= McUnitAssertIsEqual( OK, setCoilTempShutdownTol( (int) 150 ), "setCoilTempShutdownTol" );
	iInvocations++;

	iResult |= McUnitAssertIsEqual( OK, setOutletTempShutdownTol( (int) 100 ), "setOutletTempShutdownTol" );
	iInvocations++;

	iResult |= McUnitAssertIsEqual( OK, setTminValueTest( (twobytes) 3000 ), "setTminValueTest" );
	iInvocations++;

	iResult |= McUnitAssertIsEqual( OK, setTminPrimeValueTest( (twobytes) 4000 ), "setTminPrimeValueTest" );
	iInvocations++;

	iResult |= McUnitAssertIsEqual( OK, setEmcCycles( (twobytes) 1 ), "setEmcCycles" );
	iInvocations++;

	iResult |= McUnitAssertIsEqual( OK, setVprime1( (twobytes) 12 ), "setVprime1" );
	iInvocations++;

	iResult |= McUnitAssertIsEqual( OK, setVprime2( (twobytes) 12 ), "setVprime2" );
	iInvocations++;

	iResult |= McUnitAssertIsEqual( OK, setTprecond( (twobytes) 12 ), "setTprecond" );
	iInvocations++;

	iResult |= McUnitAssertIsEqual( OK, setTtempWindow( (twobytes) 2000 ), "setTtempWindow" );
	iInvocations++;

	iResult |= McUnitAssertIsEqual( OK, setCooldownTime( (twobytes) 0 ), "setCustom21" );
	iInvocations++;

	iResult |= McUnitAssertIsEqual( OK, setWrateIdle( 2450 ), "setWrateIdle" );
	iInvocations++;

	iResult |= McUnitAssertIsEqual( OK, setCustom8( 0 ), "setCustom8" );
	iInvocations++;

	iResult |= McUnitAssertIsEqual( OK, setTempUpperLimitIdle( 234 ), "setTempUpperLimitIdle" );
	iInvocations++;

	iResult |= McUnitAssertIsEqual( OK, setTempLowerLimitIdle( 21 ), "setTempLowerLimitIdle" );
	iInvocations++;

	iResult |= McUnitAssertIsEqual( OK, setPowerIdle( 120 ), "setPowerIdle" );
	iInvocations++;

	iResult |= McUnitAssertIsEqual( OK, setCustom18( 0 ), "" );
	iInvocations++;

	LogOneValue( "Number of methods tested: %d", iInvocations );

	return iResult;
}

/**
 * @brief	This method tests the invocation of all of the TherapyCode methods with valid parameters.
 *
 * @return  result as an int: 0 (UNIT_TEST_PASSED) if passed, not 0 if failed
 */
MCUNIT_TEST( CTherapyInformationTest, AllTherapyCodeMethods_Invocation )
{
	int iResult = UNIT_TEST_PASSED;
	int iInvocations = 0;

	// Invoke and check return code for each TherapyCode method

	iResult |= McUnitAssertIsEqual( TREATMENT_DEFAULT, getTreatmentCode(), "getTreatmentCode" );
	iInvocations++;

	iResult |= McUnitAssertIsEqual( OK, setTreatmentCode( TREATMENT_DEFAULT ), "setTreatmentCode" );
	iInvocations++;

	iResult |= McUnitAssertIsEqual( OK, checkTreatmentCode( TREATMENT_DEFAULT ), "checkTreatmentCode" );
	iInvocations++;

	LogOneValue( "Number of methods tested: %d", iInvocations );

	return iResult;
}

//=============================================================================
// CASE 2: PARAMETER TESTS
//=============================================================================

/**
 * @brief	This method tests the passed parameter(s) of the setTreatmentCode() method.
 *
 * @return  result as an int: 0 (UNIT_TEST_PASSED) if passed, not 0 if failed
 */
MCUNIT_TEST( CTherapyInformationTest, setTherapyCode_Parameter )
{
	int iResult = UNIT_TEST_PASSED;

	// Invoke and check return code for range of enumerated values of therapyCode
	for ( int i = 0; i < TREATMENT_CNT + 1; i++ )
		{
		// Check for correct return code
		if ( ( i > TREATMENT_UNSET && i <= TREATMENT_V_1_0 ) || TREATMENT_ENGINEERING == i || TREATMENT_UPTAKE_DEV_1 == i
		        || TREATMENT_V_1_1 == i )
			iResult |= McUnitAssertIsEqual( OK, setTreatmentCode( (upTreatmentCode) i ), "Valid parameter value" );
		else
			iResult |= McUnitAssertIsEqual( ERR_BAD_PARAM, setTreatmentCode( (upTreatmentCode) i ),
			        "Invalid parameter value" );
		}

	return iResult;
}

/**
 * @brief	This method tests the passed parameter(s) of the checkTreatmentCode() method.
 *
 * @return  result as an int: 0 (UNIT_TEST_PASSED) if passed, not 0 if failed
 */
MCUNIT_TEST( CTherapyInformationTest, checkTherapyCode_Parameter )
{
	int iResult = UNIT_TEST_PASSED;

	// Invoke and check return code for up to 16 enumerated values of therapyCode
	for ( int i = 0; i < TREATMENT_CNT + 1; i++ )
		{
		// Check for correct return code
		if ( ( i > TREATMENT_UNSET && i <= TREATMENT_V_1_0 ) || TREATMENT_ENGINEERING == i || TREATMENT_UPTAKE_DEV_1 == i
		        || TREATMENT_V_1_1 == i )
			iResult |= McUnitAssertIsEqual( OK, checkTreatmentCode( (upTreatmentCode) i ), "Valid parameter value" );
		else
			iResult |= McUnitAssertIsEqual( ERR_BAD_PARAM, checkTreatmentCode( (upTreatmentCode) i ),
			        "Invalid parameter value" );
		}

	return iResult;
}

//=============================================================================
// CASE 3: INTERACTION TESTS
//=============================================================================

/**
 * @brief	This method tests the interaction of TBD.
 *
 * @return  result as an int: 0 (UNIT_TEST_PASSED) if passed, not 0 if failed
 */

//MCUNIT_TEST( CTherapyInformationTest, <Name>_Interaction )
//{
//}

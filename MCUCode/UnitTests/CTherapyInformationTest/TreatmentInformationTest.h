/**
 * @file	TherapyInformationTest.h
 * @par		Package: CvasMcu
 * @par		Project: CVAS Generator
 * @par		Copyright (c) 2016, Uptake Medical, Inc.
 * @author	Kurt Jan 12, 2012 Created file
 * @brief	This file contains unit tests for CTreatmentInformation
 */

#ifndef THERAPYINFORMATIONTEST_H_
#define THERAPYINFORMATIONTEST_H_

// Include Files
#include <TreatmentInformation.h>
#include "McUnit.h"
#include "UnitTestIntfc.h"
#include "TestMain.h"

// Public Macros and Constants

// Public Type Definitions (Enums, Structs & Classes)

/**
 * @class	CTherapyInformationTest
 * @brief	This class provides unit tests for CTreatmentInformation
 *
 * Virtual Functions Overridden: @n@n
 *  UnitTestListSetup - Set up list of Suites, Cases and Tests to be execute @n
 * 	CommonTesthandle - Called to have tests run @n
 *
 * External Data Members: None @n@n
 *
 */


class CTherapyInformationTest: private CMcUnit<CTherapyInformationTest>, public CUnitTestIntfc, public CTestMain, private CTreatmentInformation
{
public:
	CTherapyInformationTest();
	int CommonTestHandle();
	void UnitTestListSetup();
	// Case 1: Invocation Tests
	MCUNIT_TEST_DEC( AllGetMethods_Invocation );
	MCUNIT_TEST_DEC( AllSetMethods_Invocation );
	MCUNIT_TEST_DEC( AllTherapyCodeMethods_Invocation );
	// Case 2: Parameter Tests
	MCUNIT_TEST_DEC( setTherapyCode_Parameter );
	MCUNIT_TEST_DEC( checkTherapyCode_Parameter );
	// Case 3: Interaction Tests
	//MCUNIT_TEST_DEC( <Name>_Interaction );

	SMcUnitTest sMcUnitTestList[5];	// Increment index for each test added, regardless of the case in which it falls
	SMcUnitCase sMcUnitCase[3];
	SMcUnitCase * sMcUnitCaseList[4];
	SMcUnitSuite sMcUnitTestSuite;
};

// Inline Functions (follows class definition)


#endif /* THERAPYINFORMATIONTEST_H_ */

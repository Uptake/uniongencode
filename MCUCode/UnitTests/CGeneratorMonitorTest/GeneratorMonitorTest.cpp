/**
 * @file	GeneratorMonitorTest.cpp
 * @par		Package: CvasMcu
 * @par		Project: CVAS Generator
 * @par		Copyright (c) 2016, Uptake Medical, Inc.
 * @author	Kurt Jan 27, 2012 Created file
 * @brief	This file contains unit tests for CGeneratorMonitor
 */

// Include Files
#include "GeneratorMonitorTest.h"
#include <Temperature.h>
#include <WaterCntl.h>
#include "SbcSerialIntfc.h"
#include "RFGenerator.h"
#include "ObjectSingletons.h"

// External Public Data

// File Scope Macros and Constants

// File Scope Type Definitions (Enums, Structs & Classes)

// File Scope Data
static CGeneratorMonitorTest f_GeneratorMonitorTest; // Instantiate one so it registers itself

// File Scope Functions

// Public Functions

/**
 * @brief	This is the constructor for CGeneratorMonitorTest
 *
 */

CGeneratorMonitorTest::CGeneratorMonitorTest() :
	CMcUnit<CGeneratorMonitorTest> ( this ), CTestMain( (CUnitTestIntfc*) this )
{
}

/**
 * @brief	This method calls into CMcUnit to have the tests run.
 *
 * @return   Always 1
 */
int CGeneratorMonitorTest::CommonTestHandle()
{
	McUnitSuiteRun( &sMcUnitTestSuite );
	McUnitSuiteReport( &sMcUnitTestSuite, DEPTH_TEST_REPORT );
	return 1;
}

/**
 * @brief	This method sets up the lists of tests to run.
 *
 */
void CGeneratorMonitorTest::UnitTestListSetup()
{

	int iList = 0;
	int iCase = 0;

	// Set up Case 1: Invocation Tests
	ADD_TEST_CASE( sMcUnitCase, iCase, sMcUnitTestList + iList, 1, "Invocation Tests", "Invocation Test failure")
	ADD_TEST_LIST( sMcUnitTestList, iList, &CGeneratorMonitorTest:: AllPublicMethods_Invocation, " All Public Methods invocation", "unexpected result" )

	// Set up Case 2: Parameter Tests
	ADD_TEST_CASE( sMcUnitCase, iCase, sMcUnitTestList + iList, 0, "Parameter Tests (No tests defined)", "")
	//Delete the preceding line and uncomment the two lines below to begin adding Parameter Tests
	//ADD_TEST_CASE( sMcUnitCase, iCase, sMcUnitTestList + iList, 1, "Parameter Tests", "Parameter Test failure")
	//ADD_TEST_LIST( sMcUnitTestList, iList, &CGeneratorMonitorTest::<Method>_Parameter, "<Method>() parameter", "unexpected result" )

	// Set up Case 3: Interaction Tests
	ADD_TEST_CASE( sMcUnitCase, iCase, sMcUnitTestList + iList, 6, "Interaction Tests", "Interaction Test Failure")
	//Delete the preceding line and uncomment the two lines below to begin adding Interaction Tests
	//ADD_TEST_CASE( sMcUnitCase, iCase, sMcUnitTestList + iList, 1, "Interaction Tests", "Interaction Test failure")

	ADD_TEST_LIST( sMcUnitTestList, iList, &CGeneratorMonitorTest::CheckInterlocks_Interaction, "CheckInterlocks Interaction", "unexpected result" )
	ADD_TEST_LIST( sMcUnitTestList, iList, &CGeneratorMonitorTest::CheckCriticalTemperatures_Interaction, "CheckCriticalTemperatures Interaction", "unexpected result" )
	ADD_TEST_LIST( sMcUnitTestList, iList, &CGeneratorMonitorTest::CheckOverTemp_Interaction, "CheckOverTemp Interaction", "unexpected result" )
	ADD_TEST_LIST( sMcUnitTestList, iList, &CGeneratorMonitorTest::PerformCriticalMonitoring_Interaction, "PerformCriticalMonitoring Interaction", "unexpected result" )
	ADD_TEST_LIST( sMcUnitTestList, iList, &CGeneratorMonitorTest::CheckRF_Interaction, "CheckRF Interaction", "unexpected result" )
	ADD_TEST_LIST( sMcUnitTestList, iList, &CGeneratorMonitorTest::PendingReadyCheck_Interaction, "PendingReadyCheck Interaction", "unexpected result" )

	MAKE_TEST_SUITE( sMcUnitTestSuite, sMcUnitCaseList, sMcUnitCase, iCase, "CGeneratorMonitorTest" )

}

//=============================================================================
// CASE 1: INVOCATION TESTS
//=============================================================================

/**
 * @brief	This method tests the invocation of all public methods with valid parameters.
 *
 * @return  result as an int: 0 (UNIT_TEST_PASSED) if passed, not 0 if failed
 */
MCUNIT_TEST( CGeneratorMonitorTest, AllPublicMethods_Invocation )
{
	int iResult = UNIT_TEST_PASSED;
	upInterlockFlag eInterlocks;
	bool_c bTriggerOn;
	upCvasErr eErr;
	int iInvocations = 0;

	CTimeBase::InitializeHardware();

	// TODO SbcIntfc doesn't seem to be able to keep up.
	// ERR_IPC_CRITICAL_TIMEOUT = 425 and ERR_IPC_CONNECTION_TIMEOUT = 426 have been observed.
	// Writes to the log seem to slow things down enough to yield consistent results.

	// Invoke and check return codes (if there is one)
	eErr = CheckInterlocks( false, &eInterlocks, false );
	iResult |= McUnitAssertIsOK( eErr, "CheckInterlocks" );
	//LogOneValue("Interlocks: %x", eInterlocks );
	iInvocations++;

	// Expect missing interlock when initially checking the generator
	eErr = CheckGenerator( INTLCK_ALL );
	iResult |= McUnitAssertIsEqual( ERR_INTERLOCK_MISSING, eErr, "CheckGenerator" );
	iInvocations++;
	//LogOneValue("CheckGenerator eErr: %d", eErr );
	//iResult |= McUnitAssertIsEqual( ERR_INTERLOCK_MISSING, CheckGenerator(), "CheckGenerator");

	eErr = GetDebouncedTrigger( &bTriggerOn );
	iResult |= McUnitAssertIsOK( eErr, "GetDebouncedTrigger" );
	iInvocations++;
	//LogOneValue("TriggerOn: %d", bTriggerOn );
	//iResult |= McUnitAssertIsOK(GetDebouncedTrigger( &bTriggerOn ), "GetDebouncedTrigger");

	eErr = CheckRF();
	iResult |= McUnitAssertIsOK( eErr, "CheckRF" );
	iInvocations++;
	//LogOneValue("CheckRF eErr: %d", eErr );
	//iResult |= McUnitAssertIsOK(CheckRF(), "CheckRF");

	eErr = CheckOverTemp();
	iResult |= McUnitAssertIsOK( eErr, "CheckOverTemp" );
	iInvocations++;

	eErr = PerformCriticalMonitoring();
	iResult |= McUnitAssertIsOK( eErr, "PerformCriticalMonitoring" );
	iInvocations++;
	//LogOneValue("PerformCriticalMonitoring eErr: %d", eErr );

	LogOneValue( "Number of methods tested: %d", iInvocations );

	return iResult;
}

//=============================================================================
// CASE 3: INTERACTION TESTS
//=============================================================================

/**
 * @brief	This method tests the interaction of TBD.
 *
 * @return  result as an int: 0 (UNIT_TEST_PASSED) if passed, not 0 if failed
 */

MCUNIT_TEST( CGeneratorMonitorTest, CheckCriticalTemperatures_Interaction )
{
	int iResult = UNIT_TEST_PASSED;
	SHdwSimData * pHdwSimData = 0;
	// Test 1 : No error returned if a handpiece isn't connected

	CTimeBase::InitializeHardware();
	CTimeBase::ReadFromHardware();
	g_pWaterCntl->InitializeHardware();
	g_pTemperature->InitializeHardware();
	g_pOneWireMemory->InitializeHardware();
	g_pDigitalInputOutput->InitializeHardware();
	g_pTemperature->InitializeHardware();
	//g_pSbcIntfc->InitializeHardware();
	g_pRFGen->InitializeHardware();

	m_setupHelper.InterlockAll();
	twobytes idxThrowAway = 0;
	f_simDataAccess.GetSimData( &pHdwSimData, &idxThrowAway );

	upInterlockFlag pInter;
	CheckInterlocks( false, &pInter, true );

	// Test 1: Ensure that a coil temperature over the shutdown tolerance generates an excessive temp error

	// Test 1: Setup
	pHdwSimData->m_coilTemp = g_pTreatmentInfo->getCoilTempUpper() + g_pTreatmentInfo->getCoilTempShutdownTol() + 1;
	pHdwSimData->m_outletTemp = g_pTreatmentInfo->getOutletTempMin();
	pHdwSimData->m_intTemp = g_pTreatmentInfo->getMaxInternalTemp() - 1;

	f_simDataAccess.SetSimData( pHdwSimData );

	for ( int i = 0; i < TEMPERATURE_FIR_COUNT + 1; i++ )
		g_pTemperature->ReadFromHardware();

	// Test 1 : Procedure
	for ( int i = 0; i < OVER_MAX_TEMP_CNT - 1; i++ )
		iResult |= McUnitAssertIsOK( CheckCriticalTemperatures(), "CheckCriticalTemperatures" );

	iResult |= McUnitAssertIsEqual( ERR_EXCESSIVE_TEMP, CheckCriticalTemperatures(),
	        "TCCT1: ERR_EXCESSIVE_TEMP" );

	// Test 2 : Ensure that an outlet temperature over the shutdown tolerance generates an excessive temp error

	// Test 2 : Setup
	iResult |= ClearTempError();

	pHdwSimData->m_coilTemp = g_pTreatmentInfo->getCoilTempLower();
	pHdwSimData->m_outletTemp = g_pTreatmentInfo->getOutletTempMax() + g_pTreatmentInfo->getOutletTempShutdownTol() + 1;
	pHdwSimData->m_intTemp = g_pTreatmentInfo->getMaxInternalTemp() - 1;

	f_simDataAccess.SetSimData( pHdwSimData );

	iResult |= ReadForTempFIR();

	// Test 2 : Procedure
	for ( int i = 0; i < OVER_MAX_TEMP_CNT - 1; i++ )
		iResult |= McUnitAssertIsOK( CheckCriticalTemperatures(), "CheckCriticalTemperatures" );

	iResult |= McUnitAssertIsEqual( ERR_EXCESSIVE_TEMP, CheckCriticalTemperatures(),
	        "TCCT2: ERR_EXCESSIVE_TEMP" );

	// Test 3 : Ensure that when both temperatures are over the shutdown tolerance, an error is generated

	// Test 3 : Setup
	iResult |= ClearTempError();

	pHdwSimData->m_coilTemp = g_pTreatmentInfo->getCoilTempLower();
	pHdwSimData->m_outletTemp = g_pTreatmentInfo->getOutletTempMin();
	pHdwSimData->m_intTemp = g_pTreatmentInfo->getMaxInternalTemp() - 1;

	f_simDataAccess.SetSimData( pHdwSimData );

	iResult |= ReadForTempFIR();

	iResult |= McUnitAssertIsOK( CheckCriticalTemperatures(), "CheckCriticalTemperatures" );

	pHdwSimData->m_coilTemp = g_pTreatmentInfo->getCoilTempUpper() + g_pTreatmentInfo->getCoilTempShutdownTol() + 1;
	pHdwSimData->m_outletTemp = g_pTreatmentInfo->getOutletTempMax() + g_pTreatmentInfo->getOutletTempShutdownTol() + 1;
	pHdwSimData->m_intTemp = g_pTreatmentInfo->getMaxInternalTemp() - 1;

	f_simDataAccess.SetSimData( pHdwSimData );

	iResult |= ReadForTempFIR();

	// Test 3 : Procedure
	for ( int i = 0; i < OVER_MAX_TEMP_CNT - 1; i++ )
		iResult |= McUnitAssertIsOK( CheckCriticalTemperatures(), "CheckCriticalTemperatures" );
	iResult |= McUnitAssertIsEqual( ERR_EXCESSIVE_TEMP, CheckCriticalTemperatures(),
	        "TCCT3: ERR_EXCESSIVE_TEMP" );

	// Test 4 : Non-shutdown level temperatures will not produce an error

	// Test 4 : Setup
	pHdwSimData->m_coilTemp = g_pTreatmentInfo->getCoilTempUpper();
	pHdwSimData->m_outletTemp = g_pTreatmentInfo->getOutletTempMax();

	f_simDataAccess.SetSimData( pHdwSimData );

	iResult |= ReadForTempFIR();

	// Test 4 : Procedure
	iResult |= McUnitAssertIsOK( CheckCriticalTemperatures(), "TCCT4: OK" );

	// TODO: Should temperature read errors really be masked?

	iResult |= m_setupHelper.SbcSendAllLogs();

	return iResult;
}

/**
 * @brief	This method tests the interaction of TBD.
 *
 * @return  result as an int: 0 (UNIT_TEST_PASSED) if passed, not 0 if failed
 */
MCUNIT_TEST( CGeneratorMonitorTest, CheckOverTemp_Interaction )
{
	int iResult = UNIT_TEST_PASSED;
	SHdwSimData * pHdwSimData = 0;

	// General Setup
	twobytes idxThrowAway = 0;
	f_simDataAccess.GetSimData( &pHdwSimData, &idxThrowAway );

	// Test 1: Ensure that a coil temperature over the shutdown tolerance generates an excessive temp error

	// Test 1: Setup
	pHdwSimData->m_coilTemp = g_pTreatmentInfo->getCoilTempUpper() + g_pTreatmentInfo->getCoilTempShutdownTol() + 1;
	pHdwSimData->m_outletTemp = g_pTreatmentInfo->getOutletTempMin();

	f_simDataAccess.SetSimData( pHdwSimData );

	iResult |= ReadForTempFIR();

	// Test 1 : Procedure
	for ( int i = 0; i < OVER_MAX_TEMP_CNT - 1; i++ )
		iResult |= McUnitAssertIsOK( CheckOverTemp(), "CheckOverTemp" );
	iResult |= McUnitAssertIsEqual( ERR_EXCESSIVE_TEMP, CheckOverTemp(), "COTP1: ERR_EXCESSIVE_TEMP" );

	// Test 2 : Ensure that an outlet temperature over the shutdown tolerance generates an excessive temp error

	// Test 2 : Setup
	iResult |= ClearTempError();
	pHdwSimData->m_coilTemp = g_pTreatmentInfo->getCoilTempLower();
	pHdwSimData->m_outletTemp = g_pTreatmentInfo->getOutletTempMax() + g_pTreatmentInfo->getOutletTempShutdownTol() + 1;

	f_simDataAccess.SetSimData( pHdwSimData );

	iResult |= ReadForTempFIR();

	// Test 2 : Procedure
	for ( int i = 0; i < OVER_MAX_TEMP_CNT - 1; i++ )
		iResult |= McUnitAssertIsOK( CheckOverTemp(), "CheckOverTemp" );
	iResult |= McUnitAssertIsEqual( ERR_EXCESSIVE_TEMP, CheckOverTemp(), "COTP2: ERR_EXCESSIVE_TEMP" );

	// Test 3 : Return an overtemp error for temps that are above the upper boundary, but below the shutdown level
	// 			After an overtemp error is returned, ensure that the overtemp persist error follows

	pHdwSimData->m_coilTemp = g_pTreatmentInfo->getCoilTempLower();
	pHdwSimData->m_outletTemp = g_pTreatmentInfo->getOutletTempMin();

	iResult |= ReadForTempFIR();

	// Reset the overtemp counter
	CheckOverTemp();

	// Test 3 : Setup
	pHdwSimData->m_coilTemp = g_pTreatmentInfo->getCoilTempUpper() + 1;
	pHdwSimData->m_outletTemp = g_pTreatmentInfo->getOutletTempMax();

	f_simDataAccess.SetSimData( pHdwSimData );

	iResult |= ReadForTempFIR();

	// Test 3 : Procedure
	int iDuration = MS_TO_RND_ROBIN_CYC( g_pTreatmentInfo->getTtempWindow() );

	for ( int i = 1; i < iDuration; i++ )
		iResult |= McUnitAssertIsOK( CheckOverTemp(), "COTP3: OK" );

	iResult |= McUnitAssertIsEqual( ERR_THERMO_OVER_TEMP, CheckOverTemp(), "COTP3: ERR_THERMO_OVER_TEMP" );
	iResult |= McUnitAssertIsEqual( ERR_THERMO_OVER_TEMP_PERSIST, CheckOverTemp(),
	        "COTP3: ERR_THERMO_OVER_TEMP_PERSIST" );

	// Test 5 : Ensure an excessive temp error is not overwritten another error

	// Test 5 : Setup
	pHdwSimData->m_coilTemp = g_pTreatmentInfo->getCoilTempUpper() + g_pTreatmentInfo->getCoilTempShutdownTol() + 1;
	pHdwSimData->m_outletTemp = g_pTreatmentInfo->getOutletTempMin();

	f_simDataAccess.SetSimData( pHdwSimData );

	iResult |= ReadForTempFIR();

	// Test 6 : Go path

	// Test 6 : Setup
	pHdwSimData->m_coilTemp = g_pTreatmentInfo->getCoilTempLower();
	pHdwSimData->m_outletTemp = g_pTreatmentInfo->getOutletTempMin();
	pHdwSimData->m_unused0 = g_pTreatmentInfo->getDeliveryMin();

	iResult |= ReadForTempFIR();


	// Test 6 : Procedure
	iResult |= McUnitAssertIsOK( CheckOverTemp(), "COTP6: OK" );

	iResult |= m_setupHelper.SbcSendAllLogs();

	return iResult;
}

/**
 * @brief	This method tests the interaction of TBD.
 *
 * @return  result as an int: 0 (UNIT_TEST_PASSED) if passed, not 0 if failed
 */
MCUNIT_TEST( CGeneratorMonitorTest, CheckRF_Interaction )
{
	int iResult = UNIT_TEST_PASSED;
	// Test
	SHdwSimData * pHdwSimData;
	twobytes tbIndex = 0;

	// Delay to allow previous test SBC read/writes to reset
	CTimeBase::MsDelay( 200 );

	f_simDataAccess.GetSimData( &pHdwSimData, &tbIndex );
	pHdwSimData->m_sbcAction = 0;
	pHdwSimData->m_coilTemp = g_pTreatmentInfo->getCoilTempLower();
	pHdwSimData->m_outletTemp = g_pTreatmentInfo->getOutletTempMin();
	f_simDataAccess.SetSimData( pHdwSimData );

	g_pRFGen->SetACInput( false );
	g_pRFGen->ReadFromHardware();
	m_setupHelper.StartConnectToRFGen( RF_GEN_POWER_UP_CYCLE_COUNT );

	g_pRFGen->SetOutputEnable( false );
	g_pRFGen->SetPowerRequest( 0 );
	g_pRFGen->SetACInput( false );
	iResult |= McUnitAssertIsOK( CheckRF(), "CheckRF" );

	pHdwSimData->m_tbRfGenPower = 500;
	// itherm	pHdwSimData->m_tbRfGenStatus = 0xA6;
	pHdwSimData->m_tbRfGenStatus = 0x06;

	bool_c bApPower = false;
	bool_c bEnable;

	f_simDataAccess.SetSimData( pHdwSimData );

	m_setupHelper.RFGenWriteRead();
	m_setupHelper.RFGenWriteRead();
	m_setupHelper.RFGenWriteRead();

	g_pRFGen->SetOutputEnable( true );
	g_pRFGen->GetApplyingPower( &bApPower );
	g_pRFGen->GetOutputEnable( &bEnable );

	for ( int i = 0; i < RF_STATUS_LAG_TOLERANCE; i++ )
		{
		g_pRFGen->GetApplyingPower( &bApPower );
		g_pRFGen->GetOutputEnable( &bEnable );

		iResult |= McUnitAssertIsEqual( (bool_c) false, (bool_c) bApPower, "bApPower", false );
		iResult |= McUnitAssertIsEqual( (bool_c) true, (bool_c) bEnable, "bApEnable", false );

		iResult |= McUnitAssertIsOK( CheckRF(), "CheckRF" );
		}

			// Errors found in CheckRF should result in immediate failures
		iResult |= McUnitAssertIsEqual( ERR_RF_GEN_POWER_LEVEL, CheckRF(), "ERR_RF_GEN_POWER_LEVEL" );

	iResult |= m_setupHelper.SbcSendAllLogs();

	return iResult;
}

/**
 * @brief	This method tests the interaction of TBD.
 *
 * @return  result as an int: 0 (UNIT_TEST_PASSED) if passed, not 0 if failed
 */
MCUNIT_TEST( CGeneratorMonitorTest, CheckInterlocks_Interaction )
{
	int iResult = UNIT_TEST_PASSED;
	SHdwSimData * pHdwSimData;
	upInterlockFlag pInter;
	bool_c bOn;
	int i;

	// Test 1 : Test that no interlocks are made

	// Test 1 : Setup
	twobytes tmpIndex = 0;
	f_simDataAccess.GetSimData( &pHdwSimData, &tmpIndex );
	CTimeBase::InitializeHardware();
	g_pDigitalInputOutput->InitializeHardware();
	pHdwSimData->m_digInput = 0;
	f_simDataAccess.SetSimData( pHdwSimData );

	g_pDigitalInputOutput->ReadFromHardware();

	// Test 1 : Procedure
	iResult |= McUnitAssertIsOK( CheckInterlocks( false, &pInter, false ), "CI1: No Interlocks" );
	iResult |= McUnitAssertIsEqual( INTLCK_NONE, pInter, "CI1: INTLCK_NONE" );

	// Test 2 : Test the syringe interlock

	// Test 2 : Setup
	g_pDigitalInputOutput->InitializeHardware();
	pHdwSimData->m_digInput = ( 1 << DIG_IN_SYR_PRESENT_BIT );
	f_simDataAccess.SetSimData( pHdwSimData );

	g_pDigitalInputOutput->ReadFromHardware();

	g_pDigitalInputOutput->GetInput( DIG_IN_SYR_PRESENT, &bOn );
	iResult |= McUnitAssertIsEqual( (bool_c) true, (bool_c) bOn, "CI2: Syringe bit on" );

	// Test 2 : Procedure

	iResult |= McUnitAssertIsOK( CheckInterlocks( false, &pInter, false ), "CI2: Syringe" );
	iResult |= McUnitAssertIsEqual( INTLCK_SYRINGE, pInter, "CI2: Syringe Interlock" );

	pHdwSimData->m_digInput = ( 1 << DIG_IN_SYR_PRESENT_BIT ) | ( 1 << DIG_IN_MOTOR_EXTEND_BIT );
	f_simDataAccess.SetSimData( pHdwSimData );

	g_pDigitalInputOutput->ReadFromHardware();

	g_pDigitalInputOutput->GetInput( DIG_IN_SYR_PRESENT, &bOn );
	iResult |= McUnitAssertIsEqual( (bool_c) true, (bool_c) bOn, "CI2: Syringe bit on" );

	g_pDigitalInputOutput->GetInput( DIG_IN_MOTOR_EXTEND, &bOn );
	iResult |= McUnitAssertIsEqual( (bool_c) true, (bool_c) bOn, "CI2: Extend bit on" );

	// Test 2A : Procedure
	iResult |= McUnitAssertIsEqual( INTLCK_SYRINGE, pInter, "CI2: Syringe Interlocked" );

	// Test 4 : Check that the handpiece interlock is made

	// Test 4 : Setup
	g_pDigitalInputOutput->InitializeHardware();
	pHdwSimData->m_digInput = 0;
	pHdwSimData->m_coilTemp = 28;
	pHdwSimData->m_outletTemp = 29;
	f_simDataAccess.SetSimData( pHdwSimData );
	g_pDigitalInputOutput->ReadFromHardware();
	g_pTemperature->ReadFromHardware();

	// Connect 1-wire memory. File has 50 data rows
	iResult |= McUnitAssertIsOK( f_simDataAccess.LoadDataFile( "DataFiles/FreshCode2OneWire.csv", true ),
	        "Load file DataFiles/FreshCode2OneWire.csv", false );

	g_pOneWireMemory->InitializeHardware();
	g_pOneWireMemory->EnableOwMonitor( true );

	for ( i = 0; i < 52; i++ )
		{
		iResult |= McUnitAssertIsOK( CTimeBase::ReadFromHardware(), "f_timeBase.ReadFromHardware", false );
		CTimeBase::MsDelay( (twobytes) 2 );
		iResult |= McUnitAssertIsOK( g_pOneWireMemory->ReadFromHardware(), "g_pOneWireMemory->ReadFromHardware", false );
		iResult |= McUnitAssertIsOK( g_pOneWireMemory->Execute(), "g_pOneWireMemory->Execute", false );
		iResult |= McUnitAssertIsOK( f_simDataAccess.GoToNextRecord(), "f_simDataAccess.GoToNextRecord", false );
		}

	tick tFirst;
	iResult |= McUnitAssertIsOK( CTimeBase::GetTick( &tFirst ), "GetTick", false );

	for ( i = 0; i < 10; i++ )
		{
		SetTick( tFirst += RND_ROBIN_TICK_CNT );
		iResult |= McUnitAssertIsOK( g_timeBase.ReadFromHardware(), "g_timeBase.ReadFromHardware", false );
		iResult |= McUnitAssertIsOK( g_pOneWireMemory->ReadFromHardware(), "g_pOneWireMemory->ReadFromHardware", false );
		iResult |= McUnitAssertIsOK( g_pOneWireMemory->Execute(), "Execute", false );
		}

	for ( i = 0; i < 20; i++ )
		{
		iResult |= McUnitAssertIsOK( g_pOneWireMemory->Execute(), "Execute", false );

		}

	CTimeBase::MsDelay( (twobytes) 2 );

	// Test 4 : Procedure
	iResult |= McUnitAssertIsOK( CheckInterlocks( false, &pInter, false ), "CI4: Handpiece" );
	iResult |= McUnitAssertIsEqual( INTLCK_HANDPIECE, pInter, "CI4: Handpiece Interlock" );

	// Test 5 : Test the SBC interlock

	// Test 5 : Setup
	pHdwSimData->m_owPresence = 0;
	f_simDataAccess.SetSimData( pHdwSimData );
	g_pOneWireMemory->ReadFromHardware();
	g_pOneWireMemory->Execute();

	m_setupHelper.SbcInitAndConnect();
	// Create opcond and send it
	m_setupHelper.SetOperatingConditions( OPER_STATE_IDLE, 0 );
	for ( i = 0; i < 8; i++ )
		{
		SetTick( tFirst += RND_ROBIN_TICK_CNT );
		iResult |= McUnitAssertIsOK( g_timeBase.ReadFromHardware(), "g_timeBase.ReadFromHardware", false );
		iResult |= m_setupHelper.SbcReadWrite();
		}

	// Test 5 : Procedure
	iResult |= McUnitAssertIsOK( CheckInterlocks( false, &pInter, false ), "CI5: SBC" );
	iResult |= McUnitAssertIsEqual( INTLCK_SBC, pInter, "CI5: SBC Interlock" );

	// Test 7 : Assert that when all interlocks are made and RF Enable is set to true, GetAcInput returns true

	// Test 7 : Setup
	m_setupHelper.InterlockAll();
	bool_c bcAcInput = false;

	// Test 7 : Procedure
	iResult |= McUnitAssertIsOK( CheckInterlocks( false, &pInter, false ), "CI7: INTLCK_ALL" );
	iResult |= McUnitAssertIsEqual( INTLCK_ALL, pInter, "CI7: All interlocks are made" );

	g_pRFGen->GetACInput( &bcAcInput );

	iResult |= McUnitAssertIsEqual( (bool_c) true, (bool_c) bcAcInput, "CI7: Assert that the AC input is on" );

	// Test 8 : Assert that the RF AC ON flag only works when all connections are made

	// Test 8 : Setup
	pHdwSimData->m_owPresence = 0;
	f_simDataAccess.SetSimData( pHdwSimData );
	g_pOneWireMemory->ReadFromHardware();
	g_pOneWireMemory->Execute();

	g_pRFGen->SetACInput( true );

	// Test 8 : Procedure
	iResult |= McUnitAssertIsOK( CheckInterlocks( false, &pInter, true ), "CI8: CheckInterlocks" );
	iResult |= McUnitAssertIsEqual( (int) ( INTLCK_ALL & ~INTLCK_HANDPIECE & ~INTLCK_COIL ), (int) pInter,
	        "CI8: All intlcks except DD and coil" );

	g_pRFGen->GetACInput( &bcAcInput );

	iResult |= McUnitAssertIsEqual( (bool_c) false, (bool_c) bcAcInput, "CI8: Assert that the AC input is off" );

	iResult |= m_setupHelper.SbcSendAllLogs();

	// Test 9 : Check that open TC is flagged as error as requested after connection is made

	// Test 9 : Setup. Interlock all and open a TC
	iResult |= m_setupHelper.InterlockAll();

	f_simDataAccess.GetSimData( &pHdwSimData, &tmpIndex );
	pHdwSimData->m_coilTemp = 0;
	f_simDataAccess.SetSimData( pHdwSimData );
	g_pTemperature->ReadFromHardware();

	// Test 9 : Procedure
	iResult |= McUnitAssertIsEqual( ERR_THERMO_INTERLOCK_MISSING, CheckInterlocks( true, &pInter, false ),
	        "CI9: CheckInterlocks" );
	iResult |= McUnitAssertIsEqual( (int) ( INTLCK_ALL & ~INTLCK_HANDPIECE ), (int) pInter,
	        "CI9: All intlcks except DD " );

	// Test 10 : Check that open TC is not flagged as error after connection is made

	// Test 10 : Setup. Interlock all and then open a TC
	m_setupHelper.InterlockAll();

	f_simDataAccess.GetSimData( &pHdwSimData, &tmpIndex );
	pHdwSimData->m_coilTemp = 0;
	f_simDataAccess.SetSimData( pHdwSimData );
	g_pTemperature->ReadFromHardware();

	// Test 10 : Procedure
	iResult |= McUnitAssertIsOK( CheckInterlocks( false, &pInter, false ), "CI10: CheckInterlocks" );
	iResult |= McUnitAssertIsEqual( (int) ( INTLCK_ALL & ~INTLCK_HANDPIECE ), (int) pInter,
	        "CI10: All intlcks except DD " );

	// Test 11 : Check that open TC is not flagged as new error while connection is being made until timing out

	// Test 11 : Setup. Signal disconnected 1-wire and check interlock

	pHdwSimData->m_owPresence = 0;
	pHdwSimData->m_coilTemp = 1;
	f_simDataAccess.SetSimData( pHdwSimData );
	g_pTemperature->ReadFromHardware();
	iResult |= McUnitAssertIsOK( g_pOneWireMemory->ReadFromHardware(), "g_pOneWireMemory->ReadFromHardware", false );
	iResult |= McUnitAssertIsOK( g_pOneWireMemory->Execute(), "Execute", false );

	iResult |= McUnitAssertIsOK( CheckInterlocks( true, &pInter, false ), "CI11: CheckInterlocks" );
	iResult |= McUnitAssertIsEqual( (int) ( INTLCK_ALL & ~INTLCK_HANDPIECE ), (int) pInter,
	        "CI11: All intlcks except DD " );

	// Test 11 : Procedure: Interlock 1-wire and check for TC behavior
	m_setupHelper.InterlockOneWire();

	f_simDataAccess.GetSimData( &pHdwSimData, &tmpIndex );
	pHdwSimData->m_coilTemp = 0;
	f_simDataAccess.SetSimData( pHdwSimData );
	g_pTemperature->ReadFromHardware();

	for ( int i = 0; i < THERMO_CONNECT_TOL - 1; i++ )
		{
		iResult |= McUnitAssertIsOK( CheckInterlocks( false, &pInter, false ), "CI11: CheckInterlocks" );
		iResult |= McUnitAssertIsEqual( (int) ( INTLCK_ALL & ~INTLCK_HANDPIECE ), (int) pInter,
		        "CI11: All intlcks except DD " );
		}

	iResult
	        |= McUnitAssertIsEqual( ERR_THERMO_ERROR, CheckInterlocks( false, &pInter, false ), "CI11: CheckInterlocks" );
	iResult |= McUnitAssertIsEqual( (int) ( INTLCK_ALL & ~INTLCK_HANDPIECE ), (int) pInter,
	        "CI11: All intlcks except DD " );

	iResult |= m_setupHelper.SbcSendAllLogs();

	return iResult;
}

/*
 * @brief	This method tests the interaction of TBD.
 *
 * @return  result as an int: 0 (UNIT_TEST_PASSED) if passed, not 0 if failed
 */
MCUNIT_TEST( CGeneratorMonitorTest, PerformCriticalMonitoring_Interaction )
{
	tick tNow = 0;
	int iResult = UNIT_TEST_PASSED;
	SHdwSimData * pHdwSimData = 0;
	twobytes idxThrowAway = 0;

	// General Setup
	iResult |= ClearTempError();

	// Test 1: Ensure that a coil temperature over the shutdown tolerance generates an excessive temp error

	// Test 1: Setup
	f_simDataAccess.GetSimData( &pHdwSimData, &idxThrowAway );
	pHdwSimData->m_coilTemp = g_pTreatmentInfo->getCoilTempUpper() + g_pTreatmentInfo->getCoilTempShutdownTol() + 1;
	pHdwSimData->m_outletTemp = g_pTreatmentInfo->getOutletTempMin();

	f_simDataAccess.SetSimData( pHdwSimData );

	iResult |= ReadForTempFIR();

	// Test 1 : Procedure
	for ( int i = 0; i < OVER_MAX_TEMP_CNT - 1; i++ )
		iResult |= McUnitAssertIsOK( PerformCriticalMonitoring(), "PerformCriticalMonitoring" );
	iResult |= McUnitAssertIsEqual( ERR_EXCESSIVE_TEMP, PerformCriticalMonitoring(),
	        "PCM1: ERR_EXCESSIVE_TEMP" );

	// Test 3 : Return an RF Generator Critical Error

	// Test 3 : Setup
	pHdwSimData->m_coilTemp = g_pTreatmentInfo->getCoilTempUpper();
	pHdwSimData->m_outletTemp = g_pTreatmentInfo->getOutletTempMin();

	iResult |= ReadForTempFIR();

	// TODO: It is not possible to receive an ERR_RF_GEN_COMM_FATAL in PerformCriticalMonitoring().
	// ERR_RF_GEN_COMM_FATAL results in an RF_GEN_NOT_READY.  We may want to revisit this behavior
	// in future versions.


	//iResult |= PerformWriteReadErrorCheck( OK, OK, ERR_RF_GEN_NOT_READY );
	// Test 2 : Return an SBC Timeout

	// Test 2 : Setup
	pHdwSimData->m_coilTemp = g_pTreatmentInfo->getCoilTempLower();
	pHdwSimData->m_outletTemp = g_pTreatmentInfo->getOutletTempMin();

	iResult |= ReadForTempFIR();

	pHdwSimData->m_sbcAction = UART_NO_RESP;
	f_simDataAccess.SetSimData( pHdwSimData );

	g_pSbcIntfc->ReadFromHardware();
	g_pSbcIntfc->WriteToHardware();
	CTimeBase::GetTick( &tNow );
	tNow = tNow + SBC_CRITICAL_TIMEOUT_TICKS + 1;
	CTimeBase::SetTick( tNow );

	g_pSbcIntfc->ReadFromHardware();

	// Test 2 : Procedure
	iResult |= McUnitAssertIsEqual( ERR_IPC_CRITICAL_TIMEOUT, PerformCriticalMonitoring(),
	        "PCM2: ERR_IPC_CRITICAL_TIMEOUT" );

	pHdwSimData->m_sbcAction = 0;
	f_simDataAccess.SetSimData( pHdwSimData );

	// The last call was a read, so write before the next read/write stage
	g_pSbcIntfc->WriteToHardware();

	iResult |= m_setupHelper.SbcSendAllLogs();

	return iResult;
}

/**
 * @brief	This method
 *
 * @param	eWrite Expected return value for WriteToHardware as upCvasErr
 * @param	eRead Expected return value for ReadFromHardware as upCvasErr
 * @param	eErr Expected return value for GetError as upCvasErr
 *
 * @return   Status as upCvasErr
 */
int CGeneratorMonitorTest::PerformWriteReadErrorCheck( upCvasErr eWrite, upCvasErr eRead, upCvasErr eErr )
{
	int iResult = UNIT_TEST_PASSED;
	twobytes tbErr;

	iResult |= McUnitAssertIsEqual( eWrite, g_pRFGen->WriteToHardware(), "WriteToHardware" );
	iResult |= McUnitAssertIsEqual( eRead, g_pRFGen->ReadFromHardware(), "ReadFromHardware" );
	iResult |= McUnitAssertIsEqual( eErr, g_pRFGen->GetError( &tbErr ), "GetError" );

	return iResult;
}

/**
 * @brief	This method clears the temperatures to levels that will be successful
 * 			during the treatment in m_sHdwSimData
 *
 */
void CGeneratorMonitorTest::SetStandardTemps()
{
	int iResult = UNIT_TEST_PASSED;

	m_sHdwSimData.m_coilTemp = g_pTreatmentInfo->getCoilTempLower() + 1;
	m_sHdwSimData.m_outletTemp = g_pTreatmentInfo->getOutletTempMin() + 1;

	f_simDataAccess.SetSimData( &m_sHdwSimData );

	g_pWaterCntl->InitializeHardware();
	g_pTemperature->InitializeHardware();
	g_pWaterCntl->ReadFromHardware();

	iResult |= ReadForTempFIR();
}

/**
 * @brief	This method performs ReadFromHardware operations on CTemperature for enough cycles
 * 			to have the latest set value the FIR filtered value
 *
 * @return   Status as int
 */
int CGeneratorMonitorTest::ReadForTempFIR()
{
	int iResult = UNIT_TEST_PASSED;
	for ( int i = 0; i < TEMPERATURE_FIR_COUNT + 1; i++ )
		g_pTemperature->ReadFromHardware();

	return iResult;
}

/**
 * @brief	This method clears the critical error status in CGeneratorMonitor.
 *
 * 			set normal temperatures, gets them through the FIR and then calls
 * 			CheckCriticalTemperatures() to have the error count cleared.
 *
 * @return   Status as int
 */
int CGeneratorMonitorTest::ClearTempError()
{
	int iResult = UNIT_TEST_PASSED;
	twobytes idxThrowAway = 0;
	SHdwSimData * pHdwSimData = 0;

	iResult |= f_simDataAccess.GetSimData( &pHdwSimData, &idxThrowAway );

	pHdwSimData->m_coilTemp = g_pTreatmentInfo->getCoilTempLower();
	pHdwSimData->m_outletTemp = g_pTreatmentInfo->getOutletTempMin();
	pHdwSimData->m_intTemp = g_pTreatmentInfo->getMaxInternalTemp() - 1;

	iResult |= f_simDataAccess.SetSimData( pHdwSimData );

	iResult |= ReadForTempFIR();

	iResult |= McUnitAssertIsOK( CheckCriticalTemperatures(), "CheckCriticalTemperatures" );

	return iResult;
}

/**
 * @brief	This method checks that expected substates are returned from PendingReadyCheck().
 *
 *
 * @return   Status as int
 */
MCUNIT_TEST( CGeneratorMonitorTest, PendingReadyCheck_Interaction )
{
	int iResult = UNIT_TEST_PASSED;
	twobytes idxThrowAway = 0;
	SHdwSimData * pHdwSimData = 0;
	byte bProgress;
	tick tLastEnd;
	tick tNow;
	upPendingReady pPendingSubstate;

	// Test 1: Test from lowest priority to highest then back to lowest

	iResult |= f_simDataAccess.GetSimData( &pHdwSimData, &idxThrowAway );

	SetStandardTemps();
	CTimeBase::GetTick( &tLastEnd );

	// The temps are normal.  Check that the substate is PENDING_READY_WAIT
	iResult |= McUnitAssertIsOK( PendingReadyCheck( false, tLastEnd, &pPendingSubstate, &bProgress ), "PendingReadyCheck" );
	iResult |= McUnitAssertIsEqual( PENDING_READY_WAIT, pPendingSubstate, "PENDING_READY_TRIGGER_WAIT" );

	// PENDING_READY_WAIT should still be returned even if the trigger is ON
	iResult |= McUnitAssertIsOK( PendingReadyCheck( true, tLastEnd, &pPendingSubstate, &bProgress ), "PendingReadyCheck" );
	iResult |= McUnitAssertIsEqual( PENDING_READY_WAIT, pPendingSubstate, "PENDING_READY_TRIGGER_WAIT" );

	m_setupHelper.SbcInitAndConnect();
	// Create opcond and send it
	m_setupHelper.SetOperatingConditions( OPER_STATE_PENDING, 0 );
	for ( int i = 0; i < 4; i++ )
		{
		SetTick( tLastEnd += RND_ROBIN_TICK_CNT );
		iResult |= McUnitAssertIsOK( g_timeBase.ReadFromHardware(), "g_timeBase.ReadFromHardware", false );
		iResult |= m_setupHelper.SbcReadWrite();
		}

	CTimeBase::GetTick( &tLastEnd );

	iResult |= McUnitAssertIsOK( PendingReadyCheck( true, tLastEnd, &pPendingSubstate, &bProgress ), "PendingReadyCheck" );
	iResult |= McUnitAssertIsEqual( PENDING_READY_WAIT, pPendingSubstate, "PENDING_READY_TRIGGER_WAIT" );

	// Now that X seconds have passed, the pending substate should be PENDING_READY_TRIGGER_ON
	SetTick( tNow = tLastEnd + SEC_TENTHS_TO_TICKS( g_pTreatmentInfo->getTrest() ) + 1 );
	iResult |= McUnitAssertIsOK( PendingReadyCheck( true, tLastEnd, &pPendingSubstate, &bProgress ), "PendingReadyCheck" );
	iResult |= McUnitAssertIsEqual( PENDING_READY_TRIG_ON, pPendingSubstate, "PENDING_READY_TRIGGER_ON" );

	// OverTemp should override both of these
	pHdwSimData->m_coilTemp = g_pTreatmentInfo->getCoilTempUpper() + 1;
	pHdwSimData->m_outletTemp = g_pTreatmentInfo->getOutletTempMin();
	pHdwSimData->m_intTemp = g_pTreatmentInfo->getMaxInternalTemp() - 1;

	iResult |= f_simDataAccess.SetSimData( pHdwSimData );
	iResult |= ReadForTempFIR();

	// This method must be called to increase iDuration so that the overtemp period is reached
	// This needs to be <= as the duration is checked for being over the defined window
	for ( uint8_t i = 0; i <= MS_TO_RND_ROBIN_CYC( g_pTreatmentInfo->getTtempWindow() ); i++ )
		CheckOverTemp();

	iResult |= McUnitAssertIsEqual( ERR_THERMO_OVER_TEMP_PERSIST,
	        PendingReadyCheck( true, tLastEnd, &pPendingSubstate, &bProgress ), "PendingReadyCheck" );
	iResult |= McUnitAssertIsEqual( PENDING_READY_OVER_TEMP, pPendingSubstate, "PENDING_READY_OVER_TEMP" );

// Flush check removed

	// Release these one at a time and return to OK status
	// OverTemp should override both of these
	pHdwSimData->m_coilTemp = g_pTreatmentInfo->getCoilTempLower();
	pHdwSimData->m_outletTemp = g_pTreatmentInfo->getOutletTempMin();
	pHdwSimData->m_intTemp = g_pTreatmentInfo->getMaxInternalTemp() - 1;
	pHdwSimData->m_unused1 = 0;

	iResult |= f_simDataAccess.SetSimData( pHdwSimData );
	iResult |= ReadForTempFIR();

	// The trigger is released, so it should be ok to continue
	iResult |= McUnitAssertIsOK( PendingReadyCheck( false, tLastEnd, &pPendingSubstate, &bProgress ), "PendingReadyCheck" );
	iResult |= McUnitAssertIsEqual( PENDING_READY_OK, pPendingSubstate, "PENDING_READY_OK" );

	return iResult;
}

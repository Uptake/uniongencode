/**
 * @file	GeneratorMonitorTest.h
 * @par		Package: CvasMcu
 * @par		Project: CVAS Generator
 * @par		Copyright (c) 2016, Uptake Medical, Inc.
 * @author	Kurt Jan 27, 2012 Created file
 * @brief	This file contains unit tests for CGeneratorMonitor
 */

#ifndef GENERATORMONITORTEST_H_
#define GENERATORMONITORTEST_H_

// Include Files
#include <GeneratorMonitor.h>
#include "McUnit.h"
#include "UnitTestIntfc.h"
#include "TestMain.h"
#include "SetupHelpers.h"

// Referenced classes
#include <DigitalInputOutput.h>
#include <OneWireMemory.h>
#include <SbcIntfc.h>
#include <RFGenerator.h>
#include <TreatmentInformation.h>
#include <AblationControl.h>
#include <HandpiecePrime.h>
#include <Temperature.h>
#include <TimeBase.h>

// Public Macros and Constants

// Public Type Definitions (Enums, Structs & Classes)

/**
 * @class	CGeneratorMonitorTest
 * @brief	This class provides unit tests for CGeneratorMonitor
 *
 * Virtual Functions Overridden: @n@n
 *  UnitTestListSetup - Set up list of Suites, Cases and Tests to be execute @n
 * 	CommonTesthandle - Called to have tests run @n
 *
 * External Data Members: None @n@n
 *
 */
class CGeneratorMonitorTest: public CTimeBase, private CMcUnit<CGeneratorMonitorTest>, public CUnitTestIntfc, public CTestMain, private CGeneratorMonitor
{
private:
	CSetupHelpers m_setupHelper;
	SHdwSimData m_sHdwSimData;
	void SetStandardTemps();
	int PerformWriteReadErrorCheck( upCvasErr eWrite, upCvasErr eRead, upCvasErr eErr );

public:
	CGeneratorMonitorTest();
	virtual ~CGeneratorMonitorTest(){}
	int CommonTestHandle();
	void UnitTestListSetup();

	int ClearTempError();
	int ReadForTempFIR();

	// Case 1: Invocation Tests
	MCUNIT_TEST_DEC(  AllPublicMethods_Invocation );
	// Case 2: Parameter Tests
	//MCUNIT_TEST_DEC( <Method>_Parameter );
	// Case 3: Interaction Tests
	MCUNIT_TEST_DEC( CheckCriticalTemperatures_Interaction );
	MCUNIT_TEST_DEC( CheckOverTemp_Interaction );
	MCUNIT_TEST_DEC( PerformCriticalMonitoring_Interaction );
	MCUNIT_TEST_DEC( CheckInterlocks_Interaction );
	MCUNIT_TEST_DEC( CheckRF_Interaction );
	MCUNIT_TEST_DEC( PendingReadyCheck_Interaction );
	SMcUnitTest sMcUnitTestList[7];	// Increment index for each test added, regardless of the case in which it falls
	SMcUnitCase sMcUnitCase[3];
	SMcUnitCase * sMcUnitCaseList[4];
	SMcUnitSuite sMcUnitTestSuite;
};

// Inline Functions (follows class definition)


#endif /* GENERATORMONITORTEST_H_ */

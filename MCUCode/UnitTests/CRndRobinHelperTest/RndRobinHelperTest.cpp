/**
 * @file	RndRobinHelperTest.cpp
 * @par		Package: CvasMcu
 * @par		Project: CVAS Generator
 * @par		Copyright (c) 2016, Uptake Medical, Inc.
 * @author	Glen Mar 25, 2011 Created file
 * @brief	This file contains unit tests for the CRndRobinHelper class
 */

// Include Files
#include "RndRobinHelperTest.h"
#include "SimDataAccess.h"


// External Public Data

// File Scope Macros and Constants

// File Scope Type Definitions (Enums, Structs & Classes)

// File Scope Data
static CRndRobinHelperTest f_CRndRobinHelperTest; // Instantiate one so it registers itself

// File Scope Functions

// Public Functions

/**
 * @brief	This is the constructor for StepperPositionTest
 *
 */
CRndRobinHelperTest::CRndRobinHelperTest() :
	CMcUnit<CRndRobinHelperTest> ( this ), CTestMain( (CUnitTestIntfc*) this )
{
}

/**
 * @brief	This method calls into CMcUnit to have the tests run.
 *
 * @return   Always 1
 */
int CRndRobinHelperTest::CommonTestHandle()
{
	McUnitSuiteRun( &sMcUnitTestSuite );
	McUnitSuiteReport( &sMcUnitTestSuite, DEPTH_TEST_REPORT );
	return 1;
}

void CRndRobinHelperTest::UnitTestListSetup()
{

	int iList = 0;
	int iCase = 0;

	// Set up Case 1: Invocation Tests
	ADD_TEST_CASE( sMcUnitCase, iCase, sMcUnitTestList + iList, 1, "Invocation Tests", "Invocation Test failure")
	ADD_TEST_LIST( sMcUnitTestList, iList, &CRndRobinHelperTest::AllPublicMethods_Invocation, "All Public Methods invocation", "unexpected result" )

	// Set up Case 2: Parameter Tests
	ADD_TEST_CASE( sMcUnitCase, iCase, sMcUnitTestList + iList, 0, "Interaction Tests (No tests defined)", "")
	//Delete the preceding line and uncomment the two lines below to begin adding Parameter Tests
	//ADD_TEST_CASE( sMcUnitCase, iCase, sMcUnitTestList + iList, 1, "Parameter Tests", "Parameter Test failure")
	//ADD_TEST_LIST( sMcUnitTestList, iList, &CRndRobinHelperTest::<Name>_Parameter, "<Name> Parameter", "unexpected result" )

	// Set up Case 3: Interaction Tests
	ADD_TEST_CASE( sMcUnitCase, iCase, sMcUnitTestList + iList, 1, "Interaction Tests", "Interaction Test failure")
	ADD_TEST_LIST( sMcUnitTestList, iList, &CRndRobinHelperTest::IncrementPassFlag_Interaction, "Increment Pass Flag Interaction", "unexpected result" )

	MAKE_TEST_SUITE( sMcUnitTestSuite, sMcUnitCaseList, sMcUnitCase, iCase, "CRndRobinHelperTest" )

}

//=============================================================================
// CASE 1: INVOCATION TESTS
//=============================================================================

/**
 * @brief	This method tests the invocation of all public methods with valid parameters.
 *
 * @return  result as an int: 0 (UNIT_TEST_PASSED) if passed, not 0 if failed
 *
 */
MCUNIT_TEST( CRndRobinHelperTest, AllPublicMethods_Invocation )
{
	int iResult = UNIT_TEST_PASSED;
	byte byteValue;
	int	iInvocations = 0;

	// Invoke and check return codes (if there is one)
	iResult |= McUnitAssertIsEqual( OK, SetPassMask( (byte)0 ), "SetPassMask()" );
	iInvocations++;

	iResult |= McUnitAssertIsEqual( (twobytes)0, GetPassCount(), "GetPassCount()" );
	iInvocations++;

	iResult |= McUnitAssertIsEqual( OK, ResetPassFlag(), "ResetPassFlag()" );
	iInvocations++;

	iResult |= McUnitAssertIsEqual( OK, IncrementPassFlag( &byteValue ), "IncrementPassFlag()" );
	iInvocations++;

	iResult |= McUnitAssertIsEqual( (bool_c)false, GetPassEnabled(), "GetPassEnabled()" );
	iInvocations++;

	iResult |= McUnitAssertIsEqual( OK, ClearPassCount(), "ClearPassCount()" );
	iInvocations++;

	LogOneValue("Number of methods tested: %d", iInvocations);

	return iResult;
}

//=============================================================================
// CASE 2: PARAMETER TESTS
//=============================================================================

/**
 * @brief	This method tests the passed parameter(s) of TBD.
 *
 * @return  result as an int: 0 (UNIT_TEST_PASSED) if passed, not 0 if failed
 */

//MCUNIT_TEST( CRndRobinHelperTest, <Name>_Parameter )
//{
//}


//=============================================================================
// CASE 3: INTERACTION TESTS
//=============================================================================

/**
 * @brief	This method tests the interaction of pass flag incrementing.
 *
 * @return  result as an int: 0 (UNIT_TEST_PASSED) if passed, not 0 if failed
 */
MCUNIT_TEST( CRndRobinHelperTest, IncrementPassFlag_Interaction )
{
	byte bPassIndex = 0;
	byte bLastIndex = 0;
	int iResult = UNIT_TEST_PASSED;
	upCvasErr eErr;

	// A reset and an increment should set the flag (not the index) to one
	eErr = ResetPassFlag();
	iResult |= McUnitAssertIsEqual( OK, IncrementPassFlag( &bPassIndex ), "IncrementPassFlag() #1");
	bLastIndex = bPassIndex;

	// Increment and verify the pass index bumps
	iResult |= McUnitAssertIsEqual( OK, IncrementPassFlag( &bPassIndex ), "IncrementPassFlag() #2");
	iResult |= McUnitAssertIsEqual( (byte)(bLastIndex+1), bPassIndex, "Pass flag index");

	return iResult;
}

// Private Class Functions



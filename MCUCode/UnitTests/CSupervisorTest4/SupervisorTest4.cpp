/**
 * @file	SupervisorTest3.cpp
 * @par		Package: CvasMcu
 * @par		Project: CVAS Generator
 * @par		Copyright (c) 2016, Uptake Medical, Inc.
 * @author	Kurt Jan 12, 2012 Created file
 * @brief	This file contains unit tests for CSupervisor
 */

// Include Files
#include "SupervisorTest4.h"
#include "ObjectSingletons.h"
#include "SimDataAccess.h"

// External Public Data

// File Scope Macros and Constants

// File Scope Type Definitions (Enums, Structs & Classes)

// File Scope Data
CSupervisorTest4 f_SupervisorTest4; // Instantiate one so it registers itself

// File Scope Functions

// Public Functions

/**
 * @brief	This is the constructor for CSupervisorTest4
 *
 */

CSupervisorTest4::CSupervisorTest4() :
	CMcUnit<CSupervisorTest4> ( this ), CTestMain( (CUnitTestIntfc*) this )
{
	m_pHdwSimData = NULL;
	m_tbIndex = 0;
}

/**
 * @brief	This method calls into CMcUnit to have the tests run.
 *
 * @return   Always 1
 */
int CSupervisorTest4::CommonTestHandle()
{
	McUnitSuiteRun( &sMcUnitTestSuite );
	McUnitSuiteReport( &sMcUnitTestSuite, DEPTH_TEST_REPORT );
	return 1;
}

/**
 * @brief	This method sets up the lists of tests to run
 *
 */
void CSupervisorTest4::UnitTestListSetup()
{
	int iList = 0;
	int iCase = 0;

	// Set up Case 3: Interaction Tests
	ADD_TEST_CASE( sMcUnitCase, iCase, sMcUnitTestList + iList, 2, "ConnectedState", "Interaction Test failure")
	ADD_TEST_LIST( sMcUnitTestList, iList, &CSupervisorTest4::Setup_for_Interaction, "Setup_for_Interaction", 0x0 )
	ADD_TEST_LIST( sMcUnitTestList, iList, &CSupervisorTest4::ConnectedState_Interaction, "ConnectedState_Interaction", 0x0 )

	ADD_TEST_CASE( sMcUnitCase, iCase, sMcUnitTestList + iList, 1, "PrimingState", "Interaction Test failure")
	ADD_TEST_LIST( sMcUnitTestList, iList, &CSupervisorTest4::PrimingState_Interaction, "PrimingState_Interaction", 0x0 )

	MAKE_TEST_SUITE( sMcUnitTestSuite, sMcUnitCaseList, sMcUnitCase, iCase, "CSupervisorTest4" )

}

//=============================================================================
// CASE 3: INTERACTION TESTS
//=============================================================================


/**
 * @brief	This method tests the interaction of TBD.
 *
 * @return  result as an int: 0 (UNIT_TEST_PASSED) if passed, not 0 if failed
 */
MCUNIT_TEST( CSupervisorTest4, Setup_for_Interaction )
{
	int iResult = UNIT_TEST_PASSED;

	iResult |= McUnitAssertIsOK( g_timeBase.InitializeHardware(), "g_timeBase.InitializeHardware" );
	iResult |= McUnitAssertIsOK( g_pTemperature->InitializeHardware(), "g_pTemperature->InitializeHardware" );
	iResult |= McUnitAssertIsOK( g_pWaterCntl->InitializeHardware(), "g_pWaterCntl->InitializeHardware" );
	iResult |= McUnitAssertIsOK( g_pDigitalInputOutput->InitializeHardware(),
	        "g_pDigitalInputOutput->InitializeHardware" );
	iResult |= m_setupHelpers.SbcInitAndConnect();

	m_setupHelpers.SbcSendAllLogs( GetOperState() );

	return iResult;
}

/**
 * @brief	This method tests the interaction of TBD.
 *
 * @return  result as an int: 0 (UNIT_TEST_PASSED) if passed, not 0 if failed
 */
MCUNIT_TEST( CSupervisorTest4, ConnectedState_Interaction )
{
	int iResult = UNIT_TEST_PASSED;

	// Run a good one
	iResult |= McUnitAssertIsOK( g_pDigitalInputOutput->SetLEDState( LED_OFF ), "SetLEDState" );
	iResult |= McUnitAssertIsOK( SetOperState( OPER_STATE_CNCTD ), "SetOperState" );
	iResult |= McUnitAssertIsEqual( OPER_STATE_CNCTD, GetOperState(), "GetOperState Connected" );

	iResult |= McUnitAssertIsEqual( UNIT_TEST_PASSED, m_setupHelpers.InterlockAll(), "InterlockAll", false );

	// Check being in connected
	f_simDataAccess.GetSimData( &m_pHdwSimData, &m_tbIndex );
	m_pHdwSimData->m_digInput |= ( 1 << DIG_IN_SYR_PRESENT_BIT );
	m_pHdwSimData->m_digInput |= ( 1 << DIG_IN_UNUSED_0_BIT );
	m_pHdwSimData->m_digInput &= ~( 1 << DIG_IN_TRIGGER_BIT );
	m_pHdwSimData->m_digInput |= ( 1 << DIG_IN_MOTOR_RETRACT_BIT );
	m_pHdwSimData->m_digInput &= ~( 1 << DIG_IN_MOTOR_EXTEND_BIT );
	// itherm	m_pHdwSimData->m_tbRfGenStatus = 0xA6;
	m_pHdwSimData->m_tbRfGenStatus = 0x06;
	m_pHdwSimData->m_tbRfGenPower = 0;
	m_pHdwSimData->m_outletTemp = g_pTreatmentInfo->getOutletTempMin() + 1;
	m_pHdwSimData->m_coilTemp = g_pTreatmentInfo->getCoilTempLower() + 1;
	m_pHdwSimData->m_intTemp = g_pTreatmentInfo->getMaxInternalTemp() - 10;
	iResult |= m_setupHelpers.SetAndReadSimDataRecord(m_pHdwSimData);
	iResult |= McUnitAssertIsOK( ConnectedState(), "ConnectedState" );
	// It takes one more call for the retract bit to register
	iResult |= McUnitAssertIsEqual( ERR_RETRACTING_PUMP, ConnectedState(), "ERR_PUMP_RETRACTING" );
	// Change to IDLE as the pump is retracting
	iResult |= McUnitAssertIsEqual( OPER_STATE_IDLE, GetOperState(), "GetOperState Idle: Motor retracted" );

	m_setupHelpers.SbcSendAllLogs( GetOperState() );

	return iResult;
}

/**
 * @brief	This method tests the interaction of TBD.
 *
 * @return  result as an int: 0 (UNIT_TEST_PASSED) if passed, not 0 if failed
 */
MCUNIT_TEST( CSupervisorTest4, PrimingState_Interaction )
{
	int iResult = UNIT_TEST_PASSED;

	// Run a good one
	iResult |= McUnitAssertIsOK( g_pDigitalInputOutput->SetLEDState( LED_OFF ), "SetLEDState" );
	iResult |= McUnitAssertIsOK( SetOperState( OPER_STATE_HAND_PRIMING ), "SetOperState" );
	iResult |= McUnitAssertIsEqual( OPER_STATE_HAND_PRIMING, GetOperState(), "GetOperState Priming" );

	iResult |= McUnitAssertIsEqual( UNIT_TEST_PASSED, m_setupHelpers.InterlockAll(), "InterlockAll", false );

	iResult |= McUnitAssertIsEqual( ERR_TRIGGER_OFF, HandpiecePrimingState(), "PrimingState" );

	m_setupHelpers.SbcSendAllLogs( GetOperState() );

	return iResult;
}

/**
 * @brief	This method sets up the system for a test of PnedingReadyState()
 *
 * @return   Status as int
 */
int CSupervisorTest4::SetForPendingReady()
{
	int iResult = UNIT_TEST_PASSED;

	iResult |= McUnitAssertIsOK( g_pDigitalInputOutput->SetLEDState( LED_OFF ), "SetLEDState" );
	iResult |= McUnitAssertIsEqual( UNIT_TEST_PASSED, m_setupHelpers.InterlockAll(), "InterlockAll", false );

	//
	// Still pending
	f_simDataAccess.GetSimData( &m_pHdwSimData, &m_tbIndex );
	m_pHdwSimData->m_digInput |= ( 1 << DIG_IN_SYR_PRESENT_BIT );
	m_pHdwSimData->m_digInput |= ( 1 << DIG_IN_UNUSED_0_BIT );
	m_pHdwSimData->m_digInput &= ~( 1 << DIG_IN_TRIGGER_BIT );
	m_pHdwSimData->m_digInput |= ( 1 << DIG_IN_MOTOR_RETRACT_BIT );
	m_pHdwSimData->m_digInput &= ~( 1 << DIG_IN_MOTOR_EXTEND_BIT );
	// itherm	m_pHdwSimData->m_tbRfGenStatus = 0xA6;
	m_pHdwSimData->m_tbRfGenStatus = 0x06;
	m_pHdwSimData->m_tbRfGenPower = 0;
	m_pHdwSimData->m_intTemp = g_pTreatmentInfo->getMaxInternalTemp() - 10;
	m_pHdwSimData->m_outletTemp = ( g_pTreatmentInfo->getTempUpperLimitIdle() + g_pTreatmentInfo->getTempLowerLimitIdle() ) / 2;
	m_pHdwSimData->m_coilTemp = m_pHdwSimData->m_outletTemp;
	f_simDataAccess.SetSimData( m_pHdwSimData );
	for ( int i = 0; i < TEMPERATURE_FIR_COUNT; i++ )
		iResult |= m_setupHelpers.ReadSimDataRecord();

	iResult |= McUnitAssertIsOK( SetCatheterPrimedFlag( true ), "SetPrimedFlag" );
	iResult |= McUnitAssertIsOK( SetOperState( OPER_STATE_PENDING ), "SetOperState" );
	iResult |= McUnitAssertIsEqual( OPER_STATE_PENDING, GetOperState(), "GetOperState PendingReady" );

	return iResult;
}

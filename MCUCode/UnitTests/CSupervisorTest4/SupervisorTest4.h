/**
 * @file	SupervisorTest4.h
 * @par		Package: CvasMcu
 * @par		Project: CVAS Generator
 * @par		Copyright (c) 2016, Uptake Medical, Inc.
 * @author	Kurt Jan 12, 2012 Created file
 * @brief	This file contains unit tests for CSupervisor
 */

#ifndef SUPERVISORTEST4_H_
#define SUPERVISORTEST4_H_

// Include Files
#include <Supervisor.h>
#include "McUnit.h"
#include "UnitTestIntfc.h"
#include "TestMain.h"
#include "SetupHelpers.h"

// Referenced classes

// Public Macros and Constants

// Public Type Definitions (Enums, Structs & Classes)

/**
 * @class	CSupervisorTest4
 * @brief	This class provides unit tests for CSupervisor
 *
 * Virtual Functions Overridden: @n@n
 *  UnitTestListSetup - Set up list of Suites, Cases and Tests to be execute @n
 * 	CommonTesthandle - Called to have tests run @n
 *
 * External Data Members: None @n@n
 *
 */
class CSupervisorTest4: private CMcUnit<CSupervisorTest4> , public CUnitTestIntfc, public CTestMain, private CSupervisor
{

private:
	CSetupHelpers m_setupHelpers;
	twobytes m_tbIndex;
	SHdwSimData * m_pHdwSimData;

public:
	CSupervisorTest4();
	int CommonTestHandle();
	void UnitTestListSetup();
	int SetForPendingReady();

	// Case 3: Interaction Tests
	MCUNIT_TEST_DEC( ConnectedState_Interaction );
	MCUNIT_TEST_DEC( PrimingState_Interaction );
	MCUNIT_TEST_DEC( Setup_for_Interaction );

	SMcUnitTest sMcUnitTestList[ 23 ]; // Increment index for each test added, regardless of the case in which it falls
	SMcUnitCase sMcUnitCase[ 11 ];
	SMcUnitCase * sMcUnitCaseList[ 11 ];
	SMcUnitSuite sMcUnitTestSuite;
};

// Inline Functions (follows class definition)


#endif /* SUPERVISORTEST2_H_ */

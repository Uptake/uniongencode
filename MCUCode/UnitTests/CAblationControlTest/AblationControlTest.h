/**
 * @file	AblationControlTest.h
 * @par		Package: CvasMcu
 * @par		Project: CVAS Generator
 * @par		Copyright (c) 2016, Uptake Medical, Inc.
 * @author	Kurt Jan 12, 2012 Created file
 * @brief	This file contains unit tests for CAblationControl
 */

#ifndef ABLATIONCONTROLTEST_H_
#define ABLATIONCONTROLTEST_H_

// Include Files
#include <AblationControl.h>
#include "McUnit.h"
#include "UnitTestIntfc.h"
#include "TestMain.h"
#include "SetupHelpers.h"


// Public Macros and Constants

// Public Type Definitions (Enums, Structs & Classes)

/**
 * @class	CAblationControlTest
 * @brief	This class provides unit tests for CAblationControl
 *
 * Virtual Functions Overridden: @n@n
 *  UnitTestListSetup - Set up list of Suites, Cases and Tests to be execute @n
 * 	CommonTesthandle - Called to have tests run @n
 *
 * External Data Members: None @n@n
 *
 */
class CAblationControlTest: private CMcUnit<CAblationControlTest>, public CUnitTestIntfc, public CTestMain, private CAblationControl
{

SHdwSimData m_sHdwSimData;
CSetupHelpers m_setupHelpers;

int SetTreatmentStart();
int AddOpconAndSendAll();
int CheckGenHalted();

public:
	CAblationControlTest();
	virtual ~CAblationControlTest(){}
	int CommonTestHandle();
	void UnitTestListSetup();
	// Case 1: Invocation Tests
	MCUNIT_TEST_DEC( AllPublicMethods_Invocation );
	// Case 2: Parameter Tests
	//MCUNIT_TEST_DEC( <Method>_Parameter );
	// Case 3: Interaction Tests
	MCUNIT_TEST_DEC( StartAblation_Interaction );
	MCUNIT_TEST_DEC( StartAtLimit_Interaction );
	MCUNIT_TEST_DEC( StartInitiateVaporGood_Interaction );
	MCUNIT_TEST_DEC( StartInitiateVaporError_Interaction );
	MCUNIT_TEST_DEC( MiddleTimeComplete_Interaction );
	MCUNIT_TEST_DEC( MiddleEndRamp_Interaction );
	MCUNIT_TEST_DEC( MiddleTriggerOff_Interaction );
	MCUNIT_TEST_DEC( MiddleVaporFlowError_Interaction );
	MCUNIT_TEST_DEC( PerformTreatmentError_Interaction );
	MCUNIT_TEST_DEC( GetProgress_Interaction );

	SMcUnitTest sMcUnitTestList[20];	// Increment index for each test added, regardless of the case in which it falls
	SMcUnitCase sMcUnitCase[3];
	SMcUnitCase * sMcUnitCaseList[4];
	SMcUnitSuite sMcUnitTestSuite;
};

// Inline Functions (follows class definition)


#endif /* ABLATIONCONTROLTEST_H_ */

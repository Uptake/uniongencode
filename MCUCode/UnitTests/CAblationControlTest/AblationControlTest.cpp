/**
 * @file	AblationControlTest.cpp
 * @par		Package: CvasMcu
 * @par		Project: CVAS Generator
 * @par		Copyright (c) 2016, Uptake Medical, Inc.
 * @author	Kurt Jan 12, 2012 Created file
 * @brief	This file contains unit tests for CAblationControl
 */

// Include Files
#include "AblationControlTest.h"
#include "DigitalInputOutput.h"
#include "ObjectSingletons.h"

// External Public Data

// File Scope Macros and Constants

// File Scope Type Definitions (Enums, Structs & Classes)

// File Scope Data
static CAblationControlTest f_AblationControlTest; // Instantiate one so it registers itself

// File Scope Functions

// Public Functions

/**
 * @brief	This is the constructor for CAblationControlTest
 *
 */

CAblationControlTest::CAblationControlTest() :
	CMcUnit<CAblationControlTest> ( this ), CTestMain( (CUnitTestIntfc*) this )
{
}

/**
 * @brief	This method configures the feedbacks to allow the ablation state machine to enter the start
 * 			of treatment ablation.
 *
 * @return   Status as upCvasErr
 */
int CAblationControlTest::SetTreatmentStart()
{
	int iResult = UNIT_TEST_PASSED;
	twobytes tbIndex;
	SHdwSimData * pHdwSimData;

	f_simDataAccess.GetSimData( &pHdwSimData, &tbIndex );
	pHdwSimData->m_digInput |= ( 1 << DIG_IN_TRIGGER_BIT );
	f_simDataAccess.SetSimData( pHdwSimData );
	iResult |= McUnitAssertIsOK( g_pDigitalInputOutput->ReadFromHardware(), "g_pDigitalInputOutput->ReadFromHardware" );

	pHdwSimData->m_coilTemp = g_pTreatmentInfo->getCoilTempLower() + 1;
	pHdwSimData->m_outletTemp = g_pTreatmentInfo->getOutletTempMin() + 1;
	pHdwSimData->m_intTemp = g_pTreatmentInfo->getMaxInternalTemp() - 1;
	pHdwSimData->m_unused0 = g_pTreatmentInfo->getDeliveryMin() + 1;
	// itherm	m_pHdwSimData->m_tbRfGenStatus = 0x97
	pHdwSimData->m_tbRfGenStatus = 0x05;
	pHdwSimData->m_tbRfGenPower = g_pTreatmentInfo->getPowerDelivery();
	f_simDataAccess.SetSimData( pHdwSimData );
	iResult |= McUnitAssertIsOK( g_pTemperature->ReadFromHardware(), "g_pTemperature->ReadFromHardware" );
	iResult |= McUnitAssertIsOK( g_pTemperature->ReadFromHardware(), "g_pTemperature->ReadFromHardware" );
	iResult |= McUnitAssertIsOK( g_pTemperature->ReadFromHardware(), "g_pTemperature->ReadFromHardware" );
	iResult |= McUnitAssertIsEqual( UNIT_TEST_PASSED, m_setupHelpers.RFGenWriteRead(), "RFGenWriteRead", false );
	iResult |= McUnitAssertIsEqual( UNIT_TEST_PASSED, m_setupHelpers.RFGenWriteRead(), "RFGenWriteRead", false );

	return iResult;
}

/**
 * @brief	This method calls into CMcUnit to have the tests run.
 *
 * @return   Always 1
 */
int CAblationControlTest::CommonTestHandle()
{
	McUnitSuiteRun( &sMcUnitTestSuite );
	McUnitSuiteReport( &sMcUnitTestSuite, DEPTH_TEST_REPORT );
	return 1;
}

/**
 * @brief	This method sets up the lists of tests to run
 *
 */
void CAblationControlTest::UnitTestListSetup()
{

	int iList = 0;
	int iCase = 0;

	// Set up Case 1: Invocation Tests
	ADD_TEST_CASE( sMcUnitCase, iCase, sMcUnitTestList + iList, 1, "Invocation Tests", "Invocation Test failure")
	ADD_TEST_LIST( sMcUnitTestList, iList, &CAblationControlTest::AllPublicMethods_Invocation, "AllPublicMethods() invocation", "unexpected result" )

	// Set up Case 2: Parameter Tests
	// ADD_TEST_CASE( sMcUnitCase, iCase, sMcUnitTestList + iList, 0, "Parameter Tests (No tests defined)", "")
	//Delete the preceding line and uncomment the two lines below to begin adding Parameter Tests
	//ADD_TEST_CASE( sMcUnitCase, iCase, sMcUnitTestList + iList, 1, "Parameter Tests", "Parameter Test failure")
	//ADD_TEST_LIST( sMcUnitTestList, iList, &CAblationControlTest::<Method>_Parameter, "<Method>() parameter", "unexpected result" )

	// Set up Case 3: Interaction Tests
	ADD_TEST_CASE( sMcUnitCase, iCase, sMcUnitTestList + iList, 10, "Interaction Tests", "Interaction Test Failure")
	ADD_TEST_LIST( sMcUnitTestList, iList, &CAblationControlTest::StartAblation_Interaction, "StartAblation_Interaction", "unexpected result" )
	ADD_TEST_LIST( sMcUnitTestList, iList, &CAblationControlTest::StartAtLimit_Interaction, "StartAtLimit_Interaction", "unexpected result" )
	ADD_TEST_LIST( sMcUnitTestList, iList, &CAblationControlTest::StartInitiateVaporGood_Interaction, "StartInitiateVaporGood_Interaction", "unexpected result" )
	ADD_TEST_LIST( sMcUnitTestList, iList, &CAblationControlTest::PerformTreatmentError_Interaction, "PerformTreatmentError_Interaction", "unexpected result" )
	ADD_TEST_LIST( sMcUnitTestList, iList, &CAblationControlTest::StartInitiateVaporError_Interaction, "StartInitiateVaporError_Interaction", "unexpected result" )
	ADD_TEST_LIST( sMcUnitTestList, iList, &CAblationControlTest::MiddleTimeComplete_Interaction, "MiddleTimeComplete_Interaction", "unexpected result" )
	ADD_TEST_LIST( sMcUnitTestList, iList, &CAblationControlTest::MiddleEndRamp_Interaction, "MiddleEndRamp_Interaction", "unexpected result" )
	ADD_TEST_LIST( sMcUnitTestList, iList, &CAblationControlTest::MiddleTriggerOff_Interaction, "MiddleTriggerOff_Interaction", "unexpected result" )
	ADD_TEST_LIST( sMcUnitTestList, iList, &CAblationControlTest::MiddleVaporFlowError_Interaction, "MiddleVaporFlowError_Interaction", "unexpected result" )
	ADD_TEST_LIST( sMcUnitTestList, iList, &CAblationControlTest::GetProgress_Interaction, "GetProgress_Interaction", "unexpected result" )

	MAKE_TEST_SUITE( sMcUnitTestSuite, sMcUnitCaseList, sMcUnitCase, iCase, "CAblationControlTest" )

}

/**
 * @brief	This method creates and sends an opcond.
 * 			Note that this may not create an opcond message if a user error or log
 * 			message is available and requested
 *
 * @return   Status as upCvasErr
 */
int CAblationControlTest::AddOpconAndSendAll()
{
	int iResult = UNIT_TEST_PASSED;

	byte tbProgress;
	iResult |= McUnitAssertIsOK( GetProgress( &tbProgress ), "GetProgress" );

	iResult |= McUnitAssertIsEqual( UNIT_TEST_PASSED, m_setupHelpers.SetOperatingConditions( OPER_STATE_CNCTD,
	        tbProgress ), "SetOperatingConditions" );
	iResult |= McUnitAssertIsEqual( UNIT_TEST_PASSED, m_setupHelpers.SbcSendAllLogs(), "SbcSendAllLogs" );

	return iResult;
}

/**
 * @brief	This method tests that the generator was correctly halted after a treatment-stopping error was seen.
 *
 * @return   Status as upCvasErr
 */
int CAblationControlTest::CheckGenHalted()
{
	int iResult = UNIT_TEST_PASSED;
	bool_c bOn;
	long int ilVel;

	iResult |= McUnitAssertIsEqual( UNIT_TEST_PASSED, m_setupHelpers.RFGenWriteRead(), "RFGenWriteRead" );

	iResult |= McUnitAssertIsOK( g_pWaterCntl->GetTargetVelocity( &ilVel ), "g_pWaterCntl->GetTargetVelocity" );
	iResult |= McUnitAssertIsEqual( (long int) 0, ilVel, "TargetVelocity at halt" );
	iResult |= McUnitAssertIsEqual( (twobytes) 0, g_pRFGen->GetPowerRequest(), "g_pRFGen->GetPowerRequest at halt" );
	iResult |= McUnitAssertIsOK( g_pRFGen->GetOutputEnable( &bOn ), "g_pRFGen->GetOutputEnable" );
	iResult |= McUnitAssertIsEqual( (bool_c) false, bOn, "g_pRFGen->GetOutputEnable at halt" );

	return iResult;
}



//=============================================================================
// CASE 1: INVOCATION TESTS
//=============================================================================

/**
 * @brief	This method tests the invocation of all public methods with valid parameters.
 * 			It also verifies one of the Ablation State changes that occurs when methods are called.
 *
 * @return  result as an int: 0 (UNIT_TEST_PASSED) if passed, not 0 if failed
 */
MCUNIT_TEST( CAblationControlTest, AllPublicMethods_Invocation )
{
	int iResult = UNIT_TEST_PASSED;
	byte bProgress;
	int iInvocations = 0;

	// Invoke and check return codes (if there is one)
	iResult |= McUnitAssertIsEqual( ABLATE_STATE_COMPLETE, GetAblationState(), "GetAblationState" );
	iInvocations++;

	iResult |= McUnitAssertIsOK( StartAblation(), "StartAblation" );
	iInvocations++;

	// Don't expect error
	iResult |= McUnitAssertIsEqual( ERR_INTERLOCK_MISSING, PerformTreatment(), "PerformTreatment" );
	iInvocations++;

	// Expect no progress
	iResult |= McUnitAssertIsOK( GetProgress( &bProgress ), "GetProgress" );
	iResult |= McUnitAssertIsEqual( (byte) 0, bProgress, "Progress value" );
	iInvocations++;

	// Also expect an error for Ablation State
	iResult |= McUnitAssertIsEqual( ABLATE_STATE_COMPLETE, GetAblationState(), "GetAblationState() with error present" );
	iInvocations++;

	// Call again while in error state
	iResult |= McUnitAssertIsEqual( ERR_INTERLOCK_MISSING, PerformTreatment(), "PerformTreatment() with error present" );
	iInvocations++;

	// Still expect no progress
	iResult |= McUnitAssertIsOK( GetProgress( &bProgress ), "GetProgress() with error present" );
	iResult |= McUnitAssertIsEqual( (byte) 0, bProgress, "Progress value with error present" );
	iInvocations++;

	LogOneValue( "Number of methods tested: %d", iInvocations );

	return iResult;
}

//=============================================================================
// CASE 3: INTERACTION TESTS
//=============================================================================

/**
 * @brief	This method tests the interaction of setting the start condition and the state.
 *
 * @return  result as an int: 0 (UNIT_TEST_PASSED) if passed, not 0 if failed
 */
MCUNIT_TEST( CAblationControlTest, StartAblation_Interaction )
{
	int iResult = UNIT_TEST_PASSED;

	iResult |= McUnitAssertIsOK( g_timeBase.InitializeHardware(), "g_timeBase.InitializeHardware" );
	iResult |= m_setupHelpers.SbcInitAndConnect();

	iResult |= McUnitAssertIsOK( StartAblation(), "StartAblation" );
	iResult |= McUnitAssertIsEqual( ABLATE_STATE_START, GetAblationState(), "GetAblationState" );

	iResult |= AddOpconAndSendAll();

	return iResult;
}

/**
 * @brief	This method tests the paths of PerformTreatment() that should result in ABLATE_STATE_COMPLETE
 * 			due to errors.
 *
 * @return  result as an int: 0 (UNIT_TEST_PASSED) if passed, not 0 if failed
 */
MCUNIT_TEST( CAblationControlTest, PerformTreatmentError_Interaction )
{
	int iResult = UNIT_TEST_PASSED;
	twobytes tbIndex;
	SHdwSimData * pHdwSimData;

	iResult |= SetTreatmentStart();

	iResult |= McUnitAssertIsOK( StartAblation(), "StartAblation" );
	iResult |= McUnitAssertIsEqual( ABLATE_STATE_START, GetAblationState(), "GetAblationState" );

	f_simDataAccess.GetSimData( &pHdwSimData, &tbIndex );
	pHdwSimData->m_tbRfGenPower = g_pTreatmentInfo->getPowerDelivery();
	f_simDataAccess.SetSimData( pHdwSimData );


	// Pump RF generator to get target system on-line
	for ( int i = 0; i < 8; i++ )
		{
		iResult |= McUnitAssertIsEqual( UNIT_TEST_PASSED, m_setupHelpers.RFGenWriteRead(), "RFGenWriteRead", false );
		f_simDataAccess.GoToNextRecord();
		}

		f_simDataAccess.GetSimData( &pHdwSimData, &tbIndex );
		// Test that a high temp will result in the ablation completing and an error being returned
		pHdwSimData->m_coilTemp = g_pTreatmentInfo->getCoilTempUpper() + g_pTreatmentInfo->getCoilTempShutdownTol() + 1;
		f_simDataAccess.SetSimData( pHdwSimData );
		g_pTemperature->ReadFromHardware();
		iResult |= McUnitAssertIsEqual( ERR_EXCESSIVE_TEMP, PerformTreatment(), "PerformTreatment", true );
		iResult |= McUnitAssertIsEqual( ABLATE_STATE_COMPLETE, GetAblationState(), "GetAblationState", false );
		iResult |= McUnitAssertIsEqual( UNIT_TEST_PASSED, m_setupHelpers.RFGenWriteRead(), "RFGenWriteRead", false );
		f_simDataAccess.GoToNextRecord();

	return iResult;
}

/**
 * @brief	This method tests the interaction of digital input EXTEND bit at start of a treatment.
 *
 * @return  result as an int: 0 (UNIT_TEST_PASSED) if passed, not 0 if failed
 */
MCUNIT_TEST( CAblationControlTest, StartAtLimit_Interaction )
{
	int iResult = UNIT_TEST_PASSED;
	twobytes tbIndex;
	SHdwSimData * pHdwSimData;

	iResult |= m_setupHelpers.InterlockAll();

	iResult |= McUnitAssertIsOK( StartAblation(), "StartAblation" );
	iResult |= McUnitAssertIsEqual( ABLATE_STATE_START, GetAblationState(), "GetAblationState" );

	f_simDataAccess.GetSimData( &pHdwSimData, &tbIndex );
	pHdwSimData->m_digInput |= ( 1 << DIG_IN_TRIGGER_BIT );
	pHdwSimData->m_digInput |= ( 1 << DIG_IN_MOTOR_EXTEND_BIT );
	f_simDataAccess.SetSimData( pHdwSimData );

	iResult |= McUnitAssertIsOK( g_pDigitalInputOutput->InitializeHardware(),
	        "g_pDigitalInputOutput->InitializeHardware" );
	iResult |= McUnitAssertIsOK( g_pDigitalInputOutput->ReadFromHardware(), "g_pDigitalInputOutput->ReadFromHardware" );

	iResult |= McUnitAssertIsEqual( ERR_PUMP_AT_LIMIT, PerformTreatment(), "PerformTreatment" );

	f_simDataAccess.GetSimData( &pHdwSimData, &tbIndex );
	pHdwSimData->m_digInput &= ~( 1 << DIG_IN_MOTOR_EXTEND_BIT );
	f_simDataAccess.SetSimData( pHdwSimData );
	iResult |= McUnitAssertIsOK( g_pDigitalInputOutput->ReadFromHardware(), "g_pDigitalInputOutput->ReadFromHardware" );

	iResult |= AddOpconAndSendAll();

	return iResult;
}

/**
 * @brief	This method tests the interaction of starting a treatment with all good status.
 *
 * @return  result as an int: 0 (UNIT_TEST_PASSED) if passed, not 0 if failed
 */
MCUNIT_TEST( CAblationControlTest, StartInitiateVaporGood_Interaction )
{
	int iResult = UNIT_TEST_PASSED;
	twobytes tbIndex;
	SHdwSimData * pHdwSimData;

	iResult |= McUnitAssertIsEqual( UNIT_TEST_PASSED, m_setupHelpers.InterlockAll(), "InterlockAll" );

	f_simDataAccess.GetSimData( &pHdwSimData, &tbIndex );
	pHdwSimData->m_digInput |= ( 1 << DIG_IN_TRIGGER_BIT );
	f_simDataAccess.SetSimData( pHdwSimData );
	iResult |= McUnitAssertIsOK( g_pDigitalInputOutput->ReadFromHardware(), "g_pDigitalInputOutput->ReadFromHardware" );
	iResult |= McUnitAssertIsOK( g_pTemperature->ReadFromHardware(), "g_pTemperature->ReadFromHardware" );

	iResult |= McUnitAssertIsOK( StartAblation(), "StartAblation" );
	iResult |= McUnitAssertIsEqual( ABLATE_STATE_START, GetAblationState(), "GetAblationState" );
	iResult |= McUnitAssertIsOK( PerformTreatment(), "PerformTreatment" );
	iResult |= McUnitAssertIsEqual( ABLATE_STATE_ABLATE, GetAblationState(), "GetAblationState" );

	iResult |= AddOpconAndSendAll();

	return iResult;
}

/**
 * @brief	This method tests processing of a fault (coil over temp) at the start of an ablation
 *
 * @return  result as an int: 0 (UNIT_TEST_PASSED) if passed, not 0 if failed
 */
MCUNIT_TEST( CAblationControlTest, StartInitiateVaporError_Interaction )
{
	int iResult = UNIT_TEST_PASSED;
	twobytes tbIndex;
	SHdwSimData * pHdwSimData;

	f_simDataAccess.GetSimData( &pHdwSimData, &tbIndex );
	pHdwSimData->m_digInput &= ~( 1 << DIG_IN_TRIGGER_BIT );
	f_simDataAccess.SetSimData( pHdwSimData );
	iResult |= McUnitAssertIsOK( g_pDigitalInputOutput->ReadFromHardware(), "g_pDigitalInputOutput->ReadFromHardware" );

	iResult |= McUnitAssertIsOK( StartAblation(), "StartAblation" );
	iResult |= McUnitAssertIsEqual( ABLATE_STATE_START, GetAblationState(), "GetAblationState" );

	pHdwSimData->m_coilTemp = 0;
	pHdwSimData->m_intTemp = g_pTreatmentInfo->getMaxInternalTemp() - 1;
	f_simDataAccess.SetSimData( pHdwSimData );
	g_pTemperature->ReadFromHardware();

	iResult |= McUnitAssertIsEqual( ERR_THERMO_ERROR, PerformTreatment(), "PerformTreatment" );
	iResult |= McUnitAssertIsEqual( ABLATE_STATE_COMPLETE, GetAblationState(), "GetAblationState" );

	iResult |= AddOpconAndSendAll();

	return iResult;
}

/**
 * @brief	This method tests the execution of an ablation to its completion
 *
 * @return  result as an int: 0 (UNIT_TEST_PASSED) if passed, not 0 if failed
 */
MCUNIT_TEST( CAblationControlTest, MiddleTimeComplete_Interaction )
{
	int iResult = UNIT_TEST_PASSED;
	twobytes tbIndex;
	tick tFirst;
	SHdwSimData * pHdwSimData;
	twobytes tbInitTreatCnt;
	twobytes tbRecordTreatCnt;

	tbInitTreatCnt = g_pTreatmentRecord->GetInitiatedTreatmentCount();
	tbRecordTreatCnt = g_pTreatmentRecord->GetRecordTreatmentCount();

	iResult |= SetTreatmentStart();

	iResult |= McUnitAssertIsOK( StartAblation(), "StartAblation" );
	iResult |= McUnitAssertIsEqual( ABLATE_STATE_START, GetAblationState(), "GetAblationState" );

	GetTick( &tFirst );
	twobytes tbMs;
	f_simDataAccess.GetSimData( &pHdwSimData, &tbIndex );
	pHdwSimData->m_tbRfGenPower = g_pTreatmentInfo->getPowerDelivery();
	f_simDataAccess.SetSimData( pHdwSimData );

	// Pump RF generator to get target system on-line
	for ( int i = 0; i < 8; i++ )
		{
		iResult |= McUnitAssertIsEqual( UNIT_TEST_PASSED, m_setupHelpers.RFGenWriteRead(), "RFGenWriteRead", false );
		f_simDataAccess.GoToNextRecord();
		}

	for ( tbMs = 0; tbMs < MS_PER_TENTHS * g_pTreatmentInfo->getTvapor(); tbMs += RND_ROBIN_LOOP_MS )
		{
		m_setupHelpers.SetTick( tFirst + (tick) MS_TO_TICKS( tbMs ) );
		iResult |= McUnitAssertIsOK( PerformTreatment(), "PerformTreatment", false );
		iResult |= McUnitAssertIsEqual( ABLATE_STATE_ABLATE, GetAblationState(), "GetAblationState", false );
		iResult |= McUnitAssertIsEqual( UNIT_TEST_PASSED, m_setupHelpers.RFGenWriteRead(), "RFGenWriteRead", false );
		f_simDataAccess.GoToNextRecord();
		}

	m_setupHelpers.SetTick( tFirst + (tick) MS_TO_TICKS( tbMs ) );

	iResult |= McUnitAssertIsOK( PerformTreatment(), "PerformTreatment" );
	iResult |= McUnitAssertIsEqual( ABLATE_STATE_COMPLETE, GetAblationState(), "GetAblationState" );

	iResult |= CheckGenHalted();

	iResult |= McUnitAssertIsOK( PerformTreatment(), "PerformTreatment" );
	iResult |= McUnitAssertIsEqual( ABLATE_STATE_COMPLETE, GetAblationState(), "GetAblationState" );

	tbInitTreatCnt++;
	iResult |= McUnitAssertIsEqual( tbInitTreatCnt, g_pTreatmentRecord->GetInitiatedTreatmentCount(),
	        "g_pTreatmentRecord->GetInitiatedTreatmentCount" );
	tbRecordTreatCnt++;
	iResult |= McUnitAssertIsEqual( tbRecordTreatCnt, g_pTreatmentRecord->GetRecordTreatmentCount(),
	        "g_pTreatmentRecord->GetRecordTreatmentCount" );

	iResult |= AddOpconAndSendAll();

	return iResult;
}

/**
 * @brief	This method tests that the treatment has completed the ramp and correctly set for the
 * 			middle of the ablation
 *
 * @return  result as an int: 0 (UNIT_TEST_PASSED) if passed, not 0 if failed
 */
MCUNIT_TEST( CAblationControlTest, MiddleEndRamp_Interaction )
{
	int iResult = UNIT_TEST_PASSED;
	tick tFirst;

	iResult |= SetTreatmentStart();

	iResult |= McUnitAssertIsOK( StartAblation(), "StartAblation" );
	iResult |= McUnitAssertIsEqual( ABLATE_STATE_START, GetAblationState(), "GetAblationState" );

	GetTick( &tFirst );
	twobytes tbMs;
	for ( tbMs = 0; tbMs < 4 * RND_ROBIN_LOOP_MS; tbMs += RND_ROBIN_LOOP_MS )
		{
		m_setupHelpers.SetTick( tFirst + (tick) MS_TO_TICKS( tbMs ) );
		iResult |= McUnitAssertIsOK( PerformTreatment(), "PerformTreatment" );
		iResult |= McUnitAssertIsEqual( ABLATE_STATE_ABLATE, GetAblationState(), "GetAblationState" );
		}

	long int ilVel;
	iResult |= McUnitAssertIsOK( g_pWaterCntl->GetFlowRateUlPerMin( &ilVel ), "g_pWaterCntl->GetFlowRateUlPerMin" );
	iResult |= McUnitAssertIsEqual( ilVel, (long int) g_pTreatmentInfo->getDeliveryMax(), "setDeliveryMax at start" );

	iResult |= McUnitAssertIsEqual( g_pTreatmentInfo->getPowerDelivery(), g_pRFGen->GetPowerRequest(), "Power at start" );

	iResult |= McUnitAssertIsOK( PerformTreatment(), "PerformTreatment" );
	iResult |= McUnitAssertIsEqual( ABLATE_STATE_ABLATE, GetAblationState(), "GetAblationState" );

	iResult |= McUnitAssertIsOK( g_pWaterCntl->GetFlowRateUlPerMin( &ilVel ), "g_pWaterCntl->GetFlowRateUlPerMin" );
	iResult |= McUnitAssertIsEqual( ilVel, (long int) g_pTreatmentInfo->getDeliveryMax(), "setDeliveryMax after ramp" );

	iResult |= McUnitAssertIsEqual( g_pTreatmentInfo->getPowerDelivery(), g_pRFGen->GetPowerRequest(),
	        "Custom7 after ramp" );

	iResult |= AddOpconAndSendAll();

	return iResult;
}

/**
 * @brief	This method tests the trigger being release half-way into the ramp
 *
 * @return  result as an int: 0 (UNIT_TEST_PASSED) if passed, not 0 if failed
 */
MCUNIT_TEST( CAblationControlTest, MiddleTriggerOff_Interaction )
{
	int iResult = UNIT_TEST_PASSED;
	twobytes tbIndex;
	tick tFirst;
	SHdwSimData * pHdwSimData;
	twobytes tbInitTreatCnt;
	twobytes tbRecordTreatCnt;
	twobytes tbMs = 0;

	tbInitTreatCnt = g_pTreatmentRecord->GetInitiatedTreatmentCount();
	tbRecordTreatCnt = g_pTreatmentRecord->GetRecordTreatmentCount();

	iResult |= SetTreatmentStart();

	GetTick( &tFirst );
	iResult |= McUnitAssertIsOK( StartAblation(), "StartAblation" );
	iResult |= McUnitAssertIsEqual( ABLATE_STATE_START, GetAblationState(), "GetAblationState" );

	tFirst += ( RND_ROBIN_TICK_CNT / 4 ); // edge count ahead to avoid RF gen time use issues

	for ( tbMs = 0; tbMs < 10 * RND_ROBIN_LOOP_MS; tbMs += RND_ROBIN_LOOP_MS )
		{
		m_setupHelpers.SetTick( tFirst += RND_ROBIN_TICK_CNT );
		iResult |= McUnitAssertIsOK( PerformTreatment(), "PerformTreatment" );
		iResult |= McUnitAssertIsEqual( ABLATE_STATE_ABLATE, GetAblationState(), "GetAblationState" );
		}

	// Turn off trigger
	f_simDataAccess.GetSimData( &pHdwSimData, &tbIndex );
	pHdwSimData->m_digInput &= ~( 1 << DIG_IN_TRIGGER_BIT );
	f_simDataAccess.SetSimData( pHdwSimData );
	iResult |= McUnitAssertIsOK( g_pDigitalInputOutput->ReadFromHardware(), "g_pDigitalInputOutput->ReadFromHardware" );

	// Wait out Tdeb
	for ( tbMs = 0; tbMs < (twobytes) g_pTreatmentInfo->getTdeb(); tbMs += RND_ROBIN_LOOP_MS )
		{
		m_setupHelpers.SetTick( tFirst += RND_ROBIN_TICK_CNT + 2 ); // Edge time ahead to ensure consistent tigger OFF event
		iResult |= McUnitAssertIsOK( PerformTreatment(), "PerformTreatment" );
		iResult |= McUnitAssertIsEqual( ABLATE_STATE_ABLATE, GetAblationState(), "GetAblationState" );
		}

	m_setupHelpers.SetTick( tFirst += RND_ROBIN_TICK_CNT );
	iResult |= McUnitAssertIsEqual( ERR_TRIGGER_OFF, PerformTreatment(), "PerformTreatment" );
	iResult |= McUnitAssertIsEqual( ABLATE_STATE_COMPLETE, GetAblationState(), "GetAblationState" );

	iResult |= CheckGenHalted();

	tbInitTreatCnt++;
	iResult |= McUnitAssertIsEqual( tbInitTreatCnt, g_pTreatmentRecord->GetInitiatedTreatmentCount(),
	        "g_pTreatmentRecord->GetInitiatedTreatmentCount" );
	iResult |= McUnitAssertIsEqual( tbRecordTreatCnt, g_pTreatmentRecord->GetRecordTreatmentCount(),
	        "g_pTreatmentRecord->GetRecordTreatmentCount" );

	iResult |= AddOpconAndSendAll();

	return iResult;;
}

/**
 * @brief	This method tests acting on a fault (coil over temp) during the ramp
 *
 * @return  result as an int: 0 (UNIT_TEST_PASSED) if passed, not 0 if failed
 */
MCUNIT_TEST( CAblationControlTest, MiddleVaporFlowError_Interaction )
{
	int iResult = UNIT_TEST_PASSED;
	twobytes tbIndex;
	tick tFirst;
	SHdwSimData * pHdwSimData;
	twobytes tbInitTreatCnt;
	twobytes tbRecordTreatCnt;

	tbInitTreatCnt = g_pTreatmentRecord->GetInitiatedTreatmentCount();
	tbRecordTreatCnt = g_pTreatmentRecord->GetRecordTreatmentCount();

	iResult |= SetTreatmentStart();

	iResult |= McUnitAssertIsOK( StartAblation(), "StartAblation" );
	iResult |= McUnitAssertIsEqual( ABLATE_STATE_START, GetAblationState(), "GetAblationState" );

	GetTick( &tFirst );
	twobytes tbMs;
	for ( tbMs = 0; tbMs < 4 * RND_ROBIN_LOOP_MS; tbMs += RND_ROBIN_LOOP_MS )
		{
		m_setupHelpers.SetTick( tFirst + (tick) MS_TO_TICKS( tbMs ) );
		iResult |= McUnitAssertIsOK( PerformTreatment(), "PerformTreatment" );
		iResult |= McUnitAssertIsEqual( ABLATE_STATE_ABLATE, GetAblationState(), "GetAblationState" );
		iResult |= McUnitAssertIsEqual( UNIT_TEST_PASSED, m_setupHelpers.RFGenWriteRead(), "RFGenWriteRead", false );
		}

	f_simDataAccess.GetSimData( &pHdwSimData, &tbIndex );
	pHdwSimData->m_outletTemp = g_pTreatmentInfo->getOutletTempMax() + g_pTreatmentInfo->getOutletTempShutdownTol() + 1;
	f_simDataAccess.SetSimData( pHdwSimData );
	iResult |= McUnitAssertIsOK( g_pTemperature->ReadFromHardware(), "g_pTemperature->ReadFromHardware" );

	m_setupHelpers.SetTick( tFirst + (tick) MS_TO_TICKS( tbMs ) );

	iResult |= McUnitAssertIsEqual( ERR_EXCESSIVE_TEMP, PerformTreatment(), "PerformTreatment" );
	iResult |= McUnitAssertIsEqual( ABLATE_STATE_COMPLETE, GetAblationState(), "GetAblationState" );

	iResult |= CheckGenHalted();

	iResult |= McUnitAssertIsOK( PerformTreatment(), "PerformTreatment" );
	iResult |= McUnitAssertIsEqual( ABLATE_STATE_COMPLETE, GetAblationState(), "GetAblationState" );

	tbInitTreatCnt++;
	iResult |= McUnitAssertIsEqual( tbInitTreatCnt, g_pTreatmentRecord->GetInitiatedTreatmentCount(),
	        "g_pTreatmentRecord->GetInitiatedTreatmentCount" );
	iResult |= McUnitAssertIsEqual( tbRecordTreatCnt, g_pTreatmentRecord->GetRecordTreatmentCount(),
	        "g_pTreatmentRecord->GetRecordTreatmentCount" );

	iResult |= AddOpconAndSendAll();

	return iResult;
}

/**
 * @brief	This method tests the GetProgress method.  Accuracy of the computation
 * 			of progress is not tested, but rather that the progress increases as
 * 			expected and returns a 100% completion rate at the end of an ablation.
 *
 * @return  result as an int: 0 (UNIT_TEST_PASSED) if passed, not 0 if failed
 */
MCUNIT_TEST( CAblationControlTest, GetProgress_Interaction )
{
	int iResult = UNIT_TEST_PASSED;
	tick tFirst;
	byte bProgress = 255;
	byte bLastProgress = 0;
	tick maxTime = SEC_TENTHS_TO_TICKS( g_pTreatmentInfo->getTvapor() );

	m_setupHelpers.SetTick( (tick) 0 );
	iResult |= McUnitAssertIsOK( StartAblation(), "StartAblation" );
	iResult |= McUnitAssertIsOK( GetProgress( &bProgress ), "GetProgress: 0" );
	iResult |= McUnitAssertIsEqual( (byte)0, bProgress, "0", true );
	GetTick( &tFirst );

	// Progress should always increase, but should not reach 100
	for ( tick tbMs = 1000; tbMs < maxTime; tbMs += 1000 )
		{
		m_setupHelpers.SetTick( tFirst + (tick) MS_TO_TICKS( tbMs ) );
		iResult |= McUnitAssertIsOK( GetProgress( &bProgress ), "GetProgress" );
		McUnitAssertIsEqual( (bool)(bLastProgress < bProgress), (bool)true, "Increased progress", true );
		bLastProgress = bProgress;
		}

	// When we reach the ablation time, progress should be 100
	m_setupHelpers.SetTick( tFirst + (tick) MS_TO_TICKS( maxTime ) );
	iResult |= McUnitAssertIsOK( GetProgress( &bProgress ), "GetProgress" );
	McUnitAssertIsEqual( (byte)100, bProgress, "Progress is 100%", true );

	// Ensure progress remains at 100
	m_setupHelpers.SetTick( tFirst + (tick) MS_TO_TICKS( maxTime ) + (tick)1000 );
	iResult |= McUnitAssertIsOK( GetProgress( &bProgress ), "GetProgress" );
	McUnitAssertIsEqual( (byte)100, bProgress, "Progress is 100%", true );

	// Ensure the progress isn't sticky
	iResult |= McUnitAssertIsOK( StartAblation(), "StartAblation" );
	iResult |= McUnitAssertIsOK( GetProgress( &bProgress ), "GetProgress" );
	iResult |= McUnitAssertIsEqual( (byte)0, bProgress, "Progress reset to 0", true );

	return iResult;
}

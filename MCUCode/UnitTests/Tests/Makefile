# Control running and analysis of tests

.SECONDEXPANSION:

# Directory root for unit testing
UNIT_DIR = ../

# Path to simulator executable
CVAS_SIM = $(UNIT_DIR)../simavr-simavr/SimCVAS/SimCVAS.exe
CVAS_SIM_RDO = $(UNIT_DIR)../simavr-simavr/SimCVAS/SimCVASRDO.exe

#ELF dirs
ELF_DIR_LIST := $(wildcard ../C*)
PWD := $(shell pwd)
ELF_DIR_DIR_LIST = $(subst ..,$(PWD)/..,$(ELF_DIR_LIST))

# Data file location
DATA_DIR = DataFiles/

# Output file location and extension
OUT_DIR = TestOutput/
OUT_EXT = .txt
STDOUT_EXT = .Stdouttxt
RDO_OUT_EXT = RDO.txt
RDO_STDOUT_EXT = RDO.Stdouttxt
TEST_REP = $(OUT_DIR)UnitTestReport.txt

# Gold file location and extension
GOLD_DIR = GoldFiles/
GOLD_EXT = .Difftxt

# Create list of "diff" files for each gold file. 
GOLDS	:= ${wildcard $(GOLD_DIR)*$(OUT_EXT)}
GOLDS_OUT := $(patsubst %$(OUT_EXT),%$(GOLD_EXT),$(GOLDS))
GOLDS_OUT := $(foreach fyle, $(GOLDS_OUT), $(OUT_DIR)$(notdir $(fyle)))
GOLDS_OUT_ANY := $(OUT_DIR)*$(GOLD_EXT)

# Collect ELF files
ELF_EXT = .elf
UT_ELFS := $(wildcard ../C*/*$(ELF_EXT))
UT_TEXTS := $(foreach fyle, $(UT_ELFS), $(OUT_DIR)$(notdir $(fyle)))
UT_TEXTS := $(patsubst %$(ELF_EXT),%$(OUT_EXT),$(UT_TEXTS))
RDO_UT_TEXTS := $(foreach fyle, $(UT_ELFS), $(OUT_DIR)$(notdir $(fyle)))
RDO_UT_TEXTS := $(patsubst %$(ELF_EXT),%$(RDO_OUT_EXT),$(RDO_UT_TEXTS))

# Default data file
DEFAULT_DATA := $(DATA_DIR)simtest1.csv 

.PHONY: clean all test runtests GoldCheck FailCheck 

#all: $(UT_TEXTS)
all: $(RDO_UT_TEXTS)

clean: 
	@rm -rf $(OUT_DIR)/*

# With iTherm test: $(UT_TEXTS) $(RDO_UT_TEXTS) $(TEST_REP) FailCheck GoldCheck 
test: $(RDO_UT_TEXTS) $(TEST_REP) FailCheck GoldCheck 
	@echo "Unit Tests completed"

#runtests: clean $(UT_TEXTS) $(RDO_UT_TEXTS) 
runtests: clean $(RDO_UT_TEXTS) 

vpath %$(ELF_EXT) $(ELF_DIR_DIR_LIST)

$(UT_TEXTS): $(OUT_DIR)%$(OUT_EXT) : $(CVAS_SIM) Makefile $(filter %$(notdir $(@:%$(OUT_EXT)=%$(ELF_EXT))),$(UT_ELFS)) 
	@echo Running iTherm: $(filter %$(notdir $(@:%$(OUT_EXT)=%$(ELF_EXT))),$(UT_ELFS)) 
	-@if ! $(CVAS_SIM) -d $(DEFAULT_DATA) -l $@ $(filter %$(notdir $(@:%$(OUT_EXT)=%$(ELF_EXT))),$(UT_ELFS)) > $(@:%$(OUT_EXT)=%$(STDOUT_EXT)) ; then \
		echo "Test failed in (iTherm sim): " $(filter %$(notdir $(@:%$(OUT_EXT)=%$(ELF_EXT))),$(UT_ELFS)) ; \
	fi
	
#$(RDO_UT_TEXTS): $(OUT_DIR)%$(RDO_OUT_EXT) : %$(ELF_EXT) $(CVAS_SIM_RDO) Makefile
$(RDO_UT_TEXTS): $(OUT_DIR)%$(RDO_OUT_EXT) : $(CVAS_SIM_RDO) Makefile $(filter %$(notdir $(@:%$(RDO_OUT_EXT)=%$(ELF_EXT))),$(UT_ELFS))
	@echo Running RDO: $(filter %$(notdir $(@:%$(RDO_OUT_EXT)=%$(ELF_EXT))),$(UT_ELFS)) 
	-@if ! $(CVAS_SIM_RDO) -d $(DEFAULT_DATA) -l $@ $(filter %$(notdir $(@:%$(RDO_OUT_EXT)=%$(ELF_EXT))),$(UT_ELFS)) > $(@:%$(RDO_OUT_EXT)=%$(RDO_STDOUT_EXT)) ; then \
		echo "Test failed in (RDO sim): " $(filter %$(notdir $(@:%$(RDO_OUT_EXT)=%$(ELF_EXT))),$(UT_ELFS)) ; \
	fi
	
	
# itherm $(TEST_REP) : $(UT_TEXTS) $(RDO_UT_TEXTS)
$(TEST_REP) : $(RDO_UT_TEXTS)
	@rm -rf $(TEST_REP)
	@for fyle in $^; do  \
		cat $$fyle >> $(TEST_REP); \
	done

# Invoke gold diff and give pass/fail for overall results. One bad diff is a fail
GoldCheck: $(GOLDS_OUT)
	@export files=$$(ls $(GOLDS_OUT_ANY) 2> /dev/null | wc -l); \
	if [ "$$files" != "0" ]; then \
	  echo Gold file diff error ; \
	  exit 1 ; \
	fi

# Invoke gold diff and give pass/fail for overall results. One bad diff is a fail
FailCheck: $(GOLDS_OUT)
	@export files=$$(grep "Failed" $(TEST_REP) 2> /dev/null | wc -l); \
	if [ "$$files" != "0" ]; then \
	  echo Unit test failed error ; \
	  exit 1 ; \
	fi	
	
# Perform diff between gold files and outputs
# itherm %$(GOLD_EXT) : %$(OUT_EXT) %$(RDO_OUT_EXT) $(GOLD_DIR)$(notdir $(@:$(GOLD_EXT)=$(OUT_EXT)))
%$(GOLD_EXT) : %$(RDO_OUT_EXT) $(GOLD_DIR)$(notdir $(@:$(GOLD_EXT)=$(OUT_EXT)))
	-@if /cygdrive/c/cygwin64/bin/diff -I"Library version: " $(GOLD_DIR)$(notdir $(@:$(GOLD_EXT)=$(OUT_EXT))) $(@:$(GOLD_EXT)=$(OUT_EXT)) > $@ ; then \
	  rm -f $@; \
	else \
	  echo "Error versus gold file for: $@"; \
	fi 

dump:
	@echo $(GOLDS)
	@echo $(GOLDS_OUT)
	@echo $(UT_ELFS)
	@echo $(UT_TEXTS)
	@echo $(ELF_DIR_LIST)
	@echo $(ELF_DIR_DIR_LIST)
	
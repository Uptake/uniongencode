/**
 * @file	SetupHelpers.h
 * @par		Package: CvasMcu
 * @par		Project: CVAS Generator
 * @par		Copyright (c) 2016, Uptake Medical, Inc.
 * @author	Glen Apr 13, 2012 Created file
 * @brief	This file contains the declaration for CSetupHelpers
 */

#ifndef SETUPHELPERS_H_
#define SETUPHELPERS_H_

// Include Files
#include "CvasMcuDefinitions.h"
#include "McUnit.h"
#include "RFGenerator.h"
#include "TimerAccess.h"
#include "TimeBase.h"
#include "HdwSimData.h"

// Public Macros and Constants
#define ITHERM_SEND_INITIAL_COMMAND	(1)
#define RF_GEN_COMPLETE_POWER_UP	(15)

// Public Type Definitions (Enums, Structs & Classes)

/**
 * @class	CSetupHelpers
 * @brief	This class provides methods that perform common set-up actions and support functions for
 * 			the unit tests.
 * 			This includes action such as executing the self test, send all buffered logs and getting
 * 			the system to an "interlocked" state. There are more operations than that.
 *
 * Virtual Functions Overridden: None
 *
 * External Data Members: None
 *
 */

class CSetupHelpers: private CMcUnit<CSetupHelpers> , public CTimeBase
{
	SHdwSimData * m_pHdwSimData;
	void * m_stackCheck;
	twobytes m_stackTop;

public:
	CSetupHelpers();
	virtual ~CSetupHelpers(){}
	int InterlockAll();
	int InterlockOneWire();
	int StartConnectToRFGen( twobytes tbLoopCount );
	int SetOperatingConditions( upOperState eOperState, byte bProgress );
	int GetOpCond( SOpconMsg * pOpconMsg );
	int SbcReadWrite( upCvasErr eReadStatus = OK, upCvasErr eWriteStatus = OK );
	int SendOpcondToSbc( upOperState eOperState, byte bProgress = 255 );
	int SbcInitAndConnect();
	int SbcSendAllLogs( upOperState eOperState = OPER_STATE_IDLE, byte bProgress = 255 );
	int RFGenWriteRead( upCvasErr eWriteStatus = OK, upCvasErr eReadStatus = OK );
	int LoadSelfTestFile();
	void SetTick( tick tTick );
	void GetTick( tick * ptTick );
	int ReadSimDataRecord();
	int SetAndReadSimDataRecord( SHdwSimData * pHdwSimData );
	int SetFullTreatUse( byte bFulls, twobytes tbTimePer );
	int CheckStack( bool_c bShow );
	int IdentifyRFGenerator();
	void ResetStackPointer()
	{
		m_stackCheck = NULL;
	}

	twobytes GetStackTop()
	{
		return m_stackTop;
	}

	void ReportStack();
	int ClearPumpRetraction();
};

// Inline Functions (follows class definition)


#endif /* SETUPHELPERS_H_ */

/**
 * @file	UnitTestIntfc.cpp
 * @par		Package: CvasMcu
 * @par		Project: CVAS Generator
 * @par		Copyright (c) 2016, Uptake Medical, Inc.
 * @author	Glen Mar 25, 2011 Created file
 * @brief	This file contains
 */

// Include Files
#include "UnitTestIntfc.h"

// External Public Data

// File Scope Macros and Constants

// File Scope Type Definitions (Enums, Structs & Classes)

// File Scope Data

// File Scope Functions

// Public Functions

/**
 * @brief	This is the constructor for UnitTestIntfc
 *
 */
CUnitTestIntfc::CUnitTestIntfc()
{
}

// Private Class Functions



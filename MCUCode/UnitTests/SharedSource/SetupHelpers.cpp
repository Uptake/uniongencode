/**
 * @file	SetupHelpers.cpp
 * @par		Package: CvasMcu
 * @par		Project: CVAS Generator
 * @par		Copyright (c) 2016, Uptake Medical, Inc.
 * @author	Glen Apr 13, 2012 Created file
 * @brief	This file contains
 */

// Include Files
#include "CvasMcuDefinitions.h"
#include "SetupHelpers.h"
#include "ObjectSingletons.h"

// External Public Data

// File Scope Macros and Constants

// File Scope Type Definitions (Enums, Structs & Classes)

// File Scope Data

// File Scope Functions

// Public Functions

/**
 * @brief	This is the constructor for SetupHelpers
 *
 */

CSetupHelpers::CSetupHelpers() :
	CMcUnit<CSetupHelpers> ( this )
{
	m_stackCheck = NULL;
	m_pHdwSimData = NULL;
	m_stackTop = 0;
}

/**
 * @brief	This method sets up lower-level states so that the system is fully interlocked
 *
 * @return   Status as int
 */
int CSetupHelpers::InterlockAll()
{
	int iResult = UNIT_TEST_PASSED;
	twobytes tbIndex;
	upInterlockFlag eInterlock;

	// Reset good RF gen and SBC and set normal temps
	f_simDataAccess.GetSimData( &m_pHdwSimData, &tbIndex );
	m_pHdwSimData->m_bRfGenAction = UART_NORMAL;
	m_pHdwSimData->m_sbcAction = UART_NORMAL;
	m_pHdwSimData->m_coilTemp = g_pTreatmentInfo->getCoilTempLower() + 1;
	m_pHdwSimData->m_outletTemp = g_pTreatmentInfo->getOutletTempMin() + 1;
	m_pHdwSimData->m_intTemp = g_pTreatmentInfo->getMaxInternalTemp() - 1;
	f_simDataAccess.SetSimData( m_pHdwSimData );

	// Thermocouples
	iResult |= McUnitAssertIsOK( g_pTemperature->InitializeHardware(), "g_pTemperature->InitializeHardware", false );
	iResult |= McUnitAssertIsOK( g_pTemperature->ReadFromHardware(), "g_pTemperature->ReadFromHardware", false );

	// Handshake SBC
	MsDelay( (twobytes) 2 );
	iResult |= SetOperatingConditions( OPER_STATE_IDLE, 0 );
	for ( int i = 0; i < 6; i++ )
		iResult |= SbcReadWrite();

	g_pComOp->VerifyCodeVersions();

	// iTherm connect
	iResult |= StartConnectToRFGen( RF_GEN_POWER_UP_CYCLE_COUNT + RF_GEN_COMPLETE_POWER_UP );

	// Connect 1-wire memory.
	iResult |= InterlockOneWire();

	// Connect DI
	twobytes tbIndx;
	f_simDataAccess.GetSimData( &m_pHdwSimData, &tbIndx );
	m_pHdwSimData->m_digInput = 1 << DIG_IN_SYR_PRESENT_BIT;
	f_simDataAccess.SetSimData( m_pHdwSimData );
	iResult |= McUnitAssertIsOK( g_pDigitalInputOutput->InitializeHardware(),
	        "g_pDigitalInputOutput->InitializeHardware", false );
	iResult |= McUnitAssertIsOK( g_pDigitalInputOutput->ReadFromHardware(), "g_pDigitalInputOutput->ReadFromHardware",
	        false );

	iResult |= McUnitAssertIsOK( g_pGeneratorMonitor->CheckInterlocks( false, &eInterlock, false ), "CheckInterlocks",
	        false );

	iResult |= McUnitAssertIsEqual( INTLCK_ALL, eInterlock, "Interlock test", false );

	if ( UNIT_TEST_PASSED != iResult )
		LogString( "Error in: InterlockAll" );

	return iResult;
}

/**
 * @brief	This method processes a data file so the 1-wire returns a full "presence" of TRUE
 *
 * @return   Status as int
 */
int CSetupHelpers::InterlockOneWire()
{
	int iResult = UNIT_TEST_PASSED;

	// Connect 1-wire memory. File has 52 data rows
	iResult |= McUnitAssertIsOK( f_simDataAccess.LoadDataFile( "DataFiles/FreshCode2OneWire.csv", true ),
	        "Load file DataFiles/FreshCode2OneWire.csv", false );

	g_pOneWireMemory->InitializeHardware();
	g_pOneWireMemory->ClearOwValues();
	g_pOneWireMemory->EnableOwMonitor( true );

	for ( uint8_t i = 0; i < f_simDataAccess.GetSampleCount() - 1; i++ )
		{
		iResult |= McUnitAssertIsOK( g_timeBase.ReadFromHardware(), "g_timeBase.ReadFromHardware", false );
		MsDelay( (twobytes) 2 );
		iResult |= McUnitAssertIsOK( g_pOneWireMemory->ReadFromHardware(), "g_pOneWireMemory->ReadFromHardware", false );
		iResult |= McUnitAssertIsOK( g_pOneWireMemory->Execute(), "Execute", false );
		iResult |= McUnitAssertIsOK( f_simDataAccess.GoToNextRecord(), "GoToNextRecord", false );
		}

	tick tFirst;
	iResult |= McUnitAssertIsOK( CTimeBase::GetTick( &tFirst ), "GetTick", false );

	for ( int i = 0; i < 10; i++ )
		{
		SetTick( tFirst += RND_ROBIN_TICK_CNT );
		iResult |= McUnitAssertIsOK( g_timeBase.ReadFromHardware(), "g_timeBase.ReadFromHardware", false );
		iResult |= McUnitAssertIsOK( g_pOneWireMemory->ReadFromHardware(), "g_pOneWireMemory->ReadFromHardware", false );
		iResult |= McUnitAssertIsOK( g_pOneWireMemory->Execute(), "Execute", false );
		}

	for ( int i = 0; i < 20; i++ )
		{
		iResult |= McUnitAssertIsOK( g_pOneWireMemory->Execute(), "Execute", false );
		}

	if ( UNIT_TEST_PASSED != iResult )
		LogString( "Error in: InterlockOneWire" );

	return iResult;
}

/**
 * @brief	This method performs an "AC ON" operation and waits out the delay time before communication is attempted
 *
 * @return  result as an int: 0 (UNIT_TEST_PASSED) if passed, not 0 if failed
 */

int CSetupHelpers::StartConnectToRFGen( twobytes tbLoopCount )
{
	int iResult = UNIT_TEST_PASSED;
	twobytes tbErr;
	upCvasErr eErr = OK;

	f_simDataAccess.GetSimData( &m_pHdwSimData, &tbErr );
	m_pHdwSimData -> m_bRfGenAction = UART_NORMAL;
	// itherm	m_pHdwSimData -> m_tbRfGenStatus = 0xA6;
	m_pHdwSimData -> m_tbRfGenStatus = 0x06;
	f_simDataAccess.SetSimData( m_pHdwSimData );

	iResult |= McUnitAssertIsOK( g_pRFGen->InitializeHardware(), "InitializeHardware()", false );

	iResult |= McUnitAssertIsOK( g_pRFGen->SetACInput( false ), "SetACInput( false )", false );
	iResult |= McUnitAssertIsOK( g_pRFGen->SetACInput( true ), "SetACInput( true )", false );

	// Powering up
	twobytes i;
	for ( i = 0; i < RF_GEN_POWER_UP_CYCLE_COUNT + ITHERM_SEND_INITIAL_COMMAND && i < tbLoopCount; i++ )
		{
		iResult |= McUnitAssertIsEqual( UNIT_TEST_PASSED, RFGenWriteRead(), "RFGenWriteRead", false );
		iResult |= McUnitAssertIsEqual( ERR_RF_GEN_NOT_READY, g_pRFGen->GetError( &tbErr ), "GetError", false );
		}

	// Might be bad as trying RDO
	for ( ; i < RF_GEN_POWER_UP_CYCLE_COUNT + ITHERM_SEND_INITIAL_COMMAND + 1 && i < tbLoopCount; i++ )
		{
		iResult |= McUnitAssertIsEqual( UNIT_TEST_PASSED, RFGenWriteRead(), "RFGenWriteRead", false );
		eErr = g_pRFGen->GetError( &tbErr );
		iResult |= McUnitAssertTrue( ( OK == eErr || ERR_RF_GEN_NOT_READY == eErr ),
		        "GetError ERR_RF_GEN_NOT_READY or OK", false );
		}

	// Status OK
	for ( ; i < tbLoopCount; i++ )
		{
		iResult |= McUnitAssertIsEqual( UNIT_TEST_PASSED, RFGenWriteRead(), "RFGenWriteRead", false );
		eErr = g_pRFGen->GetError( &tbErr );
		iResult |= McUnitAssertTrue( ( OK == eErr || ERR_RF_GEN_NOT_READY == eErr ),
		        "GetError ERR_RF_GEN_NOT_READY or OK", false );
		}

		iResult |= McUnitAssertTrue( OK == eErr, "GetError RF Gen Ready", false );

	if ( UNIT_TEST_PASSED != iResult )
		LogString( "Error in: StartConnectToRFGen" );
	else
		{
		LogString( "Passed: StartConnectToRFGen" );
		iResult |= IdentifyRFGenerator();
		}

	iResult |= SbcSendAllLogs();

	return iResult;
}

/**
 * @brief	This method
 *
 * @return
 */
int CSetupHelpers::GetOpCond( SOpconMsg *pOpconMsg )
{
	upInterlockFlag intTest;

	// set the operating conditions
	g_pGeneratorMonitor->CheckInterlocks( false, &intTest, false );
	pOpconMsg->connMask = (byte) intTest;

	g_pTemperature->GetThermoTemp( THERMO_COIL, &( pOpconMsg->coilTemp ) );
	g_pTemperature->GetThermoTemp( THERMO_OUTLET, &( pOpconMsg->outletTemp ) );
	g_pTemperature->GetThermoTemp( THERMO_INTERIOR, &( pOpconMsg->intTemp ) );
	g_pTemperature->GetFilteredThermoTemp( THERMO_COIL, &( pOpconMsg->firCoilTemp ) );
	g_pTemperature->GetFilteredThermoTemp( THERMO_OUTLET, &( pOpconMsg->firOutletTemp ) );

	g_pRFGen->GetPowerOutput( &( pOpconMsg->tbRfPower ) );
	g_pRFGen->GetTemperature( &( pOpconMsg->iRfTemperature ) );
	g_pRFGen->GetStatus( &( pOpconMsg->tbRfStatus ) );
	g_pRFGen->GetError( &( pOpconMsg->tbRfError ) );

	g_pWaterCntl->GetAppliedVelocity( &( pOpconMsg->waterRate ) );
	g_pWaterCntl->GetStepperPosition( &( pOpconMsg->encoderPos ), &( pOpconMsg->motorVelocity ) );

	g_pDigitalInputOutput->GetAllInput( &( pOpconMsg->digInput ) );
	g_pDigitalInputOutput->GetAllOutput( &( pOpconMsg->digOutput ) );

	g_pLogMessage->GetLogCount( &( pOpconMsg->logMsgs ) );
	g_pOneWireMemory->GetFullTreatmentCount( &( pOpconMsg->treatCnt ) );
	// user alert messages updated via SbcIntfc as well
	g_pLogMessage->GetUserMsgCount( &( pOpconMsg->userMsgs ) );

	return 0;
}

int CSetupHelpers::SetOperatingConditions( upOperState eOperState, byte bProgress )
{
	SOpconMsg * operCond;
	tick tNow;

	GetTick( &tNow );
	// get a pointer to the message contained within the SbcIntfc
	LOG_ERROR_RETURN( g_pSbcIntfc->GetMessage( (void**)&operCond, MSG_TYPE_OPCOND ) );

	GetOpCond( operCond );

	operCond->runTicks = (twobytes) tNow;
	operCond->operState = eOperState;
	operCond->maxTreatments = g_pTreatmentInfo->getDeliveryCount();
	operCond->progress = bProgress;

	return 0;
}

/**
 * @brief	This method does a read/write pair to the SBC interface
 *
 * @param eReadStatus Expected return code for ReadToHardware as upCvasErr with default of OK
 * @param eWriteStatus Expected return code for WriteToHardware as upCvasErr with default of OK
 *
 * @return   Status as upCvasErr
 */
int CSetupHelpers::SbcReadWrite( upCvasErr eReadStatus, upCvasErr eWriteStatus )
{
	int iResult = UNIT_TEST_PASSED;

	iResult |= McUnitAssertIsEqual( eReadStatus, g_pSbcIntfc->ReadFromHardware(), "g_pSbcIntfc->ReadFromHardware",
	        false );
	MsDelay( (twobytes) 2 );
	iResult
	        |= McUnitAssertIsEqual( eWriteStatus, g_pSbcIntfc->WriteToHardware(), "g_pSbcIntfc->WriteToHardware", false );
	MsDelay( (twobytes) 15 );

	if ( UNIT_TEST_PASSED != iResult )
		LogString( "Error in: SbcReadWrite" );

	return iResult;
}

/**
 * @brief	This method puts the SBC subsystem into an active and connected state.
 *
 * @return   Status as int
 */
int CSetupHelpers::SbcInitAndConnect()
{
	int iResult = UNIT_TEST_PASSED;
	iResult |= McUnitAssertIsOK( g_pSbcIntfc->InitializeHardware(), "g_pSbcIntfc->InitializeHardware", false );
	MsDelay( (twobytes) 2 );
	iResult |= SetOperatingConditions( OPER_STATE_IDLE, 0 );
	iResult |= SbcReadWrite();
	iResult |= SbcReadWrite();

	if ( UNIT_TEST_PASSED != iResult )
		LogString( "Error in: SbcInitAndConnect" );

	return iResult;
}

/**
 * @brief	This method sets a fresh opcond and then performs ABC read and write operations until
 * 			all user message and log entries are transmitted to the simulated SBC.
 *
 * 			This results in having a full record of any failures pushed into the text file for a unit test.
 *
 * @return   Status as int
 */
int CSetupHelpers::SbcSendAllLogs( upOperState eOperState, byte bProgress )
{
	int iResult = UNIT_TEST_PASSED;
	int i = 0;
	byte bLogs;
	byte bUsers;

	MsDelay( 5 );
	g_pLogMessage->GetLogCount( &bLogs );
	g_pLogMessage->GetUserMsgCount( &bUsers );
	while ( i++ < LOG_MESSAGE_CNT * 2 + 1 && ( bLogs || bUsers ) )
		{
		iResult |= SetOperatingConditions( eOperState, bProgress );
		iResult |= SbcReadWrite();
		g_pLogMessage->GetLogCount( &bLogs );
		g_pLogMessage->GetUserMsgCount( &bUsers );
		}

	MsDelay( 5 );

	if ( UNIT_TEST_PASSED != iResult )
		LogString( "Error in: SbcSendAllLogs" );

	return iResult;
}

/**
 * @brief	This method does a wrtie/read pair to the RF generator
 *
 * @param eWriteStatus Expected return code for WriteToHardware as upCvasErr with default of OK
 * @param eReadStatus Expected return code for ReadToHardware as upCvasErr with default of OK
 *
 * @return   Status as upCvasErr
 */
int CSetupHelpers::RFGenWriteRead( upCvasErr eWriteStatus, upCvasErr eReadStatus )
{
	int iResult = UNIT_TEST_PASSED;
	byte bPassIndex;

	IncrementOnlyPassFlag( &bPassIndex );
	iResult |= McUnitAssertIsEqual( eWriteStatus, g_pRFGen->WriteToHardware(), "WriteToHardware", false );
	iResult |= McUnitAssertIsEqual( eReadStatus, g_pRFGen->ReadFromHardware(), "ReadFromHardware", false );

	if ( UNIT_TEST_PASSED != iResult )
		LogString( "Error in: RFGenWriteRead" );

	return iResult;
}

/**
 * @brief	This method
 *
 * @return   Status as upCvasErr
 */
int CSetupHelpers::LoadSelfTestFile()
{
	int iResult = UNIT_TEST_PASSED;
	iResult |= McUnitAssertIsOK( f_simDataAccess.LoadDataFile( "DataFiles/SelfTest.csv", true ),
	        "Load file DataFiles/SelfTest.csv", false );
	if ( UNIT_TEST_PASSED != iResult )
		LogString( "Error in: LoadSelfTestFile" );

	return iResult;
}

/**
 * @brief	This method sets the tick counter
 *
 * @param	tTick The new tick value as tick
 *
 */
void CSetupHelpers::SetTick( tick tTick )
{
	CTimeBase::SetTick( tTick );
	CTimeBase::ReadFromHardware();
}

/**
 * @brief	This method gets the tick counter
 *
 * @param	ptTick A pointer to a tick to get the latest tick value
 *
 */
void CSetupHelpers::GetTick( tick * ptTick )
{
	CTimeBase::GetTick( ptTick );
}

/**
 * @brief	This method performs all ReadFromHardware operations needed to load a new data set
 *
 * @return   Cumulative status
 */
int CSetupHelpers::ReadSimDataRecord()
{
	int iResult = UNIT_TEST_PASSED;
	iResult |= McUnitAssertIsOK( g_pDigitalInputOutput->ReadFromHardware(), "g_pDigitalInputOutput->ReadFromHardware",
	        false );
	iResult |= McUnitAssertIsOK( g_pOneWireMemory->Execute(), "g_pOneWireMemory->Execute", false );
	iResult |= McUnitAssertIsOK( g_pTemperature->ReadFromHardware(), "g_pTemperature->ReadFromHardware", false );
	iResult |= McUnitAssertIsEqual( UNIT_TEST_PASSED, RFGenWriteRead(), "RFGenWriteRead", false );
	iResult |= McUnitAssertIsEqual( UNIT_TEST_PASSED, RFGenWriteRead(), "RFGenWriteRead", false );

	if ( UNIT_TEST_PASSED != iResult )
		LogString( "Error in: ReadSimDataRecord" );

	return iResult;
}

/**
 * @brief	This method sets a sim data record into memory and then calls ReadSimDataRecord
 * 			to have that data loaded into the driver objects.
 *
 * @param	pHdwSimData A pointer to the new data in the SHdwSimData
 *
 * @return   Cumulative status
 */
int CSetupHelpers::SetAndReadSimDataRecord( SHdwSimData *pHdwSimData )
{
	f_simDataAccess.SetSimData( pHdwSimData );
	return ReadSimDataRecord();
}

/**
 * @brief	This method clears and sets the treatment count and time in CTreatmentRecord
 *
 * @param	bFulls The number of full treatments as a byte
 * @param	tbTimePer The time in ms for each full treatment as twobytes
 *
 * @return   Cumulative status
 */
int CSetupHelpers::SetFullTreatUse( byte bFulls, twobytes tbTimePer )
{
	int iResult = UNIT_TEST_PASSED;
	twobytes tbCount = 0;

	g_pTreatmentRecord->StartNewRecord();
	for ( byte i = 0; i < bFulls; i++ )
		{
		iResult |= McUnitAssertIsOK( g_pTreatmentRecord->UpdateTreatmentRecord( DELIVERY_FULL, tbTimePer ),
		        "UpdateTreatmentRecord", false );
		iResult |= McUnitAssertIsOK( g_pOneWireMemory->Execute(), "Execute", false );
		iResult |= McUnitAssertIsOK( g_pOneWireMemory->Execute(), "Execute", false );
		iResult |= McUnitAssertIsOK( g_pOneWireMemory->Execute(), "Execute", false );
		iResult |= McUnitAssertIsOK( g_pOneWireMemory->Execute(), "Execute", false );
		iResult |= McUnitAssertIsOK( g_pOneWireMemory->Execute(), "Execute", false );
		}

	g_pOneWireMemory->GetFullTreatmentCount( &tbCount );

	iResult |= McUnitAssertIsEqual( (byte)tbCount, bFulls, "Full Use Set" );

	if ( UNIT_TEST_PASSED != iResult )
		LogString( "Error in: SetFullTreatUse" );

	return iResult;
}

/**
 * @brief	This method tests the stack by seeing if its local variable is allocated in the same place on each run
 *
 */
int CSetupHelpers::CheckStack( bool_c bShow )
{
	int iResult = UNIT_TEST_PASSED;

	if ( NULL == m_stackCheck )
		m_stackCheck = (void*) &iResult;
	else
		iResult = McUnitAssertIsEqual( (twobytes) m_stackCheck, (twobytes) (void*) &iResult, "Stack check", bShow );

	// Check for stack top
#define ZERO_CNT	(10)
	byte bZeroCnt = 0;
	twobytes * pStackPtr;
	for ( pStackPtr = (twobytes*) m_stackCheck; (twobytes) pStackPtr > 0x100 && bZeroCnt < ZERO_CNT; pStackPtr-- )
		{
		if ( 0 == *pStackPtr )
			bZeroCnt++;
		}
	m_stackTop = sizeof(twobytes) * bZeroCnt + (twobytes) pStackPtr;
	return iResult;
}

/**
 * @brief	This method logs the RF generator that is attached
 *
 * @return   Status as int
 */
int CSetupHelpers::IdentifyRFGenerator()
{
	upRFGeneratorModel eModel;
	g_pRFGen->GetRfGenModel( &eModel );
	LogOneValue( "RF Generator code: %d", (int) eModel );
	switch ( eModel )
		{
		case RF_MODEL_ITHERM:
			LogString( "RF Generator model iTherm" );
			break;

		case RF_MODEL_RDO:
			LogString( "RF Generator model RDO" );
			break;

		case RF_MODEL_UNSET:
			LogString( "RF Generator model UNSET" );
			break;

		default:
			LogString( "RF Generator model code error" );
			break;
		}
	return UNIT_TEST_PASSED;
}

/**
 * @brief	This method prints the results of the stack usage analysis
 *
 */
void CSetupHelpers::ReportStack()
{
	LogOneValue( "Stack top: 0x%04X ", m_stackTop );
	LogOneValue( "Stack max size: 0x%04X ", ( 0x1100 - m_stackTop ) );
}

/**
 * @brief	This method constructs a new opcond packet and calls the SBC read/write twice to force
 * 			it across.
 *
 * @param eOperState The current state as upOperState
 * @param bProgress The progress percentage. Optional parameter and defaults to 255.
 *
 * @return   Status as int
 */
int CSetupHelpers::SendOpcondToSbc( upOperState eOperState, byte bProgress )
{
	int iResult = UNIT_TEST_PASSED;

	iResult |= McUnitAssertIsEqual( UNIT_TEST_PASSED, SetOperatingConditions( eOperState, bProgress ),
	        "SetOperatingConditions", false );
	iResult |= McUnitAssertIsEqual( UNIT_TEST_PASSED, SbcReadWrite(), "SbcReadWrite", false );

	return iResult;
}

/**
 * @brief	This method effectively retracts the pump and thus clears the retraction flag
 *
 * @return   Status as int
 */
int CSetupHelpers::ClearPumpRetraction()
{
	int iResult = UNIT_TEST_PASSED;
	twobytes tbIndex;
	SHdwSimData * pHdwSimData;

	// Trigger OFF
	f_simDataAccess.GetSimData( &pHdwSimData, &tbIndex );
	pHdwSimData->m_digInput |= ( 1 << DIG_IN_MOTOR_RETRACT_BIT );
	pHdwSimData->m_digInput &= ~( 1 << DIG_IN_MOTOR_EXTEND_BIT );
	iResult |= SetAndReadSimDataRecord( pHdwSimData );

	// Clear pump retraction
	bool_c bRet;
	iResult |= McUnitAssertIsOK( g_pWaterCntl->PerformRodRetraction( &bRet ), "PerformRodRetraction", false );
	iResult |= McUnitAssertIsEqual( (uint8_t) true, bRet, "Is retracted", false );
	iResult |= McUnitAssertIsEqual( (uint8_t) false, g_pWaterCntl->GetRetracted( &bRet ), "GetRetracted", false );

	return iResult;
}
// Private Class Functions



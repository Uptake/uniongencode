/**
 * @file	ObjectSingletons.h
 * @par		Package: CvasMcu
 * @par		Project: CVAS Generator
 * @par		Copyright (c) 2016, Uptake Medical, Inc.
 * @author	Glen Mar 28, 2012 Created file
 * @brief	This file contains
 */

#ifndef OBJECTSINGLETONS_H_
#define OBJECTSINGLETONS_H_

// Include Files
#include "CvasMcuDefinitions.h"
#include "DiagnosticLed.h"
#include "DigitalInputOutput.h"
#include "Supervisor.h"
#include "Watchdog.h"
#include "Temperature.h"
#include "RFGenerator.h"
#include "OneWireMemory.h"
#include "WaterCntl.h"
#include "SbcIntfc.h"
#include "TimeBase.h"
#include "LogMessage.h"
#include "GeneratorMonitor.h"
#include "TreatmentInformation.h"
#include "CommonOperations.h"
#include "TreatmentRecord.h"
#include "SoundCntl.h"
#include "SelfTest.h"

// External Public Data

// File Scope Macros and Constants

// File Scope Type Definitions (Enums, Structs & Classes)

// File Scope Data

// Singletons without global pointers for sharing the object handle
extern CTimeBase g_timeBase;
extern CWatchdog g_watchdog;
extern CSupervisor g_supervisor;

// Inline Functions (follows class definition)


#endif /* OBJECTSINGLETONS_H_ */

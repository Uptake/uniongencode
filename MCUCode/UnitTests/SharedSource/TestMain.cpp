/**
 * @file	TestMain.cpp
 * @par		Package: McUnit
 * @par		Project: CVAS Generator
 * @par		Copyright (c) 2016, Uptake Medical, Inc.
 * @author	Chris May 13, 2011 Created file
 * @brief	This file contains
 */

// Include Files
#include "TestMain.h"
#include "McUnit.h"
#include "RndRobinScheduler.h"
#include "LogMessage.h"
#include "SimDataAccess.h"
#include "CommonOperations.h"

// External Public Data

// This flag controls execution that is dependent on being on real hardware or not
int g_bIsSimulated = 1;
int g_bTestAccess = 0;

// File Scope Macros and Constants

// File Scope Type Definitions (Enums, Structs & Classes)

// File Scope Data
CTestMain * CTestMain::m_pTestMain = 0;

// File Scope Functions

// Public Functions

extern "C" void __cxa_pure_virtual()
{
	while ( 1 )
		;
}

/**
 * @brief	This is the constructor for CTestMain
 *
 */
CTestMain::CTestMain( CUnitTestIntfc * pUnitTest )
{
	m_pUnitTest = pUnitTest;
	m_pTestMain = this;
}

/**
 * @brief	This method uses the pointer supplied in the constructor to set up and run tests
 *
 * @return   Count of failed tests
 */
int CTestMain::RunUnitTests()
{
	int iTestsFailed = 0;

	if ( 0 == m_pUnitTest )
		return 0xFFFF;

	// perform test setup
	m_pUnitTest->UnitTestListSetup();

	// run the tests
	iTestsFailed = m_pUnitTest->CommonTestHandle();
	return iTestsFailed;
}

// Private Class Functions


/**
 * @brief	This method is the main program for all unit tests
 *
 * @return   Count of failed tests
 */
int main()
{
	int iFailed;

	// Call static function
	if ( 0 != CTestMain::GetHandle() )
		iFailed = CTestMain::GetHandle()->RunUnitTests();
	else
		iFailed = 0xFFFF;

	// Terminate simulation
	f_simDataAccess.HaltSimulation();

	// Never actually gets here
	return iFailed;
}


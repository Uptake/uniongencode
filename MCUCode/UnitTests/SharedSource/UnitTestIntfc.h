/**
 * @file	UnitTestIntfc.h
 * @par		Package: CvasMcu
 * @par		Project: CVAS Generator
 * @par		Copyright (c) 2016, Uptake Medical, Inc.
 * @author	Glen Mar 25, 2011 Created file
 * @brief	This file contains the declaration of CUnitTestIntfc
 */

#ifndef UNITTESTINTFC_H_
#define UNITTESTINTFC_H_

// Include Files

// Public Macros and Constants

// Public Type Definitions (Enums, Structs & Classes)

/**
 * @class	CUnitTestIntfc
 * @brief	This class provides the interface definition for the methods used to create the suites of
 * 			unit tests and the invoke the unit tests.
 * 			These methods are overridden in every actual unit test class and are called from CTestMain.
 * 			This allows CTestMain::RunUnitTests to invoke the tests transparently to the class that has
 * 			defined the methods.
 *
 * Virtual Functions Overridden: None @n@n
 *
 * External Data Members: None @n@n
 *
 */

class CUnitTestIntfc
{
public:
	CUnitTestIntfc();
	virtual int CommonTestHandle() = 0;
	virtual void UnitTestListSetup() = 0;
};

// Inline Functions (follows class definition)


#endif /* UNITTESTINTFC_H_ */

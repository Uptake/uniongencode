/**
 * @file	TestMain.h
 * @par		Package: CvasMcuHardwareTest
 * @par		Project: CVAS Generator
 * @par		Copyright (c) 2016, Uptake Medical, Inc.
 * @author	Chris May 13, 2011 Created file
 * @brief	This file contains
 */

#ifndef TESTMAIN_H_
#define TESTMAIN_H_

// Include Files
#include "UnitTestIntfc.h"
#include "CvasMcuError.h"

// Public Macros and Constants

// Public Type Definitions (Enums, Structs & Classes)

/**
 * @class	CTestMain
 * @brief	This class provides the top level hooks so that all test classes may be run without creating new main() routines
 *
 * Virtual Functions Overridden: None @n@n
 *
 * External Data Members: None @n@n
 *
 */

class CTestMain
{
public:
	CUnitTestIntfc * m_pUnitTest;
	static CTestMain * m_pTestMain;
	static CTestMain * GetHandle(){return m_pTestMain;};
	CTestMain( CUnitTestIntfc * pUnitTest );
	int RunUnitTests();
	#ifdef USE_DESTRUCTOR
	virtual ~CMyTestClass();
#endif
};

// Inline Functions (follows class definition)


#endif /* TESTMAIN_H_ */

/**
 * @file	ObjectSingletons.cpp
 * @par		Package: CvasMcu
 * @par		Project: CVAS Generator
 * @par		Copyright (c) 2016, Uptake Medical, Inc.
 * @author	Glen Mar 28, 2012 Created file
 * @brief	This file contains
 */

// Include Files
#include "ObjectSingletons.h"

// External Public Data

// File Scope Macros and Constants

// File Scope Type Definitions (Enums, Structs & Classes)

// File Scope Data

// Singletons without global pointers for sharing the object handle
CTimeBase g_timeBase;
CWatchdog g_watchdog;
CSupervisor g_supervisor;

// Declare singletons for object in round-robin scheduling that have global handles
CDiagnosticLed f_diagLed;
CDigitalInputOutput f_digIO;
CTemperature f_temperature;
CRFGenerator f_rfGenerator;
CWaterCntl f_waterCntl;
CSoundCntl f_soundCntl;

// These items are placed into the external memory
CSbcIntfc f_sbcIntfc;
CLogMessage f_logMsg;
COneWireMemory f_oneWire;

// Declare singletons of objects that have global handles but are not directly in the round-robin scheme
CGeneratorMonitor f_generatorMonitor;
CTreatmentInformation f_theraInfo;
CCommonOperations f_commonOperations;
CTreatmentRecord f_treatmentRecord;
CSelfTest f_selfTest;


// File Scope Functions

// Public Functions

/**
 * @brief	This is the constructor for ObjectSingletons
 *
 */

// Private Class Functions



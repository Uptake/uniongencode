/*
 *	Copyright (c) 2010, tn
 *	All rights reserved.
 *
 *	Redistribution and use in source and binary forms, with or without
 *	modification, are permitted provided that the following conditions are met:
 *	    * Redistributions of source code must retain the above copyright
 *	      notice, this list of conditions and the following disclaimer.
 *	    * Redistributions in binary form must reproduce the above copyright
 *	      notice, this list of conditions and the following disclaimer in the
 *	      documentation and/or other materials provided with the distribution.
 *	    * Neither the name of the <organization> nor the
 *	      names of its contributors may be used to endorse or promote products
 *	      derived from this software without specific prior written permission.
 *
 *	THIS SOFTWARE IS PROVIDED BY THE COPYRIGHT HOLDERS AND CONTRIBUTORS "AS IS" AND
 *	ANY EXPRESS OR IMPLIED WARRANTIES, INCLUDING, BUT NOT LIMITED TO, THE IMPLIED
 *	WARRANTIES OF MERCHANTABILITY AND FITNESS FOR A PARTICULAR PURPOSE ARE
 *	DISCLAIMED. IN NO EVENT SHALL <COPYRIGHT HOLDER> BE LIABLE FOR ANY
 *	DIRECT, INDIRECT, INCIDENTAL, SPECIAL, EXEMPLARY, OR CONSEQUENTIAL DAMAGES
 *	(INCLUDING, BUT NOT LIMITED TO, PROCUREMENT OF SUBSTITUTE GOODS OR SERVICES;
 *	LOSS OF USE, DATA, OR PROFITS; OR BUSINESS INTERRUPTION) HOWEVER CAUSED AND
 *	ON ANY THEORY OF LIABILITY, WHETHER IN CONTRACT, STRICT LIABILITY, OR TORT
 *	(INCLUDING NEGLIGENCE OR OTHERWISE) ARISING IN ANY WAY OUT OF THE USE OF THIS
 *	SOFTWARE, EVEN IF ADVISED OF THE POSSIBILITY OF SUCH DAMAGE.
 */

#ifndef MCUNIT_H_
#define MCUNIT_H_

#define MAX_TEMP_STR (128)

// Include Files
#include "CvasMcuTypes.h"
#include "CvasMcuError.h"
#include "SimDataAccess.h"
#include <string.h>
#include <stdio.h>
#include <avr/io.h>

// Public Macros and Constants

// Public Type Definitions (Enums, Structs & Classes)
static char f_tempStr[ MAX_TEMP_STR ];

void operator delete(void * p); // or delete(void *, std::size_t)

/**
 *	Declare your test functions using this macro
 *
 *  Function pointer takes pointer to test struct to allow
 *  message and result setting
 */
#define MCUNIT_TEST_DEC(TestName)	int TestName()
#define MCUNIT_TEST(ClassName, TestName)	int ClassName::TestName()
#define CALL_MEMBER_FN(object,ptrToMember)  ((object->*(ptrToMember->test))())

/**
 * Add tests to the list using this macro
 */
#define ADD_TEST_LIST( LIST_STRUCT, INDEX, TEST_FUNC, NAME, MSG ) \
		AddTestList( LIST_STRUCT, sizeof(LIST_STRUCT), &INDEX, TEST_FUNC, NAME, MSG );

/**
 * Add cases to the list using this macro
 */
#define ADD_TEST_CASE( CASE_STRUCT, INDEX, LIST_PTR, COUNT, NAME, MSG ) \
	AddTestCase( CASE_STRUCT, (twobytes) sizeof(CASE_STRUCT), &INDEX, LIST_PTR, (byte) COUNT, NAME );

/**
 * Create suit from list of cases  using this macro
 */
#define MAKE_TEST_SUITE( SUITE_STRUCT, CASE_LIST, CASE_STRUCT, COUNT, NAME ) \
{ \
	if ( (twobytes) sizeof(SMcUnitCase*) * ( (twobytes) COUNT + 1 ) > (twobytes) sizeof(CASE_LIST) )\
		f_simDataAccess.LogExitHalt( "Failed: Not enough SMcUnitCase* in MAKE_TEST_SUITE\n", 11 ); \
	for ( byte i = 0; i < (byte) COUNT; i++) \
		CASE_LIST[ i ] = &CASE_STRUCT[ i ]; \
	CASE_LIST[ COUNT ] = NULL; \
	SUITE_STRUCT.name = NAME; \
	SUITE_STRUCT.cases = CASE_LIST; \
	SUITE_STRUCT.numCases = COUNT; \
}

#define MAX_LOG_MSG_SIZE (m_uiLogSize)
#define MAX_LOG_LENGTH (m_uiLogMsgLength)
#define DEFAULT_LOG_SIZE (2)
#define DEFAULT_LOG_MSG_SIZE (64)
#define DEPTH_SUITE_REPORT (0)
#define DEPTH_CASE_REPORT  (1)
#define DEPTH_TEST_REPORT  (2)
#define MC_VERBOSE	// Logs expected vs. actual even if tests pass
/* Use this to set suite runner msg indent. Default is 0. */
extern const char *mcunit_suite_indent;

/* Use this to set case runner msg indent. Default is \t. */
extern const char *mcunit_case_indent;

/* Use this to set case runner msg indent. Default is \t\t. */
extern const char *mcunit_test_indent;

/**
 * @brief These are the results used in unit tests to indicate pass and fail
 */
enum mcunit_codes
{
	UNIT_TEST_PASSED = 0,
	UNIT_TEST_FAILED = -1
};

extern const char * g_lastSuiteName;
extern const char * g_lastCaseName;
extern const char * g_lastTestName;

/**
 * @class	CMcUnit
 * @brief	This class provides methods for the unit test harness.
 * 			These methods construct lists of tests in a hierachy of suites->tests. There are methods
 * 			to perform test result evaluation and have those results sent to the log. At test end the
 * 			results are summarized and added to the output log.
 *
 * Virtual Functions Overridden: None
 *
 * External Data Members: None
 *
 */
template<class TestClass>
class CMcUnit
{
private:
	void AddLabelsToFail( const char * sTest );

protected:

	// You must initialize the test class via the TestClass pointer

	/**
	 * 	Just a helper typedef.
	 */
	struct SMcUnitTest;
	typedef int (TestClass::* mcunit_test_fp)();

	/**
	 * 	@brief Defines and individual test.
	 *
	 * 	Message in msg will be printf'ed on report
	 * 	generation, so you can set it inside your test to tell something about
	 * 	what happened (why it failed). Default should be set to NULL, so no that
	 * 	no message will be printf'ed. If pointer to the function is NULL,
	 * 	test will not be run. This indicates end-of-list and case runner
	 * 	will stop on such test. NULLs. Always keep NULLs in mind.
	 */
	struct SMcUnitTest
	{
		mcunit_test_fp test; /*!< Pointer to test function           */
		int result; /*!< Test result, 0 is success          */
		const char * name; /*!< Optional test name                 */
		const char * msg; /*!< Hook-up any message you want here. */
	};

	/**
	 * 	@brief Aggregates tests into test case.
	 *
	 *  You don't need to initialize passed
	 * 	and runs, as they will be set correctly by the runner. Focus on *name
	 * 	and tests. List should be terminated with an empty test, ie. test
	 *	with test function pointer set to NULL! Tests are executed from
	 *	tests[0] to tests[n], so put your setup function first and tear down
	 *	last (but before NULL-terminator!) Remember about NULLs. NULLs are important.
	 */
	struct SMcUnitCase
	{
		const char * name; /*!< It's a good idea to set some friendly name */
		int passed; /*!< Counts passed tests                        */
		int runs; /*!< Counts how many tests are run              */
		int numTests; /*!< The number of tests to run	                */
		SMcUnitTest * tests; /*!< Terminate list with NULL-ed test function! */
	};

	/**
	 * 	@brief Aggregates test cases into a suite of tests.
	 *
	 * 	Think about setting some friendly name,
	 * 	don't bother with passed and runs as they are reset by the runner. Cases list
	 * 	should be NULL-terminated! Cases are run from cases[0] to cases[n]
	 *  (assuming cases[n+1] == NULL). Yes, NULLs are important. Again.
	 */
	struct SMcUnitSuite
	{
		const char * name; /*!< Suite name, used by report generator           */
		int passed; /*!< Passed cases counter                           */
		int runs; /*!< Run cases counter                              */
		int numCases; /*!< The number of tests to run	                */
		SMcUnitCase ** cases; /*!< NULL-terminated list of pointers to test cases */
	};
	TestClass * m_pTestIntfc;

public:
	CMcUnit( TestClass * pTestIntfc );

protected:
	int McUnitAssertIsOK( upCvasErr eCompare2, const char * sInfo = 0, byte bVerb = 1 );
	int McUnitAssertIsEqual( uint8_t bCompare1, uint8_t bCompare2, const char * sInfo = 0, byte bVerb = 1 );
	int McUnitAssertIsEqual( int16_t iCompare1, int16_t iCompare2, const char * sInfo = 0, byte bVerb = 1 );
	int McUnitAssertIsEqual( uint16_t uiCompare1, uint16_t uiCompare2, const char * sInfo = 0, byte bVerb = 1 );
	int McUnitAssertIsEqual( uint32_t ulCompare1, uint32_t ulCompare2, const char * sInfo = 0, byte bVerb = 1 );
	int McUnitAssertIsEqual( int32_t iCompare1, int32_t iCompare2, const char * sInfo = 0, byte bVerb = 1 );
	int McUnitAssertTrue( bool_c bAssertion, const char * sInfo = 0, byte bVerb = 1 );
	int McUnitTestRun( SMcUnitTest * pTest );
	int McUnitCaseRun( SMcUnitCase * pTestCase );
	int McUnitSuiteRun( SMcUnitSuite * pTestSuite );
	int McUnitTestReport( SMcUnitTest * pTest );
	int McUnitCaseReport( SMcUnitCase * pTestCase, int iDepth );
	void McUnitSuiteReport( SMcUnitSuite * pTestSuite, int iDepth );

	// Helper functions
	int LogOneValue( const char *formatStr, int value );
	float LogOneValue( const char *formatStr, float value );
	twobytes LogOneValue( const char *formatStr, twobytes value );
	void LogString( const char *pStr );
	void AddTestList( SMcUnitTest listStruct[], size_t stuctSize, int * index, mcunit_test_fp testFunc,
	        const char * name, const char * msg );
	void AddTestCase( SMcUnitCase * pMcUnitCase, twobytes tbSize, int * piCase, SMcUnitTest * pTestList, byte bCount,
	        const char * pCaseName );
};

/**
 * @brief	This method is the constructor.
 *
 * 			It receives the pointer to the object that actually performs the testing
 *
 * @param	pTestIntfc Pointer to TestClass to bring in handle to class for test definitions
 *
 * @return   Status as upCvasErr
 */
template<class TestClass>
CMcUnit<TestClass>::CMcUnit( TestClass * pTestIntfc )
{
	m_pTestIntfc = pTestIntfc;
	g_lastCaseName = 0;
	g_lastSuiteName = 0;
	g_lastTestName = 0;
}

/**
 * @brief	This method adds the test name, case name and suite name to the end of a failed test log entry.
 *
 * 		Any of these could be zero so those are not output.
 *
 * @param	sInfo The test name as const char *
 *
 */
static const char * pArrow = " <== ";
template<class TestClass>
void CMcUnit<TestClass>::AddLabelsToFail( const char * sInfo )
{

	if ( 0 != sInfo )
		{
		f_simDataAccess.PrintToSimulatorLog( sInfo );
		}
	if ( 0 != g_lastTestName )
		{
		f_simDataAccess.PrintToSimulatorLog( pArrow );
		f_simDataAccess.PrintToSimulatorLog( g_lastTestName );
		}
	if ( 0 != g_lastCaseName )
		{
		f_simDataAccess.PrintToSimulatorLog( pArrow );
		f_simDataAccess.PrintToSimulatorLog( g_lastCaseName );
		}
	if ( 0 != g_lastSuiteName )
		{
		f_simDataAccess.PrintToSimulatorLog( pArrow );
		f_simDataAccess.PrintToSimulatorLog( g_lastSuiteName );
		}
	f_simDataAccess.PrintToSimulatorLog( "\n" );
}

/**
 * 	@brief This method tests whether two int values are equal.
 *
 *  		The result is returned as UNIT_TEST_PASSED or UNIT_TEST_FAILED.
 *
 * 	@param	iCompare1	The first int value to compare
 *  @param  iCompare2	The second int value to compare
 *  @param	sInfo		An OPTIONAL string with an identifier applied if values are not equal
 *  @param	bVerb		Set verbose logging with TRUE and has default value of FALSE
 *
 * 	@return	UNIT_TEST_PASSED	== Success, test passed
 *  @return	UNIT_TEST_FAILED    == Failure, test failed
 */
template<class TestClass>
int CMcUnit<TestClass>::McUnitAssertIsEqual( int16_t iCompare1, int16_t iCompare2, const char * sInfo, byte bVerb )
{
	int result = UNIT_TEST_FAILED;

	if ( iCompare1 == iCompare2 )
		{
		result = UNIT_TEST_PASSED;
#ifdef MC_VERBOSE
		if ( bVerb )
			{
			snprintf( f_tempStr, MAX_TEMP_STR, "\t%sPassed: Expected = %d, Actual = %d, %s\n", mcunit_test_indent,
			        iCompare1, iCompare2, ( sInfo ? sInfo : "" ) );
			f_simDataAccess.PrintToSimulatorLog( f_tempStr );
			}
#endif // MC_VERBOSE
		}
	else
		{
		snprintf( f_tempStr, MAX_TEMP_STR, "==> %sFailed: Expected = %d, Actual = %d, ", mcunit_test_indent, iCompare1,
		        iCompare2 );
		f_simDataAccess.PrintToSimulatorLog( f_tempStr );
		AddLabelsToFail( sInfo );
		}

	return result;

}

/**
 * 	@brief This method tests whether a uint_16t values is OK.
 *
 *   		The result is returned as UNIT_TEST_PASSED or UNIT_TEST_FAILED.
 *
 *  @param  eCompare2	The upCvasErr value to compare to OK
 *  @param	sInfo		An OPTIONAL string with an identifier applied if values are not equal
 *  @param	bVerb		Set verbose logging with TRUE and has default value of FALSE
 *
 * 	@return	UNIT_TEST_PASSED	== Success, test passed
 *  @return	UNIT_TEST_FAILED    == Failure, test failed
 */
template<class TestClass>
int CMcUnit<TestClass>::McUnitAssertIsOK( upCvasErr eCompare2, const char * sInfo, byte bVerb )
{
	int result = UNIT_TEST_FAILED;

	if ( OK == eCompare2 )
		{
		result = UNIT_TEST_PASSED;
#ifdef MC_VERBOSE
		if ( bVerb )
			{
			snprintf( f_tempStr, MAX_TEMP_STR, "\t%sPassed: Expected = OK, Actual = %u, %s\n", mcunit_test_indent,
			        (uint16_t) eCompare2, ( sInfo ? sInfo : "" ) );
			f_simDataAccess.PrintToSimulatorLog( f_tempStr );
			}
#endif // MC_VERBOSE
		}
	else
		{
		snprintf( f_tempStr, MAX_TEMP_STR, "==> %sFailed: Expected = OK, Actual = %u, ", mcunit_test_indent,
		        (uint16_t) eCompare2 );
		f_simDataAccess.PrintToSimulatorLog( f_tempStr );
		AddLabelsToFail( sInfo );
		}

	return result;

}

/**
 * 	@brief This method tests whether two uint_16t values are equal.
 *
 *   		The result is returned as UNIT_TEST_PASSED or UNIT_TEST_FAILED.
 *
 * 	@param	uiCompare1	The first uint value to compare
 *  @param  uiCompare2	The second uint value to compare
 *  @param	sInfo		An OPTIONAL string with an identifier applied if values are not equal
 *  @param	bVerb		Set verbose logging with TRUE and has default value of FALSE
 *
 * 	@return	UNIT_TEST_PASSED	== Success, test passed
 *  @return	UNIT_TEST_FAILED    == Failure, test failed
 */
template<class TestClass>
int CMcUnit<TestClass>::McUnitAssertIsEqual( uint16_t uiCompare1, uint16_t uiCompare2, const char * sInfo, byte bVerb )
{
	int result = UNIT_TEST_FAILED;

	if ( uiCompare1 == uiCompare2 )
		{
		result = UNIT_TEST_PASSED;
#ifdef MC_VERBOSE
		if ( bVerb )
			{
			snprintf( f_tempStr, MAX_TEMP_STR, "\t%sPassed: Expected = %u, Actual = %u, %s\n", mcunit_test_indent,
			        uiCompare1, uiCompare2, ( sInfo ? sInfo : "" ) );
			f_simDataAccess.PrintToSimulatorLog( f_tempStr );
			}
#endif // MC_VERBOSE
		}
	else
		{
		snprintf( f_tempStr, MAX_TEMP_STR, "==> %sFailed: Expected = %u, Actual = %u, ", mcunit_test_indent,
		        uiCompare1, uiCompare2 );
		f_simDataAccess.PrintToSimulatorLog( f_tempStr );
		AddLabelsToFail( sInfo );
		}

	return result;

}

/**
 * 	@brief This method tests whether two uint_32t values are equal.
 *
 *  The result is returned as UNIT_TEST_PASSED or UNIT_TEST_FAILED.
 *
 * 	@param	ulCompare1	The first uint_32t value to compare
 *  @param  ulCompare2	The second uint_32t value to compare
 *  @param	sInfo		An OPTIONAL string with an identifier applied if values are not equal
 *  @param	bVerb		Set verbose logging with TRUE and has default value of FALSE
 *
 * 	@return	UNIT_TEST_PASSED	== Success, test passed
 *  @return	UNIT_TEST_FAILED    == Failure, test failed
 */
template<class TestClass>
int CMcUnit<TestClass>::McUnitAssertIsEqual( uint32_t ulCompare1, uint32_t ulCompare2, const char * sInfo, byte bVerb )
{
	int result = UNIT_TEST_FAILED;
	if ( ulCompare1 == ulCompare2 )
		{
		result = UNIT_TEST_PASSED;
#ifdef MC_VERBOSE
		if ( bVerb )
			{
			snprintf( f_tempStr, MAX_TEMP_STR, "\t%sPassed: Expected = %lu, Actual = %lu, %s\n", mcunit_test_indent,
			        ulCompare1, ulCompare2, ( sInfo ? sInfo : "" ) );
			f_simDataAccess.PrintToSimulatorLog( f_tempStr );
			}
#endif // MC_VERBOSE
		}
	else
		{
		snprintf( f_tempStr, MAX_TEMP_STR, "==> %sFailed: Expected = %lu, Actual = %lu, ", mcunit_test_indent,
		        ulCompare1, ulCompare2 );
		f_simDataAccess.PrintToSimulatorLog( f_tempStr );
		AddLabelsToFail( sInfo );
		}

	return result;

}

/**
 * 	@brief This method tests whether two long int values are equal.
 *
 * 	The result is returned as UNIT_TEST_PASSED or UNIT_TEST_FAILED.
 *
 * 	@param	iCompare1	The first long int value to compare
 *  @param  iCompare2	The second long int value to compare
 *  @param	sInfo		An OPTIONAL string with an identifier applied if values are not equal
 *  @param	bVerb		Set verbose logging with TRUE and has default value of FALSE
 *
 * 	@return	UNIT_TEST_PASSED	== Success, test passed
 *  @return	UNIT_TEST_FAILED    == Failure, test failed
 */
template<class TestClass>
int CMcUnit<TestClass>::McUnitAssertIsEqual( int32_t iCompare1, int32_t iCompare2, const char * sInfo, byte bVerb )
{
	int result = UNIT_TEST_FAILED;

	if ( iCompare1 == iCompare2 )
		{
		result = UNIT_TEST_PASSED;
#ifdef MC_VERBOSE
		if ( bVerb )
			{
			snprintf( f_tempStr, MAX_TEMP_STR, "\t%sPassed: Expected = %ld, Actual = %ld, %s\n", mcunit_test_indent,
			        iCompare1, iCompare2, ( sInfo ? sInfo : "" ) );
			f_simDataAccess.PrintToSimulatorLog( f_tempStr );
			}
#endif // MC_VERBOSE
		}
	else
		{
		snprintf( f_tempStr, MAX_TEMP_STR, "==> %sFailed: Expected = %ld, Actual = %ld, ", mcunit_test_indent,
		        iCompare1, iCompare2 );
		f_simDataAccess.PrintToSimulatorLog( f_tempStr );
		AddLabelsToFail( sInfo );
		}

	return result;

}

/**
 * 	@brief This method tests whether two byte values are equal.
 *
 * The result is returned as UNIT_TEST_PASSED or UNIT_TEST_FAILED.
 *
 * 	@param	bCompare1	The first byte value to compare
 *  @param  bCompare2	The second byte value to compare
 *  @param	sInfo		An OPTIONAL string with an identifier applied if values are not equal
 *  @param	bVerb		Set verbose logging with TRUE and has default value of FALSE
 *
 * 	@return	UNIT_TEST_PASSED	== Success, test passed
 *  @return	UNIT_TEST_FAILED    == Failure, test failed
 */
template<class TestClass>
int CMcUnit<TestClass>::McUnitAssertIsEqual( uint8_t bCompare1, uint8_t bCompare2, const char * sInfo, byte bVerb )
{
	int result = UNIT_TEST_FAILED;

	if ( bCompare1 == bCompare2 )
		{
		result = UNIT_TEST_PASSED;
#ifdef MC_VERBOSE
		if ( bVerb )
			{
			snprintf( f_tempStr, MAX_TEMP_STR, "\t%sPassed: Expected = %d, Actual = %d, %s\n", mcunit_test_indent,
			        (int) bCompare1, (int) bCompare2, ( sInfo ? sInfo : "" ) );
			f_simDataAccess.PrintToSimulatorLog( f_tempStr );
			}
#endif // MC_VERBOSE
		}
	else
		{
		snprintf( f_tempStr, MAX_TEMP_STR, "==> %sFailed: Expected = %d, Actual = %d, ", mcunit_test_indent,
		        (int) bCompare1, (int) bCompare2 );
		f_simDataAccess.PrintToSimulatorLog( f_tempStr );
		AddLabelsToFail( sInfo );
		}

	return result;
}

/**
 * 	@brief This method tests whether a boolean value is true.
 *
 * 	This method is useful for evaluating an expression.
 *
 * 	@param	bAssertion	The boolean to assert
 *  @param	sInfo		An OPTIONAL string with an identifier applied if value is not TRUE
 *  @param	bVerb		Set verbose logging with TRUE and has default value of FALSE
 *
 * 	@return	UNIT_TEST_PASSED	== Success, test passed
 *  @return	UNIT_TEST_FAILED    == Failure, test failed
 */
template<class TestClass>
int CMcUnit<TestClass>::McUnitAssertTrue( bool_c bAssertion, const char * sInfo, byte bVerb )
{
	int result = UNIT_TEST_FAILED;

	if ( bAssertion )
		{
		result = UNIT_TEST_PASSED;
#ifdef MC_VERBOSE
		if ( bVerb )
			{
			snprintf( f_tempStr, MAX_TEMP_STR, "\t%sPassed: Expected = TRUE, Actual = TRUE %s\n", mcunit_test_indent,
			        ( sInfo ? sInfo : "" ) );
			f_simDataAccess.PrintToSimulatorLog( f_tempStr );
			}
#endif // MC_VERBOSE
		}
	else
		{
		snprintf( f_tempStr, MAX_TEMP_STR, "==> %sFailed: Expected = TRUE, Actual = FALSE ", mcunit_test_indent );
		f_simDataAccess.PrintToSimulatorLog( f_tempStr );
		AddLabelsToFail( sInfo );
		}

	return result;
}

/**
 * 	@brief This method runs a single test.
 *
 * 	@param	pTest A pointer to the test structure containing test function pointer, result code, etc.
 *
 * 	@return         0	== Success, test passed
 *  @return			>0	== Test failed. Result will be kept in test structure for report generator.
 * 	@return			-1	== No test function set, this means end-of-list for case runner.
 */
template<class TestClass>
int CMcUnit<TestClass>::McUnitTestRun( SMcUnitTest * pTest )
{
	int iRes = UNIT_TEST_FAILED;

	/* Check function pointer. If NULL, we have a bad test */
	if ( pTest->test != 0 )
		{
		snprintf( f_tempStr, MAX_TEMP_STR, "%sTest: %s\n", mcunit_test_indent, pTest->name );
		f_simDataAccess.PrintToSimulatorLog( f_tempStr );
		g_lastTestName = pTest->name;
		iRes = CALL_MEMBER_FN( m_pTestIntfc, pTest );
		}

	pTest->result = iRes;
	return iRes;

}

/**
 * 	@brief	This method runs a test case which is a set of tests.
 *
 *  @param	pTestCase A pointer to the test case structure containing list of individual tests,	result code, etc.
 *
 *  @return	Number of failed tests. 0 means all tests passed.
 */
template<class TestClass>
int CMcUnit<TestClass>::McUnitCaseRun( SMcUnitCase * pTestCase )
{
	int index;

	snprintf( f_tempStr, MAX_TEMP_STR, "%sCase: %s\n", mcunit_case_indent, pTestCase->name );
	f_simDataAccess.PrintToSimulatorLog( f_tempStr );
	g_lastCaseName = pTestCase->name;

	pTestCase->passed = UNIT_TEST_PASSED;
	pTestCase->runs = UNIT_TEST_PASSED;
	index = 0;
	/*
	 * 	Run tests until end of list. eol is reported by test runner.
	 */
	while ( index < pTestCase->numTests )
		{
		if ( UNIT_TEST_PASSED == McUnitTestRun( &pTestCase->tests[ index ] ) )
			pTestCase->passed++;
		pTestCase->runs++;
		index++;
		}

	return ( pTestCase->runs - pTestCase->passed );
}

/**
 * 	@brief This method runs a test suite which is a set of cases.
 *
 * 	@param	pTestSuite A pointer to the test suite structure containing list of test cases, result code, etc.
 *
 * 	@return	Number of failed test cases. 0 means all test cases passed.
 */
template<class TestClass>
int CMcUnit<TestClass>::McUnitSuiteRun( SMcUnitSuite * pTestSuite )
{
	int index = 0;
	pTestSuite->passed = UNIT_TEST_PASSED;
	pTestSuite->runs = UNIT_TEST_PASSED;
	f_simDataAccess.PrintToSimulatorLog( "\n==================================================\n" );
	snprintf( f_tempStr, MAX_TEMP_STR, "%sTesting Suite: %s\n", mcunit_suite_indent, pTestSuite->name );
	f_simDataAccess.PrintToSimulatorLog( f_tempStr );
	g_lastSuiteName = pTestSuite->name;

	/*
	 * Iterate over cases list until NULL is found. Invoke case runner
	 * for every case.
	 */
	while ( ( pTestSuite->cases[ index ] != NULL ) && index < pTestSuite->numCases )
		{
		if ( McUnitCaseRun( pTestSuite->cases[ index ] ) == 0 )
			{
			pTestSuite->passed++;
			}
		pTestSuite->runs++;
		index++;
		}
	return ( pTestSuite->runs - pTestSuite->passed ); /* All should pass */
}

/**
 * @brief This method produces a test status report.
 *
 * @param	pTest A pointer to the test structure containing test results.
 *
 * @return 1 if test failed or 0 is passed
 */
template<class TestClass>
int CMcUnit<TestClass>::McUnitTestReport( SMcUnitTest * pTest )
{
	int iRes = 0;

	if ( 0 == pTest )
		{
		f_simDataAccess.PrintToSimulatorLog( "Failed: pTest pointer zero" );
		return 1;
		}

	if ( UNIT_TEST_PASSED == pTest->result )
		{
		snprintf( f_tempStr, MAX_TEMP_STR, "%sPassed: %s\n", mcunit_test_indent, pTest->name );
		f_simDataAccess.PrintToSimulatorLog( f_tempStr );
		}
	else
		{
		int rtnLength = 0;
		rtnLength = snprintf( f_tempStr, MAX_TEMP_STR, "%sFailed: %s\n", mcunit_test_indent, pTest->name );

		// log a message if it was assigned in McUnitTestStruct
		if ( rtnLength < MAX_TEMP_STR && pTest->msg )
			{
			snprintf( f_tempStr + rtnLength - 1, MAX_TEMP_STR - rtnLength + 1, ", %s\n", pTest->msg );
			}
		f_simDataAccess.PrintToSimulatorLog( f_tempStr );
		iRes = 1;
		}

	return iRes;
}

/**
 * 	@brief This method prints test case results. If depth == 0, only brief results are printf'ed.
 *
 * 	@param	pTestCase 	A pointer to the test structure containing test results.
 * 	@param	iDepth 	If >0, print individual test results too.
 *
 *  @return Count of failed tests
 */
template<class TestClass>
int CMcUnit<TestClass>::McUnitCaseReport( SMcUnitCase * pTestCase, int iDepth )
{
	byte index = 0;
	int iFails = 0;

	if ( 0 == pTestCase )
		{
		f_simDataAccess.PrintToSimulatorLog( "Failed: pTestCase pointer zero" );
		return 1;
		}

	snprintf( f_tempStr, MAX_TEMP_STR, "%s%s: Passed %d/%d tests\n", mcunit_case_indent, pTestCase->name,
	        pTestCase->passed, pTestCase->runs );
	f_simDataAccess.PrintToSimulatorLog( f_tempStr );

	while ( iDepth > DEPTH_CASE_REPORT && index < pTestCase->numTests )
		{
		iFails += McUnitTestReport( pTestCase->tests + index );
		index++;
		}

	return iFails;
}

/**
 * 	@brief Print test case results. If depth == 0, only brief results are printf'ed.
 *
 * 	@param	pTestSuite 	A pointer to the test structure containing test results.
 *  @param	iDepth 		If 1, print individual test cases results too. If >=2, print individial tests too (full report)
 *
 */
template<class TestClass>
void CMcUnit<TestClass>::McUnitSuiteReport( SMcUnitSuite * pTestSuite, int iDepth )
{
	SMcUnitCase * pTestCase;

	int ci = 0;
	int iFails = 0;

	if ( 0 == pTestSuite )
		{
		f_simDataAccess.PrintToSimulatorLog( "Failed: pTestSuite pointer zero" );
		return;
		}

	snprintf( f_tempStr, MAX_TEMP_STR, "\n\n%s: Passed %d/%d cases\n", pTestSuite->name, pTestSuite->passed,
	        pTestSuite->runs );
	f_simDataAccess.PrintToSimulatorLog( f_tempStr );

	while ( iDepth > DEPTH_SUITE_REPORT && ci < pTestSuite->numCases )
		{
		pTestCase = pTestSuite->cases[ ci ];
		if ( pTestCase != 0 )
			iFails += McUnitCaseReport( pTestCase, iDepth );
		ci++;
		}

	f_simDataAccess.PrintToSimulatorLog( "\n\n" );
	f_simDataAccess.SetExitCode( iFails );
}

//=====================================================================================
// UNIT TESTING HELPER FUNCTIONS
//=====================================================================================

/**
 * @brief	This method formats a single integer value into the specified format and writes it to the simulator log.
 *
 * @param	formatStr	A constant format string that must include a single parameter to format (%d, %x, etc.)
 * @param	value		An integer value to be inserted into the format string
 *
 * @return  result as an int:  This is the same result as was passed in.
 */
template<class TestClass>
int CMcUnit<TestClass>::LogOneValue( const char *formatStr, int value )
{
	snprintf( f_tempStr, MAX_TEMP_STR, formatStr, value );
	LogString( f_tempStr );
	return value;
}

/**
 * @brief	This method formats a single float value into the specified format and writes it to the simulator log.
 *
 * @param	formatStr	A constant format string that must include a single parameter to format (%f)
 * @param	value		An float  value to be inserted into the format string
 *
 * @return  result as an float:  This is the same result as was passed in.
 */
template<class TestClass>
float CMcUnit<TestClass>::LogOneValue( const char *formatStr, float value )
{
	snprintf( f_tempStr, MAX_TEMP_STR, formatStr, value );
	LogString( f_tempStr );
	return value;
}

/**
 * @brief	This method formats a single twobytes value into the specified format and writes it to the simulator log.
 *
 * @param	formatStr	A constant format string that must include a single parameter to format (%ld, %X)
 * @param	value		An float  value to be inserted into the format string
 *
 * @return  result as an twobytes :  This is the same result as was passed in.
 */
template<class TestClass>
twobytes CMcUnit<TestClass>::LogOneValue( const char *formatStr, twobytes value )
{
	snprintf( f_tempStr, MAX_TEMP_STR, formatStr, value );
	LogString( f_tempStr );
	return value;
}

/**
 * @brief	This method formats a single integer value into the specified format and writes it to the simulator log.
 *
 * @param	pStr	A constant format string that must include a single parameter to format (%d, %x, etc.)
 *
 * @return  result as an int:  This is the same result as was passed in.
 */
template<class TestClass>
void CMcUnit<TestClass>::LogString( const char *pStr )
{
	f_simDataAccess.PrintToSimulatorLog( "\t\t\t" );
	f_simDataAccess.PrintToSimulatorLog( pStr );
	f_simDataAccess.PrintToSimulatorLog( "\n" );
}

/**
 * @brief	This method adds a test method to a list of methods.
 *
 * It checks that the list is long enough before adding the test and halts the program if there is not enough entries.
 *
 * @param SlistStruct A pointer to the available structure list as McUnitTest *
 * @param structSize The size of the list structure as size_t
 * @param index A pointer to the running count of tests as int *
 * @param testFunc A pointer to the test method as int(TestClass::* method)( SMcUnitTest *_test )
 * @param name The name of the test as C string by const char *
 * @param msg name The error message to give if the test fails as C string by const char *
 *
 */
template<class TestClass>
void CMcUnit<TestClass>::AddTestList( SMcUnitTest * listStruct, size_t structSize, int * index,
        mcunit_test_fp testFunc, const char * name, const char * msg )
//        int(TestClass::* testFunc)( SMcUnitTest *_test ), const char * name, const char * msg )
{
	if ( (twobytes) sizeof(SMcUnitTest) * (twobytes) *index >= (twobytes) structSize )
		{
		f_simDataAccess.LogExitHalt( "Failed: Not enough SMcUnitTest in ADD_TEST_LIST\n", 11 );
		}
	SMcUnitTest * pTest = listStruct + *index;
	pTest->name = name;
	pTest->msg = msg;
	pTest->result = UNIT_TEST_FAILED;
	pTest->test = testFunc;
	( *index )++;
}

/**
 * @brief	This method adds a test method to a list of methods.
 *
 * It checks that the list is long enough before adding the test and halts the program if there is not enough entries.
 *
 * @param pMcUnitCase A pointer to the available structure list as SMcUnitCase *
 * @param tbSize The size of the list structure as twobytes
 * @param piCase A pointer to the running count of cases as int *
 * @param pTestList A pointer to the list of tests for this case as pTestList *
 * @param bCount The count of tests in the test list for this case
 * @param pCaseName The name of the test as C string by const char *
 *
 */
template<class TestClass>
void CMcUnit<TestClass>::AddTestCase( SMcUnitCase * pMcUnitCase, twobytes tbSize, int * piCase,
        SMcUnitTest * pTestList, byte bCount, const char * pCaseName )
{
	twobytes tbCase = (twobytes) *piCase;
	if ( (twobytes) sizeof(SMcUnitCase) * tbCase >= tbSize )
		f_simDataAccess.LogExitHalt( "Failed: Not enough SMcUnitCase in ADD_TEST_CASE\n", 11 );
	SMcUnitCase * pTemp = pMcUnitCase + tbCase;
	pTemp->name = pCaseName;
	pTemp->tests = pTestList;
	pTemp->numTests = bCount;
	( *piCase )++;
}

#endif /* MCUNIT_H_ */

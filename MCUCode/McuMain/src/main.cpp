/*
 * main.cpp
 *
 *  Created on: Apr 7, 2017
 *      Author: Glen
 */

#include "UGMcuDefinitions.h"
#include "stm32f4xx_hal.h"


extern "C" {
extern int HalInit();
 }
 
int main()
{
	return HalInit();
}
